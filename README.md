# README #

This document serves as a guide to contribute to **Tez Financial Services Android App**.

## How do I get it set up? ##

* Intall [JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* Intall [JRE](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)
* Install [Android Studio](https://developer.android.com/studio/index.html)
* Run Android Studio
* **File** -> **New** -> **Project from Version Control** -> **Git**
* Add this URL(https://%YOUR_BITBUCKET_USER_NAME%@bitbucket.org/greditkeek/mobiledost-android-app.git) as the Git Repository URL 
* Click Clone
* Build and Run!

### Code Architecture ###
The app's code is structured in the following manner:
```
└───com.mobiledost.androidapp
    ├───activities
    │   └───ui
    ├───adapters
    ├───callbacks
    ├───fcm
    ├───fragments
    ├───models
    ├───preferences
    ├───receivers
    ├───repository
    │   ├───database
    │   │   ├───models
    │   │   └───store
    │   ├───network
    │   │   ├───api
    │   │   ├───handlers
    │   │   │   └───callbacks
    │   │   ├───models
    │   │   │   ├───request
    │   │   │   └───response
    │   │   ├───store
    │   │   └───utils
    │   └───store
    ├───utils
    │   ├───extract
    │   │   └───models
    │   └───textwatchers
    ├───viewholders
    └───views
```

### What do each of the directories hold? ###

- **activities** - All your activity classes go here
- **activities -> ui** - If an activity class becomes too long and you want to separate the UI and business logic, you can add all UI related code as a Base Activity for the particular Activity in this package. For example, in our app, the `SigUpActivity` is a fairly long one. It is broken into 2 classes, `com.mobiledost.androidapp.activities.ui.SignUpFormBaseActivity` and `com.mobiledost.androidapp.activities.SignUpFormActivity`(This class extends the `SignUpFormBaseActivity`) The `SignUpFormBaseActivity` contains all UI related code whereas the `SignUpFormActivity` contains business logic.
- **adapters** - All `ListView`/`RecyclerView`/`ViewPager` adapters go here. Basically all Adapters.
- **callbacks** - All system wide non network call related callbacks go here.
- **fcm** - All Firebase notification related code goes here.
- **fragments** - All Fragments go here. If a Fragment becomes too long, the approach similar to activities can be applied here.
- **models** - All non network related models go here.
- **preferences** - Code to manage Shared Preferences goes here.
- **receivers** - All BroadcastReceiver sub classes go here.
- **repository** - This package serves as a central point to access resources, be it from the database or from the server, but right now, we are not using it as we are directly calling server methods, since there is no local storage of data. The `repository` package will contain classes that provide wrappers over local and server CRUD methods. For example, if we execute a method `getCities()` on repository class. The caller is unaware of whether the data will fetched from the server or from the the database. The `getCities()` method in the repository will manage the conditions on when data should be fetched from server or from DB.
- **repository -> store** - Intended to have an interface, this interface is to implemented by the local and server stores, so that when a method is called on the repository class' object, the relevant store is initialized and data is returned.
- **repository -> database** - This package will contain DB stores and models.
- **repository -> database -> models** - This package will contain DB models.
- **repository -> database -> store** - The implementations of the stores in the `repository.store` package go here.
- **repository -> network** - This package will contain network stores, models, endpoints, and callback handlers.
- **repository -> network -> api** - This package contains Retrofit API interfaces, which are basically all the end points to access.
- **repository -> network -> handlers** - This package contains the classes to handle responses of different API calls. All classes in this package should end with `RH` suffix. All handler classes should extend the `BaseRH` generic class.
- **repository -> network -> handlers -> callbacks** - This package contains all the callbacks related to API calls. If you want that a particular API call's result should be handled in some other class too besides the response handler class, you can create a callback here for it.
- **repository -> network -> models** - All network models that are not request or response models but are objects as part of the request or response, in other words objects that have a HAS A relationship with the request or response class go here.
- **repository -> network -> models -> request** - All request models for Retrofit API calls go here.
- **repository -> network -> models -> response** - All response models for Retrofit API calls go here. These models should extend the `BaseResponse` class.
- **repository -> network -> store** - Ideally, the implementations of the stores in the `repository.store` package go here. But right now, since there is no database caching. The stores are concrete classes extended from the `BaseCloudDataStore` class. The store classes contain methods that make the actual API calls. 
- **repository -> network -> util** - This package contains utility classes for API calls. It could be interceptors or anything that cannot be added to the other network related packages.
- **utils** - This package contains all utility classes that cannot be listed in other packages.
- **utils -> extract** - This package contains data extraction related utility classes and services.
- **utils -> extract -> models** - All models for extraction of SMS, Calls, Events, Accounts etc go here.
- **utils -> textwatchers** - `EditText` TextWatchers go here for re-usability.
- **viewholders** - ViewHolders for different Adapters go here.
- **views** - Views for the app go here. 
    





### Who do I talk to? ###

* If any queries, email to [eric.bhatti@venturedive.com](mailto:eric.bhatti@venturedive.com)