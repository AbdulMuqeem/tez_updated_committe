package net.tez.storage.library.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import net.tez.storage.library.converters.ObjectToStringParser;
import net.tez.storage.library.converters.StringToObjectParser;
import net.tez.storage.library.core.StorageAdapter;
import net.tez.storage.library.strategy.PlainTextTransformStrategy;
import net.tez.storage.library.strategy.TextTransformStrategy;
import net.tez.storage.library.utils.ObjectUtil;

import java.lang.reflect.Type;


/**
 * Created by FARHAN DHANANI on 5/18/2018.
 */

@SuppressWarnings({"ConstantConditions","unused","WeakerAccess"})
public class SharedPreferenceAdapter implements StorageAdapter {

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private StringToObjectParser stringToObjectParser;
    private ObjectToStringParser objectToStringParser;
    private TextTransformStrategy transformStrategy;

    private SharedPreferenceAdapter(Builder builder) {
        prefs = builder.context.getSharedPreferences(builder.fileName, builder.mode);
        editor = prefs.edit();
        editor.apply();
        stringToObjectParser = builder.stringToObjectParser;
        objectToStringParser = builder.objectToStringParser;
        transformStrategy = builder.transformStrategy;
    }

    @Override
    public void put(@NonNull String key, int value) {
        put(key, Integer.toString(value));
    }

    @Override
    public void put(@NonNull String key, long value) {
        put(key, Long.toString(value));
    }

    @Override
    public void put(@NonNull String key, float value) {
        put(key, Float.toString(value));
    }

    @Override
    public void put(@NonNull String key, double value) {
        put(key, Double.toString(value));
    }

    @Override
    public void put(@NonNull String key, boolean value) {
        put(key, Boolean.toString(value));
    }

    @Override
    public void put(@NonNull String key, @Nullable String value) {
        if(value!=null) {
            editor.putString(key, transformStrategy.transformOnPut(value));
            editor.apply();
        }
    }

    @Override
    public void put(@NonNull String key, @Nullable Object value) {
        put(key, value, objectToStringParser);
    }

    @Override
    public void put(@NonNull String key, @Nullable Object value, @NonNull ObjectToStringParser parser) {
        if(value!=null)
            put(key, parser.convertToString(value));
        else
            remove(key);
    }

    @Override
    public int getInt(@NonNull String key) {
        try {
            return Integer.valueOf(getString(key, Integer.toString(0)));
        }catch (NumberFormatException e){
            return 0;
        }
    }

    @Override
    public int getInt(@NonNull String key, int defaultValue) {
        try {
            return Integer.valueOf(getString(key, Integer.toString(defaultValue)));
        }catch (NumberFormatException e){
            return defaultValue;
        }
    }

    @Override
    public long getLong(@NonNull String key) {
        try {
            return Long.valueOf(getString(key, Long.toString(0)));
        }catch (NumberFormatException e){
            return 0;
        }
    }

    @Override
    public long getLong(@NonNull String key, long defaultValue) {
        try {
            return Long.valueOf(getString(key, Long.toString(defaultValue)));
        } catch (NumberFormatException e){
            return defaultValue;
        }
    }

    @Override
    public float getFloat(@NonNull String key) {
        try {
            return Float.valueOf(getString(key, Float.toString(0)));
        }catch (NumberFormatException e){
            return 0;
        }
    }

    @Override
    public float getFloat(@NonNull String key, float defaultValue) {
        try {
            return Float.valueOf(getString(key, Float.toString(defaultValue)));
        }catch (NumberFormatException e){
            return defaultValue;
        }
    }

    @Override
    public double getDouble(@NonNull String key) {
        try {
            return Double.valueOf(getString(key, Double.toString(0)));
        }catch (NumberFormatException e){
            return 0;
        }
    }

    @Override
    public double getDouble(@NonNull String key, double defaultValue) {
        try {
            return Double.valueOf(getString(key, Double.toString(defaultValue)));
        }
        catch (Exception ex) {
            return defaultValue;
        }
    }

    @Override
    public boolean getBoolean(@NonNull String key) {
        return Boolean.valueOf(getString(key, Boolean.toString(false)));
    }

    @Override
    public boolean getBoolean(@NonNull String key, boolean defaultValue) {
        return Boolean.valueOf(getString(key, Boolean.toString(defaultValue)));
    }

    @NonNull
    @Override
    public String getString(@NonNull String key) {
        return getString(key, "");
    }

    @NonNull
    @Override
    public String getString(@NonNull String key, @Nullable String defaultValue) {
        String s = prefs.getString(key,defaultValue);
        if(TextUtils.equals(s, defaultValue))
            return defaultValue;

        return transformStrategy.transformOnGet(s);
    }

    @Nullable
    @Override
    public <T> T get(@NonNull String key, @NonNull Class<T> typeClass) {
        return get(key, typeClass, stringToObjectParser);
    }

    @Nullable
    @Override
    public <T> T get(@NonNull String key, @NonNull Type type) {
        return stringToObjectParser.convertListToObject(getString(key), type);
    }

    @Nullable
    @Override
    public <T> T get(@NonNull String key, @NonNull Class<T> typeClass, @NonNull StringToObjectParser parser) {
        return parser.convertToObject(getString(key), typeClass);
    }



    @Override
    public void remove(@NonNull String key) {
        editor.remove(key);
        editor.apply();
    }

    @Override
    public void clear() {
        editor.clear();
        editor.apply();
    }

    @Override
    public boolean contains(@NonNull String key) {
        return prefs.contains(key);
    }

    public static class Builder{

        @NonNull private Context context;
        @NonNull private String fileName;
        private int mode;
        private StringToObjectParser stringToObjectParser;
        private ObjectToStringParser objectToStringParser;
        private TextTransformStrategy transformStrategy;

        public Builder(@Nullable Context context) {
            this.context = ObjectUtil.requireNonNull(context);
            fileName = "sp_tez";
            mode = Context.MODE_PRIVATE;
            transformStrategy = new PlainTextTransformStrategy();
        }

        public Builder setFileName(@NonNull String fileName) {
            this.fileName = fileName;
            return this;
        }

        public Builder setMode(int mode) {
            this.mode = mode;
            return this;
        }

        public Builder setStringToObjectParser(@NonNull StringToObjectParser stringToObjectParser) {
            this.stringToObjectParser = stringToObjectParser;
            return this;
        }

        public Builder setObjectToStringParser(@NonNull ObjectToStringParser objectToStringParser) {
            this.objectToStringParser = objectToStringParser;
            return this;
        }

        public Builder setTextTransformStrategy(@NonNull TextTransformStrategy transformStrategy) {
            this.transformStrategy = transformStrategy;
            return this;
        }

        public SharedPreferenceAdapter build() {
            return new SharedPreferenceAdapter(this);
        }
    }

}