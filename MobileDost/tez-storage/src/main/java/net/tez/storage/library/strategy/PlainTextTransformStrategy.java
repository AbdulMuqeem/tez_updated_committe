package net.tez.storage.library.strategy;

import androidx.annotation.NonNull;

/**
 * Created by FARHAN DHANANI on 1/17/2019.
 */
public class PlainTextTransformStrategy implements TextTransformStrategy {
    @NonNull
    @Override
    public String transformOnPut(@NonNull String value) {
        return value;
    }

    @NonNull
    @Override
    public String transformOnGet(@NonNull String value) {
        return value;
    }
}
