package net.tez.storage.library.converters;

import androidx.annotation.NonNull;

import java.lang.reflect.Type;

/**
 * Created by FARHAN DHANANI on 5/7/2018.
 */

public interface StringToObjectParser {

    <T> T convertToObject(@NonNull String data, @NonNull Class<T> clazz);
    <T> T convertListToObject(@NonNull String data, @NonNull Type type);
}
