package net.tez.filepicker.base;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentActivity;

import com.example.vendvinodkumar.tezfilepicker.R;

import net.tez.filepicker.HostActivity;
import net.tez.filepicker.request.ClientRequest;
import net.tez.fragment.util.base.AbstractHelperFragment;
import net.tez.fragment.util.optional.Optional;

/**
 * Created by VINOD KUMAR on 3/27/2019.
 */
public abstract class BaseFragment extends AbstractHelperFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Optional.doWhen(!isSafeObject(getClientRequest()), this::finishActivity);
    }

    @Override
    public void onStart() {
        super.onStart();
        isInstanceOfActivity(HostActivity.class, hostActivity -> hostActivity.registerFragment(this));
    }


    @Nullable
    protected HostActivity getHostActivity() {
        return isInstanceOfActivity(HostActivity.class);
    }

    @Nullable
    protected View getHostActivityViewById(@IdRes int id) {
        return Optional.ifPresent(getHostActivity(), hostActivity -> {
            return hostActivity.getHostActivityViewById(id);
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String title = getClientRequest().getTitle();

        String safeTitle = isSafeObject(title) ? title : "";

        Optional.isInstanceOf(getHostActivityViewById(R.id.tvTitle),
                AppCompatTextView.class,
                tvTitle -> tvTitle.setText(safeTitle));
    }

    public boolean onBackPressed() {
        return true;
    }

    public ClientRequest getClientRequest() {
        HostActivity hostActivity = getHostActivity();
        return hostActivity != null ? hostActivity.getClientRequest() : null;
    }

    protected void finishActivity() {
        getBaseFragmentActivity(FragmentActivity::finish);
    }
}
