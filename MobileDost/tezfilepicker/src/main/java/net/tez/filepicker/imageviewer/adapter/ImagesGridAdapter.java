package net.tez.filepicker.imageviewer.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.example.vendvinodkumar.tezfilepicker.R;

import net.tez.filepicker.imageviewer.listener.ImageSelectListener;
import net.tez.filepicker.imageviewer.model.MediaImage;
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VINOD KUMAR on 3/27/2019.
 */
public class ImagesGridAdapter extends GenericRecyclerViewAdapter<MediaImage, ImageSelectListener, ImagesGridAdapter.ImageViewHolder> {

    private boolean isSingleImage;
    private int maxAllowedImages;
    private int totalSelected;

    public ImagesGridAdapter(@NonNull List<MediaImage> items,
                             @Nullable ImageSelectListener listener,
                             boolean isSingleImage,
                             int maxAllowedImages) {
        super(items, listener);
        this.isSingleImage = isSingleImage;
        this.maxAllowedImages = maxAllowedImages;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ImageViewHolder(inflate(parent.getContext(), R.layout.list_item_image, parent));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        holder.onBind(position, getItems(), getListener());
    }

    @NonNull
    public List<String> getCheckedImagesPaths() {
        List<String> checkedImages = new ArrayList<>();
        for (MediaImage image : getItems())
            if (image.isChecked())
                checkedImages.add(image.getPath());
        return checkedImages;
    }

    class ImageViewHolder extends BaseViewHolder<MediaImage, ImageSelectListener> {

        private final AppCompatImageView ivThumbnail;
        private final AppCompatCheckBox cbThumbnail;
        private final View viewOverlay;

        ImageViewHolder(View itemView) {
            super(itemView);
            this.ivThumbnail = itemView.findViewById(R.id.ivThumbnail);
            this.cbThumbnail = itemView.findViewById(R.id.cbThumbnail);
            this.viewOverlay = itemView.findViewById(R.id.viewOverlay);
        }

        @Override
        public void onBind(int position, List<MediaImage> items, @Nullable ImageSelectListener listener) {
            super.onBind(position, items, listener);

            if (listener == null)
                return;

            MediaImage item = items.get(position);

            Glide.with(itemView)
                    .load(item.getPath())
                    .centerCrop()
                    .into(ivThumbnail);

            cbThumbnail.setOnCheckedChangeListener(null);

            viewOverlay.setVisibility(item.isChecked() ? View.VISIBLE : View.GONE);

            cbThumbnail.setChecked(item.isChecked());

            cbThumbnail.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (totalSelected <= maxAllowedImages)
                    onCheckedChange(item, listener, isChecked);

                if (totalSelected == maxAllowedImages) {
                    totalSelected++;
                    listener.onImagesSelected(getCheckedImagesPaths());
                }
            });

            if (isSingleImage) {
                this.cbThumbnail.setVisibility(View.GONE);
                this.setItemViewOnClickListener(view -> {
                    view.startAnimation(AnimationUtils.loadAnimation(view.getContext(), R.anim.anim_item_clik));
                    listener.onImageSelected(item);
                });
            } else
                this.itemView.setOnClickListener(view -> {
                    view.startAnimation(AnimationUtils.loadAnimation(view.getContext(), R.anim.anim_item_clik));
                    cbThumbnail.setChecked(!cbThumbnail.isChecked());
                });
        }

        private void onCheckedChange(MediaImage item, ImageSelectListener listener, boolean isChecked) {
            item.setChecked(isChecked);
            if (isChecked) {
                totalSelected++;
                viewOverlay.setVisibility(View.VISIBLE);
            } else {
                totalSelected--;
                viewOverlay.setVisibility(View.GONE);
            }

            listener.onImageCheckedChange(isChecked);
        }
    }
}
