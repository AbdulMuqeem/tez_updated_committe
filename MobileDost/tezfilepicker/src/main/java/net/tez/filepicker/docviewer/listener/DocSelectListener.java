package net.tez.filepicker.docviewer.listener;

import net.tez.filepicker.docviewer.model.MediaDocument;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;

/**
 * Created by VINOD KUMAR on 3/28/2019.
 */
public interface DocSelectListener extends BaseRecyclerViewListener {

    void OnDocSelected(MediaDocument document);
}
