package net.tez.calendar;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.StyleRes;


/**
 * Created by FARHAN DHANANI on 1/14/2019.
 */
public interface CalendarFactory {
    static AbstractAndroidDatePickerDialog getAndroidDatePicker(@NonNull Context context, @NonNull AbstractAndroidDatePickerDialog.OnDateSetListener onDateSetListener) {
        return new AndroidDatePickerDialog(context, onDateSetListener);
    }

    static AbstractAndroidDatePickerDialog getAndroidDatePicker(@NonNull Context context, @StyleRes int themeResId, @NonNull AbstractAndroidDatePickerDialog.OnDateSetListener onDateSetListener) {
        return new AndroidDatePickerDialog(context, themeResId, onDateSetListener);
    }
}
