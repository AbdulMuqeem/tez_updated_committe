package net.tez.camera.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import net.tez.camera.base.implementation.AbstractBaseFragment;
import net.tez.camera.base.implementation.AbstractCameraFragment;
import net.tez.camera.cameras.camera1api.views.Camera1FragmentImpl;
import net.tez.camera.cameras.camera2api.R;
import net.tez.camera.cameras.camera2api.views.Camera2FragmentImpl;
import net.tez.camera.interfaces.FragmentActivityCommunication;
import net.tez.camera.request.ClientRequest;
import net.tez.camera.router.FragmentRouter;
import net.tez.camera.util.CameraUtil;
import net.tez.camera.util.Constants;
import net.tez.camera.util.PermissionUtil;

public class CameraActivity extends AppCompatActivity implements FragmentActivityCommunication {

    private static final int REQUEST_CAMERA_PERMISSIONS = 1;
    private static final int REQUEST_CAMERA_PERMISSIONS_ON_SECURITY_EXCEPTION = 2;
    private AbstractBaseFragment currentFragment;
    private ClientRequest clientRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        if (CameraUtil.checkCameraHardware(this)) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            clientRequest = (ClientRequest) getIntent().getSerializableExtra(Constants.CLIENT_REQUEST);
            this.askPermissions();
        } else {
            sendFailureResultToClient();
            finish();
        }
    }

    @Override
    public void setCurrentFragment(@NonNull AbstractBaseFragment currentFragment) {
        this.currentFragment = currentFragment;
    }

    @Override
    public ClientRequest getClientRequest() {
        return clientRequest;
    }

    @Override
    public void requestPermissionsOnSecurityException() {
        ActivityCompat.requestPermissions(this,
                PermissionUtil.requiredPermissions,
                REQUEST_CAMERA_PERMISSIONS_ON_SECURITY_EXCEPTION);
    }

    @Override
    public void replaceCamera2WithCamera1() {
        FragmentRouter.replace(getSupportFragmentManager(), R.id.fragmentContainer, new Camera1FragmentImpl());
    }

    @Override
    public void onBackPressed() {
        if (currentFragment == null || currentFragment.onBackPressed())
            super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case REQUEST_CAMERA_PERMISSIONS:
                if (checkGrantResults(grantResults))
                    execute();
                else {
                    showToast(R.string.provide_permission_message);
                    finish();
                }
                break;


            case REQUEST_CAMERA_PERMISSIONS_ON_SECURITY_EXCEPTION:
                if (checkGrantResults(grantResults))
                    this.sendPermissionSuccessResult();
                else {
                    showToast(R.string.provide_permission_message);
                    finish();
                }
                break;
        }
    }

    private boolean checkGrantResults(@NonNull int[] grantResults) {
        return grantResults.length > 2
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED
                && grantResults[2] == PackageManager.PERMISSION_GRANTED;
    }

    private void sendFailureResultToClient() {
        Intent intent = new Intent();
        intent.putExtra(Constants.IS_ERROR, true);
        intent.putExtra(Constants.ERROR_MESSAGE, Constants.NO_CAMERA_EXISTS);
        if (clientRequest.isFromCallback()) {
            intent.setAction(Constants.BROADCAST_RECEIVER_CAPTURE_RESULTS);
            sendBroadcast(intent);
        } else
            setResult(Activity.RESULT_CANCELED, intent);
    }

    private void showToast(@StringRes int resId) {
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show();
    }

    private void sendPermissionSuccessResult() {
        this.replaceWithCamera2();
    }

    private void replaceWithCamera2() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            FragmentRouter.replace(getSupportFragmentManager(), R.id.fragmentContainer, new Camera2FragmentImpl());
        }
    }

    private void askPermissions() {
        if (PermissionUtil.checkRequiredPermissions(this))
            this.execute();
        else
            ActivityCompat.requestPermissions(this, PermissionUtil.requiredPermissions, REQUEST_CAMERA_PERMISSIONS);
    }

    private void execute() {
        AbstractCameraFragment cameraFragment;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
            cameraFragment = new Camera2FragmentImpl();
        else
            cameraFragment = new Camera1FragmentImpl();

        FragmentRouter.add(getSupportFragmentManager(), R.id.fragmentContainer, cameraFragment);
    }
}
