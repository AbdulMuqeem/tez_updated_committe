package net.tez.camera.interfaces;

import androidx.annotation.NonNull;

import net.tez.camera.exceptions.TitanCameraException;

import java.util.ArrayList;

/**
 * Created by VINOD KUMAR on 12/26/2018.
 */
public interface CaptureResult {

    void onCapture(@NonNull ArrayList<String> files);

    default void onFailure(@NonNull TitanCameraException t) {

    }
}
