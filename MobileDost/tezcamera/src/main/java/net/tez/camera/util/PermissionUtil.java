package net.tez.camera.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

/**
 * Created by VINOD KUMAR on 5/6/2019.
 */
public final class PermissionUtil {

    public static final String[] requiredPermissions = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    private PermissionUtil() {

    }

    public static boolean checkRequiredPermissions(@NonNull Context context) {
        return ContextCompat.checkSelfPermission(context, requiredPermissions[0])
                == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context, requiredPermissions[1])
                == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context, requiredPermissions[2])
                == PackageManager.PERMISSION_GRANTED;
    }
}
