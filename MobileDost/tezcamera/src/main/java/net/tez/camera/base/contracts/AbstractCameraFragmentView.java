package net.tez.camera.base.contracts;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;

/**
 * Created by VINOD KUMAR on 1/8/2019.
 */
public interface AbstractCameraFragmentView extends AbstractBaseFragmentView {

    void displayImage(@NonNull Bitmap bitmap);

    void setButtonCaptureVisibility(int visibility);

    void setFrameLayoutSwitchCameraVisibility(int visibility);

    void saveImageAndFinish(@NonNull Bitmap bitmap);
}
