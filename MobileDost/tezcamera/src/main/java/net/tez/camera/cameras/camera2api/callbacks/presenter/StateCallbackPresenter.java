package net.tez.camera.cameras.camera2api.callbacks.presenter;

import android.hardware.camera2.CameraDevice;
import androidx.annotation.NonNull;

/**
 * Created by VINOD KUMAR on 1/8/2019.
 */
public interface StateCallbackPresenter {

    void showPreview(@NonNull CameraDevice cameraDevice);

    void finish();
}
