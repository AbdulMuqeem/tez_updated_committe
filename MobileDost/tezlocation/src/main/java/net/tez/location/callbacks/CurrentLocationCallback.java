package net.tez.location.callbacks;

/**
 * Created by Vinod Kumar on 10/20/2018.
 */

public interface CurrentLocationCallback extends TezLocationCallback {
    default void onPermissionDenied(){}
    default void onInitialized() {}
}
