package net.tez.location.core;

import android.Manifest;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import net.tez.location.callbacks.CurrentLocationCallback;
import net.tez.location.callbacks.InitializationCallback;
import net.tez.location.callbacks.TezLocationCallback;
import net.tez.location.defaults.LocationPermissionResultCallback;
import net.tez.location.services.ITezLocationRequest;
import net.tez.location.services.TezLocationInner;
import net.tez.location.services.TezLocationRequest;
import net.tez.permission.runtime.core.RuntimePermission;

/**
 * Created by Vinod Kumar on 10/20/2018.
 */

@SuppressWarnings({"WeakerAccess", "MissingPermission", "unused"})
public class TezLocation {

    /*--------------------------------------------*/
    // REQUEST CURRENT LOCATION
    /*--------------------------------------------*/

    public static <T extends FragmentActivity> void requestCurrentLocation(@NonNull T caller, CurrentLocationCallback callback) {
        requestCurrentLocation(caller, getDefaultLocationRequest(), callback);
    }

    public static <T extends FragmentActivity> void requestCurrentLocation(@NonNull T caller, @NonNull TezLocationRequest locationRequest, @NonNull CurrentLocationCallback callback) {
        RuntimePermission.requestPermission(caller, Manifest.permission.ACCESS_FINE_LOCATION, new LocationPermissionResultCallback(locationRequest, callback));
    }

    public static void requestCurrentLocation(@NonNull Context context, @NonNull TezLocationCallback callback) {
        requestCurrentLocation(context, getDefaultLocationRequest(), callback);
    }

    public static void requestCurrentLocation(@NonNull Context context, @NonNull TezLocationRequest locationRequest, @NonNull TezLocationCallback callback) {
        TezLocationInner.requestLocation(context, locationRequest, callback);
    }

    /*--------------------------------------------*/
    // REQUEST LOCATION UPDATES
    /*--------------------------------------------*/

    public static <T extends FragmentActivity> void init(@NonNull T caller, @NonNull InitializationCallback callback) {
        RuntimePermission.requestPermission(caller, Manifest.permission.ACCESS_FINE_LOCATION, new LocationPermissionResultCallback(getDefaultLocationRequest(), callback));
    }

    /*--------------------------------------------*/
    // HELPERS
    /*--------------------------------------------*/

    public static TezLocationRequest getDefaultLocationRequest() {
        return TezLocationRequest.create()
                .setInterval(10000)
                .setFastestInterval(5000)
                .setPriority(ITezLocationRequest.Priority.HIGH_ACCURACY);
    }
}