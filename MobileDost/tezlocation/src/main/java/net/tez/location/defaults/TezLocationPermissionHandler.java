package net.tez.location.defaults;

import android.app.AlertDialog;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import net.tez.location.handlers.LocationPermissionHandler;
import net.tez.permission.runtime.callbacks.SettingOpener;
import net.tez.permission.runtime.models.Perm;

/**
 * Created by Vinod Kumar on 10/21/2018.
 */

@SuppressWarnings({"WeakerAccess", "unused", "SameReturnValue"})
public class TezLocationPermissionHandler implements LocationPermissionHandler<FragmentActivity> {

    @Override
    public void handlePermanentDenied(@NonNull Perm<FragmentActivity> perm, @NonNull SettingOpener<FragmentActivity> settingOpener) {

        AlertDialog.Builder builder = getDialogBuilder(perm);
        builder.setTitle(getTitle(perm));
        builder.setMessage(getMessage(perm));
        builder.setPositiveButton(getPositiveButtonText(perm), (dialogInterface, which) -> settingOpener.open(perm.getCaller(), perm.getType()));
        builder.setNegativeButton(getNegativeButtonText(perm), (dialogInterface, which) -> settingOpener.doNothing(perm.getCaller(), perm.getType()));
        builder.create().show();
    }

    @NonNull
    protected AlertDialog.Builder getDialogBuilder(@NonNull Perm<FragmentActivity> perm) {
        return new AlertDialog.Builder(perm.getCaller());
    }

    @NonNull
    protected String getTitle(@NonNull Perm<FragmentActivity> perm) {
        return "Permission Missing";
    }

    @NonNull
    protected String getMessage(@NonNull Perm<FragmentActivity> perm) {
        return "Require " + perm.getType() + " permission to get updated location.";
    }

    @NonNull
    protected String getPositiveButtonText(@NonNull Perm<FragmentActivity> perm) {
        return "OPEN SETTINGS";
    }

    @NonNull
    protected String getNegativeButtonText(@NonNull Perm<FragmentActivity> perm) {
        return "NOT NOW";
    }
}
