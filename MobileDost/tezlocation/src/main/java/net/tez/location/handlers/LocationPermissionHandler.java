package net.tez.location.handlers;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import net.tez.permission.runtime.callbacks.SettingOpener;
import net.tez.permission.runtime.models.Perm;

/**
 * Created by Vinod Kumar on 10/13/2018.
 */
public interface LocationPermissionHandler<T extends FragmentActivity> {

    void handlePermanentDenied(@NonNull Perm<T> perm, @NonNull SettingOpener<T> settingOpener);
}
