package net.tez.location.shadow;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import net.tez.location.callbacks.CurrentLocationCallback;
import net.tez.location.callbacks.InitializationCallback;
import net.tez.location.services.TezLocationInner;
import net.tez.location.services.TezLocationRequest;
import net.tez.location.services.exception.TezResolvableApiException;

import java.util.concurrent.TimeoutException;

/**
 * Created by Vinod Kumar on 10/13/2018.
 */

@SuppressLint("MissingPermission")
public class LocationSettingFragment extends Fragment implements CurrentLocationCallback {

    private static final int REQUEST_CHECK_SETTINGS = 2016;
    private static LocationSettingFragment INSTANCE;
    private TezResolvableApiException e;
    private TezLocationRequest locationRequest;
    private CurrentLocationCallback currentLocationCallback;
    private InitializationCallback initializationCallback;
    private final CountDownTimer timer = new CountDownTimer(10 * 1000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            removeFragment();
            sendFailureCallback(new TimeoutException("Get location time out"));
        }
    };

    public static LocationSettingFragment newInstance(@NonNull TezLocationRequest locationRequest, @NonNull CurrentLocationCallback currentLocationCallback) {
        LocationSettingFragment.INSTANCE = new LocationSettingFragment();
        LocationSettingFragment fragment = LocationSettingFragment.INSTANCE;
        fragment.locationRequest = locationRequest;
        fragment.currentLocationCallback = currentLocationCallback;
        return fragment;
    }

    public static LocationSettingFragment newInstance(@NonNull TezLocationRequest locationRequest, @NonNull InitializationCallback initializationCallback) {
        LocationSettingFragment.INSTANCE = new LocationSettingFragment();
        LocationSettingFragment fragment = LocationSettingFragment.INSTANCE;
        fragment.locationRequest = locationRequest;
        fragment.initializationCallback = initializationCallback;
        return fragment;
    }

    public static LocationSettingFragment getInstance() {
        return LocationSettingFragment.INSTANCE;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        requestLocation();
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            try {
                Activity activity = getActivity();
                if (activity != null) {
                    TezLocationInner.getLocation(activity, locationRequest, this);
                }
            } catch (Exception e) {
                sendFailureCallback(e);
            }
        } else {
            String error = data != null
                    ? data.getStringExtra("error") != null
                    ? data.getStringExtra("error")
                    : "failed to get location"
                    : "failed to get location";

            if (currentLocationCallback != null) {
                currentLocationCallback.onFailed(error);
            } else if (initializationCallback != null) {
                initializationCallback.onFailed(error);
            }
            removeFragment();
        }
    }

    private void requestLocation() {
        try {
            Activity activity = getActivity();
            if (activity != null) {
                TezLocationInner.requestLocationWithActivity(activity, locationRequest, this);
            }
        } catch (Exception e) {
            sendFailureCallback(e);
        }
    }

    TezResolvableApiException getResolvableApiException() {
        return e;
    }

    private void sendFailureCallback(Exception e) {
        if (currentLocationCallback != null) {
            currentLocationCallback.onFailed(e.getMessage());
        } else if (initializationCallback != null) {
            initializationCallback.onFailed(e.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    private void removeFragment() {
        try {
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager != null) {
                fragmentManager.beginTransaction().remove(this).commitAllowingStateLoss();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onResult(Location location) {
        timer.cancel();
        removeFragment();
        if (location != null)
            currentLocationCallback.onResult(location);
        else
            currentLocationCallback.onFailed("Cannot fetch location");
    }

    @Override
    public void onFailed(Exception e) {
        try {
            if (e instanceof TezResolvableApiException && getActivity() != null) {
                LocationSettingFragment.this.e = (TezResolvableApiException) e;
                Intent intent = new Intent(getActivity(), ShadowLocationActivity.class);
                startActivityForResult(intent, REQUEST_CHECK_SETTINGS);
            } else {
                removeFragment();
                sendFailureCallback(e);
            }
        } catch (Exception e1) {
            removeFragment();
            sendFailureCallback(e1);
        }
    }

    @Override
    public void onInitialized() {
        if (currentLocationCallback != null) {
            currentLocationCallback.onInitialized();
            timer.start();

        } else if (initializationCallback != null) {
            initializationCallback.onSuccess();
            removeFragment();
        }
    }
}