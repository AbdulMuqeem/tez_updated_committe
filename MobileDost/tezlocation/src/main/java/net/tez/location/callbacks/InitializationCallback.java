package net.tez.location.callbacks;

/**
 * Created by Vinod Kumar on 10/21/2018.
 */
public interface InitializationCallback {
    void onSuccess();
    default void onFailed(String error){}
    default void onPermissionDenied(){}
}
