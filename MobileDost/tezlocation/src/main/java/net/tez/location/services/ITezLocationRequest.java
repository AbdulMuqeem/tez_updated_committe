package net.tez.location.services;

import androidx.annotation.NonNull;

public interface ITezLocationRequest {

    TezLocationRequest setPriority(@NonNull Priority priority);

    TezLocationRequest setInterval(int interval);

    TezLocationRequest setNumUpdates(int num);

    TezLocationRequest setFastestInterval(int interval);

    enum Priority {
        BALANCE_POWER,
        HIGH_ACCURACY,
        LOW_POWER,
        NO_POWER,

    }
}
