package net.tez.location.config;


import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import net.tez.location.defaults.TezLocationPermissionHandler;
import net.tez.location.handlers.LocationPermissionHandler;

/**
 * Created by Vinod Kumar on 10/21/2018.
 */

@SuppressWarnings({"WeakerAccess", "unused"})
public class TezLocationConfig {

    private static TezLocationConfig INSTANCE;

    @NonNull
    private LocationPermissionHandler<FragmentActivity> locationPermissionHandler;

    public static TezLocationConfig getInstance() {
        return INSTANCE == null ? INSTANCE = new TezLocationConfig() : INSTANCE;
    }

    public TezLocationConfig() {
        locationPermissionHandler = new TezLocationPermissionHandler();
    }

    public void setLocationPermissionHandler(@NonNull LocationPermissionHandler<FragmentActivity> locationPermissionHandler) {
        this.locationPermissionHandler = locationPermissionHandler;
    }

    @NonNull
    public LocationPermissionHandler<FragmentActivity> getLocationPermissionHandler() {
        return locationPermissionHandler;
    }
}