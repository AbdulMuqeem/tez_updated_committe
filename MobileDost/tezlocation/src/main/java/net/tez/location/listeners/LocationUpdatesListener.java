package net.tez.location.listeners;

import android.location.Location;
import androidx.annotation.NonNull;

/**
 * Created by Vinod Kumar on 10/21/2018.
 */

@FunctionalInterface
public interface LocationUpdatesListener {
    void onResult(@NonNull Location location);
}
