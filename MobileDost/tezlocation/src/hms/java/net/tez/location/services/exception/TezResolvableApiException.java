package net.tez.location.services.exception;


import com.huawei.hms.common.ResolvableApiException;

public class TezResolvableApiException extends RuntimeException {

    private final ResolvableApiException resolvableApiException;

    public TezResolvableApiException(ResolvableApiException e) {
        this.resolvableApiException = e;
    }

    public ResolvableApiException getResolvableApiException() {
        return resolvableApiException;
    }
}
