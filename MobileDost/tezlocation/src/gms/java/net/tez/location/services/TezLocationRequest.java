package net.tez.location.services;

import androidx.annotation.NonNull;

import com.google.android.gms.location.LocationRequest;

public final class TezLocationRequest implements ITezLocationRequest {

    private LocationRequest locationRequest;

    private TezLocationRequest() {

    }

    public static TezLocationRequest create() {
        TezLocationRequest instance = new TezLocationRequest();
        instance.locationRequest = LocationRequest.create();
        return instance;
    }

    @Override
    public TezLocationRequest setPriority(@NonNull Priority priority) {
        switch (priority) {
            case BALANCE_POWER:
                locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
                break;

            case NO_POWER:
                locationRequest.setPriority(LocationRequest.PRIORITY_NO_POWER);
                break;

            case LOW_POWER:
                locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
                break;

            case HIGH_ACCURACY:
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                break;
        }
        return this;
    }

    @Override
    public TezLocationRequest setInterval(int interval) {
        locationRequest.setInterval(interval);
        return this;
    }

    @Override
    public TezLocationRequest setNumUpdates(int num) {
        locationRequest.setNumUpdates(num);
        return this;
    }

    @Override
    public TezLocationRequest setFastestInterval(int interval) {
        locationRequest.setFastestInterval(interval);
        return this;
    }

    public LocationRequest getLocationRequest() {
        return locationRequest;
    }
}
