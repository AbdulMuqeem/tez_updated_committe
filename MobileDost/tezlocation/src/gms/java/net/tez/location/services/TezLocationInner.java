package net.tez.location.services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;

import net.tez.location.callbacks.CurrentLocationCallback;
import net.tez.location.callbacks.TezLocationCallback;
import net.tez.location.services.exception.TezResolvableApiException;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;
import static com.google.android.gms.location.LocationServices.getSettingsClient;

public final class TezLocationInner {

    @SuppressLint("MissingPermission")
    public static void requestLocation(@NonNull Context context,
                                       @NonNull TezLocationRequest request,
                                       @NonNull TezLocationCallback callback) {

        if (ActivityCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            callback.onFailed("Location permission not found");
            return;
        }

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(request.getLocationRequest());
        SettingsClient client = getSettingsClient(context.getApplicationContext());
        client.checkLocationSettings(builder.build())
                .addOnSuccessListener(locationSettingsResponse -> {
                    FusedLocationProviderClient locationService = getFusedLocationProviderClient(context.getApplicationContext());
                    locationService.requestLocationUpdates(request.getLocationRequest(), new LocationCallback() {
                        @Override
                        public void onLocationResult(LocationResult locationResult) {
                            if (locationResult.getLastLocation() != null) {
                                locationService.removeLocationUpdates(this);
                                callback.onResult(locationResult.getLastLocation());
                            }
                        }

                        @Override
                        public void onLocationAvailability(LocationAvailability locationAvailability) {
                            if (!locationAvailability.isLocationAvailable()) {
                                locationService.removeLocationUpdates(this);
                                callback.onFailed("Location is unavailable");
                            }
                        }
                    }, null);
                })
                .addOnFailureListener(e -> callback.onFailed(e.getMessage()));
    }

    public static void requestLocationWithActivity(@NonNull Activity activity,
                                                   @NonNull TezLocationRequest request,
                                                   @NonNull CurrentLocationCallback callback) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(request.getLocationRequest());
        SettingsClient client = getSettingsClient(activity);
        client.checkLocationSettings(builder.build())
                .addOnSuccessListener(locationSettingsResponse -> getLocation(activity, request, callback))
                .addOnFailureListener(e -> {
                    if (e instanceof ResolvableApiException)
                        callback.onFailed(new TezResolvableApiException((ResolvableApiException) e));
                    else
                        callback.onFailed(e);
                });
    }

    @SuppressLint("MissingPermission")
    public static void getLocation(@NonNull Activity activity,
                                   @NonNull TezLocationRequest request,
                                   @NonNull CurrentLocationCallback callback) {
        callback.onInitialized();
        FusedLocationProviderClient locationService = getFusedLocationProviderClient(activity);
        locationService.requestLocationUpdates(request.getLocationRequest(), new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                locationService.removeLocationUpdates(this);
                callback.onResult(locationResult.getLastLocation());
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                if (!locationAvailability.isLocationAvailable()) {
                    locationService.removeLocationUpdates(this);
                    callback.onFailed(new Exception("Location is unavailable"));
                }
            }
        }, null);
    }

    public static void resolveApiException(@NonNull Activity activity,
                                           @NonNull TezResolvableApiException e,
                                           int requestCode) throws IntentSender.SendIntentException {
        e.getResolvableApiException().startResolutionForResult(activity, requestCode);
    }
}
