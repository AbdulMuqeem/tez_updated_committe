package com.tez.androidapp.services.models;

import android.net.Uri;

import androidx.annotation.Nullable;

import com.huawei.agconnect.applinking.ResolvedLinkData;

public class TezDynamicLinkData implements ITezDynamicLinkData {

    private final ResolvedLinkData data;

    public TezDynamicLinkData(ResolvedLinkData data) {
        this.data = data;
    }

    @Nullable
    @Override
    public Uri getLink() {
        return data.getDeepLink();
    }
}
