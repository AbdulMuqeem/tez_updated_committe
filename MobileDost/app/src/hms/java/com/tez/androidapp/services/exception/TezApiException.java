package com.tez.androidapp.services.exception;

import com.huawei.hms.common.ApiException;

public class TezApiException extends RuntimeException {

    private final ApiException exception;

    public TezApiException(ApiException exception) {
        this.exception = exception;
    }

    public int getStatusCode() {
        return exception.getStatusCode();
    }
}
