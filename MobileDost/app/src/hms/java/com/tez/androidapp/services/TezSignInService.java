package com.tez.androidapp.services;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.huawei.hms.support.hwid.HuaweiIdAuthManager;
import com.huawei.hms.support.hwid.request.HuaweiIdAuthParams;
import com.huawei.hms.support.hwid.request.HuaweiIdAuthParamsHelper;
import com.huawei.hms.support.hwid.result.AuthHuaweiId;
import com.huawei.hms.support.hwid.service.HuaweiIdAuthService;
import com.tez.androidapp.services.models.TezSignInAccount;
import com.tez.androidapp.services.models.TezTask;

public class TezSignInService implements ITezSignInService {

    private final HuaweiIdAuthService huaweiIdAuthService;

    private TezSignInService(HuaweiIdAuthService googleSignInClient) {
        this.huaweiIdAuthService = googleSignInClient;
    }

    public static TezSignInService init(@NonNull Activity activity) {
        HuaweiIdAuthParams gso = new HuaweiIdAuthParamsHelper(HuaweiIdAuthParams.DEFAULT_AUTH_REQUEST_PARAM)
                .setId()
                .createParams();
        return new TezSignInService(HuaweiIdAuthManager.getService(activity, gso));
    }

    public static void requestPhoneNumber(@NonNull Activity activity, int requestCode) {
        //No suitable API in HMS found
    }

    @Nullable
    public static String extractPhoneNumberFromIntent(@NonNull Intent data) {
        //No suitable API in HMS found
        return null;
    }

    public static TezTask<AuthHuaweiId, TezSignInAccount> getSignedInAccountFromIntent(Intent data) {
        return new TezTask<>(
                HuaweiIdAuthManager.parseAuthResultFromIntent(data),
                AuthHuaweiId.class,
                TezSignInAccount.class
        );
    }

    @NonNull
    @Override
    public Intent getSignInIntent() {
        return huaweiIdAuthService.getSignInIntent();
    }
}
