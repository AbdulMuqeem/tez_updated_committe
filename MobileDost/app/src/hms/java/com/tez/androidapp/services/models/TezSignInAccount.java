package com.tez.androidapp.services.models;

import androidx.annotation.NonNull;

import com.huawei.hms.support.hwid.result.AuthHuaweiId;


public class TezSignInAccount implements ITezSignInAccount {

    @NonNull
    private final AuthHuaweiId account;

    public TezSignInAccount(@NonNull AuthHuaweiId account) {
        this.account = account;
    }

    @Override
    public String getId() {
        return account.getOpenId();
    }
}
