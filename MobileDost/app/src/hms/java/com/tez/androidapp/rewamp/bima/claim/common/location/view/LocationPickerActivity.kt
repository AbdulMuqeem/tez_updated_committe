package com.tez.androidapp.rewamp.bima.claim.common.location.view

import android.location.Location
import android.os.Bundle
import com.huawei.hms.maps.CameraUpdateFactory
import com.huawei.hms.maps.HuaweiMap
import com.huawei.hms.maps.OnMapReadyCallback
import com.huawei.hms.maps.SupportMapFragment
import com.huawei.hms.maps.model.*
import com.tez.androidapp.R
import com.tez.androidapp.app.base.ui.activities.BaseActivity
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback
import com.tez.androidapp.commons.receivers.LocationBroadCastReceiver
import com.tez.androidapp.commons.utils.app.Utility
import com.tez.androidapp.rewamp.bima.claim.common.location.presenter.ILocationPickerActivityPresenter
import com.tez.androidapp.rewamp.bima.claim.common.location.presenter.LocationPickerActivityPresenter
import com.tez.androidapp.rewamp.bima.claim.common.location.router.LocationPickerActivityRouter.Companion.createResultIntent
import kotlinx.android.synthetic.hms.activity_location_picker.*

class LocationPickerActivity : BaseActivity(),
        ILocationPickerActivityView,
        OnMapReadyCallback,
        LocationAvailableCallback,
        HuaweiMap.OnCameraMoveListener,
        HuaweiMap.OnCameraIdleListener {

    private val locationStatusReceiver = LocationBroadCastReceiver(this::getUserLocation)
    private lateinit var huaweiMap: HuaweiMap
    private var marker: Marker? = null
    private val iLocationPickerActivityPresenter: ILocationPickerActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_picker)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onResume() {
        super.onResume()
        locationStatusReceiver.register(this)
    }

    override fun onPause() {
        super.onPause()
        locationStatusReceiver.unregister(this)
    }

    override fun onMapReady(map: HuaweiMap) {
        this.huaweiMap = map
        getUserLocation()
        map.setPadding(Utility.dpToPx(18f), 0, 0, Utility.dpToPx(100f))
    }

    override fun setLocationDetails(name: String, address: String) {
        tvLocationName.text = name
        tvLocationAddress.text = address
    }

    override fun onLocationAvailable(location: Location) {
        val latLng = LatLng(location.latitude, location.longitude)
        initMarker(latLng)
        moveCamera(latLng)
        ivMyLocation.setDoubleTapSafeOnClickListener { animateCamera(latLng) }
    }

    override fun enableBtDone() {
        btDone.setButtonNormal()
        btDone.setDoubleTapSafeOnClickListener { setFinalAddress() }
    }

    override fun initMarker(latLng: LatLng) {
        if (marker == null) {
            marker = huaweiMap.addMarker(MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_locator))
                    .draggable(false)
                    .visible(true))
            huaweiMap.setOnCameraMoveListener(this)
            huaweiMap.setOnCameraIdleListener(this)
        }
    }

    private fun animateCamera(latLng: LatLng) {
        huaweiMap.animateCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(latLng, MAP_ZOOM)))
    }

    override fun moveCamera(latLng: LatLng) {
        huaweiMap.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(latLng, MAP_ZOOM)))
    }

    private fun getUserLocation() = getCurrentLocation(this)

    private fun setFinalAddress() =
            iLocationPickerActivityPresenter.setFinalAddress(tvLocationName.valueText, tvLocationAddress.valueText)

    override fun setResults(address: String) {
        setResult(RESULT_OK, createResultIntent(address, marker!!.position))
        finish()
    }

    override fun onCameraMove() {
        marker!!.position = huaweiMap.cameraPosition.target
    }

    override fun onCameraIdle() {
        val finalPosition = huaweiMap.cameraPosition.target
        marker!!.position = finalPosition
        iLocationPickerActivityPresenter.setLocationDetails(finalPosition)
    }

    override fun getScreenName(): String = "LocationPickerActivity"

    companion object {
        private const val MAP_ZOOM = 16.0f
    }

    init {
        iLocationPickerActivityPresenter = LocationPickerActivityPresenter(this)
    }
}