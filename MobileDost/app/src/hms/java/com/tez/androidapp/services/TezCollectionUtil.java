package com.tez.androidapp.services;

import androidx.annotation.Nullable;

import com.huawei.hms.common.utils.CollectionUtil;

import java.util.Collection;

public class TezCollectionUtil {

    public static boolean isEmpty(@Nullable Collection<?> collection) {
        return CollectionUtil.isEmpty(collection);
    }

    public static boolean contains(int[] arr, int val) {
        for (int a : arr) {
            if (a == val)
                return true;
        }
        return false;
    }
}
