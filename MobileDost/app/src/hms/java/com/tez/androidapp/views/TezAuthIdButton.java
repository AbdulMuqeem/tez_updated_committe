package com.tez.androidapp.views;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.huawei.hms.support.hwid.ui.HuaweiIdAuthButton;

import net.tez.fragment.util.base.IBaseWidget;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

public class TezAuthIdButton extends HuaweiIdAuthButton implements IBaseWidget {

    public TezAuthIdButton(Context context) {
        this(context, null);
    }

    public TezAuthIdButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TezAuthIdButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setUIMode(HuaweiIdAuthButton.THEME_NO_TITLE,
                HuaweiIdAuthButton.COLOR_POLICY_WHITE,
                HuaweiIdAuthButton.CORNER_RADIUS_SMALL);
        //remove once account sign-in is fixed
        setVisibility(GONE);
    }

    @Override
    public void setDoubleTapSafeOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        super.setOnClickListener(doubleTapSafeOnClickListener);
    }

    @NonNull
    @Override
    public String getLabel() {
        return "Huawei";
    }
}
