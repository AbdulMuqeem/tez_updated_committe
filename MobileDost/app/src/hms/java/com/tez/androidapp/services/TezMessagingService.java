package com.tez.androidapp.services;

import androidx.annotation.NonNull;

import com.huawei.hms.push.HmsMessageService;
import com.huawei.hms.push.RemoteMessage;
import com.tez.androidapp.services.models.TezRemoteMessage;

public abstract class TezMessagingService extends HmsMessageService {

    @Override
    public final void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        onMessageReceived(new TezRemoteMessage(remoteMessage));
    }

    public abstract void onMessageReceived(@NonNull TezRemoteMessage tezRemoteMessage);

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
    }
}
