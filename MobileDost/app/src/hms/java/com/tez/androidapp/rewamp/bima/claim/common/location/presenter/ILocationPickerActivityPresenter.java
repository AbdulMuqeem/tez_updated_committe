package com.tez.androidapp.rewamp.bima.claim.common.location.presenter;

import androidx.annotation.NonNull;

import com.huawei.hms.maps.model.LatLng;


public interface ILocationPickerActivityPresenter {

    void setLocationDetails(@NonNull LatLng latLng);

    void setFinalAddress(String name, String address);
}
