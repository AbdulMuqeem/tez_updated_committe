package com.tez.androidapp.rewamp.bima.claim.common.location.view

import com.tez.androidapp.app.base.ui.IBaseView
import com.huawei.hms.maps.model.LatLng

interface ILocationPickerActivityView : IBaseView {
    fun setLocationDetails(name: String, address: String)
    fun enableBtDone()
    fun initMarker(latLng: LatLng)
    fun moveCamera(latLng: LatLng)
    fun setResults(address: String)
}