package com.tez.androidapp.services;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.huawei.hms.analytics.HiAnalytics;
import com.huawei.hms.analytics.HiAnalyticsInstance;
import com.huawei.hms.analytics.HiAnalyticsTools;
import com.huawei.hms.analytics.type.HAEventType;
import com.huawei.hms.analytics.type.HAParamType;
import com.tez.androidapp.app.MobileDostApplication;

public class TezAnalytics implements ITezAnalytics {

    @NonNull
    private final HiAnalyticsInstance hiAnalyticsInstance;

    private TezAnalytics() {
        hiAnalyticsInstance = HiAnalytics.getInstance(MobileDostApplication.getAppContext());
    }

    public static TezAnalytics getInstance() {
        return LazyHolder.INSTANCE;
    }

    public static void initialize() {
        HiAnalyticsTools.enableLog();
    }

    @Override
    public void setAnalyticsCollectionEnabled(boolean enabled) {
        hiAnalyticsInstance.setAnalyticsEnabled(enabled);
    }

    @Override
    public void logEvent(@NonNull String event, @Nullable Bundle params) {
        hiAnalyticsInstance.onEvent(event, params);
    }

    @Override
    public void trackEvent(String category, String action, String label) {
        //No suitable API found in HMS
    }

    @Override
    public void setUserId(@NonNull String userId) {
        hiAnalyticsInstance.setUserId(userId);
    }

    private static final class LazyHolder {
        private static final TezAnalytics INSTANCE = new TezAnalytics();
    }

    public static final class Param {
        public static final String ITEM_CATEGORY = HAParamType.CATEGORY;
        public static final String ITEM_NAME = HAParamType.PRODUCTNAME;
    }

    public static final class Event {
        public static final String VIEW_ITEM = HAEventType.VIEWPRODUCT;
    }
}
