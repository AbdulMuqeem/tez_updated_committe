package com.tez.androidapp.services.util;

import android.hardware.Camera;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.huawei.hms.ml.scan.HmsScan;
import com.huawei.hms.ml.scan.HmsScanAnalyzer;
import com.huawei.hms.ml.scan.HmsScanAnalyzerOptions;
import com.huawei.hms.mlsdk.MLAnalyzerFactory;
import com.huawei.hms.mlsdk.common.MLFrame;
import com.huawei.hms.mlsdk.face.MLFace;
import com.huawei.hms.mlsdk.face.MLFaceAnalyzerSetting;
import com.huawei.hms.mlsdk.face.MLFaceAnalyzerSetting.Factory;
import com.huawei.hms.mlsdk.face.MLFaceKeyPoint;
import com.huawei.hms.mlsdk.text.MLLocalTextSetting;
import com.huawei.hms.mlsdk.text.MLText;
import com.huawei.hms.mlsdk.text.MLText.TextLine;
import com.huawei.hms.mlsdk.text.MLText.Word;
import com.tez.androidapp.commons.utils.cnic.detection.view.CnicScannerActivity;
import com.tez.androidapp.commons.utils.cnic.firebase.callbacks.ImageFaceRetrieveCallback;
import com.tez.androidapp.commons.utils.cnic.firebase.callbacks.ImageTextRetrieveCallback;

import java.util.ArrayList;
import java.util.List;


public class FirebaseVisionUtil {

    private static FirebaseVisionUtil firebaseVisionUtil;
    private ImageFaceRetrieveCallback imageFaceRetrieveCallback;
    @Nullable
    private ImageTextRetrieveCallback imageTextRetrieveCallback;

    private FirebaseVisionUtil() {
    }

    public static FirebaseVisionUtil getInstance() {
        if (firebaseVisionUtil == null) {
            firebaseVisionUtil = new FirebaseVisionUtil();
        }
        return firebaseVisionUtil;
    }


    public void detectTextFromByteBuffer(ImageTextRetrieveCallback imageTextRetrieveCallback,
                                         byte[] bytes,
                                         Camera camera,
                                         String detectObject) {

        this.imageTextRetrieveCallback = imageTextRetrieveCallback;

        if (detectObject.equals(CnicScannerActivity.CNIC_BACK))
            this.detectBarCodeFromByteBuffer(bytes, camera);

        MLFrame.Property metadata = new MLFrame.Property.Creator()
                .setWidth(camera.getParameters().getPreviewSize().width)
                .setHeight(camera.getParameters().getPreviewSize().height)
                .setFormatType(camera.getParameters().getPreviewFormat())
                .create();

        MLLocalTextSetting setting = new MLLocalTextSetting.Factory()
                .setOCRMode(MLLocalTextSetting.OCR_DETECT_MODE)
                .setLanguage("en")
                .create();

        MLFrame image = MLFrame.fromByteArray(bytes, metadata);
        MLAnalyzerFactory.getInstance().getLocalTextAnalyzer(setting)
                .asyncAnalyseFrame(image)
                .addOnSuccessListener(this::extractTextFromImage)
                .addOnFailureListener(this::onTextRetrievalFailure);
    }

    private void detectBarCodeFromByteBuffer(byte[] bytes, Camera camera) {

        MLFrame.Property.Creator builder = new MLFrame.Property.Creator()
                .setWidth(camera.getParameters().getPreviewSize().width)
                .setHeight(camera.getParameters().getPreviewSize().height)
                .setFormatType(camera.getParameters().getPreviewFormat());

        MLFrame image = MLFrame.fromByteArray(bytes, builder.create());

        HmsScanAnalyzerOptions options = new HmsScanAnalyzerOptions.Creator()
                .setHmsScanTypes(HmsScan.QRCODE_SCAN_TYPE)
                .create();

        new HmsScanAnalyzer.Creator()
                .setHmsScanTypes(options)
                .create()
                .analyzInAsyn(image)
                .addOnSuccessListener(this::extractBarCodeData)
                .addOnFailureListener(this::onTextRetrievalFailure);
    }

    public void detectFaceFromByteBuffer(ImageFaceRetrieveCallback imageFaceRetrieveCallback,
                                         byte[] bytes,
                                         Camera camera,
                                         boolean isSensorReversed) {
        this.imageFaceRetrieveCallback = imageFaceRetrieveCallback;

        MLFrame.Property.Creator builder = new MLFrame.Property.Creator()
                .setWidth(camera.getParameters().getPreviewSize().width)
                .setHeight(camera.getParameters().getPreviewSize().height)
                .setFormatType(camera.getParameters().getPreviewFormat())
                .setQuadrant(isSensorReversed ?
                        MLFrame.Property.SCREEN_SECOND_QUADRANT : MLFrame.Property.SCREEN_FOURTH_QUADRANT);
        MLFrame.Property metadata = builder.create();
        MLFrame image = MLFrame.fromByteArray(bytes, metadata);

        MLAnalyzerFactory.getInstance()
                .getFaceAnalyzer(
                        new Factory()
                                .setPerformanceType(MLFaceAnalyzerSetting.TYPE_PRECISION)
                                .allowTracing()
                                .create()
                ).asyncAnalyseFrame(image)
                .addOnSuccessListener(this::extractFacesFromImage)
                .addOnFailureListener(imageFaceRetrieveCallback::onFaceRetrieveFailure);
    }


    private void extractBarCodeData(List<HmsScan> barcodes) {
        List<String> textList = new ArrayList<>();

        for (HmsScan barcode : barcodes) {
            String text = barcode.getOriginalValue();
            if (text != null && text.length() == 26)
                textList.add(text.substring(12, 25));
        }

        this.onTextRetrievalSuccess(textList);
    }

    private void extractTextFromImage(MLText firebaseVisionText) {

        ArrayList<String> blockList = new ArrayList<>();

        for (MLText.Block block : firebaseVisionText.getBlocks()) {
            for (TextLine line : block.getContents()) {
                for (Word element : line.getContents()) {
                    blockList.add(element.getStringValue());
                }
            }
        }

        this.onTextRetrievalSuccess(blockList);
    }


    private void extractFacesFromImage(List<MLFace> faces) {
        ArrayList<Integer> faceIdList = new ArrayList<>();
        for (MLFace face : faces) {
            MLFaceKeyPoint leftEar = face.getFaceKeyPoint(3);
            if (leftEar != null) {
                leftEar.getPoint();
            }
            if (face.possibilityOfSmiling() != -1.0f) {
                face.possibilityOfSmiling();
            }
            if (face.opennessOfRightEye() != -1.0f) {
                face.opennessOfRightEye();
            }
            if (-1 != face.getTracingIdentity()) {
                faceIdList.add(face.getTracingIdentity());
            }
        }
        this.imageFaceRetrieveCallback.onFaceRetrieveSuccess(faceIdList);
    }

    private void onTextRetrievalFailure(@NonNull Exception e) {
        if (imageTextRetrieveCallback != null) {
            imageTextRetrieveCallback.onTextRetrieveFailure(e);
            this.imageTextRetrieveCallback = null;
        }
    }

    private void onTextRetrievalSuccess(@NonNull List<String> list) {
        if (imageTextRetrieveCallback != null) {
            imageTextRetrieveCallback.onTextRetrieveSuccess(list);
            this.imageTextRetrieveCallback = null;
        }
    }
}
