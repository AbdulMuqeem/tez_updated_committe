package com.tez.androidapp.services.models;

import androidx.annotation.NonNull;

import com.huawei.hmf.tasks.Task;
import com.huawei.hms.common.ApiException;
import com.tez.androidapp.services.exception.TezApiException;
import com.tez.androidapp.services.listeners.TezOnFailureListener;
import com.tez.androidapp.services.listeners.TezOnSuccessListener;

public final class TezTask<I, O> implements ITezTask<I, O> {

    private final Task<I> tResultTask;
    private final Class<I> iClass;
    private final Class<O> oClass;

    public TezTask(Task<I> tResultTask, Class<I> iClass, Class<O> oClass) {
        this.tResultTask = tResultTask;
        this.iClass = iClass;
        this.oClass = oClass;
    }

    @NonNull
    @Override
    public TezTask<I, O> addOnSuccessListener(@NonNull TezOnSuccessListener<O> onSuccessListener) {
        tResultTask.addOnSuccessListener(data -> {
            try {
                O wrappedData = data == null ? null : oClass.getDeclaredConstructor(iClass).newInstance(data);
                onSuccessListener.onSuccess(wrappedData);
            } catch (Exception ignore) {
                throw new IllegalStateException("You must declare a constructor of service type in wrapper class");
            }
        });
        return this;
    }

    @NonNull
    @Override
    public TezTask<I, O> addOnFailureListener(TezOnFailureListener onFailureListener) {
        tResultTask.addOnFailureListener(onFailureListener);
        return this;
    }


    @Override
    public O getResultWithException() throws TezApiException {
        try {
            I result = tResultTask.getResultThrowException(ApiException.class);
            return result == null ? null : oClass.getDeclaredConstructor(iClass).newInstance(result);
        } catch (ApiException e) {
            throw new TezApiException(e);
        } catch (Exception e) {
            throw new IllegalStateException("You must declare a constructor of service type in wrapper class");
        }
    }
}
