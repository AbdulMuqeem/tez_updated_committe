package com.tez.androidapp.services.models;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.huawei.agconnect.applinking.ShortAppLinking;


public class TezShortDynamicLink implements ITezShortDynamicLink {

    @NonNull
    private final ShortAppLinking shortDynamicLink;

    public TezShortDynamicLink(@NonNull ShortAppLinking shortDynamicLink) {
        this.shortDynamicLink = shortDynamicLink;
    }

    @Nullable
    @Override
    public Uri getShortLink() {
        return shortDynamicLink.getShortUrl();
    }
}
