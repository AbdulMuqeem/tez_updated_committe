package com.tez.androidapp.services;

import androidx.annotation.NonNull;

import com.huawei.agconnect.crash.AGConnectCrash;

public class TezCrashlytics implements ITezCrashlytics {

    private TezCrashlytics() {

    }

    @NonNull
    public static TezCrashlytics getInstance() {
        return LazyHolder.INSTANCE;
    }

    @Override
    public void setCrashlyticsCollectionEnabled(boolean enabled) {
        AGConnectCrash.getInstance().enableCrashCollection(enabled);
    }

    @Override
    public void recordException(@NonNull Throwable throwable) {
        AGConnectCrash.getInstance().recordException(throwable);
    }

    @Override
    public void log(@NonNull String message) {
        AGConnectCrash.getInstance().log(message);
    }

    @Override
    public void setUserId(@NonNull String userId) {
        AGConnectCrash.getInstance().setUserId(userId);
    }

    @Override
    public void setCustomKey(@NonNull String key, @NonNull String value) {
        AGConnectCrash.getInstance().setCustomKey(key, value);
    }

    private static final class LazyHolder {
        private static final TezCrashlytics INSTANCE = new TezCrashlytics();
    }
}
