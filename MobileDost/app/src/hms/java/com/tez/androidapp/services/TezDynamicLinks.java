package com.tez.androidapp.services;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.huawei.agconnect.applinking.AGConnectAppLinking;
import com.huawei.agconnect.applinking.AppLinking;
import com.huawei.agconnect.applinking.ResolvedLinkData;
import com.huawei.agconnect.applinking.ShortAppLinking;
import com.tez.androidapp.services.models.TezDynamicLinkData;
import com.tez.androidapp.services.models.TezShortDynamicLink;
import com.tez.androidapp.services.models.TezTask;

public class TezDynamicLinks implements ITezDynamicLinks<ResolvedLinkData, TezDynamicLinkData> {

    public static TezDynamicLinks getInstance() {
        return new TezDynamicLinks();
    }

    @NonNull
    @Override
    public TezTask<ResolvedLinkData, TezDynamicLinkData> getDynamicLink(@NonNull Activity activity, @NonNull Intent intent) {
        return new TezTask<>(AGConnectAppLinking.getInstance().getAppLinking(activity),
                ResolvedLinkData.class,
                TezDynamicLinkData.class);
    }

    @NonNull
    @Override
    public Builder createDynamicLink() {
        return new Builder(new AppLinking.Builder());
    }

    public static final class Builder implements ITezDynamicLinks.Builder<ShortAppLinking, TezShortDynamicLink> {

        private final AppLinking.Builder builder;

        public Builder(AppLinking.Builder builder) {
            this.builder = builder;
        }

        @Override
        public Builder setDomainUriPrefix(String s) {
            builder.setUriPrefix(s);
            return this;
        }

        @Override
        public Builder setLink(Uri link) {
            builder.setDeepLink(link);
            return this;
        }

        @Override
        public TezTask<ShortAppLinking, TezShortDynamicLink> buildShortDynamicLink() {
            return new TezTask<>(builder.buildShortAppLinking(ShortAppLinking.LENGTH.SHORT),
                    ShortAppLinking.class,
                    TezShortDynamicLink.class);
        }
    }
}
