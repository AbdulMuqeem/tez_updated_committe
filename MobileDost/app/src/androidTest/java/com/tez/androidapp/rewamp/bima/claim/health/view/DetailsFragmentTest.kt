package com.tez.androidapp.rewamp.bima.claim.health.view

import android.content.Intent
import android.os.Build
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import com.tez.androidapp.R
import com.tez.androidapp.rewamp.bima.claim.common.location.view.LocationPickerActivity
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DetailsFragmentTest {

    private lateinit var activityScenario: ActivityScenario<HospitalCoverageLodgeClaimActivity>

    @Before
    fun setup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            with(getInstrumentation().uiAutomation) {
                executeShellCommand("appops set " + getInstrumentation().targetContext.packageName + " android:mock_location allow")
                Thread.sleep(1000)
            }
        }
        val intent = Intent(ApplicationProvider.getApplicationContext(), HospitalCoverageLodgeClaimActivity::class.java)
        intent.putExtra("PRODUCT_ID", 4)
        intent.putExtra("POLICY_ID", 175298)
        intent.putExtra("ACTIVATION_DATE", "13-02-2020")
        intent.putExtra("EXPIRY_DATE", "13-03-2022")
        activityScenario = ActivityScenario.launch(intent)
        AmountFragmentTest().validateButtonIsClickedAndFragmentIsTransitioned_IfAmountIsEntered()
        ReasonFragmentTest().validateFragmentTransitionOnContinue()
    }

    @After
    fun tearDown() {
        activityScenario.close()
    }

    @Test
    fun validateViewsAreDisplayed() {
        onView(withText(R.string.enter_the_details_of_your_hospitalization))
                .check(matches(isDisplayed()))

        onView(withId(R.id.tilDateOfAdmission)).check(matches(isDisplayed()))
        onView(withId(R.id.etDateOfAdmission)).check(matches(isDisplayed()))

        onView(withId(R.id.tilDateOfDischarge)).check(matches(isDisplayed()))
        onView(withId(R.id.etDateOfDischarge)).check(matches(isDisplayed()))

        onView(withId(R.id.tilNameOfHospital)).check(matches(isDisplayed()))
        onView(withId(R.id.etNameOfHospital)).check(matches(isDisplayed()))

        onView(withId(R.id.tilLocationOfHospital)).check(matches(isDisplayed()))
        onView(withId(R.id.etLocationOfHospital)).check(matches(isDisplayed()))

        onView(withId(R.id.tilNearestLandmark)).check(matches(not(isDisplayed())))
        onView(withId(R.id.etNearestLandmark)).check(matches(not(isDisplayed())))

        onView(withText(R.string.string_continue)).check(matches(isDisplayed()))
    }

    @Test
    fun validateContinueButtonIsDisabledInitially() {
        onView(withId(R.id.nsvContent)).perform(swipeUp())
        onView(withText(R.string.string_continue)).perform(click())
        onView(withId(R.id.nsvContent)).perform(swipeDown())
        onView(withText(R.string.enter_the_details_of_your_hospitalization))
                .check(matches(isDisplayed()))
    }

    @Test
    fun validateCalendarOpensOnDateQuestionsAndFillsTheDate() {
        Thread.sleep(1000)
        onView(withId(R.id.etDateOfAdmission)).perform(click())
        onView(withId(R.id.datePicker)).check(matches(isDisplayed()))
        onView(withId(R.id.buttonOk)).perform(click())
        onView(withId(R.id.etDateOfAdmission)).check(matches(not(withText(""))))

        Thread.sleep(1000)
        onView(withId(R.id.etDateOfDischarge)).perform(click())
        onView(withId(R.id.datePicker)).check(matches(isDisplayed()))
        onView(withId(R.id.buttonOk)).perform(click())
        onView(withId(R.id.etDateOfDischarge)).check(matches(not(withText(""))))
    }

    @Test
    fun validateLocationIsSelectedOnLocationQuestions() {
        Thread.sleep(1000)
        Intents.init()
        onView(withId(R.id.etLocationOfHospital)).perform(click())
        intended(hasComponent(LocationPickerActivity::class.java.name))
        Thread.sleep(5000)
        onView(withText(R.string.done)).perform(click())
        onView(withId(R.id.etLocationOfHospital)).check(matches(not(withText(""))))
        onView(withId(R.id.tilNearestLandmark)).check(matches(isDisplayed()))
        onView(withId(R.id.etNearestLandmark)).check(matches(isDisplayed()))
        Intents.release()
    }

    @Test
    fun validateFragmentTransitionOnAllFieldsFilledAndContinueButtonClicked() {
        validateCalendarOpensOnDateQuestionsAndFillsTheDate()
        validateLocationIsSelectedOnLocationQuestions()
        onView(withId(R.id.nsvContent)).perform(swipeUp())
        Thread.sleep(1000)
        onView(withText(R.string.string_continue)).perform(click())
        onView(withText(R.string.attach_your_documents)).check(matches(isDisplayed()))
    }
}