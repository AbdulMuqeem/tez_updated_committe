package com.tez.androidapp.app.rewamp.signup;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import com.tez.androidapp.R;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.commons.locale.LocaleHelper;
import com.tez.androidapp.rewamp.signup.SelectLanguageActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class SelectLanguageActivityTest {

    @Rule
    public ActivityScenarioRule<SelectLanguageActivity> intentsTestRule
            = new ActivityScenarioRule<>(SelectLanguageActivity.class);


    @Test
    public void check_english_and_roman_are_present() {

        onView(withId(R.id.buttonEnglish))
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.english)));

        onView(withId(R.id.buttonRomanUrdu))
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.roman_urdu)));

        onView(withId(R.id.buttonUrdu))
                .check(matches(not(isDisplayed())))
                .check(matches(withText(R.string.urdu)));

        onView(withId(R.id.textViewFaq)).check(matches(isDisplayed()));
    }

    @Test
    public void onclick_english_assert_english_is_saved_in_preference() {
        check_clicked_language_is_saved_in_preference(R.id.buttonEnglish, LocaleHelper.ENGLISH);
    }

    @Test
    public void onclick_roman_assert_roman_is_saved_in_preference() {
        check_clicked_language_is_saved_in_preference(R.id.buttonRomanUrdu, LocaleHelper.ROMAN);
    }

    private void check_clicked_language_is_saved_in_preference(int buttonId, String preferredLanguage) {
        onView(withId(buttonId)).perform(click());
        assertEquals(LocaleHelper.getSelectedLanguage(MobileDostApplication.getAppContext()), preferredLanguage);
        new TezOnboardingActivityTest().check_viewpager_and_main_button_is_displayed();
    }
}