package com.tez.androidapp.rewamp.bima.claim.common.document.view


import android.content.Intent
import android.os.Build
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.tez.androidapp.R
import com.tez.androidapp.app.MobileDostApplication
import com.tez.androidapp.assertions.RecyclerViewItemCountAssertion
import com.tez.androidapp.commons.managers.MDPreferenceManager
import com.tez.androidapp.commons.models.network.AccessToken
import com.tez.androidapp.reader.JsonFileReader
import com.tez.androidapp.rewamp.bima.claim.common.document.adapter.EvidenceAdapter
import com.tez.androidapp.rewamp.bima.claim.health.view.AmountFragmentTest
import com.tez.androidapp.rewamp.bima.claim.health.view.DetailsFragmentTest
import com.tez.androidapp.rewamp.bima.claim.health.view.HospitalCoverageLodgeClaimActivity
import com.tez.androidapp.rewamp.bima.claim.health.view.ReasonFragmentTest
import net.tez.camera.activity.CameraActivity
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EvidenceFragmentTest {

    private val mockWebServer = MockWebServer()
    private lateinit var activityScenario: ActivityScenario<HospitalCoverageLodgeClaimActivity>

    @Before
    fun setup() {
        if (MDPreferenceManager.getAccessToken() == null)
            MDPreferenceManager.setAccessToken(AccessToken())
        mockWebServer.start(8080)
        mockWebServer.enqueue(MockResponse().setBody(JsonFileReader.readStringFromFile("json/wallets_success_response.json")))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            with(InstrumentationRegistry.getInstrumentation().uiAutomation) {
                executeShellCommand("appops set " + InstrumentationRegistry.getInstrumentation().targetContext.packageName + " android:mock_location allow")
                Thread.sleep(1000)
            }
        }
        val intent = Intent(ApplicationProvider.getApplicationContext(), HospitalCoverageLodgeClaimActivity::class.java)
        intent.putExtra("PRODUCT_ID", 4)
        intent.putExtra("POLICY_ID", 175298)
        intent.putExtra("ACTIVATION_DATE", "13-02-2020")
        intent.putExtra("EXPIRY_DATE", "13-03-2022")
        activityScenario = ActivityScenario.launch(intent)
        AmountFragmentTest().validateButtonIsClickedAndFragmentIsTransitioned_IfAmountIsEntered()
        ReasonFragmentTest().validateFragmentTransitionOnContinue()
        DetailsFragmentTest().validateFragmentTransitionOnAllFieldsFilledAndContinueButtonClicked()
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
        activityScenario.close()
    }

    @Test
    fun validateViewsAreDisplayed() {
        onView(withText(R.string.attach_your_documents)).check(matches(isDisplayed()))
        onView(withId(R.id.rvClaimDocuments)).check(matches(isDisplayed()))
        onView(withText(R.string.string_continue)).check(matches(isDisplayed()))
    }

    @Test
    fun validatePopupIsOpeningOnSelectingDocument() {
        Thread.sleep(1000)
        onView(withId(R.id.rvClaimDocuments)).perform(
                RecyclerViewActions.actionOnItemAtPosition<EvidenceAdapter.EvidenceViewHolder>(0, click()))
        onView(withText(R.string.capture_image)).check(matches(isDisplayed()))
        onView(withText(R.string.upload_document)).check(matches(isDisplayed()))
    }

    @Test
    fun validateFilesAdded() {
        var expectedFilesAdded = 1
        validateFilesAddedThroughCamera(expectedFilesAdded)
        var num = ApplicationProvider.getApplicationContext<MobileDostApplication>()
                .getString(R.string.num_files_uploaded, expectedFilesAdded)
        onView(withText(num)).check(matches(isDisplayed()))

        expectedFilesAdded = 2
        validateFilesAddedThroughCamera(expectedFilesAdded)
        num = ApplicationProvider.getApplicationContext<MobileDostApplication>()
                .getString(R.string.num_files_uploaded, expectedFilesAdded)
        onView(withText(num)).check(matches(isDisplayed()))
    }

    @Test
    fun validateFilesDeleted() {
        validateFilesAdded()
        Thread.sleep(1000)
        onView(withId(R.id.rvClaimDocuments)).perform(
                RecyclerViewActions.actionOnItemAtPosition<EvidenceAdapter.EvidenceViewHolder>(0, click()))
        onView(withText(R.string.edit)).perform(click())
        validateDeleteFile(1)
        validateDeleteFile(0)
    }

    @Test
    fun validateFragmentTransitionOnContinue() {
        Thread.sleep(1000)
        onView(withId(R.id.rvClaimDocuments)).perform(
                RecyclerViewActions.actionOnItemAtPosition<EvidenceAdapter.EvidenceViewHolder>(0, click()))
        onView(withText(R.string.capture_image)).perform(click())
        Thread.sleep(1000)
        onView(withId(R.id.imageViewCapture)).perform(click())
        Thread.sleep(1000)
        onView(withId(R.id.imageViewDone)).perform(click())
        Thread.sleep(1000)
        onView(withText(R.string.string_continue)).perform(click())
        Thread.sleep(1000)
        onView(withText(R.string.string_continue)).perform(click())
        onView(withText(R.string.claim_summary)).check(matches(isDisplayed()))
    }

    private fun validateDeleteFile(expected: Int) {
        Thread.sleep(1000)
        onView(withId(R.id.ivOptions)).perform(click())
        onView(withText(R.string.delete)).perform(click())
        if (expected == 0) {
            onView(withText(R.string.attach_your_documents)).check(matches(isDisplayed()))
        } else
            onView(withId(R.id.rvThumbnails)).check(RecyclerViewItemCountAssertion(expected))
    }

    private fun validateFilesAddedThroughCamera(expected: Int) {
        Intents.init()
        Thread.sleep(1000)
        onView(withId(R.id.rvClaimDocuments)).perform(
                RecyclerViewActions.actionOnItemAtPosition<EvidenceAdapter.EvidenceViewHolder>(0, click()))
        onView(withText(R.string.capture_image)).perform(click())
        intended(hasComponent(CameraActivity::class.java.name))
        Thread.sleep(1000)
        onView(withId(R.id.imageViewCapture)).perform(click())
        Thread.sleep(1000)
        onView(withId(R.id.imageViewDone)).perform(click())
        intended(hasComponent(UploadDocumentActivity::class.java.name))
        onView(withId(R.id.rvThumbnails)).check(RecyclerViewItemCountAssertion(expected))
        Thread.sleep(1000)
        onView(withId(R.id.btContinue)).perform(click())
        Intents.release()
    }
}