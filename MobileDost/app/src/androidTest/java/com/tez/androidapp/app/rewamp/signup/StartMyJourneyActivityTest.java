package com.tez.androidapp.app.rewamp.signup;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.signup.StartMyJourneyActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class StartMyJourneyActivityTest {

    @Rule
    public ActivityScenarioRule<StartMyJourneyActivity> activityRule
            = new ActivityScenarioRule<>(StartMyJourneyActivity.class);


    @Test
    public void check_views_are_displayed() {
        onView(withText(R.string.hello_welcome_to_tez)).check(matches(isDisplayed()));
        onView(withText(R.string.start_my_tez_journey)).check(matches(isDisplayed()));
        onView(withText(R.string.cancel)).check(matches(isDisplayed()));
    }

    @Test
    public void on_click_cancel_activity_finished() {
        onView(withId(R.id.tvCancel)).perform(click());
        activityRule.getScenario().onActivity(activity -> assertTrue(activity.isFinishing()));
    }

    @Test
    public void on_click_start_my_journey_show_signup() {
        onView(withText(R.string.start_my_tez_journey)).perform(click());
        new SignUpActivityTest().check_views_are_displayed();
    }
}