package com.tez.androidapp.rewamp.bima.claim.common.success

import android.content.Intent
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.tez.androidapp.R
import com.tez.androidapp.commons.utils.app.Constants
import com.tez.androidapp.rewamp.bima.claim.model.InsuranceClaimConfirmationDto
import com.tez.androidapp.rewamp.dashboard.view.DashboardActivity
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ClaimSubmittedActivityTest {

    private lateinit var activityScenario: ActivityScenario<ClaimSubmittedActivity>

    @Test
    fun validateViewsOnLodgeClaim() {
        val intent = Intent(ApplicationProvider.getApplicationContext(), ClaimSubmittedActivity::class.java)
        intent.putExtra("PRODUCT_ID", Constants.HOSPITAL_COVERAGE_PLAN_ID)
        intent.putExtra("DTO", InsuranceClaimConfirmationDto(
                policyId = 179412,
                policyName = "EFU-272",
                policyTag = "Secure your future",
                claimAmount = 2128.0,
                claimDate = "2020-08-16",
                claimNumber = "TPL-CLAIM",
                providerCode = Constants.INSURANCE_PROVIDER_EFU
        ))
        intent.putExtra("REVISE", false)
        activityScenario = ActivityScenario.launch(intent)
        onView(withText(R.string.congratulations)).check(matches(isDisplayed()))
        onView(withText(R.string.your_claim_has_been_lodged)).check(matches(isDisplayed()))
        onView(withText(R.string.claim_date)).check(matches(isDisplayed()))
        onView(withText(R.string.back_to_dashboard)).check(matches(isDisplayed()))
        onView(withText("EFU-272")).check(matches(isDisplayed()))
        onView(withText("Secure your future")).check(matches(isDisplayed()))
        onView(withText("Rs. 2,128")).check(matches(isDisplayed()))
        onView(withText("TPL-CLAIM")).check(matches(isDisplayed()))
        onView(withText("16-08-2020")).check(matches(isDisplayed()))
    }

    @Test
    fun validateViewsOnReviseClaim() {
        val intent = Intent(ApplicationProvider.getApplicationContext(), ClaimSubmittedActivity::class.java)
        intent.putExtra("PRODUCT_ID", Constants.HOSPITAL_COVERAGE_PLAN_ID)
        intent.putExtra("DTO", InsuranceClaimConfirmationDto(
                policyId = 179412,
                policyName = "EFU-272",
                policyTag = "Secure your future",
                claimAmount = 2128.0,
                claimDate = "2020-08-16",
                claimNumber = "TPL-CLAIM",
                providerCode = Constants.INSURANCE_PROVIDER_EFU
        ))
        intent.putExtra("REVISE", true)
        activityScenario = ActivityScenario.launch(intent)
        onView(withText(R.string.claim_has_been_resubmitted)).check(matches(isDisplayed()))
        onView(withText(R.string.you_will_be_contacted)).check(matches(isDisplayed()))
        onView(withText(R.string.revise_date)).check(matches(isDisplayed()))
        onView(withText(R.string.back_to_dashboard)).check(matches(isDisplayed()))
        onView(withText("EFU-272")).check(matches(isDisplayed()))
        onView(withText("Secure your future")).check(matches(isDisplayed()))
        onView(withText("Rs. 2,128")).check(matches(isDisplayed()))
        onView(withText("TPL-CLAIM")).check(matches(isDisplayed()))
        onView(withText("16-08-2020")).check(matches(isDisplayed()))
    }

    @Test
    fun clickContinueButton_ShouldGoToDashboard() {
        val intent = Intent(ApplicationProvider.getApplicationContext(), ClaimSubmittedActivity::class.java)
        intent.putExtra("PRODUCT_ID", Constants.HOSPITAL_COVERAGE_PLAN_ID)
        intent.putExtra("DTO", InsuranceClaimConfirmationDto(
                policyId = 179412,
                policyName = "EFU-272",
                policyTag = "Secure your future",
                claimAmount = 2128.0,
                claimDate = "2020-08-16",
                claimNumber = "TPL-CLAIM",
                providerCode = Constants.INSURANCE_PROVIDER_EFU
        ))
        intent.putExtra("REVISE", true)
        activityScenario = ActivityScenario.launch(intent)
        Intents.init()
        onView(withText(R.string.back_to_dashboard)).perform(click())
        intended(hasComponent(DashboardActivity::class.java.name))
        Intents.release()
    }
}