package com.tez.androidapp.assertions

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import org.hamcrest.core.Is.`is`

class RecyclerViewItemCountAssertion(private val expectedCount: Int) : ViewAssertion {

    override fun check(view: View?, noViewFoundException: NoMatchingViewException?) {
        noViewFoundException?.let { throw it }
        view?.let {
            val count = (it as RecyclerView).adapter?.itemCount
            val message = "Expected $expectedCount items in RecyclerView but found $count"
            assertThat(message, count, `is`(expectedCount))
        }
    }
}