package com.tez.androidapp.app.rewamp.signup;


import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.tez.androidapp.rewamp.signup.NewSplashActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class LoginFlowTest {

    @Rule
    public ActivityScenarioRule<NewSplashActivity> activityRule
            = new ActivityScenarioRule<>(NewSplashActivity.class);

    @Test
    public void sign_up_flow() {
        new SelectLanguageActivityTest().check_english_and_roman_are_present();
        new SelectLanguageActivityTest().onclick_english_assert_english_is_saved_in_preference();
        new TezOnboardingActivityTest().on_click_continue_navigate_to_new_intro();
        new NewIntroActivityTest().on_click_continue_navigate_to_existing_user_is_back();
        new ExistingUserIsBackActivityTest().on_click_continue_show_new_login_activity();
    }
}
