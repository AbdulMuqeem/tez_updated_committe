package com.tez.androidapp.app.rewamp.signup;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.signup.WelcomeBackActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class WelcomeBackActivityTest {

    @Rule
    public ActivityScenarioRule<WelcomeBackActivity> activityTestRule =
            new ActivityScenarioRule<>(WelcomeBackActivity.class);


    @Test
    public void check_views_are_displayed() {
        onView(withText(R.string.welcome)).check(matches(isDisplayed()));
        onView(withText(R.string.enter_your_4_digit_login_to_unlock_tez_account)).check(matches(isDisplayed()));
        onView(withText(R.string.forgot_pin)).check(matches(isDisplayed()));
        onView(withText(R.string.faqs)).check(matches(isDisplayed()));
        onView(withId(R.id.pinView)).check(matches(isDisplayed()));
    }
}