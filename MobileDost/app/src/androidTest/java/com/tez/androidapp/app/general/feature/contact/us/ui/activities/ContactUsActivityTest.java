package com.tez.androidapp.app.general.feature.contact.us.ui.activities;


import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.tez.androidapp.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class ContactUsActivityTest {

    @Rule
    public ActivityScenarioRule<ContactUsActivity> activityScenarioRule =
            new ActivityScenarioRule<>(ContactUsActivity.class);

    @Test
    public void check_views_are_displayed() {
        onView(withText(R.string.contact_us)).check(matches(isDisplayed()));
        onView(withText(R.string.thanks_for_reaching_us_out)).check(matches(isDisplayed()));
        onView(withText(R.string.email)).check(matches(isDisplayed()));
        onView(withText(R.string.call)).check(matches(isDisplayed()));
        onView(withText(R.string.whatsapp)).check(matches(isDisplayed()));
        onView(withText(R.string.messenger)).check(matches(isDisplayed()));
    }
}