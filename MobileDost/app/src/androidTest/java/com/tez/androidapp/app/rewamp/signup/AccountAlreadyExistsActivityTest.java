package com.tez.androidapp.app.rewamp.signup;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.tez.androidapp.R;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.signup.AccountAlreadyExistsActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class AccountAlreadyExistsActivityTest {


    @Rule
    public ActivityScenarioRule<AccountAlreadyExistsActivity> activityTestRule =
            new ActivityScenarioRule<>(AccountAlreadyExistsActivity.class);


    @Test
    public void check_views_are_displayed() {
        String greetings = MobileDostApplication.getAppContext().getString(R.string.greetings_and_welcome_back, Utility.getGreetings(MobileDostApplication.getAppContext()));
        onView(withText(greetings)).check(matches(isDisplayed()));
        onView(withText(R.string.account_already_exist_desc)).check(matches(isDisplayed()));
        onView(withText(R.string.string_continue)).check(matches(isDisplayed()));
    }

    @Test
    public void on_click_continue_finish_activity() {
        onView(withText(R.string.string_continue)).perform(click());
        activityTestRule.getScenario().onActivity(activity -> assertTrue(activity.isFinishing()));
    }
}