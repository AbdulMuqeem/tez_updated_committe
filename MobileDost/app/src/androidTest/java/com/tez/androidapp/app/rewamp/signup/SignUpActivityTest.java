package com.tez.androidapp.app.rewamp.signup;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;
import com.tez.androidapp.rewamp.signup.SignUpActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class SignUpActivityTest {

    @Rule
    public ActivityScenarioRule<SignUpActivity> activityTestRule
            = new ActivityScenarioRule<>(SignUpActivity.class);

    @Test
    public void check_views_are_displayed() {
        onView(withText(R.string.create_your_tez_account)).check(matches(isDisplayed()));
        onView(withText(R.string.sim_should_be_on_same_phone_for_verification)).check(matches(isDisplayed()));
        onView(withId(R.id.tilMobileNumber)).check(matches(isDisplayed()));
        onView(withId(R.id.pinEtCreatePin)).check(matches(isDisplayed()));
        onView(withId(R.id.pinEtConfirmPin)).check(matches(isDisplayed()));
        onView(withText(R.string.what_is_pin)).check(matches(isDisplayed()));
        onView(withText(R.string.create_account)).check(matches(isDisplayed()));
        onView(withText(R.string.already_have_an_account)).check(matches(isDisplayed()));
        onView(withText(R.string.login)).check(matches(isDisplayed()));
    }

    @Test
    public void on_click_login_navigate_to_intro() {
        onView(withText(R.string.login)).perform(click());
        activityTestRule.getScenario().onActivity(activity -> assertTrue(activity.isFinishing()));
        onView(withText(R.string.welcome_to_tez)).check(matches(isDisplayed()));
    }

    @Test
    public void on_click_what_is_pin_show_detail_dialog() {
        onView(withText(R.string.what_is_pin)).perform(click());
        onView(withText(R.string.what_is_pin)).check(matches(isDisplayed()));
        onView(withText(R.string.secret_pin_defined)).check(matches(isDisplayed()));
    }

    @Test
    public void on_click_continue_with_invalid_values_show_error() {
        onView(withText(R.string.create_account)).perform(click());
        activityTestRule.getScenario().onActivity(activity -> {
            TezTextInputLayout number = activity.findViewById(R.id.tilMobileNumber);
            TezTextInputLayout pin = activity.findViewById(R.id.pinEtCreatePin).findViewById(R.id.tilPin);

            assertNotNull(number.getError());
            assertNotNull(pin.getError());

            assertEquals(number.getError().toString(), activity.getString(R.string.string_please_enter_valid_phone_number));
            assertEquals(pin.getError().toString(), activity.getString(R.string.please_enter_valid_pin));
        });
    }
}