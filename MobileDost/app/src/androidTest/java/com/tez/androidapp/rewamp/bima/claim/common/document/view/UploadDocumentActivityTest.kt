package com.tez.androidapp.rewamp.bima.claim.common.document.view

import android.content.Intent
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.tez.androidapp.R
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.EvidenceFile
import net.tez.camera.activity.CameraActivity
import net.tez.filepicker.FilePickerActivity
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UploadDocumentActivityTest {

    private lateinit var activityScenario: ActivityScenario<UploadDocumentActivity>

    @Before
    fun setup() = Intents.init()

    @After
    fun tearDown() = Intents.release()

    @Test
    fun validateViewsAreDisplayed_OnSourceEdit() {
        val intent = Intent(ApplicationProvider.getApplicationContext(), UploadDocumentActivity::class.java)
        intent.putExtra("DOC_ID", 1)
        intent.putParcelableArrayListExtra("EVIDENCE_LIST", ArrayList(listOf(
                EvidenceFile(1, "")
        )))
        intent.putExtra("SOURCE", UploadDocumentActivity.Source.EDIT)
        activityScenario = ActivityScenario.launch(intent)
        onView(withId(R.id.ivPreview)).check(matches(isDisplayed()))
        onView(withId(R.id.rvThumbnails)).check(matches(isDisplayed()))
        onView(withId(R.id.ivAddFile)).check(matches(isDisplayed()))
        onView(withText(R.string.string_continue)).check(matches(isDisplayed()))
    }

    @Test
    fun validateCameraOpens_OnSourceCamera() {
        val intent = Intent(ApplicationProvider.getApplicationContext(), UploadDocumentActivity::class.java)
        intent.putExtra("DOC_ID", 1)
        intent.putParcelableArrayListExtra("EVIDENCE_LIST", ArrayList())
        intent.putExtra("SOURCE", UploadDocumentActivity.Source.CAMERA)
        activityScenario = ActivityScenario.launch(intent)
        intended(IntentMatchers.hasComponent(CameraActivity::class.java.name))
    }

    @Test
    fun validateFilePickerOpens_OnSourceDocument() {
        val intent = Intent(ApplicationProvider.getApplicationContext(), UploadDocumentActivity::class.java)
        intent.putExtra("DOC_ID", 1)
        intent.putParcelableArrayListExtra("EVIDENCE_LIST", ArrayList())
        intent.putExtra("SOURCE", UploadDocumentActivity.Source.DOCUMENT)
        activityScenario = ActivityScenario.launch(intent)
        intended(IntentMatchers.hasComponent(FilePickerActivity::class.java.name))
    }

    @Test
    fun validateUnEditableView_OnSourceReadOnly() {
        val intent = Intent(ApplicationProvider.getApplicationContext(), UploadDocumentActivity::class.java)
        intent.putExtra("DOC_ID", 1)
        intent.putParcelableArrayListExtra("EVIDENCE_LIST", ArrayList())
        intent.putExtra("SOURCE", UploadDocumentActivity.Source.READ_ONLY)
        activityScenario = ActivityScenario.launch(intent)
        onView(withId(R.id.ivOptions)).check(matches(not(isDisplayed())))
        onView(withId(R.id.ivAddFile)).check(matches(not(isDisplayed())))
        onView(withText(R.string.back)).check(matches(isDisplayed()))
    }

    @Test
    fun validateCapturedImageIsDisplayedInRecyclerView_OnCapturingItFromCamera() {
        val intent = Intent(ApplicationProvider.getApplicationContext(), UploadDocumentActivity::class.java)
        intent.putExtra("DOC_ID", 1)
        intent.putParcelableArrayListExtra("EVIDENCE_LIST", ArrayList())
        intent.putExtra("SOURCE", UploadDocumentActivity.Source.CAMERA)
        activityScenario = ActivityScenario.launch(intent)
        Thread.sleep(500)
        onView(withId(R.id.imageViewCapture)).perform(click())
        Thread.sleep(1000)
        onView(withId(R.id.imageViewDone)).perform(click())
        onView(withId(R.id.rvThumbnails)).check(matches(isDisplayed()))
    }

    @Test
    fun validateDialogOpens_OnClickAddFile() {
        val intent = Intent(ApplicationProvider.getApplicationContext(), UploadDocumentActivity::class.java)
        intent.putExtra("DOC_ID", 1)
        intent.putParcelableArrayListExtra("EVIDENCE_LIST", ArrayList())
        intent.putExtra("SOURCE", UploadDocumentActivity.Source.EDIT)
        activityScenario = ActivityScenario.launch(intent)
        onView(withId(R.id.ivAddFile)).perform(click())
        onView(withText(R.string.choose_type)).check(matches(isDisplayed()))
    }
}