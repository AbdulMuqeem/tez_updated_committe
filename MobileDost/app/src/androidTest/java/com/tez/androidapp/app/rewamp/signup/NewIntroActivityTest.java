package com.tez.androidapp.app.rewamp.signup;

import android.os.SystemClock;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.signup.NewIntroActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class NewIntroActivityTest {

    @Rule
    public ActivityScenarioRule<NewIntroActivity> activityRule
            = new ActivityScenarioRule<>(NewIntroActivity.class);

    @Test
    public void check_edit_text_and_button_are_displayed() {
        onView(withId(R.id.etMobileNumber)).check(matches(isDisplayed()));
        onView(withId(R.id.tilMobileNumber)).check(matches(isDisplayed()));
        onView(withId(R.id.btNext)).check(matches(isDisplayed()));
        onView(withId(R.id.btAuthId)).check(matches(isDisplayed()));
        onView(withId(R.id.btFacebook)).check(matches(isDisplayed()));
    }

    @Test
    public void on_click_continue_navigate_to_start_my_journey() {
        onView(withId(R.id.etMobileNumber)).perform(typeText("03341818918"));
        onView(withId(R.id.btNext)).perform(click());
        SystemClock.sleep(1000);
        new StartMyJourneyActivityTest().check_views_are_displayed();
    }

    @Test
    public void on_click_continue_navigate_to_existing_user_is_back() {
        onView(withId(R.id.etMobileNumber)).perform(typeText("03342523713"));
        onView(withId(R.id.btNext)).perform(click());
        SystemClock.sleep(1000);
        new ExistingUserIsBackActivityTest().check_views_are_displayed();
    }
}