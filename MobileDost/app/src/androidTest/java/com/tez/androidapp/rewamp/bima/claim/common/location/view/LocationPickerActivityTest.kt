package com.tez.androidapp.rewamp.bima.claim.common.location.view

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.tez.androidapp.R
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class LocationPickerActivityTest {

    @get:Rule
    val activityScenarioRule = ActivityScenarioRule(LocationPickerActivity::class.java)

    @Test
    fun validateViewsAreDisplayed() {
        onView(withId(R.id.ivMyLocation)).check(matches(isDisplayed()))
        onView(withId(R.id.btDone)).check(matches(isDisplayed()))
        onView(withId(R.id.cvLocationDetails)).check(matches(isDisplayed()))
    }
}