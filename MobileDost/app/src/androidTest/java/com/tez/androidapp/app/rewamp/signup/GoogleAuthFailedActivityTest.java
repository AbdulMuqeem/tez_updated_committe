package com.tez.androidapp.app.rewamp.signup;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.signup.GoogleAuthFailedActivity;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class GoogleAuthFailedActivityTest {

    @Rule
    public ActivityScenarioRule<GoogleAuthFailedActivity> activityTestRule =
            new ActivityScenarioRule<GoogleAuthFailedActivity>(GoogleAuthFailedActivity.class);


    @Test
    public void check_views_are_displayed() {
        onView(withText(R.string.google_auth_failed)).check(matches(isDisplayed()));
        onView(withText(R.string.google_auth_failed_desc)).check(matches(isDisplayed()));
        onView(withText(R.string.try_again)).check(matches(isDisplayed()));
    }

    @Test
    public void on_click_cancel_finish_activity() {
        onView(withText(R.string.strings_cancel)).perform(click());
        activityTestRule.getScenario().onActivity(activity -> assertTrue(activity.isFinishing()));
    }

    @Test
    public void on_click_continue_finish_activity() {
        onView(withText(R.string.try_again)).perform(click());
        activityTestRule.getScenario().onActivity(activity -> Assert.assertTrue(activity.isFinishing()));
    }
}