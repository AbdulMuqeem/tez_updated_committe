package com.tez.androidapp.rewamp.bima.claim.health.view

import android.content.Intent
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.tez.androidapp.R
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AmountFragmentTest {

    private lateinit var activityScenario: ActivityScenario<HospitalCoverageLodgeClaimActivity>


    @Before
    fun setup() {
        val intent = Intent(ApplicationProvider.getApplicationContext(), HospitalCoverageLodgeClaimActivity::class.java)
        intent.putExtra("PRODUCT_ID", 4)
        intent.putExtra("POLICY_ID", 175298)
        intent.putExtra("ACTIVATION_DATE", "13-02-2020")
        intent.putExtra("EXPIRY_DATE", "13-03-2020")
        activityScenario = ActivityScenario.launch(intent)
    }

    @After
    fun tearDown() {
        activityScenario.close()
    }

    @Test
    fun validateViewsAreDisplayed() {
        onView(withText(R.string.enter_the_amount_of_your_nclaim_below))
                .check(matches(isDisplayed()))
        onView(withId(R.id.tllAmount)).check(matches(isDisplayed()))
        onView(withText(R.string.string_continue)).check(matches(isDisplayed()))
    }

    @Test
    fun validateFragmentDoesNotTransit_IfAmountNotEntered() {
        onView(withId(R.id.btContinue)).perform(click())
        onView(withId(R.id.tllAmount)).check(matches(isDisplayed()))
    }

    @Test
    fun validateAmountFormatting() {
        val viewInteraction = onView(withId(R.id.etAmount))
        viewInteraction.perform(replaceText("1"))
        viewInteraction.check(matches(withText("1")))

        viewInteraction.perform(replaceText("12"))
        viewInteraction.check(matches(withText("12")))

        viewInteraction.perform(replaceText("123"))
        viewInteraction.check(matches(withText("123")))

        viewInteraction.perform(replaceText("1234"))
        viewInteraction.check(matches(withText("1,234")))

        viewInteraction.perform(replaceText("12345"))
        viewInteraction.check(matches(withText("12,345")))

        viewInteraction.perform(replaceText("123456"))
        viewInteraction.check(matches(withText("123,456")))
    }

    @Test
    fun validateButtonIsClickedAndFragmentIsTransitioned_IfAmountIsEntered() {
        onView(withId(R.id.etAmount)).perform(replaceText("1234"))
        onView(withText(R.string.string_continue)).perform(click())
        onView(withText(R.string.choose_the_reason_of_nyour_hospitalization)).check(matches(isDisplayed()))
    }
}