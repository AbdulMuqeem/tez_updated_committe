package com.tez.androidapp

import android.view.View
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.matcher.ViewMatchers.hasDescendant
import androidx.test.espresso.matcher.ViewMatchers.withId
import org.hamcrest.Matcher

class RecyclerViewItemViewClickAction(private val id: Int) : ViewAction {
    override fun getConstraints(): Matcher<View> = hasDescendant(withId(id))

    override fun getDescription(): String = "Click on view inside recycler view item"

    override fun perform(uiController: UiController?, view: View) {
        val v: View = view.findViewById(id)
        v.performClick()
    }
}