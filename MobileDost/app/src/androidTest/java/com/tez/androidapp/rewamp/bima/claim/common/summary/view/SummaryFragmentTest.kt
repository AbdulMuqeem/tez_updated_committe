package com.tez.androidapp.rewamp.bima.claim.common.summary.view

import android.content.Intent
import android.os.Build
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import com.tez.androidapp.R
import com.tez.androidapp.RecyclerViewItemViewClickAction
import com.tez.androidapp.assertions.RecyclerViewItemCountAssertion
import com.tez.androidapp.commons.managers.MDPreferenceManager
import com.tez.androidapp.commons.models.network.AccessToken
import com.tez.androidapp.reader.JsonFileReader
import com.tez.androidapp.rewamp.bima.claim.common.document.adapter.EvidenceAdapter
import com.tez.androidapp.rewamp.bima.claim.common.document.view.UploadDocumentActivity
import com.tez.androidapp.rewamp.bima.claim.common.success.ClaimSubmittedActivity
import com.tez.androidapp.rewamp.bima.claim.health.view.HospitalCoverageLodgeClaimActivity
import com.tez.androidapp.rewamp.general.purpose.PurposeAdapter
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.After
import org.junit.Before
import org.junit.Test

class SummaryFragmentTest {

    private val mockWebServer = MockWebServer()
    private lateinit var activityScenario: ActivityScenario<HospitalCoverageLodgeClaimActivity>


    @Before
    fun setUp() {
        setAccessToken()

        mockLocation()

        mockWebServer()

        launchIntent()

        inputClaimAmount()
        Thread.sleep(1000)

        inputClaimReason()
        Thread.sleep(1000)

        inputClaimDetails()
        Thread.sleep(1000)

        inputClaimEvidence()
    }

    private fun setAccessToken() {
        if (MDPreferenceManager.getAccessToken() == null)
            MDPreferenceManager.setAccessToken(AccessToken())
    }

    private fun mockLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            with(InstrumentationRegistry.getInstrumentation().uiAutomation) {
                executeShellCommand("appops set " + InstrumentationRegistry.getInstrumentation().targetContext.packageName + " android:mock_location allow")
                Thread.sleep(1000)
            }
        }
    }

    private fun mockWebServer() {
        mockWebServer.start(8080)
        mockWebServer.setDispatcher(object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                request.path?.let {
                    if (it.contains("v1/user/wallets"))
                        return MockResponse().setBody(JsonFileReader.readStringFromFile("json/wallets_success_response.json"))

                    if (it.contains("v1/claim/lodge")) {
                        return MockResponse().setBody(JsonFileReader.readStringFromFile("json/lodge_claim_success_response.json"))
                    }

                    if (it.contains("v1/claim/document/upload")) {
                        return MockResponse().setBody(JsonFileReader.readStringFromFile("json/upload_document_success_response.json"))
                    }

                    if (it.contains("v1/claim/confirmLodge/1987")) {
                        return MockResponse().setBody(JsonFileReader.readStringFromFile("json/confirm_claim_success_response.json"))
                    }
                }

                return MockResponse().setResponseCode(404)
            }
        })
    }

    private fun launchIntent() {
        val intent = Intent(ApplicationProvider.getApplicationContext(), HospitalCoverageLodgeClaimActivity::class.java)
        intent.putExtra("PRODUCT_ID", 4)
        intent.putExtra("POLICY_ID", 175298)
        intent.putExtra("ACTIVATION_DATE", "13-02-2020")
        intent.putExtra("EXPIRY_DATE", "13-03-2022")
        activityScenario = ActivityScenario.launch(intent)
    }

    private fun inputClaimAmount() {
        onView(withId(R.id.etAmount)).perform(replaceText("6782"))
        onView(withText(R.string.string_continue)).perform(click())
    }

    private fun inputClaimReason() {
        onView(withId(R.id.rvClaimPurposes)).perform(
                RecyclerViewActions.actionOnItemAtPosition<PurposeAdapter.PurposeViewHolder>(0, click()))
        onView(withId(R.id.nsvContent)).perform(swipeUp())
        onView(withId(R.id.btContinue)).perform(click())
    }

    private fun inputClaimDetails() {
        onView(withId(R.id.etDateOfAdmission)).perform(click())
        onView(withId(R.id.buttonOk)).perform(click())
        Thread.sleep(1000)
        onView(withId(R.id.etDateOfDischarge)).perform(click())
        onView(withId(R.id.buttonOk)).perform(click())
        onView(withId(R.id.etNameOfHospital)).perform(replaceText("NMC"))
        Thread.sleep(1000)
        onView(withId(R.id.etLocationOfHospital)).perform(click())
        Thread.sleep(5000)
        onView(withText(R.string.done)).perform(click())
        onView(withId(R.id.etNearestLandmark)).perform(replaceText("Kalapul"))
        onView(withId(R.id.nsvContent)).perform(swipeUp())
        Thread.sleep(1000)
        onView(withText(R.string.string_continue)).perform(click())
    }

    private fun inputClaimEvidence() {
        onView(withId(R.id.rvClaimDocuments)).perform(
                RecyclerViewActions.actionOnItemAtPosition<EvidenceAdapter.EvidenceViewHolder>(0, click()))
        onView(withText(R.string.capture_image)).perform(click())
        Thread.sleep(1000)
        onView(withId(R.id.imageViewCapture)).perform(click())
        Thread.sleep(1000)
        onView(withId(R.id.imageViewDone)).perform(click())
        Thread.sleep(1000)
        onView(withText(R.string.string_continue)).perform(click())
        Thread.sleep(1000)
        onView(withText(R.string.string_continue)).perform(click())
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
        activityScenario.close()
    }

    @Test
    fun validateViewsAreDisplayedWithValidData() {
        val isDisplayed = matches(isDisplayed())
        onView(withText(R.string.claim_summary)).check(isDisplayed)
        onView(withText("Rs. 6,782")).check(isDisplayed)
        onView(withId(R.id.rvSummary)).check(isDisplayed)
        onView(withId(R.id.rvSummary)).check(RecyclerViewItemCountAssertion(8))
        onView(withId(R.id.nsvContent)).perform(swipeUp())
        onView(withText("NMC")).check(isDisplayed)
        onView(withText("Kalapul")).check(isDisplayed)
        onView(withText(R.string.attached_documents)).check(isDisplayed)
        onView(withId(R.id.rvClaimDocuments)).check(isDisplayed)
        onView(withId(R.id.rvClaimDocuments)).check(RecyclerViewItemCountAssertion(1))
        onView(withId(R.id.cvAddWallet)).check(isDisplayed)
        onView(withText(R.string.easypaisa)).check(isDisplayed)
        onView(withText("923342523713")).check(isDisplayed)
        onView(withText(R.string.string_change)).check(isDisplayed)
        onView(withText(R.string.string_continue)).check(isDisplayed)
    }

    @Test
    fun validateUserCanSeeAttachedDocumentsByTappingOnEyeIcon() {
        Intents.init()
        onView(withId(R.id.nsvContent)).perform(swipeUp())
        Thread.sleep(1000)
        onView(withId(R.id.rvClaimDocuments)).perform(RecyclerViewItemViewClickAction(R.id.ivViewDoc))
        intended(hasComponent(UploadDocumentActivity::class.java.name))
        Intents.release()
        onView(withId(R.id.rvThumbnails)).check(RecyclerViewItemCountAssertion(1))
        Thread.sleep(1000)
        onView(withText(R.string.back)).perform(click())
        onView(withId(R.id.rvClaimDocuments)).check(matches(isDisplayed()))
    }

    @Test
    fun validateSuccessfulClaimSubmission() {
        onView(withId(R.id.nsvContent)).perform(swipeUp())
        Thread.sleep(1000)
        Intents.init()
        onView(withText(R.string.string_continue)).perform(click())
        Thread.sleep(3000)
        intended(hasComponent(ClaimSubmittedActivity::class.java.name))
        Intents.release()
    }
}