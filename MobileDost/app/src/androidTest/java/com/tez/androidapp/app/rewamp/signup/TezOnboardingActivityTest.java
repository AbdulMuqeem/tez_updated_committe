package com.tez.androidapp.app.rewamp.signup;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.signup.TezOnboardingActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class TezOnboardingActivityTest {

    @Rule
    public ActivityScenarioRule<TezOnboardingActivity> activityRule
            = new ActivityScenarioRule<>(TezOnboardingActivity.class);


    @Test
    public void check_viewpager_and_main_button_is_displayed() {
        onView(withText(R.string.lets_begin)).check(matches(isDisplayed()));
        onView(withId(R.id.viewPagerOnboarding)).check(matches(isDisplayed()));

        onView(withText(R.string.licensed_financial_institution)).check(matches(isDisplayed()));
        onView(withText(R.string.license_description)).check(matches(isDisplayed()));

        onView(withId(R.id.viewPagerOnboarding)).perform(swipeLeft());
        onView(withText(R.string.any_time_any_where)).check(matches(isDisplayed()));
        onView(withText(R.string.access_various_financial_services)).check(matches(isDisplayed()));

        onView(withId(R.id.viewPagerOnboarding)).perform(swipeLeft());
        onView(withText(R.string.safe_and_secure)).check(matches(isDisplayed()));
        onView(withText(R.string.we_value_your_trust)).check(matches(isDisplayed()));
    }

    @Test
    public void on_click_continue_navigate_to_new_intro() {
        onView(withText(R.string.lets_begin)).perform(click());
        new NewIntroActivityTest().check_edit_text_and_button_are_displayed();
    }
}