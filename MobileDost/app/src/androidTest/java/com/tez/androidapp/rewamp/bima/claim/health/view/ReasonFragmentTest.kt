package com.tez.androidapp.rewamp.bima.claim.health.view

import android.content.Intent
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.tez.androidapp.R
import com.tez.androidapp.rewamp.general.purpose.PurposeAdapter
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ReasonFragmentTest {

    private lateinit var activityScenario: ActivityScenario<HospitalCoverageLodgeClaimActivity>

    @Before
    fun setup() {
        val intent = Intent(ApplicationProvider.getApplicationContext(), HospitalCoverageLodgeClaimActivity::class.java)
        intent.putExtra("PRODUCT_ID", 4)
        intent.putExtra("POLICY_ID", 175298)
        intent.putExtra("ACTIVATION_DATE", "13-02-2020")
        intent.putExtra("EXPIRY_DATE", "13-03-2020")
        activityScenario = ActivityScenario.launch(intent)
        AmountFragmentTest().validateButtonIsClickedAndFragmentIsTransitioned_IfAmountIsEntered()
    }

    @After
    fun tearDown() {
        activityScenario.close()
    }

    @Test
    fun validateViewsAreDisplayed() {
        onView(withText(R.string.choose_the_reason_of_nyour_hospitalization))
                .check(matches(isDisplayed()))
        onView(withId(R.id.rvClaimPurposes)).check(matches(isDisplayed()))
        onView(withText(R.string.string_continue)).check(matches(isDisplayed()))
    }

    @Test
    fun validateContinueButtonIsDisabledUntilNoReasonIsSelected() {
        onView(withId(R.id.nsvContent)).perform(swipeUp())
        onView(withId(R.id.btContinue)).perform(click())
        onView(withText(R.string.choose_the_reason_of_nyour_hospitalization)).check(matches(isDisplayed()))
    }

    @Test
    fun validateFragmentTransitionOnContinue() {
        onView(withId(R.id.rvClaimPurposes)).perform(
                RecyclerViewActions.actionOnItemAtPosition<PurposeAdapter.PurposeViewHolder>(0, click()))
        onView(withId(R.id.nsvContent)).perform(swipeUp())
        onView(withId(R.id.btContinue)).perform(click())
        onView(withText(R.string.enter_the_details_of_your_hospitalization)).check(matches(isDisplayed()))
    }
}