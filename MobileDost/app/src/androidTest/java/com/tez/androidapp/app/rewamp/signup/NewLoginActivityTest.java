package com.tez.androidapp.app.rewamp.signup;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.contact.us.ui.activities.ContactUsActivityTest;
import com.tez.androidapp.rewamp.signup.NewLoginActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class NewLoginActivityTest {

    @Rule
    public ActivityScenarioRule<NewLoginActivity> activityTestRule =
            new ActivityScenarioRule<>(NewLoginActivity.class);

    @Test
    public void check_views_are_displayed() {
        onView(withText(R.string.welcome_back)).check(matches(isDisplayed()));
        onView(withText(R.string.to_access_your_account)).check(matches(isDisplayed()));
        onView(withText(R.string.kindly_enter_pin)).check(matches(isDisplayed()));
        onView(withText(R.string.forgot_pin)).check(matches(isDisplayed()));
        onView(withText(R.string.not_your_account)).check(matches(isDisplayed()));
        onView(withText(R.string.click_here)).check(matches(isDisplayed()));
        onView(withId(R.id.pinView)).check(matches(isDisplayed()));
    }

    @Test
    public void on_click_forgot_pin_show_contact_us() {
        onView(withText(R.string.forgot_pin)).perform(click());
        new ContactUsActivityTest().check_views_are_displayed();
    }

    @Test
    public void on_click_click_here_navigate_to_sign_up_activity() {
        onView(withText(R.string.click_here)).perform(click());
        new SignUpActivityTest().check_views_are_displayed();
    }
}