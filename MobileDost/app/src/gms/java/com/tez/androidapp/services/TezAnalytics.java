package com.tez.androidapp.services;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tez.androidapp.app.MobileDostApplication;

public class TezAnalytics implements ITezAnalytics {

    @NonNull
    private final FirebaseAnalytics firebaseAnalytics;

    private TezAnalytics() {
        firebaseAnalytics = FirebaseAnalytics.getInstance(MobileDostApplication.getAppContext());
    }

    public static TezAnalytics getInstance() {
        return LazyHolder.INSTANCE;
    }

    public static void initialize() {
        FirebaseApp.initializeApp(MobileDostApplication.getAppContext());
        AnalyticsTrackers.initialize(MobileDostApplication.getAppContext());
        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
    }

    @Override
    public void setAnalyticsCollectionEnabled(boolean enabled) {
        firebaseAnalytics.setAnalyticsCollectionEnabled(enabled);
    }

    @Override
    public void logEvent(@NonNull String event, @Nullable Bundle params) {
        firebaseAnalytics.logEvent(event, params);
    }

    @Override
    public void setUserId(@NonNull String userId) {
        firebaseAnalytics.setUserId(userId);
    }

    @Override
    public void trackEvent(String category, String action, String label) {
        Tracker t = AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);

        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
    }

    private static final class LazyHolder {
        private static final TezAnalytics INSTANCE = new TezAnalytics();
    }

    public static final class Param {
        public static final String ITEM_CATEGORY = FirebaseAnalytics.Param.ITEM_CATEGORY;
        public static final String ITEM_NAME = FirebaseAnalytics.Param.ITEM_NAME;
    }

    public static final class Event {
        public static final String VIEW_ITEM = FirebaseAnalytics.Event.VIEW_ITEM;
    }
}
