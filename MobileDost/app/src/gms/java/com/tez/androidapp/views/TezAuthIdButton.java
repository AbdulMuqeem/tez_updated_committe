package com.tez.androidapp.views;

import android.content.Context;
import android.util.AttributeSet;

import com.tez.androidapp.commons.widgets.TezImageButton;

public class TezAuthIdButton extends TezImageButton {
    public TezAuthIdButton(Context context) {
        super(context);
    }

    public TezAuthIdButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TezAuthIdButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
