package com.tez.androidapp.services.models;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.dynamiclinks.ShortDynamicLink;

public class TezShortDynamicLink implements ITezShortDynamicLink {

    @NonNull
    private final ShortDynamicLink shortDynamicLink;

    public TezShortDynamicLink(@NonNull ShortDynamicLink shortDynamicLink) {
        this.shortDynamicLink = shortDynamicLink;
    }

    @Nullable
    @Override
    public Uri getShortLink() {
        return shortDynamicLink.getShortLink();
    }
}
