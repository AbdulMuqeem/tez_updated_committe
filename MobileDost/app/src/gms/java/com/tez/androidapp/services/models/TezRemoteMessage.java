package com.tez.androidapp.services.models;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class TezRemoteMessage implements ITezRemoteMessage {

    @NonNull
    private final RemoteMessage remoteMessage;

    public TezRemoteMessage(@NonNull RemoteMessage remoteMessage) {
        this.remoteMessage = remoteMessage;
    }

    @NonNull
    @Override
    public Map<String, String> getData() {
        return remoteMessage.getData();
    }

    @Nullable
    @Override
    public TezNotification getNotification() {
        return remoteMessage.getNotification() != null
                ? new TezNotification(remoteMessage.getNotification())
                : null;
    }

    public static class TezNotification implements ITezRemoteMessage.ITezNotification {

        @NonNull
        private final RemoteMessage.Notification notification;

        public TezNotification(@NonNull RemoteMessage.Notification notification) {
            this.notification = notification;
        }

        @Nullable
        @Override
        public String getTitle() {
            return notification.getTitle();
        }

        @Nullable
        @Override
        public String getBody() {
            return notification.getBody();
        }
    }
}
