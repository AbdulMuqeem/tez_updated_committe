package com.tez.androidapp.services.exception;


import com.google.android.gms.common.api.ApiException;

public class TezApiException extends RuntimeException {

    private final ApiException exception;

    public TezApiException(ApiException exception) {
        this.exception = exception;
    }

    public int getStatusCode() {
        return exception.getStatusCode();
    }
}
