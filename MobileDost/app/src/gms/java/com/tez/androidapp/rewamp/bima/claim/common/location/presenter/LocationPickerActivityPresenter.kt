package com.tez.androidapp.rewamp.bima.claim.common.location.presenter

import com.google.android.gms.maps.model.LatLng
import com.tez.androidapp.commons.utils.app.Utility
import com.tez.androidapp.rewamp.bima.claim.common.location.view.ILocationPickerActivityView

class LocationPickerActivityPresenter(private val iLocationPickerActivityView: ILocationPickerActivityView)
    : ILocationPickerActivityPresenter {

    override fun setLocationDetails(latLng: LatLng) {
        val addresses = Utility.getAddressesFromLatLng(latLng.latitude, latLng.longitude)
        if (addresses != null && addresses.isNotEmpty() && addresses[0] != null) {
            val address = addresses[0]
            val addressLine = address.getAddressLine(if (address.maxAddressLineIndex == -1) 0 else address.maxAddressLineIndex)
            val featureName = address.featureName
            if (addressLine != null) {
                iLocationPickerActivityView.setLocationDetails(featureName ?: "", addressLine)
                iLocationPickerActivityView.enableBtDone()
            }
        }
    }

    override fun setFinalAddress(name: String, address: String) =
            iLocationPickerActivityView.setResults(getFinalAddress(address, name))

    internal fun getFinalAddress(address: String, name: String): String {
        val addressParts = address.split(",")
        if (addressParts.isNotEmpty()) {
            val addressFirstPartItems = addressParts[0].split(" ")
            val nameParts = name.split(" ")
            if (addressFirstPartItems.isNotEmpty()) {
                if (nameParts.isNotEmpty())
                    return if (addressFirstPartItems[0].equals(nameParts[0], ignoreCase = true)) address else "$name, $address"
                return if (addressFirstPartItems[0].equals(name, ignoreCase = true)) address else "$name, $address"
            }
            if (nameParts.isNotEmpty())
                return if (addressParts[0].equals(nameParts[0], ignoreCase = true)) address else "$name, $address"
            return if (addressParts[0].equals(name, ignoreCase = true)) address else "$name, $address"
        }
        return if (address.equals(name, ignoreCase = true)) address else "$name, $address"
    }
}