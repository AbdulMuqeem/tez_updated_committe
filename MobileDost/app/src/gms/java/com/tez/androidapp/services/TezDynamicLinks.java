package com.tez.androidapp.services;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.tez.androidapp.services.models.TezDynamicLinkData;
import com.tez.androidapp.services.models.TezShortDynamicLink;
import com.tez.androidapp.services.models.TezTask;

public class TezDynamicLinks implements ITezDynamicLinks<PendingDynamicLinkData, TezDynamicLinkData> {

    public static TezDynamicLinks getInstance() {
        return new TezDynamicLinks();
    }

    @NonNull
    @Override
    public TezTask<PendingDynamicLinkData, TezDynamicLinkData> getDynamicLink(@NonNull Activity activity, @NonNull Intent intent) {
        return new TezTask<>(FirebaseDynamicLinks.getInstance().getDynamicLink(intent),
                PendingDynamicLinkData.class,
                TezDynamicLinkData.class);
    }

    @NonNull
    @Override
    public Builder createDynamicLink() {
        return new Builder(FirebaseDynamicLinks.getInstance().createDynamicLink());
    }

    public static final class Builder implements ITezDynamicLinks.Builder<ShortDynamicLink, TezShortDynamicLink> {

        private final DynamicLink.Builder builder;

        public Builder(DynamicLink.Builder builder) {
            this.builder = builder;
        }

        @Override
        public TezDynamicLinks.Builder setDomainUriPrefix(String s) {
            builder.setDomainUriPrefix(s);
            return this;
        }

        @Override
        public TezDynamicLinks.Builder setLink(Uri link) {
            builder.setLink(link);
            return this;
        }

        @Override
        public TezTask<ShortDynamicLink, TezShortDynamicLink> buildShortDynamicLink() {
            return new TezTask<>(builder.buildShortDynamicLink(),
                    ShortDynamicLink.class,
                    TezShortDynamicLink.class);
        }
    }
}
