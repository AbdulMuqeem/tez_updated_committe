package com.tez.androidapp.services;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.tez.androidapp.services.models.TezSignInAccount;
import com.tez.androidapp.services.models.TezTask;

public class TezSignInService implements ITezSignInService {

    private final GoogleSignInClient googleSignInClient;

    private TezSignInService(GoogleSignInClient googleSignInClient) {
        this.googleSignInClient = googleSignInClient;
    }

    public static TezSignInService init(@NonNull Activity activity) {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        return new TezSignInService(GoogleSignIn.getClient(activity, gso));
    }

    public static TezTask<GoogleSignInAccount, TezSignInAccount> getSignedInAccountFromIntent(Intent data) {
        return new TezTask<>(
                GoogleSignIn.getSignedInAccountFromIntent(data),
                GoogleSignInAccount.class,
                TezSignInAccount.class
        );
    }

    public static void requestPhoneNumber(@NonNull Activity activity, int requestCode) {
        try {
            HintRequest hintRequest = new HintRequest.Builder()
                    .setPhoneNumberIdentifierSupported(true)
                    .build();

            GoogleApiClient googleApiClient = new GoogleApiClient.Builder(activity).addApi(Auth.CREDENTIALS_API).build();

            PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(googleApiClient, hintRequest);


            activity.startIntentSenderForResult(intent.getIntentSender(), requestCode, null, 0, 0, 0);
        } catch (Exception ignore) {
            //ignore
        }
    }

    @Nullable
    public static String extractPhoneNumberFromIntent(@NonNull Intent data) {
        Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
        return credential != null ? credential.getId() : null;
    }

    @NonNull
    @Override
    public Intent getSignInIntent() {
        return googleSignInClient.getSignInIntent();
    }
}
