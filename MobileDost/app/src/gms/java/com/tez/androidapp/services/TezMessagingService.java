package com.tez.androidapp.services;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tez.androidapp.services.models.TezRemoteMessage;

public abstract class TezMessagingService extends FirebaseMessagingService {

    @Override
    public final void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        onMessageReceived(new TezRemoteMessage(remoteMessage));
    }

    public abstract void onMessageReceived(@NonNull TezRemoteMessage tezRemoteMessage);

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
    }
}
