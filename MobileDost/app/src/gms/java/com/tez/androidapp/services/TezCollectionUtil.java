package com.tez.androidapp.services;

import androidx.annotation.Nullable;

import com.google.android.gms.common.util.ArrayUtils;
import com.google.android.gms.common.util.CollectionUtils;

import java.util.Collection;

public final class TezCollectionUtil {

    public static boolean isEmpty(@Nullable Collection<?> collection) {
        return CollectionUtils.isEmpty(collection);
    }

    public static boolean contains(int[] arr, int val) {
        return ArrayUtils.contains(arr, val);
    }
}
