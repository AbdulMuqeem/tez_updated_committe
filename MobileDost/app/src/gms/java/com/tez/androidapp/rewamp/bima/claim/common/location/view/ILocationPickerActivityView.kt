package com.tez.androidapp.rewamp.bima.claim.common.location.view

import com.google.android.gms.maps.model.LatLng
import com.tez.androidapp.app.base.ui.IBaseView

interface ILocationPickerActivityView : IBaseView {
    fun setLocationDetails(name: String, address: String)
    fun enableBtDone()
    fun initMarker(latLng: LatLng)
    fun moveCamera(latLng: LatLng)
    fun setResults(address: String)
}