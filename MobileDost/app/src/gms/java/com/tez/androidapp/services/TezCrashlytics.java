package com.tez.androidapp.services;

import androidx.annotation.NonNull;

import com.google.firebase.crashlytics.FirebaseCrashlytics;

public final class TezCrashlytics implements ITezCrashlytics {

    private TezCrashlytics() {

    }

    @NonNull
    public static TezCrashlytics getInstance() {
        return LazyHolder.INSTANCE;
    }

    @Override
    public void setCrashlyticsCollectionEnabled(boolean enabled) {
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(enabled);
    }

    @Override
    public void recordException(@NonNull Throwable throwable) {
        FirebaseCrashlytics.getInstance().recordException(throwable);
    }

    @Override
    public void log(@NonNull String message) {
        FirebaseCrashlytics.getInstance().log(message);
    }

    @Override
    public void setUserId(@NonNull String userId) {
        FirebaseCrashlytics.getInstance().setUserId(userId);
    }

    @Override
    public void setCustomKey(@NonNull String key, @NonNull String value) {
        FirebaseCrashlytics.getInstance().setCustomKey(key, value);
    }

    private static final class LazyHolder {
        private static final TezCrashlytics INSTANCE = new TezCrashlytics();
    }
}
