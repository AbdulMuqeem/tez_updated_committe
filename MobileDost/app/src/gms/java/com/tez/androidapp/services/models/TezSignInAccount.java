package com.tez.androidapp.services.models;

import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

public class TezSignInAccount implements ITezSignInAccount {

    @NonNull
    private final GoogleSignInAccount account;

    public TezSignInAccount(GoogleSignInAccount account) {
        this.account = account;
    }

    @Override
    public String getId() {
        return account.getId();
    }
}
