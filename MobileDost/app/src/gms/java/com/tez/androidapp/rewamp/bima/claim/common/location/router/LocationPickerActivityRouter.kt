package com.tez.androidapp.rewamp.bima.claim.common.location.router

import android.content.Intent
import com.google.android.gms.maps.model.LatLng
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter
import com.tez.androidapp.rewamp.bima.claim.common.location.view.LocationPickerActivity
import com.tez.androidapp.rewamp.bima.claim.health.view.DetailsFragment

class LocationPickerActivityRouter : BaseActivityRouter() {
    fun setDependenciesAndRoute(from: DetailsFragment) {
        val intent = Intent(from.context, LocationPickerActivity::class.java)
        from.startActivityForResult(intent, REQUEST_CODE_PICK_LOCATION)
    }

    companion object {
        const val REQUEST_CODE_PICK_LOCATION = 12989
        private const val ADDRESS = "ADDRESS"
        private const val LAT_LNG = "LAT_LNG"
        fun createInstance(): LocationPickerActivityRouter {
            return LocationPickerActivityRouter()
        }

        class Result(private val intent: Intent) {
            val address: String
                get() = intent.getStringExtra(ADDRESS)!!
            val latLng: LatLng
                get() = intent.getParcelableExtra(LAT_LNG)!!
        }

        fun getResult(data: Intent): Result = Result(data)

        @JvmStatic
        fun createResultIntent(address: String, latLng: LatLng): Intent {
            val intent = Intent()
            intent.putExtra(ADDRESS, address)
            intent.putExtra(LAT_LNG, latLng)
            return intent
        }
    }
}