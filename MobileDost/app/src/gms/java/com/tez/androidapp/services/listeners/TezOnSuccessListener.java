package com.tez.androidapp.services.listeners;

import com.google.android.gms.tasks.OnSuccessListener;

public interface TezOnSuccessListener<O> extends OnSuccessListener<O> {
}
