package com.tez.androidapp.services.models;

import android.net.Uri;

import androidx.annotation.Nullable;

import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

public class TezDynamicLinkData implements ITezDynamicLinkData {

    private final PendingDynamicLinkData data;

    public TezDynamicLinkData(PendingDynamicLinkData data) {
        this.data = data;
    }

    @Nullable
    @Override
    public Uri getLink() {
        return data.getLink();
    }
}
