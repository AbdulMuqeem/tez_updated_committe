<p>
    Tez Financial Services Limited (“TezFS” or “Tez”) is committed to
    protecting and respecting your privacy.
</p>
<p>
    Scope of policy
</p>
<p>
    This policy, together with our end-user license agreement as set out at
    http:www.tezfinancialservices.pk, and any additional terms of use applies
    to your use of:
</p>
<ol>
    <li>
        <p>
            TezFS mobile application software (App) available on our site OR
            hosted on the Google Play Store (App Site), once you have
            downloaded or streamed a copy of the App onto your mobile telephone
            or handheld device (Device).
        </p>
    </li>
</ol>
<ol>
    <li>
        <p>
            Any of the services accessible through the App (Services), those
            available on the App Site or other sites of ours (Services Sites).
        </p>
    </li>
</ol>
<p>
    This policy sets out the basis on which any personal data we collect from
    you from your consent, or that you provide to us, will be processed by us.
</p>
<p>
    Please read the following carefully to understand our views and practices
    regarding your personal data and how we will treat it. Please also note
    that the personal data that may be collected by TezFS will only be used for
    the purpose of determining whether you are eligible or not for TezFS
    services and the terms and conditions of such services.
</p>
<p>
    Information we may collect from you
</p>
<p>
    TezFS may collect and process the following data about you.
</p>
<p>
    Information you give us about you (Submitted information):
</p>
<ul>
    <li>
        <p>
            provided by filling in the forms in the App or on the App Site
            (together, Our Sites);
        </p>
    </li>
    <li>
        <p>
            provided by corresponding with us (for example, by e-mail or chat
            or helpline);
        </p>
    </li>
    <li>
        <p>
            including information you provide when you register to use the App
            Site, download or register an App, subscribe to any of our Services
            (such as applying for a loan), search for an App or Service, share
            data via an App's social media functions, enter a competition,
            promotion or survey, and when you report a problem with an App, our
            Services, or any of our Sites; and
        </p>
    </li>
    <li>
        <p>
            Including your name, address, e-mail address and phone number, the
            Device's phone number, SIM card, age, username, password and other
            registration information, financial and credit information,
            personal description and photograph.
        </p>
    </li>
</ul>
<p>
    <strong>Information we collect about you and your Device:</strong>
</p>
<p>
    Each time you visit our Site or use our App, we may automatically collect
    the following information:
</p>
<p>
    technical information, including the type of mobile device you use, unique
    device identifiers (for example, your Device's IMEI or serial number),
    information about the SIM card used by the Device, mobile network
    information, your device operating system, the type of browser you use, or
    your Device’s location and time zone setting (Device Information);
</p>
<p>
    information stored on your Device, including contact lists, call logs, SMS
    logs, camera, videos or other digital content (Content Information);
</p>
<p>
    details of your use of any of our Apps or your visits to any of Our Sites
    including, but not limited to traffic data, location data, weblogs and
    other communication data (Log Information).
</p>
<p>
    <strong>Location information.</strong>
    TezFS may also use GPS technology OR other location services to determine
    your current location.
</p>
<p>
    <strong>Removal of consent for collecting further information.</strong>
    You can withdraw your consent for further collection of this information at
    any time by logging out and uninstalling the app from your Device.
</p>
<p>
    <strong>
        Information we receive from other sources (Third Party Information).
    </strong>
    TezFS are working with a limited number of third parties (including NADRA,
    mobile wallet providers, credit bureaus, insurance companies, or other
    partner financial institutions) and may receive information about you from
    them.
</p>
<p>
    <strong>Unique application numbers.</strong>
    When you install or uninstall a Service containing a unique application
    number or when such a Service searches for automatic updates, that number
    and information about your installation, for example, the type of operating
    system, may be sent to us.
</p>
<p>
    Tracking and cookies
</p>
<p>
    TezFS may use mobile tracking technologies and/or website cookies to
    distinguish you from other users of the App, App Site, Appstore or Service
    Site. This helps us to provide you with a good experience when you use the
    App or browse any of the sites and also allows us to improve the App and
    our sites.
</p>
<p>
    Uses made of the information
</p>
<p>
    TezFS may associate any category of information with any other category of
    information and will treat the combined information as personal data in
    accordance with this policy for as long as it is combined.
</p>
<p>
    Information collected by us shall be used for the purpose of determining
    whether or not to provide a financial service or product to the customer,
    the amount of such exposure and the terms and conditions applicable to such
    services or product.
</p>
<p>
    TezFS does not disclose information about identifiable individuals to other
    parties, but we may provide them with anonymous aggregate information about
    our users (for example, we may inform them that 500 men aged under 30 have
    applied for a loan on any given day).
</p>
<p>
    Disclosure of your information
</p>
<p>
    TezFS may disclose some or all of the data we collect from you when you
    download or use the App to connect to mobile wallet providers, credit
    bureaus, insurance companies, or other partner financial institutions.
</p>
<p>
    TezFS may disclose your personal information to any member of our group,
    which means our subsidiaries, our ultimate holding company and its
    subsidiaries.
</p>
<p>
    TezFS may disclose your personal information to any local or international
    law enforcement or competent regulatory or governmental agencies so as to
    assist in the prevention, detection, investigation or prosecution of
    criminal activities or fraud.
</p>
<p>
    TezFS may disclose your personal information to third parties:
</p>
<ul>
    <li>
        <p>
            in the event that we sell or buy any business or assets, in which
            case we may disclose your personal data to the prospective seller
            or buyer of such business or assets;
        </p>
    </li>
    <li>
        <p>
            if TezFS or substantially all of its assets are acquired by a third
            party, personal data held by it about its customers will be one of
            the transferred assets;
        </p>
    </li>
    <li>
        <p>
            if TezFS is under a duty to disclose or share your personal data in
            order to comply with any legal or regulatory obligation or request;
            and/or
        </p>
    </li>
    <li>
        <p>
            in order to: enforce our Terms and Conditions and other agreements
            or to investigate potential breaches; report defaulters to any
            credit bureau; or for the purpose of publishing statistics relating
            to the use of the App, in which case all information will be
            aggregated and made anonymous.
        </p>
    </li>
</ul>
<p>
    Where TezFS stores your personal data
</p>
<p>
    The data that we collect from you may be transferred to, and stored at, a
    destination inside Pakistan. The staff members at the destination may be
    engaged in the fulfillment of your requests. By submitting your personal
    data, you agree to this transfer, storing or processing. We will take all
    steps reasonably necessary to ensure that your data is treated securely and
    in accordance with this privacy policy.
</p>
<p>
    Where we have given you (or where you have chosen) a password that enables
    you to access certain parts of Our Sites, you are responsible for keeping
    this password confidential. We ask you not to share a password with anyone.
    Unfortunately, the transmission of information via the internet is not
    completely secure. Although we will do our best to protect your personal
    data, but we cannot guarantee the security of your data transmitted to Our
    Sites; any transmission is at your own risk. Once we have received your
    information, we will use strict procedures and security features to try to
    prevent unauthorized access.
</p>
<p>
    Your rights
</p>
<p>
    We will use your data for the purposes of compiling statistics relating to
    our user base or financial product or services and may disclose such
    information to any third party for such purposes, provided that such
    information will always be anonymous.
</p>
<p>
    Should we wish to use your information for marketing purposes, we will
    inform you prior to such use. You shall be entitled to prevent such usage
    by informing us, within 10 days of being informed of the proposed use, that
    you do not wish to disclose such information. You can also exercise the
    right at any time by contacting us at
    <a
        href="mailto:support@tezfinancialservices.pk"
        title="mailto:support@tezfinancialservices.pk"
    >
        support@tezfinancialservices.pk
    </a>
</p>
<p>
    Changes to privacy policy
</p>
<p>
    Any changes TezFS may make to our privacy policy in the future will be
    posted on this page and, where appropriate, notified to you when you next
    start the App or log onto one of the Services Sites. The new terms may be
    displayed on-screen and you may be required to read and accept them to
    continue your use of the App or the Services.
</p>
<p>
    Contact
</p>
<p>
    Questions, comments and requests regarding this privacy policy are welcomed
    and should be addressed to
    <a
        href="mailto:support@tezfinancialservices.pk"
        title="mailto:support@tezfinancialservices.pk"
    >
        support@tezfinancialservices.pk
    </a>
</p>