<div>
    <strong>Tez Advance Terms and Conditions</strong>
</div>
<p>
    <ol>
</p>
<p>
    <strong><li>Parties</li></strong>
</p>
<p>
    This Agreement is between “Tez Financial Services Limited” or “Tez” or
    “TezFS” or “TFS” and the borrower or user or customer (User Name).
</p>
<br>
<p>
    <strong><li>Definitions</li></strong>
</p>
<p>
    In these conditions;
</p>
<p>
    2.1 “Account holder” means the person liable to Tez for the
    settlement of Tez Advance, interests and other such costs and expenses as
    are agreed to by the Parties hereto.
</p>
<p>
    2.2 “Conditions” mean these terms and/or any other conditions that
    regulate or relate from time to time to the availing of Tez Advance by the
    borrower.
</p>
<p>
    2.3 “Due Date” means XX weeks from the date of disbursement of Tez Advance.
</p>
<p>
    2.4 “Tez Advance” refers to the term Tez Advance given to the customer
    through the mobile phone or such other mode as shall from time to time be
    made available by Tez and in such amounts as shall be determined by Tez in
    its absolute discretion after a User makes a formal application.
</p>
<p>
    2.5 “Tez” means Tez Financial Services Limited incorporated under the laws
    of Pakistan which expression shall where the context so admits include its
    successors in title and assigns.
</p>
<p>
    2.6 “Operative Account” means the account maintained by the User with Tez
    Financial Services Limited in connection with any of its vertical
    transactions.
</p>
<p>
    2.7 Tez Financial Services Limited and the user shall together be referred
    to as the “Parties” and where the context requires individually as a
    “Party”.
</p>
<p>
    2.8 The masculine gender includes the feminine and vice versa.
</p>
<ol>
    <li>
        <p>
            <strong>Conditions Constituting Agreement</strong>
        </p>
    </li>
</ol>
<p>
    3.1 These Conditions as varied from time to time constitute the Agreement
    between the Tez user and Tez Financial Services Limited with regard to Tez
    Advance. These Conditions apply in addition to the General Terms and
    Conditions of operating an account with Tez Financial Services Limited.
</p>
<p>
    3.2 The Tez user, by accepting the Tez Advance Terms and Conditions in the
    mobile application, shall be deemed to have read, understood and agreed to
    be bound by these Terms and Conditions upon acknowledging their receipt as
    availed in the Tez Advance application process.
</p>
<p>
    3.3 The Tez user further confirms that he has considered the charges, and
    other fee levied by Tez Financial Services as specified in Condition X
    below and fully understands that failure to settle the Tez Advance on the
    due date shall result in penalties.
</p>
<ol>
    <li>
        <p>
            <strong>Product Usage Charges</strong>
        </p>
    </li>
</ol>
<ol>
    <li>
        <p>
            The charge of the Tez advance is based on individual risk profile,
            tenure and the amount of the Tez Advance. The charges of the Tez
            Advance are mentioned on the screen where user selects the amount
            of the Tez Advance.
        </p>
    </li>
</ol>
<ol>
    <li>
        <p>
            A processing fee of PKR 20 is applicable on each advance
            disbursement request. PKR 20 will be deducted upfront from the user
            advance amount and the remaining amount will be disbursed into the
            user’s mobile wallet.
        </p>
    </li>
</ol>
<ol>
    <li>
        <p>
            All other charges related to the usage of mobile wallet will be
            borne by the user.
        </p>
    </li>
</ol>
<ol>
    <li>
        <p>
            <strong>Late Payment Charges</strong>
        </p>
        <ol>
            <li>
                <p>
                    If Tez Advance is not repaid by the due date, a late
                    payment charges of 2.5% will be charged on the outstanding
                    principal amount.
                </p>
            </li>
        </ol>
    </li>
</ol>
<ol>
    <li>
        <p>
            <strong>Tez Advance Repayment</strong>
        </p>
        <ol>
            <li>
                <p>
                    The Tez Advance is repayable on the Due Date. Early
                    repayment is optional.
                </p>
            </li>
            <li>
                <p>
                    Should the Tez Advance not be paid on the Due Date, Tez
                    Financial Services Limited shall, though not obligated to,
                    demand from the user all monies, which may then or
                    thereafter be due and owing under these Tez Advance
                    Conditions, including but not limited to:
                </p>
                <ol>
                    <li>
                        <p>
                            All Tez Advance appraisal fees due, Tez Advance
                            appraisal fees on Tez Advance rolled over, interest
                            due on Tez Advance and rolled over principal due;
                        </p>
                    </li>
                    <li>
                        <p>
                            All legal and other costs, charges and expenses
                            which Tez Financial Services may pay or incur in
                            connection with these Tez Advance Conditions or the
                            recovery of any monies owing hereunder;
                        </p>
                    </li>
                    <li>
                        <p>
                            A fee to compensate Tez Financial Services for a
                            reasonable estimate of any loss incurred by Tez
                            Financial Services as a result of default to the
                            full extent permitted by law; and
                        </p>
                    </li>
                    <li>
                        <p>
                            All monies due and computed from the due date until
                            the repayment in full.
                        </p>
                    </li>
                </ol>
            </li>
        </ol>
    </li>
</ol>
<ol>
    <li>
        <p>
            <strong>INDEMNITY AND RECOVERY OF COSTS</strong>
        </p>
        <ol>
            <li>
                <p>
                    The user undertakes to indemnify and keep Tez Financial
                    Services Limited indemnified at all times against all
                    actions, claims, demands, liabilities, losses, damages,
                    costs, charges and expenses of whatever nature inclusive of
                    any legal costs and disbursements incurred by Tez Financial
                    Services Limited in obtaining payment of any monies due and
                    owing to Tez Financial Services Limited from the user. The
                    indemnity shall remain valid, subsisting and binding upon
                    the user not withstanding withdrawal and termination of the
                    contract.
                </p>
            </li>
            <li>
                <p>
                    Any legal costs and disbursements incurred by Tez Financial
                    Services Limited against the user shall be deemed to
                    include every sum which would be allowed to the Advocates
                    of Tez Financial Services Limited in taxation between the
                    Advocate and clients to the intent that the user shall
                    afford to Tez Financial Services Limited a complete
                    entitlement and unqualified indemnity in respect thereof.
                </p>
            </li>
        </ol>
    </li>
    <li>
        <p>
            <strong>VARIATION</strong>
        </p>
        <ol>
            <li>
                <p>
                    Tez Financial Services Limited reserves the right to vary,
                    amend or replace all or any of these Conditions at any time
                    without prior notice. Tez Financial Services Limited shall
                    notify the User of any changes made to these Conditions as
                    soon as is practicable and by the most expedient means as
                    determined by Tez Financial Services Limited provided that
                    failure to make such notification shall not invalidate the
                    changes.
                </p>
            </li>
        </ol>
    </li>
    <li>
        <p>
            <strong>BREACH OF CONDITIONS</strong>
        </p>
        <ol>
            <li>
                <p>
                    In the event of any breach by the User of any of these
                    Conditions, Tez Financial Services Limited may in
                    circumstances where the User fails to comply or procure
                    compliance with the terms of a notice served by Tez
                    Financial Services Limited on the User, require immediate
                    repayment in full of the outstanding balance on the
                    Operative Account.
                </p>
            </li>
        </ol>
    </li>
    <li>
        <p>
            <strong>Tez Advance Suspension</strong>
        </p>
        <ol>
            <li>
                <p>
                    Tez Financial Services Limited may at any time and without
                    notice cancel or suspend the right to utilize Tez Advance
                    entirely or entirely withdraw Tez Advance as a product
                    without affecting the User’s obligations under these Tez
                    Advance Conditions.
                </p>
            </li>
        </ol>
    </li>
    <li>
        <p>
            <strong>Recovery</strong>
        </p>
    </li>
</ol>
<p>
    9.1 Upon the occurrence and during the continuance of default by the User,
    Tez Financial Services Limited is hereby authorized at any time and from
    time to time, without notice to Borrower and to the fullest extent
    permitted by law, to recover the outstanding amount through auto debit from
    the user’s mobile-wallet account held by the partner bank at the discretion
    of the user
</p>
<ol>
    <li>
        <p>
            <strong>TERMINATION</strong>
        </p>
        <ol>
            <li>
                <p>
                    Either Party may terminate their obligations under these
                    Conditions at any time on written notice to the other
                    Party. On termination by the user the termination notice
                    should be accompanied by the repayment of the full Tez
                    Advance outstanding at the time of receipt.
                </p>
            </li>
            <li>
                <p>
                    Termination shall only be effective upon the discharge of
                    all the outstanding liabilities under these Tez Advance
                    Conditions.
                </p>
            </li>
            <li>
                <p>
                    Tez Financial Services may cancel any Tez Advance on notice
                    from User accompanied by the payment of all sum outstanding
                    Tez Advance balance.
                </p>
            </li>
            <li>
                <p>
                    Tez Financial Services may at any time and without giving
                    reasons or notice terminate these Conditions and upon such
                    termination the user must repay the full outstanding Tez
                    Advance balance within the contracted period.
                </p>
            </li>
            <li>
                <p>
                    Termination by either the User or Tez Financial Services
                    shall not affect the User’s obligations to meet any
                    liabilities incurred prior to such termination. This
                    condition will continue until the user has repaid all
                    amounts outstanding at the time of termination.
                </p>
            </li>
        </ol>
    </li>
    <li>
        <p>
            <strong>Disclosure Of Information</strong>
        </p>
        <ol>
            <li>
                <p>
                    The User agrees that Tez Financial Services may disclose
                    details relating to Tez Advance to any third Party
                    (including credit bureaus) if in Tez’s opinion such
                    disclosure is necessary for the purpose of evaluating any
                    application made to Tez Financial Services or such third
                    Party or maintaining the Operative Account with Tez
                    Financial Services or other Purpose as Tez Financial
                    Services shall deem appropriate.
                </p>
            </li>
            <li>
                <p>
                    The user agrees that Tez Financial Services may disclose
                    details relating to Tez Advance including details of
                    default in servicing the Operative Account to any third
                    party (including credit bureaus) for the purpose of
                    evaluating the user’s credit worthiness or for any other
                    lawful purpose.
                </p>
            </li>
        </ol>
    </li>
    <li>
        <p>
            <strong>Governing Law And Jurisdiction</strong>
        </p>
        <ol>
            <li>
                <p>
                    These Tez Advance Conditions are governed in all respects
                    by the Laws of Islamic Republic of Pakistan.
                </p>
            </li>
            <li>
                <p>
                    Any dispute, difference or question whatsoever which may
                    arise between the Parties including the interpretation of
                    right and liabilities of either Party the same shall be
                    resolved through Arbitration in accordance with the
                    Arbitration Act, 1940 and any applicable rules made
                    thereunder. The arbitration shall be conducted by a sole
                    arbitrator to be mutually decided between the parties and
                    also the seat of the arbitration shall be in a place
                    mutually decided by the parties. The decision of such
                    arbitrator shall be final and binding on the Parties
                    hereto.
                </p>
            </li>
            <li>
                <p>
                    Neither Party shall be entitled to commence or maintain an
                    action in a Court of law upon any matter in dispute until
                    such matter shall have been submitted and determined as
                    herein before provided and then only for the enforcement of
                    the arbitration award or such matter as is permitted by
                    law.
                </p>
            </li>
        </ol>
    </li>
    <li>
        <p>
            <strong>Non-Assignment</strong>
        </p>
    </li>
</ol>
<p>
    The User may not assign or otherwise dispose of any of the user‘s rights
    and obligations under this Agreement. Tez Financial Services may transfer
    or assign its rights and obligations under these Conditions and such
    transfer or assignment shall be effective upon notification of the same to
    the user.
</p>
<ol>
    <li>
        <p>
            <strong>Notices And Change Of Address</strong>
        </p>
        <ol>
            <li>
                <p>
                    All notices to the user made under these Conditions shall
                    be sent by short message system (SMS) to the mobile
                    telephone number availed by the User or by prepaid post to
                    the last known postal address of the User whichever will be
                    deemed to be expedient by Tez Financial Services pursuant
                    to Condition
                </p>
            </li>
            <li>
                <p>
                    The user shall update their address in the profile page of
                    the Tez Mobile App or get it updated by calling the Tez
                    Financial Services Support Center.
                </p>
            </li>
        </ol>
    </li>
    <li>
        <p>
            <strong>Insurance</strong>
        </p>
        <ol>
            <li>
                <p>
                    The user shall be provided mandatory life insurance
                </p>
            </li>
            <li>
                <p>
                    The Tez Financial Services will arrange for such life
                    insurance.
                </p>
            </li>
        </ol>
    </li>
</ol>
<ol>
    <li>
        <p>
            <strong>THIRD PARTIES</strong>
        </p>
    </li>
</ol>
<p>
    The user expressly consents and allows Tez Financial Services to forward
    personal data and file credit information to licensed credit bureaus,
    insurance companies, mobile financial service providers/ banks and other
    partner companies of Tez Financial Services to render its services to the
    User.
</p>