package com.tez.androidapp.rewamp.signup.view;

import androidx.annotation.Nullable;

import com.tez.androidapp.commons.models.network.dto.request.UserLoginRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.signup.NewLoginPermissionsActivity;
import com.tez.androidapp.rewamp.signup.presenter.NewTermsAndConditionActivityPresenter;
import com.tez.androidapp.rewamp.signup.router.SignupLoginPermissionActivityRouter;

public class SignupLoginPermissionActivity extends NewLoginPermissionsActivity implements INewTermsAndConditionActivityView {

    @Override
    protected void initiateLogin() {
        showTezLoader();
        new NewTermsAndConditionActivityPresenter(this).callLogin(getPinFromIntent());
    }


    private String getNumberFromIntent() {
        return getIntent().getStringExtra(SignupLoginPermissionActivityRouter.MOBILE_NUMBER);
    }

    private String getPinFromIntent() {
        return getIntent().getStringExtra(SignupLoginPermissionActivityRouter.PIN);
    }

    @Override
    public void navigateToDashBoardActivity() {
        DashboardActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void startLogin() {
        //Empty
    }

    @Override
    public UserLoginRequest getLoginrequest(String pin) {
        return Utility.createLoginRequest(this, getNumberFromIntent(), pin);
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    protected String getScreenName() {
        return "SignupLoginPermissionActivity";
    }
}
