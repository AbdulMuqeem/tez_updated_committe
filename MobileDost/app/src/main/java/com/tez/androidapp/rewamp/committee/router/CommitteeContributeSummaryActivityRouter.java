package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;
import android.os.Bundle;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.response.CommitteeInstallmentPayResponse;
import com.tez.androidapp.rewamp.committee.view.CommitteeContributeSummaryActivity;

import androidx.annotation.NonNull;

public class CommitteeContributeSummaryActivityRouter extends BaseActivityRouter {


    public static final String INSTALLMENT_PAY_RESPONSE = "INSTALLMENT_PAY_RESPONSE";

    public static CommitteeContributeSummaryActivityRouter createInstance() {
        return new CommitteeContributeSummaryActivityRouter();
    }


    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CommitteeContributeSummaryActivity.class);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, CommitteeInstallmentPayResponse committeeInstallmentPayResponse) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putSerializable(INSTALLMENT_PAY_RESPONSE, committeeInstallmentPayResponse);
        intent.putExtras(bundle);
        route(from, intent);
    }


}
