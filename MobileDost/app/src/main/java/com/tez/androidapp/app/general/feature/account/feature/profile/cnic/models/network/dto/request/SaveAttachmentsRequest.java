package com.tez.androidapp.app.general.feature.account.feature.profile.cnic.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 2/15/2017.
 */

public class SaveAttachmentsRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/saveAttachments";

    public static final class Params {

        public static final String FRONT_CNIC_COPY = "frontCnicCopy";
        public static final String BACK_CNIC_COPY = "backCnicCopy";
        public static final String SELFIE = "selfie";

        private Params() {
        }
    }
}
