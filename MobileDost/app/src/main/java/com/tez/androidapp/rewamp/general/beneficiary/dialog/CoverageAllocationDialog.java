package com.tez.androidapp.rewamp.general.beneficiary.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezDialog;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.util.AmountFormatterTextWatcher;

import net.tez.fragment.util.optional.TextUtil;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import androidx.annotation.Nullable;

public class CoverageAllocationDialog extends TezDialog implements TextView.OnEditorActionListener {

    @BindView(R.id.cvBase)
    private TezCardView cvBase;

    @BindView(R.id.tllAmount)
    private TezLinearLayout tllAmount;

    @BindView(R.id.etAmount)
    private TezEditTextView etAmount;

    @BindView(R.id.tvMaxCoverage)
    private TezTextView tvMaxCoverage;

    @BindView(R.id.tvRs)
    private TezTextView tvRs;

    @Nullable
    private OnCoverageAmountEnteredListener onCoverageAmountEnteredListener;

    public CoverageAllocationDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coverage_allocation_dialog);
        ViewBinder.bind(this);
        if (getWindow() != null)
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        this.cancelTezDialogOnTouchOutsideOfView(cvBase);
        tvMaxCoverage.setText(getContext().getString(R.string.maximum_coverage_amount_rs_num, Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(100000)));
        tllAmount.setOnClickListener(view -> Utility.showKeyboard(etAmount));
        etAmount.setCursorToLastPositionAlways();
        etAmount.disableCopyPaste();
        etAmount.addTextChangedListener(new AmountFormatterTextWatcher(tvRs, etAmount));
        etAmount.setOnEditorActionListener(this);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE && onCoverageAmountEnteredListener != null && TextUtil.isNotEmpty(etAmount.getValueText()))
            onCoverageAmountEnteredListener.onCoverageAmountEntered((int) Utility.getPatternAmount(String.valueOf(Utility.parseAmount(etAmount.getValueText()))));
        dismiss();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dismiss();
    }

    public void setOnCoverageAmountEnteredListener(@Nullable OnCoverageAmountEnteredListener onCoverageAmountEnteredListener) {
        this.onCoverageAmountEnteredListener = onCoverageAmountEnteredListener;
    }

    public interface OnCoverageAmountEnteredListener {

        void onCoverageAmountEntered(int amount);
    }
}
