package com.tez.androidapp.rewamp.advance.request.presenter;

import android.view.View;

import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.rewamp.advance.request.interactor.ILoanReceiptActivityInteractor;
import com.tez.androidapp.rewamp.advance.request.interactor.LoanReceiptActivityInteractor;
import com.tez.androidapp.rewamp.advance.request.view.ILoanReceiptActivityView;

public class LoanReceiptActivityPresenter implements ILoanReceiptActivityPresenter, ILoanReceiptActivityInteractorOutput {

    private final ILoanReceiptActivityView iLoanReceiptActivityView;
    private final ILoanReceiptActivityInteractor iLoanReceiptActivityInteractor;

    public LoanReceiptActivityPresenter(ILoanReceiptActivityView iLoanReceiptActivityView) {
        this.iLoanReceiptActivityView = iLoanReceiptActivityView;
        iLoanReceiptActivityInteractor = new LoanReceiptActivityInteractor(this);
    }

    @Override
    public void generateDisbursementReceipt() {
        iLoanReceiptActivityView.setClContentVisibility(View.GONE);
        iLoanReceiptActivityView.showTezLoader();
        iLoanReceiptActivityInteractor.generateDisbursementReceipt();
    }

    @Override
    public void onGenerateDisbursementReceiptSuccess(LoanDetails loanDetails) {
        iLoanReceiptActivityView.initValues(loanDetails);
        iLoanReceiptActivityView.setClContentVisibility(View.VISIBLE);
        iLoanReceiptActivityView.dismissTezLoader();
    }

    @Override
    public void onGenerateDisbursementReceiptFailure(int errorCode, String message) {
        iLoanReceiptActivityView.dismissTezLoader();
        iLoanReceiptActivityView.showError(errorCode, (dialog, which) -> iLoanReceiptActivityView.routeToDashboard());
    }
}
