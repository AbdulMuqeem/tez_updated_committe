package com.tez.androidapp.commons.utils.authorized.alert.image.popup;

import android.graphics.Bitmap;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.EmptySignature;
import com.bumptech.glide.signature.ObjectKey;
import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.utils.file.util.GlideApp;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;

/**
 * Created by FARHAN DHANANI on 7/8/2018.
 */
public class DefaultAuthorizeImageLoader
        extends BaseRH<BaseResponse>
        implements AuthorizedImageLoader {

    private UIForAlertImage customUIForAlertImage;
    private Uri url;
    private String signature;

    DefaultAuthorizeImageLoader() {
        super(new BaseCloudDataStore());
    }

    @Override
    public void setUrlToLoad(@NonNull Uri urlToLoad) {
        this.url = urlToLoad;
    }

    @Override
    public void setSignatureToFile(@Nullable String signature) {
        this.signature = signature;
    }

    @Override
    public String getSignature() {
        return signature;
    }

    @Override
    public void loadImage() {
        if (this.url != null && !Utility.isEmpty(url.toString()) && this.customUIForAlertImage != null) {
            customUIForAlertImage.onInitiate();
            RequestOptions requestOptions = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .signature(getSignature() != null ? new ObjectKey(getSignature()) : EmptySignature.obtain())
                    .placeholder(customUIForAlertImage.getPlaceholder())
                    .error(customUIForAlertImage.getErrorImageDrawable())
                    .transform(customUIForAlertImage.getTransformation())
                    .encodeQuality(70);
            GlideApp.with(MobileDostApplication.getInstance().getApplicationContext())
                    .asBitmap()
                    .load(this.url)
                    .apply(requestOptions)
                    .dontAnimate()
                    .listener(new RequestListener<Bitmap>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                            if (e != null && e.getMessage().contains(Constants.STRING_UNAUTHORIZED)) {
                                onImageLoadFailedTokenRefresh();
                            } else
                                customUIForAlertImage.onError();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                            customUIForAlertImage.onSuccess();
                            return false;
                        }
                    })
                    .into(this.customUIForAlertImage.getTarget());

        }
    }

    @Override
    public void setUI(@NonNull UIForAlertImage uiForAlertImage) {
        this.customUIForAlertImage = uiForAlertImage;
    }


    private void onImageLoadFailedTokenRefresh() {
        UserCloudDataStore.getInstance().tokenRefresh(this);
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        //Left
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (Utility.isUnauthorized(errorCode)) {
            this.loadImage();
        }
    }
}
