package com.tez.androidapp.rewamp.signup;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezToolbar;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public abstract class BaseOnboardingActivity extends BaseActivity {

    @BindView(R.id.btMainButton)
    protected TezButton btMainButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_onboarding);
        ViewBinder.bind(this);
        init();
        TezToolbar onboardingToolbar = findViewById(R.id.onboardingToolbar);
        initViews(onboardingToolbar);
    }

    private void init() {
        ViewPager viewPager = findViewById(R.id.viewPagerOnboarding);
        TabLayout tabLayout = findViewById(R.id.tabLayoutDots);
        OnboardingPagerAdapter adapter = new OnboardingPagerAdapter(getOnboardingContentList(), getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    protected abstract void initViews(TezToolbar onboardingToolbar);

    @NonNull
    protected abstract List<OnboardingContent> getOnboardingContentList();
}
