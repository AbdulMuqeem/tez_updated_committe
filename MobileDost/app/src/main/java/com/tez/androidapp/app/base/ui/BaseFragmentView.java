package com.tez.androidapp.app.base.ui;

import androidx.annotation.NonNull;

import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;

/**
 * Created by FARHAN DHANANI on 9/13/2018.
 */
public interface BaseFragmentView {

    void getCurrentLocation(@NonNull LocationAvailableCallback callback);

    boolean onBackPressed();

}
