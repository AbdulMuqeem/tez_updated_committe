package com.tez.androidapp.rewamp.bima.products.interactor;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BimaCloudDataStore;
import com.tez.androidapp.rewamp.bima.claim.callback.InitiateClaimCallback;
import com.tez.androidapp.rewamp.bima.claim.request.InitiateClaimRequest;
import com.tez.androidapp.rewamp.bima.products.callback.ProductPolicyCallback;
import com.tez.androidapp.rewamp.bima.products.response.ProductPolicyResponse;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProductPolicyActivityInteractor implements IProductPolicyActivityInteractor {

    private final IProductPolicyActivityInteractorOutput iProductPolicyActivityInteractorOutput;

    public ProductPolicyActivityInteractor(IProductPolicyActivityInteractorOutput iProductPolicyActivityInteractorOutput) {
        this.iProductPolicyActivityInteractorOutput = iProductPolicyActivityInteractorOutput;
    }

    @Override
    public void getProductPolicy(int policyId) {
        BimaCloudDataStore.getInstance().getProductPolicy(policyId, new ProductPolicyCallback() {
            @Override
            public void onGetProductPolicySuccess(ProductPolicyResponse productPolicyResponse) {
                iProductPolicyActivityInteractorOutput.onGetProductPolicySuccess(productPolicyResponse);
            }

            @Override
            public void onGetProductPolicyFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getProductPolicy(policyId);
                else
                    iProductPolicyActivityInteractorOutput.onGetProductPolicyFailure(errorCode, message);
            }
        });
    }

    @Override
    public void initiateClaim(InitiateClaimRequest initiateClaimRequest) {
        BimaCloudDataStore.getInstance().initiateClaim(initiateClaimRequest, new InitiateClaimCallback() {
            @Override
            public void onSuccessInitiateClaim(@NotNull BaseResponse response) {
                iProductPolicyActivityInteractorOutput.onSuccessInitiateClaim(response);
            }

            @Override
            public void onFailureInitiateClaim(int errorCode, @Nullable String message) {
                if (Utility.isUnauthorized(errorCode))
                    initiateClaim(initiateClaimRequest);
                else
                    iProductPolicyActivityInteractorOutput.onFailureInitiateClaim(errorCode, message);
            }
        });
    }
}
