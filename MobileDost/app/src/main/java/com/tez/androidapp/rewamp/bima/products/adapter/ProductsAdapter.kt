package com.tez.androidapp.rewamp.bima.products.adapter

import android.view.View
import android.view.ViewGroup
import com.tez.androidapp.R
import com.tez.androidapp.commons.utils.app.Utility
import com.tez.androidapp.rewamp.bima.products.adapter.ProductsAdapter.ProductListener
import com.tez.androidapp.rewamp.bima.products.adapter.ProductsAdapter.ProductViewHolder
import com.tez.androidapp.rewamp.bima.products.model.InsuranceProduct
import kotlinx.android.synthetic.main.list_item_product.view.*
import net.tez.fragment.util.base.IBaseWidget
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener
import net.tez.tezrecycler.base.viewholder.BaseViewHolder

class ProductsAdapter(items: List<InsuranceProduct>, listener: ProductListener?)
    : GenericRecyclerViewAdapter<InsuranceProduct, ProductListener?, ProductViewHolder>(items, listener) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view = inflate(parent.context, R.layout.list_item_product, parent)
        return ProductViewHolder(view)
    }

    interface ProductListener : BaseRecyclerViewListener {
        fun onPurchaseProduct(insuranceProduct: InsuranceProduct)
        fun onGetProductDetails(insuranceProduct: InsuranceProduct)
    }

    class ProductViewHolder constructor(itemView: View) : BaseViewHolder<InsuranceProduct, ProductListener?>(itemView) {

        override fun onBind(item: InsuranceProduct, listener: ProductListener?) {
            super.onBind(item, listener)
            val etGrey = Utility.getColorFromResource(R.color.editTextBottomLineColorGrey)
            val tvBlue = Utility.getColorFromResource(R.color.textViewTitleColorBlue)
            val tvBlack = Utility.getColorFromResource(R.color.textViewTextColorBlack)
            val tvGreen = Utility.getColorFromResource(R.color.textViewTextColorGreen)

            itemView.run {
                ivIcon.setImageResource(Utility.getInsuranceProductIcon(item.id))
                tvTitle.text = item.title
                tvDescription.text = item.policyTagLine
                ivArrow.visibility = if (item.disabled) View.GONE else View.VISIBLE
                ivVerticalLine.setColorFilter(if (item.disabled) etGrey else tvGreen)
                ivIcon.setColorFilter(if (item.disabled) etGrey else tvBlue)
                tvTitle.setTextColor(if (item.disabled) etGrey else tvBlue)
                tvDescription.setTextColor(if (item.disabled) etGrey else tvBlack)
                rootCv.attachClickEffect(if (item.disabled) IBaseWidget.NO_EFFECT else IBaseWidget.SCALE_EFFECT, rootCv)
            }
            setItemViewOnClickListener(
                    if (listener != null && !item.disabled)
                        DoubleTapSafeOnClickListener {
                            if (item.productPurchased)
                                listener.onGetProductDetails(item)
                            else
                                listener.onPurchaseProduct(item)
                        } else null)
        }
    }
}