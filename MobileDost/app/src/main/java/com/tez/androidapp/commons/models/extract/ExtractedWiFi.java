package com.tez.androidapp.commons.models.extract;

import android.net.wifi.WifiConfiguration;

/**
 * Created  on 12/8/2016.
 */

public class ExtractedWiFi {
    private String SSID;
    private String name;
    private String isAuthenticated;

    public ExtractedWiFi(String ssid, String name, String isAuthenticated) {
        this.SSID = ssid;
        this.name = name;
        this.isAuthenticated = isAuthenticated;
    }

    public ExtractedWiFi(WifiConfiguration wifiConfiguration) {
        this.SSID = wifiConfiguration.SSID;
        this.name = wifiConfiguration.SSID;
        this.isAuthenticated = wifiConfiguration.toString();
    }

    public String getSSID() {
        return SSID;
    }

    public void setSSID(String ssid) {
        this.SSID = ssid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsAuthenticated() {
        return isAuthenticated;
    }

    public void setIsAuthenticated(String isAuthenticated) {
        this.isAuthenticated = isAuthenticated;
    }
}
