package com.tez.androidapp.rewamp.general.network.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.tez.androidapp.rewamp.general.network.model.NetworkCall;

public abstract class NetworkBoundViewModel extends ViewModel {


    protected final MutableLiveData<NetworkCall> networkCallMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<NetworkCall> getNetworkCallMutableLiveData() {
        return networkCallMutableLiveData;
    }
}
