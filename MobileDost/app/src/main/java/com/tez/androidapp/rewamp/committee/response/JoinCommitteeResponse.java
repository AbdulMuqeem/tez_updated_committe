package com.tez.androidapp.rewamp.committee.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.tez.androidapp.app.base.response.BaseResponse;

import java.io.Serializable;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class JoinCommitteeResponse extends BaseResponse implements Serializable, Parcelable {

    private String message;
    private String startDate;

    protected JoinCommitteeResponse(Parcel in) {
        message = in.readString();
        startDate = in.readString();
    }

    public static final Creator<JoinCommitteeResponse> CREATOR = new Creator<JoinCommitteeResponse>() {
        @Override
        public JoinCommitteeResponse createFromParcel(Parcel in) {
            return new JoinCommitteeResponse(in);
        }

        @Override
        public JoinCommitteeResponse[] newArray(int size) {
            return new JoinCommitteeResponse[size];
        }
    };

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
        dest.writeString(startDate);
    }
}



