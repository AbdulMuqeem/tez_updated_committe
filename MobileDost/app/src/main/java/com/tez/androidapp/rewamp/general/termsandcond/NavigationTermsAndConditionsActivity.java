package com.tez.androidapp.rewamp.general.termsandcond;

import android.view.View;

import androidx.annotation.NonNull;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.common.BaseTermsAndConditionActivity;
import com.tez.androidapp.rewamp.signup.TermsAndCondition;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VINOD KUMAR on 8/26/2019.
 */
public class NavigationTermsAndConditionsActivity extends BaseTermsAndConditionActivity implements NavigationTermsAdapter.NavigationTermListener {

    private DetailedTermConditionDialog detailedTermConditionDialog;

    @Override
    protected void setupViews() {
        tezCheckBox.setVisibility(View.GONE);
        btMainButton.setVisibility(View.GONE);
        textViewDescription.setVisibility(View.GONE);
    }

    @Override
    protected void setAdapter() {
        NavigationTermsAdapter adapter = new NavigationTermsAdapter(getNavigationTerms(), this);
        recyclerViewTermsAndCondition.setAdapter(adapter);
    }

    private List<NavigationTerm> getNavigationTerms() {
        List<NavigationTerm> termsAndConditionList = new ArrayList<>();
        termsAndConditionList.add(new NavigationTerm("terms_and_conditions.txt", getString(R.string.tez_t_and_c)));
        termsAndConditionList.add(new NavigationTerm("privacy_policy.txt", getString(R.string.privacy_policies)));
        termsAndConditionList.add(new NavigationTerm("loan_terms.txt", getString(R.string.tez_advance_terms)));
        termsAndConditionList.add(new NavigationTerm("loan_bima.txt", getString(R.string.tez_bima_terms)));
        return termsAndConditionList;
    }

    @Override
    protected List<TermsAndCondition> getTermAndConditions() {
        return null;
    }

    @Override
    protected void onClickBtMainButton() {

    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    public void onClickTerm(@NonNull String fileName) {
        if (detailedTermConditionDialog == null || !detailedTermConditionDialog.isShowing()) {
            detailedTermConditionDialog = new DetailedTermConditionDialog(this);
            detailedTermConditionDialog.setFileName(fileName);
            detailedTermConditionDialog.show();
        }
    }

    @Override
    protected String getScreenName() {
        return "NavigationTermsAndConditionsActivity";
    }
}
