package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.signature.ObjectKey;
import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.authorized.alert.image.popup.DefaultUIForAlertImage;
import com.tez.androidapp.commons.utils.authorized.alert.image.popup.RenderAlertImageOnUI;
import com.tez.androidapp.rewamp.TezDrawableHelper;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.UUID;

/**
 * Created by VINOD KUMAR on 8/2/2019.
 */
public class CnicReviewCardView extends TezConstraintLayout {

    @BindView(R.id.ivUploadedImage)
    private TezImageView ivUploadedImage;

    @BindView(R.id.ivEditPicture)
    private TezImageView ivEditPicture;

    @BindView(R.id.pbImageLoad)
    private ProgressBar pbImageLoad;

    @BindView(R.id.viewOverlay)
    private View viewOverlay;

    @BindView(R.id.ivError)
    private TezImageView ivError;

    @BindView(R.id.tvDescription)
    private TezTextView tvDescription;

    @BindView(R.id.tvUploadAgain)
    private TezTextView tvUploadAgain;

    @BindView(R.id.tcParentView)
    private TezConstraintLayout tcParentView;

    public CnicReviewCardView(@NonNull Context context) {
        super(context);
    }

    public CnicReviewCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public CnicReviewCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    private void init(@NonNull Context context, @NonNull AttributeSet attrs) {
        inflate(context, R.layout.cnic_review_card, this);
        attachClickEffect(NO_EFFECT, this);
        ViewBinder.bind(this, this );
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CnicReviewCardView);

        try {

            String text = typedArray.getString(R.styleable.CnicReviewCardView_cnic_side_text);
            Drawable drawable = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.CnicReviewCardView_placeholder_image);
            this.tvDescription.setText(text);
            this.ivUploadedImage.setImageDrawable(drawable);

        } finally {
            typedArray.recycle();
        }
    }

    public void setOnEditPictureClickListener(@Nullable DoubleTapSafeOnClickListener listener) {
        ivEditPicture.setOnClickListener(listener);
    }

    public void setVisibillityForIvEditPicture(int visibllity){
        this.ivEditPicture.setVisibility(visibllity);
    }

    public void setImageLoaderVisibility(boolean visibility) {
        this.pbImageLoad.setVisibility(visibility ? VISIBLE : GONE);
        this.viewOverlay.setVisibility(visibility ? VISIBLE : GONE);
        this.ivError.setVisibility(GONE);
    }

    public void setRejected(boolean isRejected){
        if(isRejected) {
            this.tcParentView.setBackgroundResource(R.drawable.cnic_filled_rect_red_bg);
            this.ivEditPicture.setBackgroundResource(R.drawable.cnic_review_card_pencil_rejected_bg);
            this.ivUploadedImage.setBackgroundResource(R.drawable.cnic_filled_rect_red_bg);
            this.tvUploadAgain.setVisibility(VISIBLE);
        } else {
            this.tcParentView.setBackgroundResource(R.color.transparent);
            this.ivEditPicture.setBackgroundResource(R.drawable.cnic_review_card_pencil_bg);
            this.ivUploadedImage.setBackgroundResource(R.drawable.cnic_filled_rect_green_bg);
            this.tvUploadAgain.setVisibility(GONE);
        }
    }

    public void setUploadedImage(@NonNull String path, @DrawableRes int placeholderId) {
        Uri serverUri = Uri.parse(path);
        String signature = UUID.randomUUID().toString();
        DefaultUIForAlertImage uiForAlertImage = new DefaultUIForAlertImage(this.ivUploadedImage){
            @Override
            public void onInitiate() {
                super.onInitiate();
                setImageLoaderVisibility(true);
                ivError.setVisibility(GONE);
                ivError.setDoubleTapSafeOnClickListener(null);
            }

            @Override
            public void onSuccess() {
                super.onSuccess();
                setImageLoaderVisibility(false);
                ivError.setVisibility(GONE);
                ivError.setDoubleTapSafeOnClickListener(null);
                tvDescription.setTextColor(getColor(getContext(), R.color.textViewTextColorBlack));
                tvDescription.setTypeface(ResourcesCompat.getFont(getContext(), R.font.barlow_medium));
            }

            @Override
            public void onError() {
                 super.onError();
                setImageLoaderVisibility(false);
                ivError.setVisibility(VISIBLE);
                viewOverlay.setVisibility(VISIBLE);
                ivError.setDoubleTapSafeOnClickListener(v-> setUploadedImage(path, placeholderId));
            }
        };
        uiForAlertImage.setErrorImage(placeholderId);
        uiForAlertImage.setPlaceholder(placeholderId);
        uiForAlertImage.setTransformation(new CenterCrop(), new RoundedCorners(dpToPx(getContext(), 4)));
        RenderAlertImageOnUI.renderDefaultImageUIAlert(uiForAlertImage, serverUri, signature);
    }

    private void loadImage(TezImageView imageView, @NonNull String path) {
        Glide.with(getContext())
                .load(path)
                .transform(new CenterCrop(), new RoundedCorners(dpToPx(getContext(), 4)))
                .into(imageView);
    }
}
