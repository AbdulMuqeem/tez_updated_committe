package com.tez.androidapp.rewamp.signup.view;

import com.tez.androidapp.app.base.ui.IBaseView;

public interface IIntroActivityView extends IBaseView {

    void navigateToLoginActivity(String mobileNumber, String socialId, int socialType);

    void navigateToSignUpActivity(String mobileNumber, String socialId, int socialType);

}
