package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;

import androidx.annotation.DrawableRes;

import com.tez.androidapp.R;

/**
 * Created by VINOD KUMAR on 5/22/2019.
 */
public class TezAudioTextView extends TezTextView {

    public TezAudioTextView(Context context) {
        super(context);
    }

    public TezAudioTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public TezAudioTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
   /*     this.changeDrawable(R.drawable.ic_speaker_off);
        this.setCompoundDrawablePadding(dpToPx(context, 8));
        this.setIncludeFontPadding(false);*/
        this.setGravity(Gravity.CENTER);
    }

    public void changeDrawable(@DrawableRes int rightEnd) {
        this.changeDrawable(this, 0, 0, rightEnd, 0);
    }
}
