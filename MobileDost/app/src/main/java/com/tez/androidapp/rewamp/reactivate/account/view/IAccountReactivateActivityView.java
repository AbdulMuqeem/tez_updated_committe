package com.tez.androidapp.rewamp.reactivate.account.view;

import com.tez.androidapp.app.base.ui.IBaseView;

public interface IAccountReactivateActivityView extends IBaseView {
    void setPinViewOtpEnabled(boolean enabled);

    void setTvResendCodeEnabled(boolean enabled);

    void setPinViewOtpClearText();

    void startTimer();

    void navigateToDashboard();

    void registerSmsReceiver();
}
