package com.tez.androidapp.rewamp.advance.request.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.advance.request.callback.ProcessingFeesCallback;

import net.tez.fragment.util.optional.Optional;

public class ProcessingFeesRH extends BaseRH<ProcessingFeesResponse> {

    private ProcessingFeesCallback processingFeesCallback;

    public ProcessingFeesRH(BaseCloudDataStore baseCloudDataStore,
                            ProcessingFeesCallback processingFeesCallback) {
        super(baseCloudDataStore);
        this.processingFeesCallback = processingFeesCallback;
    }

    @Override
    protected void onSuccess(Result<ProcessingFeesResponse> value) {
        Optional.ifPresent(value.response().body(), processingFeesResponse -> {
            Optional.doWhen(processingFeesResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR,
                    () -> this.processingFeesCallback.onProcessingFeesSuccess(processingFeesResponse.getProcessingFees()),
                    () -> this.onFailure(processingFeesResponse.getStatusCode(), processingFeesResponse.getErrorDescription()));
        }, this::sendDefaultFailure);
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        processingFeesCallback.onProcessingFeesFailure(errorCode, message);
    }
}
