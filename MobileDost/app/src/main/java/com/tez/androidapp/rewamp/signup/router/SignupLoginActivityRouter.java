package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.view.SignupLoginActivity;

public class SignupLoginActivityRouter extends BaseActivityRouter {

    public static final String MOBILE_NUMBER = "MOBILE_NUMBER";

    public static SignupLoginActivityRouter createInstance() {
        return new SignupLoginActivityRouter();
    }


    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        @NonNull String mobileNumber) {

        Intent intent = createIntent(from);
        intent.putExtra(MOBILE_NUMBER, mobileNumber);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, SignupLoginActivity.class);
    }
}
