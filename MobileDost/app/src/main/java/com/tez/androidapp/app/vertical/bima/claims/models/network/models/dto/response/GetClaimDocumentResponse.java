package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response;
import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created by FARHAN DHANANI on 7/2/2018.
 */
public class GetClaimDocumentResponse extends BaseResponse {

    private byte[] claimDocument;
    private Integer docId;

    public byte[] getClaimDocument() {
        return claimDocument;
    }

    public void setClaimDocument(byte[] claimDocument) {
        this.claimDocument = claimDocument;
    }

    public Integer getDocId(){
        return docId;
    }

}
