package com.tez.androidapp.app.vertical.advance.loan.questions.callbacks;

import com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.LoanQuestion;

/**
 * Created  on 2/17/2017.
 */

public interface LoanQuestionsCallback {

    void onLoanQuestionsSuccess(LoanQuestion loanQuestions);

    void onLoanQuestionsFailure(int errorCode, String message);
}
