package com.tez.androidapp.rewamp.committee.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class CommitteeLoginRequest extends BaseRequest {

    public static final String METHOD_NAME = "http://3.21.127.35/api/v2/user/login";

    @SerializedName("principalName")
    @Expose
    public String principalName;

    @SerializedName("password")
    @Expose
    public String password;

    @SerializedName("imei")
    @Expose
    public String imei;

    @SerializedName("deviceBrand")
    @Expose
    public String deviceBrand;

    @SerializedName("deviceModel")
    @Expose
    public String deviceModel;

    @SerializedName("networkOperator")
    @Expose
    public String networkOperator;

    @SerializedName("deviceOs")
    @Expose
    public String deviceOs;

    @SerializedName("defaultBrowser")
    @Expose
    public String defaultBrowser;

    @SerializedName("lat")
    @Expose
    public double lat;

    @SerializedName("lng")
    @Expose
    public double lng;

    @SerializedName("languageCode")
    @Expose
    public String languageCode;

    @SerializedName("deviceKey")
    @Expose
    public String deviceKey;

    @SerializedName("appVersion")
    @Expose
    public String appVersion;


}
