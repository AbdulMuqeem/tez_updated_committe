package com.tez.androidapp.rewamp.advance.request.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.request.view.WaitForDisbursementActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

public class WaitForDisbursementActivityRouter extends BaseActivityRouter {

    public static WaitForDisbursementActivityRouter createInstance() {
        return new WaitForDisbursementActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, WaitForDisbursementActivity.class);
    }
}
