package com.tez.androidapp.rewamp.bima.claim.revise.viewmodel

import androidx.lifecycle.MutableLiveData
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.Evidence
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.EvidenceFile
import com.tez.androidapp.commons.utils.app.Utility
import com.tez.androidapp.commons.utils.file.util.FileUtil
import com.tez.androidapp.repository.network.store.BimaCloudDataStore
import com.tez.androidapp.rewamp.bima.claim.callback.ConfirmClaimCallback
import com.tez.androidapp.rewamp.bima.claim.callback.UploadDocumentCallback
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Failure
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Loading
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Success
import com.tez.androidapp.rewamp.bima.claim.health.viewmodel.EvidenceViewModel
import com.tez.androidapp.rewamp.bima.claim.request.ConfirmReviseClaimRequest
import com.tez.androidapp.rewamp.bima.claim.request.UploadDocumentRequest
import com.tez.androidapp.rewamp.bima.claim.response.ConfirmClaimResponse
import com.tez.androidapp.rewamp.bima.claim.response.UploadDocumentResponse
import okhttp3.MediaType
import okhttp3.MultipartBody
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class ReviseClaimEvidenceViewModel : EvidenceViewModel() {

    private val evidenceFilesQueue: Queue<EvidenceFile> = LinkedList()
    private val uploadedDocumentIds: MutableList<Int> = ArrayList()
    val confirmReviseClaimLiveData: MutableLiveData<ConfirmClaimResponse> = MutableLiveData()

    fun initiateQueue(evidenceList: List<Evidence>) {
        evidenceFilesQueue.clear()
        uploadedDocumentIds.clear()
        evidenceList.forEach {
            evidenceFilesQueue.addAll(it.evidenceFileList)
        }
    }

    fun resubmitClaim(claimId: Int) = uploadDocument(claimId)

    private fun uploadDocument(claimId: Int) {
        networkCallMutableLiveData.value = Loading
        if (evidenceFilesQueue.isEmpty()) {
            confirmReviseClaim(ConfirmReviseClaimRequest(claimId, uploadedDocumentIds))
            return
        }
        val evidenceFile = evidenceFilesQueue.peek()!!
        val file = File(evidenceFile.path)
        if (file.extension != "pdf")
            Utility.compressFile(file, 70)
        val name = "${System.currentTimeMillis()}.${file.extension}"
        val data = MultipartBody.Part.createFormData(UploadDocumentRequest.ATTACHMENT,
                name,
                MultipartBody.create(MediaType.parse(Utility.getMimeTypeFromLocalFileUri(file.absolutePath)), file))

        val claimIdPart = MultipartBody.Part.createFormData(UploadDocumentRequest.CLAIM_ID, claimId.toString())
        val categoryIdPart = MultipartBody.Part.createFormData(UploadDocumentRequest.CATEGORY_ID, evidenceFile.categoryId.toString())

        BimaCloudDataStore.getInstance().uploadDocument(data, claimIdPart, categoryIdPart,
                object : UploadDocumentCallback {
                    override fun onSuccessUploadDocument(uploadDocumentResponse: UploadDocumentResponse) {
                        evidenceFilesQueue.poll()
                        uploadedDocumentIds.add(uploadDocumentResponse.data!!)
                        uploadDocument(claimId)
                    }

                    override fun onFailureUploadDocument(errorCode: Int, message: String?) {
                        if (Utility.isUnauthorized(errorCode))
                            uploadDocument(claimId)
                        else
                            networkCallMutableLiveData.value = Failure(errorCode)
                    }
                })
    }

    private fun confirmReviseClaim(request: ConfirmReviseClaimRequest) {
        BimaCloudDataStore.getInstance().confirmReviseClaim(request, object : ConfirmClaimCallback {
            override fun onSuccessConfirmClaim(confirmClaimResponse: ConfirmClaimResponse) {
                FileUtil.deleteClaimFiles()
                networkCallMutableLiveData.value = Success
                confirmReviseClaimLiveData.value = confirmClaimResponse
            }

            override fun onFailureConfirmClaim(errorCode: Int, message: String?) {
                if (Utility.isUnauthorized(errorCode))
                    confirmReviseClaim(request)
                else
                    networkCallMutableLiveData.value = Failure(errorCode)
            }
        })
    }
}