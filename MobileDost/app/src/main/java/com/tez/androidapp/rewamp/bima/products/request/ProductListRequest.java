package com.tez.androidapp.rewamp.bima.products.request;

import com.tez.androidapp.app.base.request.BaseRequest;

public class ProductListRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/insurance/products";
}
