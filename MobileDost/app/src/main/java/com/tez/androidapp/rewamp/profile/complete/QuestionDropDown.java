package com.tez.androidapp.rewamp.profile.complete;

import java.util.List;

/**
 * Created by Rehman Murad Ali
 **/
public class QuestionDropDown {

    private String question;
    private List<String> options;

    public QuestionDropDown(String question, List<String> options) {
        this.question = question;
        this.options = options;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public String getQuestion() {
        return question;
    }

    public List<String> getOptions() {
        return options;
    }
}
