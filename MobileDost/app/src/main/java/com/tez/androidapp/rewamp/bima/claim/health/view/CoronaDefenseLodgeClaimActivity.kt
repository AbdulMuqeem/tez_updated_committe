package com.tez.androidapp.rewamp.bima.claim.health.view

import androidx.navigation.NavDirections
import com.tez.androidapp.R
import com.tez.androidapp.commons.widgets.StepperView
import com.tez.androidapp.rewamp.bima.claim.common.base.BaseClaimLodgeActivity
import com.tez.androidapp.rewamp.bima.claim.common.document.view.EvidenceFragmentDirections

class CoronaDefenseLodgeClaimActivity : BaseClaimLodgeActivity() {

    override fun initNavDirections(): List<NavDirections> = listOfNotNull(
            AmountFragmentDirections.actionAmountFragmentToDetailsFragment(2,
                    dependencies.activationDate,
                    dependencies.expiryDate),
            DetailsFragmentDirections.actionDetailsFragmentToEvidenceFragment(3),
            EvidenceFragmentDirections.actionEvidenceFragmentToSummaryFragment(4,
                    dependencies.policyId)
    )

    override fun initStepperList(): List<StepperView.Step> = listOfNotNull(
            StepperView.Step(1, R.string.amount, R.drawable.ic_amount_active),
            StepperView.Step(2, R.string.details, R.drawable.ic_information_active),
            StepperView.Step(3, R.string.evidence, R.drawable.ic_evidence_active),
            StepperView.Step(4, R.string.summary, R.drawable.ic_summary_active)
    )

    override fun getScreenName(): String = "CoronaDefenseLodgeClaimActivity"
}