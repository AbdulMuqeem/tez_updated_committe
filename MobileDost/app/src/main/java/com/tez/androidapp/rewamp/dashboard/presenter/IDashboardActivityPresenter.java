package com.tez.androidapp.rewamp.dashboard.presenter;

public interface IDashboardActivityPresenter {

    void setDashboard();

    void setDashboardFromPreference();
}
