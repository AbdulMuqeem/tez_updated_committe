package com.tez.androidapp.rewamp.profile.trust.view;

import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;

public class CompleteYourProfileActivity extends ProfileTrustActivity {

    @Override
    public void onBackPressed() {
        DashboardActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this);
    }

    @Override
    protected String getScreenName() {
        return "CompleteYourProfileActivity";
    }
}
