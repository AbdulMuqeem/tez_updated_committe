package com.tez.androidapp.repository.network.store;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.general.feature.questions.callbacks.ClaimQuestionsCallback;
import com.tez.androidapp.app.general.feature.questions.handers.ClaimQuestionsRH;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks.SetBeneficiaryCallback;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks.SetDefaultBeneficiaryCallback;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.handlers.SetBeneficiaryRH;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.handlers.SetDefaultBeneficiaryRH;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.models.network.dto.request.BeneficiaryRequest;
import com.tez.androidapp.app.vertical.bima.claims.callback.ClaimDetailsCallBack;
import com.tez.androidapp.app.vertical.bima.claims.callback.ClaimDocumentListCallback;
import com.tez.androidapp.app.vertical.bima.claims.callback.ClaimsListCallBack;
import com.tez.androidapp.app.vertical.bima.claims.handlers.ClaimDetailsRH;
import com.tez.androidapp.app.vertical.bima.claims.handlers.ClaimDocumentListRH;
import com.tez.androidapp.app.vertical.bima.claims.handlers.ClaimListRH;
import com.tez.androidapp.app.vertical.bima.policies.callbacks.GetInsurancePoliciesCallback;
import com.tez.androidapp.app.vertical.bima.policies.callbacks.InsurancePolicyDetailsCallback;
import com.tez.androidapp.app.vertical.bima.policies.handlers.GetInsurancePoliciesRH;
import com.tez.androidapp.app.vertical.bima.policies.handlers.InsurancePolicyDetailsRH;
import com.tez.androidapp.app.vertical.bima.shared.api.client.TezBimaAPI;
import com.tez.androidapp.rewamp.advance.request.callback.LoanRemainingStepsCallback;
import com.tez.androidapp.rewamp.advance.request.response.LoanRemainingStepsRH;
import com.tez.androidapp.rewamp.bima.claim.callback.ConfirmClaimCallback;
import com.tez.androidapp.rewamp.bima.claim.callback.InitiateClaimCallback;
import com.tez.androidapp.rewamp.bima.claim.callback.InitiateReviseClaimCallback;
import com.tez.androidapp.rewamp.bima.claim.callback.LodgeClaimCallback;
import com.tez.androidapp.rewamp.bima.claim.callback.UploadDocumentCallback;
import com.tez.androidapp.rewamp.bima.claim.request.ConfirmClaimRequest;
import com.tez.androidapp.rewamp.bima.claim.request.ConfirmReviseClaimRequest;
import com.tez.androidapp.rewamp.bima.claim.request.InitiateClaimRequest;
import com.tez.androidapp.rewamp.bima.claim.request.LodgeClaimRequest;
import com.tez.androidapp.rewamp.bima.claim.response.handler.ConfirmClaimRH;
import com.tez.androidapp.rewamp.bima.claim.response.handler.InitiateClaimRH;
import com.tez.androidapp.rewamp.bima.claim.response.handler.InitiateReviseClaimRH;
import com.tez.androidapp.rewamp.bima.claim.response.handler.LodgeClaimRH;
import com.tez.androidapp.rewamp.bima.claim.response.handler.UploadDocumentRH;
import com.tez.androidapp.rewamp.bima.products.callback.ProductDetailTermConditionCallback;
import com.tez.androidapp.rewamp.bima.products.callback.ProductListCallback;
import com.tez.androidapp.rewamp.bima.products.callback.ProductPolicyCallback;
import com.tez.androidapp.rewamp.bima.products.callback.ProductTermConditionsCallback;
import com.tez.androidapp.rewamp.bima.products.policy.RH.InitiatePurchasePolicyRH;
import com.tez.androidapp.rewamp.bima.products.policy.RH.InsuranceProductDetailsRH;
import com.tez.androidapp.rewamp.bima.products.policy.RH.PurchasePolicyRH;
import com.tez.androidapp.rewamp.bima.products.policy.callbacks.InitiatePurchasePolicyCallback;
import com.tez.androidapp.rewamp.bima.products.policy.callbacks.InsuranceProductDetailCallback;
import com.tez.androidapp.rewamp.bima.products.policy.callbacks.PurchasePolicyCallback;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.request.PurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.response.handler.ProductDetailTermConditionRH;
import com.tez.androidapp.rewamp.bima.products.response.handler.ProductListRH;
import com.tez.androidapp.rewamp.bima.products.response.handler.ProductPolicyRH;
import com.tez.androidapp.rewamp.bima.products.response.handler.ProductTermConditionsRH;
import com.tez.androidapp.rewamp.general.beneficiary.listener.DeleteBeneficiaryListener;
import com.tez.androidapp.rewamp.general.beneficiary.listener.GetBeneficiariesListener;
import com.tez.androidapp.rewamp.general.beneficiary.listener.GetBeneficiaryRelationsListener;
import com.tez.androidapp.rewamp.general.beneficiary.listener.PutBeneficiaryAllocationListener;
import com.tez.androidapp.rewamp.general.beneficiary.request.BeneficiaryAllocationRequest;
import com.tez.androidapp.rewamp.general.beneficiary.response.handler.BeneficiariesRH;
import com.tez.androidapp.rewamp.general.beneficiary.response.handler.BeneficiaryAllocationRH;
import com.tez.androidapp.rewamp.general.beneficiary.response.handler.BeneficiaryRelationsRH;
import com.tez.androidapp.rewamp.general.beneficiary.response.handler.DeleteBeneficiaryRH;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;

/**
 * Created  on 8/25/2017.
 */

public class BimaCloudDataStore extends BaseAuthCloudDataStore {

    private static BimaCloudDataStore bimaCloudDataStore;
    private TezBimaAPI tezBimaAPI;

    private BimaCloudDataStore() {
        super();
        tezBimaAPI = tezAPIBuilder.build().create(TezBimaAPI.class);
    }

    public static void clear() {
        bimaCloudDataStore = null;
    }

    public static BimaCloudDataStore getInstance() {
        if (bimaCloudDataStore == null)
            bimaCloudDataStore = new BimaCloudDataStore();
        return bimaCloudDataStore;
    }

    public void setDefaultBeneficiary(int beneficiaryId, SetDefaultBeneficiaryCallback callback) {
        tezBimaAPI.setDefaultBeneficiary(beneficiaryId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).
                subscribe(new SetDefaultBeneficiaryRH(this, callback));
    }

    public void setBeneficiary(BeneficiaryRequest beneficiaryRequest, SetBeneficiaryCallback setBeneficiaryCallback) {
        tezBimaAPI.setBeneficiary(beneficiaryRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).
                subscribe(new SetBeneficiaryRH(this, setBeneficiaryCallback));
    }

    public void getBeneficiaries(GetBeneficiariesListener beneficiariesListener) {
        tezBimaAPI.getBeneficiaries().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).
                subscribe(new BeneficiariesRH(this, beneficiariesListener));
    }

    public void getBeneficiaryRelations(GetBeneficiaryRelationsListener listener) {
        tezBimaAPI.getBeneficiaryRelations().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).
                subscribe(new BeneficiaryRelationsRH(this, listener));
    }

    public void deleteBeneficiary(int beneficiaryId, DeleteBeneficiaryListener listener) {
        tezBimaAPI.deleteBeneficiary(beneficiaryId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new DeleteBeneficiaryRH(this, listener));
    }

    public void putBeneficiaryAllocation(BeneficiaryAllocationRequest request, PutBeneficiaryAllocationListener listener) {
        tezBimaAPI.putBeneficiaryAllocation(request).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new BeneficiaryAllocationRH(this, listener));
    }

    public void getInsurancePolicies(GetInsurancePoliciesCallback getInsurancePoliciesCallback) {
        tezBimaAPI.getInsurancePolicies().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new
                GetInsurancePoliciesRH(this, getInsurancePoliciesCallback));
    }

    public void getClaimList(ClaimsListCallBack claimsListCallBack) {
        tezBimaAPI.getClaimList().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new ClaimListRH(this, claimsListCallBack));
    }

    public void getClaimDetails(int claimId, ClaimDetailsCallBack callBack) {
        tezBimaAPI.getClaimDetails(claimId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new ClaimDetailsRH(this, callBack));
    }

    public void getClaimQuestions(int insurancePolicyId, ClaimQuestionsCallback claimQuestionsCallback) {
        tezBimaAPI.getClaimQuestions(insurancePolicyId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new ClaimQuestionsRH(this, claimQuestionsCallback));
    }

    public void getClaimDocumentList(int insurancePolicyId, ClaimDocumentListCallback claimDocumentListCallback) {
        tezBimaAPI.getClaimDocumentList(insurancePolicyId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new ClaimDocumentListRH(this, claimDocumentListCallback));
    }

    public void getInsurancePolicyDetails(int policyId,
                                          InsurancePolicyDetailsCallback insurancePolicyDetailsCallback) {
        tezBimaAPI.getInsurancePolicyDetails(policyId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new InsurancePolicyDetailsRH(this, insurancePolicyDetailsCallback));
    }

    public void getProductPolicy(int policyId, @NonNull ProductPolicyCallback callback) {
        tezBimaAPI.getProductPolicy(policyId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new ProductPolicyRH(this, callback));
    }

    public void getInsuranceProducts(@NonNull ProductListCallback callback) {
        tezBimaAPI.getInsuranceProducts().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new ProductListRH(this, callback));
    }

    public void getProductTermConditions(int productId, @NonNull ProductTermConditionsCallback callback) {
        tezBimaAPI.getProductTermConditions(productId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new ProductTermConditionsRH(this, callback));
    }

    public void getProductTermCondition(int productId, @NonNull ProductDetailTermConditionCallback callback) {
        tezBimaAPI.getProductTermCondition(productId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new ProductDetailTermConditionRH(this, callback));
    }

    public void getInsuranceRemainingSteps(LoanRemainingStepsCallback callback) {
        tezBimaAPI.getInsuranceRemainingSteps().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(
                new LoanRemainingStepsRH(this, callback));
    }

    public void getInsuranceCoverage(int productId, InsuranceProductDetailCallback insuranceProductDetailCallback) {
        tezBimaAPI.getInsuranceProductDetail(productId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(
                new InsuranceProductDetailsRH(this, insuranceProductDetailCallback));
    }

    public void initiatePurchasePolicy(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest, InitiatePurchasePolicyCallback initiatePurchasePolicyCallback) {
        tezBimaAPI.initiatePurchasePolicy(initiatePurchasePolicyRequest).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new InitiatePurchasePolicyRH(this, initiatePurchasePolicyCallback));
    }

    public void purchasePolicy(PurchasePolicyRequest purchasePolicyRequest, PurchasePolicyCallback purchasePolicyCallback) {
        tezBimaAPI.purchasePolicy(purchasePolicyRequest).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new PurchasePolicyRH(this, purchasePolicyCallback));

    }

    public void initiateClaim(InitiateClaimRequest request, InitiateClaimCallback callback) {
        tezBimaAPI.initiateClaim(request.getMobileUserInsurancePolicyId()).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new InitiateClaimRH(this, callback));
    }

    public void lodgeClaim(LodgeClaimRequest request, LodgeClaimCallback callback) {
        tezBimaAPI.lodgeClaim(request).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new LodgeClaimRH(this, callback));
    }

    public void uploadDocument(MultipartBody.Part data,
                               MultipartBody.Part claimId,
                               MultipartBody.Part categoryId,
                               UploadDocumentCallback callback) {
        tezBimaAPI.uploadDocument(data, claimId, categoryId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new UploadDocumentRH(this, callback));
    }

    public void confirmClaim(ConfirmClaimRequest request, ConfirmClaimCallback callback) {
        tezBimaAPI.confirmClaim(request.getClaimId()).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new ConfirmClaimRH(this, callback));
    }

    public void initiateReviseClaim(int claimId, InitiateReviseClaimCallback callback) {
        tezBimaAPI.initiateReviseClaim(claimId).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new InitiateReviseClaimRH(this, callback));
    }

    public void confirmReviseClaim(ConfirmReviseClaimRequest request, ConfirmClaimCallback callback) {
        tezBimaAPI.confirmReviseClaim(request).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new ConfirmClaimRH(this, callback));
    }
}
