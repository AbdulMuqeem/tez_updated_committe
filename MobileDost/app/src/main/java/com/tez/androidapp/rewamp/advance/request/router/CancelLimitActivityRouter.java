package com.tez.androidapp.rewamp.advance.request.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.request.view.CancelLimitActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

public class CancelLimitActivityRouter extends BaseActivityRouter {

    public static final String LOAN_ID = "LOAN_D";

    public static CancelLimitActivityRouter createInstance() {
        return new CancelLimitActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, int loanId) {
        Intent intent = createIntent(from);
        intent.putExtra(LOAN_ID, loanId);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CancelLimitActivity.class);
    }
}
