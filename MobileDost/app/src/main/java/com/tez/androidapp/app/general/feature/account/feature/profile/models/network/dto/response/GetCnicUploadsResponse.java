package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.UserProfile;

public class GetCnicUploadsResponse extends BaseResponse {
    private UserProfile userProfile;

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }
}
