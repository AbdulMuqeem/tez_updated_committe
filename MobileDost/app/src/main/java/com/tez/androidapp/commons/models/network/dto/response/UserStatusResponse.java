package com.tez.androidapp.commons.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.models.network.MobileUserInfo;

/**
 * Created by Rehman Murad Ali on 3/1/2018.
 */

public class UserStatusResponse extends BaseResponse {
    private MobileUserInfo userStatus;

    public MobileUserInfo getMobileUserInfo() {
        return userStatus;
    }
}
