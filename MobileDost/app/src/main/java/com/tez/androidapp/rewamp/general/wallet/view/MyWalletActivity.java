package com.tez.androidapp.rewamp.general.wallet.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.validators.annotations.TextInputLayoutRegex;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezCheckBox;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;
import com.tez.androidapp.commons.widgets.WalletCardView;
import com.tez.androidapp.rewamp.general.wallet.presenter.IMyWalletActivityPresenter;
import com.tez.androidapp.rewamp.general.wallet.presenter.MyWalletActivityPresenter;
import com.tez.androidapp.rewamp.general.wallet.router.AddWalletTermsAndConditionActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.router.UpdateWalletActivityRouter;
import com.tez.androidapp.rewamp.profile.trust.router.CompleteYourProfileActivityRouter;
import com.tez.androidapp.rewamp.util.DialogUtil;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import net.tez.fragment.util.actions.Actionable0;
import net.tez.fragment.util.optional.Optional;
import net.tez.logger.library.utils.TextUtil;
import net.tez.validator.library.annotations.Order;
import net.tez.validator.library.core.FieldValidator;
import net.tez.validator.library.core.FilterChain;
import net.tez.validator.library.core.ValidationError;
import net.tez.validator.library.core.ValidationListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.BindViews;
import net.tez.viewbinder.library.core.OnClick;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MyWalletActivity extends BaseActivity implements IMyWalletActivityView, ValidationListener {

    private final IMyWalletActivityPresenter iMyWalletActivityPresenter;

    @BindView(R.id.viewFooter)
    protected View viewFooter;

    @BindViews({R.id.walletCardEasyPaisa,
            R.id.walletCardUblOmni,
            R.id.walletCardSimSim,
            R.id.walletCardJazzCash})
    private List<WalletCardView> wallets;

    @BindView(R.id.layoutNoWalletFound)
    private View layoutNoWalletFound;

    @BindView(R.id.nsvContent)
    private NestedScrollView nsvContent;

    @BindView(R.id.layoutAccountDetail)
    private View layoutAccountDetail;

    @BindView(R.id.btSave)
    private TezButton btSave;

    @TextInputLayoutRegex(regex = Constants.PHONE_NUMBER_VALIDATOR_REGEX, value = {1, R.string.incorrect_mobile_number})
    @Order(1)
    @BindView(R.id.tilMobileNumber)
    private TezTextInputLayout tilMobileNumber;

    @BindView(R.id.etMobileNumber)
    private TezEditTextView etMobileNumber;

    @BindView(R.id.cbMakeDefaultWallet)
    private TezCheckBox cbMakeDefaultWallet;

    private CompositeDisposable allDisposables;

    public MyWalletActivity() {
        iMyWalletActivityPresenter = new MyWalletActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet);
        ViewBinder.bind(this);
        init();
    }

    private void init() {
        initClickListeners();
        Optional.ifPresent(MDPreferenceManager.getUser(), (user) -> {
            Optional.doWhen(TextUtil.isNotEmpty(user.getCnic()),
                    this::getAllWallets,
                    () -> showError(ResponseStatusCode.WALLET_CANNOT_BE_ADDED.getCode(),
                            (d, v) -> CompleteYourProfileActivityRouter.createInstance().setDependenciesAndRoute(this)));
        }, Utility::directUserToIntroScreenAtGlobalLevel);
    }

    protected void getAllWallets() {
        iMyWalletActivityPresenter.getAllWallets();
    }

    private void listenChangesOnViews() {
        if (allDisposables == null || allDisposables.isDisposed()) {
            this.allDisposables = new CompositeDisposable();
            allDisposables.add(RxTextView.textChanges(this.etMobileNumber).toFlowable(BackpressureStrategy.LATEST)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(charSequence -> FieldValidator.validate(this, this)));
        }
    }

    private void dispose() {
        if (allDisposables != null)
            allDisposables.dispose();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.dispose();
    }

    @Override
    public void setEmptyWalletsVisibility(boolean visible) {
        this.layoutNoWalletFound.setVisibility(visible ? View.VISIBLE : View.GONE);
        this.nsvContent.setVisibility(visible ? View.GONE : View.VISIBLE);
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void setNsvContentVisibility(int visibility) {
        this.nsvContent.setVisibility(visibility);
    }

    @Override
    public void showShimmer() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideShimmer() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.GONE);
    }

    @OnClick(R.id.btAddWallet)
    private void onAddWallet() {
        setEmptyWalletsVisibility(false);
    }

    @OnClick(R.id.tvCreateNewWallet)
    private void onCreateWallet() {
        Utility.openPlayStoreForApp(this, Constants.EASYPAISA_PACKAGE_NAME);
    }

    @OnClick(R.id.tvWhyINeedWallet)
    private void defineNeedOfWallet() {
        DialogUtil.showInformativeDialog(this, R.string.why_do_i_need_a_wallet, R.string.wallet_need_defined);
    }

    @Override
    public void setWalletCardState(@NonNull Wallet wallet) {
        WalletCardView walletCardView = getWalletCardView(wallet.getServiceProviderId());
        if (walletCardView != null) {
            walletCardView.setWallet(wallet);
            walletCardView.setCardType(wallet.isDefault()
                    ? WalletCardView.CardType.DEFAULT
                    : WalletCardView.CardType.ACTIVE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == AddWalletTermsAndConditionActivityRouter.REQUEST_CODE_ADD_WALLET
                || requestCode == UpdateWalletActivityRouter.REQUEST_CODE_UPDATE_WALLET)
                && resultCode == RESULT_OK)
            getAllWallets();

        else if (requestCode == UpdateWalletActivityRouter.REQUEST_CODE_UPDATE_WALLET
                && resultCode == UpdateWalletActivityRouter.RESULT_DELETE
                && data != null) {
            iMyWalletActivityPresenter.deleteWallet(data.getIntExtra(UpdateWalletActivityRouter.MOBILE_ACCOUNT_ID, -100));
        }
    }

    private WalletCardView getWalletCardView(int serviceProviderId) {

        switch (serviceProviderId) {

            case Constants.EASYPAISA_ID:
                return wallets.get(0);

            case Constants.UBL_OMNI_ID:
                return wallets.get(1);

            case Constants.SIMSIM_ID:
                return wallets.get(2);

            case Constants.JAZZ_CASH_ID:
                return wallets.get(3);
            default:
                return null;
        }
    }

    private void initClickListeners() {
        for (int i = 0; i < wallets.size(); i++) {
            final int index = i;
            wallets.get(i).setOnWalletSelectedListener(((viewId, wallet, state) -> onWalletSelected(index, viewId, wallet, state)));
        }
    }

    private void onWalletSelected(int index, int viewId, Wallet wallet, WalletCardView.CardType cardType) {

        if (wallets.get(index).isSelected())
            return;

        switch (cardType) {

            case INACTIVE:
                onSelectInActiveWallet(viewId);
                break;

            case ACTIVE:
                onSelectActiveWallet(wallet);
                break;

            case DEFAULT:
                onSelectDefaultWallet(wallet);
                break;
        }

        this.setWalletSelected(index);

        this.setLayoutAccountDetailVisibility(View.VISIBLE);
    }

    private void onSelectInActiveWallet(int viewId) {
        this.listenChangesOnViews();
        this.setLayoutAccountDetailEnabled(true);
        this.setCbMakeDefaultWalletCheckedChangeListener(null);
        this.etMobileNumber.clearText();
        this.btSave.setText(R.string.string_continue);
        this.setBtSaveOnClickListenerWithValidator(() ->
                startAddWalletTermsAndConditionsActivity(getServiceProviderId(viewId),
                        etMobileNumber.getValueText()));
        this.cbMakeDefaultWallet.setChecked(false);
    }

    private void onSelectActiveWallet(Wallet wallet) {
        if (wallet == null) return;
        this.dispose();
        this.setLayoutAccountDetailEnabled(true);
        this.setCbMakeDefaultWalletCheckedChangeListener(wallet);
        this.tilMobileNumber.setEnabled(false);
        this.etMobileNumber.setText(wallet.getMobileAccountNumberFormatted());
        this.etMobileNumber.setTextViewDrawableColor(R.color.disabledColor);
        this.etMobileNumber.setTextColor(Utility.getColorFromResource(R.color.disabledColor));
        this.btSave.setText(R.string.edit_wallet);
        this.btSave.setButtonNormal();
        this.cbMakeDefaultWallet.setChecked(false);
        this.setUpdateWalletListener(wallet);
    }

    public void onSelectDefaultWallet(Wallet wallet) {
        if (wallet == null) return;
        this.dispose();
        this.setLayoutAccountDetailEnabled(false);
        this.setCbMakeDefaultWalletCheckedChangeListener(null);
        this.etMobileNumber.setText(wallet.getMobileAccountNumberFormatted());
        this.cbMakeDefaultWallet.setChecked(true);
        this.btSave.setText(R.string.edit_wallet);
        this.btSave.setButtonNormal();
        this.setUpdateWalletListener(wallet);
    }

    private void setUpdateWalletListener(final Wallet wallet) {
        this.btSave.setDoubleTapSafeOnClickListener(v -> UpdateWalletActivityRouter
                .createInstance()
                .setDependenciesAndRouteForResult(this,
                        wallet.getServiceProviderId(),
                        wallet.getMobileAccountNumber(),
                        wallet.getMobileAccountId(),
                        wallet.isDefault()));
    }

    private void startAddWalletTermsAndConditionsActivity(int serviceProviderId, String mobileAccountNumber) {
        AddWalletTermsAndConditionActivityRouter
                .createInstance()
                .setDependenciesAndRouteForResult(this,
                        serviceProviderId,
                        mobileAccountNumber,
                        null,
                        cbMakeDefaultWallet.isChecked());
    }

    private void setCbMakeDefaultWalletCheckedChangeListener(@Nullable Wallet wallet) {
        CompoundButton.OnCheckedChangeListener listener = wallet != null ? (buttonView, isChecked) -> {
            if (isChecked)
                takeConfirmationToMakeDefaultWallet(wallet);
        } : null;
        this.cbMakeDefaultWallet.setOnCheckedChangeListener(listener);
    }

    private void setBtSaveOnClickListenerWithValidator(@NonNull final Actionable0 actionable) {
        this.btSave.setDoubleTapSafeOnClickListener(view -> FieldValidator.validate(this, new ValidationListener() {
            @Override
            public void validateSuccess() {
                actionable.action();
            }

            @Override
            public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
                Utility.setErrorOnTezViews(validationError.getView(), validationError.getMessage());
                btSave.setButtonInactive();
                filterChain.doFilter();
            }
        }));
    }

    @Override
    public void setLayoutAccountDetailVisibility(int visibility) {
        this.layoutAccountDetail.setVisibility(visibility);
    }

    @Override
    public void resetCards() {
        for (WalletCardView wallet : wallets) {
            wallet.setWallet(null);
            wallet.setSelected(false);
            wallet.setCardType(WalletCardView.CardType.INACTIVE);
        }
    }

    private void takeConfirmationToMakeDefaultWallet(final Wallet wallet) {
        String confirmation = getString(R.string.are_you_sure_to_make_default_wallet,
                Utility.getWalletName(wallet.getServiceProviderId()));
        DialogUtil.showActionableDialog(this,
                R.string.default_wallet,
                confirmation,
                R.string.string_yes,
                R.string.string_no,
                (dialog, which) -> {
                    if (which == DialogInterface.BUTTON_POSITIVE)
                        iMyWalletActivityPresenter.setDefaultWallet(wallet);
                    if (which == DialogInterface.BUTTON_NEGATIVE)
                        cbMakeDefaultWallet.setChecked(false);
                });
    }

    private int getServiceProviderId(int viewId) {

        switch (viewId) {

            case R.id.walletCardEasyPaisa:
                return Constants.EASYPAISA_ID;

            case R.id.walletCardSimSim:
                return Constants.SIMSIM_ID;

            case R.id.walletCardUblOmni:
                return Constants.UBL_OMNI_ID;

            case R.id.walletCardJazzCash:
                return Constants.JAZZ_CASH_ID;
        }

        return -1;
    }

    private void setLayoutAccountDetailEnabled(boolean enabled) {
        int disabledColor = Utility.getColorFromResource(R.color.disabledColor);
        this.tilMobileNumber.setEnabled(enabled);
        this.tilMobileNumber.setError(null);
        this.etMobileNumber.setTextColor(enabled ? Utility.getColorFromResource(R.color.editTextTextColorGrey) : disabledColor);
        if (enabled)
            this.etMobileNumber.clearTextViewDrawableColor();
        else
            this.etMobileNumber.setTextViewDrawableColor(R.color.disabledColor);
        this.cbMakeDefaultWallet.setEnabled(enabled);
        this.cbMakeDefaultWallet.setButtonDrawableColor(enabled ? R.color.textViewTextColorGreen : R.color.disabledColor);
        this.cbMakeDefaultWallet.setTextColor(enabled ? Utility.getColorFromResource(R.color.textViewTextColorBlack) : disabledColor);
    }

    private void setWalletSelected(int index) {
        wallets.get(index).setSelected(true);
        for (int i = 0; i < wallets.size(); i++) {
            if (i != index) {
                wallets.get(i).setSelected(false);
            }
        }
    }

    @Override
    public void validateSuccess() {
        this.btSave.setButtonNormal();
    }

    @Override
    public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
        this.btSave.setButtonInactive();
        filterChain.doFilter();
    }

    @Override
    public void validatePassed(@NonNull View v) {
        Utility.setErrorOnTezViews(v, null);
    }

    @Override
    protected String getScreenName() {
        return "MyWalletActivity";
    }
}
