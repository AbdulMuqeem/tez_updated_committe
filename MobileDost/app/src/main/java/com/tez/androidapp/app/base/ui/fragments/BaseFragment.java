package com.tez.androidapp.app.base.ui.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.location.handlers.LocationHandler;
import com.tez.androidapp.commons.widgets.TezLoader;

import net.tez.fragment.util.actions.Actionable1;
import net.tez.fragment.util.base.AbstractHelperFragment;

/**
 * Created by Rehman Murad Ali on 12/12/2017.
 */

public abstract class BaseFragment extends AbstractHelperFragment implements IBaseView {


    protected TezLoader tezLoader;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBaseFragmentContext(context -> {
            tezLoader = new TezLoader(context);
        });
    }

    @Nullable
    @Override
    public TezLoader getInitializedLoader() {
        return tezLoader;
    }

    @Nullable
    @Override
    public BaseActivity getHostActivity() {
        return isInstanceOfActivity(BaseActivity.class);
    }

    @Override
    public void getCurrentLocation(@NonNull LocationAvailableCallback callback) {
        getBaseFragmentActivity(fragmentActivity -> {
            LocationHandler.requestCurrentLocation(fragmentActivity, callback);
        });
    }

    protected void getBaseActivity(@NonNull Actionable1<BaseActivity> activityActionable1) {
        isInstanceOfActivity(BaseActivity.class, activityActionable1);
    }
}