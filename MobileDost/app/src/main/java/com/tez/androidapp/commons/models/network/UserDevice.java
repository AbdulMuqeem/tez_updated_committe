package com.tez.androidapp.commons.models.network;

import java.io.Serializable;

/**
 * Created  on 2/14/2017.
 */

public class UserDevice implements Serializable {

    private String imei;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    @Override
    public String toString() {
        return "UserDevice{" +
                "imei='" + imei + '\'' +
                '}';
    }
}
