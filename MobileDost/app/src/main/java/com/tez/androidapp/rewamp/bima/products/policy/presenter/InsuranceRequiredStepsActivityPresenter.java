package com.tez.androidapp.rewamp.bima.products.policy.presenter;

import com.tez.androidapp.rewamp.advance.request.presenter.LoanRequiredStepsActivityPresenter;
import com.tez.androidapp.rewamp.advance.request.view.ILoanRequiredStepsActivityView;
import com.tez.androidapp.rewamp.bima.products.policy.interactor.InsuranceRequiredStepsActivityInteractor;

public class InsuranceRequiredStepsActivityPresenter extends LoanRequiredStepsActivityPresenter {


    public InsuranceRequiredStepsActivityPresenter(ILoanRequiredStepsActivityView iInsuranceRequiredStepsActivityView) {
        super(iInsuranceRequiredStepsActivityView);
        this.iLoanRequiredStepsActivityInteractor = new InsuranceRequiredStepsActivityInteractor(this);
    }
}
