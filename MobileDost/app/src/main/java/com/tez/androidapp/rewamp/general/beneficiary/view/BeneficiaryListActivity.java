package com.tez.androidapp.rewamp.general.beneficiary.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.ToolbarActivity;
import com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network.BeneficiaryDetails;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.general.beneficiary.adapter.BeneficiaryListAdapter;
import com.tez.androidapp.rewamp.general.beneficiary.entity.Beneficiary;
import com.tez.androidapp.rewamp.general.beneficiary.presenter.BeneficiaryListActivityPresenter;
import com.tez.androidapp.rewamp.general.beneficiary.presenter.IBeneficiaryListActivityPresenter;
import com.tez.androidapp.rewamp.general.beneficiary.router.BeneficiaryListActivityRouter;
import com.tez.androidapp.rewamp.general.beneficiary.router.EditBeneficiaryActivityRouter;

import net.tez.viewbinder.library.core.BindView;

import java.util.List;

public class BeneficiaryListActivity extends ToolbarActivity implements IBeneficiaryListActivityView, BeneficiaryListAdapter.BeneficiaryListener {

    private final IBeneficiaryListActivityPresenter iBeneficiaryListActivityPresenter;

    @BindView(R.id.rvBeneficiaryList)
    private RecyclerView rvBeneficiaryList;

    @BindView(R.id.cvAddNewBeneficiary)
    private TezCardView cvAddNewBeneficiary;

    @BindView(R.id.clContent)
    private TezConstraintLayout clContent;

    private BeneficiaryListAdapter beneficiaryListAdapter;

    private boolean isResultOk;

    public BeneficiaryListActivity() {
        iBeneficiaryListActivityPresenter = new BeneficiaryListActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beneficiary_list);
        this.tezToolbar.setToolbarTitle(R.string.manage_beneficiary);
        init();
    }

    private void init() {
        iBeneficiaryListActivityPresenter.getBeneficiaries();
        initOnClickListener();
    }

    private void initOnClickListener() {
        cvAddNewBeneficiary.setDoubleTapSafeOnClickListener(view -> onAddNewBeneficiary());
    }

    @Override
    public void setClContentVisibility(int visibility) {
        clContent.setVisibility(visibility);
    }

    @Override
    public void initAdapter(@NonNull List<Beneficiary> beneficiaryList) {
        this.beneficiaryListAdapter = new BeneficiaryListAdapter(beneficiaryList, this);
        rvBeneficiaryList.setLayoutManager(new LinearLayoutManager(this));
        rvBeneficiaryList.setAdapter(beneficiaryListAdapter);
    }

    @Override
    public void onClickBeneficiary(@NonNull Beneficiary beneficiary) {
        int mobileUserInsurancePolicyBeneficiaryId = getIntent().getIntExtra(BeneficiaryListActivityRouter.MOBILE_USER_BENEFICIARY_ID, -100);
        if (mobileUserInsurancePolicyBeneficiaryId != -100)
            iBeneficiaryListActivityPresenter.setAdvanceBimaBeneficiary(beneficiary.getId(), mobileUserInsurancePolicyBeneficiaryId);
        else
            iBeneficiaryListActivityPresenter.setDefaultBeneficiary(beneficiary.getId());
    }

    @Override
    public void onEditBeneficiary(@NonNull Beneficiary beneficiary) {
        BeneficiaryDetails beneficiaryDetails = new BeneficiaryDetails();
        beneficiaryDetails.setId(beneficiary.getId());
        beneficiaryDetails.setName(beneficiary.getName());
        beneficiaryDetails.setRelationshipId(beneficiary.getRelationshipId());
        beneficiaryDetails.setMobileNumber(beneficiary.getMobileNumber());
        beneficiaryDetails.setEditable(beneficiary.isEditable());
        EditBeneficiaryActivityRouter.createInstance().setDependenciesAndRouteForResult(this, beneficiaryListAdapter.getOneTimeAddOnlyAddedRelations(), beneficiaryDetails);
    }

    private void onAddNewBeneficiary() {
        EditBeneficiaryActivityRouter.createInstance().setDependenciesAndRouteForResult(this,
                beneficiaryListAdapter.getOneTimeAddOnlyAddedRelations());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == EditBeneficiaryActivityRouter.REQUEST_CODE_EDIT_BENEFICIARY) {
            this.isResultOk = true;
            iBeneficiaryListActivityPresenter.getBeneficiaries();
        }
    }

    @Override
    public void finishActivityWithResultOk() {
        setResult(RESULT_OK);
        super.finishActivity();
    }

    @Override
    public void onBackPressed() {
        setResult(isResultOk ? RESULT_OK : RESULT_CANCELED);
        finish();
    }

    @Override
    public void showShimmer() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideShimmer() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.GONE);
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    protected String getScreenName() {
        return "BeneficiaryListActivity";
    }
}
