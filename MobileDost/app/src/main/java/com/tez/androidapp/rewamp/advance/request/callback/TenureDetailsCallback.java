package com.tez.androidapp.rewamp.advance.request.callback;

import com.tez.androidapp.rewamp.advance.request.entity.TenureDetail;

import java.util.List;

public interface TenureDetailsCallback {

    void onGetTenuresDetailsSuccess(List<TenureDetail> tenureDetailList);

    void onGetTenuresDetailsFailure(int statusCode, String errorDescription);
}
