package com.tez.androidapp.rewamp.general.fragment

import androidx.navigation.fragment.NavHostFragment

class TezNavHostFragment : NavHostFragment()