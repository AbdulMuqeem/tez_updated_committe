package com.tez.androidapp.rewamp.advance.request.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.onboarding.AdvanceOnBoardingActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

public class AdvanceOnBoardingActivityRouter extends BaseActivityRouter {

    public static AdvanceOnBoardingActivityRouter createInstance() {
        return new AdvanceOnBoardingActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, AdvanceOnBoardingActivity.class);
    }
}
