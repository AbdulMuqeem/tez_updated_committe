package com.tez.androidapp.commons.utils.file.util;

import android.content.Context;
import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Registry;
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.module.AppGlideModule;
import com.readystatesoftware.chuck.ChuckInterceptor;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.commons.utils.network.AuthHeaderInterceptor;

import java.io.InputStream;

import okhttp3.OkHttpClient;

/**
 * Created by FARHAN DHANANI on 7/12/2018.
 */
@com.bumptech.glide.annotation.GlideModule
public class GlideModule extends AppGlideModule {

    @Override
    public void registerComponents(@NonNull Context context, @NonNull Glide glide, @NonNull Registry registry) {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new AuthHeaderInterceptor())
                //.addInterceptor(new ChuckInterceptor(MobileDostApplication.getInstance()))
                .build();

        OkHttpUrlLoader.Factory factory = new OkHttpUrlLoader.Factory(client);
        registry.replace(GlideUrl.class, InputStream.class, factory);
    }
}
