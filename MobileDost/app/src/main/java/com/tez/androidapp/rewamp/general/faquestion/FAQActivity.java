package com.tez.androidapp.rewamp.general.faquestion;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.ToolbarActivity;
import com.tez.androidapp.app.general.feature.faq.models.network.FAQ;
import com.tez.androidapp.commons.locale.LocaleHelper;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.rewamp.general.faquestion.adapter.FAQAdapter;
import com.tez.androidapp.rewamp.general.faquestion.presenter.FAQPresenter;
import com.tez.androidapp.rewamp.general.faquestion.presenter.IFAQPresenter;

import net.tez.viewbinder.library.core.BindView;

import java.util.List;

public class FAQActivity extends ToolbarActivity implements IFAQActivityView {

    @BindView(R.id.rvFaq)
    private RecyclerView rvFaq;

    @BindView(R.id.shimmer)
    private TezLinearLayout shimmerTezLinearLayout;

    private final IFAQPresenter ifaqPresenter;

    public FAQActivity() {
        ifaqPresenter = new FAQPresenter(this);
    }

    private final BroadcastReceiver isOnValidatePinActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setResultCode(1);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        this.tezToolbar.setToolbarTitle(R.string.faqs);
        init();
    }

    private void init() {
        this.rvFaq.setVisibility(View.GONE);
        this.shimmerTezLinearLayout.setVisibility(View.VISIBLE);
        ifaqPresenter.callForFAQS(LocaleHelper.getSelectedLanguageForBackend(this));
    }

    @Override
    public void renderRecyclerView(List<FAQ> faqs) {
        this.rvFaq.setVisibility(View.VISIBLE);
        this.rvFaq.setAdapter(new FAQAdapter(faqs));
        this.shimmerTezLinearLayout.setVisibility(View.GONE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        try{
            unregisterReceiver(isOnValidatePinActivity);
        } catch (IllegalArgumentException e){
            //igonore
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(isOnValidatePinActivity, new IntentFilter(Constants.BROAD_CAST_RECIEVER_ON_VALIDATE_PIN_ACTIVITY));
    }

    @Override
    protected String getScreenName() {
        return "FAQActivity";
    }
}
