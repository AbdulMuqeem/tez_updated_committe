package com.tez.androidapp.app.general.feature.notification.callbacks;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;

/**
 * Created  on 8/24/2017.
 */

public interface GetMoreNotificationsListener extends BaseRecyclerViewListener {

    void fetchNotifications();
}
