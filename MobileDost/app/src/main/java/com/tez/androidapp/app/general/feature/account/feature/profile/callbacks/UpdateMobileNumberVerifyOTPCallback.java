package com.tez.androidapp.app.general.feature.account.feature.profile.callbacks;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created by Rehman Murad Ali on 9/8/2017.
 */

public interface UpdateMobileNumberVerifyOTPCallback {

    void onUpdateMobileNumberVerifyOTPSuccess(BaseResponse baseResponse);

    void onUpdateMobileNumberVerifyOTPFailure(int errorCode, String message);

}
