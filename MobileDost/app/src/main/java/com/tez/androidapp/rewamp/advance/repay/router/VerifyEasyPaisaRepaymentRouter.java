package com.tez.androidapp.rewamp.advance.repay.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.repay.view.VerifyEasyPaisaRepaymentActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

public class VerifyEasyPaisaRepaymentRouter extends BaseActivityRouter {

    public static final String LOAN_ID = "LOAN_ID";
    public static final String WALLET_MOBILE_ACCOUNT_ID = "WALLET_MOBILE_ACCOUNT_ID";
    public static final String WALLET_SERVICE_PROVIDER_ID = "WALLET_SERVICE_PROVIDER_ID";
    public static final String AMOUNT = "AMOUNT";

    public static VerifyEasyPaisaRepaymentRouter createInstance() {
        return new VerifyEasyPaisaRepaymentRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        int loanId,
                                        int walletServiceProviderId,
                                        int walletMobileAccountId,
                                        double amount) {
        Intent intent = createIntent(from);
        intent.putExtra(LOAN_ID, loanId);
        intent.putExtra(WALLET_SERVICE_PROVIDER_ID, walletServiceProviderId);
        intent.putExtra(WALLET_MOBILE_ACCOUNT_ID, walletMobileAccountId);
        intent.putExtra(AMOUNT, amount);
        route(from, intent);
    }

    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, VerifyEasyPaisaRepaymentActivity.class);
    }
}
