package com.tez.androidapp.rewamp.bima.claim.response.handler

import com.jakewharton.retrofit2.adapter.rxjava2.Result
import com.tez.androidapp.app.base.handlers.BaseRH
import com.tez.androidapp.repository.network.store.BaseCloudDataStore
import com.tez.androidapp.rewamp.bima.claim.callback.ConfirmClaimCallback
import com.tez.androidapp.rewamp.bima.claim.response.ConfirmClaimResponse

class ConfirmClaimRH(baseCloudDataStore: BaseCloudDataStore?,
                     private val confirmClaimCallback: ConfirmClaimCallback)
    : BaseRH<ConfirmClaimResponse>(baseCloudDataStore) {
    override fun onSuccess(value: Result<ConfirmClaimResponse>) {
        val response = value.response().body()
        response?.let {
            if (isErrorFree(it))
                confirmClaimCallback.onSuccessConfirmClaim(it)
            else
                onFailure(it.statusCode, it.errorDescription)
        } ?: sendDefaultFailure()
    }

    override fun onFailure(errorCode: Int, message: String?) {
        confirmClaimCallback.onFailureConfirmClaim(errorCode, message)
    }
}