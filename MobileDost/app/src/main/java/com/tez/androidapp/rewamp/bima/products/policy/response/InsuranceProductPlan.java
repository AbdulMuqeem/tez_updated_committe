package com.tez.androidapp.rewamp.bima.products.policy.response;

import java.util.List;

public class InsuranceProductPlan {

    private double coverage;
    private List<InsuranceTenure> tenures;

    public double getCoverage() {
        return coverage;
    }

    public List<InsuranceTenure> getTenures() {
        return tenures;
    }
}
