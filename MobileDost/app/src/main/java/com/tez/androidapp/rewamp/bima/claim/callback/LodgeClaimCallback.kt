package com.tez.androidapp.rewamp.bima.claim.callback

import com.tez.androidapp.rewamp.bima.claim.response.LodgeClaimResponse

interface LodgeClaimCallback {

    fun onSuccessLodgeClaim(lodgeClaimResponse: LodgeClaimResponse)

    fun onFailureLodgeClaim(errorCode: Int, message: String?)
}