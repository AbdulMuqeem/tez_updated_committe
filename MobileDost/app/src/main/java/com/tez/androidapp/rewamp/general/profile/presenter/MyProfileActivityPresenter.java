package com.tez.androidapp.rewamp.general.profile.presenter;

import android.view.View;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.AccountLinkRequest;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.User;
import com.tez.androidapp.commons.models.network.UserAddress;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.rewamp.general.profile.interactor.IMyProfileActivityInteractor;
import com.tez.androidapp.rewamp.general.profile.interactor.MyProfileActivityInteractor;
import com.tez.androidapp.rewamp.general.profile.view.IMyProfileActivityView;

import java.util.List;

public class MyProfileActivityPresenter implements
        IMyProfileActivityPresenter, IMyProfileActivityInteractorOutput {

    private final IMyProfileActivityInteractor iMyProfileActivityInteractor;
    private final IMyProfileActivityView iMyProfileActivityView;

    public MyProfileActivityPresenter(IMyProfileActivityView iMyProfileActivityView) {
        this.iMyProfileActivityView = iMyProfileActivityView;
        this.iMyProfileActivityInteractor = new MyProfileActivityInteractor(this);
    }

    @Override
    public void getProfileDetails() {
        this.iMyProfileActivityInteractor.getProfileDetails();
    }

    @Override
    public void linkUserSocialAccount(String socialId, Integer socialType){
        this.iMyProfileActivityView.showTezLoader();
        this.iMyProfileActivityInteractor.linkUserAccount(new AccountLinkRequest(socialId, socialType));
    }


    @Override
    public void onGetUserProfileSuccess(@NonNull User user) {
        this.iMyProfileActivityView.setTextToTvFullName(user.getFullName());
        this.iMyProfileActivityView.setTextToTvCnic(user.getCnic());
        this.iMyProfileActivityView.setTextToTvMobileNumber(user.getMobileNumber());
        this.iMyProfileActivityView.setTextToTvDateOfBirth(user.getDateOfBirth());
        this.iMyProfileActivityView.setTextToTvEmailAddress(user.getEmail());
        this.iMyProfileActivityView.setVisibillityToBtCompleteProfile(user.isProfileCompleted()?
                View.GONE: View.VISIBLE);
        setUserAddresses(user.getAddresses());
        setSocialAccounts(user.getLinkedSocialAccounts());
        this.updateUserPrefrence(user);
        this.iMyProfileActivityView.setVisibillityForTezConstraintLayoutPager(View.VISIBLE);
        this.iMyProfileActivityView.setVisibillityForTezLinearLayoutShimmer(View.GONE);
        this.iMyProfileActivityView.dismissTezLoader();
    }

    private void updateUserPrefrence(User user){
        Optional.ifPresent(MDPreferenceManager.getUser(), cacheUser->{
            cacheUser.setFullName(user.getFullName());
            cacheUser.setCnic(user.getCnic());
            cacheUser.setMobileNumber(user.getMobileNumber());
            cacheUser.setDateOfBirth(user.getDateOfBirth());
            cacheUser.setEmail(user.getEmail());
            cacheUser.setAddresses(user.getAddresses());
            MDPreferenceManager.setUser(cacheUser);
        }, ()->MDPreferenceManager.setUser(user));
    }

    @Override
    public void setUserAddresses(List<UserAddress> listUserAddresses){
        Optional.ifPresent(listUserAddresses, userAddresses -> {
            Optional.doWhen(userAddresses.size() >= 1,
                    () -> Optional.ifPresent(userAddresses.get(0), userAddress -> {
                        this.iMyProfileActivityView.setTextToTvCurrentAddress(userAddress.getAddress());
                    }));
        });
    }
    private void setSocialAccounts(List<Integer> socialAccounts){
        Optional.ifPresent(socialAccounts, listLinkedAccounts->{
            for(Integer socialId: listLinkedAccounts){
                updateViewsForSocialId(socialId);
            }
        });
    }

    private void updateViewsForSocialId(Integer socialId){
        if(socialId == Utility.PrincipalType.FACEBOOK.getValue()){
            this.iMyProfileActivityView.setListenerToBtFacebook(null);
            this.iMyProfileActivityView.setVisibillityToIvFacebookTick(View.VISIBLE);
        } else {
            this.iMyProfileActivityView.setListenerToBtGoogle(null);
            this.iMyProfileActivityView.setVisibillityToIvGoogleTick(View.VISIBLE);
        }
    }

    @Override
    public void onGetUserProfileFailure(int errorCode, String message) {
        this.iMyProfileActivityView.showError(errorCode,
                (d, v) -> this.iMyProfileActivityView.finishActivity());
    }

    @Override
    public void accountLinkSuccess(Integer accountId) {
        this.iMyProfileActivityView.dismissTezLoader();
        updateViewsForSocialId(accountId);
    }

    @Override
    public void accountLinkFailure(int errorCode, String message) {
        this.iMyProfileActivityView.dismissTezLoader();
        this.iMyProfileActivityView.showError(errorCode);
    }
}
