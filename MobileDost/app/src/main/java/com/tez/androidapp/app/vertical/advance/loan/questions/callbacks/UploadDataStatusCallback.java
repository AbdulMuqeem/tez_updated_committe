package com.tez.androidapp.app.vertical.advance.loan.questions.callbacks;

import com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.dto.response.UploadDataStatusResponse;

/**
 * Created by FARHAN DHANANI on 10/15/2018.
 */
public interface UploadDataStatusCallback {

    void onUploadDataStatusSuccess(UploadDataStatusResponse uploadDataStatusResponse);

    void onUploadDataStatusFailure(int errorCode, String message);
}
