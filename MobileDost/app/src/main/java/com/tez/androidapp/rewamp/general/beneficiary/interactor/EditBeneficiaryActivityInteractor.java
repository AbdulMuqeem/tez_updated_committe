package com.tez.androidapp.rewamp.general.beneficiary.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks.SetBeneficiaryCallback;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.models.network.dto.request.BeneficiaryRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BimaCloudDataStore;
import com.tez.androidapp.rewamp.general.beneficiary.presenter.IEditBeneficiaryActivityInteractorOutput;

public class EditBeneficiaryActivityInteractor implements IEditBeneficiaryActivityInteractor {

    private final IEditBeneficiaryActivityInteractorOutput iEditBeneficiaryActivityInteractorOutput;

    public EditBeneficiaryActivityInteractor(IEditBeneficiaryActivityInteractorOutput iEditBeneficiaryActivityInteractorOutput) {
        this.iEditBeneficiaryActivityInteractorOutput = iEditBeneficiaryActivityInteractorOutput;
    }

    @Override
    public void setBeneficiary(@NonNull BeneficiaryRequest request) {
        BimaCloudDataStore.getInstance().setBeneficiary(request, new SetBeneficiaryCallback() {
            @Override
            public void onSetBeneficiarySuccess() {
                iEditBeneficiaryActivityInteractorOutput.onSetBeneficiarySuccess();
            }

            @Override
            public void onSetBeneficiaryFailure(int statusCode, String message) {
                if (Utility.isUnauthorized(statusCode))
                    setBeneficiary(request);
                else
                    iEditBeneficiaryActivityInteractorOutput.onSetBeneficiaryFailure(statusCode, message);
            }
        });
    }
}
