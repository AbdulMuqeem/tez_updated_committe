package com.tez.androidapp.commons.validators.filters;

import android.widget.EditText;

import androidx.annotation.NonNull;

import com.tez.androidapp.commons.validators.annotations.MaxDateDeviationFromCurrentDate;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;

import net.tez.validator.library.filters.Filter;
import net.tez.validator.library.utils.TextUtil;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by FARHAN DHANANI on 11/25/2018.
 */
public class MaxDateDeviationFromCurrentDateFilter implements Filter<TezTextInputLayout, MaxDateDeviationFromCurrentDate> {
    @Override
    public boolean isValidated(@NonNull TezTextInputLayout view, @NonNull MaxDateDeviationFromCurrentDate annotation) {
        EditText editText = view.getEditText();
        if (editText == null)
            return true;
        String text = editText.getText().toString();
        try {
            if (TextUtil.isEmpty(text)) {
                return true;
            } else {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.YEAR, annotation.yearDeviation());
                calendar.add(Calendar.MONTH, annotation.monthDeviation());
                calendar.add(Calendar.DATE, annotation.dayDeviation());
                return TextUtil.getFormattedDate(text, annotation.dateFormat())
                        .before(calendar.getTime());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }
}
