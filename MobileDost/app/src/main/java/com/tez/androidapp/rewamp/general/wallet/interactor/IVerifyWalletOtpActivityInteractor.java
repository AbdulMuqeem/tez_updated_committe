package com.tez.androidapp.rewamp.general.wallet.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.models.network.dto.request.AddWalletRequest;

public interface IVerifyWalletOtpActivityInteractor extends IBaseInteractor {

    void addWallet(@NonNull AddWalletRequest addWalletRequest);

    void verifyAddWalletOtp(int serviceProviderId, String mobileAccountNumber, String otp);

    void resendAddWalletOtp(int serviceProviderId, String mobileAccountNumber);
}
