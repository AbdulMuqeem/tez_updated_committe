package com.tez.androidapp.rewamp.bima.products.callback;

import com.tez.androidapp.rewamp.bima.products.response.ProductPolicyResponse;

public interface ProductPolicyCallback {

    void onGetProductPolicySuccess(ProductPolicyResponse productPolicyResponse);

    void onGetProductPolicyFailure(int errorCode, String message);
}
