package com.tez.androidapp.app.general.feature.transactions.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.transactions.models.network.Transaction;

import java.util.List;

/**
 * Created  on 9/2/2017.
 */

public class GetUserTransactionsResponse extends BaseResponse {

    private List<Transaction> transactionList;

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }
}
