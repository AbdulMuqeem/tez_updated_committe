package com.tez.androidapp.commons.location.callbacks;

import androidx.annotation.NonNull;


import com.tez.androidapp.commons.models.app.City;

import java.util.List;

/**
 * Created  on 2/9/2017.
 */

public interface GetCitiesCallback {

    void onGetCitiesSuccess(@NonNull List<City> cities);

    void onGetCitiesError(int errorcode,String message);
}
