package com.tez.androidapp.app.vertical.bima.policies.callbacks;

import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.MobileUserPolicyDetailsDto;

/**
 * Created by Vinod Kumar on 5/5/2020.
 */
public interface InsurancePolicyDetailsCallback {

    void onGetActiveInsurancePolicyDetailsSuccess(MobileUserPolicyDetailsDto mobileUserPolicyDetailsDto);

    void onGetActiveInsurancePolicyDetailsFailure(int errorCode, String message);
}
