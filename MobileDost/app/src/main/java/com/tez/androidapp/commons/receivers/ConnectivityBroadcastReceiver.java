package com.tez.androidapp.commons.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.file.util.FileUtil;
import com.tez.androidapp.repository.network.store.DataLiftCloudDataStore;

import java.io.File;

/**
 * Created  on 9/19/2017.
 */

public class ConnectivityBroadcastReceiver extends BroadcastReceiver {

    public static final String CONNECTIVITY_CHANGE_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";

    private static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            return activeNetwork != null && activeNetwork.isConnected();
        }
        return false;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (CONNECTIVITY_CHANGE_ACTION.equals(action) && isConnected(context) && MDPreferenceManager.getIsUploadDeviceDataReady()) {
            File root = FileUtil.getOutputRoot(context);
            String zipName = "data.zip";
            File zipFile = new File(root, zipName);
            if (zipFile.exists()) {
                startNewThread(zipFile);
            }
        }
    }

    public void register(Context context) {
        context.registerReceiver(this, new IntentFilter(CONNECTIVITY_CHANGE_ACTION));
    }

    private void startNewThread(File zipFile) {
        new Thread(() -> DataLiftCloudDataStore.getInstance().uploadDeviceData(zipFile)).start();
    }
}
