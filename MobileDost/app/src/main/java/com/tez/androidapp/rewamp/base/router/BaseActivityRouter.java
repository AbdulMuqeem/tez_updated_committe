package com.tez.androidapp.rewamp.base.router;

import android.content.ActivityNotFoundException;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.base.ui.fragments.BaseFragment;

/**
 * Created by VINOD KUMAR on 7/31/2019.
 */
public class BaseActivityRouter {


    protected void route(@NonNull BaseActivity from, @NonNull Intent intent) {
        try {
            from.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // ignore
        }
    }

    protected void route(@NonNull BaseFragment from, @NonNull Intent intent) {
        try {
            from.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // ignore
        }
    }

    protected void routeForResult(@NonNull BaseActivity from,
                                  @NonNull Intent intent,
                                  int requestCode) {
        try {
            from.startActivityForResult(intent, requestCode);
        } catch (ActivityNotFoundException e) {
            // ignore
        }
    }

    protected void routeForResult(@NonNull BaseFragment from,
                                  @NonNull Intent intent,
                                  int requestCode) {
        try {
            from.startActivityForResult(intent, requestCode);
        } catch (ActivityNotFoundException e) {
            // ignore
        }
    }
}
