package com.tez.androidapp.app.general.feature.inivite.user.handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.inivite.user.callback.GetReferralCodeCallback;
import com.tez.androidapp.app.general.feature.inivite.user.models.dto.response.GetReferralCodeResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by FARHAN DHANANI on 9/6/2018.
 */
public class GetReferralCodeRH extends BaseRH<GetReferralCodeResponse> {
    private final GetReferralCodeCallback getReferralCodeCallback;

    public GetReferralCodeRH(BaseCloudDataStore baseCloudDataStore, @NonNull GetReferralCodeCallback getReferralCodeCallback) {
        super(baseCloudDataStore);
        this.getReferralCodeCallback = getReferralCodeCallback;
    }

    @Override
    protected void onSuccess(Result<GetReferralCodeResponse> value) {
        GetReferralCodeResponse getReferralCodeResponse = value.response().body();
        if (getReferralCodeResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            getReferralCodeCallback.onGetReferralCodeSuccess(getReferralCodeResponse.getReferralCode());
        else
            onFailure(getReferralCodeResponse.getStatusCode(), getReferralCodeResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (getReferralCodeCallback != null)
            getReferralCodeCallback.onGetReferralCodeFailure(errorCode, message);
    }
}
