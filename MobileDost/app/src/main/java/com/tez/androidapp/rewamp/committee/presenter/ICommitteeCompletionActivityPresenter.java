package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.request.CommitteeCreateRequest;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface ICommitteeCompletionActivityPresenter {
    void createCommittee(CommitteeCreateRequest committeeCreateRequest);
}
