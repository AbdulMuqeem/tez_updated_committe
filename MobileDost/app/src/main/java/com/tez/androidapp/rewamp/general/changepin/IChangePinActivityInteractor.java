package com.tez.androidapp.rewamp.general.changepin;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;

/**
 * Created by VINOD KUMAR on 8/26/2019.
 */
public interface IChangePinActivityInteractor extends IBaseInteractor {

    void changePin(String newPin, String oldPin);
}
