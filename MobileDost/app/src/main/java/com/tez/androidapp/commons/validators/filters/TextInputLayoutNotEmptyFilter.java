package com.tez.androidapp.commons.validators.filters;

import android.widget.EditText;

import androidx.annotation.NonNull;

import com.tez.androidapp.commons.validators.annotations.TextInputLayoutNotEmpty;
import com.tez.androidapp.commons.validators.annotations.TextInputLayoutRegex;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;

import net.tez.validator.library.filters.Filter;
import net.tez.validator.library.utils.TextUtil;

public class TextInputLayoutNotEmptyFilter implements Filter<TezTextInputLayout, TextInputLayoutNotEmpty> {
    @Override
    public boolean isValidated(@NonNull TezTextInputLayout view, @NonNull TextInputLayoutNotEmpty annotation) {
        EditText editText = view.getEditText();
        String text = editText == null ?
                null :
                editText.getText() == null ?
                        null : editText.getText().toString();
        return TextUtil.isNotEmpty(text);
    }
}
