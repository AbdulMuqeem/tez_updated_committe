package com.tez.androidapp.rewamp.dashboard.callbacks;

import com.tez.androidapp.rewamp.dashboard.response.UserProfileStatusResponse;

public interface UserProfileStatusCallback {

    void onUserProfileStatusSuccess(UserProfileStatusResponse userProfileStatusResponse);

    void onUserProfileStatusFailure(int errorCode, String message);
}
