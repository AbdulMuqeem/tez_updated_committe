package com.tez.androidapp.app.general.feature.notification.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.notification.callbacks.UserNotificationsCallback;
import com.tez.androidapp.commons.models.network.dto.response.UserNotificationsResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created  on 8/22/2017.
 */

public class UserNotificationsRH extends BaseRH<UserNotificationsResponse> {

    UserNotificationsCallback userNotificationsCallback;

    public UserNotificationsRH(BaseCloudDataStore baseCloudDataStore, UserNotificationsCallback userNotificationsCallback) {
        super(baseCloudDataStore);
        this.userNotificationsCallback = userNotificationsCallback;
    }

    @Override
    protected void onSuccess(Result<UserNotificationsResponse> value) {
        UserNotificationsResponse userNotificationsResponse = value.response().body();
        if (isErrorFree(userNotificationsResponse) && userNotificationsResponse!=null)
            userNotificationsCallback.onUserNotificationsSuccess(userNotificationsResponse.getRecordCount(), userNotificationsResponse.getNotifications());

        else
            userNotificationsCallback.onUserNotificationsError(userNotificationsResponse.getStatusCode(), userNotificationsResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        userNotificationsCallback.onUserNotificationsError(errorCode, message);
    }
}
