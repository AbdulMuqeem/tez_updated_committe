package com.tez.androidapp.rewamp.general.beneficiary.request;

import com.tez.androidapp.app.base.request.BaseRequest;

public class BeneficiaryRelationsRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/beneficiary/relationships";
}
