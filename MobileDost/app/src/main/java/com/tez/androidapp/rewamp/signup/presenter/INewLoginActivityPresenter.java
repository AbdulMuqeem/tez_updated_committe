package com.tez.androidapp.rewamp.signup.presenter;

import com.tez.androidapp.commons.models.network.dto.request.UserLoginRequest;

public interface INewLoginActivityPresenter {
    void callLogin(UserLoginRequest userLoginRequest);

    void resendOtp();
}
