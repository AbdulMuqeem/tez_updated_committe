package com.tez.androidapp.rewamp.advance.request.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.rewamp.advance.request.entity.TenureDetail;

import java.util.List;

public class TenureDetailsResponse extends BaseResponse {

    private List<TenureDetail> tenures;

    public List<TenureDetail> getTenures() {
        return tenures;
    }

    public void setTenures(List<TenureDetail> tenures) {
        this.tenures = tenures;
    }
}
