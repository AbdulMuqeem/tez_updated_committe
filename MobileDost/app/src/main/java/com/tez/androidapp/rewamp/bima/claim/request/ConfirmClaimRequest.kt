package com.tez.androidapp.rewamp.bima.claim.request

import com.tez.androidapp.app.base.request.BaseRequest

class ConfirmClaimRequest(val claimId: Int) : BaseRequest() {

    companion object {
        const val CLAIM_ID = "claimId"
        const val METHOD_NAME = "v1/claim/confirmLodge/{$CLAIM_ID}"
    }
}