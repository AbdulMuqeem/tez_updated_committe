package com.tez.androidapp.rewamp.general.beneficiary.request;

import com.tez.androidapp.app.base.request.BaseRequest;

public class DeleteBeneficiaryRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/beneficiary/{" + Params.ID + "}";

    public static final class Params {
        public static final String ID = "id";
    }
}
