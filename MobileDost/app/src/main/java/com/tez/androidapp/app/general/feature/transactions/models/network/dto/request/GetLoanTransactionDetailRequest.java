package com.tez.androidapp.app.general.feature.transactions.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 9/2/2017.
 */

public class GetLoanTransactionDetailRequest extends BaseRequest{
    public static final String METHOD_NAME = "v1/loan/transaction";

    private Integer id;

    public GetLoanTransactionDetailRequest(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

}
