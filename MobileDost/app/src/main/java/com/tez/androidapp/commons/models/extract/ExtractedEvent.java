package com.tez.androidapp.commons.models.extract;

import com.tez.androidapp.commons.utils.app.Utility;

import me.everything.providers.android.calendar.Event;

/**
 * Created  on 12/8/2016.
 */

public class ExtractedEvent {

    private String title;
    private String dTStart;
    private String dTend;
    private String description;
    private String allowedReminders;
    private String maxReminders;
    private String originalAllDay;

    public ExtractedEvent(String title, long dTStart, long dTend, String description, String allowedReminders, int
            maxReminders, boolean originalAllDay) {
        this.title = title;
        this.dTStart = Utility.getFormattedDate(dTStart);
        this.dTend = Utility.getFormattedDate(dTend);
        this.description = description;
        this.allowedReminders = allowedReminders;
        this.maxReminders = String.valueOf(maxReminders);
        this.originalAllDay = String.valueOf(originalAllDay);
    }

    public ExtractedEvent(Event event) {
        this.title = event.title;
        this.dTStart = Utility.getFormattedDate(event.dTStart);
        this.dTend = Utility.getFormattedDate(event.dTend);
        this.description = event.description;
        this.allowedReminders = event.allowedReminders;
        this.maxReminders = String.valueOf(event.maxReminders);
        this.originalAllDay = String.valueOf(event.originalAllDay);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getdTStart() {
        return dTStart;
    }

    public void setdTStart(String dTStart) {
        this.dTStart = dTStart;
    }

    public String getdTend() {
        return dTend;
    }

    public void setdTend(String dTend) {
        this.dTend = dTend;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAllowedReminders() {
        return allowedReminders;
    }

    public void setAllowedReminders(String allowedReminders) {
        this.allowedReminders = allowedReminders;
    }

    public String getMaxReminders() {
        return maxReminders;
    }

    public void setMaxReminders(String maxReminders) {
        this.maxReminders = maxReminders;
    }

    public String getOriginalAllDay() {
        return originalAllDay;
    }

    public void setOriginalAllDay(String originalAllDay) {
        this.originalAllDay = originalAllDay;
    }
}
