package com.tez.androidapp.app.vertical.bima.add.beneficiary.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks.SetBeneficiaryCallback;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created  on 8/25/2017.
 */

public class SetBeneficiaryRH extends BaseRH<BaseResponse> {

    private final SetBeneficiaryCallback setBeneficiaryCallback;

    public SetBeneficiaryRH(BaseCloudDataStore baseCloudDataStore, SetBeneficiaryCallback setBeneficiaryCallback) {
        super(baseCloudDataStore);
        this.setBeneficiaryCallback = setBeneficiaryCallback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse response = value.response().body();
        if (response != null) {
            if (isErrorFree(response))
                setBeneficiaryCallback.onSetBeneficiarySuccess();
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());
        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        setBeneficiaryCallback.onSetBeneficiaryFailure(errorCode, message);
    }
}
