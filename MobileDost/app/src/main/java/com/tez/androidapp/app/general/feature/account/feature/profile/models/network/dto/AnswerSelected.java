package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto;

import android.text.TextUtils;

import androidx.annotation.Nullable;

public class AnswerSelected {
    private String questionReference;
    private String answerReference;
    private String answer;

    public AnswerSelected(String questionReference, Answer answerSelected){
        this.questionReference = questionReference;
        this.answerReference = answerSelected.getRefName();
        this.answer = answerSelected.getText();
    }

    public String getQuestionReference() {
        return questionReference;
    }

    public void setQuestionReference(String questionReference) {
        this.questionReference = questionReference;
    }

    public String getAnswerReference() {
        return answerReference;
    }

    public void setAnswerReference(String answerReference) {
        this.answerReference = answerReference;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return obj instanceof AnswerSelected
                && TextUtils.equals(((AnswerSelected) obj).answerReference, answerReference)
                && TextUtils.equals(((AnswerSelected) obj).questionReference, questionReference);
    }
}
