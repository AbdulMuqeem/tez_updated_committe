package com.tez.androidapp.rewamp.general.beneficiary.listener;

import com.tez.androidapp.rewamp.general.beneficiary.entity.BeneficiaryRelation;

import java.util.List;

public interface GetBeneficiaryRelationsListener {

    void onGetBeneficiaryRelationSuccess(List<BeneficiaryRelation> relationList);

    void onGetBeneficiaryRelationFailure(int errorCode, String errorDescription);
}
