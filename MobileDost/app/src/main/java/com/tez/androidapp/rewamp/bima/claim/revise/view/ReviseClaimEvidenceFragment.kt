package com.tez.androidapp.rewamp.bima.claim.revise.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import com.tez.androidapp.R
import com.tez.androidapp.app.base.ui.fragments.BaseFragment
import com.tez.androidapp.commons.utils.app.Utility
import com.tez.androidapp.commons.widgets.TezButton
import com.tez.androidapp.commons.widgets.TezTextView
import com.tez.androidapp.rewamp.bima.claim.common.document.adapter.EvidenceAdapter
import com.tez.androidapp.rewamp.bima.claim.common.document.router.UploadDocumentActivityRouter
import com.tez.androidapp.rewamp.bima.claim.common.document.view.UploadDocumentActivity
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Failure
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Loading
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Success
import com.tez.androidapp.rewamp.bima.claim.revise.listener.ReviseClaimListener
import com.tez.androidapp.rewamp.bima.claim.revise.viewmodel.ReviseClaimEvidenceViewModel
import kotlinx.android.synthetic.main.fragment_revise_claim_evidence.*
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener

class ReviseClaimEvidenceFragment : BaseFragment(), EvidenceAdapter.OnEvidenceUpdateListener {

    private val viewModel: ReviseClaimEvidenceViewModel by activityViewModels()
    private val args: ReviseClaimEvidenceFragmentArgs by navArgs()
    private lateinit var listener: ReviseClaimListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as ReviseClaimListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_revise_claim_evidence, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        initComments(view.context)
    }

    private fun initObservers() {
        viewModel.evidenceListLiveData.observe(viewLifecycleOwner, { evidenceList ->
            args.rejectedDocumentIds?.let { rejectedDocumentIds ->
                val ids = rejectedDocumentIds.split(",")
                evidenceList.filter { ids.contains(it.id.toString()) }
                        .forEach {
                            it.rejected = true
                        }
            }
            val enable = evidenceList.find { it.rejected && it.evidenceFileList.isEmpty() } == null
            setButton(enable)
            val adapter = EvidenceAdapter(evidenceList, this)
            rvClaimDocuments.adapter = adapter
            viewModel.initiateQueue(evidenceList)
        })
        viewModel.networkCallMutableLiveData.observe(viewLifecycleOwner, {
            when (it) {
                is Loading -> {
                    showTezLoader()
                }
                is Failure -> {
                    dismissTezLoader()
                    showError(it.errorCode)
                }
                is Success -> {

                }
            }
        })

        viewModel.confirmReviseClaimLiveData.observe(viewLifecycleOwner) {
            listener.onClaimSubmitted(it.insuranceClaimConfirmationDto)
        }
    }

    private fun initComments(context: Context) {
        tllComments.visibility = if (args.comments.isNullOrEmpty()) View.GONE else View.VISIBLE
        args.comments?.forEach {
            val textView = TezTextView(context)
            textView.text = it
            textView.textSize = 12f
            textView.setTypeface(R.font.barlow_regular)
            textView.setTextColor(Utility.getColorFromResource(R.color.textViewTextColorBlack))
            textView.compoundDrawablePadding = Utility.dpToPx(12f)
            textView.changeDrawable(textView, R.drawable.ic_bullet_dot_black, 0, 0, 0)
            val params = LinearLayoutCompat.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                    LinearLayoutCompat.LayoutParams.WRAP_CONTENT)
            params.topMargin = Utility.dpToPx(4f)
            textView.layoutParams = params
            tllComments.addView(textView)
        }
    }

    private fun setButton(enable: Boolean) {
        btResubmitClaim.setButtonBackgroundStyle(if (enable) TezButton.ButtonStyle.NORMAL
        else TezButton.ButtonStyle.INACTIVE)
        btResubmitClaim.setDoubleTapSafeOnClickListener(if (enable) DoubleTapSafeOnClickListener {
            viewModel.resubmitClaim(args.claimId)
        } else null)
    }

    override fun onEditEvidence(docId: Int) =
            routeUploadDocument(docId, UploadDocumentActivity.Source.EDIT)

    override fun onCaptureImage(docId: Int) =
            routeUploadDocument(docId, UploadDocumentActivity.Source.CAMERA)

    override fun onUploadDocument(docId: Int) =
            routeUploadDocument(docId, UploadDocumentActivity.Source.DOCUMENT)

    private fun routeUploadDocument(docId: Int, source: UploadDocumentActivity.Source) =
            UploadDocumentActivityRouter()
                    .setDependenciesAndRoute(this,
                            docId,
                            viewModel.getEvidenceFileList(docId),
                            source)

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == UploadDocumentActivityRouter.REQUEST_CODE_UPLOAD_DOCUMENT_ACTIVITY
                && resultCode == Activity.RESULT_OK
                && data != null) {
            val result = UploadDocumentActivityRouter.getResult(data)
            viewModel.updateEvidenceFileList(result.docId, result.evidenceFileList)
        }
    }
}