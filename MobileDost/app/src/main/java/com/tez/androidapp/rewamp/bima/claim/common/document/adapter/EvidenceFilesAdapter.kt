package com.tez.androidapp.rewamp.bima.claim.common.document.adapter

import android.net.Uri
import android.view.View
import android.view.ViewGroup
import com.tez.androidapp.R
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.EvidenceFile
import com.tez.androidapp.commons.utils.authorized.alert.image.popup.DefaultUIForAlertImage
import com.tez.androidapp.commons.utils.authorized.alert.image.popup.RenderAlertImageOnUI
import com.tez.androidapp.rewamp.bima.claim.common.document.adapter.EvidenceFilesAdapter.EvidenceFileListener
import com.tez.androidapp.rewamp.bima.claim.common.document.adapter.EvidenceFilesAdapter.EvidenceFileViewHolder
import kotlinx.android.synthetic.main.list_item_evidence_file.view.*
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener
import net.tez.tezrecycler.base.viewholder.BaseViewHolder
import java.io.File

class EvidenceFilesAdapter(val docId: Int, items: MutableList<EvidenceFile>, listener: EvidenceFileListener?)
    : GenericRecyclerViewAdapter<EvidenceFile, EvidenceFileListener?, EvidenceFileViewHolder?>(items, listener) {

    private var selectedPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EvidenceFileViewHolder {
        val view = inflate(parent.context, R.layout.list_item_evidence_file, parent)
        return EvidenceFileViewHolder(view)
    }

    override fun onBindViewHolder(holder: EvidenceFileViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.onBind(position, items, listener)
    }

    fun deleteSelected() {
        removeAt(selectedPosition)
        if (selectedPosition == itemCount && itemCount > 0)
            selectedPosition--
        notifyItemRangeChanged(0, itemCount)
    }

    fun replaceSelected(evidenceFile: EvidenceFile) =
            set(selectedPosition, evidenceFile)

    interface EvidenceFileListener : BaseRecyclerViewListener {
        fun onClickEvidenceFileThumbnail(evidenceFile: EvidenceFile)
    }

    inner class EvidenceFileViewHolder(itemView: View) : BaseViewHolder<EvidenceFile, EvidenceFileListener?>(itemView) {

        override fun onBind(position: Int, items: List<EvidenceFile>, listener: EvidenceFileListener?) {
            super.onBind(position, items, listener)
            val item = items[position]
            loadImage(item.path)
            itemView.viewOverlay.visibility = if (selectedPosition == position) View.GONE else View.VISIBLE
            itemView.setOnClickListener { notify(position) }
            if (selectedPosition == position)
                listener?.onClickEvidenceFileThumbnail(item)
        }

        private fun loadImage(path: String) {
            if (File(path).extension == "pdf") {
                itemView.ivThumbnail.setImageResource(R.drawable.ic_pdf_preview)
            } else {
                val uiForAlertImage = DefaultUIForAlertImage(itemView.ivThumbnail)
                RenderAlertImageOnUI.renderDefaultImageUIAlert(uiForAlertImage, Uri.parse(path), path)
            }
        }

        private fun notify(position: Int) {
            if (selectedPosition != position) {
                val oldPosition = selectedPosition
                selectedPosition = position
                notifyItemChanged(oldPosition)
                notifyItemChanged(position)
            }
        }
    }
}