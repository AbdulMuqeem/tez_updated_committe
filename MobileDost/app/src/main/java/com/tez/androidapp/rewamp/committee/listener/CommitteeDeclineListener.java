package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommitteeDeclineResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface CommitteeDeclineListener {

    void onCommitteeDeclineSuccess(CommitteeDeclineResponse committeeDeclineResponse);

    void onCommitteeDeclineeFailure(int errorCode, String message);
}
