package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 3/2/2017.
 */

public class UploadDynamicRequest extends BaseRequest{
    public static final String METHOD_NAME = "user/upload/dynamic";

    public abstract static class Params {
        public static final String FILE = "file";
        public static final String IMEI = "imei";
        public static final String START_DATE = "startDate";
        public static final String END_DATE = "endDate";
        public static final String PRINCIPAL_NAME = "principalName";
    }
}
