package com.tez.androidapp.rewamp.util;

import android.text.Editable;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.utils.textwatchers.TezTextWatcher;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezTextView;

/**
 * Created by Vinod Kumar
 * <p>
 * Note: {@link AmountFormatterTextWatcher} will only work on amount of up to 6 figures.
 * It should not be used if amount exceeds the 6 figures.
 * </p>
 **/
public class AmountFormatterTextWatcher implements TezTextWatcher {

    @NonNull
    private TezEditTextView etAmount;

    @NonNull
    private TezTextView tvRs;

    public AmountFormatterTextWatcher(@NonNull TezTextView tvRs, @NonNull TezEditTextView etAmount) {
        this.etAmount = etAmount;
        this.tvRs = tvRs;
    }

    @CallSuper
    @Override
    public void afterTextChanged(Editable s) {
        tvRs.setTextColor(s.length() > 0 ? Utility.getColorFromResource(R.color.textViewTitleColorBlue) : Utility.getColorFromResource(R.color.editTextBottomLineColorGrey));
        String s1 = s.toString().replace(",", "");
        if (isBadShortString(s, s1)) {
            etAmount.setText(s1);
            etAmount.setSelection(s1.length());
        } else if (isBadlyFormatted(s)) {
            String formattedAmount = Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(Utility.parseAmount(s.toString()));
            etAmount.setText(formattedAmount);
            etAmount.setSelection(Math.min(formattedAmount.length(), 7));
        }

        onAmountEntered(Utility.getPatternAmount(s1));
    }

    private boolean isBadShortString(Editable s, String s1) {
        return s.length() <= 4 && s.toString().contains(",") && s1.length() <= 3;
    }

    private boolean isBadlyFormatted(Editable s) {
        int length = s.length();
        return (length == 4 || length == 5) && s.charAt(1) != ','
                || length == 6 && s.charAt(2) != ','
                || length == 7 && s.charAt(3) != ',';
    }

    protected void onAmountEntered(double amount) {

    }
}
