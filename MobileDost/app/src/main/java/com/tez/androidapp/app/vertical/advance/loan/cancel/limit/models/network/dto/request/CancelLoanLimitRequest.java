package com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Rehman Murad Ali on 8/30/2017.
 */

public class CancelLoanLimitRequest extends BaseRequest{
    public static final String METHOD_NAME = "v1/loan/limit/cancel";

    Integer loanId;
    Integer cancellationReasonId;

    public CancelLoanLimitRequest(Integer loanId, Integer cancellationReasonId) {
        this.loanId = loanId;
        this.cancellationReasonId = cancellationReasonId;
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public Integer getCancellationReasonId() {
        return cancellationReasonId;
    }

    public void setCancellationReasonId(Integer cancellationReasonId) {
        this.cancellationReasonId = cancellationReasonId;
    }

    @Override
    public String toString() {
        return "CancelLoanLimitRequest{" +
                "loanId=" + loanId +
                ", cancellationReasonId='" + cancellationReasonId + '\'' +
                '}';
    }
}
