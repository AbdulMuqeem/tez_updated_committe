package com.tez.androidapp.rewamp.bima.claim.health.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tez.androidapp.R
import com.tez.androidapp.rewamp.general.purpose.Purpose
import kotlin.random.Random
import kotlin.random.nextInt

class ReasonViewModel : ViewModel() {

    val purposeListLiveData = MutableLiveData(getRandomOrderReasons())
    var selectedReasonPosition: Int = -1

    internal fun getRandomOrderReasons(): List<Purpose> {
        val list = mutableListOf(
                Purpose(285, R.string.breathing_issue),
                Purpose(286, R.string.dengue_malaria),
                Purpose(287, R.string.diarrhea),
                Purpose(288, R.string.burn_poisoning),
                Purpose(289, R.string.cancer_hiv),
                Purpose(290, R.string.heart_condition),
                Purpose(291, R.string.heart_stroke),
                Purpose(292, R.string.road_accident),
                Purpose(293, R.string.pneumonia),
        )
        val random = Random(System.currentTimeMillis())
        val listSize = list.size
        val randomList = MutableList(listSize) { index ->
            val randomIndex = random.nextInt(IntRange(0, listSize - 1 - index))
            list.removeAt(randomIndex)
        }
        randomList.add(Purpose(OTHER_REASON, R.string.other))
        return randomList
    }

    companion object {
        const val OTHER_REASON: Int = 295
    }
}