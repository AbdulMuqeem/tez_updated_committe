package com.tez.androidapp.rewamp.bima.claim.common.summary.view

import android.app.Activity
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import com.tez.androidapp.R
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.Evidence
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback
import com.tez.androidapp.commons.managers.MDPreferenceManager
import com.tez.androidapp.commons.utils.app.Constants
import com.tez.androidapp.commons.utils.app.Utility
import com.tez.androidapp.commons.widgets.AddWalletCardView
import com.tez.androidapp.rewamp.bima.claim.common.document.adapter.ReadOnlyEvidenceAdapter
import com.tez.androidapp.rewamp.bima.claim.common.document.router.UploadDocumentActivityRouter
import com.tez.androidapp.rewamp.bima.claim.common.document.view.UploadDocumentActivity
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Failure
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Loading
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Success
import com.tez.androidapp.rewamp.bima.claim.common.summary.adapter.SummaryAdapter
import com.tez.androidapp.rewamp.bima.claim.common.summary.model.Summary
import com.tez.androidapp.rewamp.bima.claim.common.summary.viewmodel.SummaryViewModel
import com.tez.androidapp.rewamp.bima.claim.health.base.BaseClaimStepFragment
import com.tez.androidapp.rewamp.bima.claim.health.viewmodel.ReasonViewModel
import com.tez.androidapp.rewamp.bima.claim.request.LodgeClaimRequest
import com.tez.androidapp.rewamp.general.wallet.router.AddWalletActivityRouter
import com.tez.androidapp.rewamp.general.wallet.router.ChangeWalletActivityRouter
import kotlinx.android.synthetic.main.fragment_summary.*
import kotlinx.android.synthetic.main.fragment_summary.view.*

/**
 * Created by Vinod Kumar on 4/20/2020
 */
class SummaryFragment : BaseClaimStepFragment(), ReadOnlyEvidenceAdapter.ReadOnlyEvidenceListener, LocationAvailableCallback {

    private val args: SummaryFragmentArgs by navArgs()
    private val adapter = SummaryAdapter()
    private val model: SummaryViewModel by activityViewModels()


    override fun initView(baseView: View, savedInstanceState: Bundle?) {
        baseView.rvSummary.adapter = adapter
        claimSharedViewModel.run {
            amountLiveData.observe(viewLifecycleOwner, {
                adapter.add(Summary(Utility.getStringFromResource(R.string.claim_amount),
                        baseView.context.getString(R.string.rs_value_string,
                                Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(it.toDouble())),
                        R.drawable.ic_pin_active))
            })
            reasonLiveData.observe(viewLifecycleOwner, {
                val reason = if (it.id == ReasonViewModel.OTHER_REASON && !it.other.isNullOrEmpty()) it.other!!
                else Utility.getStringFromResource(it.titleRes)
                adapter.add(Summary(Utility.getStringFromResource(R.string.reason_of_hospitalization),
                        reason,
                        R.drawable.ic_pin_inactive))
            })
            claimQuestionListLiveData.observe(viewLifecycleOwner, { questions ->

                adapter.add(Summary(questions.dateOfAdmission.question,
                        questions.dateOfAdmission.date!!,
                        R.drawable.ic_calendar))

                adapter.add(Summary(questions.dateOfDischarge.question,
                        questions.dateOfDischarge.date!!,
                        R.drawable.ic_calendar))

                questions.locationOfHospital.hospitalName?.let {
                    if (it.isNotEmpty())
                        adapter.add(Summary(Utility.getStringFromResource(R.string.name_of_hospital),
                                it,
                                R.drawable.ic_pin_inactive))
                }

                questions.locationOfHospital.hospitalNameMap?.let {
                    if (it.isNotEmpty())
                        adapter.add(Summary(questions.locationOfHospital.question,
                                it,
                                R.drawable.ic_place))
                }

                questions.locationOfHospital.nearestLandMark?.let {
                    if (it.isNotEmpty())
                        adapter.add(Summary(Utility.getStringFromResource(R.string.nearest_landmark),
                                it,
                                R.drawable.ic_pin_inactive))
                }

                adapter.add(Summary(Utility.getStringFromResource(R.string.date_of_claim_submission),
                        Utility.getCurrentDateInUIFormat(),
                        R.drawable.ic_calendar))
            })

            claimDocumentListLiveData.observe(viewLifecycleOwner, {
                rvClaimDocuments.adapter = ReadOnlyEvidenceAdapter(it, this@SummaryFragment)
                model.initiateQueue(it)
            })
        }

        model.walletLiveData.observe(viewLifecycleOwner) { wallet: Wallet? ->
            setNavigationOnBtContinue(wallet != null)
            cvAddWallet.wallet = wallet
        }

        model.networkCallMutableLiveData.observe(viewLifecycleOwner) {
            when (it) {
                is Loading -> showTezLoader()
                is Success -> dismissTezLoader()
                is Failure -> {
                    dismissTezLoader()
                    showError(it.errorCode)
                }
            }
        }

        model.lodgeClaimNetworkLiveData.observe(viewLifecycleOwner) {
            when (it) {
                is Loading -> showTezLoader()
                is Failure -> {
                    dismissTezLoader()
                    showError(it.errorCode)
                }
                Success -> {
                }
            }
        }

        model.confirmClaimLiveData.observe(viewLifecycleOwner) {
            listener.onClaimSubmitted(it.insuranceClaimConfirmationDto)
        }

        baseView.cvAddWallet.setChangeWalletListener(object : AddWalletCardView.ChangeWalletListener {

            override fun onAddWallet() {
                AddWalletActivityRouter.createInstance()
                        .setDependenciesAndRouteForResult(this@SummaryFragment)
            }

            override fun onChangeWallet() {
                ChangeWalletActivityRouter.createInstance()
                        .setDependenciesAndRouteForResult(this@SummaryFragment)
            }
        })

        model.getWallet()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ChangeWalletActivityRouter.REQUEST_CODE_CHANGE_WALLET -> {
                    model.walletLiveData.value = data?.getParcelableExtra(ChangeWalletActivityRouter.RESULT_DATA_WALLET)
                }

                AddWalletActivityRouter.REQUEST_CODE_ADD_WALLET -> {
                    model.getWallet()
                }
            }
        }
    }

    override fun onClickEvidence(evidence: Evidence) {
        UploadDocumentActivityRouter()
                .setDependenciesAndRoute(this,
                        evidence.id,
                        evidence.evidenceFileList,
                        UploadDocumentActivity.Source.READ_ONLY)
    }

    override fun onClickContinue() {
        model.claimId?.let { model.uploadDocument(it) }
                ?: getCurrentLocation(this)
    }

    override fun onLocationRequestInitialized() {
        super.onLocationRequestInitialized()
        showTezLoader()
    }

    override fun onLocationPermissionDenied() {
        super.onLocationPermissionDenied()
        showInformativeMessage(R.string.string_location_permission_required)
    }

    override fun onLocationFailed(message: String?) {
        super.onLocationFailed(message)
        dismissTezLoader()
        showInformativeMessage(R.string.string_location_not_fetch)
    }

    override fun onLocationAvailable(location: Location) {
        MDPreferenceManager.setLastLocation(location.latitude, location.longitude)
        val locationQuestion = claimSharedViewModel.claimQuestionListLiveData.value!!.locationOfHospital
        val admissionDate = claimSharedViewModel.claimQuestionListLiveData.value!!.dateOfAdmission.date!!
        val dischargeDate = claimSharedViewModel.claimQuestionListLiveData.value!!.dateOfDischarge.date!!
        val other = claimSharedViewModel.reasonLiveData.value?.other
        val lodgeClaimRequest = LodgeClaimRequest(
                policyId = args.policyId,
                amount = claimSharedViewModel.amountLiveData.value!!,
                reasonId = claimSharedViewModel.reasonLiveData.value?.id,
                otherReason = if (other.isNullOrEmpty()) null else other,
                dateOfAdmission = Utility.getFormattedDate(admissionDate, Constants.UI_DATE_FORMAT, Constants.BACKEND_DATE_FORMAT),
                dateOfDischarge = Utility.getFormattedDate(dischargeDate, Constants.UI_DATE_FORMAT, Constants.BACKEND_DATE_FORMAT),
                hospitalName = locationQuestion.hospitalName!!,
                hospitalNameMap = if (locationQuestion.hospitalNameMap.isNullOrEmpty()) null else locationQuestion.hospitalNameMap,
                hospitalLocation = if (locationQuestion.hospitalLocation.isNullOrEmpty()) null else locationQuestion.hospitalLocation,
                nearestLandmark = if (locationQuestion.nearestLandMark.isNullOrEmpty()) null else locationQuestion.nearestLandMark,
                mobileUserAccountId = model.walletLiveData.value?.mobileAccountId!!,
                claimantLocation = "${location.latitude},${location.longitude}",
        )

        model.lodgeClaim(lodgeClaimRequest)
    }

    override fun onDestroy() {
        super.onDestroy()
        model.lodgeClaimNetworkLiveData.value = null
    }

    @get:LayoutRes
    override val layoutResId: Int
        get() = R.layout.fragment_summary

    override val stepNumber: Int
        get() = args.stepNumber
}