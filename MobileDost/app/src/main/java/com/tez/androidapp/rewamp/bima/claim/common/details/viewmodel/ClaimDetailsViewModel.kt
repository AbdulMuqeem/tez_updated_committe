package com.tez.androidapp.rewamp.bima.claim.common.details.viewmodel

import androidx.lifecycle.MutableLiveData
import com.tez.androidapp.app.vertical.bima.claims.callback.ClaimDetailsCallBack
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.ClaimDetailDto
import com.tez.androidapp.commons.utils.app.Utility
import com.tez.androidapp.repository.network.store.BimaCloudDataStore
import com.tez.androidapp.rewamp.bima.claim.callback.InitiateReviseClaimCallback
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Failure
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Loading
import com.tez.androidapp.rewamp.bima.claim.common.network.model.NetworkState
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Success
import com.tez.androidapp.rewamp.bima.claim.common.network.viewmodel.NetworkBoundViewModel
import com.tez.androidapp.rewamp.bima.claim.response.InitiateReviseClaimResponse

class ClaimDetailsViewModel : NetworkBoundViewModel() {

    private var claimDetailsLiveData: MutableLiveData<ClaimDetailDto>? = null
    val initiateReviseClaimLiveData: MutableLiveData<InitiateReviseClaimResponse> = MutableLiveData()
    val initiateReviseClaimNetworkLiveData: MutableLiveData<NetworkState> = MutableLiveData()

    fun getClaimDetailsLiveData(claimId: Int): MutableLiveData<ClaimDetailDto> {
        if (claimDetailsLiveData == null) {
            claimDetailsLiveData = MutableLiveData()
            getClaimDetails(claimId)
        }
        return claimDetailsLiveData!!
    }

    private fun getClaimDetails(claimId: Int) {
        networkCallMutableLiveData.value = Loading
        BimaCloudDataStore.getInstance().getClaimDetails(claimId, object : ClaimDetailsCallBack {
            override fun onMyClaimSuccess(claimDetailDto: ClaimDetailDto) {
                networkCallMutableLiveData.value = Success
                claimDetailsLiveData!!.value = claimDetailDto
            }

            override fun onMyClaimFailure(errorCode: Int, message: String?) {
                networkCallMutableLiveData.value = Failure(errorCode)
            }
        })
    }

    fun initiateReviseClaim(claimId: Int) {
        initiateReviseClaimNetworkLiveData.value = Loading
        BimaCloudDataStore.getInstance().initiateReviseClaim(claimId, object : InitiateReviseClaimCallback {
            override fun onInitiateReviseClaimSuccess(response: InitiateReviseClaimResponse) {
                initiateReviseClaimNetworkLiveData.value = Success
                initiateReviseClaimLiveData.value = response
            }

            override fun onInitiateReviseClaimFailure(errorCode: Int, message: String?) {
                if (Utility.isUnauthorized(errorCode))
                    initiateReviseClaim(claimId)
                else
                    initiateReviseClaimNetworkLiveData.value = Failure(errorCode)
            }

        })
    }
}