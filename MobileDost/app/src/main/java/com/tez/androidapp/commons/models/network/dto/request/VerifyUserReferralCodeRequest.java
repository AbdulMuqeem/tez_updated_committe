package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by FARHAN DHANANI on 11/1/2018.
 */
public class VerifyUserReferralCodeRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/verify/referralCode/{" + Params.REF_CODE + "}";

    public abstract static class Params {
        public static final String REF_CODE = "code";
    }
}
