package com.tez.androidapp.rewamp.advance.request.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;

public interface ISelectLoanActivityInteractor extends IBaseInteractor {

    void getLoanLimit();

    void getLoanDetails();
}
