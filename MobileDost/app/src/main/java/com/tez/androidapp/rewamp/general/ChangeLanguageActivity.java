package com.tez.androidapp.rewamp.general;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.locale.LocaleHelper;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.signup.SelectLanguageActivity;
import com.tez.androidapp.rewamp.signup.presenter.ChangeLanguageActivityPresenter;
import com.tez.androidapp.rewamp.signup.presenter.IChangeLanguageActivityPresenter;
import com.tez.androidapp.rewamp.signup.view.IChangeLanguageActivityView;

public class ChangeLanguageActivity extends SelectLanguageActivity implements IChangeLanguageActivityView {

    private final IChangeLanguageActivityPresenter iChangeLanguageActivityPresenter;


    public ChangeLanguageActivity() {
        this.iChangeLanguageActivityPresenter = new ChangeLanguageActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enableToolbarBackButton();
    }

    @Override
    protected boolean isEnableButtonOnStart() {
        return false;
    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        this.setEnableButtons(false);
        switch (view.getId()) {

            case R.id.buttonUrdu:
                iChangeLanguageActivityPresenter.changeLanguage(LocaleHelper.URDU);
                break;

            case R.id.buttonRomanUrdu:
                iChangeLanguageActivityPresenter.changeLanguage(LocaleHelper.ROMAN);
                break;

            case R.id.buttonEnglish:
                iChangeLanguageActivityPresenter.changeLanguage(LocaleHelper.ENGLISH);
                break;

        }
    }

    @Override
    protected void setButtonSelected(TezButton button) {
        super.setButtonSelected(button);
        button.setDoubleTapSafeOnClickListener(null);
        button.setEnabled(false);
    }

    @Override
    protected void setButtonWhite(TezButton button) {
        super.setButtonWhite(button);
        button.setDoubleTapSafeOnClickListener(this);
        button.setEnabled(true);
    }

    @Override
    public void onUpdateLanguageSuccess(@NonNull String language) {
        LocaleHelper.updateLanguage(this, language);
        routeToDashboard();
    }

    @Override
    public void onUpdateLanguageFailure() {
        this.setEnableButtons(true);
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    private void routeToDashboard() {
        DashboardActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    protected void checkIfSetSelectedLanguage() {
        setSelectedLanguage();
    }

    @Override
    protected void setChangeIpListener() {

    }

    @Override
    protected String getScreenName() {
        return "ChangeLanguageActivity";
    }
}
