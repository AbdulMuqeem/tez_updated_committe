package com.tez.androidapp.rewamp.general.beneficiary.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.beneficiary.view.BeneficiaryListActivity;

public class BeneficiaryListActivityRouter extends BaseActivityRouter {

    public static final String MOBILE_USER_BENEFICIARY_ID = "MOBILE_USER_BENEFICIARY_ID";
    public static final int REQUEST_CODE_BENEFICIARY_LIST = 1109;


    public static BeneficiaryListActivityRouter createInstance() {
        return new BeneficiaryListActivityRouter();
    }

    public void setDependenciesAndRouteForResult(@NonNull BaseActivity from, Integer mobileUserInsurancePolicyBeneficiaryId) {
        Intent intent = createIntent(from);
        intent.putExtra(MOBILE_USER_BENEFICIARY_ID, mobileUserInsurancePolicyBeneficiaryId);
        routeForResult(from, intent, REQUEST_CODE_BENEFICIARY_LIST);
    }

    public void setDependenciesAndRouteForResult(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        routeForResult(from, intent, REQUEST_CODE_BENEFICIARY_LIST);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, BeneficiaryListActivity.class);
    }
}
