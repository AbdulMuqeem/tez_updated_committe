package com.tez.androidapp.commons.location.models.network;

import java.io.Serializable;

/**
 * Created  on 2/9/2017.
 */

public class City implements Serializable {

    Integer id;
    String name;

    public City(String name){
        this.name = name;
    }

    public City(Integer id, String name){
        this.name = name;
        this.id = id;
    }

    public Integer getCityId() {
        return id;
    }

    public String getCityName() {
        return name;
    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
