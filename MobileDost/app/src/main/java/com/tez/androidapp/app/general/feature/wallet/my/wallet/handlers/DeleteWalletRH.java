package com.tez.androidapp.app.general.feature.wallet.my.wallet.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks.DeleteWalletCallback;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsRH;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsResponse;

/**
 * Created by Rehman Murad Ali on 12/28/2017.
 */

public class DeleteWalletRH extends DashboardCardsRH<DashboardCardsResponse> {

    private DeleteWalletCallback deleteWalletCallback;

    public DeleteWalletRH(BaseCloudDataStore baseCloudDataStore, DeleteWalletCallback deleteWalletCallback) {
        super(baseCloudDataStore);
        this.deleteWalletCallback = deleteWalletCallback;
    }

    @Override
    protected void onSuccess(Result<DashboardCardsResponse> value) {
        super.onSuccess(value);
        DashboardCardsResponse baseResponse = value.response().body();
        if (baseResponse != null) {
            if (isErrorFree(baseResponse))
                deleteWalletCallback.onDeleteWalletSuccess(baseResponse);
            else
                onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        deleteWalletCallback.onDeleteWalletFailure(errorCode, message);
    }
}
