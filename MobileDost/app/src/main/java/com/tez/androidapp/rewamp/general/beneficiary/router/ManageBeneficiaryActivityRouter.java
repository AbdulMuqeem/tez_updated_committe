package com.tez.androidapp.rewamp.general.beneficiary.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.beneficiary.view.ManageBeneficiaryActivity;

/**
 * Created by VINOD KUMAR on 8/30/2019.
 */
public class ManageBeneficiaryActivityRouter extends BaseActivityRouter {


    public static ManageBeneficiaryActivityRouter createInstance() {
        return new ManageBeneficiaryActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, ManageBeneficiaryActivity.class);
    }
}
