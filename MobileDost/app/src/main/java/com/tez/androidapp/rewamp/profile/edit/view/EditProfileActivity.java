package com.tez.androidapp.rewamp.profile.edit.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.google.android.material.textfield.TextInputLayout;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.models.app.City;
import com.tez.androidapp.commons.models.network.User;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.validators.annotations.TextInputLayoutNotEmpty;
import com.tez.androidapp.commons.validators.annotations.TextInputLayoutRegex;
import com.tez.androidapp.commons.widgets.TezAutoCompleteTextView;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.general.profile.router.MyProfileActivityRouter;
import com.tez.androidapp.rewamp.profile.edit.presenter.EditProfileActivityPresenter;
import com.tez.androidapp.rewamp.profile.edit.presenter.IEditProfileActivityPresenter;
import com.tez.androidapp.rewamp.profile.edit.router.EditProfileVerificationActivityRouter;

import net.tez.camera.cameras.CameraProperties;
import net.tez.camera.request.CameraRequestBuilder;
import net.tez.filepicker.request.image.ImagePickerRequestBuilder;
import net.tez.logger.library.utils.TextUtil;
import net.tez.validator.library.annotations.Order;
import net.tez.validator.library.core.FieldValidator;
import net.tez.validator.library.core.FilterChain;
import net.tez.validator.library.core.ValidationError;
import net.tez.validator.library.core.ValidationListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class EditProfileActivity extends BaseActivity
        implements IEditProfileActivityView, ValidationListener {

    @TextInputLayoutRegex(regex = Constants.PHONE_NUMBER_VALIDATOR_REGEX, value = {1, R.string.string_please_enter_valid_phone_number})
    @Order(1)
    @BindView(R.id.tilMobileNumber)
    private TextInputLayout tilMobileNumber;

    @TextInputLayoutRegex(regex = Constants.STRING_EMAIL_VALIDATOR_REGEX, value = {1, R.string.string_input_valid_email})
    @Order(2)
    @BindView(R.id.tilEmail)
    private TextInputLayout tilEmail;

    @TextInputLayoutNotEmpty(value = {1, R.string.string_input_valid_current_address})
    @Order(3)
    @BindView(R.id.tilCurrentCity)
    private TezTextInputLayout tilCurrentCity;

    @TextInputLayoutNotEmpty(value = {1, R.string.string_input_valid_current_address})
    @Order(4)
    @BindView(R.id.tilCurrentAddress)
    private TextInputLayout tilCurrentAddress;

    @BindView(R.id.etMobileNumber)
    private TezEditTextView etMobileNumber;

    @BindView(R.id.etEmail)
    private TezEditTextView etEmail;

    @BindView(R.id.etCurrentAddress)
    private TezEditTextView etCurrtentAddress;

    @BindView(R.id.autoCompleteTextViewCurrentCity)
    private TezAutoCompleteTextView autoCompleteTextViewCurrentCity;

    @BindView(R.id.btContinue)
    private TezButton btContinue;

    @BindView(R.id.shimmer)
    private TezLinearLayout tlShimmer;

    @BindView(R.id.tvEditPicure)
    private TezTextView tvEditPicure;

    @BindView(R.id.ivUserImage)
    private TezImageView ivUserImage;

    private final IEditProfileActivityPresenter iEditProfileActivityPresenter;

    private CompositeDisposable allDisposables;

    private City selectedCity;

    public EditProfileActivity() {
        this.iEditProfileActivityPresenter = new EditProfileActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ViewBinder.bind(this);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.allDisposables = new CompositeDisposable();
        listenChangesOnViews();
    }

    @Override
    protected void onPause() {
        super.onPause();
        allDisposables.dispose();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null && requestCode == ImagePickerRequestBuilder.REQUEST_CODE_IMAGE) {
            this.onImageAvailable(data.getStringArrayListExtra(ImagePickerRequestBuilder.RESULT_IMAGE_ARRAY_LIST));
        }
    }

    @Override
    public void setDoubleTapSafeOnClickListenerOnTvEditPicture() {
        this.tvEditPicure.setDoubleTapSafeOnClickListener(this::createImagePickerUtility);
    }

    private void listenChangesOnViews() {
        allDisposables.add(RxTextView.textChanges(this.etMobileNumber).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));
        allDisposables.add(RxTextView.textChanges(this.etCurrtentAddress).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));
        allDisposables.add(RxTextView.textChanges(this.etEmail).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));
        allDisposables.add(RxTextView.textChanges(this.autoCompleteTextViewCurrentCity).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));
    }

    private void init() {
        this.iEditProfileActivityPresenter.loadProfile();
        loadProfilePicture();
    }

    private void loadProfilePicture() {
        Utility.loadProfilePicture(ivUserImage);
    }

    @Override
    public void populateDropDown(@NonNull List<City> cities) {
        ArrayAdapter<City> cityArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, cities);
        this.autoCompleteTextViewCurrentCity.setAdapter(cityArrayAdapter);
        setOnItemClickListenerToAutoCompleteTextViewCurrentCity(cityArrayAdapter);
    }

    private void setSelectedCity(City selectedCity) {
        this.selectedCity = selectedCity;
    }

    @Override
    public void setDoubleTapSafeOnClickListenerToAutoCompleteTextViewCity() {
        this.autoCompleteTextViewCurrentCity.setDoubleTapSafeOnClickListener(v -> autoCompleteTextViewCurrentCity.showDropDown());
    }

    @Override
    public void setOnItemClickListenerToAutoCompleteTextViewCurrentCity(ArrayAdapter<City> cities) {
        this.autoCompleteTextViewCurrentCity.setOnItemClickListener((parent, view, position, id) -> {
            setSelectedCity(cities.getItem(position));
            Utility.hideKeyboard(this, this.autoCompleteTextViewCurrentCity);
            autoCompleteTextViewCurrentCity.setError(null);
        });
    }

    @Override
    public void setVisibillityToTilMobileNumber(int visibillity) {
        tilMobileNumber.setVisibility(visibillity);
    }

    @Override
    public void setVisibillityToTilEmail(int visibillity) {
        this.tilEmail.setVisibility(visibillity);
    }

    @Override
    public void setVisibillityToTilCurrentAddress(int visibillity) {
        this.tilCurrentAddress.setVisibility(visibillity);
    }

    @Override
    public void setVisibillityToAutoCompleteTextViewCurrentCity(int visibillity) {
        this.autoCompleteTextViewCurrentCity.setVisibility(visibillity);
    }

    @Override
    public void setVisibillityToBtContinue(int visibillity) {
        this.btContinue.setVisibility(visibillity);
    }

    @Override
    public void setVisibillityToTlShimmer(int visibillity) {
        this.tlShimmer.setVisibility(visibillity);
    }

    @Override
    public void setTextToEtMobileNumber(String text) {
        this.etMobileNumber.setText(text);
    }

    @Override
    public void setTextToEtEmail(String text) {
        this.etEmail.setText(text);
    }

    @Override
    public void setTextToEtCurrentAddress(String text) {
        this.etCurrtentAddress.setText(text);
    }

    @Override
    public void setCityTextToAutoCompleteTextViewCurrentCity(City city) {
        this.autoCompleteTextViewCurrentCity.setText(city.getCityName());
        this.selectedCity = city;
    }

    @Override
    public void disableCustomInputsOfUserOnAutoCompleteTextCurrentCity(List<City> cities) {
        this.autoCompleteTextViewCurrentCity.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {

                if (cities != null && !cities.isEmpty()) {

                    int cityIndex = cities.indexOf(new City(autoCompleteTextViewCurrentCity.getText().toString()));

                    if (cityIndex != -1) {
                        setSelectedCity(cities.get(cityIndex));
                        return;
                    }
                }

                autoCompleteTextViewCurrentCity.setText(null);
            }
        });
    }

    @Override
    public void setBtContinueEnabled(boolean enabled) {
        this.btContinue.setEnabled(enabled);
    }

    @Override
    public void setDoubleTapOnClickListenerOnBtContinue(User user) {
        this.btContinue.setDoubleTapSafeOnClickListener(view -> {
            autoCompleteTextViewCurrentCity.clearFocus();
            FieldValidator.validate(EditProfileActivity.this, new ValidationListener() {
                @Override
                public void validateSuccess() {
                    validationPassedSuccessfully(!TextUtil.equals(user.getMobileNumber(), getTextFromEtMobileNumber()));
                }

                @Override
                public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
                    Utility.setErrorOnTezViews(validationError.getView(), validationError.getMessage());
                    btContinue.setButtonInactive();
                    filterChain.doFilter();
                }
            });
        });
    }

    @Override
    public String getTextFromEtMobileNumber() {
        return this.etMobileNumber.getValueText();
    }

    @Override
    public String getTextFromEtEmail() {
        String email = this.etEmail.getValueText();
        return TextUtil.isEmpty(email) ? null : email;
    }

    @Override
    public String getTextFromEtCurrentAddress() {
        return this.etCurrtentAddress.getValueText();
    }

    @Override
    public Integer getSelectedCityId() {
        return this.selectedCity.getCityId();
    }

    private void validationPassedSuccessfully(boolean isNumberChanged) {
        setBtContinueEnabled(false);
        showTezLoader();
        this.iEditProfileActivityPresenter.updateUserProfile(isNumberChanged);
    }

    private void createImagePickerUtility(View v) {
        Utility.showChoiceDialog(this, R.array.profile_picture_choice_options, (dialog, which) -> {
            switch (which) {
                case 0:
                    takePictureFromCamera();
                    break;
                case 1:
                    takePictureFromGallery();
                    break;

                default:
                        /*
                          Default case will never be called as we show only two options to user in
                          dialog
                        */
                    break;
            }
        });
    }

    private void takePictureFromCamera() {
        new CameraRequestBuilder()
                .setLensFacing(CameraProperties.LENS_FACING_BACK)
                .setEnableSwitchCamera(true)
                .build()
                .startCamera(this, this::onImageAvailable);
    }

    private void takePictureFromGallery() {
        new ImagePickerRequestBuilder()
                .setTitle(getString(R.string.select_image))
                .setMaxImageCount(1)
                .build()
                .pickImage(this);
    }


    private void onImageAvailable(@NonNull ArrayList<String> capturedFiles) {
        File capturedFile = new File(capturedFiles.get(0));
        Utility.compressFile(capturedFile, 70);
        showTezLoader();
        this.iEditProfileActivityPresenter.setUserPicture(capturedFile);
    }

    @Override
    public void loadImageIntoIvUserImage(File file) {
        if ((Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1 || !isDestroyed())) {
            Glide.with(this)
                    .load(file)
                    .transform(new CenterCrop())
                    .into(ivUserImage);
        }
    }

    @Override
    public void loadMyProfileActivity() {
        MyProfileActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void validateSuccess() {
        this.btContinue.setButtonNormal();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void validatePassed(@NonNull View v) {
        Utility.setErrorOnTezViews(v, null);
    }

    @Override
    public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
        this.btContinue.setButtonInactive();
        filterChain.doFilter();
    }

    @Override
    public void navigateToEditProfileVerificationActivity(@NonNull String mobileNumber,
                                                          final String userAddress,
                                                          final String userEmail,
                                                          final int userId) {
        EditProfileVerificationActivityRouter.createInstance().setDependenciesAndRoute(this,
                mobileNumber, userAddress, userEmail, selectedCity == null ? -1 : selectedCity.getCityId());
    }

    @Override
    protected String getScreenName() {
        return "EditProfileActivity";
    }
}
