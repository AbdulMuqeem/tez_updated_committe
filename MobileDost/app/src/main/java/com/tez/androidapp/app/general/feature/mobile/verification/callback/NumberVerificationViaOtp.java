package com.tez.androidapp.app.general.feature.mobile.verification.callback;

import android.content.Context;
import android.os.CountDownTimer;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.request.MobileVerificationRequest;
import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.request.NumberVerifyGenerateOtpRequest;
import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.request.NumberVerifyOtpRequest;
import com.tez.androidapp.app.general.feature.playback.TezCountDownTimer;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.receivers.SMSReceiver;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by FARHAN DHANANI on 2/27/2019.
 */
public abstract class NumberVerificationViaOtp implements NumberVerifyGenerateOtpCallback {

    private boolean isOtpRecieved = false;
    private boolean countDownFinished = false;

    private final SMSReceiver smsReceiver = new SMSReceiver(otp -> UserCloudDataStore.getInstance().numberVerifyOtp(
            new NumberVerifyOtpRequest(getMobileNumber(), getPrincipalName(), otp),
            new NumberVerifyOtpCallback() {
                @Override
                public synchronized void onNumberVerifyOtpSuccess() {
                    Optional.doWhen(!isOtpRecieved, () -> {
                        isOtpRecieved = true;
                        smsReceiver.unregister(getContext());
                        sendOtpSuccessToServer(getMobileNumber(), getPrincipalName());
                        Utility.resetFailureCounter();
                        onVerifyOtpSuccess();
                    });

                }

                @Override
                public synchronized void onNumberVerifyOtpFailure(int errorCode, String message) {
                    Optional.doWhen(Utility.isUnauthorized(errorCode),
                            () -> UserCloudDataStore.getInstance().numberVerifyOtp(new NumberVerifyOtpRequest(getMobileNumber(), getPrincipalName(),otp), this),
                            () -> Optional.doWhen(errorCode == ResponseStatusCode.SIGNUP_CACHE_TIMEOUT.getCode(),
                                    NumberVerificationViaOtp.this::userSessionTimeOut,
                                    () -> Optional.doWhen(!isOtpRecieved, () -> {
                                        isOtpRecieved = true;
                                        smsReceiver.unregister(getContext());
                                        Optional.doWhen(!countDownFinished,
                                                () -> {
                                                    sendOtpFailureToServer(getMobileNumber(), getPrincipalName());
                                                    countDownTimer.cancel();
                                                    onVerifyOtpFailure();
                                                });
                                    })));
                }
            }));

    private final CountDownTimer countDownTimer = new TezCountDownTimer(Constants.ONE_MIN, Constants.ONE_SECOND) {
        @Override
        public void onTick(long millisUntilFinished) {
            super.onTick(millisUntilFinished);
            countDownTimerTick(millisUntilFinished);
        }

        @Override
        public void onFinish() {
            Optional.doWhen(!isOtpRecieved, () -> {
                countDownFinished = true;
                sendOtpFailureToServer(getMobileNumber(), getPrincipalName());
                smsReceiver.unregister(getContext());
                onCountDownFinish();
            });
        }
    };

    @Override
    public void registerSmsReciever() {
        smsReceiver.register(getContext());
    }

    private void sendOtpSuccessToServer(String mobileNumber, String cnic) {
        MobileVerificationRequest mobileVerificationRequest = new MobileVerificationRequest(cnic, mobileNumber);
        mobileVerificationRequest.setVerificationMethod(Constants.MobileNumberVerificationMethodOtp);
        mobileVerificationRequest.setVerificationTry(MDPreferenceManager.getFlashCallVerificationFailureCount()
                + MDPreferenceManager.getOtpVerificationFailureCount() + 1);
        UserCloudDataStore.getInstance().mobileNumberVerification(mobileVerificationRequest, new MobileVerificationCallback() {

            @Override
            public void onMobileNumberVerificationSuccess() {
                /*
                    Left
                 */
            }

            @Override
            public void onMobileNumberVerificatoinFailure(int errorCode, String message) {
                Optional.doWhen(Utility.isUnauthorized(errorCode),
                        () -> sendOtpFailureToServer(mobileNumber, cnic),
                        () -> Optional.doWhen(errorCode == ResponseStatusCode.SIGNUP_CACHE_TIMEOUT.getCode(),
                                () -> userSessionTimeOut()));
            }
        });
    }

    private static void increaseOtpFailureCountByOne() {
        MDPreferenceManager.setOtpVerificationFailureCount(
                MDPreferenceManager.getOtpVerificationFailureCount() + 1);
    }

    protected void sendOtpFailureToServer(String mobileNumber, String cnic) {
        increaseOtpFailureCountByOne();
        MobileVerificationRequest mobileVerificationRequest = new MobileVerificationRequest(cnic, mobileNumber);
        mobileVerificationRequest.setVerificationMethod(Constants.MobileNumberVerificationMethodOtp);
        mobileVerificationRequest.setStatus(Constants.MobileNumberVerificationStatusFailed);
        UserCloudDataStore.getInstance().mobileNumberVerification(mobileVerificationRequest, new MobileVerificationCallback() {
            @Override
            public void onMobileNumberVerificationSuccess() {
                /*
                    Left
                 */
            }

            @Override
            public void onMobileNumberVerificatoinFailure(int errorCode, String message) {
                Optional.doWhen(Utility.isUnauthorized(errorCode),
                        () -> sendOtpFailureToServer(mobileNumber, cnic),
                        () -> Optional.doWhen(errorCode == ResponseStatusCode.SIGNUP_CACHE_TIMEOUT.getCode(),
                                () -> userSessionTimeOut()));

            }
        });
    }

    protected void handleUnAuthorizeGenerateOtpRequest() {
        this.smsReceiver.unregister(getContext());
        UserCloudDataStore.getInstance()
                .numberVerifyGenerateOtp(new NumberVerifyGenerateOtpRequest(getPrincipalName(),
                        getMobileNumber()), this);
    }

    protected void startCountDownTimer() {
        this.countDownTimer.start();
    }

    @Nullable
    protected abstract String getPrincipalName();

    @NonNull
    protected abstract String getMobileNumber();

    @NonNull
    protected abstract Context getContext();

    protected abstract void onCountDownFinish();

    protected abstract void onVerifyOtpSuccess();

    protected abstract void onVerifyOtpFailure();

    public abstract void userSessionTimeOut();

    public abstract void countDownTimerTick(long time);
}
