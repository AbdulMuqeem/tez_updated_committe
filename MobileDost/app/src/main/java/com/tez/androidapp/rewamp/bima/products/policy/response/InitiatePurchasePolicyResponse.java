package com.tez.androidapp.rewamp.bima.products.policy.response;

import com.tez.androidapp.app.base.response.BaseResponse;

public class InitiatePurchasePolicyResponse extends BaseResponse {

    private  PolicyDetails policyDetails;


    public PolicyDetails getPolicyDetails() {
        return policyDetails;
    }

}
