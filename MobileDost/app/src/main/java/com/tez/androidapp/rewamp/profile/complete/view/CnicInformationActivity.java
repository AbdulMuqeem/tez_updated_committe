package com.tez.androidapp.rewamp.profile.complete.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Answer;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.AnswerSelected;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Option;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.UserProfile;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.GetCnicPictureRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateProfileWithCnicRequest;
import com.tez.androidapp.commons.calendar.TezCalendar;
import com.tez.androidapp.commons.locale.LocaleHelper;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.app.City;
import com.tez.androidapp.commons.models.network.User;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.utils.cnic.detection.model.RetumDataModel;
import com.tez.androidapp.commons.utils.cnic.detection.view.CnicScannerActivity;
import com.tez.androidapp.commons.utils.file.util.FileUtil;
import com.tez.androidapp.commons.validators.annotations.CnicExpiryDateIsValid;
import com.tez.androidapp.commons.validators.annotations.TextInputLayoutNotEmpty;
import com.tez.androidapp.commons.widgets.CnicReviewCardView;
import com.tez.androidapp.commons.widgets.TezAutoCompleteTextView;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezSpinner;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;
import com.tez.androidapp.rewamp.profile.complete.presenter.CnicInformationFragmentPresenter;
import com.tez.androidapp.rewamp.profile.complete.presenter.ICnicInformationFragmentPresenter;
import com.tez.androidapp.rewamp.profile.complete.router.CnicInformationActivityRouter;
import com.tez.androidapp.rewamp.profile.complete.router.PersonalInformationActivityRouter;
import com.tez.androidapp.rewamp.profile.router.CompleteProfileRouter;

import net.tez.camera.cameras.CameraProperties;
import net.tez.camera.request.CameraRequestBuilder;
import net.tez.fragment.util.optional.Optional;
import net.tez.logger.library.utils.TextUtil;
import net.tez.validator.library.annotations.Order;
import com.tez.androidapp.commons.validators.annotations.MaxDateDeviationFromCurrentDate;
import net.tez.validator.library.annotations.text.Regex;
import net.tez.validator.library.core.FieldValidator;
import net.tez.validator.library.core.FilterChain;
import net.tez.validator.library.core.ValidationError;
import net.tez.validator.library.core.ValidationListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class CnicInformationActivity extends BaseActivity
        implements ICnicInformationFragmentView, ValidationListener {

    @TextInputLayoutNotEmpty(value = {1, R.string.string_input_valid_current_name})
    @Order(1)
    @BindView(R.id.tilName)
    private TezTextInputLayout tilName;


    @TextInputLayoutNotEmpty(value = {1, R.string.string_input_valid_current_name})
    @Order(2)
    @BindView(R.id.tilFatherName)
    private TezTextInputLayout tilFatherName;

    @TextInputLayoutNotEmpty(value = {1, R.string.string_input_your_gender})
    @Order(3)
    @BindView(R.id.tilGender)
    private TezTextInputLayout tilGender;

    @TextInputLayoutNotEmpty(value = {1, R.string.string_input_your_marital_status})
    @Order(4)
    @BindView(R.id.tilMaritalStatus)
    private TezTextInputLayout tilMaritalStatus;


    @Regex(regex = Constants.STRING_CNIC_VALIDATOR_REGEX, value = {1, R.string.string_error_drust_cnic_number_enter_karien})
    @Order(5)
    @BindView(R.id.etCnicNumber)
    private TezEditTextView etCnicNumber;


    @BindView(R.id.etDateOfIssue)
    private TezEditTextView etDateOfIssue;

    @BindView(R.id.etDateOfExpiry)
    private TezEditTextView etDateOfExpiry;

    @BindView(R.id.etDOB)
    private TezEditTextView etDOB;

    @TextInputLayoutNotEmpty(value = {1, R.string.string_input_valid_current_address})
    @Order(6)
    @BindView(R.id.tilPlaceOfBirth)
    private TezTextInputLayout tilPlaceOfBirth;


    @BindView(R.id.tilCurrentCity)
    private TezTextInputLayout tilCurrentCity;


    @BindView(R.id.tilCurrentAddress)
    private TezTextInputLayout tilCurrentAddress;

    @TextInputLayoutNotEmpty(value = {1, R.string.string_input_valid_current_name})
    @Order(7)
    @BindView(R.id.tilMothersMaiden)
    private TezTextInputLayout tilMothersMaiden;

    @MaxDateDeviationFromCurrentDate(dateFormat = Constants.UI_DATE_FORMAT,
            dayDeviation = 0,
            monthDeviation = 0,
            yearDeviation = -18,
            value = {2, R.string.string_error_user_must_be_18_years_old})
    @Order(8)
    @BindView(R.id.tilDOB)
    private TezTextInputLayout tilDOB;

    @MaxDateDeviationFromCurrentDate(dateFormat = Constants.UI_DATE_FORMAT,
            dayDeviation = 0,
            monthDeviation = 0,
            yearDeviation = 0,
            value = {2, R.string.string_invalid_cnic_issue_date})
    @Order(9)
    @BindView(R.id.tilDateOfIssue)
    private TezTextInputLayout tilDateOfIssue;

    @CnicExpiryDateIsValid(dateFormat = Constants.UI_DATE_FORMAT, value = {2, R.string.string_invalid_cnic_expiry_date})
    @Order(10)
    @BindView(R.id.tilDateOfExpiry)
    private TezTextInputLayout tilDateOfExpiry;

    @BindView(R.id.etName)
    private TezEditTextView etName;

    @BindView(R.id.etFatherName)
    private TezEditTextView etFatherName;

    @BindView(R.id.tilCnicNumber)
    private TezTextInputLayout tilCnicNumber;

    @BindView(R.id.sGender)
    private TezSpinner sGender;

    @BindView(R.id.sMaritalStatus)
    private TezSpinner sMaritalStatus;

    @BindView(R.id.cvFrontPart)
    private CnicReviewCardView cvFrontPart;

    @BindView(R.id.cvBackPart)
    private CnicReviewCardView cvBackPart;

    @BindView(R.id.cvSelfiePart)
    private CnicReviewCardView cvSelfiePart;

    @BindView(R.id.autoCompleteTextViewPlaceOfBirth)
    private TezAutoCompleteTextView autoCompleteTextViewPlaceOfBirth;

    @BindView(R.id.autoCompleteTextViewCurrentCity)
    private TezAutoCompleteTextView autoCompleteTextViewCurrentCity;

    @BindView(R.id.etCurrentAddress)
    private TezEditTextView etCurrentAddress;

    @BindView(R.id.etMothersMaiden)
    private TezEditTextView etMothersMaiden;

    @BindView(R.id.btContinue)
    private TezButton btContinue;

    @BindView(R.id.detailCard)
    private TezCardView detailCard;

    @BindView(R.id.shimmer)
    private TezLinearLayout shimmer;

    @BindView(R.id.nestedScrollView)
    private NestedScrollView nestedScrollView;

    private CompositeDisposable allDisposables;

    private City selectedPlaceOfBirthCity;
    private City selectedCurrentCity;
    private AnswerSelected selectedMaritalStatus;
    private final ICnicInformationFragmentPresenter iCnicInformationFragmentPresenter;

    public CnicInformationActivity() {
        iCnicInformationFragmentPresenter = new CnicInformationFragmentPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_cnic_information);
        ViewBinder.bind(this);
        initOCRParameters();
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.allDisposables = new CompositeDisposable();
        listenChangesOnViews();
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.allDisposables.dispose();
    }

    @Override
    public void callForCnicImageInCvFrontPart() {
        this.cvFrontPart.setUploadedImage(GetCnicPictureRequest.METHOD_NAME_FRONT_NIC, R.drawable.img_cnic_back_placeholder);
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void setCvFrontPartRejected(boolean isRejected) {
        Optional.doWhen(isRejected, () -> {
                    this.makeCnicImageInCvFrontPartEditable(true);
                    this.cvFrontPart.setRejected(true);
                },
                () -> {
                    this.makeCnicImageInCvFrontPartEditable(false);
                    this.cvFrontPart.setRejected(false);
                });
    }

    @Override
    public void setCvBackPartRejected(boolean isRejected) {
        Optional.doWhen(isRejected, () -> {
                    this.makeCnicImageInCvBackPartEditable(true);
                    this.cvBackPart.setRejected(true);
                },
                () -> {
                    this.makeCnicImageInCvBackPartEditable(false);
                    this.cvBackPart.setRejected(false);
                });
    }

    @Override
    public void setCvSelfiePartRejected(boolean isRejected) {
        Optional.doWhen(isRejected, () -> {
                    this.makeCnicImageInCvSelfiePartEditable(true);
                    this.cvSelfiePart.setRejected(true);
                },
                () -> {
                    this.makeCnicImageInCvSelfiePartEditable(false);
                    this.cvSelfiePart.setRejected(false);
                });
    }

    @Override
    public void populateDropDownForPlaceOfBirth(@NonNull List<City> cities) {
        ArrayAdapter<City> cityArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, cities);
        this.autoCompleteTextViewPlaceOfBirth.setAdapter(cityArrayAdapter);
        initListenersForAutoCompleteTextViewPlaceOfBirth(cities, cityArrayAdapter);
    }

    @Override
    public void populateDropDownForCurrentCity(@NonNull List<City> cities) {
        ArrayAdapter<City> cityArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, cities);
        this.autoCompleteTextViewCurrentCity.setAdapter(cityArrayAdapter);
        initListenersForAutoCompleteTextViewCurrentCity(cities, cityArrayAdapter);
    }

    @Override
    @Nullable
    public String getSelectedLanguage() {
        return LocaleHelper.getSelectedLanguageForBackend(this);
    }

    @Override
    public void routeToPersonalInformationScreen() {
        PersonalInformationActivityRouter.createInstance().setDependenciesAndRoute(this, getKeyRoute());
    }

    private int getKeyRoute(){
        return getIntent().getIntExtra(CompleteProfileRouter.ROUTE_KEY, CompleteProfileRouter.ROUTE_TO_DEFAULT);
    }

    @Override
    public AnswerSelected getSelectedMaritalStatus() {
        return this.selectedMaritalStatus;
    }


    private boolean isUserHasEnteredRequiredFields(User user, UpdateProfileWithCnicRequest updateProfileWithCnicRequest) {
        return (user.getMobileUserRejectedFieldsDto().getFullNameRejected() && !TextUtil.equals(updateProfileWithCnicRequest.getFullName(), user.getFullName()))
                || (user.getMobileUserRejectedFieldsDto().getGenderRejected() && !TextUtil.equals(updateProfileWithCnicRequest.getGender(), user.getGender()))
                || (user.getMobileUserRejectedFieldsDto().getPlaceOfBirthRejected() && !TextUtil.equals(updateProfileWithCnicRequest.getPlaceOfBirthId(), user.getPlaceOfBirthId()))
                || (user.getMobileUserRejectedFieldsDto().getMotherMaidenNameRejected() && !TextUtil.equals(updateProfileWithCnicRequest.getMotherMaidenName(), user.getMotherMaidenName()))
                || (user.getMobileUserRejectedFieldsDto().getDateOfBirthRejected() && !TextUtil.equals(updateProfileWithCnicRequest.getDateOfBirth(), user.getDateOfBirth()))
                || (user.getMobileUserRejectedFieldsDto().getFatherNameRejected() && userHasEnteredFatherName() && !TextUtil.equals(updateProfileWithCnicRequest.getFatherName(), user.getFatherName()))
                || (user.getMobileUserRejectedFieldsDto().getHusbandNameRejected() && userHasEnterdHusbandName() && !TextUtil.equals(updateProfileWithCnicRequest.getHusbandName(), user.getHusbandName()))
                || (user.getMobileUserRejectedFieldsDto().getCnicRejected() && !TextUtil.equals(updateProfileWithCnicRequest.getCnic(), user.getCnic()))
                || (user.getMobileUserRejectedFieldsDto().getCnicDateOfIssueRejected() && !TextUtil.equals(updateProfileWithCnicRequest.getCnicDateOfIssue(), user.getCnicDateOfIssue()))
                || (user.getMobileUserRejectedFieldsDto().getCnicExpiryDateRejected() && !TextUtil.equals(updateProfileWithCnicRequest.getCnicDateOfExpiry(), user.getCnicDateOfExpiry()))
                || (user.getMobileUserRejectedFieldsDto().getMaritalStatusRejected() && !this.iCnicInformationFragmentPresenter.isMaritalStatusEquals());
    }

    private void listenChangesOnViews() {
        this.allDisposables.add(RxTextView.textChanges(this.etDateOfIssue).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));

        this.allDisposables.add(RxTextView.textChanges(this.etDateOfExpiry).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));

        this.allDisposables.add(RxTextView.textChanges(this.etDOB).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));
    }

    @Override
    public void setDoubleTapOnClickListenerToBtContinue(UserProfile userProfile, User user) {
        this.btContinue.setDoubleTapSafeOnClickListener(view -> FieldValidator.validate(this, new ValidationListener() {
            boolean validationsPartiallyPassed = true;

            @Override
            public void validateSuccess() {
                UpdateProfileWithCnicRequest updateProfileWithCnicRequest = getUpdateUserProfileRequest();
                Optional.doWhen(!user.getEditable() || userProfile.getRequestId() == null ||
                                isUserHasEnteredRequiredFields(user, updateProfileWithCnicRequest) ,
                        () -> iCnicInformationFragmentPresenter.updateUserProfile(userProfile, user, updateProfileWithCnicRequest, selectedPlaceOfBirthCity, selectedCurrentCity, selectedMaritalStatus),
                        () -> {
                            showInformativeDialog(R.string.notice, R.string.please_enter_all_fields_correctly);
                            iCnicInformationFragmentPresenter.handleEditableFeilds(user);
                        });

            }

            @Override
            public void validateUnSuccessFull() {
                iCnicInformationFragmentPresenter.updateUserProfile(userProfile, user,
                        getUpdateUserProfileRequest(), validationsPartiallyPassed,
                        selectedPlaceOfBirthCity, selectedCurrentCity, selectedMaritalStatus);
            }

            @Override
            public void validatePassed(@NonNull View v) {
                Utility.setErrorOnTezViews(v, null);
            }

            @Override
            public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
                if (validationError.getView().getId() == etCnicNumber.getId()
                        || validationError.getView().getId() == tilDateOfIssue.getId()
                        || validationError.getView().getId() == tilDateOfExpiry.getId()
                        || validationError.getView().getId() == tilDOB.getId()) {
                    Utility.setErrorOnTezViews(validationError.getView(), validationError.getMessage());
                    validationsPartiallyPassed = false;
                }
                filterChain.doFilter();
            }
        }));
    }

    @Nullable
    private String getTextFromEtName() {
        String value = this.etName.getValueText();
        return TextUtil.isEmpty(value) ? null : value;
    }

    @Nullable
    private String getTextFromSGender() {
        String value = this.sGender.getText().toString();
        return TextUtil.isEmpty(value) ? null : value.charAt(0) + "";
    }

    @Nullable
    private String getTextFromEtFatherName() {
        String value = this.etFatherName.getValueText();
        return TextUtil.isEmpty(value) ? null : value;
    }

    @Nullable
    private String getTextFromEtCnicNumber() {
        String value = this.etCnicNumber.getValueText();
        return TextUtil.isEmpty(value) ? null : value;
    }

    @Nullable
    private String getTextFromCurrentAddress() {
        String value = this.etCurrentAddress.getValueText();
        return TextUtil.isEmpty(value) ? null : value;
    }

    @Nullable
    private String getTextFromEtMothersMaidenName() {
        String value = this.etMothersMaiden.getValueText();
        return TextUtil.isEmpty(value) ? null : value;
    }

    @Nullable
    private Integer getSelectedPlaceOfBirthId() {
        return this.selectedPlaceOfBirthCity == null ? null : this.selectedPlaceOfBirthCity.getCityId();
    }

    @Nullable
    private Integer getSelectedCurrentCityId() {
        return this.selectedCurrentCity == null ? null : this.selectedCurrentCity.getCityId();
    }

    @Nullable
    private String getTextFromEtDateOfIssue() {
        String value = this.etDateOfIssue.getValueText();
        return TextUtil.isEmpty(value) ? null : Utility.getFormattedDate(value,
                Constants.UI_DATE_FORMAT, Constants.BACKEND_DATE_FORMAT);
    }

    @Nullable
    private String getTextFromEtDateOfExpiry() {
        String value = this.etDateOfExpiry.getValueText();
        return TextUtil.isEmpty(value) || isLifeTimeChecked() ? null : Utility.getFormattedDate(value,
                Constants.UI_DATE_FORMAT, Constants.BACKEND_DATE_FORMAT);
    }

    private boolean isLifeTimeChecked() {
        String value = this.etDateOfExpiry.getValueText();
        return !TextUtil.isEmpty(value) && TextUtil.equals(Utility.getStringFromResource(R.string.string_lifetime), value);
    }

    @Nullable
    private String getTextFromEtDob() {
        String value = this.etDOB.getValueText();
        return TextUtil.isEmpty(value) ? null : Utility.getFormattedDate(value,
                Constants.UI_DATE_FORMAT, Constants.BACKEND_DATE_FORMAT);
    }

    private UpdateProfileWithCnicRequest getUpdateUserProfileRequest() {
        UpdateProfileWithCnicRequest updateProfileWithCnicRequest = new UpdateProfileWithCnicRequest();
        updateProfileWithCnicRequest.setFullName(getTextFromEtName());
        updateProfileWithCnicRequest.setGender(getTextFromSGender());
        updateProfileWithCnicRequest.setCnic(getTextFromEtCnicNumber());
        updateProfileWithCnicRequest.setAddress(getTextFromCurrentAddress());
        updateProfileWithCnicRequest.setMotherMaidenName(getTextFromEtMothersMaidenName());
        updateProfileWithCnicRequest.setPlaceOfBirthId(getSelectedPlaceOfBirthId());
        updateProfileWithCnicRequest.setCityId(getSelectedCurrentCityId());
        updateProfileWithCnicRequest.setCnicDateOfIssue(getTextFromEtDateOfIssue());
        updateProfileWithCnicRequest.setCnicDateOfExpiry(getTextFromEtDateOfExpiry());
        updateProfileWithCnicRequest.setDateOfBirth(getTextFromEtDob());
        updateProfileWithCnicRequest.setCnicLifeTimeValid(isLifeTimeChecked());
        Optional.ifPresent(selectedMaritalStatus, status -> {
            List<AnswerSelected> answerSelecteds = new ArrayList<>();
            answerSelecteds.add(status);
            updateProfileWithCnicRequest.setPersonalInformationList(answerSelecteds);
        });
        Optional.doWhen(this.userHasEnteredFatherName(),
                () -> updateProfileWithCnicRequest.setFatherName(getTextFromEtFatherName()));
        Optional.doWhen(userHasEnterdHusbandName(), () -> updateProfileWithCnicRequest.setHusbandName(getTextFromEtFatherName()));
        return updateProfileWithCnicRequest;
    }

    private boolean userHasEnteredFatherName() {
        return TextUtil.equals(getHintToTilFatherName(), Utility.getStringFromResource(R.string.father_name));
    }

    private boolean userHasEnterdHusbandName() {
        return TextUtil.equals(getHintToTilFatherName(), Utility.getStringFromResource(R.string.husband_name));
    }

    @Override
    public void makeCnicImageInCvFrontPartEditable(boolean editable) {
        this.cvFrontPart.setVisibillityForIvEditPicture(editable ? View.VISIBLE : View.GONE);
        Optional.doWhen(editable, () -> this.cvFrontPart.setOnEditPictureClickListener(
                v -> this.iCnicInformationFragmentPresenter.startCamera(Constants.CNIC_REQUEST_CODE_FRONT,
                        CnicScannerActivity.CNIC_FRONT, false)));
    }

    @Override
    public void makeCnicImageInCvBackPartEditable(boolean editable) {
        this.cvBackPart.setVisibillityForIvEditPicture(editable ? View.VISIBLE : View.GONE);
        Optional.doWhen(editable, () -> this.cvBackPart.setOnEditPictureClickListener(
                v -> this.iCnicInformationFragmentPresenter.startCamera(Constants.CNIC_REQUEST_CODE_BACK,
                        CnicScannerActivity.CNIC_BACK, false)));
    }

    @Override
    public void makeCnicImageInCvSelfiePartEditable(boolean editable) {
        this.cvSelfiePart.setVisibillityForIvEditPicture(editable ? View.VISIBLE : View.GONE);
        Optional.doWhen(editable, () -> this.cvSelfiePart.setOnEditPictureClickListener(
                v -> this.iCnicInformationFragmentPresenter.startCamera(Constants.CNIC_REQUEST_CODE_FACE,
                        CnicScannerActivity.FACE, true)));
    }

    @Override
    public void callForCnicImageInCvBackPart() {
        this.cvBackPart.setUploadedImage(GetCnicPictureRequest.METHOD_NAME_BACK_NIC, R.drawable.img_cnic_front_placeholder);
    }

    @Override
    public void callForCnicImageInCvSelfiePart() {
        this.cvSelfiePart.setUploadedImage(GetCnicPictureRequest.METHOD_NAME_SELFIE_NIC, R.drawable.img_selfie_placeholder);
    }

    @Override
    public void setTextToSGender(int position, Option option) {
        List<Answer> maritalOptions = option.getAnswers();
        this.sMaritalStatus.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, maritalOptions));
        this.sGender.setText(getResources().getStringArray(R.array.genders)[position]);
        this.sMaritalStatus.setOnItemClickListener((maritalStatusParent,
                                                    maritalStatusView, maritalStatusIndex,
                                                    maritalStatusId) -> {
            this.selectedMaritalStatus = new AnswerSelected(option.getRefName(), maritalOptions.get(maritalStatusIndex));
            updateHintToTilFatherName(position);
        });
    }

    @Override
    public void setTextToSMaritalStatus(AnswerSelected answerSelected) {
        this.sMaritalStatus.setText(answerSelected.getAnswer());
        this.selectedMaritalStatus = answerSelected;
    }

    @Override
    public void validateSuccess() {
    }

    @Override
    public void validatePassed(@NonNull View v) {
        Utility.setErrorOnTezViews(v, null);
    }

    @Override
    public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
        filterChain.doFilter();
    }

    @Override
    public void setEditableSGender(boolean editable) {
        this.sGender.setEnabled(editable);
    }

    @Override
    public void setsGenderRejected(boolean rejected) {
        setEditableSGender(rejected);
        Optional.doWhen(rejected, () -> this.tilGender.setError(getString(R.string.does_not_match_nadra_records)));
    }

    @Override
    public void setEditableSMaritalStatus(boolean editable) {
        this.sMaritalStatus.setEnabled(editable);
    }

    @Override
    public void setsMaritalStatusRejected(boolean rejected) {
        setEditableSMaritalStatus(rejected);
        Optional.doWhen(rejected, () -> this.tilMaritalStatus.setError(getString(R.string.does_not_match_nadra_records)));
    }

    @Override
    public void setTextToAutoCompleteTextViewPlaceOfBirth(String text, Integer id) {
        Optional.doWhen(id != null && TextUtil.isNotEmpty(text),
                () -> {
                    this.autoCompleteTextViewPlaceOfBirth.setText(text);
                    this.selectedPlaceOfBirthCity = new City(id, text);
                });
    }

    @Override
    public void setEditableAutoCompleteTextViewPlaceOfBirth(boolean editable) {
        this.autoCompleteTextViewPlaceOfBirth.setEnabled(editable);
    }

    @Override
    public void setAutoCompleteTextViewPlaceOfBirthRejected(boolean rejected) {
        this.setEditableAutoCompleteTextViewPlaceOfBirth(rejected);
        Optional.doWhen(rejected, () -> this.tilPlaceOfBirth.setError(getString(R.string.does_not_match_nadra_records)));
    }


    @Override
    public void setTextToAutoCompleteTextViewCurrentCity(City city) {
        this.autoCompleteTextViewCurrentCity.setText(city.getCityName());
        setSelectedCurrentCity(city);
    }

    @Override
    public void setEditableAutoCompleteTextViewCurrentCity(boolean editable) {
        this.autoCompleteTextViewCurrentCity.setEnabled(editable);
    }

    @Override
    public void setTextToEtCurrentAddress(String text) {
        this.etCurrentAddress.setText(text);
    }

    @Override
    public void setEditableEtCurrentAddress(boolean editable) {
        this.etCurrentAddress.setEnabled(editable);
    }

    @Override
    public void setTextToEtMothersMaiden(String text) {
        this.etMothersMaiden.setText(text);
    }

    @Override
    public void setEditableEtMothersMaiden(boolean editable) {
        this.etMothersMaiden.setEnabled(editable);
        Optional.doWhen(editable, ()->
            etMothersMaiden.setOnEditorActionListener((v, actionId, event) -> {
                Optional.doWhen(actionId == EditorInfo.IME_ACTION_DONE, ()->{
                    nestedScrollView.post(() -> nestedScrollView.fullScroll(View.FOCUS_DOWN));
                });
                return false;
            }));
    }

    @Override
    public void setEtMothersMaidenRejected(boolean rejected) {
        setEditableEtMothersMaiden(rejected);
        Optional.doWhen(rejected, () -> this.tilMothersMaiden.setError(getString(R.string.does_not_match_nadra_records)));
    }

    @Override
    public void setTextToEtDOB(String text) {
        Optional.doWhen(this.etDOB.isEnabled(), () -> this.etDOB.setText(Utility.getFormattedDate(text, Constants.BACKEND_DATE_FORMAT, Constants.UI_DATE_FORMAT)));
    }

    @Override
    public void setTextToEtDobFromOcr(String text){
        Optional.doWhen(this.etDOB.isEnabled(), () -> this.etDOB.setText(text));
    }

    @Override
    public void setEditableEtDob(boolean editable) {
        this.etDOB.setEnabled(editable);
    }

    @Override
    public void setEtDobRejected(boolean rejected) {
        setEditableEtDob(rejected);
        Optional.doWhen(rejected, () -> this.tilDOB.setError(getString(R.string.does_not_match_nadra_records)));
    }

    @Override
    public void setTextToEtDateOfIssue(String text) {
        Optional.doWhen(this.etDateOfIssue.isEnabled(), () -> this.etDateOfIssue.setText(Utility.getFormattedDate(text, Constants.BACKEND_DATE_FORMAT, Constants.UI_DATE_FORMAT)));
    }

    @Override
    public void setTextToEtDateOfIssueFromOcr(String text){
        Optional.doWhen(this.etDateOfIssue.isEnabled(), () -> this.etDateOfIssue.setText(text));
    }


    @Override
    public void setEditableToEtDateOfIssue(boolean editable) {
        this.etDateOfIssue.setEnabled(editable);
    }

    @Override
    public void setEtDateOfIssueRejected(boolean rejected) {
        setEditableToEtDateOfIssue(rejected);
        Optional.doWhen(rejected, () -> tilDateOfIssue.setError(getString(R.string.does_not_match_nadra_records)));

    }

    @Override
    public void setTextToEtDateOfExpiry(String text) {
        Optional.doWhen(this.etDateOfExpiry.isEnabled(),
                () -> this.etDateOfExpiry.setText(Utility.getFormattedDate(text, Constants.BACKEND_DATE_FORMAT, Constants.UI_DATE_FORMAT)));
    }

    @Override
    public void setLifeTimeToEtDateOfExpiry() {
        Optional.doWhen(this.etDateOfExpiry.isEnabled(), () -> CnicInformationActivity.this.etDateOfExpiry.setText(CnicInformationActivity.this.getString(R.string.string_lifetime)));
    }

    @Override
    public void setTextToEtDateOfExpiryFromOcr(String text){
        Optional.doWhen(this.etDateOfExpiry.isEnabled(),
                () -> this.etDateOfExpiry.setText(text));
    }

    @Override
    public void setEditableEtDateOfExpiry(boolean editable) {
        this.etDateOfExpiry.setEnabled(editable);
    }

    @Override
    public void setEtDateOfExpiryRejeced(boolean rejeced) {
        setEditableEtDateOfExpiry(rejeced);
        Optional.doWhen(rejeced, () -> this.tilDateOfExpiry.setError(getString(R.string.does_not_match_nadra_records)));
    }

    @Override
    public void setTextToEtName(String text) {
        this.etName.setText(text);
    }

    @Override
    public void setEditableEtName(boolean editable) {
        this.etName.setEnabled(editable);
    }

    @Override
    public void setEtNameRejected(boolean rejected) {
        setEditableEtName(rejected);
        Optional.doWhen(rejected, () -> this.tilName.setError(getString(R.string.does_not_match_nadra_records)));
    }

    @Override
    public void setTextToEtFatherName(String text) {
        this.etFatherName.setText(text);
    }

    @Override
    public void setEditableEtFatherName(boolean editable) {
        this.etFatherName.setEnabled(editable);
    }

    @Override
    public void setEtFatherNameRejected(boolean rejected) {
        setEditableEtFatherName(rejected);
        Optional.doWhen(rejected, () -> this.tilFatherName.setError(getString(R.string.does_not_match_nadra_records)));
    }

    @Override
    public void setTextToEtCnicNumber(String text) {
        Optional.doWhen(this.etCnicNumber.isEnabled(), () -> this.etCnicNumber.setText(text));
    }

    @Override
    public void setEditableCnicNumber(boolean editable) {
        this.etCnicNumber.setEnabled(editable);
    }

    @Override
    public void setEtCnicNumberRejected(boolean rejected) {
        setEditableCnicNumber(rejected);
        Optional.doWhen(rejected, () -> this.tilCnicNumber.setError(getString(R.string.does_not_match_nadra_records)));
    }

    @Override
    public void startRetumModuleScreen(int requestCode, String detectObject) {
        Intent intent = new Intent(this, CnicScannerActivity.class);
        intent.putExtra(CnicScannerActivity.DETECT_OBJECT, detectObject);
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void startBackupCamera(int requestCode, boolean isSelfie) {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.CNIC_REQUEST_CODE, requestCode);
        new CameraRequestBuilder()
                .setCustomLayoutId(isSelfie ? R.layout.selfie_preview_template : R.layout.cnic_scanner_rectangle)
                .setEnableSwitchCamera(false)
                .setLensFacing(isSelfie ? CameraProperties.LENS_FACING_FRONT : CameraProperties.LENS_FACING_BACK)
                .setRootPath(FileUtil.getSignUpRootPath(this))
                .build()
                .startCamera(this, bundle);
    }

    @Override
    public void setBtContinueActive(String cnic, String dob,
                                    String issueDate, String expiryDate) {
        this.btContinue.setButtonNormal();
    }

    @Override
    public void loadImageInUpCVFrontPart(String imagePath) {
        this.cvFrontPart.setUploadedImage(imagePath, R.drawable.img_cnic_front_placeholder);
    }

    @Override
    public void setLoaderInUpCVFrontPartVisible(boolean visibile) {
        this.cvFrontPart.setImageLoaderVisibility(visibile);
    }

    @Override
    public void setLoaderInUpCVSelfiePartVisbile(boolean visibile) {
        this.cvSelfiePart.setImageLoaderVisibility(visibile);
    }

    @Override
    public void setLoaderInUpCVBackPart(boolean visible) {
        this.cvBackPart.setImageLoaderVisibility(visible);
    }

    @Override
    public void loadImageInUpCVSelfiePart(String imagePath) {
        this.cvSelfiePart.setUploadedImage(imagePath, R.drawable.img_selfie_placeholder);
    }

    @Override
    public void loadImageInUpCVBackPart(String imagePath) {
        this.cvBackPart.setUploadedImage(imagePath, R.drawable.img_cnic_back_placeholder);
    }

    @Override
    public void updateClickListeners(String cnic, String dob, String issueDate, String expiryDate) {

    }

    @Override
    public void setVisibillityToShimmer(int visibillity) {
        this.shimmer.setVisibility(visibillity);
    }

    @Override
    public void setVisibillityToDetailCardView(int visibillity) {
        this.detailCard.setVisibility(visibillity);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (isRequestCodeBelongToOCR(requestCode) && resultCode == RESULT_OK && data != null) {
            RetumDataModel retumDataModel = new RetumDataModel();
            retumDataModel.setCnicNumber(data.getStringExtra(CnicScannerActivity.CNIC_NUMBER));
            retumDataModel.setCnicIssueDate(data.getStringExtra(CnicScannerActivity.CNIC_ISSUE_DATE));
            retumDataModel.setCnicExpiryDate(data.getStringExtra(CnicScannerActivity.CNIC_EXPIRY_DATE));
            retumDataModel.setDateOfBirth(data.getStringExtra(CnicScannerActivity.CNIC_DATE_OF_BIRTH));
            retumDataModel.setImageURL(data.getStringExtra(CnicScannerActivity.IMAGE_PATH));
            MDPreferenceManager.setOCRFailedCount(0);
            this.iCnicInformationFragmentPresenter.updateCnicImageCardViews(requestCode,
                    retumDataModel.getImageURL(),
                    true,
                    retumDataModel);
        } else if (isRequestCodeBelongToOCR(requestCode) && resultCode == RESULT_CANCELED && data != null &&
                CnicScannerActivity.CNIC_DETECTION_FAILED
                        .equalsIgnoreCase(data.getStringExtra(CnicScannerActivity.FAILURE_REASON))) {
            MDPreferenceManager.setOCRFailedCount(MDPreferenceManager.getOCRFailedCount() + 1);
            showInformativeMessage(R.string.cnic_fields_not_detected);

        } else if (isRequestCodeBelongToOCR(requestCode) && resultCode == RESULT_CANCELED && data != null &&
                CnicScannerActivity.FACE_DETECTION_FAIILED
                        .equalsIgnoreCase(data.getStringExtra(CnicScannerActivity.FAILURE_REASON))) {
            MDPreferenceManager.setOCRFailedCount(MDPreferenceManager.getOCRFailedCount() + 1);
            showInformativeMessage(R.string.cnic_fields_not_detected);
        } else if (requestCode == CameraRequestBuilder.REQUEST_CODE_CAMERA
                && resultCode == RESULT_OK
                && data != null) {
            Optional.ifPresent(data.getStringArrayListExtra(CameraRequestBuilder.RESULT_IMAGE_FILES_ARRAY_LIST), paths -> {
                Optional.ifPresent(data.getBundleExtra(CameraRequestBuilder.PARAMETERS_BUNDLE), bundle -> {
                    this.iCnicInformationFragmentPresenter.updateCnicImageCardViews(
                            bundle.getInt(Constants.CNIC_REQUEST_CODE, -1),
                            paths.get(0),
                            false, null);
                });
            });
        }
    }

    @Override
    public void onClickListenerForSGender(List<Option> maritalStatus) {
        this.sGender.setOnItemClickListener((parent, view, position, id) -> {
            Option option = maritalStatus.get(position);
            List<Answer> maritalOptions = option.getAnswers();
            this.sMaritalStatus.setAdapter(new ArrayAdapter<>(this,
                    android.R.layout.simple_spinner_dropdown_item, maritalOptions));
            updateHintToTilFatherName(position);
            this.sMaritalStatus.setText("");
            this.sMaritalStatus.setOnItemClickListener((maritalStatusParent,
                                                        maritalStatusView, maritalStatusIndex,
                                                        maritalStatusId) -> {
                selectedMaritalStatus = new AnswerSelected(option.getRefName(), maritalOptions.get(maritalStatusIndex));
                updateHintToTilFatherName(position);
            });
        });
    }

    @Override
    public void updateHintToTilFatherName(int position) {
        Optional.ifPresent(selectedMaritalStatus, status -> {
            Optional.doWhen(position == 1
                            && !TextUtil.equals(status.getAnswerReference(), "divorced")
                            && !TextUtil.equals(status.getAnswerReference(), "divorced_and_children")
                            && !TextUtil.equals(status.getAnswerReference(), "single"),
                    () -> this.setHintToTilFatherName(Utility.getStringFromResource(R.string.husband_name)),
                    () -> this.setHintToTilFatherName(Utility.getStringFromResource(R.string.father_name)));
        });

        this.etFatherName.clearText();
    }

    @Override
    public void setHintToTilFatherName(String hint) {
        this.tilFatherName.setHint(hint);
    }

    public String getHintToTilFatherName() {
        return this.tilFatherName.getHint() == null ? null : this.tilFatherName.getHint().toString();
    }

    private void initListenersForAutoCompleteTextViewPlaceOfBirth(List<City> cities, ArrayAdapter<City> adapter) {
        Optional.doWhen(autoCompleteTextViewPlaceOfBirth.isEnabled(), () -> {
            this.setOnItemClickListenerToAutoCompleteTextViewPlaceOfBirth(adapter);
            this.setDoubleTapSafeOnClickListenerToAutoCompleteTextViewPlaceOfBirth();
            this.disableCustomInputsOfUserOnAutoCompleteTextViewPlaceOfBirth(cities);
        });
    }

    private void initListenersForAutoCompleteTextViewCurrentCity(List<City> cities, ArrayAdapter<City> adapter) {
        Optional.doWhen(autoCompleteTextViewCurrentCity.isEnabled(), () -> {
            this.setOnItemClickListenerToAutoCompleteTextViewCurrentCity(adapter);
            this.setDoubleTapSafeOnClickListenerToAutoCompleteTextViewCurrentCity();
            this.disableCustomInputsOfUserOnAutoCompleteTextViewCurrentCity(cities);
        });
    }

    private void initViews() {
        this.sGender.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.genders)));
        this.sMaritalStatus.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, new ArrayList<Answer>()));
        this.etDOB.setOnClickListener(v -> TezCalendar.showCalendarForDob(this, etDOB));
        this.etDateOfIssue.setOnClickListener(v -> TezCalendar.showCalendarForIssueDate(this, etDateOfIssue));
        this.etDateOfExpiry.setOnClickListener(v -> TezCalendar.showCalendarForExpiryDate(this, etDateOfExpiry));
        this.setVisibillityToShimmer(View.VISIBLE);
        this.setVisibillityToDetailCardView(View.GONE);
        this.iCnicInformationFragmentPresenter.getProfile();
    }

    private void initOCRParameters() {
        String cnic = getCnicFromIntent();
        String issueDate = getIssueDateFromIntent();
        String expiryDate = getExpiryDateFromIntent();
        String dob = getDOBFromIntent();
        this.iCnicInformationFragmentPresenter.setUserCnic(cnic);
        this.iCnicInformationFragmentPresenter.setUserCNICDateOfBirth(dob);
        this.iCnicInformationFragmentPresenter.setUserCNICDateOfIssue(issueDate);
        this.iCnicInformationFragmentPresenter.setUserCNICDateOfExpiry(expiryDate);
        this.iCnicInformationFragmentPresenter.setOcrHasBeenRun(
                !(cnic == null && dob == null && issueDate == null && expiryDate == null));
    }

    private void setOnItemClickListenerToAutoCompleteTextViewPlaceOfBirth(ArrayAdapter<City> adapter) {
        this.autoCompleteTextViewPlaceOfBirth.setOnItemClickListener((parent, view, position, id) -> {
            setSelectedPlaceOfBirthCity(adapter.getItem(position));
            Utility.hideKeyboard(this, autoCompleteTextViewPlaceOfBirth);
            autoCompleteTextViewPlaceOfBirth.setError(null);
        });
    }

    private void setOnItemClickListenerToAutoCompleteTextViewCurrentCity(ArrayAdapter<City> adapter) {
        this.autoCompleteTextViewCurrentCity.setOnItemClickListener((parent, view, position, id) -> {
            setSelectedCurrentCity(adapter.getItem(position));
            Utility.hideKeyboard(this, autoCompleteTextViewCurrentCity);
            autoCompleteTextViewCurrentCity.setError(null);
        });
    }

    private void setSelectedPlaceOfBirthCity(City selectedCity) {
        this.selectedPlaceOfBirthCity = selectedCity;
    }

    private void setSelectedCurrentCity(City selectedCity) {
        this.selectedCurrentCity = selectedCity;
    }

    private void setDoubleTapSafeOnClickListenerToAutoCompleteTextViewPlaceOfBirth() {
        this.autoCompleteTextViewPlaceOfBirth.setDoubleTapSafeOnClickListener(v -> autoCompleteTextViewPlaceOfBirth.showDropDown());
    }

    private void setDoubleTapSafeOnClickListenerToAutoCompleteTextViewCurrentCity() {
        this.autoCompleteTextViewCurrentCity.setDoubleTapSafeOnClickListener(v -> autoCompleteTextViewCurrentCity.showDropDown());
    }


    private void disableCustomInputsOfUserOnAutoCompleteTextViewPlaceOfBirth(List<City> cities) {
        this.autoCompleteTextViewPlaceOfBirth.setOnFocusChangeListener((v, hasFocus) ->
                Optional.doWhen(!hasFocus,
                        () -> Optional.doWhen(cities.size() == 0
                                        || !cities.contains(new City(autoCompleteTextViewPlaceOfBirth.getText().toString())),
                                () -> autoCompleteTextViewPlaceOfBirth.setText(null))));
    }

    private void disableCustomInputsOfUserOnAutoCompleteTextViewCurrentCity(List<City> cities) {
        this.autoCompleteTextViewCurrentCity.setOnFocusChangeListener((v, hasFocus) ->
                Optional.doWhen(!hasFocus,
                        () -> Optional.doWhen(cities.size() == 0
                                        || !cities.contains(new City(autoCompleteTextViewCurrentCity.getText().toString())),
                                () -> autoCompleteTextViewCurrentCity.setText(null))));
    }

    private String getCnicFromIntent() {
        return getIntent().getStringExtra(CnicInformationActivityRouter.OCR_CNIC);
    }

    private String getDOBFromIntent() {
        return getIntent().getStringExtra(CnicInformationActivityRouter.OCR_DOB);
    }

    private String getExpiryDateFromIntent() {
        return getIntent().getStringExtra(CnicInformationActivityRouter.OCR_EXPIRY_DATE);
    }

    private String getIssueDateFromIntent() {
        return getIntent().getStringExtra(CnicInformationActivityRouter.OCR_ISSUE_DATE);
    }

    @Override
    protected String getScreenName() {
        return "CnicInformationActivity";
    }
}
