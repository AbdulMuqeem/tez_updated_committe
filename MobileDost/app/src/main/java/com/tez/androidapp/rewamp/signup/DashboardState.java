package com.tez.androidapp.rewamp.signup;

/**
 * Created by VINOD KUMAR on 6/12/2019.
 */
public class DashboardState {

    private String status;

    private State state;

    private int textWidth;


    public DashboardState(String status, State state) {
        this.status = status;
        this.state = state;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getTextWidth() {
        return textWidth;
    }

    public void setTextWidth(int textWidth) {
        this.textWidth = textWidth;
    }

    public enum State {

        LOCKED, UNLOCKED, PASSED, REJECTED
    }
}
