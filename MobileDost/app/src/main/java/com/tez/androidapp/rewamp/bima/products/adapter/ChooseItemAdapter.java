package com.tez.androidapp.rewamp.bima.products.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.bima.products.model.Item;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public class ChooseItemAdapter extends GenericRecyclerViewAdapter<Item, ChooseItemAdapter.ChooseItemListener, ChooseItemAdapter.CallNumberViewHolder> {

    public ChooseItemAdapter(@NonNull List<Item> items, @Nullable ChooseItemListener listener) {
        super(items, listener);
    }

    @NonNull
    @Override
    public CallNumberViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.list_item_choose_item, parent);
        return new CallNumberViewHolder(view);
    }

    public interface ChooseItemListener extends BaseRecyclerViewListener {

        void onClickNumber(@NonNull Item number);
    }

    static class CallNumberViewHolder extends BaseViewHolder<Item, ChooseItemListener> {

        @BindView(R.id.tvNumber)
        private TezTextView tvNumber;

        private CallNumberViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(Item item, @Nullable ChooseItemListener listener) {
            super.onBind(item, listener);
            tvNumber.setText(item.getText());
            tvNumber.changeDrawable(tvNumber, item.getIcon(), 0, 0, 0);
            if (listener != null)
                this.setItemViewOnClickListener(v -> listener.onClickNumber(item));
        }
    }
}
