package com.tez.androidapp.rewamp.bima.claim.health.view

import androidx.navigation.NavDirections
import com.tez.androidapp.R
import com.tez.androidapp.commons.widgets.StepperView
import com.tez.androidapp.rewamp.bima.claim.common.base.BaseClaimLodgeActivity
import com.tez.androidapp.rewamp.bima.claim.common.document.view.EvidenceFragmentDirections

class HospitalCoverageLodgeClaimActivity : BaseClaimLodgeActivity() {

    override fun initNavDirections(): List<NavDirections> = listOfNotNull(
            AmountFragmentDirections.actionAmountFragmentToReasonFragment(2),
            ReasonFragmentDirections.actionReasonFragmentToDetailsFragment(3,
                    dependencies.activationDate,
                    dependencies.expiryDate),
            DetailsFragmentDirections.actionDetailsFragmentToEvidenceFragment(4),
            EvidenceFragmentDirections.actionEvidenceFragmentToSummaryFragment(5, dependencies.policyId)
    )

    override fun initStepperList(): List<StepperView.Step> = listOfNotNull(
            StepperView.Step(1, R.string.amount, R.drawable.ic_amount_active),
            StepperView.Step(2, R.string.reason, R.drawable.ic_reason_active),
            StepperView.Step(3, R.string.details, R.drawable.ic_information_active),
            StepperView.Step(4, R.string.evidence, R.drawable.ic_evidence_active),
            StepperView.Step(5, R.string.summary, R.drawable.ic_summary_active)
    )

    override fun getScreenName(): String = "HospitalCoverageLodgeClaimActivity"
}