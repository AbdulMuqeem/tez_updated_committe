package com.tez.androidapp.rewamp.profile.complete.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.profile.complete.view.PersonalInformationActivity;
import com.tez.androidapp.rewamp.profile.router.CompleteProfileRouter;

public class PersonalInformationActivityRouter extends CompleteProfileRouter {

    public static PersonalInformationActivityRouter createInstance() {
        return new PersonalInformationActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, int routeTo) {
        Intent intent = createIntent(from);
        addAfterCompletionRoute(intent, routeTo);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, PersonalInformationActivity.class);
    }
}
