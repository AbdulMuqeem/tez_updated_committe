package com.tez.androidapp.commons.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import androidx.annotation.NonNull;

/**
 * Created by VINOD KUMAR on 1/18/2019.
 */
public class LocationBroadCastReceiver extends BroadcastReceiver {

    @NonNull
    private LocationEnabledCallback locationEnabledCallback;

    public LocationBroadCastReceiver(@NonNull LocationEnabledCallback callback) {
        locationEnabledCallback = callback;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (intent.getAction() != null
                && intent.getAction().matches(LocationManager.PROVIDERS_CHANGED_ACTION)
                && locationManager != null
                && (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
                || locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))) {
            locationEnabledCallback.onUserEnabledLocation();
        }
    }

    public void register(@NonNull Context context) {
        context.registerReceiver(this, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
    }

    public void unregister(@NonNull Context context) {
        context.unregisterReceiver(this);
    }

    public interface LocationEnabledCallback {

        void onUserEnabledLocation();
    }
}
