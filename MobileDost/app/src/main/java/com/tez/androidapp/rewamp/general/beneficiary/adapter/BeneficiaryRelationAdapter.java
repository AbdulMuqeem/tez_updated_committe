package com.tez.androidapp.rewamp.general.beneficiary.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.vertical.bima.shared.models.BimaRelation;
import net.tez.fragment.util.base.IBaseWidget;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public class BeneficiaryRelationAdapter extends GenericRecyclerViewAdapter<BimaRelation,
        BeneficiaryRelationAdapter.BeneficiaryRelationListener,
        BeneficiaryRelationAdapter.BeneficiaryRelationViewHolder> {

    private final int selectedBeneficiaryRelationshipId;

    public BeneficiaryRelationAdapter(@NonNull List<BimaRelation> items,
                                      int selectedBeneficiaryRelationshipId,
                                      @Nullable BeneficiaryRelationListener listener) {
        super(items, listener);
        this.selectedBeneficiaryRelationshipId = selectedBeneficiaryRelationshipId;
    }

    @NonNull
    @Override
    public BeneficiaryRelationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.grid_item_beneficiary_relation, parent);
        return new BeneficiaryRelationViewHolder(view);
    }

    public interface BeneficiaryRelationListener extends BaseRecyclerViewListener {

        void onClickBeneficiaryRelation(@NonNull BimaRelation bimaRelation);
    }

    class BeneficiaryRelationViewHolder extends BaseViewHolder<BimaRelation, BeneficiaryRelationListener> {

        @BindView(R.id.ivIcon)
        private TezImageView ivIcon;

        @BindView(R.id.tvRelation)
        private TezTextView tvRelation;

        @BindView(R.id.cvContent)
        private TezCardView cvContent;

        @BindView(R.id.ivSelectedRelation)
        private TezImageView ivSelectedRelation;

        private BeneficiaryRelationViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(BimaRelation item, @Nullable BeneficiaryRelationListener listener) {
            super.onBind(item, listener);
            ivIcon.setImageResource(item.getRelationShipIcon());
            tvRelation.setText(item.getRelationshipNameId());
            ivSelectedRelation.setVisibility(selectedBeneficiaryRelationshipId == item.getRelationShipId() && item.getRelationShipId() != -1 ? View.VISIBLE : View.GONE);
            this.setItemViewOnClickListener(view -> {
                if (listener != null && item.getRelationShipId() != -1)
                    listener.onClickBeneficiaryRelation(item);
            });
            cvContent.attachClickEffect(item.getRelationShipId() != -1 ? IBaseWidget.SCALE_EFFECT : IBaseWidget.NO_EFFECT, cvContent);
        }
    }
}
