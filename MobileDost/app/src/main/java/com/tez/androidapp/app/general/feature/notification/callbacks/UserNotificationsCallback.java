package com.tez.androidapp.app.general.feature.notification.callbacks;

import com.tez.androidapp.app.general.feature.notification.models.netwrok.Notification;

import java.util.List;

/**
 * Created  on 8/22/2017.
 */

public interface UserNotificationsCallback {

    void onUserNotificationsSuccess(int recordCount, List<Notification> notificationList);

    void onUserNotificationsError(int errorCode, String message);
}
