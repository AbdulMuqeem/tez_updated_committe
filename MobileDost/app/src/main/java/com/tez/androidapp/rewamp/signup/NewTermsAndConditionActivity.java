package com.tez.androidapp.rewamp.signup;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.models.network.dto.request.UserLoginRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezCheckBox;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.general.termsandcond.NavigationTermsAndConditionsActivityRouter;
import com.tez.androidapp.rewamp.signup.presenter.INewTermsAndConditionActivityPresenter;
import com.tez.androidapp.rewamp.signup.presenter.NewTermsAndConditionActivityPresenter;
import com.tez.androidapp.rewamp.signup.router.NewTermsAndConditionActivityRouter;
import com.tez.androidapp.rewamp.signup.router.SignupLoginActivityRouter;
import com.tez.androidapp.rewamp.signup.view.INewTermsAndConditionActivityView;

import net.tez.fragment.util.listener.DoubleTapSafeClickableSpan;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.fragment.util.optional.Optional;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;
import java.util.List;

public class NewTermsAndConditionActivity extends BaseActivity implements
        CompoundButton.OnCheckedChangeListener,
        INewTermsAndConditionActivityView {

    @BindView(R.id.recyclerView)
    private RecyclerView recyclerViewTermsAndCondition;

    @BindView(R.id.tcCheckbox)
    private TezCheckBox tezCheckBox;

    @BindView(R.id.btMainButton)
    private TezButton btMainButton;

    private final INewTermsAndConditionActivityPresenter iNewTermsAndConditionActivityPresenter;

    public NewTermsAndConditionActivity() {
        this.iNewTermsAndConditionActivityPresenter = new NewTermsAndConditionActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_condition);
        ViewBinder.bind(this);
        init();
    }

    private void init() {
        TermsAndConditionAdapter termsAndCondtionsAdapter = new TermsAndConditionAdapter(this.getTermsAndCondition());
        this.recyclerViewTermsAndCondition.setLayoutManager(new LinearLayoutManager(this));
        this.recyclerViewTermsAndCondition.setAdapter(termsAndCondtionsAdapter);
        this.btMainButton.setDoubleTapSafeOnClickListener(null);
        this.tezCheckBox.setOnCheckedChangeListener(this);
        this.tezCheckBox.setText(getSpannableStringOnTermsAndConditions());
        this.tezCheckBox.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void onClickBtMainButton() {
        showTezLoader();
        this.iNewTermsAndConditionActivityPresenter.callSetPin(getPin());
    }

    private String getPin() {
        return getIntent().getStringExtra(NewTermsAndConditionActivityRouter.USER_PIN);
    }

    private void setOnClickListnerOnBtMainButton(DoubleTapSafeOnClickListener onClickListener) {
        this.btMainButton.setDoubleTapSafeOnClickListener(onClickListener);
    }

    @NonNull
    private List<TermsAndCondition> getTermsAndCondition() {
        return new ArrayList<TermsAndCondition>() {
            {
                add(new TermsAndCondition(getString(R.string.tc_tez_1)));
                add(new TermsAndCondition(getString(R.string.tc_tez_2)));
                add(new TermsAndCondition(getString(R.string.tc_tez_3)));
            }
        };
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Optional.doWhen(isChecked, () -> {
                    this.btMainButton.setButtonNormal();
                    setOnClickListnerOnBtMainButton(view -> onClickBtMainButton());
                },
                () -> {
                    this.btMainButton.setButtonInactive();
                    setOnClickListnerOnBtMainButton(null);
                });
    }

    @Override
    public void navigateToDashBoardActivity() {
        DashboardActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void startLogin() {
        SignupLoginActivityRouter.createInstance().setDependenciesAndRoute(this, getMobileNumber());
        finish();
    }


    @Override
    public UserLoginRequest getLoginrequest(String pin) {
        return Utility.createLoginRequest(this, getMobileNumber(), pin);
    }

    private String getMobileNumber() {
        return getIntent().getStringExtra(NewTermsAndConditionActivityRouter.MOBILE_NUMBER);
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    private SpannableString getSpannableStringOnTermsAndConditions() {
        String string = getString(R.string.i_accept_these_terms_and_conditions);
        String foregroundString = getString(R.string.tc);
        SpannableString spannableString = new SpannableString(string);
        int startIndex = string.indexOf(foregroundString);
        ClickableSpan foregroundColorSpan =  new DoubleTapSafeClickableSpan(){

            @Override
            public void doubleTapSafeSpanOnClick(@NonNull View widget) {
                NavigationTermsAndConditionsActivityRouter.createInstance()
                        .setDependenciesAndRoute(NewTermsAndConditionActivity.this);
            }
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        spannableString.setSpan(foregroundColorSpan, startIndex, startIndex+foregroundString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), startIndex, startIndex+foregroundString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.
                setSpan(new ForegroundColorSpan(Utility.getColorFromResource(R.color.textViewTitleColorBlue)),
                        startIndex,
                        startIndex + foregroundString.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    @Override
    protected String getScreenName() {
        return "NewTermsAndConditionActivity";
    }
}
