package com.tez.androidapp.rewamp.general.changepin;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by VINOD KUMAR on 8/26/2019.
 */
public class ChangePinActivityPresenter implements IChangePinActivityPresenter, IChangePinActivityInteractorOutput {

    private final IChangePinActivityView iChangePinActivityView;
    private final IChangePinActivityInteractor iChangePinActivityInteractor;

    public ChangePinActivityPresenter(IChangePinActivityView iChangePinActivityView) {
        this.iChangePinActivityView = iChangePinActivityView;
        iChangePinActivityInteractor = new ChangePinActivityInteractor(this);
    }

    @Override
    public void changePin(String newPin, String oldPin) {
        iChangePinActivityView.setBtChangePinEnabled(false);
        iChangePinActivityView.showTezLoader();
        iChangePinActivityInteractor.changePin(newPin, oldPin);
    }

    @Override
    public void onChangePinSuccess(BaseResponse baseResponse) {
        iChangePinActivityView.setTezLoaderToBeDissmissedOnTransition();
        iChangePinActivityView.onPinChangedSuccessfully();
    }

    @Override
    public void onChangePinFailure(int errCode, String message) {
        iChangePinActivityView.clearPin();
        iChangePinActivityView.setBtChangePinEnabled(true);
        iChangePinActivityView.dismissTezLoader();
        iChangePinActivityView.showError(errCode);
    }
}
