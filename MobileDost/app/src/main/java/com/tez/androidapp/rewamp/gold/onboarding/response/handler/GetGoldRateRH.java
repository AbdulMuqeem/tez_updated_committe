package com.tez.androidapp.rewamp.gold.onboarding.response.handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.gold.onboarding.callback.GetGoldRateCallback;
import com.tez.androidapp.rewamp.gold.onboarding.response.GetGoldRateResponse;

public class GetGoldRateRH extends BaseRH<GetGoldRateResponse> {

    private final GetGoldRateCallback callback;

    public GetGoldRateRH(@NonNull BaseCloudDataStore baseCloudDataStore,
                         @NonNull GetGoldRateCallback callback) {
        super(baseCloudDataStore);
        this.callback = callback;
    }

    @Override
    protected void onSuccess(Result<GetGoldRateResponse> value) {
        GetGoldRateResponse response = value.response().body();
        if (response != null) {
            if (isErrorFree(response))
                callback.onGetGoldRateSuccess(response);
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());
        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        callback.onGetGoldRateFailure(errorCode, message);
    }
}
