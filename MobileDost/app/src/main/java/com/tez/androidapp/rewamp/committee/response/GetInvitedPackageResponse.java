package com.tez.androidapp.rewamp.committee.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.tez.androidapp.app.base.response.BaseResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class GetInvitedPackageResponse extends BaseResponse implements Parcelable {

    public List<CommitteeList> committeeList = new ArrayList<>();

    protected GetInvitedPackageResponse(Parcel in) {
        in.readList(committeeList, getClass().getClassLoader());
    }

    public static final Creator<GetInvitedPackageResponse> CREATOR = new Creator<GetInvitedPackageResponse>() {
        @Override
        public GetInvitedPackageResponse createFromParcel(Parcel in) {
            return new GetInvitedPackageResponse(in);
        }

        @Override
        public GetInvitedPackageResponse[] newArray(int size) {
            return new GetInvitedPackageResponse[size];
        }
    };

    public List<CommitteeList> getCommitteeList() {
        return committeeList;
    }

    public void setCommitteeList(List<CommitteeList> committeeList) {
        this.committeeList = committeeList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(committeeList);
    }

    public static class CommitteeList implements Parcelable {
        public int id;
        public String name;
        public int tenor;
        public int frequency;
        public int amount;
        public String startDate;
        public String dueDate;
        public String receivingDate;
        public int installment;
        public int userId;
        public int inviteId;
        public int memberSize;
        public List<Invite> invites = new ArrayList<>();
        public List<Member> members = new ArrayList<>();

        protected CommitteeList(Parcel in) {
            id = in.readInt();
            name = in.readString();
            tenor = in.readInt();
            frequency = in.readInt();
            amount = in.readInt();
            startDate = in.readString();
            dueDate = in.readString();
            receivingDate = in.readString();
            installment = in.readInt();
            userId = in.readInt();
            inviteId = in.readInt();
            memberSize = in.readInt();
            in.readList(invites, getClass().getClassLoader());
            in.readList(members, getClass().getClassLoader());
        }

        public static final Creator<CommitteeList> CREATOR = new Creator<CommitteeList>() {
            @Override
            public CommitteeList createFromParcel(Parcel in) {
                return new CommitteeList(in);
            }

            @Override
            public CommitteeList[] newArray(int size) {
                return new CommitteeList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(id);
            parcel.writeString(name);
            parcel.writeInt(tenor);
            parcel.writeInt(frequency);
            parcel.writeInt(amount);
            parcel.writeString(startDate);
            parcel.writeString(dueDate);
            parcel.writeString(receivingDate);
            parcel.writeInt(installment);
            parcel.writeInt(userId);
            parcel.writeInt(inviteId);
            parcel.writeInt(memberSize);
            parcel.writeList(invites);
            parcel.writeList(members);
        }


        public static class Invite implements Parcelable {
            public String email;
            public String mobilNumber;
            public String status;
            public int inviteId;

            protected Invite(Parcel in) {
                email = in.readString();
                mobilNumber = in.readString();
                status = in.readString();
                inviteId = in.readInt();
            }

            public static final Creator<Invite> CREATOR = new Creator<Invite>() {
                @Override
                public Invite createFromParcel(Parcel in) {
                    return new Invite(in);
                }

                @Override
                public Invite[] newArray(int size) {
                    return new Invite[size];
                }
            };

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getMobilNumber() {
                return mobilNumber;
            }

            public void setMobilNumber(String mobilNumber) {
                this.mobilNumber = mobilNumber;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public int getInviteId() {
                return inviteId;
            }

            public void setInviteId(int inviteId) {
                this.inviteId = inviteId;
            }

            @Override
            public int describeContents() {
                return 0;
            }


            @Override
            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeString(email);
                parcel.writeString(mobilNumber);
                parcel.writeString(status);
                parcel.writeInt(inviteId);
            }
        }

        public static class Member implements Parcelable {
            public int userId;
            public String name;
            public String mobilNumber;
            public String joiningDate;
            public int round;
            public String profilePicture;
            public String email;
            public String dueDate;


            protected Member(Parcel in) {
                userId = in.readInt();
                name = in.readString();
                mobilNumber = in.readString();
                joiningDate = in.readString();
                round = in.readInt();
                profilePicture = in.readString();
                email = in.readString();
                dueDate = in.readString();
            }

            public static final Creator<Member> CREATOR = new Creator<Member>() {
                @Override
                public Member createFromParcel(Parcel in) {
                    return new Member(in);
                }

                @Override
                public Member[] newArray(int size) {
                    return new Member[size];
                }
            };

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getMobilNumber() {
                return mobilNumber;
            }

            public void setMobilNumber(String mobilNumber) {
                this.mobilNumber = mobilNumber;
            }

            public String getJoiningDate() {
                return joiningDate;
            }

            public void setJoiningDate(String joiningDate) {
                this.joiningDate = joiningDate;
            }

            public int getRound() {
                return round;
            }

            public void setRound(int round) {
                this.round = round;
            }

            public String getProfilePicture() {
                return profilePicture;
            }

            public void setProfilePicture(String profilePicture) {
                this.profilePicture = profilePicture;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getDueDate() {
                return dueDate;
            }

            public void setDueDate(String dueDate) {
                this.dueDate = dueDate;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(userId);
                parcel.writeString(name);
                parcel.writeString(mobilNumber);
                parcel.writeString(joiningDate);
                parcel.writeInt(round);
                parcel.writeString(profilePicture);
                parcel.writeString(email);
                parcel.writeString(dueDate);
            }
        }


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getTenor() {
            return tenor;
        }

        public void setTenor(int tenor) {
            this.tenor = tenor;
        }

        public int getFrequency() {
            return frequency;
        }

        public void setFrequency(int frequency) {
            this.frequency = frequency;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getDueDate() {
            return dueDate;
        }

        public void setDueDate(String dueDate) {
            this.dueDate = dueDate;
        }

        public String getReceivingDate() {
            return receivingDate;
        }

        public void setReceivingDate(String receivingDate) {
            this.receivingDate = receivingDate;
        }

        public int getInstallment() {
            return installment;
        }

        public void setInstallment(int installment) {
            this.installment = installment;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public List<Invite> getInvites() {
            return invites;
        }

        public void setInvites(List<Invite> invites) {
            this.invites = invites;
        }

        public List<Member> getMembers() {
            return members;
        }

        public void setMembers(List<Member> members) {
            this.members = members;
        }

        public int getInviteId() {
            return inviteId;
        }

        public void setInviteId(int inviteId) {
            this.inviteId = inviteId;
        }

        public int getMemberSize() {
            return memberSize;
        }

        public void setMemberSize(int memberSize) {
            this.memberSize = memberSize;
        }

    }


}



