package com.tez.androidapp.rewamp.general.beneficiary.listener;

import com.tez.androidapp.rewamp.general.beneficiary.response.BeneficiaryAllocationResponse;

public interface PutBeneficiaryAllocationListener {

    void onBeneficiaryAllocationSuccess(BeneficiaryAllocationResponse response);

    void onBeneficiaryAllocationFailure(int errorCode, String message);
}
