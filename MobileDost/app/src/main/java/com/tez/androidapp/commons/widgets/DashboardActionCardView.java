package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.dashboard.ActionTypes;
import com.tez.androidapp.rewamp.dashboard.entity.Action;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by VINOD KUMAR on 6/11/2019.
 */
public class DashboardActionCardView extends TezCardView {

    @Nullable
    private InfoCard infoCard;

    @Nullable
    private LimitCard limitCard;

    private ActionStatusListener actionStatusListener;


    public DashboardActionCardView(@NonNull Context context) {
        super(context);
    }

    public DashboardActionCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public DashboardActionCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    public InfoCard getInfoCard() {
        if (infoCard == null)
            setCardType(Type.INFO);
        return infoCard;
    }


    public LimitCard getLimitCard() {
        if (limitCard == null)
            setCardType(Type.LIMIT);
        return limitCard;
    }

    public void setActionStatusListener(ActionStatusListener actionStatusListener) {
        this.actionStatusListener = actionStatusListener;
    }

    public void setCardType(@NonNull Type type) {


        switch (type) {

            case INFO:
                if (infoCard == null) {
                    this.removeAllViews();
                    infoCard = new InfoCard();
                    limitCard = null;
                }
                break;

            case LIMIT:
                if (limitCard == null) {
                    this.removeAllViews();
                    limitCard = new LimitCard();
                    infoCard = null;
                }
        }
    }

    public void setCardBackground(@NonNull Color color) {

        View layout = findViewById(R.id.cardLayout);

        switch (color) {

            case BLUE:
                layout.setBackgroundResource(R.drawable.ic_cta_card_blue_bg);
                break;

            case RED:
                layout.setBackgroundResource(R.drawable.ic_cta_card_red_bg);
                break;
        }
    }

    private void init(@NonNull Context context, @NonNull AttributeSet attrs) {

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.DashboardActionCardView);
        try {
            int cardType = typedArray.getInt(R.styleable.DashboardActionCardView_card_type, 0);
            this.setCardTypeInternal(cardType);

            int cardColor = typedArray.getInt(R.styleable.DashboardActionCardView_card_color, 0);
            this.setCardBackgroundInternal(cardColor);

        } finally {
            typedArray.recycle();
        }
    }

    private void setCardTypeInternal(int cardType) {
        switch (cardType) {

            default:
            case 0:
                infoCard = new InfoCard();
                break;

            case 1:
                limitCard = new LimitCard();
                break;
        }
    }

    private void setCardBackgroundInternal(int cardColor) {
        switch (cardColor) {

            default:
            case 0:
                setCardBackground(Color.BLUE);
                break;

            case 1:
                setCardBackground(Color.RED);
                break;
        }
    }

    public void setAction(Action action) {

        ActionTypes actionType = ActionTypes.valueOf(action.getActionType());

        switch (actionType) {

            case PROFILE_STATUS:
                onActionProfileStatus(action);
                break;

            case ADD_MOBILE_WALLET:
                onAddMobileWallet();
                break;

            case REPAY:
                onRepay(action);
                break;

            case FEEDBACK:
                onFeedback();
                break;

            case VERIFICATION_FAILED:
                onVerificationFailed();
                break;

            case LIMIT_REJECTED:
                onLimitRejected();
                break;

            case LIMIT_DENIED:
                onLimitDenied();
                break;

            case LIMIT_EXPIRED:
                onLimitExpired();
                break;

            case DISBURSEMENT_FAILED:
                onDisbursementFailed();
                break;

            case PREVIOUS_ACTION:
                //Must not be implemented because Card doesn't need to update
                break;
        }
    }

    private void onActionProfileStatus(Action action) {
        setCardType(Type.INFO);
        setCardBackground(Color.BLUE);
        InfoCard infoCard = getInfoCard();
        infoCard.setIllustrationImage(R.drawable.ic_card_profile_status);
        infoCard.setTopText(getResources().getString(R.string.profile_status));
        infoCard.setBottomText(getResources().getString(R.string.percent_completed, action.getValue()));
        infoCard.setBottomTextSize(14);
        infoCard.setBtActionText(R.string.string_continue);
        infoCard.setBtActionListener(view -> actionStatusListener.onActionProfile());
    }

    private void onAddMobileWallet() {
        setCardType(Type.INFO);
        setCardBackground(Color.BLUE);
        InfoCard infoCard = getInfoCard();
        infoCard.setIllustrationImage(R.drawable.ic_card_wallet_missing);
        infoCard.setBottomTextSize(16);
        infoCard.setTopText(getResources().getString(R.string.mobile_wallet_missing));
        infoCard.setBottomText(getResources().getString(R.string.add_mobile_wallet));
        infoCard.setBtActionText(R.string.string_add);
        infoCard.setBtActionListener(view -> actionStatusListener.onActionAddWallet());
    }

    private void onRepay(Action action) {
        setCardType(Type.INFO);
        Date date = Utility.getFormattedDateBackendWithZeroTime(action.getValue());
        if (date != null && date.before(Utility.getZeroTimeDate(Calendar.getInstance().getTime())))
            setCardBackground(Color.RED);
        else
            setCardBackground(Color.BLUE);
        InfoCard infoCard = getInfoCard();
        infoCard.setIllustrationImage(R.drawable.ic_card_repay);
        infoCard.setBottomTextSize(13);
        infoCard.setTopText(getResources().getString(R.string.repay_your_advance));
        infoCard.setBottomText(getResources().getString(R.string.due_date).concat(Utility.getFormattedDate(action.getValue(), Constants.INPUT_DATE_FORMAT, Constants.OUTPUT_DATE_FORMAT_WITH_TWO_DIGIT_YEAR)));
        infoCard.setBtActionText(R.string.pay_now);
        infoCard.setBtActionListener(view -> actionStatusListener.onActionRepay());
    }

    private void onFeedback() {
        setCardType(Type.INFO);
        setCardBackground(Color.BLUE);
        InfoCard infoCard = getInfoCard();
        infoCard.setIllustrationImage(R.drawable.ic_card_feedback);
        infoCard.setBottomTextSize(14);
        infoCard.setTopText(getResources().getString(R.string.thankyou_for_repayment));
        infoCard.setBottomText(getResources().getString(R.string.share_your_experience));
        infoCard.setBtActionText(R.string.string_continue);
        infoCard.setBtActionListener(view -> actionStatusListener.onActionFeedback());
    }

    private void onVerificationFailed() {
        setCardType(Type.INFO);
        setCardBackground(Color.RED);
        InfoCard infoCard = getInfoCard();
        infoCard.setIllustrationImage(R.drawable.ic_card_verification_failed);
        infoCard.setBottomTextSize(16);
        infoCard.setTopText(getResources().getString(R.string.profile_verification_failed));
        infoCard.setBottomText(getResources().getString(R.string.resubmit_profile));
        infoCard.setBtActionText(R.string.string_continue);
        infoCard.setBtActionListener(view -> actionStatusListener.onActionProfile());
    }

    private void onLimitRejected() {
        setCardType(Type.INFO);
        setCardBackground(Color.RED);
        InfoCard infoCard = getInfoCard();
        infoCard.setIllustrationImage(R.drawable.ic_card_repay);
        infoCard.setBottomTextSize(16);
        infoCard.setTopText(getResources().getString(R.string.limit_rejected));
        infoCard.setBottomText(getResources().getString(R.string.find_out_why));
        infoCard.setBtActionText(R.string.string_continue);
        infoCard.setBtActionListener(view -> actionStatusListener.onActionLimitRejected());
    }

    private void onLimitDenied() {
        setCardType(Type.INFO);
        setCardBackground(Color.RED);
        InfoCard infoCard = getInfoCard();
        infoCard.setIllustrationImage(R.drawable.ic_card_repay);
        infoCard.setBottomTextSize(16);
        infoCard.setTopText(getResources().getString(R.string.limit_denied));
        infoCard.setBottomText(getResources().getString(R.string.find_out_why));
        infoCard.setBtActionText(R.string.string_continue);
        infoCard.setBtActionListener(view -> actionStatusListener.onActionLimitDenied());
    }

    private void onLimitExpired() {
        setCardType(Type.INFO);
        setCardBackground(Color.RED);
        InfoCard infoCard = getInfoCard();
        infoCard.setIllustrationImage(R.drawable.ic_card_limit_expired);
        infoCard.setBottomTextSize(16);
        infoCard.setTopText(getResources().getString(R.string.loan_limit_expired));
        infoCard.setBottomText(getResources().getString(R.string.reapply_now));
        infoCard.setBtActionText(R.string.string_continue);
        infoCard.setBtActionListener(view -> actionStatusListener.onActionLimitExpired());
    }

    private void onDisbursementFailed() {
        setCardType(Type.INFO);
        setCardBackground(Color.RED);
        InfoCard infoCard = getInfoCard();
        infoCard.setIllustrationImage(R.drawable.ic_card_repay);
        infoCard.setBottomTextSize(16);
        infoCard.setTopText(getResources().getString(R.string.disbursement_failed));
        infoCard.setBottomText(getResources().getString(R.string.something_went_wrong));
        infoCard.setBtActionText(R.string.retry);
        infoCard.setBtActionListener(view -> actionStatusListener.onActionDisbursementFailed());
    }

    public enum Type {

        INFO, LIMIT
    }

    public enum Color {

        BLUE, RED
    }

    public interface ActionStatusListener {

        void onActionProfile();

        void onActionAddWallet();

        void onActionLimitRejected();

        void onActionLimitDenied();

        void onActionLimitExpired();

        void onActionDisbursementFailed();

        void onActionRepay();

        void onActionFeedback();
    }

    private class InfoCard {

        @BindView(R.id.ivIllustration)
        private TezImageView ivIllustration;

        @BindView(R.id.tvTop)
        private TezTextView tvTop;

        @BindView(R.id.tvBottom)
        private TezTextView tvBottom;

        @BindView(R.id.btAction)
        private TezButton btAction;


        private InfoCard() {
            inflate(getContext(), R.layout.card_view_dashboard_action_info, DashboardActionCardView.this);
            ViewBinder.bind(this, DashboardActionCardView.this);
        }

        void setIllustrationImage(@DrawableRes int imageRes) {
            ivIllustration.setImageResource(imageRes);
        }

        void setTopText(String text) {
            tvTop.setText(text);
        }

        void setBottomText(String text) {
            tvBottom.setText(text);
        }

        void setBottomTextSize(int sp) {
            tvBottom.setTextSize(sp);
        }

        void setBtActionText(@StringRes int text) {
            btAction.setText(text);
        }

        void setBtActionListener(@Nullable DoubleTapSafeOnClickListener listener) {
            TezButton btAction = findViewById(R.id.btAction);
            btAction.setDoubleTapSafeOnClickListener(listener);
        }
    }

    private class LimitCard {

        private LimitCard() {
            inflate(getContext(), R.layout.card_view_dashboard_action_limit, DashboardActionCardView.this);
        }

        public void setTitleText(@StringRes int stringRes) {
            TezTextView tvTitle = findViewById(R.id.tvTitle);
            tvTitle.setText(stringRes);
        }

        public void setAmountText(@StringRes int stringRes) {
            TezTextView tvAmount = findViewById(R.id.tvAmount);
            tvAmount.setText(stringRes);
        }

        public void setDateText(@StringRes int stringRes) {
            TezTextView tvDate = findViewById(R.id.tvDate);
            tvDate.setText(stringRes);
        }

        public void setBtActionListener(@Nullable DoubleTapSafeOnClickListener listener) {
            TezButton btAction = findViewById(R.id.btAction);
            btAction.setDoubleTapSafeOnClickListener(listener);
        }
    }
}
