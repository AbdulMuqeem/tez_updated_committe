package com.tez.androidapp.rewamp.committee.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.CommitteeAuthCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeCreationLIstener;
import com.tez.androidapp.rewamp.committee.presenter.ICommitteeTermsCondActivityInteractorOutput;
import com.tez.androidapp.rewamp.committee.request.CommitteeCreateRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeTermsCondActivityInteractor implements ICommitteeTermsCondInteractor {


    private final ICommitteeTermsCondActivityInteractorOutput mICommitteeTermsCondActivityInteractorOutput;

    public CommitteeTermsCondActivityInteractor(ICommitteeTermsCondActivityInteractorOutput mICommitteeTermsCondActivityInteractorOutput) {
        this.mICommitteeTermsCondActivityInteractorOutput = mICommitteeTermsCondActivityInteractorOutput;
    }


    @Override
    public void createCommittee(CommitteeCreateRequest committeeCreateRequest) {
        CommitteeAuthCloudDataStore.getInstance().createCommittee(committeeCreateRequest, new CommitteeCreationLIstener() {

            @Override
            public void onCommitteeCreateSuccess(CommitteeCreateResponse committeeCreateResponse) {
                mICommitteeTermsCondActivityInteractorOutput.onCommitteeCreateSuccess(committeeCreateResponse);
            }

            @Override
            public void onCommitteeCreateFailed(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    createCommittee(committeeCreateRequest);
                else
                    mICommitteeTermsCondActivityInteractorOutput.onCommitteeCreateFailed(errorCode, message);
            }
        });
    }
}
