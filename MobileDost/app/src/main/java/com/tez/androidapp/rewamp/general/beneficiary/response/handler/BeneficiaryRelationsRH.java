package com.tez.androidapp.rewamp.general.beneficiary.response.handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.general.beneficiary.listener.GetBeneficiaryRelationsListener;
import com.tez.androidapp.rewamp.general.beneficiary.response.BeneficiaryRelationsResponse;

public class BeneficiaryRelationsRH extends BaseRH<BeneficiaryRelationsResponse> {

    private GetBeneficiaryRelationsListener listener;

    public BeneficiaryRelationsRH(BaseCloudDataStore baseCloudDataStore, @NonNull GetBeneficiaryRelationsListener listener) {
        super(baseCloudDataStore);
        this.listener = listener;
    }

    @Override
    protected void onSuccess(Result<BeneficiaryRelationsResponse> value) {
        BeneficiaryRelationsResponse response = value.response().body();
        if (response != null) {
            if (isErrorFree(response))
                listener.onGetBeneficiaryRelationSuccess(response.getRelationshipList());
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());
        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        listener.onGetBeneficiaryRelationFailure(errorCode, message);
    }
}
