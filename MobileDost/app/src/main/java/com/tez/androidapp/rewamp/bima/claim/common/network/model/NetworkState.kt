package com.tez.androidapp.rewamp.bima.claim.common.network.model

sealed class NetworkState

object Loading : NetworkState()
object Success : NetworkState()
data class Failure(val errorCode: Int) : NetworkState()


