package com.tez.androidapp.rewamp.profile.edit.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateUserProfileRequest;

public interface IEditProfileVerificationActivityInteractor {
    void updateUserProfile(@NonNull UpdateUserProfileRequest updateUserProfileRequest);

    void validateInfo(@NonNull String mobileNumber);
}
