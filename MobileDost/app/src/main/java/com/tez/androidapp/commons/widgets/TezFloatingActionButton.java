package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import net.tez.fragment.util.base.IBaseWidget;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

/**
 * Created by VINOD KUMAR on 2/15/2019.
 */
public class TezFloatingActionButton extends FloatingActionButton implements IBaseWidget {

    public TezFloatingActionButton(Context context) {
        super(context);
    }

    public TezFloatingActionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TezFloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public void setDoubleTapSafeOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        super.setOnClickListener(doubleTapSafeOnClickListener);
    }
}
