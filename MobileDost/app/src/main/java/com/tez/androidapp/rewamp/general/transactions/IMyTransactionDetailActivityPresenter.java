package com.tez.androidapp.rewamp.general.transactions;

/**
 * Created by VINOD KUMAR on 8/28/2019.
 */
public interface IMyTransactionDetailActivityPresenter {

    void getLoanTransactionDetail(int transactionId);
}
