package com.tez.androidapp.app.general.feature.account.feature.profile.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.GetUserProfileCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.GetUserProfileResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created by Rehman Murad Ali on 9/8/2017.
 */

public class GetUserProfileRH extends BaseRH<GetUserProfileResponse> {
    GetUserProfileCallback getUserProfileCallback;

    public GetUserProfileRH(BaseCloudDataStore baseCloudDataStore, GetUserProfileCallback getUserProfileCallback) {
        super(baseCloudDataStore);
        this.getUserProfileCallback = getUserProfileCallback;
    }

    @Override
    protected void onSuccess(Result<GetUserProfileResponse> value) {
        GetUserProfileResponse getUserProfileResponse = value.response().body();
        if (getUserProfileResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            getUserProfileCallback.onGetUserProfileSuccess(getUserProfileResponse);
        else onFailure(getUserProfileResponse.getStatusCode(), getUserProfileResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        getUserProfileCallback.onGetUserProfileFailure(errorCode, message);
    }
}
