package com.tez.androidapp.rewamp.bima.products.policy.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.bima.products.policy.response.PurchasedInsurancePolicy;
import com.tez.androidapp.rewamp.bima.products.policy.router.SuccessfulPurchaseActivityRouter;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;

import net.tez.fragment.util.optional.Optional;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.OnClick;
import net.tez.viewbinder.library.core.ViewBinder;

public class SuccessfulPurchaseActivity extends BaseActivity {

    @BindView(R.id.ivIcon)
    private TezImageView ivIcon;

    @BindView(R.id.tvTitle)
    private TezTextView tvTitle;

    @BindView(R.id.tvMessage1)
    private TezTextView tvMessage1;

    @BindView(R.id.activation_date)
    private TezTextView tvActivationDate;

    @BindView(R.id.tvPolicyNumber)
    private TezTextView tvPolicyNumber;

    @BindView(R.id.ivProviderLogo)
    private TezImageView ivProviderLogo;

    @BindView(R.id.ivLogo)
    private TezImageView ivLogo;

    @BindView(R.id.tvIssuanceDateValue)
    private TezTextView tvIssuanceDateValue;

    @BindView(R.id.tvActivationDateValue)
    private TezTextView tvActivationDateValue;

    @BindView(R.id.tvExpiryDateValue)
    private TezTextView tvExpiryDateValue;

    @BindView(R.id.tvPremiumValue)
    private TezTextView tvPremiumValue;

    @BindView(R.id.tvCharges)
    private TezTextView tvCharges;

    @BindView(R.id.tvDiscountValue)
    private TezTextView tvDiscountValue;

    @BindView(R.id.tvDiscount)
    private TezTextView tvDiscount;

    @BindView(R.id.tvFinalPriceValue)
    private TezTextView tvFinalPriceValue;

    @BindView(R.id.tvAmountReturn)
    private TezTextView tvAmountReturn;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_successful_purchase);
        ViewBinder.bind(this);
        Optional.ifPresent(getPurchasedPolicy(), this::init, this::routeToDashboard);
    }

    private void init(@NonNull PurchasedInsurancePolicy purchasePolicyResponse) {
        ivIcon.setImageResource(Utility.getInsuranceProductIcon(purchasePolicyResponse.getProductId()));
        tvTitle.setText(purchasePolicyResponse.getPolicyName());
        tvPolicyNumber.setText(purchasePolicyResponse.getPolicyNumber());
        ivProviderLogo.setImageResource(Utility.getInsuranceProviderLogo(purchasePolicyResponse.getPolicyProviderCode()));
        tvIssuanceDateValue.setText(Utility.getFormattedDate(purchasePolicyResponse.getIssueDate(), Constants.INPUT_DATE_FORMAT, Constants.OUTPUT_DATE_FORMAT));
        tvExpiryDateValue.setText(Utility.getFormattedDate(purchasePolicyResponse.getExpiryDate(), Constants.INPUT_DATE_FORMAT, Constants.OUTPUT_DATE_FORMAT));
        tvPremiumValue.setText(getString(R.string.rs_value_string,
                Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(purchasePolicyResponse.getPremium())));
        tvDiscountValue.setText(getString(R.string.rs_value_string,
                Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(purchasePolicyResponse.getDiscount())));
        tvFinalPriceValue.setText(getString(R.string.rs_value_string,
                Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(purchasePolicyResponse.getFinalPrice())));
        tvActivationDateValue.setText(Utility.getFormattedDate(purchasePolicyResponse.getActivationDate(), Constants.INPUT_DATE_FORMAT, Constants.OUTPUT_DATE_FORMAT));

        boolean isDigitalOpd = purchasePolicyResponse.getProductId() == Constants.DIGITAL_OPD_PLAN_ID;
        boolean isCorona = purchasePolicyResponse.getProductId() == Constants.CORONA_DEFENSE_PLAN_ID;

        ivLogo.setImageResource(isDigitalOpd
                ? R.drawable.ic_illustration_digital_opd
                : isCorona
                ? R.drawable.ic_illustration_corona_defense
                : R.drawable.ic_illustration_hospital_coverage);

        tvMessage1.setText(isDigitalOpd
                ? R.string.you_have_successfully_purchased_your_doctor_plan
                : isCorona
                ? R.string.successful_purchase_corona_defense
                : R.string.successful_purchase_policy_desc);

        tvCharges.setText(isDigitalOpd ? R.string.service_fee : R.string.premium);

        tvAmountReturn.setText(isCorona
                ? R.string.amount_paid
                : R.string.final_price);

        tvCharges.setVisibility(isCorona ? View.GONE : View.VISIBLE);
        tvDiscount.setVisibility(isCorona ? View.GONE : View.VISIBLE);
        tvPremiumValue.setVisibility(isCorona ? View.GONE : View.VISIBLE);
        tvDiscountValue.setVisibility(isCorona ? View.GONE : View.VISIBLE);

        if (purchasePolicyResponse.getActivationDate() == null) {
            tvActivationDate.setVisibility(View.GONE);
            tvActivationDateValue.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btOkay)
    private void routeToDashboard() {
        DashboardActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this);
    }

    protected PurchasedInsurancePolicy getPurchasedPolicy() {
        return getIntent().getParcelableExtra(SuccessfulPurchaseActivityRouter.PURCHASED_POLICY);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected String getScreenName() {
        return "SuccessfulPurchaseActivity";
    }
}
