package com.tez.androidapp.rewamp.advance.request.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.advance.request.callback.LoanPricingDetailsCallback;

import net.tez.fragment.util.optional.Optional;

public class LoanPricingDetailsRH extends BaseRH<LoanPricingDetailsResponse> {

    private final LoanPricingDetailsCallback loanPricingDetailsCallback;

    public LoanPricingDetailsRH(BaseCloudDataStore baseCloudDataStore, LoanPricingDetailsCallback loanPricingDetailsCallback) {
        super(baseCloudDataStore);
        this.loanPricingDetailsCallback = loanPricingDetailsCallback;
    }

    @Override
    protected void onSuccess(Result<LoanPricingDetailsResponse> value) {
        Optional.ifPresent(value.response().body(), loanPricingDetailsResponse -> {
            Optional.doWhen(loanPricingDetailsResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR,
                    () -> this.loanPricingDetailsCallback.onSuccessLoanPricingDetails(loanPricingDetailsResponse.getPricing(), loanPricingDetailsResponse.getLatePaymentCharges()),
                    () -> this.onFailure(loanPricingDetailsResponse.getStatusCode(), loanPricingDetailsResponse.getErrorDescription()));
        }, this::sendDefaultFailure);
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.loanPricingDetailsCallback.onFailureLoanPricingDetails(errorCode, message);
    }
}
