package com.tez.androidapp.rewamp.advance.repay.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.repay.view.VerifyJazzRepaymentActivity;

public class VerifyJazzRepaymentActivityRouter extends VerifyEasyPaisaRepaymentRouter {

    public static VerifyJazzRepaymentActivityRouter createInstance() {
        return new VerifyJazzRepaymentActivityRouter();
    }

    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, VerifyJazzRepaymentActivity.class);
    }
}
