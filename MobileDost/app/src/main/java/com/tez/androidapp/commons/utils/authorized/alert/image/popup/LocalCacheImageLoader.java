package com.tez.androidapp.commons.utils.authorized.alert.image.popup;

import android.net.Uri;
import androidx.annotation.NonNull;

/**
 * Created by FARHAN DHANANI on 7/8/2018.
 */
public interface LocalCacheImageLoader extends ImageLoader {
    void setFileToLoad(@NonNull Uri fileToLoad);
}
