package com.tez.androidapp.app.general.feature.faq.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 2/28/2017.
 */

public class GetFAQsRequest extends BaseRequest{

    public static final String METHOD_NAME = "v1/common/faqs";

    public static final class Params {
        public static final String LANGUAGE_CODE = "languageCode";

        private Params() {
        }
    }
}
