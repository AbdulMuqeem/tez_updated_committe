package com.tez.androidapp.rewamp.advance.limit.router;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.limit.view.CheckYourLimitPermissionsActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.NewLoginPermissionsActivity;
import com.tez.androidapp.rewamp.signup.router.NewLoginPermissionsAcivityRouter;

public class CheckYourLimitPermissionsActivityRouter extends BaseActivityRouter {

    public static CheckYourLimitPermissionsActivityRouter createInstance() {
        return new CheckYourLimitPermissionsActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CheckYourLimitPermissionsActivity.class);
    }
}
