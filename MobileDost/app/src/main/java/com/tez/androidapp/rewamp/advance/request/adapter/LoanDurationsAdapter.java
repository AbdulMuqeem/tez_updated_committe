package com.tez.androidapp.rewamp.advance.request.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezCardView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.advance.request.model.LoanDuration;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rehman Murad Ali
 **/
public class LoanDurationsAdapter extends GenericRecyclerViewAdapter<LoanDuration, LoanDurationsAdapter.LoanDurationListener, LoanDurationsAdapter.LoanDurationViewHolder> {

    private List<LoanDuration> completeList = new ArrayList<>();

    private int selectedLoanDurationPosition = 0;

    public LoanDurationsAdapter(@NonNull List<LoanDuration> items, @Nullable LoanDurationListener listener) {
        super(items, listener);
        this.completeList.addAll(items);
        setLoanAmount(1000);
    }

    @NonNull
    @Override
    public LoanDurationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_loan_duration, parent, false);
        return new LoanDurationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LoanDurationViewHolder holder, int position) {
        holder.onBind(position, getItems(), getListener());
    }

    public LoanDuration getSelectedLoanDuration() {
        return get(selectedLoanDurationPosition);
    }

    public void setLoanAmount(double loanAmount) {
        List<LoanDuration> items = new ArrayList<>();
        for (LoanDuration item : completeList) {
            if (loanAmount >= item.getMinAmount() && loanAmount <= item.getMaxAmount())
                items.add(item);
        }
        if (!items.equals(getItems())) {
            selectedLoanDurationPosition = items.indexOf(getItems().get(selectedLoanDurationPosition));
            if (selectedLoanDurationPosition == -1)
                selectedLoanDurationPosition = 0;
            setItems(items);
        }
    }

    public interface LoanDurationListener extends BaseRecyclerViewListener {

        void onClickLoanDuration(@NonNull LoanDuration tenureDetail);
    }

    class LoanDurationViewHolder extends BaseViewHolder<LoanDuration, LoanDurationListener> {

        @BindView(R.id.cardViewContainer)
        private TezCardView cardViewContainer;

        @BindView(R.id.tvTitle)
        private TezTextView tvTitle;


        private LoanDurationViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(int position, List<LoanDuration> items, @Nullable LoanDurationListener listener) {
            LoanDuration item = items.get(position);
            tvTitle.setText(item.getTitle());

            cardViewContainer.setOnClickListener(v -> {
                if (selectedLoanDurationPosition != position) {
                    notifyItemChanged(selectedLoanDurationPosition);
                    notifyItemChanged(position);
                    selectedLoanDurationPosition = position;
                }
            });

            boolean isSelected = selectedLoanDurationPosition == position;

            if (listener != null && isSelected)
                listener.onClickLoanDuration(item);

            cardViewContainer.setCardBackgroundColor(Utility.getColorFromResource(isSelected
                    ? R.color.textViewTextColorGreen
                    : R.color.screen_background));

            tvTitle.setTextColor(Utility.getColorFromResource(isSelected
                    ? R.color.colorWhite
                    : R.color.editTextTextColorGrey));
        }
    }
}
