package com.tez.androidapp.rewamp.bima.products.adapter

import android.view.View
import android.view.ViewGroup
import com.tez.androidapp.R
import com.tez.androidapp.app.vertical.bima.policies.models.network.Policy
import com.tez.androidapp.commons.utils.app.Utility
import kotlinx.android.synthetic.main.list_item_policy.view.*
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener
import net.tez.tezrecycler.base.viewholder.BaseViewHolder

class PoliciesAdapter(items: List<Policy>, listener: PolicyListener?)
    : GenericRecyclerViewAdapter<Policy, PoliciesAdapter.PolicyListener?, PoliciesAdapter.PolicyViewHolder>(items, listener) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PolicyViewHolder {
        val view = inflate(parent.context, R.layout.list_item_policy, parent)
        return PolicyViewHolder(view)
    }

    interface PolicyListener : BaseRecyclerViewListener {
        fun onClickPolicy(policy: Policy)
    }

    class PolicyViewHolder constructor(itemView: View) : BaseViewHolder<Policy, PolicyListener?>(itemView) {

        override fun onBind(item: Policy, listener: PolicyListener?) {
            super.onBind(item, listener)
            itemView.run {
                ivIcon.setImageResource(Utility.getInsuranceProductIcon(item.id))
                tvTitle.text = item.policyName
                tvDescription.text = item.policyNumber
                tvIssueDate.text = item.dateApplied
                tvExpiryDate.text = item.expiryDate
                tvActivationDate.text = if (item.activationDate == null) "-" else item.activationDate
                tllActivationDate.visibility = if (item.activationDate == null) View.GONE else View.VISIBLE
            }
            setItemViewOnClickListener { listener?.onClickPolicy(item) }
        }
    }
}