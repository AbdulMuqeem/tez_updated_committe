package com.tez.androidapp.app.general.feature.transactions.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.transactions.callbacks.GetUserTransactionsCallback;
import com.tez.androidapp.app.general.feature.transactions.models.network.dto.response.GetUserTransactionsResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created  on 9/2/2017.
 */

public class GetUserTransactionsRH extends BaseRH<GetUserTransactionsResponse> {

    private GetUserTransactionsCallback getUserTransactionsCallback;

    public GetUserTransactionsRH(BaseCloudDataStore baseCloudDataStore, GetUserTransactionsCallback getUserTransactionsCallback) {
        super(baseCloudDataStore);
        this.getUserTransactionsCallback = getUserTransactionsCallback;
    }

    @Override
    protected void onSuccess(Result<GetUserTransactionsResponse> value) {
        GetUserTransactionsResponse getUserTransactionsResponse = value.response().body();
        if (isErrorFree(getUserTransactionsResponse)) {
            getUserTransactionsCallback.onGetUserTransactionsSuccess(getUserTransactionsResponse);
        } else onFailure(getUserTransactionsResponse.getStatusCode(), getUserTransactionsResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        getUserTransactionsCallback.onGetUserTransactionsFailure(errorCode, message);
    }
}
