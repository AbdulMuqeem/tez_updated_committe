package com.tez.androidapp.rewamp.signup;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

/**
 * Created by FARHAN DHANANI on 6/1/2019.
 */
public class TermsAndConditionAdapter extends GenericRecyclerViewAdapter<TermsAndCondition,
        BaseRecyclerViewListener,
        TermsAndConditionAdapter.TermsAndConditionViewHolder> {

    public TermsAndConditionAdapter(@NonNull List<TermsAndCondition> items) {
        super(items);
    }

    @NonNull
    @Override
    public TermsAndConditionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.terms_and_condition_item, parent, false);
        return new TermsAndConditionViewHolder(view);
    }

    class TermsAndConditionViewHolder extends BaseViewHolder<TermsAndCondition, BaseRecyclerViewListener> {

        @BindView(R.id.tvTermsAndCondition)
        private TezTextView textViewTermsAndCondition;

        private TermsAndConditionViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(TermsAndCondition item, @Nullable BaseRecyclerViewListener listener) {
            super.onBind(item, listener);
            this.textViewTermsAndCondition.setText(item.getTermsAndCondtion());
        }

    }
}
