package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto;

import androidx.annotation.NonNull;

import net.tez.logger.library.utils.TextUtil;

import java.util.List;
import java.util.Map;

public class Option {
    private String refName;
    private String text;
    private List<Answer> answers;

    public String getRefName() {
        return refName;
    }

    public void setRefName(String refName) {
        this.refName = refName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    @NonNull
    @Override
    public String toString() {
        return text;
    }
}
