package com.tez.androidapp.app.vertical.bima.policies.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.bima.policies.models.network.Policy;

import java.util.List;

/**
 * Created by Rehman Murad Ali on 8/25/2017.
 */

public class GetInsurancePoliciesResponse extends BaseResponse {

    private List<Policy> policies;

    public List<Policy> getPolicies() {
        return policies;
    }

    public void setPolicies(List<Policy> policies) {
        this.policies = policies;
    }

}
