package com.tez.androidapp.rewamp.profile.trust.presenter;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.UserProfile;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.rewamp.profile.trust.interactor.IProfileTrustActivityInteractor;
import com.tez.androidapp.rewamp.profile.trust.interactor.ProfileTrustActivityInteractor;
import com.tez.androidapp.rewamp.profile.trust.view.IProfileTrustActivityView;

public class ProfileTrustActivityPresnter
        implements IProfileTrustActivityPresenter, IProfileTrustActivityInteractorOutput {

    private final IProfileTrustActivityInteractor iProfileTrustActivityInteractor;
    private final IProfileTrustActivityView iProfileTrustActivityView;

    public ProfileTrustActivityPresnter(IProfileTrustActivityView iProfileTrustActivityView) {
        this.iProfileTrustActivityView = iProfileTrustActivityView;
        this.iProfileTrustActivityInteractor = new ProfileTrustActivityInteractor(this);
    }

    @Override
    public void getCnicUploads(){
        this.iProfileTrustActivityInteractor.getCnicUploads();
    }

    @Override
    public void onGetCnicUploadsCallbackSuccess(UserProfile userProfile) {
        this.iProfileTrustActivityView.setTezLoaderToBeDissmissedOnTransition();
        Optional.doWhen(userProfile.getRequestId()!=null,
                this.iProfileTrustActivityView::routeToCnicInformationActivity,
                ()->Optional.doWhen(userProfile.getCnicUploads()==null,
                        this.iProfileTrustActivityView::routeToCnicInformationActivity,
                        ()-> this.loadProfileCompletionCnicUploadActivity(userProfile.getCnicUploads())));
    }

    private void loadProfileCompletionCnicUploadActivity(String [] cnicUploads){
        boolean [] flagsForPicturesNeedToUploaded = getFlagsForPicturesNeedsToUploaded(cnicUploads);
        this.iProfileTrustActivityView.routeToProfileCompletionCnicUploadActivity(
                flagsForPicturesNeedToUploaded[0],
                flagsForPicturesNeedToUploaded[1],
                flagsForPicturesNeedToUploaded[2]);
    }

    @Override
    public void onGetCnicUploadsCallbackFailure(int errorCode, String message) {
        this.iProfileTrustActivityView.dismissTezLoader();
        this.iProfileTrustActivityView.showError(errorCode);
        //this.iProfileTrustActivityView.routeToCnicInformationActivity();
    }
}
