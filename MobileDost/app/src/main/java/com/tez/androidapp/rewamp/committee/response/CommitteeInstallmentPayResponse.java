package com.tez.androidapp.rewamp.committee.response;

import com.tez.androidapp.app.base.response.BaseResponse;

import java.io.Serializable;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class CommitteeInstallmentPayResponse extends BaseResponse implements Serializable {


    public String message;
    public PaymentInfo paymentInfo;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public static class PaymentInfo implements Serializable{
        public int round;
        public int committeeAmount;
        public int installmentAmount;
        public String walletProvider;
        public String walletNumber;
        public String transactionId;
        public String date;


        public int getRound() {
            return round;
        }

        public void setRound(int round) {
            this.round = round;
        }

        public int getCommitteeAmount() {
            return committeeAmount;
        }

        public void setCommitteeAmount(int committeeAmount) {
            this.committeeAmount = committeeAmount;
        }

        public int getInstallmentAmount() {
            return installmentAmount;
        }

        public void setInstallmentAmount(int installmentAmount) {
            this.installmentAmount = installmentAmount;
        }

        public String getWalletProvider() {
            return walletProvider;
        }

        public void setWalletProvider(String walletProvider) {
            this.walletProvider = walletProvider;
        }

        public String getWalletNumber() {
            return walletNumber;
        }

        public void setWalletNumber(String walletNumber) {
            this.walletNumber = walletNumber;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
    }
}




