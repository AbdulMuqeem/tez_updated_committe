package com.tez.androidapp.rewamp.advance.limit.view;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.advance.limit.router.CheckYourLimitActivityRouter;
import com.tez.androidapp.rewamp.advance.limit.router.CheckYourLimitPermissionsActivityRouter;
import com.tez.androidapp.rewamp.signup.PermissionActivity;

import java.util.List;

public class CheckYourLimitPermissionsActivity extends PermissionActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected String getScreenName() {
        return "CheckYourLimitPermissionsActivity";
    }

    @Override
    protected List<String> getRequiredPermissions() {
        List<String> missingPermissions =  Utility.getSignupPermissions(this);
        missingPermissions.addAll(Utility.getOptionalPermissions(this));
        return missingPermissions;
    }

    protected void onAllPermissionsGranted() {
       super.onAllPermissionsGranted();
        CheckYourLimitActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    protected boolean isOptionalPermissionNotRequired() {
        return false;
    }
}
