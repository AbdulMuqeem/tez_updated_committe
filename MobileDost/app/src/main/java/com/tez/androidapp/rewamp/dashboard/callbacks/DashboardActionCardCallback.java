package com.tez.androidapp.rewamp.dashboard.callbacks;

import com.tez.androidapp.rewamp.dashboard.response.DashboardActionCardResponse;

public interface DashboardActionCardCallback {

    void onDashboardActionCardSuccess(DashboardActionCardResponse dashboardActionCardResponse);

    void onDashboardActionCardFailure(int errorCode, String message);
}
