package com.tez.androidapp.rewamp.advance.request.response;

import com.tez.androidapp.app.base.response.BaseResponse;

import java.util.List;

public class LoanRemainingStepsResponse extends BaseResponse {

    private List<String> stepsLeft;

    public List<String> getStepsLeft() {
        return stepsLeft;
    }
}
