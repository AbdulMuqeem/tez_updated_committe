package com.tez.androidapp.app.general.feature.account.feature.profile.cnic.callbacks;

import com.tez.androidapp.app.general.feature.account.feature.profile.cnic.models.network.dto.response.SaveAttachmentsResponse;

/**
 * Created  on 2/15/2017.
 */

public interface SaveAttachmentsCallback {

    void onSaveAttachmentsSuccess(SaveAttachmentsResponse saveAttachmentsResponse);

    void onSaveAttachmentsFailure(int errorCode, String message);
}
