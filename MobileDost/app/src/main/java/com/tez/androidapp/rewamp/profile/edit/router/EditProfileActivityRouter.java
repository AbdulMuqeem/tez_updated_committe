package com.tez.androidapp.rewamp.profile.edit.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.profile.edit.view.EditProfileActivity;

public class EditProfileActivityRouter extends BaseActivityRouter {

    public static EditProfileActivityRouter createInstance() {
        return new EditProfileActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, EditProfileActivity.class);
    }
}
