package com.tez.androidapp.rewamp.general.transactions;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.IBaseView;

import java.util.List;

/**
 * Created by VINOD KUMAR on 8/28/2019.
 */
public interface IMyTransactionDetailActivityView extends IBaseView {

    void setTransactionSummaryError();

    void setLayoutEmptyTransactionDetailVisibility(int visibility);

    void setTvRemainingAmountText(String text);

    void setTvRemainingAmountVisibility();

    void setRvMyTransactionDetailsVisibility(int visibility);

    void setMyTransactionDetailAdapter(@NonNull List<MyTransactionDetail> myTransactionDetailList);
}
