package com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks;

import com.tez.androidapp.commons.models.network.LoanDetails;

/**
 * Created  on 8/29/2017.
 */

public interface RepayReceiptCallback {

    void onRepayReceiptSuccess(LoanDetails loanDetails);

    void onRepayReceiptFailure(int errorCode, String message);
}
