package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.listener.CommitteeInstallmentPayLIstener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeWalletVerificationLIstener;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface ICommitteeWalletVerificationActivityInteractorOutput extends CommitteeWalletVerificationLIstener, CommitteeInstallmentPayLIstener {
}
