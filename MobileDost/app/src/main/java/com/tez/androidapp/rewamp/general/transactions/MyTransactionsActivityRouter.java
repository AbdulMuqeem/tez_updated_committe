package com.tez.androidapp.rewamp.general.transactions;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

/**
 * Created by VINOD KUMAR on 8/28/2019.
 */
public class MyTransactionsActivityRouter extends BaseActivityRouter {

    public static MyTransactionsActivityRouter createInstance() {
        return new MyTransactionsActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, MyTransactionsActivity.class);
    }
}
