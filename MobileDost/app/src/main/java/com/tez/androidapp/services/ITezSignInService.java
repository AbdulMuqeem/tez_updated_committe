package com.tez.androidapp.services;

import android.content.Intent;

import androidx.annotation.NonNull;

public interface ITezSignInService {

    @NonNull
    Intent getSignInIntent();
}
