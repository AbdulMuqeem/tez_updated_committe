package com.tez.androidapp.app.vertical.bima.policies.handlers;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.vertical.bima.policies.callbacks.InsurancePolicyDetailsCallback;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.response.InsurancePolicyDetailsResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by Vinod Kumar on 5/5/2020.
 */
public class InsurancePolicyDetailsRH extends BaseRH<InsurancePolicyDetailsResponse> {

    private InsurancePolicyDetailsCallback insurancePolicyDetailsCallback;

    public InsurancePolicyDetailsRH(BaseCloudDataStore baseCloudDataStore,
                                    @NonNull InsurancePolicyDetailsCallback insurancePolicyDetailsCallback) {
        super(baseCloudDataStore);
        this.insurancePolicyDetailsCallback = insurancePolicyDetailsCallback;
    }

    @Override
    protected void onSuccess(Result<InsurancePolicyDetailsResponse> value) {
        InsurancePolicyDetailsResponse response = value.response().body();
        if (response != null) {

            if (isErrorFree(response))
                insurancePolicyDetailsCallback.onGetActiveInsurancePolicyDetailsSuccess(response.getMobileUserPolicyDto());
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());

        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.insurancePolicyDetailsCallback.onGetActiveInsurancePolicyDetailsFailure(errorCode, message);
    }
}
