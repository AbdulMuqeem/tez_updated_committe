package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 4/12/2017.
 */

public class UploadDeviceDataRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/upload/deviceData";

    public abstract static class Params {
        public static final String FILE = "file";
        public static final String IMEI = "imei";
        public static final String PRINCIPAL_NAME = "principalName";
        public static final String LATITUDE = "lat";
        public static final String LONGITUDE = "lng";
    }
}
