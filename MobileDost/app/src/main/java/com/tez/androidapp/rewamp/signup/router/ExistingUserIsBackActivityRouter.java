package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.ExistingUserIsBackActivity;

/**
 * Created by VINOD KUMAR on 8/7/2019.
 */
public class ExistingUserIsBackActivityRouter extends BaseActivityRouter {

    public static final String MOBILE_NUMBER = "MOBILE_NUMBER";
    public static final String SOCIAL_ID = "SOCIAL_ID";
    public static final String SOCIAL_TYPE = "SOCIAL_TYPE";
    public static final String REF_CODE = "REF_CODE";



    public static ExistingUserIsBackActivityRouter createInstance() {
        return new ExistingUserIsBackActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        @NonNull String mobileNumber,
                                        @Nullable String socialId,
                                        @Nullable Integer socialType,
                                        @Nullable String refCode) {
        Intent intent = createIntent(from);
        intent.putExtra(MOBILE_NUMBER, mobileNumber);
        intent.putExtra(SOCIAL_ID, socialId);
        intent.putExtra(SOCIAL_TYPE, socialType);
        intent.putExtra(REF_CODE, refCode);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, ExistingUserIsBackActivity.class);
    }
}
