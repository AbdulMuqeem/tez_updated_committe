package com.tez.androidapp.rewamp.committee.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;

import java.util.List;

public class CommitteeWalletResponse extends BaseResponse implements Parcelable {


    public List<Wallet> wallets;

    public List<Wallet> getWallets() {
        return wallets;
    }

    public void setWallets(List<Wallet> wallets) {
        this.wallets = wallets;
    }

    protected CommitteeWalletResponse(Parcel in) {
        wallets = in.createTypedArrayList(Wallet.CREATOR);
    }

    public static final Creator<CommitteeWalletResponse> CREATOR = new Creator<CommitteeWalletResponse>() {
        @Override
        public CommitteeWalletResponse createFromParcel(Parcel in) {
            return new CommitteeWalletResponse(in);
        }

        @Override
        public CommitteeWalletResponse[] newArray(int size) {
            return new CommitteeWalletResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(wallets);
    }
}
