package com.tez.androidapp.rewamp.advance.limit.presenter;

import com.tez.androidapp.app.vertical.advance.loan.shared.limit.callbacks.LoanLimitCallback;

public interface ILimitAssignedActivityInteractorOutput extends LoanLimitCallback {
}
