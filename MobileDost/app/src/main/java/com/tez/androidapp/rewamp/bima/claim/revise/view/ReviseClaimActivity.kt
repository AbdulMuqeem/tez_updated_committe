package com.tez.androidapp.rewamp.bima.claim.revise.view

import android.os.Bundle
import com.tez.androidapp.R
import com.tez.androidapp.app.base.ui.activities.BaseActivity
import com.tez.androidapp.commons.widgets.StepperView
import com.tez.androidapp.rewamp.bima.claim.common.success.router.ClaimSubmittedActivityRouter
import com.tez.androidapp.rewamp.bima.claim.model.InsuranceClaimConfirmationDto
import com.tez.androidapp.rewamp.bima.claim.revise.listener.ReviseClaimListener
import com.tez.androidapp.rewamp.bima.claim.revise.router.ReviseClaimActivityRouter
import com.tez.androidapp.rewamp.general.fragment.TezNavHostFragment
import kotlinx.android.synthetic.main.activity_revise_claim.*

class ReviseClaimActivity : BaseActivity(), ReviseClaimListener {

    private lateinit var dependencies: ReviseClaimActivityRouter.Dependencies

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_revise_claim)
        dependencies = ReviseClaimActivityRouter.getDependencies(intent)
        init()
    }

    override fun onClaimSubmitted(insuranceClaimConfirmationDto: InsuranceClaimConfirmationDto) {
        ClaimSubmittedActivityRouter().setDependenciesAndRoute(this,
                dependencies.productId,
                true,
                insuranceClaimConfirmationDto)
        finish()
    }

    private fun init() {
        initGraph()
        initStepper()
    }

    private fun initGraph() {
        val tezNavHostFragment = supportFragmentManager.findFragmentById(R.id.tezNavHostFragment) as TezNavHostFragment
        val navGraph = tezNavHostFragment.navController.navInflater.inflate(R.navigation.nav_graph_revise_claim)
        val bundle = Bundle()
        bundle.putInt("claimId", dependencies.claimId)
        bundle.putString("rejectedDocumentIds", dependencies.rejectedDocumentIds)
        bundle.putStringArray("comments", dependencies.comments)
        tezNavHostFragment.navController.setGraph(navGraph, bundle)
    }

    private fun initStepper() {
        stepperView.setSteps(listOf(
                StepperView.Step(1, R.string.evidence, R.drawable.ic_evidence_active),
        ))
    }

    override fun getScreenName(): String = "ReviseClaimActivity"
}