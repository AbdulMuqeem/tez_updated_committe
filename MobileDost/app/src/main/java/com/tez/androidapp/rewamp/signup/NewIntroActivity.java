package com.tez.androidapp.rewamp.signup;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.ToolbarActivity;
import com.tez.androidapp.commons.models.app.FacebookData;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.utils.textwatchers.PhoneNumberTextWatcher;
import com.tez.androidapp.commons.validators.annotations.TextInputLayoutRegex;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezImageButton;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;
import com.tez.androidapp.rewamp.signup.presenter.IIntroActivityPresenter;
import com.tez.androidapp.rewamp.signup.presenter.IntroActivityPresenter;
import com.tez.androidapp.rewamp.signup.router.ExistingUserIsBackActivityRouter;
import com.tez.androidapp.rewamp.signup.router.FacebookAuthFailedActivityRouter;
import com.tez.androidapp.rewamp.signup.router.GoogleAuthFailedActivityRouter;
import com.tez.androidapp.rewamp.signup.router.StartMyJourneyActivityRouter;
import com.tez.androidapp.rewamp.signup.view.IIntroActivityView;
import com.tez.androidapp.services.TezCrashlytics;
import com.tez.androidapp.services.TezDynamicLinks;
import com.tez.androidapp.services.TezSignInService;
import com.tez.androidapp.services.exception.TezApiException;
import com.tez.androidapp.services.models.TezSignInAccount;
import com.tez.androidapp.views.TezAuthIdButton;

import net.tez.fragment.util.optional.Optional;
import net.tez.validator.library.annotations.Order;
import net.tez.validator.library.core.FieldValidator;
import net.tez.validator.library.core.FilterChain;
import net.tez.validator.library.core.ValidationError;
import net.tez.validator.library.core.ValidationListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import org.json.JSONObject;

import java.util.Arrays;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class NewIntroActivity extends ToolbarActivity implements ValidationListener,
        IIntroActivityView, FacebookCallback<LoginResult>, GraphRequest.GraphJSONObjectCallback {
    private static final int RC_SIGN_IN = 9001;
    private static final int RESOLVE_HINT = 19980;

    private final IIntroActivityPresenter iIntroActivityPresenter;

    @BindView(R.id.etMobileNumber)
    private TezEditTextView etMobileNumber;

    @BindView(R.id.btFacebook)
    private TezImageButton tezImageButtonFaceBook;

    @BindView(R.id.btAuthId)
    private TezAuthIdButton btAuthId;

    @TextInputLayoutRegex(regex = Constants.PHONE_NUMBER_VALIDATOR_REGEX, value = {1, R.string.enter_number_in_format})
    @Order(1)
    @BindView(R.id.tilMobileNumber)
    private TezTextInputLayout tilMobileNumber;

    @BindView(R.id.btNext)
    private TezButton tezButtonNext;

    private String refCode = "";

    private TezSignInService tezSignInService;

    private CompositeDisposable allDisposables;

    public NewIntroActivity() {
        iIntroActivityPresenter = new IntroActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_intro);
        ViewBinder.bind(this);
        initClickListeners();
        initAccountSignIn();
        getReferralCodeFromDynamicLink();
        requestPhoneNumber();
        etMobileNumber.addTextChangedListener(new PhoneNumberTextWatcher(etMobileNumber));
    }

    private void requestPhoneNumber() {
        TezSignInService.requestPhoneNumber(this, RESOLVE_HINT);
    }

    private void getReferralCodeFromDynamicLink() {
        TezDynamicLinks.getInstance()
                .getDynamicLink(this, getIntent())
                .addOnSuccessListener(pendingDynamicLinkData ->
                        Optional.ifPresent(pendingDynamicLinkData, linkData -> {
                            Optional.ifPresent(linkData.getLink(), link -> {
                                this.refCode = link.getQueryParameter(Constants.REF_CODE);
                            });
                        }))
                .addOnFailureListener(e -> {
                    TezCrashlytics.getInstance().recordException(e);
                    TezCrashlytics.getInstance().log("Deep linking failed to recieve ref Code");
                });
    }

    private void initAccountSignIn() {
        tezSignInService = TezSignInService.init(this);
    }

    private void signIn() {
        Intent signInIntent = tezSignInService.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.etMobileNumber.clearText();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    private void initClickListeners() {
        this.tezButtonNext.setDoubleTapSafeOnClickListener(view -> onClickNextButton());
        this.tezImageButtonFaceBook.setDoubleTapSafeOnClickListener(view -> onClickTezImageButtonFacebook());
        this.btAuthId.setDoubleTapSafeOnClickListener(view -> signIn());
    }

    private void introValidationsPassed() {
        this.iIntroActivityPresenter.callUserInfo(getTextFromEtMobileNumber(),
                null,
                Utility.PrincipalType.MOBILE_NUMBER.getValue());
    }

    private String getTextFromEtMobileNumber() {
        return this.etMobileNumber.getValueText();
    }

    private void onClickNextButton() {
        FieldValidator.validate(this, new ValidationListener() {
            @Override
            public void validateSuccess() {
                introValidationsPassed();
            }

            @Override
            public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
                Utility.setErrorOnTezViews(validationError.getView(), validationError.getMessage());
                filterChain.doFilter();
            }
        });
    }

    private void onClickTezImageButtonFacebook() {
        try {
            LoginManager.getInstance().logInWithReadPermissions(this,
                    Arrays.asList(Constants.KEY_USER_EMAIL,
                            Constants.KEY_USER_PUBLIC_PROFILE));
        } catch (Exception e) {
            //Left
            FacebookAuthFailedActivityRouter.createInstance().setDependenciesAndRoute(this);
        }
    }

    private void registeringCallBacksForFacebook(int requestCode, int resultCode, @Nullable Intent intent) {
        CallbackManager callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, this);
        callbackManager.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Optional.doWhen(requestCode == RC_SIGN_IN, () -> handleGoogleSignInResult(data), () -> registeringCallBacksForFacebook(requestCode, resultCode, data));

        if (requestCode == RESOLVE_HINT && resultCode == RESULT_OK && data != null) {

            String number = TezSignInService.extractPhoneNumberFromIntent(data);

            if (number != null) {
                number = Utility.getFormattedMobileNumber(number);
                etMobileNumber.setText(number);
                etMobileNumber.setSelection(Math.min(number.length(), 11));
            }
        }
    }

    private void handleGoogleSignInResult(Intent data) {

        try {
            TezSignInAccount account = TezSignInService.getSignedInAccountFromIntent(data).getResultWithException();
            Optional.ifPresent(account, ac -> {
                this.iIntroActivityPresenter.callUserInfo(null,
                        ac.getId(),
                        Utility.PrincipalType.GOOGLE.getValue());
            });
        } catch (TezApiException e) {
            Optional.doWhen(e.getStatusCode() != 12501, () ->
                    GoogleAuthFailedActivityRouter.createInstance().setDependenciesAndRoute(this));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.allDisposables = new CompositeDisposable();
        listenChangesOnViews();
    }

    private void listenChangesOnViews() {
        allDisposables.add(RxTextView.textChanges(this.etMobileNumber).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));
    }

    @Override
    public void validateSuccess() {
        this.tezButtonNext.setButtonNormal();
    }

    @Override
    public void validatePassed(@NonNull View v) {
        Utility.setErrorOnTezViews(v, null);
    }

    @Override
    public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
        this.tezButtonNext.setButtonInactive();
        filterChain.doFilter();
    }

    @Override
    public void navigateToLoginActivity(String mobileNumber, String socialId, int socialType) {
        ExistingUserIsBackActivityRouter.createInstance().setDependenciesAndRoute(this, mobileNumber, socialId, socialType, this.refCode);
    }

    @Override
    public void navigateToSignUpActivity(String mobileNumber, String socialId, int socialType) {
        StartMyJourneyActivityRouter.createInstance().setDependenciesAndRoute(this, mobileNumber, socialId, socialType, this.refCode);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        Utility.getDataFromFacebook(loginResult.getAccessToken(), this);
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {
        //DialogUtil.showInformativeMessage(this, Utility.getStringFromResource(R.string.facebook_login_error));
        FacebookAuthFailedActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void onCompleted(JSONObject object, GraphResponse response) {
        FacebookData facebookData = Utility.extractJsonDataFromFacebook(response);
        this.iIntroActivityPresenter.callUserInfo(null,
                facebookData.getId(),
                Utility.PrincipalType.FACEBOOK.getValue());
    }

    @Override
    protected String getScreenName() {
        return "NewIntroActivity";
    }
}
