package com.tez.androidapp.rewamp.bima.products.policy.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.bima.products.policy.view.CoronaDefenseActivity;

public class CoronaDefenseActivityRouter extends BaseProductActivityRouter {

    public static CoronaDefenseActivityRouter createInstance() {
        return new CoronaDefenseActivityRouter();
    }

    @NonNull
    @Override
    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CoronaDefenseActivity.class);
    }
}
