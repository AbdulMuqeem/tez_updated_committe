package com.tez.androidapp.app.base.observer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;

import com.tez.androidapp.commons.utils.app.Constants;
import net.tez.fragment.util.optional.Optional;

/**
 * Created by VINOD KUMAR on 2/21/2019.
 */
public class SessionTimeOutObserver implements DefaultLifecycleObserver {

    @Nullable
    private OnAppStart clientObserver;

    private long inActiveTimeStamp = 0;
    private boolean isAppActive = false;

    private SessionTimeOutObserver() {

    }

    public static SessionTimeOutObserver getInstance() {
        return LazyHolder.INSTANCE;
    }


    @Override
    public void onStart(@NonNull LifecycleOwner owner) {
        setAppActive(true);
        Optional.ifPresent(getClientObserver(), observer -> {
            observer.onAppStart(inActiveTimeExceeded() && getInActiveTimeStamp() != 0);
        });
    }


    @Override
    public void onStop(@NonNull LifecycleOwner owner) {
        setInActiveTimeStamp(getCurrentSystemTime());
        setAppActive(false);
    }


    private boolean inActiveTimeExceeded() {
        return (getCurrentSystemTime() - getInActiveTimeStamp()) > Constants.THIRTY_SECONDS;
    }

    private long getCurrentSystemTime() {
        return System.currentTimeMillis();
    }

    @Nullable
    private OnAppStart getClientObserver() {
        return clientObserver;
    }

    public void setClientObserver(@Nullable OnAppStart clientObserver) {
        this.clientObserver = clientObserver;
    }

    private long getInActiveTimeStamp() {
        return inActiveTimeStamp;
    }

    private void setInActiveTimeStamp(long inActiveTimeStamp) {
        this.inActiveTimeStamp = inActiveTimeStamp;
    }

    public boolean isAppActive() {
        return isAppActive;
    }

    private void setAppActive(boolean appActive) {
        this.isAppActive = appActive;
    }

    private static final class LazyHolder {
        private static final SessionTimeOutObserver INSTANCE = new SessionTimeOutObserver();

        private LazyHolder() {

        }
    }
}
