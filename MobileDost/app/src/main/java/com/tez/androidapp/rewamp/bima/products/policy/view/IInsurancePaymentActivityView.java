package com.tez.androidapp.rewamp.bima.products.policy.view;

public interface IInsurancePaymentActivityView extends IEasyPaisaInsurancePaymentActivityView {
    void setTvResendCodeEnabled(boolean enabled);

    void startTimer();
}
