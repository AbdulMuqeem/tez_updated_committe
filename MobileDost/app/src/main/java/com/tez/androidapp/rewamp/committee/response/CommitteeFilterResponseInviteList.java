
package com.tez.androidapp.rewamp.committee.response;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CommitteeFilterResponseInviteList implements Serializable {

    @SerializedName("inviteId")
    private Long mInviteId;
    @SerializedName("mobilNumber")
    private String mMobilNumber;
    @SerializedName("status")
    private String mStatus;

    private boolean header;
    private String joiningDate;

    public String getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
    }

    public boolean isHeader() {
        return header;
    }

    public void setHeader(boolean header) {
        this.header = header;
    }

    public Long getInviteId() {
        return mInviteId;
    }

    public void setInviteId(Long inviteId) {
        mInviteId = inviteId;
    }

    public String getMobilNumber() {
        return mMobilNumber;
    }

    public void setMobilNumber(String mobilNumber) {
        mMobilNumber = mobilNumber;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

}
