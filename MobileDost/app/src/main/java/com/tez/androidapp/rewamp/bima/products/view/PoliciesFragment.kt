package com.tez.androidapp.rewamp.bima.products.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.tez.androidapp.R
import com.tez.androidapp.app.base.ui.fragments.BaseFragment
import com.tez.androidapp.app.vertical.bima.policies.models.network.Policy
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Failure
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Loading
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Success
import com.tez.androidapp.rewamp.bima.products.adapter.PoliciesAdapter
import com.tez.androidapp.rewamp.bima.products.router.ProductPolicyActivityRouter
import com.tez.androidapp.rewamp.bima.products.viewmodel.PoliciesViewModel
import kotlinx.android.synthetic.main.policies_fragment.*

class PoliciesFragment : BaseFragment(), PoliciesAdapter.PolicyListener {

    private var baseView: View? = null
    private lateinit var callback: BimaCallback
    private val model: PoliciesViewModel by activityViewModels()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = context as BimaCallback
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        baseView = baseView ?: inflater.inflate(R.layout.policies_fragment, container, false)
        return baseView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callback.setIllustrationVisibility(View.GONE)
        model.networkCallMutableLiveData.observe(viewLifecycleOwner, {
            when (it) {
                is Loading -> {
                    shimmer.visibility = View.VISIBLE
                    clContent.visibility = View.GONE
                }
                is Failure -> {
                    shimmer.visibility = View.GONE
                    clContent.visibility = View.GONE
                    showError(it.errorCode)
                }
                is Success -> {
                    shimmer.visibility = View.GONE
                    clContent.visibility = View.VISIBLE
                }
            }
        })

        model.getPolicyListLiveData().observe(viewLifecycleOwner, {
            if (rvPolicies.adapter == null) {
                rvPolicies.adapter = PoliciesAdapter(it, this)
                layoutEmptyPolicy.visibility = if (it.isEmpty()) View.VISIBLE else View.GONE
                clContent.visibility = if (it.isEmpty()) View.GONE else View.VISIBLE
            }
            callback.setIllustrationAndVisibility(R.drawable.ic_illustration_empty_policy,
                    if (it.isEmpty()) View.VISIBLE else View.GONE)
        })

        btPurchasePolicy.setDoubleTapSafeOnClickListener { callback.showProductsFragment() }
    }

    override fun onClickPolicy(policy: Policy) = getBaseActivity {
        ProductPolicyActivityRouter.createInstance()
                .setDependenciesAndRoute(it,
                        policy.mobileUserInsurancePolicyId,
                        policy.id,
                        policy.policyName)
    }

    companion object {
        fun newInstance(): PoliciesFragment = PoliciesFragment()
    }
}