package com.tez.androidapp.app.vertical.bima.claims.models;

/**
 * Created by VINOD KUMAR on 11/13/2018.
 */
public interface CommonAnswers {

    void setLongitude(Double l);

    void setLatitude(Double l);

    void setResponse(String s);

    void setAdditionalInfo(String s);

    Double getLongitude();

    Double getLatitude();

    String getResponse();

    String getAdditionalInfo();
}
