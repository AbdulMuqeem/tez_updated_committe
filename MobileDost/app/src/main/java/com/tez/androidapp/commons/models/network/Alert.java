package com.tez.androidapp.commons.models.network;

import java.io.Serializable;

/**
 * Created by VINOD KUMAR on 7/2/2019.
 */
public class Alert implements Serializable {

    private String title;
    private String message;

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }
}
