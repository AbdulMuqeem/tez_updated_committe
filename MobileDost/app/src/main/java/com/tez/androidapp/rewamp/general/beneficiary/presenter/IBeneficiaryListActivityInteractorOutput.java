package com.tez.androidapp.rewamp.general.beneficiary.presenter;

import com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks.SetBeneficiaryCallback;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks.SetDefaultBeneficiaryCallback;
import com.tez.androidapp.rewamp.general.beneficiary.listener.GetBeneficiariesListener;

public interface IBeneficiaryListActivityInteractorOutput extends GetBeneficiariesListener, SetBeneficiaryCallback, SetDefaultBeneficiaryCallback {
}
