package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.view.JoinCommitteeInvitationActivity;

import androidx.annotation.NonNull;

public class JoinCommitteeInvitationActivityRouter extends BaseActivityRouter {


    public static JoinCommitteeInvitationActivityRouter createInstance() {
        return new JoinCommitteeInvitationActivityRouter();
    }


    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, JoinCommitteeInvitationActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

}
