package com.tez.androidapp.commons.validators.filters;

import androidx.annotation.NonNull;

import com.tez.androidapp.commons.validators.annotations.PinViewRegex;
import com.tez.androidapp.commons.widgets.TezPinEditText;

import net.tez.validator.library.filters.Filter;
import net.tez.validator.library.utils.TextUtil;

/**
 * Created by FARHAN DHANANI on 6/3/2019.
 */
public class PinViewFilter implements Filter<TezPinEditText, PinViewRegex> {
    @Override
    public boolean isValidated(@NonNull TezPinEditText view, @NonNull PinViewRegex annotation) {
        return TextUtil.isValidWithRegex(view.getPin(), annotation.regex());
    }
}
