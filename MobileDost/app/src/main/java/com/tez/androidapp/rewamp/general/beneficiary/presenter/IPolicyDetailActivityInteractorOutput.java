package com.tez.androidapp.rewamp.general.beneficiary.presenter;

import com.tez.androidapp.app.vertical.bima.policies.callbacks.InsurancePolicyDetailsCallback;

public interface IPolicyDetailActivityInteractorOutput extends InsurancePolicyDetailsCallback {
}
