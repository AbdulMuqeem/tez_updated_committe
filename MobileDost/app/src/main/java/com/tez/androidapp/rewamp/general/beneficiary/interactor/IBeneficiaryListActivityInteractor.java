package com.tez.androidapp.rewamp.general.beneficiary.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.models.network.dto.request.BeneficiaryRequest;

public interface IBeneficiaryListActivityInteractor extends IBaseInteractor {

    void getBeneficiaries();

    void setAdvanceBimaBeneficiary(@NonNull BeneficiaryRequest request);

    void setDefaultBeneficiary(int beneficiaryId);
}
