package com.tez.androidapp.rewamp.advance.request.entity;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import static net.tez.fragment.util.optional.Optional.doWhen;
import static net.tez.fragment.util.optional.Optional.ifPresent;

public class LoanDetail implements Serializable {

    private final Map<Integer, TreeSet<PricingDetail>> mapPricingDetails = new HashMap<>();
    private final Map<Integer, TreeSet<ProcessingFee>> mapProcessingFees = new HashMap<>();
    private final double latePaymentCharges;
    private List<TenureDetail> tenureDetails;

    public LoanDetail(List<TenureDetail> tenureDetails,
                      List<PricingDetail> pricingDetails,
                      List<ProcessingFee> processingFees,
                      double latePaymentCharges) {
        this.tenureDetails = tenureDetails;
        this.latePaymentCharges = latePaymentCharges;
        this.initiallizeProcessingFees(processingFees);
        this.initiallizePricingDetails(pricingDetails);
    }

    public List<TenureDetail> getTenureDetails() {
        return tenureDetails;
    }

    public void setTenureDetails(List<TenureDetail> tenureDetails) {
        this.tenureDetails = tenureDetails;
    }

    private void initiallizeProcessingFees(List<ProcessingFee> processingFees) {
        for (ProcessingFee processingFeeItem : processingFees) {
            doWhen(mapProcessingFees.containsKey(processingFeeItem.getWalletProviderId()),
                    () -> ifPresent(mapProcessingFees.get(processingFeeItem.getWalletProviderId()),
                            processingFeesListForWallet -> {
                                processingFeesListForWallet.add(processingFeeItem);
                            }),
                    () -> mapProcessingFees.put(processingFeeItem.getWalletProviderId(),
                            new TreeSet<ProcessingFee>((o1, o2) -> o1.getMaxDisbursementAmount().compareTo(o2.getMaxDisbursementAmount())) {
                                {
                                    add(processingFeeItem);
                                }
                            }));
        }
    }

    private void initiallizePricingDetails(List<PricingDetail> pricingDetails) {
        for (PricingDetail pricingDetail : pricingDetails) {
            doWhen(mapPricingDetails.containsKey(pricingDetail.getWalletProviderId()),
                    () -> ifPresent(mapPricingDetails.get(pricingDetail.getWalletProviderId()),
                            pricingDetailsList -> {
                                pricingDetailsList.add(pricingDetail);
                            }),
                    () -> mapPricingDetails.put(pricingDetail.getWalletProviderId(),
                            new TreeSet<PricingDetail>((o1, o2) -> o1.getTenure().compareTo(o2.getTenure())) {
                                {
                                    add(pricingDetail);
                                }
                            }));
        }
    }

    public double getProcessingFees(int walletProviderId, double loanAmount) {
        double[] processingFees = new double[]{-1};
        doWhen(mapProcessingFees.containsKey(walletProviderId),
                () -> ifPresent(mapProcessingFees.get(walletProviderId),
                        processingFeesListForWallet -> {
                            for (ProcessingFee processingFee : processingFeesListForWallet) {
                                if (processingFee.getMaxDisbursementAmount() >= loanAmount) {
                                    processingFees[0] = processingFee.getProcessingFee();
                                    break;
                                }
                            }
                        }));
        return processingFees[0];
    }

    public double getPricingAmount(int walletProviderId, int tenure) {
        double[] pricingAmount = new double[]{-1};
        doWhen(mapPricingDetails.containsKey(walletProviderId),
                () -> ifPresent(mapPricingDetails.get(walletProviderId),
                        pricingDetailTreeSet -> {
                            for (PricingDetail pricingDetail : pricingDetailTreeSet) {
                                if (pricingDetail.getTenure() >= tenure) {
                                    pricingAmount[0] = pricingDetail.getPricing();
                                    break;
                                }
                            }
                        }));
        return pricingAmount[0];
    }

    public TenureDetail getTenureLimit(double loanAmount) {
        for (TenureDetail tenureDetail : tenureDetails) {
            if (loanAmount >= tenureDetail.getMinDisbursementAmount() && loanAmount <= tenureDetail.getMaxDisbursementAmount()) {
                return tenureDetail;
            }
        }
        return null;
    }

    public double getLatePaymentCharges() {
        return this.latePaymentCharges;
    }
}
