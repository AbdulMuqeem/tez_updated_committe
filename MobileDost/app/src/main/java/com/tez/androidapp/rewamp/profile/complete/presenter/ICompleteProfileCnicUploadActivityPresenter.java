package com.tez.androidapp.rewamp.profile.complete.presenter;

import androidx.annotation.Nullable;

import com.tez.androidapp.commons.utils.cnic.detection.model.RetumDataModel;

public interface ICompleteProfileCnicUploadActivityPresenter {
    void setUserSelfieIsUploaded();

    void setUserFrontNicIsUploaded();

    void setUserBackNicIsUploaded();

    void startCamera(int requestCode, String detectObject, boolean isSelfie);

    void setUserCnic(String cnic);

    void setUserCNICDateOfExpiry(String dateOfExpiry);

    void setUserCNICDateOfBirth(String dateOfBirth);

    void setUserCNICDateOfIssue(String userCNICDateOfIssue);

    void setOcrHasBeenRun(boolean ocrHasBeenRun);

    void updateCnicImageCardViews(int requestCode, @Nullable String filePath,
                                  boolean isAutoDetected,
                                  @Nullable RetumDataModel retumDataModel);
}
