package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request

/**
 * Created by Vinod Kumar on 10/19/2020.
 */
object ClaimDetailsRequest {
    const val METHOD_NAME = "v1/claim/{" + Params.CLAIM_ID + "}"

    object Params {
        const val CLAIM_ID = "claimId"
    }
}