package com.tez.androidapp.rewamp.bima.claim.response.handler

import com.jakewharton.retrofit2.adapter.rxjava2.Result
import com.tez.androidapp.app.base.handlers.BaseRH
import com.tez.androidapp.app.base.response.BaseResponse
import com.tez.androidapp.repository.network.store.BaseCloudDataStore
import com.tez.androidapp.rewamp.bima.claim.callback.InitiateClaimCallback

class InitiateClaimRH(baseCloudDataStore: BaseCloudDataStore,
                      private val initiateClaimCallback: InitiateClaimCallback)
    : BaseRH<BaseResponse>(baseCloudDataStore) {

    override fun onSuccess(value: Result<BaseResponse>) {
        val response = value.response().body()
        response?.let {
            if (isErrorFree(response))
                initiateClaimCallback.onSuccessInitiateClaim(it)
            else
                onFailure(it.statusCode, it.errorDescription)
        } ?: sendDefaultFailure()
    }

    override fun onFailure(errorCode: Int, message: String?) {
        initiateClaimCallback.onFailureInitiateClaim(errorCode, message)
    }
}