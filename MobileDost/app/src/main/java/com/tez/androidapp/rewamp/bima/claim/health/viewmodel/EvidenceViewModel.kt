package com.tez.androidapp.rewamp.bima.claim.health.viewmodel

import androidx.lifecycle.MutableLiveData
import com.tez.androidapp.R
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.Evidence
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.EvidenceFile
import com.tez.androidapp.rewamp.bima.claim.common.network.viewmodel.NetworkBoundViewModel

open class EvidenceViewModel : NetworkBoundViewModel() {
    val evidenceListLiveData: MutableLiveData<List<Evidence>> = MutableLiveData(listOf(
            Evidence(id = 296, name = R.string.medical_bills),
            Evidence(id = 297, name = R.string.discharge_slip),
            Evidence(id = 298, name = R.string.treatment_records),
            Evidence(id = 299, name = R.string.doctor_notes),
    ))

    fun updateEvidenceFileList(docId: Int, evidenceFileList: MutableList<EvidenceFile>) {
        val evidenceList = evidenceListLiveData.value
        evidenceList?.forEach {
            if (it.id == docId) {
                it.evidenceFileList = evidenceFileList
                evidenceListLiveData.value = evidenceList
            }
        }
    }

    fun getEvidenceFileList(docId: Int): List<EvidenceFile> =
            evidenceListLiveData.value?.find { it.id == docId }?.evidenceFileList ?: emptyList()
}