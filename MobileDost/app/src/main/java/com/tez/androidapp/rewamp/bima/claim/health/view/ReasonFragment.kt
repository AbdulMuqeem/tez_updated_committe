package com.tez.androidapp.rewamp.bima.claim.health.view

import android.os.Bundle
import android.text.Editable
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.tez.androidapp.R
import com.tez.androidapp.commons.utils.textwatchers.TezTextWatcher
import com.tez.androidapp.rewamp.bima.claim.health.base.BaseClaimStepFragment
import com.tez.androidapp.rewamp.bima.claim.health.viewmodel.ReasonViewModel
import com.tez.androidapp.rewamp.general.purpose.Purpose
import com.tez.androidapp.rewamp.general.purpose.PurposeAdapter
import com.tez.androidapp.rewamp.general.purpose.PurposeAdapter.PurposeListener
import kotlinx.android.synthetic.main.fragment_reason.*
import kotlinx.android.synthetic.main.fragment_reason.view.*

/**
 * Created by Vinod Kumar on 4/17/2020
 */
class ReasonFragment : BaseClaimStepFragment(), PurposeListener {

    private val viewModel: ReasonViewModel by activityViewModels()
    private val args: ReasonFragmentArgs by navArgs()


    override fun initView(baseView: View, savedInstanceState: Bundle?) {
        viewModel.purposeListLiveData.observe(viewLifecycleOwner, {
            if (rvClaimPurposes.adapter == null) {
                val purposeAdapter = PurposeAdapter(it, viewModel.selectedReasonPosition, this)
                rvClaimPurposes.layoutManager = GridLayoutManager(context, 3)
                rvClaimPurposes.adapter = purposeAdapter
            }
        })

        baseView.etOther.addTextChangedListener(object : TezTextWatcher {
            override fun afterTextChanged(s: Editable) {
                super.afterTextChanged(s)
                claimSharedViewModel.reasonLiveData.value?.other = s.toString()
            }
        })
    }

    @get:LayoutRes
    override val layoutResId: Int
        get() = R.layout.fragment_reason

    override val stepNumber: Int
        get() = args.stepNumber

    override fun onClickPurpose(position: Int, purpose: Purpose) {
        setNavigationOnBtContinue(true)
        val isOther = purpose.id == ReasonViewModel.OTHER_REASON
        if (isOther) {
            tilOther.visibility = View.VISIBLE
            claimSharedViewModel.reasonLiveData.value?.other = etOther.valueText
            etOther.requestFocus()
        } else {
            tilOther.visibility = View.GONE
            claimSharedViewModel.reasonLiveData.value?.other = null
            btContinue.requestFocus()
        }

        claimSharedViewModel.reasonLiveData.value = purpose
        viewModel.selectedReasonPosition = position
    }
}