package com.tez.androidapp.commons.utils.app;

public final class PushNotificationConstants {

    /*
        Keys from Backend
     */
    public static final String KEY_VERTICAL = "vertical";
    public static final String KEY_TYPE = "type";
    public static final String KEY_TITLE = "title";
    public static final String KEY_MESSAGE_TEXT = "messageText";
    public static final String KEY_CARDS = "cards";

    /*
        Values from Backend
     */
    public static final String INSURANCE = "INSURANCE";
    public static final String LOAN_LIMIT_STATUS = "LOAN_LIMIT_STATUS";
    public static final String LOAN_DISBURSEMENT_STATUS = "LOAN_DISBURSEMENT_STATUS";
    public static final String SESSION_EXPIRED = "SESSION_EXPIRED";
    public static final String LOAN_REPAYMENT_STATUS = "LOAN_REPAYMENT_STATUS";
    public static final String CNIC_VERIFICATION_STATUS = "CNIC_VERIFICATION_STATUS";
    public static final String APPROVED = "APPROVED";
    public static final String INSURANCE_PURCHASE = "INSURANCE_PURCHASE";
    public static final String CLAIM_LODGE = "CLAIM_LODGE";
    public static final String CLAIM_REVISE_PORTAL = "CLAIM_REVISE_PORTAL";


    /*
        Front-end broadcast types to send broadcasts
     */
    public static final String BROADCAST_TYPE_LOAN_LIMIT = "TYPE_LOAN_LIMIT";
    public static final String BROADCAST_TYPE_LOAN_DISBURSEMENT = "TYPE_LOAN_DISBURSEMENT";
    public static final String BROADCAST_TYPE_LOAN_REPAYMENT = "TYPE_LOAN_REPAYMENT";
    public static final String BROADCAST_TYPE_CNIC_VERIFICATION = "TYPE_CNIC_VERIFICATION";

    /*
        Front-end Keys
     */

    public static final String KEY_ROUTE_TO = "KEY_ROUTE_TO";

    /*
        Front-end Values
     */

    public static final String LIMIT_ASSIGNED_ACTIVITY = "LIMIT_ASSIGNED_ACTIVITY";
    public static final String LOAN_RECEIPT_ACTIVITY = "LOAN_RECEIPT_ACTIVITY";
    public static final String PROFILE_TRUST_ACTIVITY = "PROFILE_TRUST_ACTIVITY";
    public static final String BIMA_ACTIVITY_MY_POLICY_FRAGMENT = "BIMA_ACTIVITY_MY_POLICY_FRAGMENT";
    public static final String BIMA_ACTIVITY_MY_CLAIMS_FRAGMENT = "BIMA_ACTIVITY_MY_CLAIMS_FRAGMENT";

    private PushNotificationConstants() {

    }
}
