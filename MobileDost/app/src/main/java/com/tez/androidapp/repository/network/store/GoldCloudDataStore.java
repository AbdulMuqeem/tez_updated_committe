package com.tez.androidapp.repository.network.store;

import androidx.annotation.NonNull;

import com.tez.androidapp.rewamp.gold.api.client.TezGoldApi;
import com.tez.androidapp.rewamp.gold.onboarding.callback.GetGoldRateCallback;
import com.tez.androidapp.rewamp.gold.onboarding.response.handler.GetGoldRateRH;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class GoldCloudDataStore extends BaseAuthCloudDataStore {

    private static GoldCloudDataStore instance;
    private final TezGoldApi tezGoldApi;

    public GoldCloudDataStore() {
        tezGoldApi = tezAPIBuilder.build().create(TezGoldApi.class);
    }


    public static GoldCloudDataStore getInstance() {
        if (instance == null)
            instance = new GoldCloudDataStore();
        return instance;
    }

    public void getGoldRate(@NonNull GetGoldRateCallback callback) {
        tezGoldApi.getGoldRate()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new GetGoldRateRH(this, callback));
    }
}
