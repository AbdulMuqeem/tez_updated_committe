package com.tez.androidapp.rewamp.committee.interactor;

import android.widget.Toast;

import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.CommitteeAuthCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.JoinCommitteeVerificationLIstener;
import com.tez.androidapp.rewamp.committee.presenter.IJoinVerificationCommitteeActivityInteractorOutput;
import com.tez.androidapp.rewamp.committee.request.JoinCommitteeVerificationRequest;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeVerificationResponse;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class JoinCommitteeVerificationActivityInteractor implements IJoinCommitteeVerificationActivityInteractor {

    private final IJoinVerificationCommitteeActivityInteractorOutput mJoinVerificationCommitteeActivityInteractorOutput;

    public JoinCommitteeVerificationActivityInteractor(IJoinVerificationCommitteeActivityInteractorOutput mJoinVerificationCommitteeActivityInteractorOutput) {
        this.mJoinVerificationCommitteeActivityInteractorOutput = mJoinVerificationCommitteeActivityInteractorOutput;
    }


    @Override
    public void verifyOtp(JoinCommitteeVerificationRequest joinCommitteeVerificationRequest) {
        CommitteeAuthCloudDataStore.getInstance().verifyOtp(joinCommitteeVerificationRequest, new JoinCommitteeVerificationLIstener() {
            @Override
            public void onJoinCommitteeVerificationSuccess(JoinCommitteeVerificationResponse joinCommitteeVerificationResponse) {
                if (!(joinCommitteeVerificationResponse.isResponse())) {
                    Toast.makeText(MobileDostApplication.getAppContext(), "Invalid Otp.", Toast.LENGTH_LONG).show();
                    mJoinVerificationCommitteeActivityInteractorOutput.onJoinCommitteeVerificationSuccess(null);
                } else
                    mJoinVerificationCommitteeActivityInteractorOutput.onJoinCommitteeVerificationSuccess(joinCommitteeVerificationResponse);
            }

            @Override
            public void onJoinCommitteeVerificationFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    verifyOtp(joinCommitteeVerificationRequest);
                else
                    mJoinVerificationCommitteeActivityInteractorOutput.onJoinCommitteeVerificationFailure(errorCode, message);
            }
        });
    }
}
