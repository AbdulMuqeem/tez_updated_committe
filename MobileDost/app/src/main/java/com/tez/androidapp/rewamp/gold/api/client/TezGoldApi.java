package com.tez.androidapp.rewamp.gold.api.client;

import com.tez.androidapp.rewamp.gold.onboarding.request.GetGoldRateRequest;


import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.rewamp.gold.onboarding.response.GetGoldRateResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface TezGoldApi {

    @GET(GetGoldRateRequest.METHOD_NAME)
    Observable<Result<GetGoldRateResponse>> getGoldRate();
}
