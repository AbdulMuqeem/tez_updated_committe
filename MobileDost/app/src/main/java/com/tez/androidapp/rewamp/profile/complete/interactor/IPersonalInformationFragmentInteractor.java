package com.tez.androidapp.rewamp.profile.complete.interactor;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.AnswerSelected;

import java.util.List;

public interface IPersonalInformationFragmentInteractor {
    void getPersonalQuestions(String lang);

    void updatePersonalInfo(String file, String questionRef, List<AnswerSelected> answerSelecteds);
}
