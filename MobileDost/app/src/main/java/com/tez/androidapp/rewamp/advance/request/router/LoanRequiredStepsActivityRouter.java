package com.tez.androidapp.rewamp.advance.request.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.request.view.LoanRequiredStepsActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

import java.util.ArrayList;
import java.util.List;

public class LoanRequiredStepsActivityRouter extends BaseActivityRouter {

    public static final int REQUEST_CODE_LOAN_REQUIRED_STEPS_ACTIVITY = 10992;
    public static final String STEPS_LEFT_LIST = "STEPS_LEFT_LIST";
    public static final String IS_PROFILE_COMPLETED = "IS_PROFILE_COMPLETED";

    public static LoanRequiredStepsActivityRouter createInstance() {
        return new LoanRequiredStepsActivityRouter();
    }

    public void setDependenciesAndRouteForResult(@NonNull BaseActivity from) {
        routeForResult(from, createIntent(from), REQUEST_CODE_LOAN_REQUIRED_STEPS_ACTIVITY);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, @NonNull List<String> stepsLeft) {
        Intent intent = createIntent(from);
        intent.putStringArrayListExtra(STEPS_LEFT_LIST, new ArrayList<>(stepsLeft));
        route(from, intent);
    }

    public void routeToExistingInstanceOfActivity(@NonNull BaseActivity from, boolean isProfileCompleted) {
        Intent intent = createIntent(from);
        intent.putExtra(IS_PROFILE_COMPLETED, isProfileCompleted);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        route(from, intent);
    }

    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, LoanRequiredStepsActivity.class);
    }
}
