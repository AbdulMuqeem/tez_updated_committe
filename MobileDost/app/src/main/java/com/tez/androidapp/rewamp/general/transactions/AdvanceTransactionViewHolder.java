package com.tez.androidapp.rewamp.general.transactions;

import android.view.View;

import androidx.annotation.ColorRes;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.transactions.models.network.Transaction;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.RippleFrameLayout;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.viewbinder.library.core.BindView;

public class AdvanceTransactionViewHolder extends BaseTransactionViewHolder {

    @BindView(R.id.tvPaymentStatus)
    private TezTextView tvPaymentStatus;

    @BindView(R.id.tvAdvanceAmount)
    private TezTextView tvAdvanceAmount;

    @BindView(R.id.tvDate)
    private TezTextView tvDate;

    @BindView(R.id.flAction)
    private RippleFrameLayout flAction;

    @BindView(R.id.tvAction)
    private TezTextView tvAction;

    AdvanceTransactionViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onBind(Transaction item, @Nullable MyTransactionsAdapter.MyTransactionListener listener) {
        super.onBind(item, listener);
        tvPaymentStatus.setText(item.getPaymentStatus());
        tvAdvanceAmount.setText(itemView.getContext().getString(R.string.rs_value_string, item.getStringAmount()));
        tvDate.setText(item.getDueDate());
        flAction.setDoubleTapSafeOnClickListener(view -> {
            if (listener != null)
                listener.onClickMyTransaction(item);
        });
        tvAction.setText(item.isCleared() ? R.string.view : R.string.repay);
        this.setTvPaymentStatusTextColor(item.isCleared() ? R.color.textViewTextColorGreen : R.color.stateRejectedColorRed);
    }

    private void setTvPaymentStatusTextColor(@ColorRes int color) {
        tvPaymentStatus.setTextColor(Utility.getColorFromResource(color));
    }
}
