package com.tez.androidapp.app.vertical.advance.loan.repayment.shared.callbacks;

import com.tez.androidapp.commons.models.network.LoanDetails;

/**
 * Created  on 6/16/2017.
 */

public interface RepayLoanCallback {

    void onRepayLoanSuccess(LoanDetails loanDetails);

    void onRepayLoanFailure(int errorCode, String message);
}
