package com.tez.androidapp.rewamp.general.wallet.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.ToolbarActivity;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.validators.annotations.TextInputLayoutRegex;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.general.wallet.router.AddWalletTermsAndConditionActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.router.UpdateWalletActivityRouter;
import com.tez.androidapp.rewamp.util.DialogUtil;

import net.tez.fragment.util.optional.TextUtil;
import net.tez.validator.library.annotations.Order;
import net.tez.validator.library.core.FieldValidator;
import net.tez.validator.library.core.FilterChain;
import net.tez.validator.library.core.ValidationError;
import net.tez.validator.library.core.ValidationListener;
import net.tez.viewbinder.library.core.BindView;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class UpdateWalletActivity extends ToolbarActivity implements ValidationListener {

    @BindView(R.id.etMobileWallet)
    private TezEditTextView etMobileWallet;

    @TextInputLayoutRegex(regex = Constants.PHONE_NUMBER_VALIDATOR_REGEX, value = {1, R.string.incorrect_mobile_number})
    @Order(1)
    @BindView(R.id.tilMobileWalletNumber)
    private TezTextInputLayout tilMobileWalletNumber;

    @BindView(R.id.etMobileWalletNumber)
    private TezEditTextView etMobileWalletNumber;

    @BindView(R.id.btUpdateWallet)
    private TezButton btUpdatedWallet;

    @BindView(R.id.tvDeleteWallet)
    private TezTextView tvDeleteWallet;

    private CompositeDisposable allDisposables;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_wallet);
        tezToolbar.setToolbarTitle(R.string.editing_wallet);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.allDisposables = new CompositeDisposable();
        listenChangesOnViews();
    }

    @Override
    protected void onPause() {
        super.onPause();
        allDisposables.dispose();
    }

    @Override
    public void validateSuccess() {
        this.btUpdatedWallet.setButtonNormal();
    }

    @Override
    public void validatePassed(@NonNull View v) {
        Utility.setErrorOnTezViews(v, null);
    }

    @Override
    public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
        this.btUpdatedWallet.setButtonInactive();
        filterChain.doFilter();
    }

    private void init() {
        int serviceProviderId = getIntent().getIntExtra(UpdateWalletActivityRouter.SERVICE_PROVIDER_ID, -100);
        int mobileUserAccountId = getIntent().getIntExtra(UpdateWalletActivityRouter.MOBILE_ACCOUNT_ID, -100);
        String mobileAccountNumber = getIntent().getStringExtra(UpdateWalletActivityRouter.MOBILE_ACCOUNT_NUMBER);
        etMobileWallet.setText(Utility.getWalletName(serviceProviderId));
        if (mobileAccountNumber != null)
            etMobileWalletNumber.setText(Utility.getFormattedMobileNumber(mobileAccountNumber));
        this.btUpdatedWallet.setDoubleTapSafeOnClickListener(v -> FieldValidator.validate(this, new ValidationListener() {
            @Override
            public void validateSuccess() {
                if (mobileAccountNumber != null
                        && TextUtil.equals(Utility.getFormattedMobileNumber(mobileAccountNumber), etMobileWalletNumber.getValueText()))
                    showInformativeMessage(R.string.your_new_number_must_not_be_same);
                else {
                    boolean isChecked = getIntent().getBooleanExtra(UpdateWalletActivityRouter.IS_DEFAULT, false);
                    AddWalletTermsAndConditionActivityRouter
                            .createInstance()
                            .setDependenciesAndRouteForResult(UpdateWalletActivity.this,
                                    serviceProviderId,
                                    etMobileWalletNumber.getValueText(),
                                    mobileUserAccountId,
                                    isChecked);
                }
            }

            @Override
            public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
                Utility.setErrorOnTezViews(validationError.getView(), validationError.getMessage());
                btUpdatedWallet.setButtonInactive();
                filterChain.doFilter();
            }
        }));
        tvDeleteWallet.setDoubleTapSafeOnClickListener(v ->
                takeConfirmationToDeleteWallet(serviceProviderId, mobileUserAccountId));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AddWalletTermsAndConditionActivityRouter.REQUEST_CODE_ADD_WALLET && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    private void takeConfirmationToDeleteWallet(final int serviceProviderId, final int mobileAccountId) {
        String confirmation = getString(R.string.are_you_sure_to_delete_wallet, Utility.getWalletName(serviceProviderId));
        DialogUtil.showActionableDialog(this,
                R.string.remove_wallet,
                confirmation,
                R.string.string_yes,
                R.string.string_no,
                (dialog, which) -> {
                    if (which == DialogInterface.BUTTON_POSITIVE) {
                        Intent data = new Intent();
                        data.putExtra(UpdateWalletActivityRouter.MOBILE_ACCOUNT_ID, mobileAccountId);
                        setResult(UpdateWalletActivityRouter.RESULT_DELETE, data);
                        finish();
                    }

                });
    }

    private void listenChangesOnViews() {
        allDisposables.add(RxTextView.textChanges(this.etMobileWalletNumber).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));
    }

    @Override
    protected String getScreenName() {
        return "UpdateWalletActivity";
    }
}