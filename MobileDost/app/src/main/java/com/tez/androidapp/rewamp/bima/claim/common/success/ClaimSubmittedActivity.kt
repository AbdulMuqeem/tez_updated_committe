package com.tez.androidapp.rewamp.bima.claim.common.success

import android.os.Bundle
import com.tez.androidapp.R
import com.tez.androidapp.app.base.ui.activities.BaseActivity
import com.tez.androidapp.commons.utils.app.Constants
import com.tez.androidapp.commons.utils.app.Utility
import com.tez.androidapp.rewamp.bima.claim.common.success.router.ClaimSubmittedActivityRouter
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter
import kotlinx.android.synthetic.main.activity_claim_submitted.*

class ClaimSubmittedActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_claim_submitted)
        init(ClaimSubmittedActivityRouter.getDependencies(intent))
    }

    private fun init(dependencies: ClaimSubmittedActivityRouter.Dependencies) {
        val dto = dependencies.insuranceClaimConfirmationDto
        if (dependencies.revise) {
            tvHeading.setText(R.string.claim_has_been_resubmitted)
            tvMessage1.setText(R.string.you_will_be_contacted)
            tvClaimDate.setText(R.string.revise_date)
        }
        ivIcon.setImageResource(Utility.getInsuranceProductIcon(dependencies.productId))
        ivProviderLogo.setImageResource(Utility.getInsuranceProviderLogo(dto.providerCode))
        tvTitle.text = dto.policyName
        tvClaimAmountValue.text = getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(dto.claimAmount))
        tvClaimDateValue.text = Utility.getFormattedDate(dto.claimDate, Constants.BACKEND_DATE_FORMAT, Constants.UI_DATE_FORMAT)
        tvClaimNumberValue.text = dto.claimNumber
        tvPolicyTag.text = dto.policyTag
        btOkay.setDoubleTapSafeOnClickListener {
            DashboardActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this)
        }
    }

    override fun onBackPressed() {

    }

    override fun getScreenName(): String = "ClaimSubmittedActivity"
}