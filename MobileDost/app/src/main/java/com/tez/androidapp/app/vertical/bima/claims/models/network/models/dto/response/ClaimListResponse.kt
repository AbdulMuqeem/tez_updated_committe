package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response

import com.tez.androidapp.app.base.response.BaseResponse
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.Claim

/**
 * * Created by Vinod Kumar on 10/14/2020.
 */
class ClaimListResponse(val claims: List<Claim>) : BaseResponse()