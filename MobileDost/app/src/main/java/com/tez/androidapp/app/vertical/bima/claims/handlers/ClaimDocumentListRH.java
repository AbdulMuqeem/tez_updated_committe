package com.tez.androidapp.app.vertical.bima.claims.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.vertical.bima.claims.callback.ClaimDocumentListCallback;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.ClaimDocumentListResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by FARHAN DHANANI on 6/14/2018.
 */
public class ClaimDocumentListRH extends BaseRH<ClaimDocumentListResponse> {

    private ClaimDocumentListCallback claimDocumentListCallback;

    public ClaimDocumentListRH(BaseCloudDataStore baseCloudDataStore, @Nullable ClaimDocumentListCallback
            claimDocumentListCallback) {
        super(baseCloudDataStore);
        this.claimDocumentListCallback = claimDocumentListCallback;
    }

    @Override
    protected void onSuccess(Result<ClaimDocumentListResponse> value) {
        ClaimDocumentListResponse response = value.response().body();
        if (response != null) {
            if (isErrorFree(response))
                claimDocumentListCallback.onClaimDocumentListSuccess(response.getDocumentList());
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());
        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        claimDocumentListCallback.onClaimDocumentListFailure(errorCode, message);
    }
}
