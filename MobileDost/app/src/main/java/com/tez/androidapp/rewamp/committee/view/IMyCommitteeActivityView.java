package com.tez.androidapp.rewamp.committee.view;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface IMyCommitteeActivityView extends IBaseView {
    void showLoader();

    void hideLoader();

    void onMyCommittee(MyCommitteeResponse myCommitteeResponse);
}
