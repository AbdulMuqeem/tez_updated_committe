package com.tez.androidapp.commons.utils.app;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.tez.androidapp.R;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.commons.models.app.Permission;
import net.tez.fragment.util.optional.Optional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Utility class to manage permission request
 * <p>
 * Created  on 12/29/2016.
 */

public class PermissionCheckUtil {

    private static PermissionCheckUtil permissionCheckUtil;
    private List<String> permissionFullNames;
    private List<String> permissionDisplayNames;
    private List<Permission> permissionsList = new ArrayList<>();
    private Context context;

    /**
     * Populate the PermissionCheckUtil with all the required permissions for Tez
     */
    private PermissionCheckUtil(Context context) {
        this.context = context;
        Resources resources = context.getResources();
        permissionFullNames = new ArrayList<>();
        permissionFullNames.add(Manifest.permission.WRITE_CONTACTS);
        permissionFullNames.add(Manifest.permission.WRITE_CALENDAR);
        permissionFullNames.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissionFullNames.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionFullNames.add(Manifest.permission.READ_SMS);
        permissionDisplayNames = Arrays.asList(resources.getStringArray(R.array.permission_display_names));
    }

    /**
     * Get PermissionCheckUtil singleton instance
     */
    public static PermissionCheckUtil getInstance(Context context) {
        if (permissionCheckUtil == null) permissionCheckUtil = new PermissionCheckUtil(context);
        return permissionCheckUtil;
    }


    public static PermissionCheckUtil getNewInstance(Context context) {
        destroyInstance();
        if (permissionCheckUtil == null) permissionCheckUtil = new PermissionCheckUtil(context);
        return permissionCheckUtil;
    }

    /**
     * Destroy PermissionCheckUtil singleton instance
     */
    private static void destroyInstance() {
        permissionCheckUtil = null;
    }

    /**
     * Get list of all required permissions for Tez
     *
     * @return list of all required permissions
     */
    public List<String> getPermissionNames() {
        return permissionFullNames;
    }

    /**
     * Get the list of permissions {@link Permission}, the list also contains info of whether the permission is allowed or not.
     */
    private void fillPermissionsList() {
        permissionsList.clear();
        for (int i = 0; i < permissionFullNames.size(); i++) {
            permissionsList.add(new Permission(permissionFullNames.get(i), permissionDisplayNames.get(i),
                    ActivityCompat.checkSelfPermission(context, permissionFullNames.get(i)) == PackageManager
                            .PERMISSION_GRANTED));
        }
    }


    /**
     * Check if all the required permissions for Tez are granted or not
     *
     * @return true if all are allowed, false otherwise.
     */
    public boolean areAllPermissionsAllowed() {
        if (permissionsList.isEmpty())
            fillPermissionsList();

        for (Permission permission : permissionsList)
            if (!permission.isGranted())
                return false;
        return true;
    }

    public List<String> getMissingPermissionsForSignUp(String[] requiredPermissions) {
        List<String> missingPermissions = new ArrayList<>();
        for (String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(context, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        return missingPermissions;
    }

    public static List<String> getSignupMissingPermissions(Context context) {
        List<String> missingPermissions = new ArrayList<>();
        for (String permission : MobileDostApplication.getSignupPermissions()) {
            if (ContextCompat.checkSelfPermission(context, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        return missingPermissions;
    }

    @NonNull
    public static List<String> getOptionalMissingPermissions(Context context){
        List<String> missingPermissions = new ArrayList<>();
        for(String permission: MobileDostApplication.OptionalPermissions()){
            if(ContextCompat.checkSelfPermission(context, permission)!= PackageManager.PERMISSION_GRANTED){
                missingPermissions.add(permission);
            }
        }
        return missingPermissions;
    }

    public static List<String> getMobileVerificationMissingPermission(Context context) {
        List<String> missingPermissions = new ArrayList<>();
        for (String permission : MobileDostApplication.getInstance().getNumberVerificationPermissions()) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        return missingPermissions;
    }
}
