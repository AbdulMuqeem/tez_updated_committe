package com.tez.androidapp.rewamp.advance.request.presenter;

import android.location.Location;

import androidx.annotation.NonNull;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.rewamp.advance.request.interactor.AdvanceTermsAndConditionActivityInteractor;
import com.tez.androidapp.rewamp.advance.request.interactor.IAdvanceTermsAndConditionActivityInteractor;
import com.tez.androidapp.rewamp.advance.request.request.LoanApplyRequest;
import com.tez.androidapp.rewamp.advance.request.view.IAdvanceTermsAndConditionActivityView;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import java.util.List;

public class AdvanceTermsAndConditionActivityPresenter implements IAdvanceTermsAndConditionActivityPresenter, IAdvanceTermsAndConditionActivityInteractorOutput, LocationAvailableCallback {

    private final IAdvanceTermsAndConditionActivityView iAdvanceTermsAndConditionActivityView;
    private final IAdvanceTermsAndConditionActivityInteractor iAdvanceTermsAndConditionActivityInteractor;

    public AdvanceTermsAndConditionActivityPresenter(IAdvanceTermsAndConditionActivityView iAdvanceTermsAndConditionActivityView) {
        this.iAdvanceTermsAndConditionActivityView = iAdvanceTermsAndConditionActivityView;
        iAdvanceTermsAndConditionActivityInteractor = new AdvanceTermsAndConditionActivityInteractor(this);
    }

    @Override
    public void checkRemainingSteps() {
        iAdvanceTermsAndConditionActivityView.setBtMainButtonEnabled(false);
        iAdvanceTermsAndConditionActivityView.showTezLoader();
        iAdvanceTermsAndConditionActivityInteractor.getLoanRemainingSteps();
    }

    @Override
    public void applyForLoan() {
        iAdvanceTermsAndConditionActivityView.showTezLoader();
        iAdvanceTermsAndConditionActivityView.getCurrentLocation(this);
    }

    @Override
    public void onLoanApplySuccess(int statusCode) {
        if (statusCode == 0)
            iAdvanceTermsAndConditionActivityView.routeToWaitForDisbursementActivity();

        else if (statusCode == ResponseStatusCode.CNIC_EXPIRED.getCode() || statusCode == ResponseStatusCode.CNIC_ABOUT_TO_EXPIRE.getCode())
            iAdvanceTermsAndConditionActivityView.showError(statusCode, (d, w) -> iAdvanceTermsAndConditionActivityView.routeToCompleteYourProfileActivity());
        else
            iAdvanceTermsAndConditionActivityView.showError(statusCode,
                    iAdvanceTermsAndConditionActivityView.getFormattedErrorMessage(ResponseStatusCode.getDescriptionFromErrorCode(statusCode)),
                    (d, w) -> iAdvanceTermsAndConditionActivityView.routeToDashboard());
    }

    @Override
    public void onLoanApplyFailure(int errorCode, String message) {
        iAdvanceTermsAndConditionActivityView.setBtMainButtonEnabled(true);
        iAdvanceTermsAndConditionActivityView.dismissTezLoader();
        iAdvanceTermsAndConditionActivityView.showError(errorCode);
    }

    @Override
    public void onGetLoanRemainingStepsSuccess(List<String> stepsLeft) {
        if (stepsLeft.isEmpty())
            applyForLoan();
        else {
            iAdvanceTermsAndConditionActivityView.setTezLoaderToBeDissmissedOnTransition();
            iAdvanceTermsAndConditionActivityView.setBtMainButtonEnabled(true);

            if (stepsLeft.size() == 1 && stepsLeft.get(0).equals(Constants.BENEFICIARY))
                iAdvanceTermsAndConditionActivityView.routeToAddBeneficiaryActivity();

            else
                iAdvanceTermsAndConditionActivityView.routeToLoanRequiredStepsActivity(stepsLeft);
        }
    }

    @Override
    public void onGetLoanRemainingStepsFailure(int errorCode, String message) {
        iAdvanceTermsAndConditionActivityView.dismissTezLoader();
        iAdvanceTermsAndConditionActivityView.showError(errorCode);
        iAdvanceTermsAndConditionActivityView.setBtMainButtonEnabled(true);
    }

    @Override
    public void onLocationFailed(String message) {
        iAdvanceTermsAndConditionActivityView.setBtMainButtonEnabled(true);
        iAdvanceTermsAndConditionActivityView.dismissTezLoader();
        iAdvanceTermsAndConditionActivityView.showInformativeMessage(R.string.string_location_not_fetch);
    }

    @Override
    public void onLocationAvailable(@NonNull Location location) {
        LoanApplyRequest loanApplyRequest = iAdvanceTermsAndConditionActivityView.getLoanApplyRequest();
        if (loanApplyRequest != null) {
            loanApplyRequest.setLat(location.getLatitude());
            loanApplyRequest.setLng(location.getLongitude());
            iAdvanceTermsAndConditionActivityInteractor.applyLoan(loanApplyRequest);
        }
    }
}
