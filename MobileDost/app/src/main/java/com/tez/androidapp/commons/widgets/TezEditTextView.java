package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.TezDrawableHelper;

import net.tez.fragment.util.base.IBaseWidget;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.fragment.util.optional.Optional;
import net.tez.fragment.util.optional.TextUtil;

/**
 * Created by FARHAN DHANANI on 1/9/2019.
 */
public class TezEditTextView extends AppCompatEditText implements IBaseWidget {
    private EmojiExcludeFilter emojiExcludeFilter;

    public TezEditTextView(Context context) {
        super(context);
    }

    public TezEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public TezEditTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void disableCopyPaste() {

        this.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });

        this.setOnLongClickListener(v -> true);
    }

    @NonNull
    @Override
    public String getLabel() {
        return TextUtil.isNotEmpty(getText()) ? getText().toString() : "";
    }

    public void clearText() {
        setText("");
    }

    public String getValueText() {
        Editable editable = getText();
        if (editable != null)
            return editable.toString().trim();
        return null;
    }

    public void setCursorToLastPositionAlways() {
        this.setOnClickListener(v -> {
            Editable text = getText();
            if (text != null)
                setSelection(text.length());
        });
    }

    public void acceptOnlyDigits() {
        this.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        this.setKeyListener(DigitsKeyListener.getInstance(getResources().getString(R.string.digits)));
        ViewCompat.setLayoutDirection(this, ViewCompat.LAYOUT_DIRECTION_LTR);
    }

    public void acceptOnlyNumberDecimal() {
        this.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        this.setKeyListener(DigitsKeyListener.getInstance(false, true));
        ViewCompat.setLayoutDirection(this, ViewCompat.LAYOUT_DIRECTION_LTR);
    }

    public void setMaxDigits(int numDigits) {
        InputFilter[] editFilters = this.getFilters();
        InputFilter[] newFilters = new InputFilter[editFilters.length + 1];
        System.arraycopy(editFilters, 0, newFilters, 0, editFilters.length);
        newFilters[editFilters.length] = new InputFilter.LengthFilter(numDigits);
        this.setFilters(newFilters);
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TezEditTextView);
        try{
            setFilters(getInputFilters());
            Drawable start = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezEditTextView_drawableStartCompat);
            Drawable end = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezEditTextView_drawableEndCompat);
            Drawable top = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezEditTextView_drawableTopCompat);
            Drawable bottom = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezEditTextView_drawableBottomCompat);
            changeDrawable(this, start, top, end, bottom);

            int rawInputType = typedArray.getInt(R.styleable.TezEditTextView_raw_input, 0);

            if (rawInputType == 1)
                this.acceptOnlyDigits();

            else if (rawInputType == 2)
                this.acceptOnlyNumberDecimal();

        } finally {
            typedArray.recycle();
        }
    }

    private InputFilter[] getInputFilters(){
        InputFilter[] inputFilters = getFilters();
        if (inputFilters == null || inputFilters.length == 0){
            return new InputFilter[]{new EmojiExcludeFilter()};
        } else {
            InputFilter[] newInputFilters = new InputFilter[inputFilters.length+1];
            System.arraycopy(inputFilters, 0, newInputFilters, 0, inputFilters.length);
            newInputFilters[inputFilters.length] = new EmojiExcludeFilter();
            return newInputFilters;
        }
    }

    public void setTextViewDrawableColor(@ColorRes int color) {
        for (Drawable drawable : getCompoundDrawables())
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(getContext(), color), PorterDuff.Mode.SRC_IN));
            }
    }

    public void clearTextViewDrawableColor() {
        for (Drawable drawable : getCompoundDrawables())
            if (drawable != null) {
                drawable.clearColorFilter();
            }
    }

    @Override
    public void setError(CharSequence error) {
        super.setError(error);
        changeDrawableColorOnError(error);
    }

    @Override
    public void setError(CharSequence error, Drawable icon) {
        super.setError(error, icon);
        changeDrawableColorOnError(error);
    }

    public void changeDrawableColorOnError(CharSequence error) {
        Optional.ifPresent(error,
                s -> {
                    this.setTextViewDrawableColor(R.color.colorRed);
                }, this::clearTextViewDrawableColor);
    }

    @Override
    public void setDoubleTapSafeOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        super.setOnClickListener(doubleTapSafeOnClickListener);
    }


    private static class EmojiExcludeFilter implements InputFilter {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            boolean keepOriginal = true;
            StringBuilder sb = new StringBuilder(end - start);
            for (int i = start; i < end; i++) {
                char c = source.charAt(i);
                int type = Character.getType(c);
                if (type == Character.SURROGATE || type == Character.OTHER_SYMBOL) {
                    keepOriginal = false;
                } else {
                    sb.append(c);
                }
            }
            if (keepOriginal) {
                return null;
            } else {
                if (source instanceof Spanned) {
                    SpannableString sp = new SpannableString(sb);
                    TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                    return sp;
                } else {
                    return sb;
                }
            }
        }
    }
}
