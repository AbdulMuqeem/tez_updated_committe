package com.tez.androidapp.rewamp.general.beneficiary.response.handler;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.general.beneficiary.listener.PutBeneficiaryAllocationListener;
import com.tez.androidapp.rewamp.general.beneficiary.response.BeneficiaryAllocationResponse;

public class BeneficiaryAllocationRH extends BaseRH<BeneficiaryAllocationResponse> {

    private final PutBeneficiaryAllocationListener listener;

    public BeneficiaryAllocationRH(BaseCloudDataStore baseCloudDataStore, PutBeneficiaryAllocationListener listener) {
        super(baseCloudDataStore);
        this.listener = listener;
    }

    @Override
    protected void onSuccess(Result<BeneficiaryAllocationResponse> value) {
        BeneficiaryAllocationResponse response = value.response().body();

        if (response != null) {

            if (isErrorFree(response))
                listener.onBeneficiaryAllocationSuccess(response);
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());

        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        listener.onBeneficiaryAllocationFailure(errorCode, message);
    }
}
