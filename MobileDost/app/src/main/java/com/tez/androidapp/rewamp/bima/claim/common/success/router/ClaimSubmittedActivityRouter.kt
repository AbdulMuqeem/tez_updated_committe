package com.tez.androidapp.rewamp.bima.claim.common.success.router

import android.content.Intent
import com.tez.androidapp.app.base.ui.activities.BaseActivity
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter
import com.tez.androidapp.rewamp.bima.claim.common.success.ClaimSubmittedActivity
import com.tez.androidapp.rewamp.bima.claim.model.InsuranceClaimConfirmationDto

class ClaimSubmittedActivityRouter : BaseActivityRouter() {

    fun setDependenciesAndRoute(from: BaseActivity,
                                productId: Int,
                                revise: Boolean,
                                insuranceClaimConfirmationDto: InsuranceClaimConfirmationDto) {
        val intent = Intent(from, ClaimSubmittedActivity::class.java)
        intent.putExtra(PRODUCT_ID, productId)
        intent.putExtra(DTO, insuranceClaimConfirmationDto)
        intent.putExtra(REVISE, revise)
        route(from, intent)
    }

    class Dependencies internal constructor(private val intent: Intent) {

        val productId: Int
            get() = intent.getIntExtra(PRODUCT_ID, -100)

        val insuranceClaimConfirmationDto: InsuranceClaimConfirmationDto
            get() = intent.getParcelableExtra(DTO)!!

        val revise: Boolean
            get() = intent.getBooleanExtra(REVISE, false)
    }

    companion object {
        private const val PRODUCT_ID = "PRODUCT_ID"
        private const val DTO = "DTO"
        private const val REVISE = "REVISE"

        fun getDependencies(intent: Intent) = Dependencies(intent)
    }
}