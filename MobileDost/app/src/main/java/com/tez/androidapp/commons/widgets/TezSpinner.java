package com.tez.androidapp.commons.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.core.content.ContextCompat;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.TezDrawableHelper;

import net.tez.fragment.util.base.IBaseWidget;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

/**
 * Created by VINOD KUMAR on 2/15/2019.
 */
public class TezSpinner extends AppCompatAutoCompleteTextView implements IBaseWidget {

    private boolean showPopup = true;

    public TezSpinner(Context context) {
        super(context);
    }

    public TezSpinner(Context arg0, AttributeSet arg1) {
        super(arg0, arg1);
        if(arg1!=null)
            init(arg0, arg1);
    }

    public TezSpinner(Context arg0, AttributeSet arg1, int arg2) {
        super(arg0, arg1, arg2);
        if(arg1!=null)
            init(arg0, arg1);

    }

    @Override
    public void setDoubleTapSafeOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        super.setOnClickListener(doubleTapSafeOnClickListener);
    }

    private void init(@NonNull Context context, @NonNull AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TezSpinner);
        try {
            Drawable start = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezSpinner_drawableStartCompat);
            Drawable end = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezSpinner_drawableEndCompat);
            Drawable top = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezSpinner_drawableTopCompat);
            Drawable bottom = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezSpinner_drawableBottomCompat);
            changeDrawable(this, start, top, end, bottom);

        } finally {
            typedArray.recycle();
        }
    }

    @Override
    public boolean enoughToFilter() {
        return true;
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction,
                                  Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (focused) {
            performFiltering("", 0);
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindowToken(), 0);
            setKeyListener(null);
            dismissDropDown();
        } else
            showPopup = true;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(!isEnabled() || !isFocusable())
            return false;

        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                if (showPopup) {
                    requestFocus();
                    showDropDown();
                    showPopup = false;
                } else
                    showPopup = true;
                break;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void setOnItemClickListener(AdapterView.OnItemClickListener l) {
        super.setOnItemClickListener(new OnItemClickListenerImpl(l));
    }

    @Override
    public void setCompoundDrawablesWithIntrinsicBounds(Drawable left, Drawable top, Drawable right, Drawable bottom) {
        Drawable dropdownIcon = ContextCompat.getDrawable(getContext(), R.drawable.ic_expand_more_black_18dp);
        if (dropdownIcon != null) {
            right = dropdownIcon;
            right.mutate().setAlpha(128);
        }
        super.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom);
    }

    public String getValueText() {
        Editable editable = getText();
        if (editable != null)
            return editable.toString().trim();
        return null;
    }

    private class OnItemClickListenerImpl implements AdapterView.OnItemClickListener {

        private AdapterView.OnItemClickListener listener;

        private OnItemClickListenerImpl(AdapterView.OnItemClickListener listener) {
            this.listener = listener;
        }

        @CallSuper
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            showPopup = true;
            this.listener.onItemClick(adapterView, view, i, l);
        }
    }
}
