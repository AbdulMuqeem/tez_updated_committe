package com.tez.androidapp.rewamp.advance.request.view;

import com.tez.androidapp.app.base.ui.IBaseView;

public interface ICancelLimitActivityView extends IBaseView {

    void routeToDashboardActivity();
}
