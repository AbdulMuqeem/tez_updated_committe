package com.tez.androidapp.commons.location.service;

import android.location.Location;
import androidx.annotation.NonNull;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.SimpleJobService;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.location.handlers.LocationHandler;
import com.tez.androidapp.commons.utils.extract.ExtractionUtil;
import com.tez.androidapp.commons.utils.file.util.FileUtil;

/**
 * Created by Rehman Murad Ali on 5/31/2018.
 */
public class LocationUpdateJobService extends SimpleJobService implements LocationAvailableCallback {

    @Override
    public void onLocationAvailable(@NonNull Location location) {
        FileUtil.writeObjectToFile(getApplicationContext(),
                ExtractionUtil.TYPE_LOCATION,
                new com.tez.androidapp.commons.location.models.Location(
                        location.getLatitude(),
                        location.getLongitude()
                )
                , "$");
    }

    @Override
    public int onRunJob(JobParameters job) {
        LocationHandler.requestCurrentLocation(getApplicationContext(), this);
        return RESULT_SUCCESS;
    }
}
