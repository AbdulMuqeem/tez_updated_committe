package com.tez.androidapp.app.general.feature.mobile.verification.callback;

/**
 * Created by FARHAN DHANANI on 2/26/2019.
 */
public interface NumberVerifyGenerateOtpCallback {
    void onNumberVerifyGenerateOtpSuccess();

    void registerSmsReciever();

    void onNumberVerifyGenerateOtpFailure(int errorCode, String message);
}
