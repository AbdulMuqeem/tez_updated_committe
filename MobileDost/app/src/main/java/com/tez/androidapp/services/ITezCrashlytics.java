package com.tez.androidapp.services;

import androidx.annotation.NonNull;

public interface ITezCrashlytics {

    void setCrashlyticsCollectionEnabled(boolean enabled);

    void recordException(@NonNull Throwable throwable);

    void log(@NonNull String message);

    void setUserId(@NonNull String userId);

    void setCustomKey(@NonNull String key, @NonNull String value);
}
