package com.tez.androidapp.rewamp.general.feedback.presenter;

import androidx.annotation.Nullable;

public interface ILoanFeedbackActivityPresenter {

    void submitFeedback(int loanId, int rating, @Nullable Integer cause, @Nullable String comment, boolean fullRating);
}
