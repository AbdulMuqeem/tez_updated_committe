package com.tez.androidapp.app.vertical.advance.loan.shared.answers.callbacks;

/**
 * Created  on 2/22/2017.
 */

public interface LoanAnswersCallback {

    void onLoanAnswersSuccess();

    void onLoanAnswersFailure(int statusCode, String errorDescription);
}
