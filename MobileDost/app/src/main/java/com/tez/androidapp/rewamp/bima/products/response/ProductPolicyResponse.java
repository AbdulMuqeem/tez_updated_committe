package com.tez.androidapp.rewamp.bima.products.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.rewamp.bima.products.model.ProductPolicy;

public class ProductPolicyResponse extends BaseResponse {

    private ProductPolicy policyDetailsDto;

    public ProductPolicy getPolicyDetailsDto() {
        return policyDetailsDto;
    }
}
