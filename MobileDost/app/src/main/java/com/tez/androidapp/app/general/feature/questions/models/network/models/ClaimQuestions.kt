package com.tez.androidapp.app.general.feature.questions.models.network.models

data class ClaimQuestions(
        val dateOfAdmission: DateQuestion,
        val dateOfDischarge: DateQuestion,
        val locationOfHospital: LocationQuestion,
)