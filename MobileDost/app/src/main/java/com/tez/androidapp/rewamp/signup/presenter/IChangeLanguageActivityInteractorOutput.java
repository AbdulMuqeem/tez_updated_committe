package com.tez.androidapp.rewamp.signup.presenter;

import androidx.annotation.NonNull;

/**
 * Created by VINOD KUMAR on 8/9/2019.
 */
public interface IChangeLanguageActivityInteractorOutput {

    void onLanguageSuccess(@NonNull String lang);

    void onLanguageFailure(int errorCode, String errorDescription);
}
