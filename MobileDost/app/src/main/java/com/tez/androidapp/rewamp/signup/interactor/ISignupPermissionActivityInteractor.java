package com.tez.androidapp.rewamp.signup.interactor;

import com.tez.androidapp.commons.models.network.dto.request.UserSignUpRequest;

public interface ISignupPermissionActivityInteractor {
    void submitUserSignUpRequest(UserSignUpRequest userSignUpRequest);
}
