package com.tez.androidapp.rewamp.advance.request.presenter;

import com.tez.androidapp.app.vertical.advance.loan.confirmation.callbacks.GenerateDisbursementReceiptCallback;

public interface ILoanReceiptActivityInteractorOutput extends GenerateDisbursementReceiptCallback {
}
