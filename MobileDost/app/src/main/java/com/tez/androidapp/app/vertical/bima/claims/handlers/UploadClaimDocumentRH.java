package com.tez.androidapp.app.vertical.bima.claims.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.vertical.bima.claims.callback.UploadClaimDocumentCallBack;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.UploadClaimDocumentResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

import java.util.List;

/**
 * Created by FARHAN DHANANI on 6/21/2018.
 */
public class UploadClaimDocumentRH extends BaseRH<UploadClaimDocumentResponse> {
    private UploadClaimDocumentCallBack uploadClaimAnswersCallBack;

    public UploadClaimDocumentRH(BaseCloudDataStore baseCloudDataStore,
                                 @Nullable UploadClaimDocumentCallBack uploadClaimAnswersCallBack) {
        super(baseCloudDataStore);
        this.uploadClaimAnswersCallBack = uploadClaimAnswersCallBack;
    }

    @Override
    protected void onSuccess(Result<UploadClaimDocumentResponse> value) {
        UploadClaimDocumentResponse uploadClaimDocumentResponse = value.response().body();
        if (uploadClaimDocumentResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            if (this.uploadClaimAnswersCallBack != null)
                uploadClaimAnswersCallBack.onUploadClaimDocumentSuccess(uploadClaimDocumentResponse);
        } else {
            onFailure(uploadClaimDocumentResponse.getStatusCode(), uploadClaimDocumentResponse.getErrorDescription());
        }
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (uploadClaimAnswersCallBack != null)
            this.uploadClaimAnswersCallBack.onUploadClaimDocumentFailure(errorCode, message);
    }
}
