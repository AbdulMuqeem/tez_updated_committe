package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto

import java.util.*

/**
 * Created by Vinod Kumar on 5/6/2020.
 */
data class Evidence(
        val id: Int,
        val name: Int,
        var evidenceFileList: MutableList<EvidenceFile> = ArrayList(),
        var rejected: Boolean = false,
)