package com.tez.androidapp.app.base.observer;

/**
 * Created by VINOD KUMAR on 2/21/2019.
 */
@FunctionalInterface
public interface OnAppStart {

    void onAppStart(boolean inactiveTimeExceeded);

}
