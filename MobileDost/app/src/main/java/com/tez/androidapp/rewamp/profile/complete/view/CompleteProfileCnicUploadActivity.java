package com.tez.androidapp.rewamp.profile.complete.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.GetCnicPictureRequest;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.utils.cnic.detection.model.RetumDataModel;
import com.tez.androidapp.commons.utils.cnic.detection.view.CnicScannerActivity;
import com.tez.androidapp.commons.utils.file.util.FileUtil;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.commons.widgets.UploadImageCardView;
import com.tez.androidapp.rewamp.profile.complete.presenter.CompleteProfileCnicUploadActivityPresenter;
import com.tez.androidapp.rewamp.profile.complete.presenter.ICompleteProfileCnicUploadActivityPresenter;
import com.tez.androidapp.rewamp.profile.complete.router.CompleteProfileCnicUploadActivityRouter;
import com.tez.androidapp.rewamp.profile.complete.router.CnicInformationActivityRouter;
import com.tez.androidapp.rewamp.profile.router.CompleteProfileRouter;

import net.tez.camera.cameras.CameraProperties;
import net.tez.camera.request.CameraRequestBuilder;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class CompleteProfileCnicUploadActivity extends BaseActivity
        implements ICompleteProfileCnicUploadActivityView, DoubleTapSafeOnClickListener {

    @BindView(R.id.btContinue)
    private TezButton btContinue;

    @BindView(R.id.tvSecondaryButton)
    private TezTextView tvSecondaryButton;

    @BindView(R.id.cvFrontPart)
    private UploadImageCardView cvFrontPart;

    @BindView(R.id.cvBackPart)
    private UploadImageCardView cvBackPart;

    @BindView(R.id.cvSelfiePart)
    private UploadImageCardView cvSelfiePart;

    private final ICompleteProfileCnicUploadActivityPresenter iCompleteProfileCnicUploadActivityPresenter;

    public CompleteProfileCnicUploadActivity() {
        this.iCompleteProfileCnicUploadActivityPresenter =
                new CompleteProfileCnicUploadActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_cnic_upload_profile);
        ViewBinder.bind(this);
        init();
        initOnclickListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void onImageSelfiePartIsUploaded(){
        this.cvSelfiePart.onImageCaptured(GetCnicPictureRequest.METHOD_NAME_SELFIE_NIC, R.drawable.img_selfie_placeholder);
        this.iCompleteProfileCnicUploadActivityPresenter.setUserSelfieIsUploaded();
    }

    private void onImageFrontPartIsUploaded(){
        this.cvFrontPart.onImageCaptured(GetCnicPictureRequest.METHOD_NAME_FRONT_NIC, R.drawable.img_cnic_back_placeholder);
        this.iCompleteProfileCnicUploadActivityPresenter.setUserFrontNicIsUploaded();
    }

    private void onImageBackPartIsUploaded(){
        this.cvBackPart.onImageCaptured(GetCnicPictureRequest.METHOD_NAME_BACK_NIC, R.drawable.img_cnic_front_placeholder);
        this.iCompleteProfileCnicUploadActivityPresenter.setUserBackNicIsUploaded();
    }

    public void init(){
        Optional.ifPresent(getIntent(), intent -> {
            Optional.doWhen(!intent.getBooleanExtra(CompleteProfileCnicUploadActivityRouter.IS_CNIC_SELFIE_TO_BE_UPLOADED, false),
                    this::onImageSelfiePartIsUploaded);

            Optional.doWhen(!intent.getBooleanExtra(CompleteProfileCnicUploadActivityRouter.IS_CNIC_FRONT_PIC_TO_BE_UPLOADED, false),
                    this::onImageFrontPartIsUploaded);

            Optional.doWhen(!intent.getBooleanExtra(CompleteProfileCnicUploadActivityRouter.IS_CNIC_BACK_PIC_TO_BE_UPLOADED, false),
                    this::onImageBackPartIsUploaded);
        });
        this.tvSecondaryButton.setDoubleTapSafeOnClickListener(this::routeToProfileCompletionDetailsActivity);
    }

    private void initOnclickListener() {
        this.cvBackPart.setDoubleTapSafeOnClickListener(this);
        this.cvFrontPart.setDoubleTapSafeOnClickListener(this);
        this.cvSelfiePart.setDoubleTapSafeOnClickListener(this);
    }

    @Override
    public void startRetumModuleScreen(int requestCode, String detectObject) {
        Intent intent = new Intent(this, CnicScannerActivity.class);
        intent.putExtra(CnicScannerActivity.DETECT_OBJECT, detectObject);
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void startBackupCamera(int requestCode, boolean isSelfie) {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.CNIC_REQUEST_CODE, requestCode);
        new CameraRequestBuilder()
                .setCustomLayoutId(isSelfie ? R.layout.selfie_preview_template : R.layout.cnic_scanner_rectangle)
                .setEnableSwitchCamera(false)
                .setLensFacing(isSelfie ? CameraProperties.LENS_FACING_FRONT : CameraProperties.LENS_FACING_BACK)
                .setRootPath(FileUtil.getSignUpRootPath(this))
                .build()
                .startCamera(this, bundle);
    }

    @Override
    public void setBtContinueActive(String cnic, String dob,
                                    String issueDate, String expiryDate){
        this.btContinue.setButtonBackgroundStyle(TezButton.ButtonStyle.NORMAL);
        this.btContinue.setDoubleTapSafeOnClickListener(v ->this.routeToProfileCompletionDetailsActivity(cnic,
                dob, issueDate, expiryDate));
        this.tvSecondaryButton.setVisibility(View.GONE);
    }

    private void routeToProfileCompletionDetailsActivity(String cnic, String dob,
                                                         String issueDate, String expiryDate){
        CnicInformationActivityRouter.createInstance().setDependenciesAndRoute(this,
                cnic, dob, issueDate, expiryDate, getKeyRoute());
        finish();
    }

    private void routeToProfileCompletionDetailsActivity(View v){
        CnicInformationActivityRouter.createInstance().setDependenciesAndRoute(this, getKeyRoute());
        finish();
    }

    private int getKeyRoute(){
        return getIntent().getIntExtra(CompleteProfileRouter.ROUTE_KEY, CompleteProfileRouter.ROUTE_TO_DEFAULT);
    }

    @Override
    public void loadImageInUpCVFrontPart(String imagePath) {
        this.cvFrontPart.onImageCaptured(imagePath, R.drawable.img_cnic_front_placeholder);
    }

    @Override
    public void setLoaderInUpCVFrontPartVisible(boolean visibile) {
        this.cvFrontPart.setImageLoaderVisibility(visibile);
    }

    @Override
    public void setLoaderInUpCVSelfiePartVisbile(boolean visibile) {
        this.cvSelfiePart.setImageLoaderVisibility(visibile);
    }

    @Override
    public void setLoaderInUpCVBackPart(boolean visible) {
        this.cvBackPart.setImageLoaderVisibility(visible);
    }

    @Override
    public void loadImageInUpCVSelfiePart(String imagePath) {
        this.cvSelfiePart.onImageCaptured(imagePath, R.drawable.img_selfie_placeholder);
    }


    @Override
    public void loadImageInUpCVBackPart(String imagePath) {
        this.cvBackPart.onImageCaptured(imagePath, R.drawable.img_cnic_back_placeholder);
    }

    @Override
    public void updateClickListeners(String cnic, String dob, String issueDate, String expiryDate) {
        this.tvSecondaryButton.setDoubleTapSafeOnClickListener((v)->
                this.routeToProfileCompletionDetailsActivity(cnic, dob, issueDate, expiryDate));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (isRequestCodeBelongToOCR(requestCode) && resultCode == RESULT_OK && data != null) {
            RetumDataModel retumDataModel = new RetumDataModel();
            retumDataModel.setCnicNumber(data.getStringExtra(CnicScannerActivity.CNIC_NUMBER));
            retumDataModel.setCnicIssueDate(data.getStringExtra(CnicScannerActivity.CNIC_ISSUE_DATE));
            retumDataModel.setCnicExpiryDate(data.getStringExtra(CnicScannerActivity.CNIC_EXPIRY_DATE));
            retumDataModel.setDateOfBirth(data.getStringExtra(CnicScannerActivity.CNIC_DATE_OF_BIRTH));
            retumDataModel.setImageURL(data.getStringExtra(CnicScannerActivity.IMAGE_PATH));
            MDPreferenceManager.setOCRFailedCount(0);
            this.iCompleteProfileCnicUploadActivityPresenter.updateCnicImageCardViews(requestCode,
                    retumDataModel.getImageURL(),
                    true,
                    retumDataModel);
        } else if (isRequestCodeBelongToOCR(requestCode) && resultCode == RESULT_CANCELED && data != null &&
                CnicScannerActivity.CNIC_DETECTION_FAILED
                        .equalsIgnoreCase(data.getStringExtra(CnicScannerActivity.FAILURE_REASON))) {
            MDPreferenceManager.setOCRFailedCount(MDPreferenceManager.getOCRFailedCount()+1);
            showInformativeMessage(R.string.cnic_fields_not_detected);

        } else if (isRequestCodeBelongToOCR(requestCode) && resultCode == RESULT_CANCELED && data != null &&
                CnicScannerActivity.FACE_DETECTION_FAIILED
                        .equalsIgnoreCase(data.getStringExtra(CnicScannerActivity.FAILURE_REASON))) {
            MDPreferenceManager.setOCRFailedCount(MDPreferenceManager.getOCRFailedCount()+1);
            showInformativeMessage(R.string.cnic_fields_not_detected);
        } else if (requestCode == CameraRequestBuilder.REQUEST_CODE_CAMERA
                && resultCode == RESULT_OK
                && data != null) {
            Optional.ifPresent(data.getStringArrayListExtra(CameraRequestBuilder.RESULT_IMAGE_FILES_ARRAY_LIST), paths->{
                Optional.ifPresent(data.getBundleExtra(CameraRequestBuilder.PARAMETERS_BUNDLE), bundle -> {
                    this.iCompleteProfileCnicUploadActivityPresenter.updateCnicImageCardViews(
                            bundle.getInt(Constants.CNIC_REQUEST_CODE, -1),
                            paths.get(0),
                            false, null);
                });
            });
        }
    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        int requestId;
        String detectObject;
        boolean isSelfie = false;
        switch (view.getId()) {
            case R.id.cvFrontPart:
                requestId = Constants.CNIC_REQUEST_CODE_FRONT;
                detectObject = CnicScannerActivity.CNIC_FRONT;
                break;
            case R.id.cvBackPart:
                requestId = Constants.CNIC_REQUEST_CODE_BACK;
                detectObject = CnicScannerActivity.CNIC_BACK;
                break;
            case R.id.cvSelfiePart:
                requestId = Constants.CNIC_REQUEST_CODE_FACE;
                detectObject = CnicScannerActivity.FACE;
                isSelfie = true;
                break;
            default:
                requestId = -1;
                detectObject = null;
        }
        this.iCompleteProfileCnicUploadActivityPresenter.startCamera(requestId, detectObject, isSelfie);
    }

    @Override
    protected String getScreenName() {
        return "CompleteProfileCnicUploadActivity";
    }
}
