package com.tez.androidapp.rewamp.bima.products.interactor;

import com.tez.androidapp.rewamp.bima.claim.callback.InitiateClaimCallback;
import com.tez.androidapp.rewamp.bima.products.callback.ProductPolicyCallback;

public interface IProductPolicyActivityInteractorOutput extends ProductPolicyCallback, InitiateClaimCallback {
}
