package com.tez.androidapp.rewamp.general.changepin;

import android.content.Intent;

import com.tez.androidapp.rewamp.signup.PinChangedActivity;
import com.tez.androidapp.rewamp.signup.router.WelcomeBackActivityRouter;

/**
 * Created by VINOD KUMAR on 8/26/2019.
 */
public class InAppPinChangedActivity extends PinChangedActivity {

    @Override
    protected void onClickBtOkay() {
        WelcomeBackActivityRouter.createInstance().setDependenciesAndRouteWithClearAndNewTask(this);
    }

    @Override
    public void onBackPressed() {
        onClickBtOkay();
    }

    @Override
    protected String getScreenName() {
        return "InAppPinChangedActivity";
    }
}
