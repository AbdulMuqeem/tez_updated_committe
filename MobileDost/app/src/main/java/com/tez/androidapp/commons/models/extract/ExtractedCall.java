package com.tez.androidapp.commons.models.extract;

import com.tez.androidapp.commons.utils.app.Utility;

import me.everything.providers.android.calllog.Call;

/**
 * Created  on 9/7/2017.
 */

public class ExtractedCall {

    private String name;
    private String number;
    private String type;
    private String callDate;
    private long id;
    private long duration;
    private boolean isRead;

    public ExtractedCall(Call call) {

        type = call.type == null ? null : call.type.name();
        callDate = Utility.getFormattedDate(call.callDate);
        duration = call.duration;
        id = call.id;
        isRead = call.isRead;
        name = call.name;
        number = call.number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCallDate() {
        return callDate;
    }

    public void setCallDate(String callDate) {
        this.callDate = callDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }
}
