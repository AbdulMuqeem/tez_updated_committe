package com.tez.androidapp.app.general.feature.account.feature.profile.handlers;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.AccountLinkCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.AccountLinkResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.GetUserProfileResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

public class AccountLinkRH extends BaseRH<AccountLinkResponse> {

    private final AccountLinkCallback accountLinkCallback;

    public AccountLinkRH(BaseCloudDataStore baseCloudDataStore, AccountLinkCallback accountLinkCallback) {
        super(baseCloudDataStore);
        this.accountLinkCallback = accountLinkCallback;
    }

    @Override
    protected void onSuccess(Result<AccountLinkResponse> value) {
        @NonNull AccountLinkResponse accountLinkResponse = value.response().body();
        if (accountLinkResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            accountLinkCallback.accountLinkSuccess();
        else onFailure(accountLinkResponse.getStatusCode(), accountLinkResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        accountLinkCallback.accountLinkFailure(errorCode, message);
    }
}
