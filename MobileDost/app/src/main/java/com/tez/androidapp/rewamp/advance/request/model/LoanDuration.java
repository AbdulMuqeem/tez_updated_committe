package com.tez.androidapp.rewamp.advance.request.model;

/**
 * Created by Rehman Murad Ali
 **/
public class LoanDuration {

    private int dayTenure;
    private String title;
    private int minAmount;
    private int maxAmount;

    public LoanDuration(int dayTenure, String title, int minAmount, int maxAmount) {
        this.dayTenure = dayTenure;
        this.title = title;
        this.minAmount = minAmount;
        this.maxAmount = maxAmount;
    }

    public int getDayTenure() {
        return dayTenure;
    }

    public String getTitle() {
        return title;
    }

    public int getMinAmount() {
        return minAmount;
    }

    public int getMaxAmount() {
        return maxAmount;
    }
}
