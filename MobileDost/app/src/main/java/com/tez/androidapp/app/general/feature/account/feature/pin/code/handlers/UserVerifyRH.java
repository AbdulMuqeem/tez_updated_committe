package com.tez.androidapp.app.general.feature.account.feature.pin.code.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.UserVerifyCallback;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserVerifyResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created  on 2/9/2017.
 */

public class UserVerifyRH extends BaseRH<UserVerifyResponse> {

    private UserVerifyCallback userVerifyCallback;

    public UserVerifyRH(BaseCloudDataStore baseCloudDataStore, @Nullable UserVerifyCallback userVerifyCallback) {
        super(baseCloudDataStore);
        this.userVerifyCallback = userVerifyCallback;
    }

    @Override
    protected void onSuccess(Result<UserVerifyResponse> value) {
        UserVerifyResponse userVerifyResponse = value.response().body();
        if (isErrorFree(userVerifyResponse)) {
            if (userVerifyCallback != null) userVerifyCallback.onUserVerifySuccess(userVerifyResponse);
        } else onFailure(userVerifyResponse.getStatusCode(), userVerifyResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (userVerifyCallback != null) userVerifyCallback.onUserVerifyFailure(new BaseResponse(errorCode, message));
    }
}
