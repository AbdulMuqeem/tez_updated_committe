package com.tez.androidapp.commons.calendar;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.widget.CheckBox;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Utility;

import net.tez.calendar.AndroidDatePickerDialog;

/**
 * Created by VINOD KUMAR on 10/29/2018.
 */
class LifetimeDatePickerDialog extends AndroidDatePickerDialog {

    @NonNull
    private OnLifetimeChecked onLifetimeChecked;

    LifetimeDatePickerDialog(@NonNull Context context, @NonNull OnDateSetListener onDateSetListener, @NonNull OnLifetimeChecked onLifetimeChecked) {
        super(context, onDateSetListener);
        this.onLifetimeChecked = onLifetimeChecked;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CheckBox lifetimeCheckBox = getInflatedView().findViewById(R.id.checkboxLifetime);
        lifetimeCheckBox.setVisibility(View.VISIBLE);
        lifetimeCheckBox.setButtonDrawable(R.drawable.t_and_c_checkbox);
        lifetimeCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> enableDatePickerView(!isChecked));
    }

    @Override
    protected void setButtonOkOnClickListener() {
        buttonOk.setOnClickListener(view -> {
            CheckBox lifetimeCheckBox = getInflatedView().findViewById(R.id.checkboxLifetime);
            if (lifetimeCheckBox.isChecked())
                onLifetimeChecked.onLifetimeChecked(Utility.getStringFromResource(R.string.string_lifetime));
            else
                notifyListener();
            dismiss();
        });
    }

    @Override
    protected int getClientCustomLayout() {
        return R.layout.android_date_picker_view;
    }
}
