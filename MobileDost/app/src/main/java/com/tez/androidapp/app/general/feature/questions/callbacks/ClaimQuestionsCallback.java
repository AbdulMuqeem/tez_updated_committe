package com.tez.androidapp.app.general.feature.questions.callbacks;

import com.tez.androidapp.app.general.feature.questions.models.network.models.ClaimQuestion;

import java.util.List;

/**
 * Created  on 2/17/2017.
 */

public interface ClaimQuestionsCallback {

    void onQuestionsSuccess(List<ClaimQuestion> claimQuestions);

    void onQuestionsFailure(int errorCode, String message);
}
