package com.tez.androidapp.rewamp.bima.products.response.handler;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.bima.products.callback.ProductDetailTermConditionCallback;

import java.io.IOException;

import okhttp3.ResponseBody;

public class ProductDetailTermConditionRH extends BaseRH<ResponseBody> {

    private final ProductDetailTermConditionCallback callback;

    public ProductDetailTermConditionRH(BaseCloudDataStore baseCloudDataStore, ProductDetailTermConditionCallback callback) {
        super(baseCloudDataStore);
        this.callback = callback;
    }

    @Override
    protected void onSuccess(Result<ResponseBody> value) {
        ResponseBody responseBody = value.response().body();
        try {
            if (responseBody != null)
                callback.onGetProductDetailTermConditionSuccess(responseBody.string());
            else
                sendDefaultFailure();
        } catch (IOException e) {
            sendDefaultFailure();
        }
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        callback.onGetProductDetailTermConditionFailure(errorCode, message);
    }
}
