package com.tez.androidapp.rewamp.committee.response;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeCreationLIstener;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import androidx.annotation.Nullable;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class CommitteeCreationRH extends BaseRH<CommitteeCreateResponse> {

    private final CommitteeCreationLIstener listener;

    public CommitteeCreationRH(BaseCloudDataStore baseCloudDataStore, CommitteeCreationLIstener committeeCreationLIstener) {
        super(baseCloudDataStore);
        this.listener = committeeCreationLIstener;
    }

    @Override
    protected void onSuccess(Result<CommitteeCreateResponse> value) {
        CommitteeCreateResponse committeeCreateResponse = value.response().body();
        if (committeeCreateResponse != null && committeeCreateResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.listener.onCommitteeCreateSuccess(committeeCreateResponse);
        else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onCommitteeCreateFailed(errorCode, message);
    }
}
