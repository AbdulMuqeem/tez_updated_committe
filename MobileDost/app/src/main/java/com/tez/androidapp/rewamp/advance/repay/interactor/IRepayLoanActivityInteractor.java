package com.tez.androidapp.rewamp.advance.repay.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;

public interface IRepayLoanActivityInteractor extends IBaseInteractor {

    void getRepayReceipt(int loanId);
}
