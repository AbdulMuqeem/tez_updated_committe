package com.tez.androidapp.app.general.feature.mobile.verification.callback;

/**
 * Created by VINOD KUMAR on 10/18/2018.
 */
@FunctionalInterface
public interface OnBackPressAtSinchRetryScreen {

    void onBackPressedAtSinchRetryScreen();
}
