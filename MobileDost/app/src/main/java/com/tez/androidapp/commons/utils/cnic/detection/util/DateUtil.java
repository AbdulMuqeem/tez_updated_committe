package com.tez.androidapp.commons.utils.cnic.detection.util;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;

import com.tez.androidapp.commons.utils.app.Utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class DateUtil {
    @SuppressLint("SimpleDateFormat")
    private static Date getFormattedDate(@NonNull String dateString, @NonNull String format) {
        try {
            return new SimpleDateFormat(format).parse(dateString);
        } catch (Exception e) {
            return null;
        }
    }

    public static Date getDateFromText(@NonNull String dateString) {
        List<String> possibleDateFormat = new ArrayList<>();
        possibleDateFormat.add("dd/MM/yyyy");
        possibleDateFormat.add("dd.MM.yyyy");
        possibleDateFormat.add("dd-MM-yyyy");
        for (String formatString : possibleDateFormat) {
            Date date = getFormattedDate(dateString, formatString);
            if (date != null) {
                return date;
            }
        }
        return null;
    }

    static List<Date> getSortedDatesFromListOfString(@NonNull List<String> blockData) {
        blockData = Utility.deleteDuplicatesFromList(blockData);
        List<Date> dateList = new ArrayList<>();
        for (String data : blockData) {
            Date date = getDateFromText(data);
            if (date != null) {
                dateList.add(date);
            }
        }
        Collections.sort(dateList);
        return dateList;
    }

    static List<Date> validateDateList(List<Date> dateList, int maxAgeOfUser, int maxValidDate) {
        ArrayList<Date> tempDateList = new ArrayList<>();
        for (Date date : dateList) {
            Calendar minimumCalendar = Calendar.getInstance();
            Calendar maximumCalendar = Calendar.getInstance();
            minimumCalendar.add(Calendar.YEAR, -maxAgeOfUser);
            maximumCalendar.add(Calendar.YEAR, maxValidDate);
            if (date.compareTo(minimumCalendar.getTime()) >= 0 && date.compareTo(maximumCalendar.getTime()) <= 0) {
                tempDateList.add(date);
            }
        }
        return tempDateList;
    }

    public static Calendar toCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }
}
