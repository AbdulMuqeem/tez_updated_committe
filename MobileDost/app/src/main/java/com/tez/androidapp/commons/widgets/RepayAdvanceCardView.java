package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.util.RepayAmountTextWatcher;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Rehman Murad Ali
 **/
public class RepayAdvanceCardView extends TezCardView {

    @BindView(R.id.etAmount)
    private TezEditTextView etAmount;

    @BindView(R.id.tvDueDateValue)
    private TezTextView tvDueDateValue;

    @BindView(R.id.tvOutstandingAmount)
    private TezTextView tvOutstandingAmount;

    @BindView(R.id.tvOutstandingAmountValue)
    private TezTextView tvOutstandingAmountValue;

    @BindView(R.id.tllAmount)
    private TezLinearLayout tllAmount;

    @BindView(R.id.tvDueDate)
    private TezTextView tvDueDate;

    public RepayAdvanceCardView(@NonNull Context context) {
        super(context);
    }

    public RepayAdvanceCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public RepayAdvanceCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    private void init(@NonNull Context context, @NonNull AttributeSet attrs) {
        inflate(context, R.layout.card_view_advance_repay_amount, this);
        ViewBinder.bind(this, this);
        tllAmount.setOnClickListener(v -> Utility.showKeyboard(etAmount));
        etAmount.addTextChangedListener(new RepayAmountTextWatcher(context, etAmount));
    }

    public double getAmount() {
        return Utility.getPatternAmount(etAmount.getValueText());
    }

    public void setOutstandingAmount(double amount) {
        this.tvOutstandingAmountValue.setText(getContext().getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabel(amount)));
    }

    public void setRepayDueDate(@NonNull String backendDate) {
        Date backendDateObject = Utility.getFormattedDateBackendWithZeroTime(backendDate);
        Date today = Utility.getZeroTimeDate(Calendar.getInstance().getTime());

        if (backendDateObject != null && backendDateObject.before(today)) {
            int colorRed = getColor(getContext(), R.color.textViewHeadingColorRed);
            tvDueDateValue.setTextColor(colorRed);
            tvDueDate.setTextColor(colorRed);
            tvOutstandingAmount.setTextColor(colorRed);
            tvOutstandingAmountValue.setTextColor(colorRed);
        }

        tvDueDateValue.setText(Utility.getFormattedDate(backendDate, Constants.INPUT_DATE_FORMAT, Constants.OUTPUT_DATE_FORMAT_DD_MMMM_YYYY));
    }
}
