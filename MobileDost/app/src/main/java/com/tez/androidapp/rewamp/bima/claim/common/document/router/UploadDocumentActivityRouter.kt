package com.tez.androidapp.rewamp.bima.claim.common.document.router

import android.content.Intent
import com.tez.androidapp.app.base.ui.fragments.BaseFragment
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.EvidenceFile
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter
import com.tez.androidapp.rewamp.bima.claim.common.document.view.UploadDocumentActivity
import java.util.*

class UploadDocumentActivityRouter : BaseActivityRouter() {
    fun setDependenciesAndRoute(from: BaseFragment,
                                docId: Int,
                                evidenceFileList: List<EvidenceFile>,
                                source: UploadDocumentActivity.Source) {
        val intent = Intent(from.context, UploadDocumentActivity::class.java)
        intent.putExtra(DOC_ID, docId)
        intent.putParcelableArrayListExtra(EVIDENCE_FILE_LIST, ArrayList(evidenceFileList))
        intent.putExtra(SOURCE, source)
        from.startActivityForResult(intent, REQUEST_CODE_UPLOAD_DOCUMENT_ACTIVITY)
    }

    class Dependencies(private val intent: Intent) {
        val docId: Int
            get() = intent.getIntExtra(DOC_ID, -100)
        val source: UploadDocumentActivity.Source
            get() = intent.getSerializableExtra(SOURCE) as UploadDocumentActivity.Source
        val evidenceFileList: MutableList<EvidenceFile>
            get() = intent.getParcelableArrayListExtra(EVIDENCE_FILE_LIST)!!
    }

    class Result(private val data: Intent) {
        val docId: Int
            get() = data.getIntExtra(DOC_ID, -100)
        val evidenceFileList: MutableList<EvidenceFile>
            get() = data.getParcelableArrayListExtra(EVIDENCE_FILE_LIST)!!
    }

    companion object {
        const val REQUEST_CODE_UPLOAD_DOCUMENT_ACTIVITY = 34709
        private const val EVIDENCE_FILE_LIST = "EVIDENCE_LIST"
        private const val DOC_ID = "DOC_ID"
        private const val SOURCE = "SOURCE"

        fun createResultIntent(docId: Int, evidenceFileList: List<EvidenceFile>): Intent {
            val intent = Intent()
            intent.putExtra(DOC_ID, docId)
            intent.putParcelableArrayListExtra(EVIDENCE_FILE_LIST, ArrayList(evidenceFileList))
            return intent
        }

        fun getDependencies(intent: Intent): Dependencies = Dependencies(intent)

        fun getResult(data: Intent): Result = Result(data)
    }
}