package com.tez.androidapp.rewamp.signup.verification;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by FARHAN DHANANI on 6/5/2019.
 */
public interface INumberVerificationRetryInteractor {
    void requestOtp(@NonNull String cnic, @NonNull String mobileNumber, @NonNull Context context);

    void sendFlashCallSuccessToServer(String cnic, String mobileNumber);

    void sendFlashCallFailureToServer(String cnic, String mobileNumber);

    void increaseFlashVerificaionFailureCountByOne();
}
