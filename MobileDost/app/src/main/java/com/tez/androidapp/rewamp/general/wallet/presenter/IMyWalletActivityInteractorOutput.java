package com.tez.androidapp.rewamp.general.wallet.presenter;

import com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks.DeleteWalletCallback;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks.GetAllWalletCallback;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks.SetDefaultWalletCallback;

public interface IMyWalletActivityInteractorOutput extends GetAllWalletCallback,
        SetDefaultWalletCallback,
        DeleteWalletCallback {
}
