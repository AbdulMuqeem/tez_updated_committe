package com.tez.androidapp.rewamp.signup.presenter;

import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.UserLoginCallback;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.UserVerifyCallback;
import com.tez.androidapp.repository.network.models.response.VerifyActiveDeviceResponse;

public interface IWelcomeBackActivityInteractorOutput extends UserLoginCallback,
        UserVerifyCallback {

    void onVerifyActiveDeviceSuccess(VerifyActiveDeviceResponse verifyActiveDeviceResponse, String pin);
    void onVerifyActiveDeviceFailure(int errorCode, String message);

}
