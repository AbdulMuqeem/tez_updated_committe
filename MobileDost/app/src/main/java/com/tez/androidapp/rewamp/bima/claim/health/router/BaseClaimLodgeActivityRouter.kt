package com.tez.androidapp.rewamp.bima.claim.health.router

import android.content.Intent
import com.tez.androidapp.app.base.ui.activities.BaseActivity
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter

abstract class BaseClaimLodgeActivityRouter : BaseActivityRouter() {

    class Dependencies constructor(private val intent: Intent) {

        val productId: Int
            get() = intent.getIntExtra(PRODUCT_ID, -100)

        val policyId: Int
            get() = intent.getIntExtra(POLICY_ID, -100)

        val activationDate: String
            get() = intent.getStringExtra(ACTIVATION_DATE)!!

        val expiryDate: String
            get() = intent.getStringExtra(EXPIRY_DATE)!!
    }

    fun setDependenciesAndRoute(from: BaseActivity,
                                productId: Int,
                                policyId: Int,
                                activationDate: String,
                                expiryDate: String) {
        val intent = createIntent(from)
        intent.putExtra(PRODUCT_ID, productId)
        intent.putExtra(POLICY_ID, policyId)
        intent.putExtra(ACTIVATION_DATE, activationDate)
        intent.putExtra(EXPIRY_DATE, expiryDate)
        route(from, intent)
    }

    abstract fun createIntent(from: BaseActivity): Intent

    companion object {
        private const val PRODUCT_ID = "PRODUCT_ID"
        private const val POLICY_ID = "POLICY_ID"
        private const val ACTIVATION_DATE = "ACTIVATION_DATE"
        private const val EXPIRY_DATE = "EXPIRY_DATE"

        fun getDependencies(intent: Intent): Dependencies = Dependencies(intent)
    }
}