package com.tez.androidapp.rewamp.profile.edit.presenter;

import android.view.View;

import androidx.annotation.NonNull;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateUserProfileRequest;
import com.tez.androidapp.commons.models.app.City;
import com.tez.androidapp.commons.models.network.User;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.rewamp.profile.edit.interactor.EditProfileActivityInteractor;
import com.tez.androidapp.rewamp.profile.edit.interactor.IEditProfileActivityInteractor;
import com.tez.androidapp.rewamp.profile.edit.view.IEditProfileActivityView;

import java.io.File;
import java.util.List;

public class EditProfileActivityPresenter implements
        IEditProfileActivityPresenter, IEditProfileActivityInteractorOutput {

    private final IEditProfileActivityInteractor iEditProfileActivityInteractor;
    private final IEditProfileActivityView iEditProfileActivityView;

    public EditProfileActivityPresenter(IEditProfileActivityView iEditProfileActivityView) {
        this.iEditProfileActivityView = iEditProfileActivityView;
        this.iEditProfileActivityInteractor = new EditProfileActivityInteractor(this);
    }

    @Override
    public void loadProfile() {
        this.iEditProfileActivityInteractor.getCities();
    }

    @Override
    public void setUserPicture(File file) {
        this.iEditProfileActivityView.showTezLoader();
        this.iEditProfileActivityInteractor.setUserPicture(file);
    }

    @Override
    public void onGetCitiesSuccess(@NonNull List<City> cities) {
        this.iEditProfileActivityView.populateDropDown(cities);
        this.iEditProfileActivityView.setDoubleTapSafeOnClickListenerToAutoCompleteTextViewCity();
        this.iEditProfileActivityView.disableCustomInputsOfUserOnAutoCompleteTextCurrentCity(cities);
    }

    @Override
    public void onGetCitiesError(int errorcode, String message) {
        this.iEditProfileActivityView.showError(errorcode, (d, v) ->
                this.iEditProfileActivityView.finishActivity());
    }

    @Override
    public void updateUserProfile(boolean isNumberchanged) {
        Optional.doWhen(isNumberchanged, this::validateUser,
                this::updateUserProfile);
    }

    @Override
    public void onGetUserProfileSuccess(@NonNull User user) {
        this.iEditProfileActivityView.setTextToEtMobileNumber(user.getMobileNumber());
        this.iEditProfileActivityView.setTextToEtEmail(user.getEmail());
        this.iEditProfileActivityView.setDoubleTapOnClickListenerOnBtContinue(user);
        this.iEditProfileActivityView.setDoubleTapSafeOnClickListenerOnTvEditPicture();
        Optional.ifPresent(user.getAddresses(), userAddresses -> {
            Optional.doWhen(userAddresses.size() >= 1
                            && userAddresses.get(0) != null,
                    () -> {
                        this.iEditProfileActivityView.setTextToEtCurrentAddress(userAddresses.get(0).getAddress());
                        Optional.ifPresent(userAddresses.get(0).getCity(),
                                this.iEditProfileActivityView::setCityTextToAutoCompleteTextViewCurrentCity);
                    });
        });
        this.iEditProfileActivityView.setVisibillityToTilMobileNumber(View.VISIBLE);
        this.iEditProfileActivityView.setVisibillityToTilEmail(View.VISIBLE);
        this.iEditProfileActivityView.setVisibillityToTilCurrentAddress(View.VISIBLE);
        this.iEditProfileActivityView.setVisibillityToAutoCompleteTextViewCurrentCity(View.VISIBLE);
        this.iEditProfileActivityView.setVisibillityToBtContinue(View.VISIBLE);
        this.iEditProfileActivityView.setVisibillityToTlShimmer(View.GONE);
    }

    @Override
    public void onGetUserProfileFailure(int errorCode, String message) {
        this.iEditProfileActivityView.showError(errorCode, (d, v) ->
                this.iEditProfileActivityView.finishActivity());
    }

    @Override
    public void onUpdateUserProfileSuccess() {
        this.iEditProfileActivityView.setTezLoaderToBeDissmissedOnTransition();
        this.iEditProfileActivityView.loadMyProfileActivity();
    }

    @Override
    public void onUpdateUserProfileFailure(int errorCode, String message) {
        this.iEditProfileActivityView.dismissTezLoader();
        this.iEditProfileActivityView.showError(errorCode,
                (d, v) -> this.iEditProfileActivityView.finishActivity());
    }

    @Override
    public void setPictureOnSuccess(File file) {
        this.iEditProfileActivityView.dismissTezLoader();
        this.iEditProfileActivityView.loadImageIntoIvUserImage(file);
    }

    @Override
    public void setPictureOnFailure(int errCode, String message) {
        this.iEditProfileActivityView.dismissTezLoader();
        this.iEditProfileActivityView.showError(errCode);
    }

    @Override
    public void onValidateInfoSuccess(@NonNull String mobileNumber, String userAddress, String userEmail, int userId) {
        this.iEditProfileActivityView.setTezLoaderToBeDissmissedOnTransition();
        this.iEditProfileActivityView.navigateToEditProfileVerificationActivity(mobileNumber,
                userAddress, userEmail, userId);
        this.iEditProfileActivityView.setBtContinueEnabled(true);
    }

    @Override
    public void onValidateInfoFailure(int errorCode, String message) {
        this.iEditProfileActivityView.dismissTezLoader();
        this.iEditProfileActivityView.showError(errorCode);
        this.iEditProfileActivityView.setBtContinueEnabled(true);
    }

    private void updateUserProfile() {
        UpdateUserProfileRequest updateUserProfileRequest = new UpdateUserProfileRequest();
        updateUserProfileRequest.setMobileNumber(this.iEditProfileActivityView.getTextFromEtMobileNumber());
        updateUserProfileRequest.setEmail(this.iEditProfileActivityView.getTextFromEtEmail());
        updateUserProfileRequest.setAddress(this.iEditProfileActivityView.getTextFromEtCurrentAddress());
        updateUserProfileRequest.setCityId(this.iEditProfileActivityView.getSelectedCityId());
        this.iEditProfileActivityInteractor.updateUserProfile(updateUserProfileRequest);
    }

    private void validateUser() {
        this.iEditProfileActivityInteractor.validateInfo(
                this.iEditProfileActivityView.getTextFromEtMobileNumber(),
                this.iEditProfileActivityView.getTextFromEtCurrentAddress(),
                this.iEditProfileActivityView.getTextFromEtEmail(),
                this.iEditProfileActivityView.getSelectedCityId());
    }
}
