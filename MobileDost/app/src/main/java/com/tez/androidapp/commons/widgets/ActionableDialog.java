package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;

import net.tez.fragment.util.listener.DoubleTapSafeDialogClickListener;

/**
 * Created by VINOD KUMAR on 7/23/2019.
 */
public class ActionableDialog extends TezDialog {

    @DrawableRes
    private int headerImageRes;

    private String title;

    private String message;

    @Nullable
    private String btOkText;

    @Nullable
    private String btCancelText;

    @Nullable
    private DoubleTapSafeDialogClickListener listener;

    public ActionableDialog(@NonNull Context context) {
        super(context);
    }

    public void setHeaderImageRes(int headerImageRes) {
        this.headerImageRes = headerImageRes;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setBtOkText(@Nullable String btOkText) {
        this.btOkText = btOkText;
    }

    public void setBtCancelText(@Nullable String btCancelText) {
        this.btCancelText = btCancelText;
    }

    public void setListener(@Nullable DoubleTapSafeDialogClickListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actionable_dialog);
        setCanceledOnTouchOutside(false);
        init();
    }

    private void init() {
        setHeaderImageVisibility();
        setTitle();
        setMessage();
        setBtOkText();
        setBtCancelText();
        setOkClickListener();
        setCancelClickListener();
    }

    private void setTitle() {
        TezTextView tvTitle = findViewById(R.id.tvAlertTitle);
        tvTitle.setText(title);
    }

    private void setMessage() {
        TezTextView tvMessage = findViewById(R.id.tvAlertMessage);
        tvMessage.setText(message);
    }

    private void setBtOkText() {
        TezButton button = findViewById(R.id.btOk);
        if (btOkText != null)
            button.setText(btOkText);
    }

    private void setBtCancelText() {
        TezTextView textView = findViewById(R.id.tvCancel);
        if (btCancelText != null)
            textView.setText(btCancelText);
    }

    private void setOkClickListener() {
        TezButton button = findViewById(R.id.btOk);
        button.setOnClickListener(view -> {
            cancel();
            if (listener != null)
                listener.onClick(this, BUTTON_POSITIVE);
        });
    }

    private void setCancelClickListener() {
        TezFrameLayout frameLayout = findViewById(R.id.flCancel);
        frameLayout.setOnClickListener(view -> {
            cancel();
            if (listener != null)
                listener.onClick(this, BUTTON_NEGATIVE);
        });
    }

    private void setHeaderImageVisibility() {
        boolean isValid = headerImageRes != 0 && headerImageRes != -1;
        TezImageView ivHeaderImage = findViewById(R.id.ivAlertHeaderImage);
        ivHeaderImage.setVisibility(isValid ? View.VISIBLE : View.GONE);
        if (isValid)
            ivHeaderImage.setImageResource(headerImageRes);
    }
}
