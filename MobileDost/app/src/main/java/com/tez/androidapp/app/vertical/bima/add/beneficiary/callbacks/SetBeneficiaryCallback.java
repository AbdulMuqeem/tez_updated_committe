package com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks;

/**
 * Created  on 8/25/2017.
 */

public interface SetBeneficiaryCallback {

    void onSetBeneficiarySuccess();

    void onSetBeneficiaryFailure(int statusCode, String message);
}
