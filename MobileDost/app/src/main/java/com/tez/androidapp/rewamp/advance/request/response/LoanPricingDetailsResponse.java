package com.tez.androidapp.rewamp.advance.request.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.rewamp.advance.request.entity.PricingDetail;

import java.util.List;

public class LoanPricingDetailsResponse extends BaseResponse {

    private List<PricingDetail> pricing;
    private Double latePaymentCharges;

    public Double getLatePaymentCharges() {
        return latePaymentCharges;
    }

    public void setLatePaymentCharges(Double latePaymentCharges) {
        this.latePaymentCharges = latePaymentCharges;
    }

    public List<PricingDetail> getPricing() {
        return pricing;
    }

    public void setPricing(List<PricingDetail> pricing) {
        this.pricing = pricing;
    }
}
