package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;
import com.tez.androidapp.commons.managers.MDPreferenceManager;

/**
 * Created by Rehman Murad Ali on 8/30/2017.
 */

public class GetUserPictureRequest extends BaseRequest {

    public static final String METHOD_NAME = MDPreferenceManager.getBaseURL() + "v1/user/account/picture";
}
