package com.tez.androidapp.rewamp;

import android.app.Activity;

import androidx.annotation.NonNull;

/**
 * Created by VINOD KUMAR on 7/25/2019.
 */
public final class CustomerSupport {

    private static final String EMAIL_ADDRESS = "info@tezfinancialservices.pk";
    private static final String HELPLINE_CONTACT_NUMBER = "+9242111839839";
    private static final String WHATSAPP_CONTACT_NUMBER = "923468229571";
    private static final String BIMA_WHATSAPP_CONTACT_NUMBER = "923458502361";
    private static final String FACEBOOK_USER_ID = "140128919861701";


    private CustomerSupport() {

    }

    public static void openEmail(@NonNull Activity activity) {
        AppOpener.openEmail(activity, EMAIL_ADDRESS);
    }

    public static void openDialer(@NonNull Activity activity) {
        AppOpener.openDialer(activity, HELPLINE_CONTACT_NUMBER);
    }

    public static void openWhatsapp(@NonNull Activity activity) {
        AppOpener.openWhatsapp(activity, WHATSAPP_CONTACT_NUMBER);
    }

    public static void openBimaWhatsapp(@NonNull Activity activity) {
        AppOpener.openWhatsapp(activity, BIMA_WHATSAPP_CONTACT_NUMBER);
    }

    public static void openMessenger(@NonNull Activity activity) {
        AppOpener.openMessenger(activity, Long.parseLong(FACEBOOK_USER_ID));
    }
}
