package com.tez.androidapp.rewamp.bima.products.policy.response;

public class InsuranceTenure {
    private int id;
    private String tenureTitle;
    private int premium;
    private int discount;
    private int tenureInDays;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenureTitle() {
        return tenureTitle;
    }

    public void setTenureTitle(String tenureTitle) {
        this.tenureTitle = tenureTitle;
    }

    public int getPremium() {
        return premium;
    }

    public void setPremium(int premium) {
        this.premium = premium;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getTenureInDays() {
        return tenureInDays;
    }

    public void setTenureInDays(int tenureInDays) {
        this.tenureInDays = tenureInDays;
    }
}
