package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto;

import androidx.annotation.NonNull;

import java.util.List;

public class Question {

    private String refName;
    private String text;
    private int index = 0;
    private boolean lastIfNoRuleMatches;
    private String answerRefName;
    private List<Answer> answers;
    private List<Rule> rules;
    public List<Rule> getRules() {
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    public String getRefName() {
        return refName;
    }

    public void setRefName(String refName) {
        this.refName = refName;
    }

    public String getText() {
        return text;
    }

    public boolean isRulesExist(){
        return rules!=null && rules.size()>0;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public String getAnswerRefName() {
        return answerRefName;
    }

    public void setAnswerRefName(String answerRefName) {
        this.answerRefName = answerRefName;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isLastIfNoRuleMatches() {
        return lastIfNoRuleMatches;
    }

    public void setLastIfNoRuleMatches(boolean lastIfNoRuleMatches) {
        this.lastIfNoRuleMatches = lastIfNoRuleMatches;
    }


    @NonNull
    @Override
    public String toString() {
        return text;
    }
}
