package com.tez.androidapp.rewamp.profile.edit.presenter;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateUserProfileRequest;

import java.io.File;

public interface IEditProfileActivityPresenter {
    void loadProfile();

    void setUserPicture(File file);

    void updateUserProfile(boolean isNumberChanged);
}
