package com.tez.androidapp.rewamp.bima.claim.response

import com.tez.androidapp.app.base.response.BaseResponse

class LodgeClaimResponse(val claimId: Int) : BaseResponse()