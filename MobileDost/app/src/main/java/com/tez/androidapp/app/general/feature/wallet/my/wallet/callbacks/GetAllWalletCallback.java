package com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks;

import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;

import java.util.List;

/**
 * Created by Rehman Murad Ali on 12/14/2017.
 */

public interface GetAllWalletCallback {

    void onGetAllWalletSuccess(List<Wallet> wallets);

    void onGetAllWalletFailure(int errorCode,String message);
}
