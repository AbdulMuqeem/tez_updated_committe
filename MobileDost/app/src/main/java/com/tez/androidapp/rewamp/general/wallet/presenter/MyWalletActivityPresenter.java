package com.tez.androidapp.rewamp.general.wallet.presenter;

import android.view.View;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.rewamp.general.wallet.interactor.IMyWalletActivityInteractor;
import com.tez.androidapp.rewamp.general.wallet.interactor.MyWalletActivityInteractor;
import com.tez.androidapp.rewamp.general.wallet.view.IMyWalletActivityView;

import java.util.List;


public class MyWalletActivityPresenter implements IMyWalletActivityPresenter, IMyWalletActivityInteractorOutput {

    private final IMyWalletActivityView iMyWalletActivityView;
    private final IMyWalletActivityInteractor iMyWalletActivityInteractor;

    public MyWalletActivityPresenter(IMyWalletActivityView iMyWalletActivityView) {
        this.iMyWalletActivityView = iMyWalletActivityView;
        iMyWalletActivityInteractor = new MyWalletActivityInteractor(this);
    }

    @Override
    public void getAllWallets() {
        iMyWalletActivityView.setNsvContentVisibility(View.GONE);
        iMyWalletActivityView.showShimmer();
        iMyWalletActivityInteractor.getAllWallets();
    }

    @Override
    public void onGetAllWalletSuccess(List<Wallet> wallets) {
        iMyWalletActivityView.resetCards();
        for (Wallet wallet : wallets)
            iMyWalletActivityView.setWalletCardState(wallet);
        iMyWalletActivityView.setLayoutAccountDetailVisibility(View.GONE);
        iMyWalletActivityView.setEmptyWalletsVisibility(wallets.isEmpty());
        iMyWalletActivityView.dismissTezLoader();
        iMyWalletActivityView.hideShimmer();
    }

    @Override
    public void onGetAllWalletFailure(int errorCode, String message) {
        iMyWalletActivityView.dismissTezLoader();
        iMyWalletActivityView.hideShimmer();
        iMyWalletActivityView.showError(errorCode, (dialog, which) -> iMyWalletActivityView.finishActivity());
    }

    @Override
    public void setDefaultWallet(@NonNull Wallet wallet) {
        iMyWalletActivityView.showTezLoader();
        iMyWalletActivityInteractor.setDefaultWallet(wallet);
    }

    @Override
    public void setDefaultWalletSuccess(BaseResponse baseResponse) {
        iMyWalletActivityInteractor.getAllWallets();
    }

    @Override
    public void setDefaultWalletFailure(int errorCode, String message) {
        iMyWalletActivityView.dismissTezLoader();
        iMyWalletActivityView.showError(errorCode);
    }

    @Override
    public void deleteWallet(int mobileAccountId) {
        iMyWalletActivityView.showTezLoader();
        iMyWalletActivityInteractor.deleteWallet(mobileAccountId);
    }

    @Override
    public void onDeleteWalletSuccess(BaseResponse response) {
        iMyWalletActivityInteractor.getAllWallets();
    }

    @Override
    public void onDeleteWalletFailure(int errorCode, String message) {
        iMyWalletActivityView.dismissTezLoader();
        iMyWalletActivityView.showError(errorCode);
    }
}
