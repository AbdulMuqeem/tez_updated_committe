package com.tez.androidapp.rewamp.profile.trust.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.profile.trust.view.CompleteYourProfileActivity;

public class CompleteYourProfileActivityRouter extends BaseActivityRouter {

    public static CompleteYourProfileActivityRouter createInstance() {
        return new CompleteYourProfileActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = new Intent(from, CompleteYourProfileActivity.class);
        route(from, intent);
    }
}
