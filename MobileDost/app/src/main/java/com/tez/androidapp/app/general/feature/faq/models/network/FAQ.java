package com.tez.androidapp.app.general.feature.faq.models.network;

import net.tez.tezrecycler.model.GenericDropDownListModel;

import java.io.Serializable;

/**
 * Created  on 2/28/2017.
 */

public class FAQ extends GenericDropDownListModel implements Serializable {

    private String title;
    private String description;

    public FAQ() {
    }

    public FAQ(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "FAQ{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", isOpen=" + isOpen() +
                '}';
    }
}
