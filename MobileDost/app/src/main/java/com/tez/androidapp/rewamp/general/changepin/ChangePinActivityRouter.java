package com.tez.androidapp.rewamp.general.changepin;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.router.ChangeLanguageActivityRouter;

/**
 * Created by VINOD KUMAR on 8/26/2019.
 */
public class ChangePinActivityRouter extends BaseActivityRouter {

    public static ChangePinActivityRouter createInstance() {
        return new ChangePinActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, new Intent(from, ChangePinActivity.class));
    }
}
