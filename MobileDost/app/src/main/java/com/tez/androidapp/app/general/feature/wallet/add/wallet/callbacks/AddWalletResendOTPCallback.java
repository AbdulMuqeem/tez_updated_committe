package com.tez.androidapp.app.general.feature.wallet.add.wallet.callbacks;

/**
 * Created  on 2/14/2017.
 */

public interface AddWalletResendOTPCallback {

    void onAddWalletResendOTPSuccess();

    void onAddWalletResendOTPFailure(int errorCode, String message);
}
