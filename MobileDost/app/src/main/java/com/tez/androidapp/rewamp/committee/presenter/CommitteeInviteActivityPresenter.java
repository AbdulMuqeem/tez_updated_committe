package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.interactor.CommitteeInviteActivityInteractor;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeInviteResponse;
import com.tez.androidapp.rewamp.committee.view.ICommitteeInviteActivityView;

import java.util.List;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeInviteActivityPresenter implements ICommitteeInviteActivityPresenter, ICommitteeInviteActivityInteractorOutput {

    private final ICommitteeInviteActivityView iCommitteeInviteActivityView;
    private final CommitteeInviteActivityInteractor committeeInviteActivityInteractor;

    public CommitteeInviteActivityPresenter(ICommitteeInviteActivityView iCommitteeInviteActivityView) {
        this.iCommitteeInviteActivityView = iCommitteeInviteActivityView;
        committeeInviteActivityInteractor = new CommitteeInviteActivityInteractor(this);
    }

    @Override
    public void getInvite(List<CommitteeInviteRequest> committeeInviteRequest) {
        iCommitteeInviteActivityView.showLoader();
        committeeInviteActivityInteractor.getInvite(committeeInviteRequest);
    }

    @Override
    public void onInviteSuccess(CommitteeInviteResponse committeeInviteResponse) {
        iCommitteeInviteActivityView.hideLoader();
        iCommitteeInviteActivityView.onInvite();
    }

    @Override
    public void onInviteFailure(int errorCode, String message) {
        iCommitteeInviteActivityView.hideLoader();
        iCommitteeInviteActivityView.showError(errorCode,
                (dialog, which) -> iCommitteeInviteActivityView.finishActivity());
    }

}
