package com.tez.androidapp.services;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.tez.androidapp.services.models.TezTask;

public interface ITezDynamicLinks<I, O> {

    @NonNull
    TezTask<I, O> getDynamicLink(@NonNull Activity activity, @NonNull Intent intent);

    @NonNull
    TezDynamicLinks.Builder createDynamicLink();

    interface Builder<I, O> {

        TezDynamicLinks.Builder setDomainUriPrefix(String s);

        TezDynamicLinks.Builder setLink(Uri link);

        TezTask<I, O> buildShortDynamicLink();
    }
}
