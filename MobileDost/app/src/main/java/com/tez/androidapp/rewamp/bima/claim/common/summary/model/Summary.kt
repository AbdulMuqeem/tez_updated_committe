package com.tez.androidapp.rewamp.bima.claim.common.summary.model

import androidx.annotation.DrawableRes

data class Summary(val question: String,
                   val answer: String,
                   @DrawableRes val icon: Int)