package com.tez.androidapp.commons.utils.file.util;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;

/**
 * Created by FARHAN DHANANI on 7/16/2018.
 */
public interface IDocumentDownloader {
    static Intent createIntentForDownloadCompletedBroadCastReceiver(@NonNull String fileUri) {
        String mimeType = Utility.getMimeTypeFromLocalFileUri(fileUri);
        final Intent intentDocumentDownloadedSuccessfully = new Intent("");
        intentDocumentDownloadedSuccessfully.putExtra(Constants.STRING_DOCUMENT_URI, fileUri);
        intentDocumentDownloadedSuccessfully.putExtra(Constants.STRING_DOCUMENT_MIME_TYPE, mimeType);
        return intentDocumentDownloadedSuccessfully;
    }

    @NonNull
    Uri getFileUrl();

    @NonNull
    String getFileTitle();

    @Nullable
    default String getSignature() {
        return null;
    }

    default boolean getAuthorization() {
        return true;
    }

    default String getDestinationFileDirectory() {
        return Environment.DIRECTORY_DOWNLOADS;
    }

    @Nullable
    default String getFileDescription() {
        return null;
    }

    default Intent getIntentForDownloadCompletedBroadCastReceiver(@NonNull String fileUri) {
        return createIntentForDownloadCompletedBroadCastReceiver(fileUri);
    }

    default boolean showNotification() {
        return true;
    }

    void downloadDocuments(DownloadDocumentsCallBack downloadDocumentsCallBack);
}
