package com.tez.androidapp.app.vertical.advance.loan.confirmation.callbacks;

import com.tez.androidapp.commons.models.network.LoanDetails;

/**
 * Created by Rehman Murad Ali on 9/14/2017.
 */

public interface GenerateDisbursementReceiptCallback {

    void onGenerateDisbursementReceiptSuccess(LoanDetails loanDetails);

    void onGenerateDisbursementReceiptFailure(int errorCode, String message);
}
