package com.tez.androidapp.rewamp.general.beneficiary.adapter.viewholder;

import android.view.View;

import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.BeneficiaryCardView;
import com.tez.androidapp.rewamp.general.beneficiary.adapter.BeneficiaryListAdapter;
import com.tez.androidapp.rewamp.general.beneficiary.entity.Beneficiary;

import net.tez.fragment.util.optional.Optional;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public abstract class BaseBeneficiaryViewHolder extends BaseViewHolder<Beneficiary, BeneficiaryListAdapter.BeneficiaryListener> {

    @BindView(R.id.cvBeneficiary)
    BeneficiaryCardView cvBeneficiary;

    BeneficiaryListAdapter adapter;


    BaseBeneficiaryViewHolder(View itemView, BeneficiaryListAdapter adapter) {
        super(itemView);
        ViewBinder.bind(this, itemView);
        this.adapter = adapter;
    }

    @Override
    public void onBind(int position, List<Beneficiary> items, @Nullable BeneficiaryListAdapter.BeneficiaryListener listener) {
        super.onBind(position, items, listener);
        Beneficiary item = items.get(position);
        cvBeneficiary.setName(item.getName());
        cvBeneficiary.setRelation(item.getRelationshipId());
        cvBeneficiary.setNumber(item.getMobileNumber());
        cvBeneficiary.setRelationIcon(item.getRelationshipId());
    }
}
