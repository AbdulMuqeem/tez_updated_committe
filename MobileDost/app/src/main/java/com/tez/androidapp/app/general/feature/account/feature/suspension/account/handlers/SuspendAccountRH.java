package com.tez.androidapp.app.general.feature.account.feature.suspension.account.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.SuspendAccountCallback;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created by Rehman Murad Ali on 8/22/2017.
 */

public class SuspendAccountRH extends BaseRH<BaseResponse> {

    SuspendAccountCallback suspendAccountCallback;

    public SuspendAccountRH(BaseCloudDataStore baseCloudDataStore, @Nullable SuspendAccountCallback suspendAccountCallback) {
        super(baseCloudDataStore);
        this.suspendAccountCallback = suspendAccountCallback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse baseResponse = value.response().body();
        if (baseResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            suspendAccountCallback.onSuspendAccountSuccess(value.response().body());
        else onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (suspendAccountCallback != null) suspendAccountCallback.onSuspendAccountFailure(errorCode, message);
    }

}
