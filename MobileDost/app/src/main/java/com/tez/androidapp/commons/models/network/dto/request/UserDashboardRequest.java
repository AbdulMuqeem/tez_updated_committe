package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 9/9/2017.
 */

public class UserDashboardRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/dashboard";
}
