package com.tez.androidapp.rewamp.signup;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.fragments.BaseFragment;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;

/**
 * Created by VINOD KUMAR on 5/28/2019.
 */
public class OnboardingContentFragment extends BaseFragment {

    private static final String ONBOARDING_CONTENT = "ONBOARDING_CONTENT";
    private OnboardingContent onboardingContent;

    public static OnboardingContentFragment newInstance(@NonNull OnboardingContent onboardingContent) {
        OnboardingContentFragment fragment = new OnboardingContentFragment();
        fragment.onboardingContent = onboardingContent;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_onboarding_content, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TezImageView ivIllustration = view.findViewById(R.id.ivIllustration);
        TezTextView tvTitle = view.findViewById(R.id.tvTitle);
        TezTextView tvMessage = view.findViewById(R.id.tvMessage);

        if (onboardingContent == null && savedInstanceState != null && savedInstanceState.containsKey(ONBOARDING_CONTENT))
            onboardingContent = (OnboardingContent) savedInstanceState.getSerializable(ONBOARDING_CONTENT);

        if (onboardingContent != null) {
            ivIllustration.setImageResource(onboardingContent.getIllustration());
            tvTitle.setText(onboardingContent.getTitle());
            tvMessage.setText(onboardingContent.getMessage());
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable(ONBOARDING_CONTENT, onboardingContent);
    }
}
