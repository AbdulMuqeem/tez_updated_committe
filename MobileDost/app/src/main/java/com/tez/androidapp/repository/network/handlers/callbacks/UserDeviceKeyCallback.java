package com.tez.androidapp.repository.network.handlers.callbacks;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created by Rehman Murad Ali on 8/30/2017.
 */

public interface UserDeviceKeyCallback {

    void onUserDeviceKeySuccess(BaseResponse baseResponse);

    void onUserDeviceKeyFailure(int errorCode, String message);

}
