
package com.tez.androidapp.rewamp.committee.response;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class ChatMessage {

    @Expose
    private Long committeeId;
    @Expose
    private String committeeTitle;
    @Expose
    private Long id;
    @Expose
    private String text;
    @Expose
    private Long userId;
    @Expose
    private String userName;
    @Expose
    private String date;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getCommitteeId() {
        return committeeId;
    }

    public void setCommitteeId(Long committeeId) {
        this.committeeId = committeeId;
    }

    public String getCommitteeTitle() {
        return committeeTitle;
    }

    public void setCommitteeTitle(String committeeTitle) {
        this.committeeTitle = committeeTitle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

}
