package com.tez.androidapp.rewamp.dashboard.view;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.rewamp.dashboard.entity.Action;
import com.tez.androidapp.rewamp.dashboard.entity.Advance;
import com.tez.androidapp.rewamp.dashboard.entity.LoanStatusDto;

public interface IDashboardActivityView extends IBaseView {

    void enableRefresh();

    void setDashboardAdvanceCardStatus(Advance advance);

    void removeCvAdvanceCardListener();

    void setCvAdvanceCardListener(@NonNull LoanStatusDto loanStatusDto);

    void setLoanIdForActionCard(@Nullable Integer loanId);

    void setProductsShimmerContainerVisibility(int visibility);

    void setProductsContainerVisibility(int visibility);

    void setTvProfileStatusSuccess(String text, @DrawableRes int end);

    void setTvProfileStatusError();

    void setCvDashboardActionVisibility(int visibility);

    void setDashboardActionState(@NonNull Action action);

    void setCvDashboardAdvanceDetailVisibility(int visibility);

    void setShimmerDashboardAdvanceDetailVisibility(int visibility);

    void setErrorDashboardAdvanceDetailVisibility(int visibility);

    void setShimmerDashboardActionCardVisibility(int visibility);

    void setErrorDashboardActionCardVisibility(int visibility);
}
