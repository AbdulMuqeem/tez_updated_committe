package com.tez.androidapp.rewamp.bima.claim.model

import android.os.Parcel
import android.os.Parcelable

data class InsuranceClaimConfirmationDto(
        val policyId: Int,
        val policyName: String,
        val policyTag: String,
        val claimAmount: Double,
        val claimDate: String,
        val claimNumber: String,
        val providerCode: String,
) : Parcelable {
    private constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readDouble(),
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readString()!!,
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(policyId)
        parcel.writeString(policyName)
        parcel.writeString(policyTag)
        parcel.writeDouble(claimAmount)
        parcel.writeString(claimDate)
        parcel.writeString(claimNumber)
        parcel.writeString(providerCode)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<InsuranceClaimConfirmationDto> {
        override fun createFromParcel(parcel: Parcel): InsuranceClaimConfirmationDto =
                InsuranceClaimConfirmationDto(parcel)

        override fun newArray(size: Int): Array<InsuranceClaimConfirmationDto?> = arrayOfNulls(size)
    }
}