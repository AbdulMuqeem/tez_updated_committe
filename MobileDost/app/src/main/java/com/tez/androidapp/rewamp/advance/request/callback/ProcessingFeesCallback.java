package com.tez.androidapp.rewamp.advance.request.callback;

import com.tez.androidapp.rewamp.advance.request.entity.ProcessingFee;

import java.util.List;

public interface ProcessingFeesCallback {

    void onProcessingFeesSuccess(List<ProcessingFee> processingFees);

    void onProcessingFeesFailure(int errorCode, String message);
}
