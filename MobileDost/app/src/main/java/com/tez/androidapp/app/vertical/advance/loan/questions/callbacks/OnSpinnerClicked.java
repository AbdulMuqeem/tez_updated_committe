package com.tez.androidapp.app.vertical.advance.loan.questions.callbacks;

/**
 * Created by VINOD KUMAR on 10/1/2018.
 */
public interface OnSpinnerClicked {

    void onSpinnerClicked();
}
