package com.tez.androidapp.rewamp.general.purpose;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.tez.androidapp.R;

public class Purpose {

    private int id;

    @DrawableRes
    private int iconRes;

    @StringRes
    private int titleRes;

    @Nullable
    private String other;

    public Purpose(int id, int iconRes, int titleRes) {
        this.id = id;
        this.iconRes = iconRes;
        this.titleRes = titleRes;
    }

    public Purpose(int id, int titleRes) {
        this.id = id;
        this.iconRes = R.drawable.ic_bill;
        this.titleRes = titleRes;
    }

    public int getId() {
        return id;
    }

    int getIconRes() {
        return iconRes;
    }

    public int getTitleRes() {
        return titleRes;
    }

    @Nullable
    public String getOther() {
        return other;
    }

    public void setOther(@Nullable String other) {
        this.other = other;
    }
}
