package com.tez.androidapp.rewamp.advance.limit.view;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.rewamp.general.timer.ITimerActivityView;

public interface ICheckYourLimitActivityView extends IBaseView, ITimerActivityView {

    void startExtractionIntentServiceToUploadUserData();

    void sendLimitFailureNotification();

    void onDeviceNotValidError(int errorCode);

    void onCnicExpiredError(int errorCode);
}
