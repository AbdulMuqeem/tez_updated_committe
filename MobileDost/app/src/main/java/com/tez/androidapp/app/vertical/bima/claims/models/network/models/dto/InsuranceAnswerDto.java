package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto;

import com.tez.androidapp.app.vertical.bima.claims.models.InsuranceAnswerResponseDto;

import java.io.Serializable;

/**
 * Created by FARHAN DHANANI on 6/21/2018.
 */
public class InsuranceAnswerDto implements Serializable {
    private Integer insuranceQuestionResponseId;
    private QuestionDto questionDto;
    private String status;
    private InsuranceAnswerResponseDto insuranceAnswerResponseDto;

    public InsuranceAnswerDto(Integer questionId, InsuranceAnswerResponseDto insuranceAnswerResponseDto) {
        this.questionDto = new QuestionDto(questionId);
        this.insuranceAnswerResponseDto = insuranceAnswerResponseDto;
    }

    public InsuranceAnswerDto(Integer questionId, int insuranceQuestionResponseId, InsuranceAnswerResponseDto insuranceAnswerResponseDto) {
        this.questionDto = new QuestionDto(questionId);
        this.insuranceAnswerResponseDto = insuranceAnswerResponseDto;
        this.insuranceQuestionResponseId = insuranceQuestionResponseId;
    }

    public String getStatus() {
        return status;
    }

    public QuestionDto getQuestionDto() {
        return questionDto;
    }

    public InsuranceAnswerResponseDto getInsuranceAnswerResponseDto() {
        return insuranceAnswerResponseDto.getInsuranceAnswerResponseDto(getQuestionDto().getQuestionType());
    }

    public Integer getInsuranceQuestionResponseId() {
        return insuranceQuestionResponseId;
    }
}
