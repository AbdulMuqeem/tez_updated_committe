package com.tez.androidapp.rewamp.bima.products.policy.view;

import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.bima.products.policy.router.BaseProductActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.ProductRequiredTermConditionsActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.router.PurchasePolicyActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.OnClick;
import net.tez.viewbinder.library.core.ViewBinder;

public abstract class BaseProductActivity extends BaseActivity {

    @BindView(R.id.tvHeading)
    private TezTextView tvHeading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        ViewBinder.bind(this);
        tvHeading.setText(getProductTitle());
    }

    @OnClick(R.id.btOkay)
    private void onClickBtOkay() {
        PurchasePolicyActivityRouter.createInstance().setDependenciesAndRoute(this, getProductId(), getProductTitle(), false);
    }

    @OnClick(R.id.tvTermsAndCondition)
    private void onClickTermsAndCondition() {
        ProductRequiredTermConditionsActivityRouter.createInstance().setDependenciesAndRoute(this, getProductId(), getProductTitle());
    }

    private String getProductTitle() {
        return getIntent().getStringExtra(BaseProductActivityRouter.TITLE);
    }

    @LayoutRes
    protected abstract int getContentView();

    protected abstract int getProductId();
}
