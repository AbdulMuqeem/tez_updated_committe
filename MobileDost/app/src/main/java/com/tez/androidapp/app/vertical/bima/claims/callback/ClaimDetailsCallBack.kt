package com.tez.androidapp.app.vertical.bima.claims.callback

import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.ClaimDetailDto

/**
 * Created by Vinod Kumar on 10/19/2020.
 */
interface ClaimDetailsCallBack {

    fun onMyClaimSuccess(claimDetailDto: ClaimDetailDto)

    fun onMyClaimFailure(errorCode: Int, message: String?)
}