package com.tez.androidapp.rewamp.general.beneficiary.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks.SetBeneficiaryCallback;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks.SetDefaultBeneficiaryCallback;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.models.network.dto.request.BeneficiaryRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BimaCloudDataStore;
import com.tez.androidapp.rewamp.general.beneficiary.listener.GetBeneficiariesListener;
import com.tez.androidapp.rewamp.general.beneficiary.presenter.IBeneficiaryListActivityInteractorOutput;
import com.tez.androidapp.rewamp.general.beneficiary.response.BeneficiariesResponse;

public class BeneficiaryListActivityInteractor implements IBeneficiaryListActivityInteractor {

    private final IBeneficiaryListActivityInteractorOutput iBeneficiaryListActivityInteractorOutput;

    public BeneficiaryListActivityInteractor(IBeneficiaryListActivityInteractorOutput iBeneficiaryListActivityInteractorOutput) {
        this.iBeneficiaryListActivityInteractorOutput = iBeneficiaryListActivityInteractorOutput;
    }

    @Override
    public void getBeneficiaries() {
        BimaCloudDataStore.getInstance().getBeneficiaries(new GetBeneficiariesListener() {
            @Override
            public void onGetBeneficiariesSuccess(BeneficiariesResponse beneficiariesResponse) {
                iBeneficiaryListActivityInteractorOutput.onGetBeneficiariesSuccess(beneficiariesResponse);
            }

            @Override
            public void onGetBeneficiariesFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getBeneficiaries();
                else
                    iBeneficiaryListActivityInteractorOutput.onGetBeneficiariesFailure(errorCode, message);

            }
        });
    }

    @Override
    public void setAdvanceBimaBeneficiary(@NonNull BeneficiaryRequest request) {
        BimaCloudDataStore.getInstance().setBeneficiary(request, new SetBeneficiaryCallback() {
            @Override
            public void onSetBeneficiarySuccess() {
                iBeneficiaryListActivityInteractorOutput.onSetBeneficiarySuccess();
            }

            @Override
            public void onSetBeneficiaryFailure(int statusCode, String message) {
                if (Utility.isUnauthorized(statusCode))
                    setAdvanceBimaBeneficiary(request);
                else
                    iBeneficiaryListActivityInteractorOutput.onSetBeneficiaryFailure(statusCode, message);
            }
        });
    }

    @Override
    public void setDefaultBeneficiary(int beneficiaryId) {
        BimaCloudDataStore.getInstance().setDefaultBeneficiary(beneficiaryId, new SetDefaultBeneficiaryCallback() {
            @Override
            public void onSetDefaultBeneficiarySuccess() {
                iBeneficiaryListActivityInteractorOutput.onSetDefaultBeneficiarySuccess();
            }

            @Override
            public void onSetDefaultBeneficiaryFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    setDefaultBeneficiary(beneficiaryId);
                else
                    iBeneficiaryListActivityInteractorOutput.onSetDefaultBeneficiaryFailure(errorCode, message);
            }
        });
    }
}
