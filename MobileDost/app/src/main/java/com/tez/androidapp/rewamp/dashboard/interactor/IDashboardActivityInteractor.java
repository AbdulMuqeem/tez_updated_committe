package com.tez.androidapp.rewamp.dashboard.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;

public interface IDashboardActivityInteractor extends IBaseInteractor {

    void getDashboardAdvanceCard();

    void getDashboardActionCard();

    void getUserProfileStatus();
}
