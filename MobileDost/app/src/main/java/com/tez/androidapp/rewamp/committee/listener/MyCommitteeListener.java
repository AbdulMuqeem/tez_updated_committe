package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.MyCommitteeResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface MyCommitteeListener {

    void onMyCommitteeSuccess(MyCommitteeResponse myCommitteeResponse);

    void onMyCommitteeFailure(int errorCode, String message);
}
