package com.tez.androidapp.rewamp.bima.claim.health.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tez.androidapp.R
import com.tez.androidapp.app.general.feature.questions.models.network.models.ClaimQuestions
import com.tez.androidapp.app.general.feature.questions.models.network.models.DateQuestion
import com.tez.androidapp.app.general.feature.questions.models.network.models.LocationQuestion
import com.tez.androidapp.commons.utils.app.Constants
import com.tez.androidapp.commons.utils.app.Utility

class DetailsViewModel : ViewModel() {
    val questionListLiveData: MutableLiveData<ClaimQuestions> = MutableLiveData(
            ClaimQuestions(
                    dateOfAdmission = DateQuestion(question = Utility.getStringFromResource(R.string.date_of_admission)),
                    dateOfDischarge = DateQuestion(question = Utility.getStringFromResource(R.string.date_of_discharge)),
                    locationOfHospital = LocationQuestion(question = Utility.getStringFromResource(R.string.location_of_hospital)),
            )
    )

    fun validate(activationDate: String,
                 expiryDate: String,
                 admissionDate: String,
                 dischargeDate: String,
                 nameOfHospital: String): Pair<Int, Int>? {
        when {

            admissionDate.isEmpty() -> {
                return Pair(1, R.string.admission_date_empty_error)
            }

            Utility.compareDates(admissionDate,
                    activationDate,
                    Constants.UI_DATE_FORMAT) == -1 -> {
                return Pair(1, R.string.code_3007)
            }

            Utility.compareDates(admissionDate,
                    Utility.getCurrentDateInUIFormat(),
                    Constants.UI_DATE_FORMAT) == 1 -> {
                return Pair(1, R.string.code_3008)
            }

            Utility.compareDates(admissionDate,
                    expiryDate,
                    Constants.UI_DATE_FORMAT) == 1 -> {
                return Pair(1, R.string.code_3009)
            }

            dischargeDate.isEmpty() -> {
                return Pair(2, R.string.discharge_date_empty_error)
            }

            Utility.compareDates(dischargeDate,
                    admissionDate,
                    Constants.UI_DATE_FORMAT) == -1 -> {
                return Pair(2, R.string.code_3010)
            }

            Utility.compareDates(dischargeDate,
                    Utility.getCurrentDateInUIFormat(),
                    Constants.UI_DATE_FORMAT) == 1 -> {
                return Pair(2, R.string.code_3011)
            }

            nameOfHospital.isEmpty() -> {
                return Pair(3, R.string.name_of_hospital_empty_error)
            }

            else -> return null
        }
    }
}