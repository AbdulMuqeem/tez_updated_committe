package com.tez.androidapp.rewamp.profile.edit.presenter;

import androidx.annotation.NonNull;

public interface IEditProfileVerificationActivityPresenter {
    void validateUser(@NonNull String mobileNumber);

    void updateUserProfile();
}
