package com.tez.androidapp.rewamp.advance.limit.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.limit.view.LimitAssignedActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

public class LimitAssignedActivityRouter extends BaseActivityRouter {

    public static LimitAssignedActivityRouter createInstance() {
        return new LimitAssignedActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    public void setDependenciesAndRouteWithClearAndNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, LimitAssignedActivity.class);
    }
}
