package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.rewamp.TezDrawableHelper;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

/**
 * Created by VINOD KUMAR on 7/11/2019.
 */
public class WalletCardView extends TezCardView {

    @BindView(R.id.cbWalletCheck)
    private TezCheckBox cbWalletCheck;

    @BindView(R.id.ivWalletLogo)
    private TezImageView ivWalletLogo;

    @BindView(R.id.clContent)
    private TezConstraintLayout clContent;

    private boolean isSelected;

    private Wallet wallet;

    private Drawable inActiveImg;
    private Drawable activeImg;

    @Nullable
    private WalletSelectedListener onWalletCheckChangeListener;


    public WalletCardView(@NonNull Context context) {
        super(context);
    }

    public WalletCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public WalletCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    private void init(@NonNull Context context, @NonNull AttributeSet attrs) {
        inflate(context, R.layout.card_view_wallet, this);
        ViewBinder.bind(this, this);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.WalletCardView);

        try {

            this.inActiveImg = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.WalletCardView_inactive_img);
            this.activeImg = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.WalletCardView_active_img);

            this.setCardType(CardType.INACTIVE);


        } finally {
            typedArray.recycle();
        }
    }

    public void setActiveImg(Drawable activeImg) {
        this.activeImg = activeImg;
    }

    public void setInActiveImg(Drawable inActiveImg) {
        this.inActiveImg = inActiveImg;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public void setCardType(CardType state) {
        switch (state) {

            case INACTIVE:
                this.ivWalletLogo.setImageDrawable(inActiveImg);
                this.cbWalletCheck.setVisibility(GONE);
                break;

            case ACTIVE:
                this.ivWalletLogo.setImageDrawable(activeImg);
                this.cbWalletCheck.setVisibility(GONE);
                break;

            case DEFAULT:
                this.ivWalletLogo.setImageDrawable(activeImg);
                this.cbWalletCheck.setVisibility(VISIBLE);
                break;
        }

        this.setOnClickListener(view -> {
            if (onWalletCheckChangeListener != null)
                onWalletCheckChangeListener.onWalletSelected(view.getId(), wallet, state);
        });
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
        this.clContent.setBackgroundResource(selected ? R.drawable.wallet_selected_bg : 0);
        this.ivWalletLogo.setImageDrawable(selected || wallet != null ? activeImg : inActiveImg);
    }

    @Override
    public boolean isSelected() {
        return isSelected;
    }

    public void setOnWalletSelectedListener(@Nullable WalletSelectedListener listener) {
        this.onWalletCheckChangeListener = listener;
    }

    public interface WalletSelectedListener {

        void onWalletSelected(@IdRes int viewId, @Nullable Wallet wallet, CardType state);
    }

    public enum CardType {
        INACTIVE,
        ACTIVE,
        DEFAULT
    }
}
