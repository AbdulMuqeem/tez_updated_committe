package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Rehman Murad Ali on 8/25/2017.
 */

public class SetUserPictureRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/account/picture";

    public static final class Params {
        public static final String PROFILE_PICTURE = "profilePicture";

        private Params() {}
    }
}
