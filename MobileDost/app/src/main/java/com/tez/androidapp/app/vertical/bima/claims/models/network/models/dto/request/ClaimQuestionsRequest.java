package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by FARHAN DHANANI on 6/13/2018.
 */
public class ClaimQuestionsRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/policy/{" + Params.INSURANCE_POLICY_ID + "}/claim/questions";

    public abstract static class Params{
        public static final String INSURANCE_POLICY_ID = "id";
    }
}
