package com.tez.androidapp.app.general.feature.mobile.verification.handler;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.mobile.verification.callback.NumberVerifyOtpCallback;
import com.tez.androidapp.app.general.feature.mobile.verification.models.network.dto.response.NumberVerifyOtpResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

import net.tez.fragment.util.optional.Optional;

/**
 * Created by FARHAN DHANANI on 2/26/2019.
 */
public class NumberVerifyOtpRH extends BaseRH<NumberVerifyOtpResponse> {

    @Nullable
    private final NumberVerifyOtpCallback numberVerifyOtpCallback;

    public NumberVerifyOtpRH(BaseCloudDataStore baseCloudDataStore,
                             @Nullable NumberVerifyOtpCallback numberVerifyOtpCallback) {
        super(baseCloudDataStore);
        this.numberVerifyOtpCallback = numberVerifyOtpCallback;
    }

    @Override
    protected void onSuccess(Result<NumberVerifyOtpResponse> value) {
        Optional.ifPresent(value.response().body(),
                numberVerifyOtpResponse -> {
                    Optional.doWhen(isErrorFree(numberVerifyOtpResponse),
                            () -> Optional.ifPresent(this.numberVerifyOtpCallback,
                                    NumberVerifyOtpCallback::onNumberVerifyOtpSuccess),
                            () -> this.onFailure(numberVerifyOtpResponse.getStatusCode(), numberVerifyOtpResponse.getErrorDescription()));
                });
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        Optional.ifPresent(numberVerifyOtpCallback, numberVerifyGenerateOtpCallback -> {
            numberVerifyGenerateOtpCallback.onNumberVerifyOtpFailure(errorCode, message);
        });
    }
}
