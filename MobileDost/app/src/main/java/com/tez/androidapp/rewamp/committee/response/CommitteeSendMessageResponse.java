package com.tez.androidapp.rewamp.committee.response;

import com.google.gson.annotations.SerializedName;
import com.tez.androidapp.app.base.response.BaseResponse;

public class CommitteeSendMessageResponse extends BaseResponse {

    @SerializedName("message")
    String message;
    @SerializedName("response")
    boolean response;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }
}
