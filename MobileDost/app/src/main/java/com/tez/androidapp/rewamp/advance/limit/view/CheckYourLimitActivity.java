package com.tez.androidapp.rewamp.advance.limit.view;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.utils.extract.ExtractionIntentService;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.advance.AdvanceStatus;
import com.tez.androidapp.rewamp.advance.limit.presenter.CheckYourLimitActivityPresenter;
import com.tez.androidapp.rewamp.advance.limit.presenter.ICheckYourLimitActivityPresenter;
import com.tez.androidapp.rewamp.advance.limit.router.LimitAssignedActivityRouter;
import com.tez.androidapp.rewamp.advance.limit.router.LimitDeniedActivityRouter;
import com.tez.androidapp.rewamp.advance.limit.router.LimitRejectedActivityRouter;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.general.timer.TimerActivity;
import com.tez.androidapp.rewamp.profile.trust.router.ProfileTrustActivityRouter;
import com.tez.androidapp.rewamp.signup.router.WelcomeBackActivityRouter;

public class CheckYourLimitActivity extends TimerActivity implements ICheckYourLimitActivityView {


    private final ICheckYourLimitActivityPresenter iCheckYourLimitActivityPresenter;

    private final BroadcastReceiver limitPushNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String status = intent.getStringExtra(PushNotificationConstants.LOAN_LIMIT_STATUS);

            if (status != null) {

                AdvanceStatus advanceStatus = AdvanceStatus.valueOf(status);

                switch (advanceStatus) {

                    case LIMIT_CREATED:
                        LimitAssignedActivityRouter.createInstance().setDependenciesAndRoute(CheckYourLimitActivity.this);
                        break;

                    case LIMIT_REJECTED:
                        LimitRejectedActivityRouter.createInstance().setDependenciesAndRoute(CheckYourLimitActivity.this);
                        break;

                    case LIMIT_DENIED:
                        LimitDeniedActivityRouter.createInstance().setDependenciesAndRoute(CheckYourLimitActivity.this);
                        break;

                    default:
                        routeToDashboard();
                        break;
                }
            } else
                routeToDashboard();

            stopTimer();

            finish();
        }
    };

    public CheckYourLimitActivity() {
        iCheckYourLimitActivityPresenter = new CheckYourLimitActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tvMessage.setText(R.string.please_wait_limit_is_being_assigned);
        tvPrecautionNote.setText(R.string.please_make_sure_message_for_limit);
        init();
        registerReceiver(limitPushNotificationReceiver, new IntentFilter(PushNotificationConstants.BROADCAST_TYPE_LOAN_LIMIT));
    }

    private void init() {
        iCheckYourLimitActivityPresenter.applyLoanLimit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(limitPushNotificationReceiver);
        } catch (IllegalArgumentException e){
            //ignore
        }
    }

    @Override
    protected long getTotalTime() {
        return 120000; // 2-minutes in milliseconds
    }

    @Override
    public void startExtractionIntentServiceToUploadUserData() {

        if (Utility.isDeviceRooted(this) || Utility.checkIsEmulator())
            showInformativeMessage(R.string.string_device_rooted, (dialog, which) -> System.exit(1));

        if (!Utility.isMyServiceRunning(this, ExtractionIntentService.class)) {
            Intent extractionServiceIntent = new Intent(this, ExtractionIntentService.class);
            startService(extractionServiceIntent);
        }
    }

    @Override
    public void sendLimitFailureNotification() {
        Intent nextIntent = new Intent();
        Utility.sendNotification(nextIntent, "Sorry! Hum is waqt limit assign nahi karsakte");
    }

    @Override
    public void onDeviceNotValidError(int errorCode) {
        sendLimitFailureNotification();
        showError(errorCode, (dialog, which) -> WelcomeBackActivityRouter.createInstance().setDependenciesAndRouteWithClearAndNewTask(this));
    }

    @Override
    public void onCnicExpiredError(int errorCode) {
        sendLimitFailureNotification();
        showError(errorCode, (dialog, which) -> {
            ProfileTrustActivityRouter.createInstance().setDependenciesAndRoute(this);
            finish();
        });
    }

    @Override
    protected void onTimerFinish() {
        routeToDashboard();
    }

    private void routeToDashboard() {
        DashboardActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this);
    }

    @Override
    public void onBackPressed() {

    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    protected String getScreenName() {
        return "CheckYourLimitActivity";
    }
}