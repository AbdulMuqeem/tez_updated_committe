package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto

import java.io.Serializable

/**
 * Created by Vinod Kumar on 10/19/2020.
 */
data class ClaimDetailDto(
        val id: Int,
        val mobileUserInsurancePolicyId: Int,
        val mobileUserInsurancePolicyNumber: String,
        val claimDate: String,
        val insurancePolicyName: String,
        val claimStatus: String,
        val claimReferenceNumber: String,
        val productId: Int,
        val transactionId: String?,
        val claimAmount: Double? = null,
        val mobileUserAccountName: String? = null,
        val mobileUserAccountNumber: String? = null,
        val transactionDate: String? = null,
        val comments: List<String>? = null,
) : Serializable