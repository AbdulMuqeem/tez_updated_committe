package com.tez.androidapp.commons.dialog.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.util.DialogUtil;

public class DialogActivity extends AppCompatActivity {

    public static final String MESSAGE = "MESSAGE";
    public static final String IS_SESSION_TIMEOUT = "IS_SESSION_TIMEOUT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setFinishOnTouchOutside(false);
        String message = getIntent().getStringExtra(MESSAGE);
        boolean isSessionTimeout = getIntent().getBooleanExtra(IS_SESSION_TIMEOUT, false);
        if (message != null && isSessionTimeout)
            DialogUtil.showInformativeMessage(this, message, (dialog, which) -> Utility.directUserToIntroScreenAtGlobalLevel());
        else
            finish();
    }
}
