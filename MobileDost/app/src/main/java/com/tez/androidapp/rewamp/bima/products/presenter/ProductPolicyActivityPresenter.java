package com.tez.androidapp.rewamp.bima.products.presenter;

import android.view.View;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.bima.claim.request.InitiateClaimRequest;
import com.tez.androidapp.rewamp.bima.products.interactor.IProductPolicyActivityInteractor;
import com.tez.androidapp.rewamp.bima.products.interactor.IProductPolicyActivityInteractorOutput;
import com.tez.androidapp.rewamp.bima.products.interactor.ProductPolicyActivityInteractor;
import com.tez.androidapp.rewamp.bima.products.model.ProductPolicy;
import com.tez.androidapp.rewamp.bima.products.response.ProductPolicyResponse;
import com.tez.androidapp.rewamp.bima.products.view.IProductPolicyActivityView;

import net.tez.fragment.util.optional.Optional;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProductPolicyActivityPresenter implements IProductPolicyActivityPresenter,
        IProductPolicyActivityInteractorOutput {

    private final IProductPolicyActivityView iProductPolicyActivityView;
    private final IProductPolicyActivityInteractor iProductPolicyActivityInteractor;

    private ProductPolicy productPolicy;

    public ProductPolicyActivityPresenter(IProductPolicyActivityView iProductPolicyActivityView) {
        this.iProductPolicyActivityView = iProductPolicyActivityView;
        this.iProductPolicyActivityInteractor = new ProductPolicyActivityInteractor(this);
    }

    @Override
    public void getProductPolicy(int policyId) {
        iProductPolicyActivityView.setClContentVisibility(View.GONE);
        iProductPolicyActivityView.showShimmer();
        iProductPolicyActivityInteractor.getProductPolicy(policyId);
    }

    @Override
    public void onGetProductPolicySuccess(ProductPolicyResponse productPolicyResponse) {
        ProductPolicy productPolicy = productPolicyResponse.getPolicyDetailsDto();
        this.productPolicy = productPolicy;
        iProductPolicyActivityView.hideShimmer();
        iProductPolicyActivityView.initPolicyDetails(productPolicy);
        iProductPolicyActivityView.setInsuranceProviderLogo(Utility.getInsuranceProviderLogo(productPolicy.getInsuranceServiceProviderCode()));

        Optional.ifPresent(productPolicy.getContact().getContactEmail(),
                iProductPolicyActivityView::setGroupEmailListener,
                iProductPolicyActivityView::hideTllEmail);

        Optional.ifPresent(productPolicy.getContact().getContactWhatsApp(),
                iProductPolicyActivityView::setGroupWhatsappListener,
                iProductPolicyActivityView::hideWhatsapp);

        String[] numbers = productPolicy.getContact().getContactPhone().split(",");

        if (numbers.length == 1)
            iProductPolicyActivityView.setGroupCallListener(numbers[0]);
        else
            iProductPolicyActivityView.setGroupCallListener(numbers);

        iProductPolicyActivityView.setClContentVisibility(View.VISIBLE);
    }

    @Override
    public void onGetProductPolicyFailure(int errorCode, String message) {
        iProductPolicyActivityView.hideShimmer();
        iProductPolicyActivityView.showError(errorCode, (dialog, which) -> iProductPolicyActivityView.finishActivity());
    }

    @Override
    public void initiateClaim(int mobileUserInsurancePolicyId) {
        iProductPolicyActivityView.showTezLoader();
        iProductPolicyActivityInteractor.initiateClaim(new InitiateClaimRequest(mobileUserInsurancePolicyId));
    }

    @Override
    public void onSuccessInitiateClaim(@NotNull BaseResponse response) {
        iProductPolicyActivityView.setTezLoaderToBeDissmissedOnTransition();
        iProductPolicyActivityView.routeToLodgeClaim(this.productPolicy);
    }

    @Override
    public void onFailureInitiateClaim(int errorCode, @Nullable String message) {
        iProductPolicyActivityView.dismissTezLoader();
        iProductPolicyActivityView.showError(errorCode);
    }
}
