package com.tez.androidapp.rewamp.bima.claim.common.document.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.View
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.tez.androidapp.R
import com.tez.androidapp.app.base.ui.activities.BaseActivity
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.EvidenceFile
import com.tez.androidapp.commons.utils.app.Constants
import com.tez.androidapp.commons.utils.authorized.alert.image.popup.DefaultUIForAlertImage
import com.tez.androidapp.commons.utils.authorized.alert.image.popup.RenderAlertImageOnUI
import com.tez.androidapp.commons.utils.file.util.FileUtil
import com.tez.androidapp.rewamp.bima.claim.common.document.adapter.EvidenceFilesAdapter
import com.tez.androidapp.rewamp.bima.claim.common.document.adapter.EvidenceFilesAdapter.EvidenceFileListener
import com.tez.androidapp.rewamp.bima.claim.common.document.router.UploadDocumentActivityRouter
import com.tez.androidapp.rewamp.bima.products.dialog.ChooseItemDialog
import com.tez.androidapp.rewamp.bima.products.model.Item
import kotlinx.android.synthetic.main.activity_upload_document.*
import net.tez.camera.request.CameraRequestBuilder
import net.tez.filepicker.request.document.DocumentPickerRequestBuilder
import net.tez.filepicker.utils.Util
import net.tez.fragment.util.listener.DoubleTapSafeDialogClickListener
import net.tez.fragment.util.optional.Optional
import java.io.File
import java.io.Serializable

class UploadDocumentActivity : BaseActivity(), EvidenceFileListener {

    private lateinit var adapter: EvidenceFilesAdapter

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_document)
        init(UploadDocumentActivityRouter.getDependencies(intent))
    }

    private fun init(dependencies: UploadDocumentActivityRouter.Dependencies) {
        initAdapter(dependencies.docId, dependencies.evidenceFileList)
        setArrowsBehaviour()
        initListener()
        initSource(dependencies.source)
    }

    private fun initListener() {
        btContinue.setDoubleTapSafeOnClickListener { setResults() }
        ivOptions.setDoubleTapSafeOnClickListener {
            val popupMenu = PopupMenu(it.context, it, Gravity.START)
            popupMenu.inflate(R.menu.doc_file_options)
            popupMenu.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.replace -> {
                        val dialog = ChooseItemDialog(this)
                        dialog.setHeading(R.string.choose_type)
                        dialog.setItemList(listOf(
                                Item(1, R.drawable.ic_camera_green, getString(R.string.capture_image)),
                                Item(2, R.drawable.ic_upload_green, getString(R.string.upload_document)),
                        ))
                        dialog.setListener { item ->
                            dialog.dismiss()
                            when (item.id) {
                                1 -> {
                                    CameraRequestBuilder()
                                            .setMaxImageCount(1)
                                            .setEnableGalleryMode(true)
                                            .setRootPath(FileUtil.getClaimFilesPath())
                                            .build()
                                            .startCamera(this, REQUEST_CODE_REPLACE_IMAGE)
                                }
                                2 -> {
                                    openDocument(REQUEST_CODE_REPLACE_DOCUMENT)
                                }
                            }
                        }
                        dialog.show()
                    }
                    R.id.delete -> {
                        adapter.deleteSelected()
                        if (adapter.isEmpty())
                            setResults()
                        else
                            setAddButtonVisibility()
                    }
                }
                true
            }
            popupMenu.show()
        }
        ivAddFile.setDoubleTapSafeOnClickListener {
            val dialog = ChooseItemDialog(this)
            dialog.setHeading(R.string.choose_type)
            dialog.setItemList(listOf(
                    Item(1, R.drawable.ic_camera_green, getString(R.string.capture_image)),
                    Item(2, R.drawable.ic_upload_green, getString(R.string.upload_document)),
            ))
            dialog.setListener { item ->
                dialog.dismiss()
                when (item.id) {
                    1 -> {
                        openCamera(REQUEST_CODE_ADD_IMAGE)
                    }
                    2 -> {
                        openDocument(REQUEST_CODE_ADD_DOCUMENT)
                    }
                }
            }
            dialog.show()
        }
    }

    private fun setAddButtonVisibility() {
        ivAddFile.visibility = if (adapter.itemCount == Constants.MAX_FILES_PER_CLAIM_DOCUMENT) View.GONE else View.VISIBLE
    }

    private fun initSource(source: Source) {
        when (source) {
            Source.CAMERA -> openCamera()

            Source.DOCUMENT -> openDocument()

            Source.READ_ONLY -> {
                ivOptions.visibility = View.GONE
                ivAddFile.visibility = View.GONE
                btContinue.setText(R.string.back)
            }

            Source.EDIT -> {
            }
        }
    }

    private fun setResults() {
        setResult(RESULT_OK, UploadDocumentActivityRouter.createResultIntent(adapter.docId, adapter.items))
        finish()
    }

    private fun openCamera(requestCode: Int = CameraRequestBuilder.REQUEST_CODE_CAMERA) = CameraRequestBuilder()
            .setMaxImageCount(Constants.MAX_FILES_PER_CLAIM_DOCUMENT - adapter.itemCount)
            .setEnableGalleryMode(true)
            .setRootPath(FileUtil.getClaimFilesPath())
            .build()
            .startCamera(this, requestCode)

    private fun openDocument(requestCode: Int = DocumentPickerRequestBuilder.REQUEST_CODE_DOCUMENT) = DocumentPickerRequestBuilder()
            .addFileSupport("PDF", arrayOf(".pdf"))
            .build()
            .pickDocument(this, requestCode)

    private fun setArrowsBehaviour() {
        ivLeftArrow.setOnClickListener { rvThumbnails.smoothScrollToPosition(0) }
        ivRightArrow.setOnClickListener { rvThumbnails.smoothScrollToPosition(adapter.itemCount - 1) }
        ivLeftArrow.alpha = 0.2f

        Optional.doWhen(!rvThumbnails.canScrollHorizontally(1)) { ivRightArrow.alpha = 0.2f }

        rvThumbnails.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                Optional.doWhen(rvThumbnails.canScrollHorizontally(1),
                        { ivRightArrow.alpha = 1f },
                        { ivRightArrow.alpha = 0.2f })

                Optional.doWhen(rvThumbnails.canScrollHorizontally(-1),
                        { ivLeftArrow.alpha = 1f },
                        { ivLeftArrow.alpha = 0.2f })
            }
        })
    }

    override fun onBackPressed() = setResults()

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && data != null) {
            when (requestCode) {
                CameraRequestBuilder.REQUEST_CODE_CAMERA, REQUEST_CODE_ADD_IMAGE -> {
                    addFiles(data.getStringArrayListExtra(CameraRequestBuilder.RESULT_IMAGE_FILES_ARRAY_LIST)!!)
                }

                DocumentPickerRequestBuilder.REQUEST_CODE_DOCUMENT -> {
                    checkLengthAndAddFiles(data.getStringArrayListExtra(DocumentPickerRequestBuilder.RESULT_DOCUMENT_ARRAY_LIST)!!) { _, _ ->
                        if (adapter.isEmpty())
                            finish()
                    }
                }

                REQUEST_CODE_ADD_DOCUMENT -> {
                    checkLengthAndAddFiles(data.getStringArrayListExtra(DocumentPickerRequestBuilder.RESULT_DOCUMENT_ARRAY_LIST)!!)
                }

                REQUEST_CODE_REPLACE_DOCUMENT -> {
                    val path = data.getStringArrayListExtra(DocumentPickerRequestBuilder.RESULT_DOCUMENT_ARRAY_LIST)?.get(0)!!
                    if (Util.scaleUpByte(File(path).length().toDouble()) > Constants.MAX_SIZE_FOR_CLAIM_DOCUMENT)
                        showInformativeMessage(R.string.document_must_be_less_than_mb)
                    else
                        adapter.replaceSelected(EvidenceFile(adapter.docId, path))
                }

                REQUEST_CODE_REPLACE_IMAGE -> {
                    val path = data.getStringArrayListExtra(CameraRequestBuilder.RESULT_IMAGE_FILES_ARRAY_LIST)?.get(0)
                    adapter.replaceSelected(EvidenceFile(adapter.docId, path!!))
                }

            }
            setAddButtonVisibility()
        } else if (requestCode == CameraRequestBuilder.REQUEST_CODE_CAMERA
                || requestCode == DocumentPickerRequestBuilder.REQUEST_CODE_DOCUMENT)
            finish()
    }

    private fun checkLengthAndAddFiles(list: List<String>, listener: DoubleTapSafeDialogClickListener? = null) {
        list.find {
            Util.scaleUpByte(File(it).length().toDouble()) > Constants.MAX_SIZE_FOR_CLAIM_DOCUMENT
        }?.let {
            listener?.let {
                showInformativeMessage(R.string.document_must_be_less_than_mb, it)
            } ?: showInformativeMessage(R.string.document_must_be_less_than_mb)
        } ?: addFiles(list)
    }

    private fun addFiles(paths: List<String>) {
        adapter.addAll(paths.map {
            EvidenceFile(adapter.docId, it)
        })
    }

    private fun initAdapter(docId: Int, evidenceFileList: MutableList<EvidenceFile>) {
        adapter = EvidenceFilesAdapter(docId, evidenceFileList, this)
        rvThumbnails.adapter = adapter
        setAddButtonVisibility()
    }

    override fun getScreenName(): String = "UploadDocumentActivity"

    override fun onClickEvidenceFileThumbnail(evidenceFile: EvidenceFile) {
        if (File(evidenceFile.path).extension == "pdf") {
            ivPreview.setImageResource(R.drawable.ic_pdf_preview)
            tvNoPreview.visibility = View.VISIBLE
        } else {
            tvNoPreview.visibility = View.GONE
            val uiForAlertImage = DefaultUIForAlertImage(ivPreview)
            RenderAlertImageOnUI.renderDefaultImageUIAlert(uiForAlertImage, Uri.parse(evidenceFile.path), evidenceFile.path)
        }
    }

    companion object {
        private const val REQUEST_CODE_ADD_IMAGE = 96
        private const val REQUEST_CODE_ADD_DOCUMENT = 97
        private const val REQUEST_CODE_REPLACE_DOCUMENT = 98
        private const val REQUEST_CODE_REPLACE_IMAGE = 99
    }

    enum class Source : Serializable {
        CAMERA, DOCUMENT, READ_ONLY, EDIT
    }
}