package com.tez.androidapp.rewamp.bima.products.policy.router;

import android.content.Intent;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.view.EasyPaisaInsurancePaymentActivity;

public class EasyPaisaInsurancePaymentActivityRouter extends BaseActivityRouter {

    public static final String PRODUCT_DETAILS = "product_details";
    public static final String WALLET_PROVIDER_ID = "wallet_provider_id";

    public static EasyPaisaInsurancePaymentActivityRouter createInstance() {
        return new EasyPaisaInsurancePaymentActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        int walletProviderId,
                                        @NonNull InitiatePurchasePolicyRequest initiatePurchasePolicyRequest) {
        Intent intent = createIntent(from);
        intent.putExtra(WALLET_PROVIDER_ID, walletProviderId);
        intent.putExtra(PRODUCT_DETAILS, (Parcelable) initiatePurchasePolicyRequest);
        from.startActivity(intent);
    }

    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, EasyPaisaInsurancePaymentActivity.class);
    }
}
