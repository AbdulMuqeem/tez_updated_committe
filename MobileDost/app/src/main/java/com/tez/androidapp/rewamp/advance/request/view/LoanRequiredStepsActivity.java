package com.tez.androidapp.rewamp.advance.request.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.widgets.StepCardView;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.commons.widgets.TezToolbar;
import com.tez.androidapp.rewamp.advance.request.presenter.ILoanRequiredStepsActivityPresenter;
import com.tez.androidapp.rewamp.advance.request.presenter.LoanRequiredStepsActivityPresenter;
import com.tez.androidapp.rewamp.advance.request.router.FreeBimaOnBoardingActivityRouter;
import com.tez.androidapp.rewamp.advance.request.router.LoanRequiredStepsActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.router.AddWalletActivityRouter;
import com.tez.androidapp.rewamp.profile.router.CompleteProfileRouter;
import com.tez.androidapp.rewamp.profile.trust.router.ProfileTrustActivityRouter;

import net.tez.fragment.util.optional.Optional;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;

public class LoanRequiredStepsActivity extends BaseActivity implements ILoanRequiredStepsActivityView {

    protected ILoanRequiredStepsActivityPresenter iLoanRequiredStepsActivityPresenter;

    @BindView(R.id.toolbar)
    protected TezToolbar toolbar;

    @BindView(R.id.tvLoanReadyToDisburse)
    protected TezTextView tvLoanReadyToDisburse;

    @BindView(R.id.tvCompleteSteps)
    protected TezTextView tvCompleteSteps;

    @BindView(R.id.cvCompleteProfile)
    protected StepCardView cvCompleteProfile;

    @BindView(R.id.cvMobileWallet)
    private StepCardView cvMobileWallet;

    @BindView(R.id.btContinue)
    private TezButton btContinue;

    @BindView(R.id.clContent)
    private TezConstraintLayout clContent;

    public LoanRequiredStepsActivity() {
        iLoanRequiredStepsActivityPresenter = new LoanRequiredStepsActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_required_steps);
        ViewBinder.bind(this);
        init();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        boolean isProfileCompleted = intent.getBooleanExtra(LoanRequiredStepsActivityRouter.IS_PROFILE_COMPLETED, false);
        if (isProfileCompleted)
            iLoanRequiredStepsActivityPresenter.removeStep(Constants.COMPLETE_PROFILE);
    }

    private void init() {
        ArrayList<String> stepsLeftFromIntent = getIntent().getStringArrayListExtra(LoanRequiredStepsActivityRouter.STEPS_LEFT_LIST);
        Optional.ifPresent(stepsLeftFromIntent,
                iLoanRequiredStepsActivityPresenter::setLoanRemainingSteps,
                iLoanRequiredStepsActivityPresenter::getLoanRemainingSteps);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AddWalletActivityRouter.REQUEST_CODE_ADD_WALLET && resultCode == RESULT_OK)
            finishActivityWithResultOk();
    }

    @Override
    public void setCompleteProfileCompleted(boolean completed) {
        cvCompleteProfile.setStepCompleted(completed);
    }

    @Override
    public void setMobileWalletCompleted(boolean completed) {
        cvMobileWallet.setStepCompleted(completed);
    }

    @Override
    public void setBtContinueOnClickListener(@NonNull String nextStep) {
        this.btContinue.setDoubleTapSafeOnClickListener(view -> iLoanRequiredStepsActivityPresenter.onClickBtContinue(nextStep));
    }

    @Override
    public void routeToAddBeneficiaryActivity() {
        FreeBimaOnBoardingActivityRouter.createInstance().setDependenciesAndRoute(this);
        finish();
    }

    @Override
    public void routeToCompleteProfile() {
        ProfileTrustActivityRouter.createInstance().setDependenciesAndRoute(this, CompleteProfileRouter.ROUTE_TO_LOAN);
    }

    @Override
    public void routeToAddWallet() {
        AddWalletActivityRouter.createInstance().setDependenciesAndRouteForResult(this);
    }

    @Override
    public void setClContentVisibility(int visibility) {
        this.clContent.setVisibility(visibility);
    }

    @Override
    public void finishActivityWithResultOk() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void showShimmer() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideShimmer() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.GONE);
    }

    @Override
    protected String getScreenName() {
        return "LoanRequiredStepsActivity";
    }
}