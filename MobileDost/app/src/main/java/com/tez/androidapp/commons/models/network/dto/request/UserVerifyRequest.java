package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;
import com.tez.androidapp.commons.models.network.DeviceInfo;

/**
 * Created  on 2/22/2017.
 */

public class UserVerifyRequest extends BaseRequest {
    public static final String METHOD_NAME = "v2/user/verify";
    private String pin;
    private String appVersion;
    private DeviceInfo deviceInfo;

    public UserVerifyRequest(String pin, String appVersion, DeviceInfo deviceInfo) {
        this.pin = pin;
        this.appVersion = appVersion;
        this.deviceInfo = deviceInfo;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

}
