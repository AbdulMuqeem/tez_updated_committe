package com.tez.androidapp.rewamp.advance.request.view;

import androidx.annotation.NonNull;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.commons.models.network.LoanDetails;

import net.tez.viewbinder.library.core.OnClick;

public interface ILoanReceiptActivityView extends IBaseView {

    void initValues(@NonNull LoanDetails loanDetails);

    void setClContentVisibility(int visibility);

    @OnClick(R.id.btBack)
    void routeToDashboard();
}
