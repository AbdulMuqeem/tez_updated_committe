package com.tez.androidapp.rewamp.advance.repay.view;

public class VerifyJazzRepaymentActivity extends VerifyEasyPaisaRepaymentActivity {
    private static final int JAZZ_PIN_LENGTH = 4;

    @Override
    protected int getPinLength() {
        return JAZZ_PIN_LENGTH;
    }
}
