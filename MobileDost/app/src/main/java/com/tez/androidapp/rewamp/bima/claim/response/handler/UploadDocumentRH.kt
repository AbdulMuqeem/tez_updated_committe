package com.tez.androidapp.rewamp.bima.claim.response.handler

import com.jakewharton.retrofit2.adapter.rxjava2.Result
import com.tez.androidapp.app.base.handlers.BaseRH
import com.tez.androidapp.repository.network.store.BaseCloudDataStore
import com.tez.androidapp.rewamp.bima.claim.callback.UploadDocumentCallback
import com.tez.androidapp.rewamp.bima.claim.response.UploadDocumentResponse

class UploadDocumentRH(baseCloudDataStore: BaseCloudDataStore?,
                       private val uploadDocumentCallback: UploadDocumentCallback)
    : BaseRH<UploadDocumentResponse>(baseCloudDataStore) {
    override fun onSuccess(value: Result<UploadDocumentResponse>) {
        val response = value.response().body()
        response?.let {
            if (isErrorFree(it))
                uploadDocumentCallback.onSuccessUploadDocument(it)
            else
                onFailure(it.statusCode, it.errorDescription)

        } ?: sendDefaultFailure()
    }

    override fun onFailure(errorCode: Int, message: String?) {
        uploadDocumentCallback.onFailureUploadDocument(errorCode, message)
    }
}