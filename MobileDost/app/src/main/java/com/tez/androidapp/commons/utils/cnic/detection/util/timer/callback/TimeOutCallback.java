package com.tez.androidapp.commons.utils.cnic.detection.util.timer.callback;

public interface TimeOutCallback {
    void onTimeOut();
}
