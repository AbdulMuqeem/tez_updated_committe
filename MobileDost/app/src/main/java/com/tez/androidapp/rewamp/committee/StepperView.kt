package com.tez.androidapp.rewamp.committee;
import android.content.Context
import android.graphics.PorterDuff
import android.util.AttributeSet
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintSet
import com.tez.androidapp.R
import com.tez.androidapp.commons.widgets.TezConstraintLayout
import kotlinx.android.synthetic.main.committee_round_step.view.*

class StepperView @JvmOverloads constructor(context: Context?, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : TezConstraintLayout(context, attrs, defStyleAttr) {
    private lateinit var stepViews: List<StepView>
    private fun initDummyList() = setSteps(listOfNotNull(
            Step(1, R.string.profile, R.drawable.ic_profile_cnic),
            Step(1, R.string.profile, R.drawable.ic_profile_cnic),
            Step(1, R.string.profile, R.drawable.ic_profile_cnic),
            Step(1, R.string.profile, R.drawable.ic_profile_cnic)
    ))

    fun setSteps(steps: List<Step>) {
        var counter = 1
        stepViews = steps.map { StepView(context, counter++, it) }
        removeAllViews()
        initializeStepViews()
        setStepNumber(1)
    }

    fun setStepNumber(number: Int) {
        val index = number - 1
        if (index >= 0 && index < stepViews.size) {
            for (i in 0..index) stepViews[i].setEnableStep(true)
            for (i in index + 1 until stepViews.size) stepViews[i].setEnableStep(false)
        }
    }

    private fun initializeStepViews() {
        val set = ConstraintSet()
        var currentView: StepView? = null
        for (i in stepViews.indices) {
            val nextView = stepViews[i]
            addView(nextView)
            set.clone(this)
            if (currentView == null) {
                nextView.removeStartStepBar()
                set.connect(nextView.id, ConstraintSet.START, id, ConstraintSet.START)
                set.setHorizontalChainStyle(nextView.id, ConstraintSet.CHAIN_SPREAD)
            } else {
                set.connect(nextView.id, ConstraintSet.START, currentView.id, ConstraintSet.END)
                set.connect(currentView.id, ConstraintSet.END, nextView.id, ConstraintSet.START)
            }
            set.connect(nextView.id, ConstraintSet.TOP, id, ConstraintSet.TOP)
            set.connect(nextView.id, ConstraintSet.BOTTOM, id, ConstraintSet.BOTTOM)
            if (i + 1 == stepViews.size) {
                nextView.removeEndStepBar()
                set.connect(nextView.id, ConstraintSet.END, id, ConstraintSet.END)
            }
            set.applyTo(this)
            currentView = nextView
        }
    }

    private class StepView(context: Context?, id: Int, step: Step) : TezConstraintLayout(context) {

        fun setEnableStep(enabled: Boolean) {
            val greenColor = getColor(context, R.color.textViewTextColorGreen)
            tvStepName.setTextColor(if (enabled) greenColor else getColor(context, R.color.colorWhite))
            ivStepIcon.setColorFilter(if (enabled) greenColor else getColor(context, R.color.editTextBottomLineColorGrey), PorterDuff.Mode.SRC_IN)
            tvStepNumber.setBackgroundResource(if (enabled) R.drawable.circle_green_filled else R.drawable.round_bg_grey)
        }

        fun removeStartStepBar() = removeView(ivStepStartBar)

        fun removeEndStepBar() = removeView(ivStepEndBar)

        init {
            setId(id)
            val params = LayoutParams(0, LayoutParams.WRAP_CONTENT)
            layoutParams = params
            inflate(context, R.layout.committee_round_step, this)
            tvStepNumber.text = step.number.toString()
            tvStepName.setText(step.name)
            ivStepIcon.setImageResource(step.icon)
        }
    }

    class Step(val number: Int,
               @field:StringRes @param:StringRes val name: Int,
               @field:DrawableRes @param:DrawableRes val icon: Int)

    init {
        if (id == NO_ID) {
            val id = 1298
            setId(id)
        }
        initDummyList()
    }
}