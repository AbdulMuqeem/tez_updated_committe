package com.tez.androidapp.rewamp.profile.trust.interactor;

import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.GetCnicUploadsCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.UserProfile;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.profile.trust.presenter.IProfileTrustActivityInteractorOutput;

public class ProfileTrustActivityInteractor implements IProfileTrustActivityInteractor, GetCnicUploadsCallback {
    private final IProfileTrustActivityInteractorOutput iProfileTrustActivityInteractorOutput;

    public ProfileTrustActivityInteractor(IProfileTrustActivityInteractorOutput iProfileTrustActivityInteractorOutput) {
        this.iProfileTrustActivityInteractorOutput = iProfileTrustActivityInteractorOutput;
    }

    @Override
    public void getCnicUploads(){
        UserAuthCloudDataStore.getInstance().getCnicUploads(this);
    }

    @Override
    public void onGetCnicUploadsCallbackSuccess(UserProfile userProfile) {
        this.iProfileTrustActivityInteractorOutput.onGetCnicUploadsCallbackSuccess(userProfile);
    }

    @Override
    public void onGetCnicUploadsCallbackFailure(int errorCode, String message) {
        Optional.doWhen(Utility.isUnauthorized(errorCode),
                this::getCnicUploads,
                ()->this.iProfileTrustActivityInteractorOutput.onGetCnicUploadsCallbackFailure(errorCode, message));
    }
}
