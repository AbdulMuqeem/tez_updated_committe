package com.tez.androidapp.rewamp.general.suspend.account.presenter;


import com.tez.androidapp.rewamp.general.suspend.account.interactor.ISuspendAccountFeedbackActivityInteractor;
import com.tez.androidapp.rewamp.general.suspend.account.interactor.SuspendAccountFeedbackActivityInteractor;
import com.tez.androidapp.rewamp.general.suspend.account.view.ISuspendAccountFeedbackActivityView;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public class SuspendAccountFeedbackActivityPresenter implements ISuspendAccountFeedbackActivityPresenter,
        ISuspendAccountFeedbackActivityInteractorOutput {

    private final ISuspendAccountFeedbackActivityView iSuspendAccountFeedbackActivityView;
    private final ISuspendAccountFeedbackActivityInteractor iSuspendAccountFeedbackActivityInteractor;

    public SuspendAccountFeedbackActivityPresenter(ISuspendAccountFeedbackActivityView iSuspendAccountFeedbackActivityView) {
        this.iSuspendAccountFeedbackActivityView = iSuspendAccountFeedbackActivityView;
        iSuspendAccountFeedbackActivityInteractor = new SuspendAccountFeedbackActivityInteractor(this);
    }

    @Override
    public void sendUserFeedBackToServer(String feedback) {
        iSuspendAccountFeedbackActivityView.setBtSubmitEnabled(false);
        iSuspendAccountFeedbackActivityView.showTezLoader();
        iSuspendAccountFeedbackActivityInteractor.sendFeedback(feedback);
    }

    @Override
    public void onFeedbackSuccess() {
        iSuspendAccountFeedbackActivityView.takeUserToIntroScreen();
    }

    @Override
    public void onFeedbackFailure(int errorCode, String message) {
        iSuspendAccountFeedbackActivityView.setBtSubmitEnabled(true);
        iSuspendAccountFeedbackActivityView.dismissTezLoader();
        iSuspendAccountFeedbackActivityView.showError(errorCode);
    }
}
