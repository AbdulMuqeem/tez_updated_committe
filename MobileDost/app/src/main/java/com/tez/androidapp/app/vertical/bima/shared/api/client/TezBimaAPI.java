package com.tez.androidapp.app.vertical.bima.shared.api.client;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.questions.models.network.models.dto.response.ClaimQuestionsResponse;
import com.tez.androidapp.app.vertical.bima.add.beneficiary.models.network.dto.request.BeneficiaryRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request.ClaimDocumentListRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request.ClaimQuestionsRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request.GetSizeForExtentionRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request.ClaimListRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request.ClaimDetailsRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request.UploadClaimAnswerRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request.UploadClaimDocumentRequest;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.ClaimDocumentListResponse;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.GetSizeForExtentionResponse;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.ClaimListResponse;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.ClaimDetailsResponse;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.UploadClaimAnswerResponse;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.UploadClaimDocumentResponse;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.request.GetInsurancePoliciesRequest;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.request.InsurancePolicyDetailsRequest;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.response.GetInsurancePoliciesResponse;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.response.InsurancePolicyDetailsResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.rewamp.advance.request.request.InsuranceRemainingStepsRequest;
import com.tez.androidapp.rewamp.advance.request.response.LoanRemainingStepsResponse;
import com.tez.androidapp.rewamp.bima.claim.request.ConfirmClaimRequest;
import com.tez.androidapp.rewamp.bima.claim.request.ConfirmReviseClaimRequest;
import com.tez.androidapp.rewamp.bima.claim.request.InitiateClaimRequest;
import com.tez.androidapp.rewamp.bima.claim.request.InitiateReviseClaimRequest;
import com.tez.androidapp.rewamp.bima.claim.request.LodgeClaimRequest;
import com.tez.androidapp.rewamp.bima.claim.request.UploadDocumentRequest;
import com.tez.androidapp.rewamp.bima.claim.response.ConfirmClaimResponse;
import com.tez.androidapp.rewamp.bima.claim.response.InitiateReviseClaimResponse;
import com.tez.androidapp.rewamp.bima.claim.response.LodgeClaimResponse;
import com.tez.androidapp.rewamp.bima.claim.response.UploadDocumentResponse;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.request.InsuranceProductDetailRequest;
import com.tez.androidapp.rewamp.bima.products.policy.request.PurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.response.InitiatePurchasePolicyResponse;
import com.tez.androidapp.rewamp.bima.products.policy.response.InsuranceProductDetailResponse;
import com.tez.androidapp.rewamp.bima.products.policy.response.PurchasePolicyResponse;
import com.tez.androidapp.rewamp.bima.products.request.ProductDetailTermConditionRequest;
import com.tez.androidapp.rewamp.bima.products.request.ProductListRequest;
import com.tez.androidapp.rewamp.bima.products.request.ProductPolicyRequest;
import com.tez.androidapp.rewamp.bima.products.request.ProductTermConditionsRequest;
import com.tez.androidapp.rewamp.bima.products.response.ProductListResponse;
import com.tez.androidapp.rewamp.bima.products.response.ProductPolicyResponse;
import com.tez.androidapp.rewamp.bima.products.response.ProductTermConditionsResponse;
import com.tez.androidapp.rewamp.general.beneficiary.request.BeneficiariesRequest;
import com.tez.androidapp.rewamp.general.beneficiary.request.BeneficiaryAllocationRequest;
import com.tez.androidapp.rewamp.general.beneficiary.request.BeneficiaryRelationsRequest;
import com.tez.androidapp.rewamp.general.beneficiary.request.DeleteBeneficiaryRequest;
import com.tez.androidapp.rewamp.general.beneficiary.response.BeneficiariesResponse;
import com.tez.androidapp.rewamp.general.beneficiary.response.BeneficiaryAllocationResponse;
import com.tez.androidapp.rewamp.general.beneficiary.response.BeneficiaryRelationsResponse;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created  on 8/25/2017.
 */

public interface TezBimaAPI {

    @GET(BeneficiariesRequest.METHOD_NAME)
    Observable<Result<BeneficiariesResponse>> getBeneficiaries();

    @GET(BeneficiaryRelationsRequest.METHOD_NAME)
    Observable<Result<BeneficiaryRelationsResponse>> getBeneficiaryRelations();

    @PUT(BeneficiaryAllocationRequest.METHOD_NAME)
    Observable<Result<BeneficiaryAllocationResponse>> putBeneficiaryAllocation(@Body BeneficiaryAllocationRequest request);

    @DELETE(DeleteBeneficiaryRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> deleteBeneficiary(@Path(DeleteBeneficiaryRequest.Params.ID) int beneficiaryId);

    @GET(ClaimQuestionsRequest.METHOD_NAME)
    Observable<Result<ClaimQuestionsResponse>> getClaimQuestions(@Path(ClaimQuestionsRequest.Params.INSURANCE_POLICY_ID) int insuarancePolicyId);

    @GET(ClaimDocumentListRequest.METHOD_NAME)
    Observable<Result<ClaimDocumentListResponse>> getClaimDocumentList(@Path(ClaimDocumentListRequest.Params.INSURANCE_POLICY_ID) int insuarancePolicyId);

    @GET(ClaimListRequest.METHOD_NAME)
    Observable<Result<ClaimListResponse>> getClaimList();

    @GET(ClaimDetailsRequest.METHOD_NAME)
    Observable<Result<ClaimDetailsResponse>> getClaimDetails(@Path(ClaimDetailsRequest.Params.CLAIM_ID) int insuranceClaimId);

    @GET(GetInsurancePoliciesRequest.METHOD_NAME)
    Observable<Result<GetInsurancePoliciesResponse>> getInsurancePolicies();

    @GET(ProductTermConditionsRequest.METHOD_NAME)
    Observable<Result<ProductTermConditionsResponse>> getProductTermConditions(@Path(ProductTermConditionsRequest.Params.PRODUCT_ID) int productId);

    @GET(ProductDetailTermConditionRequest.METHOD_NAME)
    Observable<Result<ResponseBody>> getProductTermCondition(@Path(ProductDetailTermConditionRequest.Params.PRODUCT_ID) int productId);

    @GET(InsurancePolicyDetailsRequest.METHOD_NAME)
    Observable<Result<InsurancePolicyDetailsResponse>> getInsurancePolicyDetails(@Path(InsurancePolicyDetailsRequest.Params.ACTIVE_INSURANCE_POLICY_ID) int insurancePolicyId);

    @GET(ProductPolicyRequest.METHOD_NAME)
    Observable<Result<ProductPolicyResponse>> getProductPolicy(@Path(ProductPolicyRequest.Params.ID) int insurancePolicyId);

    @GET(GetSizeForExtentionRequest.METHOD_NAME)
    Observable<Result<GetSizeForExtentionResponse>> getSizeForExtention();

    @POST(UploadClaimAnswerRequest.METHOD_NAME)
    Observable<Result<UploadClaimAnswerResponse>> uploadClaimAnswers(@Body UploadClaimAnswerRequest uploadClaimAnswerRequest);

    @PUT(BeneficiaryRequest.PUT_METHOD_NAME)
    Observable<Result<BaseResponse>> setDefaultBeneficiary(@Path(BeneficiaryRequest.Params.BENEFICIARY_ID) int beneficiaryId);

    @POST(BeneficiaryRequest.POST_METHOD_NAME)
    Observable<Result<BaseResponse>> setBeneficiary(@Body BeneficiaryRequest beneficiaryRequest);

    @Multipart
    @POST(UploadClaimDocumentRequest.METHOD_NAME)
    Observable<Result<UploadClaimDocumentResponse>> uploadClaimDocument(@Header(Constants.HEADER_API_KEY) String apiKey,
                                                                        @Part MultipartBody.Part docId,
                                                                        @Part List<MultipartBody.Part> partsImages);

    @GET(ProductListRequest.METHOD_NAME)
    Observable<Result<ProductListResponse>> getInsuranceProducts();

    @GET(InsuranceRemainingStepsRequest.METHOD_NAME)
    Observable<Result<LoanRemainingStepsResponse>> getInsuranceRemainingSteps();

    @GET(InsuranceProductDetailRequest.METHOD_NAME)
    Observable<Result<InsuranceProductDetailResponse>> getInsuranceProductDetail(@Path(InsuranceProductDetailRequest.Params.PRODUCT_ID) int productId);

    @POST(InitiatePurchasePolicyRequest.METHOD_NAME)
    Observable<Result<InitiatePurchasePolicyResponse>> initiatePurchasePolicy(@Body InitiatePurchasePolicyRequest initiatePurchasePolicyRequest);

    @POST(PurchasePolicyRequest.METHOD_NAME)
    Observable<Result<PurchasePolicyResponse>> purchasePolicy(@Body PurchasePolicyRequest purchasePolicyRequest);

    @GET(InitiateClaimRequest.METHOD_NAME)
    Observable<Result<BaseResponse>> initiateClaim(@Path(InitiateClaimRequest.MOBILE_USER_INSURANCE_POLICY_ID) int mobileUserInsurancePolicyId);

    @POST(LodgeClaimRequest.METHOD_NAME)
    Observable<Result<LodgeClaimResponse>> lodgeClaim(@Body LodgeClaimRequest lodgeClaimRequest);

    @Multipart
    @POST(UploadDocumentRequest.METHOD_NAME)
    Observable<Result<UploadDocumentResponse>> uploadDocument(@Part MultipartBody.Part data,
                                                              @Part MultipartBody.Part claimId,
                                                              @Part MultipartBody.Part categoryId);
    @PUT(ConfirmClaimRequest.METHOD_NAME)
    Observable<Result<ConfirmClaimResponse>> confirmClaim(@Path(ConfirmClaimRequest.CLAIM_ID) int claimId);

    @GET(InitiateReviseClaimRequest.METHOD_NAME)
    Observable<Result<InitiateReviseClaimResponse>> initiateReviseClaim(@Path(InitiateReviseClaimRequest.CLAIM_ID) int claimId);

    @POST(ConfirmReviseClaimRequest.METHOD_NAME)
    Observable<Result<ConfirmClaimResponse>> confirmReviseClaim(@Body ConfirmReviseClaimRequest confirmReviseClaimRequest);
}
