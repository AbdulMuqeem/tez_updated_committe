package com.tez.androidapp.rewamp.signup.presenter;

import androidx.annotation.NonNull;

import com.tez.androidapp.commons.locale.LocaleHelper;
import com.tez.androidapp.rewamp.signup.interactor.ChangeLanguageActivityInteractor;
import com.tez.androidapp.rewamp.signup.interactor.IChangeLanguageActivityInteractor;
import com.tez.androidapp.rewamp.signup.view.IChangeLanguageActivityView;

/**
 * Created by VINOD KUMAR on 8/9/2019.
 */
public class ChangeLanguageActivityPresenter implements IChangeLanguageActivityPresenter, IChangeLanguageActivityInteractorOutput {

    private final IChangeLanguageActivityView iChangeLanguageActivityView;
    private final IChangeLanguageActivityInteractor iChangeLanguageActivityInteractor;

    public ChangeLanguageActivityPresenter(IChangeLanguageActivityView iChangeLanguageActivityView) {
        this.iChangeLanguageActivityView = iChangeLanguageActivityView;
        this.iChangeLanguageActivityInteractor = new ChangeLanguageActivityInteractor(this);
    }

    @Override
    public void changeLanguage(@NonNull final String lang) {

        String languageCode = null;

        switch (lang) {

            case LocaleHelper.ENGLISH:
                languageCode = LocaleHelper.BACKEND_ENGLISH;
                break;

            case LocaleHelper.ROMAN:
                languageCode = LocaleHelper.BACKEND_ROMAN;
                break;

            case LocaleHelper.URDU:
                languageCode = LocaleHelper.BACKEND_URDU;
                break;
        }

        if (languageCode != null) {
            iChangeLanguageActivityView.showTezLoader();
            iChangeLanguageActivityInteractor.sendUpdatedLanguageCodeToServer(languageCode, lang);
        } else
            iChangeLanguageActivityView.onUpdateLanguageFailure();
    }

    @Override
    public void onLanguageSuccess(@NonNull String lang) {
        iChangeLanguageActivityView.setTezLoaderToBeDissmissedOnTransition();
        iChangeLanguageActivityView.onUpdateLanguageSuccess(lang);
    }

    @Override
    public void onLanguageFailure(int errorCode, String errorDescription) {
        iChangeLanguageActivityView.dismissTezLoader();
        iChangeLanguageActivityView.onUpdateLanguageFailure();
        iChangeLanguageActivityView.showError(errorCode);
    }
}
