package com.tez.androidapp.rewamp.advance.request.presenter;

import androidx.annotation.NonNull;

import java.util.List;

public interface ILoanRequiredStepsActivityPresenter {

    void getLoanRemainingSteps();

    void removeStep(@NonNull String step);

    void setLoanRemainingSteps(List<String> stepsLeft);

    void onClickBtContinue(@NonNull String nextStep);
}
