package com.tez.androidapp.rewamp.general.notification.data.source;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.tez.androidapp.app.general.feature.notification.models.netwrok.Notification;

public class NotificationDataSourceFactory extends DataSource.Factory {

    private MutableLiveData<NotificationDataSource> notificationLiveDataSource
            = new MutableLiveData<>();

    @Override
    public DataSource create() {
        NotificationDataSource notificationDataSource = new NotificationDataSource();
        notificationLiveDataSource.postValue(notificationDataSource);
        return notificationDataSource;
    }

    public MutableLiveData<NotificationDataSource> getItemNotificationDataSource() {
        return notificationLiveDataSource;
    }
}
