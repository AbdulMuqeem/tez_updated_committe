package com.tez.androidapp.rewamp.advance.request.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;

public interface ILoanReceiptActivityInteractor extends IBaseInteractor {

    void generateDisbursementReceipt();
}
