package com.tez.androidapp.repository.network.store;

import com.tez.androidapp.commons.utils.network.AuthHeaderInterceptor;

import okhttp3.OkHttpClient;

/**
 * Data store to access data from network with the added capability of authorization
 * <p>
 * Created  on 6/14/2017.
 */

public class BaseAuthCloudDataStore extends EncryptionBaseCloudDataStore {

    @Override
    protected OkHttpClient.Builder addMoreFeaturesToClient() {
        okHttpClientBuilder = super.addMoreFeaturesToClient();
        okHttpClientBuilder.addInterceptor(new AuthHeaderInterceptor());
        return okHttpClientBuilder;
    }
}
