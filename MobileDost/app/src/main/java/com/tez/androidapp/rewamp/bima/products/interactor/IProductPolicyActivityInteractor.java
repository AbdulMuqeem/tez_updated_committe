package com.tez.androidapp.rewamp.bima.products.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;
import com.tez.androidapp.rewamp.bima.claim.request.InitiateClaimRequest;

public interface IProductPolicyActivityInteractor extends IBaseInteractor {

    void getProductPolicy(int policyId);

    void initiateClaim(InitiateClaimRequest initiateClaimRequest);
}
