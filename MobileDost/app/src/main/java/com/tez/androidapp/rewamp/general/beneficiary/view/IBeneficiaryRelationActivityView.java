package com.tez.androidapp.rewamp.general.beneficiary.view;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.vertical.bima.shared.models.BimaRelation;

import java.util.List;

public interface IBeneficiaryRelationActivityView extends IBaseView {

    void initAdapter(@NonNull List<BimaRelation> bimaRelationList);

    void setClContentVisibility(int visibility);
}
