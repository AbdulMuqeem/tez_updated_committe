package com.tez.androidapp.rewamp.advance.request.entity;

import java.io.Serializable;

public class PricingDetail implements Serializable {

    private Integer id;
    private Integer walletProviderId;
    private Integer tenure;
    private Double pricing;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWalletProviderId() {
        return walletProviderId;
    }

    public void setWalletProviderId(Integer walletProviderId) {
        this.walletProviderId = walletProviderId;
    }

    public Integer getTenure() {
        return tenure;
    }

    public void setTenure(Integer tenure) {
        this.tenure = tenure;
    }

    public Double getPricing() {
        return pricing;
    }

    public void setPricing(Double pricing) {
        this.pricing = pricing;
    }
}
