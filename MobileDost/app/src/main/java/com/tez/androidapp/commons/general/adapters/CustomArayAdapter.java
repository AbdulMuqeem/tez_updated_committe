package com.tez.androidapp.commons.general.adapters;

import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.tez.androidapp.commons.general.adapters.callback.BaseArrayAdapterCallBack;
import com.tez.androidapp.commons.widgets.TezTextView;

import java.util.List;

/**
 * Created by FARHAN DHANANI on 8/15/2018.
 */
public class CustomArayAdapter<T> extends ArrayAdapter<T> {

    @Nullable
    private final BaseArrayAdapterCallBack baseArrayAdapterCallBack;
    private T selectedItem;
    private String itemHexColor;

    public CustomArayAdapter(Context context, int resource, @Nullable BaseArrayAdapterCallBack baseArrayAdapterCallBack) {
        super(context, resource);
        this.baseArrayAdapterCallBack = baseArrayAdapterCallBack;
    }

    public CustomArayAdapter(Context context, int resource, int textViewResourceId, @Nullable BaseArrayAdapterCallBack baseArrayAdapterCallBack) {
        super(context, resource, textViewResourceId);
        this.baseArrayAdapterCallBack = baseArrayAdapterCallBack;
    }

    public CustomArayAdapter(Context context, int resource, T[] objects, @Nullable BaseArrayAdapterCallBack baseArrayAdapterCallBack) {
        super(context, resource, objects);
        this.baseArrayAdapterCallBack = baseArrayAdapterCallBack;
    }

    public CustomArayAdapter(Context context, int resource, int textViewResourceId, T[] objects, BaseArrayAdapterCallBack baseArrayAdapterCallBack) {
        super(context, resource, textViewResourceId, objects);
        this.baseArrayAdapterCallBack = baseArrayAdapterCallBack;
    }

    public CustomArayAdapter(Context context, int resource, List<T> objects, @Nullable BaseArrayAdapterCallBack baseArrayAdapterCallBack) {
        super(context, resource, objects);
        this.baseArrayAdapterCallBack = baseArrayAdapterCallBack;
    }

    public CustomArayAdapter(Context context, int resource, int textViewResourceId, List<T> objects, BaseArrayAdapterCallBack baseArrayAdapterCallBack) {
        super(context, resource, textViewResourceId, objects);
        this.baseArrayAdapterCallBack = baseArrayAdapterCallBack;
    }

    public ArrayAdapter<T> setHexColorForItems(int colorForItems) {
        this.itemHexColor = String.format("#%06X", (0xFFFFFF & colorForItems));
        return this;
    }

    public ArrayAdapter<T> setHexColorForItems(String colorForItems) {
        this.itemHexColor = colorForItems;
        return this;
    }

    public @Nullable
    T getSelectedItem() {
        return this.selectedItem;
    }


    private void setColorToTextView(TezTextView view) {
        if (itemHexColor != null)
            view.setTextColor(Color.parseColor(itemHexColor));
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        if (view instanceof TezTextView)
            this.setColorToTextView((TezTextView) view);
        return view;
    }
}
