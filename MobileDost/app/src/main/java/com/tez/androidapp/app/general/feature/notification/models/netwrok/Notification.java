package com.tez.androidapp.app.general.feature.notification.models.netwrok;

import com.tez.androidapp.commons.utils.app.Utility;

import java.io.Serializable;

/**
 * Created  on 8/29/2017.
 */

public class Notification implements Serializable {

    private Integer id;
    private String notificationTypeName;
    private String messageText;
    private String createdTimestamp;

    public Notification(String notificationTypeName) {
        this.notificationTypeName = notificationTypeName;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getCreatedTimestamp() {
        return Utility.getFormattedDateWithUpdatedTimeZone(createdTimestamp, "yyyy-MM-dd'T'HH:mm:ss.SSSZ", "MMM dd, yyyy HH:mm");
    }

    public void setCreatedTimestamp(String createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public Integer getId(){
        return id;
    }

    public String getNotificationTypeName() {
        return notificationTypeName;
    }
}
