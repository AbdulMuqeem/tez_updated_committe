package com.tez.androidapp.app.general.feature.wallet.add.wallet.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.wallet.add.wallet.callbacks.AddWalletResendOTPCallback;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created  on 2/14/2017.
 */

public class AddWalletResendOTPRH extends BaseRH<BaseResponse> {

    private AddWalletResendOTPCallback addWalletResendOTPCallback;

    public AddWalletResendOTPRH(BaseCloudDataStore baseCloudDataStore, @Nullable AddWalletResendOTPCallback
            addWalletResendOTPCallback) {
        super(baseCloudDataStore);
        this.addWalletResendOTPCallback = addWalletResendOTPCallback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse baseResponse = value.response().body();
        if (addWalletResendOTPCallback != null) {
            if (baseResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
                addWalletResendOTPCallback.onAddWalletResendOTPSuccess();
            else onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
        }
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (addWalletResendOTPCallback != null) addWalletResendOTPCallback.onAddWalletResendOTPFailure(errorCode, message);
    }
}
