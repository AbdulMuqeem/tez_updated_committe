package com.tez.androidapp.rewamp.general.beneficiary.presenter;

/**
 * Created by VINOD KUMAR on 8/30/2019.
 */
public interface IManageBeneficiaryActivityPresenter {

    void getManageBeneficiaryPolicies();
}
