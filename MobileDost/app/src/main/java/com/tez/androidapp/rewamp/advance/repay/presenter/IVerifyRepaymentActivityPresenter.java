package com.tez.androidapp.rewamp.advance.repay.presenter;

public interface IVerifyRepaymentActivityPresenter extends IVerifyEasyPaisaRepaymentActivityPresenter {

    void initiateRepayment(int loanId, int mobileAccountId);

    void resendCode(int loanId, int mobileAccountId);
}
