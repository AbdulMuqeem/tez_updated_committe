package com.tez.androidapp.rewamp.bima.products.policy.RH;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.bima.products.policy.callbacks.PurchasePolicyCallback;
import com.tez.androidapp.rewamp.bima.products.policy.response.InitiatePurchasePolicyResponse;
import com.tez.androidapp.rewamp.bima.products.policy.response.PurchasePolicyResponse;
import com.tez.androidapp.rewamp.bima.products.policy.response.PurchasedInsurancePolicy;

public class PurchasePolicyRH extends BaseRH<PurchasePolicyResponse> {

    private PurchasePolicyCallback purchasePolicyCallback;

    public PurchasePolicyRH(BaseCloudDataStore baseCloudDataStore, PurchasePolicyCallback purchasePolicyCallback) {
        super(baseCloudDataStore);
        this.purchasePolicyCallback = purchasePolicyCallback;
    }

    @Override
    protected void onSuccess(Result<PurchasePolicyResponse> value) {
        PurchasePolicyResponse response = value.response().body();

        if (response != null) {
            if (isErrorFree(response))
                this.purchasePolicyCallback.purchasePolicySuccess(response.getFinalizeInsurancePolicyResponseDto());
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());
        }
        else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.purchasePolicyCallback.purchasePolicyFailure(errorCode, message);


    }
}
