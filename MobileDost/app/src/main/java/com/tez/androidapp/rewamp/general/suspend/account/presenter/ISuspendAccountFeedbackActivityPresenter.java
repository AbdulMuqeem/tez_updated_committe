package com.tez.androidapp.rewamp.general.suspend.account.presenter;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public interface ISuspendAccountFeedbackActivityPresenter {

    void sendUserFeedBackToServer(String feedback);
}
