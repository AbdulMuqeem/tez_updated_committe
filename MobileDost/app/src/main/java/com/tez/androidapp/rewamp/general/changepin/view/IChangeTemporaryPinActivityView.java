package com.tez.androidapp.rewamp.general.changepin.view;

import com.tez.androidapp.app.base.ui.IBaseView;

public interface IChangeTemporaryPinActivityView extends IBaseView {

    void onTemporaryPinChangedSuccessfully();
}
