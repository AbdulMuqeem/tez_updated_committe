package com.tez.androidapp.app.vertical.advance.loan.apply.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.advance.loan.apply.models.network.ProcessingFee;

import java.util.List;

public class ProcessingFeesResponse extends BaseResponse {
    private List<ProcessingFee> processingFees;

    public List<ProcessingFee> getProcessingFees(){
        return this.processingFees;
    }
}
