package com.tez.androidapp.app.general.feature.inivite.user.models.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by FARHAN DHANANI on 9/6/2018.
 */
public class GetReferralCodeRequest extends BaseRequest {

    public static final String METHOD_NAME = "v2/user/referralCode";
}
