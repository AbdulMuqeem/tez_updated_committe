package com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 6/16/2017.
 */

public class RepayLoanRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/loan/repay/";

    private Double amount;
    private Double lat;
    private Double lng;
    private Integer repaymentId;
    private Integer mobileAccountId;
    private String otp;
    private String pin;
    private String repaymentCorrelationId;


    public void setRepaymentCorrelationId(String repaymentCorrelationId) {
        this.repaymentCorrelationId = repaymentCorrelationId;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public int getMobileAccountId() {
        return mobileAccountId;
    }

    public void setMobileAccountId(int mobileAccountId) {
        this.mobileAccountId = mobileAccountId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getRepaymentId() {
        return repaymentId;
    }

    public void setRepaymentId(Integer repaymentId) {
        this.repaymentId = repaymentId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
