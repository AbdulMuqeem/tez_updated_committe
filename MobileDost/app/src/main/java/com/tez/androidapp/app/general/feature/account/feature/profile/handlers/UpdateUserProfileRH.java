package com.tez.androidapp.app.general.feature.account.feature.profile.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateUserProfileCallback;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsRH;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsResponse;

/**
 * Created by Rehman Murad Ali on 9/7/2017.
 */

public class UpdateUserProfileRH extends DashboardCardsRH<DashboardCardsResponse> {

    private UpdateUserProfileCallback updateUserProfileCallback;

    public UpdateUserProfileRH(BaseCloudDataStore baseCloudDataStore, UpdateUserProfileCallback updateUserProfileCallback) {
        super(baseCloudDataStore);
        this.updateUserProfileCallback = updateUserProfileCallback;
    }

    @Override
    protected void onSuccess(Result<DashboardCardsResponse> value) {
        super.onSuccess(value);
        DashboardCardsResponse baseResponse = value.response().body();
        //Check if GOOGLE Enxpiry date is edited or not
        //UserAuthCloudDataStore.newInstance().updateUserStatus();
        if (baseResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            updateUserProfileCallback.onUpdateUserProfileSuccess();
        else
            updateUserProfileCallback.onUpdateUserProfileFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        updateUserProfileCallback.onUpdateUserProfileFailure(errorCode, message);
    }
}
