package com.tez.androidapp.app.general.feature.wallet.add.wallet.models.network.dto.request;

import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 2/14/2017.
 */

public class AddWalletRequest extends BaseRequest {


    public static final String METHOD_NAME = "v2/user/wallet";
    @Nullable
    private Integer previousMobileUserAccountId;
    private String mobileAccountNumber;
    private Integer walletServiceProviderId;
    private Boolean isDefault;

    public void setPreviousMobileUserAccountId(@Nullable Integer previousMobileUserAccountId) {
        this.previousMobileUserAccountId = previousMobileUserAccountId;
    }

    public String getMobileAccountNumber() {
        return mobileAccountNumber;
    }

    public void setMobileAccountNumber(String mobileAccountNumber) {
        this.mobileAccountNumber = mobileAccountNumber;
    }

    public void setDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public Integer getWalletServiceProviderId() {
        return walletServiceProviderId;
    }

    public void setWalletServiceProviderId(Integer walletServiceProviderId) {
        this.walletServiceProviderId = walletServiceProviderId;
    }
}
