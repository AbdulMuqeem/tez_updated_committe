package com.tez.androidapp.rewamp.bima.products.policy.presenter;

import android.view.View;

import androidx.annotation.Nullable;

import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.bima.products.policy.adapters.InsuranceDuration;
import com.tez.androidapp.rewamp.bima.products.policy.interactor.IPurchasePolicyActivityInteractor;
import com.tez.androidapp.rewamp.bima.products.policy.interactor.PurchasePolicyActivityInteractor;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.response.InsuranceProductDetailResponse;
import com.tez.androidapp.rewamp.bima.products.policy.response.InsuranceProductDetails;
import com.tez.androidapp.rewamp.bima.products.policy.response.InsuranceProductPlan;
import com.tez.androidapp.rewamp.bima.products.policy.response.InsuranceTenure;
import com.tez.androidapp.rewamp.bima.products.policy.view.IPurchasePolicyActivityView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PurchasePolicyActivityPresenter
        implements IPurchasePolicyActivityPresenter, IPurchasePolicyActivityInteractorOutput {

    private final IPurchasePolicyActivityView iPurchasePolicyActivityView;
    private final IPurchasePolicyActivityInteractor iPurchasePolicyActivityInteractor;

    @Nullable
    private Date activationDate;
    @Nullable
    private String activationDateLabel;
    private String issuanceDate;
    private double finalPrice;
    private int tenureId;
    private InsuranceProductDetails insuranceProductDetails;

    public PurchasePolicyActivityPresenter(IPurchasePolicyActivityView iPurchasePolicyActivityView) {
        this.iPurchasePolicyActivityView = iPurchasePolicyActivityView;
        this.iPurchasePolicyActivityInteractor = new PurchasePolicyActivityInteractor(this);
    }


    @Override
    public void getInsuranceCoverage(int productId) {
        iPurchasePolicyActivityView.setClContentVisibility(View.GONE);
        iPurchasePolicyActivityView.showTezLoader();
        iPurchasePolicyActivityInteractor.getInsuranceCoverage(productId);
    }

    @Override
    public void onSuccessInsuranceProductDetail(InsuranceProductDetailResponse insuranceProductDetailResponse) {
        iPurchasePolicyActivityView.dismissTezLoader();
        iPurchasePolicyActivityView.setClContentVisibility(View.VISIBLE);
        iPurchasePolicyActivityView.setListeners();
        insuranceProductDetails = insuranceProductDetailResponse.getInsuranceProductDetails();

        this.iPurchasePolicyActivityView.setDetails(insuranceProductDetails.getDescription(),
                insuranceProductDetails.getMinCoverage(),
                insuranceProductDetails.getMaxCoverage());

        this.iPurchasePolicyActivityView.setUserWallet(insuranceProductDetails.getDefaultWallet());
        this.iPurchasePolicyActivityView.setInsuranceDurations(this.getInsuranceTenures(insuranceProductDetails.getMinCoverage()));

        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(Utility.getFormattedDate(insuranceProductDetails.getIssueDate(), Constants.INPUT_DATE_FORMAT));
            this.issuanceDate = Utility.getFormattedDate(calendar.getTimeInMillis(), Constants.OUTPUT_DATE_FORMAT);
            if (insuranceProductDetails.getActivationDays() != null) {
                calendar.add(Calendar.DATE, insuranceProductDetails.getActivationDays());
                this.activationDate = calendar.getTime();
                this.activationDateLabel = Utility.getFormattedDate(calendar.getTimeInMillis(), Constants.OUTPUT_DATE_FORMAT);
            }
            this.iPurchasePolicyActivityView.setActivationDateVisibility(this.activationDateLabel == null ? View.GONE : View.VISIBLE);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setValue(InsuranceDuration tenure) {
        this.iPurchasePolicyActivityView.setTvInsuranceCompValue(insuranceProductDetails.getInsuranceProvider());
        this.iPurchasePolicyActivityView.setIssuanceDate(this.issuanceDate);
        this.iPurchasePolicyActivityView.setActivationDate(this.activationDateLabel == null ? "-" : this.activationDateLabel);
        this.finalPrice = tenure.getPremium() - tenure.getDiscount();
        this.tenureId = tenure.getId();
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = activationDate == null ? Utility.getFormattedDate(insuranceProductDetails.getIssueDate(), Constants.INPUT_DATE_FORMAT) : activationDate;
            calendar.setTime(date);
            calendar.add(Calendar.DATE, tenure.getDayTenure());
            String expiryDateLabel = Utility.getFormattedDate(calendar.getTimeInMillis(), Constants.OUTPUT_DATE_FORMAT);
            this.iPurchasePolicyActivityView.setTvExpiryDateValue(expiryDateLabel);
        } catch (Exception e) {
            this.iPurchasePolicyActivityView.setTvExpiryDateValue("-");
        }
    }

    @Override
    public List<InsuranceDuration> getInsuranceTenures(double coverage) {
        List<InsuranceProductPlan> insuranceProductPlans = this.insuranceProductDetails.getProductPlans();
        List<InsuranceDuration> insuranceDurations = new ArrayList<>();
        for (InsuranceProductPlan insuranceProductPlan : insuranceProductPlans) {
            if (insuranceProductPlan.getCoverage() == coverage) {
                for (InsuranceTenure insuranceTenure : insuranceProductPlan.getTenures()) {
                    insuranceDurations.add(new InsuranceDuration(insuranceTenure.getId(),
                            insuranceTenure.getTenureInDays(),
                            insuranceTenure.getTenureTitle(),
                            insuranceTenure.getPremium(),
                            insuranceTenure.getDiscount()));
                }
            }
        }
        return insuranceDurations;
    }


    @Override
    public void onFailureInsuranceProductDetal(int errorCode, String message) {
        iPurchasePolicyActivityView.dismissTezLoader();
        iPurchasePolicyActivityView.showError(errorCode, (dialog, which) -> iPurchasePolicyActivityView.finishActivity());
    }

    @Override
    public void completeInitiatePurchasePolicyRequest(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest,
                                                      int productId,
                                                      int walletServiceProviderId) {
        initiatePurchasePolicyRequest.setActivationDate(this.activationDateLabel == null ? null : Utility.getFormattedDate(this.activationDateLabel, Constants.OUTPUT_DATE_FORMAT, Constants.BACKEND_DATE_FORMAT));
        initiatePurchasePolicyRequest.setFinalPrice(this.finalPrice);
        initiatePurchasePolicyRequest.setPin(null);
        initiatePurchasePolicyRequest.setProductId(productId);
        initiatePurchasePolicyRequest.setProductPlanId(this.tenureId);
        this.iPurchasePolicyActivityView.routeToTermsAndCondition(initiatePurchasePolicyRequest, walletServiceProviderId);
    }
}
