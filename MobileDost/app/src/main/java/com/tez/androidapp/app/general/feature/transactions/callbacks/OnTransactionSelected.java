package com.tez.androidapp.app.general.feature.transactions.callbacks;

import com.tez.androidapp.app.general.feature.transactions.models.network.Transaction;

import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;

/**
 * Created  on 9/2/2017.
 */

public interface OnTransactionSelected extends BaseRecyclerViewListener {

    void onSelect(Transaction transaction);
}
