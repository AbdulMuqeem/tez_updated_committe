package com.tez.androidapp.app.general.feature.faq.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.faq.callbacks.GetFAQsCallback;
import com.tez.androidapp.app.general.feature.faq.models.network.dto.response.GetFAQsResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created  on 2/28/2017.
 */

public class GetFAQsRH extends BaseRH<GetFAQsResponse> {

    private GetFAQsCallback getFAQsCallback;

    public GetFAQsRH(BaseCloudDataStore baseCloudDataStore, GetFAQsCallback getFAQsCallback) {
        super(baseCloudDataStore);
        this.getFAQsCallback = getFAQsCallback;
    }

    @Override
    protected void onSuccess(Result<GetFAQsResponse> value) {
        GetFAQsResponse getFAQsResponse = value.response().body();
        if (getFAQsCallback != null) getFAQsCallback.onGetFAQsSuccess(getFAQsResponse.getFaqs());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (getFAQsCallback != null) getFAQsCallback.onGetFAQsError(errorCode, message);
    }
}
