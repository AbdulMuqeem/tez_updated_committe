package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by FARHAN DHANANI on 12/3/2018.
 */
public class ValidateInfoRequest extends BaseRequest {
    public static final String METHOD_NAME = "v1/user/validateInfo";
    private String cnic;
    private String mobileNumber;


    public ValidateInfoRequest(String cnic, String mobileNumber){
        this.cnic = cnic;
        this.mobileNumber = mobileNumber;
    }

    public String getCnic() {
        return this.cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getMobileNumber() {
        return this.mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
