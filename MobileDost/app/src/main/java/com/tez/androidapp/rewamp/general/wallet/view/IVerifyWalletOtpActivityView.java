package com.tez.androidapp.rewamp.general.wallet.view;

import androidx.annotation.StringRes;

import com.tez.androidapp.app.base.ui.IBaseView;

public interface IVerifyWalletOtpActivityView extends IBaseView {

    void showEasyPaisaError(@StringRes int message);

    void showWalletDoesNotExistError(int serviceProviderId, @StringRes int message);

    void showWalletDoesNotMatchCnicError(int serviceProviderId, @StringRes int message);

    void routeToCompleteYourProfileActivity();

    String getFormattedErrorMessage(int serviceProviderId, @StringRes int message);

    void setPinViewOtpClearText();

    void startWalletAddedActivity();

    void setPinViewOtpEnabled(boolean enabled);

    void setTvResendCodeEnabled(boolean enabled);

    void startTimer();
}
