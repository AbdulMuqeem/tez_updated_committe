package com.tez.androidapp.app.general.feature.authentication.signup.callbacks;

/**
 * Created by FARHAN DHANANI on 11/1/2018.
 */
public interface VerifyUserReferralCodeCallback {
    void userReferalCodeVerifiedSuccessfully();
    void userReferalCodeVerificationFailed(int errorCode, String message);
}
