package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.tez.androidapp.R;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.GetUserPictureRequest;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.authorized.alert.image.popup.DefaultUIForAlertImage;
import com.tez.androidapp.commons.utils.authorized.alert.image.popup.RenderAlertImageOnUI;
import com.tez.androidapp.rewamp.TezDrawableHelper;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.UUID;

/**
 * Created by VINOD KUMAR on 7/25/2019.
 */
public class UploadImageCardView extends TezCardView {


    @BindView(R.id.ivUploadedImage)
    private TezImageView ivUploadedImage;

    @BindView(R.id.ivTakePicture)
    private TezImageView ivTakePicture;

    @BindView(R.id.ivEditPicture)
    private TezImageView ivEditPicture;

    @BindView(R.id.tvDescription)
    private TezTextView tvDescription;

    @BindView(R.id.pbImageLoad)
    private ProgressBar pbImageLoad;

    @BindView(R.id.viewOverlay)
    private View viewOverlay;

    @BindView(R.id.ivError)
    private TezImageView ivError;

    public UploadImageCardView(@NonNull Context context) {
        super(context);
    }

    public UploadImageCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public UploadImageCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    public void setImageLoaderVisibility(boolean visibility) {
        this.pbImageLoad.setVisibility(visibility ? VISIBLE : GONE);
        this.viewOverlay.setVisibility(visibility ? VISIBLE : GONE);
        this.ivError.setVisibility(GONE);
    }

    private void init(@NonNull Context context, @NonNull AttributeSet attrs) {
        inflate(context, R.layout.upload_image_card, this);
        ViewBinder.bind(this, this);
        attachClickEffect(NO_EFFECT, this);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.UploadImageCardView);
        try {

            String title = typedArray.getString(R.styleable.UploadImageCardView_title);
            String descriptionText = typedArray.getString(R.styleable.UploadImageCardView_description_text);
            int descriptionTextColour = typedArray.getColor(R.styleable.UploadImageCardView_description_text_colour, getColor(context, R.color.textViewTextColorBlack));
            Drawable placeholderImage = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.UploadImageCardView_placeholder_image);
            setTitle(title);
            setDescriptionText(descriptionText, descriptionTextColour);
            setPlaceholderImage(placeholderImage);

        } finally {
            typedArray.recycle();
        }
    }

    @NonNull
    @Override
    public String getLabel() {
        return tvDescription.getLabel();
    }

    public void onImageCaptured(@NonNull String path, @DrawableRes int placeholderId) {
        ivUploadedImage.setBackgroundResource(R.drawable.cnic_filled_rect_green_bg);
        Uri serverUri = Uri.parse(path);
        String signature = UUID.randomUUID().toString();
        DefaultUIForAlertImage uiForAlertImage = new DefaultUIForAlertImage(this.ivUploadedImage){
            @Override
            public void onInitiate() {
                super.onInitiate();
                setImageLoaderVisibility(true);
                ivError.setVisibility(GONE);
                ivError.setDoubleTapSafeOnClickListener(null);
            }

            @Override
            public void onSuccess() {
                super.onSuccess();
                setImageLoaderVisibility(false);
                ivError.setVisibility(GONE);
                ivError.setDoubleTapSafeOnClickListener(null);
                tvDescription.setTextColor(getColor(getContext(), R.color.textViewTextColorGreen));
                tvDescription.setTypeface(ResourcesCompat.getFont(getContext(), R.font.barlow_medium));
                ivTakePicture.setVisibility(GONE);
                ivEditPicture.setVisibility(VISIBLE);
            }

            @Override
            public void onError() {
                super.onError();
                setImageLoaderVisibility(false);
                ivError.setVisibility(VISIBLE);
                viewOverlay.setVisibility(VISIBLE);
                ivError.setDoubleTapSafeOnClickListener(v-> onImageCaptured(path, placeholderId));
            }
        };
        uiForAlertImage.setErrorImage(placeholderId);
        uiForAlertImage.setPlaceholder(placeholderId);
        uiForAlertImage.setTransformation(new CenterCrop(), new RoundedCorners(dpToPx(getContext(), 4)));
        RenderAlertImageOnUI.renderDefaultImageUIAlert(uiForAlertImage, serverUri, signature);
    }


    private void loadImage(TezImageView imageView, @NonNull String path) {
        Glide.with(getContext())
                .load(path)
                .transform(new CenterCrop(), new RoundedCorners(dpToPx(getContext(), 4)))
                .into(imageView);
    }


    public void setActive(boolean isActive) {
        findViewById(R.id.ivTakePicture).setEnabled(isActive);
        tvDescription.setTextColor(isActive ? getColor(getContext(), R.color.textViewTextColorGreen) : getColor(getContext(), R.color.textViewTextColorBlack));
        tvDescription.setTypeface(isActive ? ResourcesCompat.getFont(getContext(), R.font.barlow_semi_bold) : ResourcesCompat.getFont(getContext(), R.font.barlow_medium));
        ivTakePicture.setColorFilter(isActive ? getColor(getContext(), R.color.textViewTextColorGreen) : getColor(getContext(), R.color.editTextBottomLineColorGrey));
    }

    public void setOnTakePictureClickListener(@Nullable DoubleTapSafeOnClickListener listener) {
        ivTakePicture.setOnClickListener(listener);
    }


    public void setOnTextViewDetailClickListener(@NonNull DoubleTapSafeOnClickListener listener) {
        tvDescription.setOnClickListener(listener);
    }


    public void setOnEditPictureClickListener(@Nullable DoubleTapSafeOnClickListener listener) {
        ivEditPicture.setOnClickListener(listener);
    }

    public void setTitle(String title) {
        TezTextView textView = findViewById(R.id.tvTitle);
        textView.setText(title);
    }

    private void setDescriptionText(String text, int color) {
        tvDescription.setTextColor(color);
        tvDescription.setText(text);
    }

    public void setPlaceholderImage(Drawable placeholderImage) {
        if (placeholderImage != null) {
            ivUploadedImage.setImageDrawable(placeholderImage);
        }
    }

    public void resetUploadImageCardView(){
        this.ivUploadedImage.setImageResource(R.drawable.img_cnic_front_placeholder);
        this.ivEditPicture.setVisibility(View.GONE);
        this.ivTakePicture.setVisibility(View.VISIBLE);
    }
}
