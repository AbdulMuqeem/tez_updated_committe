package com.tez.androidapp.app.general.feature.authentication.shared.handler;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.authentication.shared.callbacks.UserInfoCallback;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.dto.response.UserInfoResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created  on 2/9/2017.
 */

public class UserInfoRH extends BaseRH<UserInfoResponse> {

    private UserInfoCallback userInfoCallback;

    public UserInfoRH(BaseCloudDataStore baseCloudDataStore, @Nullable UserInfoCallback userInfoCallback) {
        super(baseCloudDataStore);
        this.userInfoCallback = userInfoCallback;
    }

    @Override
    protected void onSuccess(Result<UserInfoResponse> value) {
        UserInfoResponse userInfoResponse = value.response().body();
        if (userInfoResponse != null) {
            if (userInfoResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
                //MDPreferenceManager.setPrincipalName(userInfoResponse.getPrincipalName());
                if (userInfoCallback != null)
                    userInfoCallback.onUserInfoSuccess(userInfoResponse);
            } else {
                onFailure(userInfoResponse.getStatusCode(), userInfoResponse.getErrorDescription());
            }
        } else onFailure(0, null);
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        message = message == null || message.isEmpty() ? "Moaziz saarif, hum aap ko filhaal services farhaam nahi kar sakte. Baraye meherbani thori dair baad koshish karien." : message;
        if (userInfoCallback != null)
            userInfoCallback.onUserInfoError(errorCode, message);
    }
}
