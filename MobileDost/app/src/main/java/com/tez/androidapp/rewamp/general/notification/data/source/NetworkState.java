package com.tez.androidapp.rewamp.general.notification.data.source;

public class NetworkState {

    public enum Status{
        RUNNING,
        SUCCESS,
        FAILED
    }


    private final Status status;
    private final int errorCode;

    public static final NetworkState LOADED;
    public static final NetworkState LOADING;

    public NetworkState(Status status, int errorCode) {
        this.status = status;
        this.errorCode = errorCode;
    }

    static {
        LOADED=new NetworkState(Status.SUCCESS,-1);
        LOADING=new NetworkState(Status.RUNNING,-2);
    }

    public Status getStatus() {
        return status;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
