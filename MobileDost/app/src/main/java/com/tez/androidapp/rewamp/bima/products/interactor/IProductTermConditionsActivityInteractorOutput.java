package com.tez.androidapp.rewamp.bima.products.interactor;

import com.tez.androidapp.rewamp.bima.products.callback.ProductDetailTermConditionCallback;
import com.tez.androidapp.rewamp.bima.products.callback.ProductTermConditionsCallback;

public interface IProductTermConditionsActivityInteractorOutput extends ProductTermConditionsCallback,
        ProductDetailTermConditionCallback {
}
