package com.tez.androidapp.app.general.feature.account.feature.profile.callbacks;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Option;

import java.util.List;

public interface QuestionOptionsCallBack {

    void onQuestionOptionsSuccess(List<Option> data);
    void onQuestionOptionsFailure(int errorCode, String message);
}
