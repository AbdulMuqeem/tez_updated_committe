package com.tez.androidapp.services.models;

import android.net.Uri;

import androidx.annotation.Nullable;

public interface ITezShortDynamicLink {

    @Nullable
    Uri getShortLink();
}
