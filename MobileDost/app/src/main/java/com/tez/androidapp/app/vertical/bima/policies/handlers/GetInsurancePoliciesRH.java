package com.tez.androidapp.app.vertical.bima.policies.handlers;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.vertical.bima.policies.callbacks.GetInsurancePoliciesCallback;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.response.GetInsurancePoliciesResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by Rehman Murad Ali on 8/25/2017.
 */

public class GetInsurancePoliciesRH extends BaseRH<GetInsurancePoliciesResponse> {

    @NonNull
    private GetInsurancePoliciesCallback getInsurancePoliciesCallback;

    public GetInsurancePoliciesRH(BaseCloudDataStore baseCloudDataStore, @NonNull GetInsurancePoliciesCallback getInsurancePoliciesCallback) {
        super(baseCloudDataStore);
        this.getInsurancePoliciesCallback = getInsurancePoliciesCallback;
    }

    @Override
    protected void onSuccess(Result<GetInsurancePoliciesResponse> value) {
        GetInsurancePoliciesResponse response = value.response().body();

        if (response != null) {

            if (isErrorFree(response))
                getInsurancePoliciesCallback.onGetInsurancePoliciesSuccess(response);
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());

        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        getInsurancePoliciesCallback.onGetInsurancePoliciesFailure(errorCode, message);
    }
}
