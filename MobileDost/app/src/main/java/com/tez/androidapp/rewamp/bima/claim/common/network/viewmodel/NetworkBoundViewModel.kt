package com.tez.androidapp.rewamp.bima.claim.common.network.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tez.androidapp.rewamp.bima.claim.common.network.model.NetworkState

abstract class NetworkBoundViewModel : ViewModel() {
    val networkCallMutableLiveData = MutableLiveData<NetworkState>()
}