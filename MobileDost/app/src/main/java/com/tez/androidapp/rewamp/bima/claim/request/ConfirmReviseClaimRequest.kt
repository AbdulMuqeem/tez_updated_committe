package com.tez.androidapp.rewamp.bima.claim.request

import com.tez.androidapp.app.base.request.BaseRequest

class ConfirmReviseClaimRequest(
        val claimId: Int,
        val documentIdList: List<Int>,
) : BaseRequest() {

    companion object {
        const val METHOD_NAME = "v1/claim/revise/confirm"
    }
}