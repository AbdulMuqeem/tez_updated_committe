package com.tez.androidapp.rewamp.advance.request.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.rewamp.advance.request.request.LoanApplyRequest;
import com.tez.androidapp.rewamp.advance.request.callback.LoanApplyCallback;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.LoanCloudDataStore;
import com.tez.androidapp.rewamp.advance.request.callback.LoanRemainingStepsCallback;
import com.tez.androidapp.rewamp.advance.request.presenter.IAdvanceTermsAndConditionActivityInteractorOutput;

import java.util.List;

public class AdvanceTermsAndConditionActivityInteractor implements IAdvanceTermsAndConditionActivityInteractor, LoanRemainingStepsCallback {

    private final IAdvanceTermsAndConditionActivityInteractorOutput iAdvanceTermsAndConditionActivityInteractorOutput;

    public AdvanceTermsAndConditionActivityInteractor(IAdvanceTermsAndConditionActivityInteractorOutput iAdvanceTermsAndConditionActivityInteractorOutput) {
        this.iAdvanceTermsAndConditionActivityInteractorOutput = iAdvanceTermsAndConditionActivityInteractorOutput;
    }

    @Override
    public void getLoanRemainingSteps() {
        LoanCloudDataStore.getInstance().getLoanRemainingSteps(this);
    }

    @Override
    public void onGetLoanRemainingStepsSuccess(List<String> stepsLeft) {
        iAdvanceTermsAndConditionActivityInteractorOutput.onGetLoanRemainingStepsSuccess(stepsLeft);
    }

    @Override
    public void onGetLoanRemainingStepsFailure(int errorCode, String message) {
        if (Utility.isUnauthorized(errorCode))
            getLoanRemainingSteps();
        else
            iAdvanceTermsAndConditionActivityInteractorOutput.onGetLoanRemainingStepsFailure(errorCode, message);
    }

    @Override
    public void applyLoan(@NonNull LoanApplyRequest loanApplyRequest) {
        LoanCloudDataStore.getInstance().applyForLoan(loanApplyRequest, new LoanApplyCallback() {
            @Override
            public void onLoanApplySuccess(int statusCode) {
                iAdvanceTermsAndConditionActivityInteractorOutput.onLoanApplySuccess(statusCode);
            }

            @Override
            public void onLoanApplyFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    applyLoan(loanApplyRequest);
                else
                    iAdvanceTermsAndConditionActivityInteractorOutput.onLoanApplyFailure(errorCode, message);
            }
        });
    }
}
