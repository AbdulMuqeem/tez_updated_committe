package com.tez.androidapp.app.general.feature.wallet.my.wallet.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;

import java.util.List;

/**
 * Created by Rehman Murad Ali on 12/14/2017.
 */

public class GetAllWalletResponse extends BaseResponse {

    private List<Wallet> wallets;

    public List<Wallet> getWalletList() {
        return wallets;
    }

    @Override
    public String toString() {
        return "GetAllWalletResponse{" +
                "walletList=" + wallets +
                '}';
    }

    public void setWallets(List<Wallet> wallets) {
        this.wallets = wallets;
    }
}
