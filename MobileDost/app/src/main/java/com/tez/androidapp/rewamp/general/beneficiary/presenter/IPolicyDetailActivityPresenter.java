package com.tez.androidapp.rewamp.general.beneficiary.presenter;

public interface IPolicyDetailActivityPresenter {

    void getPolicyDetails(int policyId);
}
