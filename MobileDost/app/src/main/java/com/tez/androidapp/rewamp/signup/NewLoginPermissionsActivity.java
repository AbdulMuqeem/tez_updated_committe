package com.tez.androidapp.rewamp.signup;

import android.content.Intent;

import androidx.annotation.Nullable;

import com.tez.androidapp.commons.models.network.dto.request.UserInfoRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.signup.router.LoginNumberVerificationActivityRouter;
import com.tez.androidapp.rewamp.signup.router.NewLoginActivityRouter;
import com.tez.androidapp.rewamp.signup.router.NewLoginPermissionsAcivityRouter;

import java.util.List;

public class NewLoginPermissionsActivity  extends PermissionActivity {

    @Override
    public void onAppStart(boolean inactiveTimeExceeded) {

    }

    @Override
    protected List<String> getRequiredPermissions() {
        return Utility.getSignupPermissions(this);
    }

    @Override
    protected void onAllPermissionsGranted() {
        super.onAllPermissionsGranted();
        initiateLogin();
    }

    protected void initiateLogin() {
        startLoginNumberVerificationActivity(getMobileNumber(), getPin());
    }


    private void startLoginNumberVerificationActivity(String mobileNumber, String pin){
        LoginNumberVerificationActivityRouter.createInstance().setDependenciesAndRoute(this,
                mobileNumber, pin, getSocialId(), getSocialType());
        finish();
    }

    private String getMobileNumber() {
        Intent intent = getIntent();
        return intent.hasExtra(NewLoginActivityRouter.MOBILE_NUMBER)
                ? intent.getStringExtra(NewLoginActivityRouter.MOBILE_NUMBER)
                : "";
    }

    private String getSocialId(){
        return getIntent().getStringExtra(NewLoginPermissionsAcivityRouter.SOCIAL_ID);
    }

    private Integer getSocialType(){
        return getIntent().getIntExtra(NewLoginPermissionsAcivityRouter.SOCIAL_TYPE,
                Utility.PrincipalType.MOBILE_NUMBER.getValue());
    }

    private String getPin(){
        return getIntent().getStringExtra(NewLoginPermissionsAcivityRouter.PIN);
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    protected String getScreenName() {
        return "NewLoginPermissionsActivity";
    }
}
