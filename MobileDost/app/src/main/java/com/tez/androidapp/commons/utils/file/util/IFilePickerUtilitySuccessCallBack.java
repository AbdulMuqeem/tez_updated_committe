package com.tez.androidapp.commons.utils.file.util;

/**
 * Created by FARHAN DHANANI on 7/10/2018.
 */
public interface IFilePickerUtilitySuccessCallBack {
    void onSuccess(String filePath);
}
