package com.tez.androidapp.rewamp.bima.claim.callback

import com.tez.androidapp.rewamp.bima.claim.response.UploadDocumentResponse

interface UploadDocumentCallback {

    fun onSuccessUploadDocument(uploadDocumentResponse: UploadDocumentResponse)

    fun onFailureUploadDocument(errorCode: Int, message: String?)
}