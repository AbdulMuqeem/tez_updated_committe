package com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models;

import java.io.Serializable;

/**
 * Created by FARHAN DHANANI on 9/18/2018.
 */
public class Answer implements Serializable {

    private String roman;
    private String english;

    public String getRoman() {
        return roman;
    }

    public void setRoman(String roman) {
        this.roman = roman;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }
}
