package com.tez.androidapp.rewamp.bima.products.policy.interactor;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.rewamp.bima.products.policy.callbacks.InitiatePurchasePolicyCallback;
import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;
import com.tez.androidapp.rewamp.bima.products.policy.request.PurchasePolicyRequest;

public interface IEasyPaisaInsurancePaymentActivityInteractor {
    void initiatePurchasePolicy(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest);

    void initiatePurchasePolicy(@NonNull InitiatePurchasePolicyRequest initiateRepaymentRequest,
                                @NonNull String pin,
                                @Nullable Location location);

    void purchasePolicy(PurchasePolicyRequest purchasePolicyRequest);

    void resendCode(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest, InitiatePurchasePolicyCallback callback);
}
