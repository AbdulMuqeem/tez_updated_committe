package com.tez.androidapp.rewamp.bima.claim.revise.router

import android.content.Intent
import com.tez.androidapp.app.base.ui.activities.BaseActivity
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter
import com.tez.androidapp.rewamp.bima.claim.revise.view.ReviseClaimActivity

class ReviseClaimActivityRouter : BaseActivityRouter() {


    fun setDependenciesAndRoute(from: BaseActivity,
                                productId: Int,
                                claimId: Int,
                                rejectedDocumentIds: String?,
                                comments: Array<String>?) {
        val intent = Intent(from, ReviseClaimActivity::class.java)
        intent.putExtra(PRODUCT_ID, productId)
        intent.putExtra(CLAIM_ID, claimId)
        intent.putExtra(REJECTED_DOCUMENTS_ID_LIST, rejectedDocumentIds)
        intent.putExtra(COMMENTS, comments)
        route(from, intent)
    }

    class Dependencies(intent: Intent) {
        val productId: Int = intent.getIntExtra(PRODUCT_ID, -100)
        val claimId: Int = intent.getIntExtra(CLAIM_ID, -100)
        val rejectedDocumentIds: String? = intent.getStringExtra(REJECTED_DOCUMENTS_ID_LIST)
        val comments: Array<String>? = intent.getStringArrayExtra(COMMENTS)
    }

    companion object {
        private const val PRODUCT_ID = "PRODUCT_ID"
        private const val CLAIM_ID = "CLAIM_ID"
        private const val COMMENTS = "COMMENTS"
        private const val REJECTED_DOCUMENTS_ID_LIST = "ID_LIST"

        fun getDependencies(intent: Intent) = Dependencies(intent)
    }
}