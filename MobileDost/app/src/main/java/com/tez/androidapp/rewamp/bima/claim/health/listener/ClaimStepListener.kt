package com.tez.androidapp.rewamp.bima.claim.health.listener

import com.tez.androidapp.rewamp.bima.claim.model.InsuranceClaimConfirmationDto

interface ClaimStepListener {

    fun setStep(stepNumber: Int)

    fun navigate(currentStep: Int)

    fun onClaimSubmitted(insuranceClaimConfirmationDto: InsuranceClaimConfirmationDto)
}