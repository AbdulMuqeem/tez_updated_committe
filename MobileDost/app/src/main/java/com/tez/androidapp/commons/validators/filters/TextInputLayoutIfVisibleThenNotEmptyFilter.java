package com.tez.androidapp.commons.validators.filters;

import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.tez.androidapp.commons.validators.annotations.TextInputLayoutIfVisibleThenNotEmpty;
import com.tez.androidapp.commons.validators.annotations.TextInputLayoutRegex;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;

import net.tez.validator.library.filters.Filter;
import net.tez.validator.library.utils.TextUtil;

public class TextInputLayoutIfVisibleThenNotEmptyFilter implements Filter<TezTextInputLayout, TextInputLayoutIfVisibleThenNotEmpty> {
    @Override
    public boolean isValidated(@NonNull TezTextInputLayout view, @NonNull TextInputLayoutIfVisibleThenNotEmpty annotation) {
        EditText editText = view.getEditText();
        String text = editText == null ?
                null :
                editText.getText() == null ?
                        null : editText.getText().toString();
        return view.getVisibility()== View.GONE
                || editText==null
                || editText.getVisibility() == View.GONE
                || TextUtil.isNotEmpty(text);
    }
}