package com.tez.androidapp.app;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.telephony.TelephonyManager;

import androidx.annotation.Nullable;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.multidex.MultiDexApplication;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.tez.androidapp.BuildConfig;
import com.tez.androidapp.app.base.adapters.TezLogFileStrategy;
import com.tez.androidapp.app.base.observer.SessionTimeOutObserver;
import com.tez.androidapp.commons.locale.LocaleHelper;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.pref.GenericObjectParser;
import com.tez.androidapp.commons.pref.GenericStringParser;
import com.tez.androidapp.commons.pref.KeyStoreEncryptionStrategy;
import com.tez.androidapp.commons.receivers.BluetoothReceiver;
import com.tez.androidapp.commons.receivers.ConnectivityBroadcastReceiver;
import com.tez.androidapp.commons.receivers.WifiReceiver;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.services.TezAnalytics;
import com.tez.androidapp.services.TezCrashlytics;

import net.tez.logger.library.adapters.AndroidLogAdapter;
import net.tez.logger.library.core.TezLog;
import net.tez.logger.library.formats.StyleLogFormatStrategy;
import net.tez.storage.library.adapters.SharedPreferenceAdapter;
import net.tez.storage.library.core.Storage;

import java.util.ArrayList;

/**
 * Created  on 12/2/2016.
 */

public class MobileDostApplication extends MultiDexApplication {

    private static MobileDostApplication mobileDostApplication;
    private boolean isProductionBuildChangeable = BuildConfig.FLAVOR_env.equalsIgnoreCase("prod");

    public static MobileDostApplication getInstance() {
        return mobileDostApplication;
    }

    private static void setInstance(MobileDostApplication instance) {
        mobileDostApplication = instance;
    }

    public static Context getAppContext() {
        return MobileDostApplication.getInstance().getApplicationContext();
    }

    @Nullable
    public static TelephonyManager getTelephonyManagerService() {
        return (TelephonyManager) getAppContext().getSystemService(Context.TELEPHONY_SERVICE);
    }

    @Nullable
    public static ResolveInfo getActivityResolverForInternet() {
        return getAppContext().getPackageManager()
                .resolveActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://"))
                        , PackageManager.MATCH_DEFAULT_ONLY);
    }

    public static String[] getSignupPermissions() {
        ArrayList<String> perms = new ArrayList<>();
        perms.add(Manifest.permission.READ_PHONE_STATE);
        perms.add(Manifest.permission.CALL_PHONE);
        perms.add(Manifest.permission.ACCESS_NETWORK_STATE);
        perms.add(Manifest.permission.INTERNET);
        perms.add(Manifest.permission.ACCESS_FINE_LOCATION);
        perms.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        perms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        perms.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        perms.add(Manifest.permission.READ_CALL_LOG);
        perms.add(Manifest.permission.RECEIVE_SMS);
        perms.add(Manifest.permission.READ_SMS);
        return perms.toArray(new String[0]);
    }

    public static String[] OptionalPermissions() {
        ArrayList<String> perms = new ArrayList<>();
        perms.add(Manifest.permission.READ_CONTACTS);
        perms.add(Manifest.permission.READ_CALENDAR);
        return perms.toArray(new String[0]);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setInstance(this);
        TezAnalytics.initialize();
        Utility.initNotificationChannels(this);
        LocaleHelper.updateLanguage(this, LocaleHelper.getSelectedLanguage(this));
        TezAnalytics.getInstance().setAnalyticsCollectionEnabled(enableFireBaseCrashReport());
        TezCrashlytics.getInstance().setCrashlyticsCollectionEnabled(enableFireBaseCrashReport());
        ProcessLifecycleOwner.get().getLifecycle().addObserver(SessionTimeOutObserver.getInstance());
        if (!FacebookSdk.isInitialized())
            FacebookSdk.sdkInitialize(this);
        AppEventsLogger.activateApp(this);
        FacebookSdk.setIsDebugEnabled(Utility.isProdReleaseBuild());
        initTezLoggerAdapter();
        initStorage();
        register();
    }

    private boolean enableFireBaseCrashReport() {
        return Utility.isProdReleaseBuild() ||
                (BuildConfig.FLAVOR_env.equalsIgnoreCase("qa") && !BuildConfig.IS_DEBUG);
    }

    public void initTezLoggerAdapter() {
        TezLog.addAdapter(new AndroidLogAdapter(
                StyleLogFormatStrategy.newBuilder()
                        .logStrategy(new TezLogFileStrategy())
                        .build()) {
            @Override
            public boolean isLoggable(int priority, @Nullable String tag) {
                return !Constants.IS_EXTERNAL_BUILD;
            }
        });
    }

    public void initStorage() {
        Storage.setAdapter(new SharedPreferenceAdapter.Builder(this)
                .setFileName(MDPreferenceManager.PREFS_NAME)
                .setMode(Context.MODE_PRIVATE)
                .setTextTransformStrategy(new KeyStoreEncryptionStrategy())
                .setObjectToStringParser(new GenericStringParser())
                .setStringToObjectParser(new GenericObjectParser())
                .build());
    }

    public boolean isProductionBuildChangeable() {
        return isProductionBuildChangeable;
    }

    public void setProductionBuildChangeable(boolean productionBuildChangeable) {
        isProductionBuildChangeable = productionBuildChangeable;
    }

    private void register() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            new WifiReceiver().register(this);
            new BluetoothReceiver().register(this);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            new ConnectivityBroadcastReceiver().register(this);
    }

    public String[] getNumberVerificationPermissions() {
        return new String[]{
                Manifest.permission.RECEIVE_SMS,
                Manifest.permission.READ_SMS
        };
    }
}
