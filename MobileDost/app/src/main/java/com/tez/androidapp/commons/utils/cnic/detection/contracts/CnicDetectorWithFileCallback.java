package com.tez.androidapp.commons.utils.cnic.detection.contracts;



import androidx.annotation.NonNull;

import com.tez.androidapp.commons.utils.cnic.detection.model.RetumDataModel;

import java.io.File;

public interface CnicDetectorWithFileCallback {
    void onCNICDetectionSuccess(@NonNull  File file,@NonNull RetumDataModel retumDataModel);
    void onCNICDetectionFailure();
}
