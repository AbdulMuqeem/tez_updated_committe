package com.tez.androidapp.rewamp.bima.products.viewmodel

import androidx.lifecycle.MutableLiveData
import com.tez.androidapp.app.vertical.bima.policies.callbacks.GetInsurancePoliciesCallback
import com.tez.androidapp.app.vertical.bima.policies.models.network.Policy
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.response.GetInsurancePoliciesResponse
import com.tez.androidapp.commons.utils.app.Utility
import com.tez.androidapp.repository.network.store.BimaCloudDataStore
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Failure
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Loading
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Success
import com.tez.androidapp.rewamp.bima.claim.common.network.viewmodel.NetworkBoundViewModel

class PoliciesViewModel : NetworkBoundViewModel() {
    private var policyListLiveData: MutableLiveData<List<Policy>>? = null
    fun getPolicyListLiveData(): MutableLiveData<List<Policy>> {
        if (policyListLiveData == null) {
            policyListLiveData = MutableLiveData()
            getInsurancePolicies()
        }
        return policyListLiveData!!
    }

    private fun getInsurancePolicies() {
        networkCallMutableLiveData.value = Loading
        BimaCloudDataStore.getInstance().getInsurancePolicies(object : GetInsurancePoliciesCallback {
            override fun onGetInsurancePoliciesSuccess(getInsurancePoliciesResponse: GetInsurancePoliciesResponse) {
                policyListLiveData!!.value = getInsurancePoliciesResponse.policies
                networkCallMutableLiveData.value = Success
            }

            override fun onGetInsurancePoliciesFailure(errorCode: Int, message: String) {
                if (Utility.isUnauthorized(errorCode))
                    getInsurancePolicies()
                else
                    networkCallMutableLiveData.value = Failure(errorCode)
            }
        })
    }
}