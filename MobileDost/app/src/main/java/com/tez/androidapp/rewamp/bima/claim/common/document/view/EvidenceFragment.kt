package com.tez.androidapp.rewamp.bima.claim.common.document.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import com.tez.androidapp.R
import com.tez.androidapp.rewamp.bima.claim.common.document.adapter.EvidenceAdapter
import com.tez.androidapp.rewamp.bima.claim.common.document.adapter.EvidenceAdapter.OnEvidenceUpdateListener
import com.tez.androidapp.rewamp.bima.claim.common.document.router.UploadDocumentActivityRouter
import com.tez.androidapp.rewamp.bima.claim.health.base.BaseClaimStepFragment
import com.tez.androidapp.rewamp.bima.claim.health.viewmodel.EvidenceViewModel
import kotlinx.android.synthetic.main.fragment_evidence.*

/**
 * Created by Vinod Kumar on 4/20/2020
 */
class EvidenceFragment : BaseClaimStepFragment(), OnEvidenceUpdateListener {

    private val args: EvidenceFragmentArgs by navArgs()
    private val viewModel: EvidenceViewModel by activityViewModels()
    private var adapter: EvidenceAdapter? = null


    override fun initView(baseView: View, savedInstanceState: Bundle?) {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.evidenceListLiveData.observe(viewLifecycleOwner, { evidenceList ->
            val enable = evidenceList.find { it.evidenceFileList.isNotEmpty() } != null
            setNavigationOnBtContinue(enable)
            if (adapter == null) {
                adapter = EvidenceAdapter(evidenceList, this)
                rvClaimDocuments.adapter = adapter
            } else
                adapter?.items = evidenceList
            claimSharedViewModel.claimDocumentListLiveData.value = evidenceList.filter { it.evidenceFileList.isNotEmpty() }
        })
    }

    @get:LayoutRes
    override val layoutResId: Int
        get() = R.layout.fragment_evidence

    override val stepNumber: Int
        get() = args.stepNumber

    override fun onEditEvidence(docId: Int) =
            routeUploadDocument(docId, UploadDocumentActivity.Source.EDIT)

    override fun onCaptureImage(docId: Int) =
            routeUploadDocument(docId, UploadDocumentActivity.Source.CAMERA)

    override fun onUploadDocument(docId: Int) =
            routeUploadDocument(docId, UploadDocumentActivity.Source.DOCUMENT)

    private fun routeUploadDocument(docId: Int, source: UploadDocumentActivity.Source) =
            UploadDocumentActivityRouter()
                    .setDependenciesAndRoute(this,
                            docId,
                            viewModel.getEvidenceFileList(docId),
                            source)

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == UploadDocumentActivityRouter.REQUEST_CODE_UPLOAD_DOCUMENT_ACTIVITY
                && resultCode == Activity.RESULT_OK
                && data != null) {
            val result = UploadDocumentActivityRouter.getResult(data)
            viewModel.updateEvidenceFileList(result.docId, result.evidenceFileList)
        }
    }
}