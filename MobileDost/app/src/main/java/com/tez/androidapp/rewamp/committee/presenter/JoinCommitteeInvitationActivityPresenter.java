package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.interactor.JoinCommitteeInvitationActivityInteractor;
import com.tez.androidapp.rewamp.committee.response.GetInvitedPackageResponse;
import com.tez.androidapp.rewamp.committee.view.IJoinCommitteeInvitationActivityView;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class JoinCommitteeInvitationActivityPresenter implements IJoinCommitteeInvitationActivityPresenter, IJoinCommitteeInvitationActivityInteractorOutput {


    private final IJoinCommitteeInvitationActivityView mIJoinCommitteeInvitationActivityView;
    private final JoinCommitteeInvitationActivityInteractor mJoinCommitteeInvitationActivityInteractor;

    public JoinCommitteeInvitationActivityPresenter(IJoinCommitteeInvitationActivityView iJoinCommitteeInvitationActivityView) {
        this.mIJoinCommitteeInvitationActivityView = iJoinCommitteeInvitationActivityView;
        mJoinCommitteeInvitationActivityInteractor = new JoinCommitteeInvitationActivityInteractor(this);
    }


    @Override
    public void onGetInvitedPackageSuccess(GetInvitedPackageResponse getInvitedPackageResponse) {
        mIJoinCommitteeInvitationActivityView.hideLoader();
        mIJoinCommitteeInvitationActivityView.onGetInvitedPackageSuccess(getInvitedPackageResponse);
    }

    @Override
    public void onGetInvitedPackageFailure(int errorCode, String message) {
        mIJoinCommitteeInvitationActivityView.showError(errorCode,
                (dialog, which) -> mIJoinCommitteeInvitationActivityView.finishActivity());
    }

    @Override
    public void getInvitedPackage(String mobileNumber) {
        mIJoinCommitteeInvitationActivityView.showLoader();
        mJoinCommitteeInvitationActivityInteractor.getInvitedPackage(mobileNumber);
    }
}
