package com.tez.androidapp.app.general.feature.playback;

import android.os.CountDownTimer;

/**
 * Created by VINOD KUMAR on 5/10/2019.
 */
public abstract class TezCountDownTimer extends CountDownTimer {

    /**
     * @param millisInFuture    The number of millis in the future from the call
     *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                          is called.
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick(long)} callbacks.
     */

    protected TezCountDownTimer(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    @Override
    public void onTick(long millisUntilFinished) {
        /*
            Implementation by child classes
         */
    }
}
