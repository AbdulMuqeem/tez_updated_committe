package com.tez.androidapp.rewamp.gold.onboarding.request;

import com.tez.androidapp.app.base.request.BaseRequest;

public class GetGoldRateRequest extends BaseRequest {

    public static final String METHOD_NAME = "v2/user/goldRate";
}
