package com.tez.androidapp.rewamp.general.termsandcond;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

/**
 * Created by VINOD KUMAR on 8/26/2019.
 */
public class NavigationTermsAdapter extends GenericRecyclerViewAdapter<NavigationTerm,
        NavigationTermsAdapter.NavigationTermListener,
        NavigationTermsAdapter.NavigationTermViewHolder> {

    NavigationTermsAdapter(@NonNull List<NavigationTerm> items,
                           @NonNull NavigationTermListener listener) {
        super(items, listener);
    }

    @NonNull
    @Override
    public NavigationTermViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.terms_and_condition_item, parent);
        return new NavigationTermViewHolder(view);
    }

    public interface NavigationTermListener extends BaseRecyclerViewListener {

        void onClickTerm(@NonNull String fileName);
    }

    public class NavigationTermViewHolder extends BaseViewHolder<NavigationTerm, NavigationTermListener> {

        @BindView(R.id.tvTermsAndCondition)
        private TezTextView textViewTermsAndCondition;

        NavigationTermViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(NavigationTerm item, @Nullable NavigationTermListener listener) {
            super.onBind(item, listener);
            this.textViewTermsAndCondition.setText(item.getTermsAndCondtion());
            this.itemView.setOnClickListener((DoubleTapSafeOnClickListener) view -> {
                if (listener != null)
                    listener.onClickTerm(item.getFileName());
            });
        }
    }
}
