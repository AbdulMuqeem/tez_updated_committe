package com.tez.androidapp.rewamp.general.beneficiary.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.rewamp.general.beneficiary.entity.BeneficiaryRelation;

import java.util.List;

public class BeneficiaryRelationsResponse extends BaseResponse {

    private List<BeneficiaryRelation> relationshipList;

    public List<BeneficiaryRelation> getRelationshipList() {
        return relationshipList;
    }
}
