package com.tez.androidapp.rewamp.advance.limit.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.advance.limit.view.CheckYourLimitActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

public class CheckYourLimitActivityRouter extends BaseActivityRouter {

    public static CheckYourLimitActivityRouter createInstance() {
        return new CheckYourLimitActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        if(Utility.isAllPermissionGranted(from))
            route(from, createIntent(from));
        else
            CheckYourLimitPermissionsActivityRouter.createInstance().setDependenciesAndRoute(from);

    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CheckYourLimitActivity.class);
    }
}
