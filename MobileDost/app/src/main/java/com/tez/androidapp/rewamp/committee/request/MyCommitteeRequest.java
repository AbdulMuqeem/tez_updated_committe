package com.tez.androidapp.rewamp.committee.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Ahmad Izaz on 21-Nov-20
 **/
public class MyCommitteeRequest extends BaseRequest {
    public static final String METHOD_NAME = "v1/committee/";
}
