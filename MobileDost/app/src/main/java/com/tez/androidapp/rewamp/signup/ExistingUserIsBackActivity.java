package com.tez.androidapp.rewamp.signup;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.models.network.dto.request.UserInfoRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.signup.router.ExistingUserIsBackActivityRouter;
import com.tez.androidapp.rewamp.signup.router.NewLoginActivityRouter;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.ViewBinder;

public class ExistingUserIsBackActivity extends BaseActivity implements DoubleTapSafeOnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_existing_user_is_back);
        ViewBinder.bind(this);
        init();
    }

    private void init() {
        initListeners();
        initTexts();
    }

    private void initTexts() {
        TezTextView textViewHeading = findViewById(R.id.textViewHeading);
        TezTextView tvNumber = findViewById(R.id.tvNumber);

        String welcomeGreetings = getString(R.string.greetings_and_welcome_back, Utility.getGreetings(this));
        String number = getIntent().getStringExtra(ExistingUserIsBackActivityRouter.MOBILE_NUMBER);

        textViewHeading.setText(welcomeGreetings);
        tvNumber.setText(number);
    }

    private void initListeners() {
        TezTextView tvEditNumber = findViewById(R.id.tvEditNumber);
        TezTextView tvCancel = findViewById(R.id.tvCancel);
        TezButton btStartJourney = findViewById(R.id.btStartJourney);
        tvEditNumber.setDoubleTapSafeOnClickListener(this);
        tvCancel.setDoubleTapSafeOnClickListener(this);
        btStartJourney.setDoubleTapSafeOnClickListener(this);
    }

    @Override
    public void doubleTapSafeOnClick(View view) {

        switch (view.getId()) {

            case R.id.tvEditNumber:
            case R.id.tvCancel:
                finish();
                break;

            case R.id.btStartJourney:
                startLogin();
        }
    }

    private void startLogin() {
        Intent intent = getIntent();
        String number = intent.getStringExtra(ExistingUserIsBackActivityRouter.MOBILE_NUMBER);
        String socialId = intent.getStringExtra(ExistingUserIsBackActivityRouter.SOCIAL_ID);
        String refCode = intent.getStringExtra(ExistingUserIsBackActivityRouter.REF_CODE);
        int socialType = intent.getIntExtra(ExistingUserIsBackActivityRouter.SOCIAL_TYPE, Utility.PrincipalType.MOBILE_NUMBER.getValue());
        NewLoginActivityRouter.createInstance().setDependenciesAndRoute(this, number, socialId, socialType, refCode);
    }

    @Override
    protected String getScreenName() {
        return "ExistingUserIsBackActivity";
    }
}
