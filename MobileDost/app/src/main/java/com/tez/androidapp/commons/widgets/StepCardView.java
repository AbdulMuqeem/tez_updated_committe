package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.TezDrawableHelper;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

/**
 * Created by VINOD KUMAR on 8/2/2019.
 */
public class StepCardView extends TezCardView {

    @BindView(R.id.cbStepCheck)
    private TezCheckBox cbStepCheck;

    @BindView(R.id.tvStepDescription)
    private TezTextView tvStepDescription;

    public StepCardView(@NonNull Context context) {
        super(context);
    }

    public StepCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public StepCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    public void setStepCompleted(boolean completed) {
        cbStepCheck.setVisibility(completed ? VISIBLE : GONE);
    }

    public void setStepDescription(@StringRes int id) {
        tvStepDescription.setText(id);
    }

    private void init(@NonNull Context context, @NonNull AttributeSet attrs) {
        inflate(context, R.layout.step_card_view, this);
        ViewBinder.bind(this, this);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.StepCardView);

        try {

            String description = typedArray.getString(R.styleable.StepCardView_step_description);
            Drawable image = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.StepCardView_step_image);

            TezImageView ivStepImage = findViewById(R.id.ivStepImage);

            ivStepImage.setImageDrawable(image);
            tvStepDescription.setText(description);

        } finally {
            typedArray.recycle();
        }
    }
}
