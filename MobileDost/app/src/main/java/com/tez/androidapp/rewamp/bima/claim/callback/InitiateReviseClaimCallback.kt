package com.tez.androidapp.rewamp.bima.claim.callback

import com.tez.androidapp.rewamp.bima.claim.response.InitiateReviseClaimResponse

interface InitiateReviseClaimCallback {

    fun onInitiateReviseClaimSuccess(response: InitiateReviseClaimResponse)

    fun onInitiateReviseClaimFailure(errorCode: Int, message: String?)
}