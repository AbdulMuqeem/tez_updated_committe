package com.tez.androidapp.rewamp.bima.products.viewmodel

import androidx.lifecycle.MutableLiveData
import com.tez.androidapp.commons.utils.app.Utility
import com.tez.androidapp.repository.network.store.BimaCloudDataStore
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Failure
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Loading
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Success
import com.tez.androidapp.rewamp.bima.claim.common.network.viewmodel.NetworkBoundViewModel
import com.tez.androidapp.rewamp.bima.products.callback.ProductListCallback
import com.tez.androidapp.rewamp.bima.products.model.InsuranceProduct
import com.tez.androidapp.rewamp.bima.products.response.ProductListResponse

class ProductsViewModel : NetworkBoundViewModel() {
    private var insuranceProductListLiveData: MutableLiveData<List<InsuranceProduct>>? = null
    fun getInsuranceProductListLiveData(): MutableLiveData<List<InsuranceProduct>> {
        if (insuranceProductListLiveData == null) {
            insuranceProductListLiveData = MutableLiveData()
            getProductsList()
        }
        return insuranceProductListLiveData!!
    }

    private fun getProductsList() {
        networkCallMutableLiveData.value = Loading
        BimaCloudDataStore.getInstance().getInsuranceProducts(object : ProductListCallback {
            override fun onGetProductListSuccess(productListResponse: ProductListResponse) {
                insuranceProductListLiveData!!.value = productListResponse.products
                networkCallMutableLiveData.value = Success
            }

            override fun onGetProductListFailure(errorCode: Int, message: String) {
                if (Utility.isUnauthorized(errorCode))
                    getProductsList()
                else
                    networkCallMutableLiveData.value = Failure(errorCode)
            }
        })
    }
}