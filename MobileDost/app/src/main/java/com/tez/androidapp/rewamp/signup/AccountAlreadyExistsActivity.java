package com.tez.androidapp.rewamp.signup;

import android.os.Bundle;
import android.view.View;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.models.network.dto.request.UserInfoRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.signup.router.AccountAlreadyExistsActivityRouter;
import com.tez.androidapp.rewamp.signup.router.NewLoginActivityRouter;
import com.tez.androidapp.rewamp.signup.router.SignupNumberVerificationActivityRouter;

public class AccountAlreadyExistsActivity extends BaseErrorActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        String welcomeGreetings = getString(R.string.greetings_and_welcome_back, Utility.getGreetings(this));
        setTvHeadingText(welcomeGreetings);
        setTvMessageText(R.string.account_already_exist_desc);
        setBtMainButtonText(R.string.string_continue);
        setIvLogoImageResource(R.drawable.img_existing_user_login);
        setBtMainButtonOnClickListener(view -> startLoginActivity());
        setTvNumberVisibility(View.VISIBLE);
        setTvNumberText(getTextForTezTextViewNumber());
    }

    private void startLoginActivity() {
        NewLoginActivityRouter.createInstance().setDependenciesAndRoute(this, getMobileNumber(),
                getSocialId(),
                getSocialType(),
                getReferralCode());
        finish();
    }

    private String getSocialId() {
        return getIntent().getStringExtra(AccountAlreadyExistsActivityRouter.SOCIAL_ID);
    }

    private String getReferralCode() {
        return getIntent().getStringExtra(AccountAlreadyExistsActivityRouter.REF_CODE);
    }

    private int getSocialType() {
        return getIntent().getIntExtra(AccountAlreadyExistsActivityRouter.SOCIAL_TYPE,
                Utility.PrincipalType.MOBILE_NUMBER.getValue());
    }

    private String getTextForTezTextViewNumber() {
        return getMobileNumber();
    }

    private String getMobileNumber() {
        return getIntent().getStringExtra(AccountAlreadyExistsActivityRouter.MOBILE_NUMBER);
    }

    @Override
    protected String getScreenName() {
        return "AccountAlreadyExistsActivity";
    }
}
