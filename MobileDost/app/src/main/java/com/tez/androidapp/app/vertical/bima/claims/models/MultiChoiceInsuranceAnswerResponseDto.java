package com.tez.androidapp.app.vertical.bima.claims.models;

/**
 * Created by VINOD KUMAR on 11/13/2018.
 */
public class MultiChoiceInsuranceAnswerResponseDto extends InsuranceAnswerResponseDto {

    public MultiChoiceInsuranceAnswerResponseDto(String response) {
        this.response = response;
    }

    @Override
    public void setAnswer(CommonAnswers insuranceAnswerDto) {
        insuranceAnswerDto.setResponse(response);
    }
}
