package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.BuildConfig;
import com.tez.androidapp.app.base.request.BaseRequest;
import com.tez.androidapp.commons.models.network.DeviceInfo;

import java.io.Serializable;

/**
 * Created  on 2/10/2017.
 */

public class UserSignUpRequest extends BaseRequest implements Serializable {

    public static final String METHOD_NAME = "v1/user/signup";
    private String mobileNumber;
    private Double longitude;
    private Double latitude;
    private String deviceKey;
    private String socialId;
    private Integer socialType;
    private String referralCode;
    private String appVersion = BuildConfig.VERSION_NAME;
    private DeviceInfo deviceInfo;
    private String languageCode;

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }


    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public void cleanseSelf() {
        referralCode = referralCode.isEmpty() ? null : referralCode;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public Integer getSocialType() {
        return socialType;
    }

    public void setSocialType(Integer socialType) {
        this.socialType = socialType;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }
}
