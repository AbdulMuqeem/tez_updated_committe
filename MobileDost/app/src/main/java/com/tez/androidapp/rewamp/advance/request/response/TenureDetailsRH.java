package com.tez.androidapp.rewamp.advance.request.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.advance.request.callback.TenureDetailsCallback;

import net.tez.fragment.util.optional.Optional;

public class TenureDetailsRH extends BaseRH<TenureDetailsResponse> {

    private final TenureDetailsCallback tenureDetailsCallback;

    public TenureDetailsRH(BaseCloudDataStore baseCloudDataStore,
                           TenureDetailsCallback tenureDetailsCallback) {
        super(baseCloudDataStore);
        this.tenureDetailsCallback = tenureDetailsCallback;
    }

    @Override
    protected void onSuccess(Result<TenureDetailsResponse> value) {
        Optional.ifPresent(value.response().body(), tenureDetailsResponse -> {
            Optional.doWhen(tenureDetailsResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR,
                    () -> this.tenureDetailsCallback.onGetTenuresDetailsSuccess(tenureDetailsResponse.getTenures()),
                    () -> this.tenureDetailsCallback.onGetTenuresDetailsFailure(tenureDetailsResponse.getStatusCode(),
                            tenureDetailsResponse.getErrorDescription()));
        }, this::sendDefaultFailure);
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.tenureDetailsCallback.onGetTenuresDetailsFailure(errorCode, message);
    }
}
