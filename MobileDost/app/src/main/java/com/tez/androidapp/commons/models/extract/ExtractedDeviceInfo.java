package com.tez.androidapp.commons.models.extract;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;

import com.tez.androidapp.BuildConfig;

/**
 * Created  on 9/8/2017.
 */

public class ExtractedDeviceInfo {

    private String deviceId;
    private String serialNumber;
    private String deviceSoftware;
    private String manufacturer;
    private String model;
    private String networkOperator;
    private String networkOperatorName;
    private String simOperator;
    private String appVersion;

    public ExtractedDeviceInfo(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        deviceSoftware = Build.VERSION.RELEASE;
        manufacturer = Build.BRAND;
        model = Build.MODEL;
        networkOperator = telephonyManager.getNetworkOperatorName();
        simOperator = telephonyManager.getSimOperatorName();
        appVersion = BuildConfig.VERSION_NAME;
    }

    @Override
    public String toString() {
        return "ExtractedDeviceInfo{" +
                "deviceId='" + deviceId + '\'' +
                ", serialNumber='" + serialNumber + '\'' +
                ", deviceSoftware='" + deviceSoftware + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", networkOperator='" + networkOperator + '\'' +
                ", networkOperatorName='" + networkOperatorName + '\'' +
                ", simOperator='" + simOperator + '\'' +
                '}';
    }

    public void setNetworkOperatorName(String networkOperatorName) {
        this.networkOperatorName = networkOperatorName;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
