package com.tez.androidapp.commons.utils.textwatchers;

import androidx.annotation.NonNull;
import android.text.Editable;
import android.text.InputFilter;
import android.text.method.NumberKeyListener;
import android.view.inputmethod.EditorInfo;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezEditTextView;

import net.tez.logger.library.utils.TextUtil;

import java.util.regex.Pattern;

/**
 * Simple TextWatcher implementation just to make sure the input is in Pakistani NIC format, that is: "12345-1234567-1"
 * <p>
 * Created  on 1/28/2017.
 */

public class NICTextWatcher implements TezTextWatcher {

    private static final int MAX_LENGTH = 15;
    private TezEditTextView mEditText;


    public NICTextWatcher(TezEditTextView mEditText) {
        this.mEditText = mEditText;
        mEditText.setMaxLines(1);
        mEditText.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
        mEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15), new NumberKeyListener() {
            @NonNull
            @Override
            protected char[] getAcceptedChars() {
                return "0123456789-".toCharArray();
            }

            @Override
            public int getInputType() {
                return EditorInfo.TYPE_CLASS_NUMBER;
            }
        }});
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        mEditText.removeTextChangedListener(this);
        String inputString = s.toString();
        Editable editTextEditable = this.mEditText.getText();
        if (editTextEditable != null) {
            String editTextString = editTextEditable.toString();
            if (TextUtil.isNotEmpty(inputString) && TextUtil.isNotEmpty(editTextString) &&
                    before < count) {
                String regex1 = "^\\d{5}$";
                String regex2 = "^\\d{5}-\\d{7}$";
                String regex3 = "^\\d{5,12}$";
                if (Pattern.matches(regex1, inputString)) {
                    this.mEditText.setText(inputString + "-");
                    this.mEditText.setSelection(inputString.length() + 1);
                } else if (Pattern.matches(regex2, inputString)) {
                    this.mEditText.setText(inputString + "-");
                    this.mEditText.setSelection(inputString.length() + 1);
                } else if (Pattern.matches(regex3, inputString)) {
                    this.mEditText.setText(inputString.substring(0, 5) + "-" + inputString.substring(5));
                    this.mEditText.setSelection(inputString.length() + 1);
                }
            }
        }
        mEditText.addTextChangedListener(this);
        if (count == MAX_LENGTH)
            Utility.hideKeyboard(mEditText.getContext(), mEditText.getRootView());
    }

    @Override
    public void afterTextChanged(Editable s) {
        String regex1 = "^\\d{13,}$";
        String regex2 = "^\\d{5}-\\d{8,}$";
        String regex3 = "^[0-9-]{15}$";
        String regex4 = "^\\d{5}-\\d{7}-\\d{1}$";
        String regex5 = "^\\d{12}-\\d{1}";
        String inputString = s.toString();
        if (Pattern.matches(regex1, inputString)) {
            this.mEditText.setText(inputString.substring(0, 5) + "-" + inputString.substring(5, 12) + inputString.substring(12, 13));
            this.mEditText.setSelection(15);
        } else if (Pattern.matches(regex2, inputString)) {
            this.mEditText.setText(inputString.substring(0, 13) + "-" + inputString.substring(13, 14));
            this.mEditText.setSelection(15);
        } else if(Pattern.matches(regex3, inputString) && !Pattern.matches(regex4, inputString)){
            String newS = inputString.replaceAll("-", "");
            this.mEditText.setText(newS.substring(0, 5) + "-" + newS.substring(5, 12) + newS.substring(12, 13));
            this.mEditText.setSelection(15);
        } else if(Pattern.matches(regex5, inputString)){
            this.mEditText.setText(inputString.substring(0, 5) + "-" + inputString.substring(5));
            this.mEditText.setSelection(inputString.length() + 1);
        }
    }
}
