package com.tez.androidapp.rewamp.bima.products.policy.presenter;

import com.tez.androidapp.rewamp.bima.products.policy.request.InitiatePurchasePolicyRequest;

public interface IInsurancePaymentActivityPresenter extends IEasyPaisaInsurancePaymentActivityPresenter{
    void resendCode(InitiatePurchasePolicyRequest initiatePurchasePolicyRequest);
}
