package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;
import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.NewTermsAndConditionActivity;

/**
 * Created by VINOD KUMAR on 7/31/2019.
 */
public class NewTermsAndConditionActivityRouter extends BaseActivityRouter {

    public static final String USER_PIN = "USER_PIN";
    public static final String MOBILE_NUMBER = "MOBILE_NO";

    public static NewTermsAndConditionActivityRouter createInstance() {
        return new NewTermsAndConditionActivityRouter();
    }


    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        @NonNull String userPin,
                                        @NonNull String mobileNumber) {

        Intent intent = createIntent(from);
        intent.putExtra(USER_PIN, userPin);
        intent.putExtra(MOBILE_NUMBER, mobileNumber);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, NewTermsAndConditionActivity.class);
    }
}
