package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.response.CommitteeMetaDataResponse;
import com.tez.androidapp.rewamp.committee.view.CommitteeCreationActivity;

import androidx.annotation.NonNull;

public class CommitteeCreationActivityRouter extends BaseActivityRouter {


    public static final String COMMITTEE_METADATA = "COMMITTEE_METADATA";

    public static CommitteeCreationActivityRouter createInstance() {
        return new CommitteeCreationActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, CommitteeMetaDataResponse committeeMetaDataResponse) {
        Intent intent = createIntent(from);
        intent.putExtra(COMMITTEE_METADATA, committeeMetaDataResponse);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CommitteeCreationActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

}
