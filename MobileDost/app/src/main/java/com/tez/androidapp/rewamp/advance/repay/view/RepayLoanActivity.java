package com.tez.androidapp.rewamp.advance.repay.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.AddWalletCardView;
import com.tez.androidapp.commons.widgets.RepayAdvanceCardView;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.advance.repay.presenter.IRepayLoanActivityPresenter;
import com.tez.androidapp.rewamp.advance.repay.presenter.RepayLoanActivityPresenter;
import com.tez.androidapp.rewamp.advance.repay.router.RepayLoanActivityRouter;
import com.tez.androidapp.rewamp.advance.repay.router.VerifyEasyPaisaRepaymentRouter;
import com.tez.androidapp.rewamp.advance.repay.router.VerifyJazzRepaymentActivityRouter;
import com.tez.androidapp.rewamp.advance.repay.router.VerifyRepaymentActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.router.ChangeWalletActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class RepayLoanActivity extends BaseActivity implements IRepayLoanActivityView {

    private final IRepayLoanActivityPresenter iRepayLoanActivityPresenter;

    @BindView(R.id.cvEnterAmount)
    private RepayAdvanceCardView cvEnterAmount;

    @BindView(R.id.tvLatePaymentCharges)
    private TezTextView tvLatePaymentCharges;

    @BindView(R.id.tvLatePaymentChargesValue)
    private TezTextView tvLatePaymentChargesValue;

    @BindView(R.id.tvChargesValue)
    private TezTextView tvChargesValue;

    @BindView(R.id.tvLoanAmountValue)
    private TezTextView tvLoanAmountValue;

    @BindView(R.id.tvTotalRepayAmountValue)
    private TezTextView tvTotalRepayAmountValue;

    @BindView(R.id.cvMobileWallet)
    private AddWalletCardView cvMobileWallet;

    @BindView(R.id.btRepayAdvance)
    private TezButton btRepayAdvance;

    @BindView(R.id.clContent)
    private TezConstraintLayout clContent;

    @BindView(R.id.greyBackground)
    private TezLinearLayout greyBackground;

    public RepayLoanActivity() {
        this.iRepayLoanActivityPresenter = new RepayLoanActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repay_loan);
        ViewBinder.bind(this);
        init();
    }

    private void init() {
        iRepayLoanActivityPresenter.getRepayReceipt(getLoanIdFromIntent());
    }

    @Override
    public void initDetails(@NonNull LoanDetails loanDetails) {
        double amountToRepay = loanDetails.getAdvanceAmount() + loanDetails.getCharges() + loanDetails.getLatePaymentCharges();
        tvLoanAmountValue.setText(getString(R.string.rs_value_string, format(loanDetails.getAdvanceAmount())));
        tvChargesValue.setText(getString(R.string.rs_value_string, format(loanDetails.getCharges())));
        tvLatePaymentChargesValue.setText(getString(R.string.rs_value_string, format(loanDetails.getLatePaymentCharges())));
        tvTotalRepayAmountValue.setText(getString(R.string.rs_value_string, format(amountToRepay)));
        cvEnterAmount.setRepayDueDate(loanDetails.getDueDate());
        cvEnterAmount.setOutstandingAmount(loanDetails.getAmountToReturn());
        cvMobileWallet.setWallet(loanDetails.getWallet());
        cvMobileWallet.setChangeWalletListener(() -> ChangeWalletActivityRouter.createInstance().setDependenciesAndRouteForResult(this));
        btRepayAdvance.setDoubleTapSafeOnClickListener(view -> iRepayLoanActivityPresenter.onClickRepayAdvance(loanDetails, cvMobileWallet.getWallet(), cvEnterAmount.getAmount()));
    }

    @Override
    public void routeToVerifyEasyPaisaRepaymentActivity(@NonNull LoanDetails loanDetails, @NonNull Wallet wallet) {
        VerifyEasyPaisaRepaymentRouter.createInstance().setDependenciesAndRoute(this, getLoanIdFromIntent(), wallet.getServiceProviderId(), wallet.getMobileAccountId(), cvEnterAmount.getAmount());
    }

    @Override
    public void routeToVerifyJazzRepaymentActivity(@NonNull LoanDetails loanDetails, @NonNull Wallet wallet){
        VerifyJazzRepaymentActivityRouter.createInstance().setDependenciesAndRoute(this, getLoanIdFromIntent(), wallet.getServiceProviderId(), wallet.getMobileAccountId(), cvEnterAmount.getAmount());
    }

    @Override
    public void routeToVerifyRepaymentActivity(@NonNull LoanDetails loanDetails, @NonNull Wallet wallet) {
        VerifyRepaymentActivityRouter.createInstance().setDependenciesAndRoute(this, getLoanIdFromIntent(), wallet.getServiceProviderId(), wallet.getMobileAccountId(), cvEnterAmount.getAmount());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ChangeWalletActivityRouter.REQUEST_CODE_CHANGE_WALLET && data != null && resultCode == RESULT_OK)
            cvMobileWallet.setWallet(data.getParcelableExtra(ChangeWalletActivityRouter.RESULT_DATA_WALLET));
    }

    private String format(Double value) {
        return Utility.getFormattedAmountWithoutCurrencyLabel(value);
    }

    @Override
    public void setTvLatePaymentChargesVisibility(int visibility) {
        this.tvLatePaymentCharges.setVisibility(visibility);
        this.tvLatePaymentChargesValue.setVisibility(visibility);
    }

    @Override
    public void setClContentVisibility(int visibility) {
        this.clContent.setVisibility(visibility);
        this.greyBackground.setVisibility(visibility);
    }

    @Override
    public void showTezLoader() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissTezLoader() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.GONE);
        ViewGroup viewGroup = findViewById(R.id.rootViewGroup);
        viewGroup.removeView(shimmer);
    }

    @Override
    protected boolean childConditionOnHideKeyboard(MotionEvent event) {
        return Utility.isTouchOutSideViewBoundary(cvEnterAmount, event);
    }

    private int getLoanIdFromIntent() {
        return getIntent().getIntExtra(RepayLoanActivityRouter.LOAN_ID, -100);
    }

    @Override
    protected String getScreenName() {
        return "RepayLoanActivity";
    }
}
