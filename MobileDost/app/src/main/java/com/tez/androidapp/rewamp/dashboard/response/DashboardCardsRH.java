package com.tez.androidapp.rewamp.dashboard.response;

import androidx.annotation.CallSuper;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

public abstract class DashboardCardsRH<T extends DashboardCardsResponse> extends BaseRH<T> {

    public DashboardCardsRH(BaseCloudDataStore baseCloudDataStore) {
        super(baseCloudDataStore);
    }

    @CallSuper
    @Override
    protected void onSuccess(Result<T> value) {

        T response = value.response().body();

        if (response != null && response.getCards() != null)
            Utility.persistDashboardCards(response.getCards());
    }
}
