package com.tez.androidapp.app.general.feature.account.feature.profile.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.ProfileBasicCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.ProfileBasicResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

public class ProfileBasicRH extends BaseRH<ProfileBasicResponse> {
    private final ProfileBasicCallback profileBasicCallback;

    public ProfileBasicRH(BaseCloudDataStore baseCloudDataStore, ProfileBasicCallback profileBasicCallback) {
        super(baseCloudDataStore);
        this.profileBasicCallback = profileBasicCallback;
    }

    @Override
    protected void onSuccess(Result<ProfileBasicResponse> value) {
        ProfileBasicResponse profileBasicResponse = value.response().body();
        if (profileBasicResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            profileBasicCallback.onProfileBasicSuccess(profileBasicResponse.getBasicInformation());
        } else {
            onFailure(profileBasicResponse.getStatusCode(), profileBasicResponse.getErrorDescription());
        }
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        profileBasicCallback.onProfileBasicFailure(errorCode, message);
    }
}
