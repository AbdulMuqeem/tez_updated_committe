package com.tez.androidapp.rewamp.signup.view;

import androidx.annotation.Nullable;

import com.tez.androidapp.commons.models.network.dto.request.UserLoginRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.signup.NewLoginActivity;
import com.tez.androidapp.rewamp.signup.presenter.NewTermsAndConditionActivityPresenter;
import com.tez.androidapp.rewamp.signup.router.SignupLoginActivityRouter;
import com.tez.androidapp.rewamp.signup.router.SignupLoginPermissionActivityRouter;

public class SignupLoginActivity extends NewLoginActivity implements INewTermsAndConditionActivityView {

    @Override
    protected void startLogin(String pin) {
        showTezLoader();
        new NewTermsAndConditionActivityPresenter(this).callLogin(pin);
    }

    @Override
    protected void askPermission(String pin) {
        SignupLoginPermissionActivityRouter.createInstance().setDependenciesAndRoute(this, getNumberFromIntent(), pin);
    }

    @Override
    public void navigateToDashBoardActivity() {
        DashboardActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void startLogin() {
        this.pinView.setEnabled(true);
    }

    @Override
    public UserLoginRequest getLoginrequest(String pin) {
        return Utility.createLoginRequest(this, getNumberFromIntent(), pin);
    }

    private String getNumberFromIntent() {
        return getIntent().getStringExtra(SignupLoginActivityRouter.MOBILE_NUMBER);
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }

    @Override
    protected String getScreenName() {
        return "SignupLoginActivity";
    }
}
