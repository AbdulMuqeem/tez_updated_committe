package com.tez.androidapp.rewamp.bima.claim.request

import com.tez.androidapp.app.base.request.BaseRequest

class LodgeClaimRequest(val policyId: Int,
                        val amount: Double,
                        val reasonId: Int?,
                        val otherReason: String?,
                        val dateOfAdmission: String,
                        val dateOfDischarge: String,
                        val hospitalName: String,
                        val hospitalNameMap: String?,
                        val hospitalLocation: String?,
                        val nearestLandmark: String?,
                        val mobileUserAccountId: Int,
                        val claimantLocation: String) : BaseRequest() {

    companion object {
        const val METHOD_NAME = "v1/claim/lodge"
    }
}