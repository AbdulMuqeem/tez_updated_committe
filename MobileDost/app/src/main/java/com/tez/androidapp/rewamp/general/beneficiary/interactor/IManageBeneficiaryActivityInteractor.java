package com.tez.androidapp.rewamp.general.beneficiary.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;

/**
 * Created by VINOD KUMAR on 8/30/2019.
 */
public interface IManageBeneficiaryActivityInteractor extends IBaseInteractor {

    void getManageBeneficiaryPolicies();
}
