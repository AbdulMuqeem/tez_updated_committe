package com.tez.androidapp.rewamp.bima.products.policy.view;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Constants;

public class CoronaDefenseActivity extends BaseProductActivity {


    @Override
    protected int getContentView() {
        return R.layout.activity_corona_defense;
    }

    @Override
    protected int getProductId() {
        return Constants.CORONA_DEFENSE_PLAN_ID;
    }

    @Override
    protected String getScreenName() {
        return "CoronaDefenseActivity";
    }
}