package com.tez.androidapp.rewamp.committee.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tez.androidapp.R

class CommitteeLeaveReasonActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_committee_leave_reason)
    }
}