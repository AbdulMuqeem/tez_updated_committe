package com.tez.androidapp.app.general.feature.feedback.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.feedback.callbacks.UserFeedbackCallback;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created by Rehman Murad Ali on 8/30/2017.
 */

public class UserFeedbackRH extends BaseRH<BaseResponse> {
    UserFeedbackCallback userFeedbackCallback;

    public UserFeedbackRH(BaseCloudDataStore baseCloudDataStore, UserFeedbackCallback userFeedbackCallback) {
        super(baseCloudDataStore);
        this.userFeedbackCallback = userFeedbackCallback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse response = value.response().body();
        if (response.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) userFeedbackCallback.onUserFeedbackSuccess(response);
        else onFailure(response.getStatusCode(), response.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        userFeedbackCallback.onUserFeedbackFailure(errorCode, message);
    }


}
