package com.tez.androidapp.rewamp.advance.repay.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.repay.view.VerifyRepaymentActivity;

public class VerifyRepaymentActivityRouter extends VerifyEasyPaisaRepaymentRouter {

    public static VerifyRepaymentActivityRouter createInstance() {
        return new VerifyRepaymentActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        int loanId,
                                        int walletServiceProviderId,
                                        int walletMobileAccountId,
                                        double amount) {
        Intent intent = createIntent(from);
        intent.putExtra(LOAN_ID, loanId);
        intent.putExtra(WALLET_SERVICE_PROVIDER_ID, walletServiceProviderId);
        intent.putExtra(WALLET_MOBILE_ACCOUNT_ID, walletMobileAccountId);
        intent.putExtra(AMOUNT, amount);
        route(from, intent);
    }

    @Override
    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, VerifyRepaymentActivity.class);
    }
}
