package com.tez.androidapp.app.general.feature.transactions.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.transactions.models.network.TransactionDetail;

/**
 * Created  on 9/2/2017.
 */

public class GetLoanTransactionDetailResponse extends BaseResponse {

    private TransactionDetail transactionDetail;

    public TransactionDetail getTransactionDetail() {
        return transactionDetail;
    }

}
