package com.tez.androidapp.rewamp.signup.router;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.view.WelcomeBackPermissionsActivity;

public class WelcomeBackPermissionsActivityRouter extends BaseActivityRouter {
    public static final String MOBILE_NUMBER = "MOBILE_NUMBER";
    public static final String USER_PIN = "USER_PIN";
    public static final String CNIC = "PRINCIPLE";
    public static final String USER_SESSION_TIMEOUT = "user_session_timeout";

    public static WelcomeBackPermissionsActivityRouter createInstance() {
        return new WelcomeBackPermissionsActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from,
                                        @NonNull String mobileNumber,
                                        @NonNull String userPin,
                                        boolean userSessionTimeout,
                                        @Nullable String userPushRoute) {

        Intent intent = createIntent(from);
        intent.putExtra(MOBILE_NUMBER, mobileNumber);
        intent.putExtra(USER_PIN, userPin);
        intent.putExtra(CNIC, mobileNumber);
        intent.putExtra(USER_SESSION_TIMEOUT, userSessionTimeout);
        intent.putExtra(PushNotificationConstants.KEY_ROUTE_TO, userPushRoute);
        Optional.doWhen(userSessionTimeout,
                ()->routeForResult(from, intent, Constants.USER_SESSION_EXPIRED_REQUEST_CODE),
                ()-> {
                    intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                    route(from, intent);
                });
    }

    public void setDependenciesAndRouteToForwardResult(@NonNull BaseActivity from,
                                        @NonNull String mobileNumber,
                                        @NonNull String userPin,
                                        boolean userSessionTimeout,
                                        @Nullable String userPushRoute) {

        Intent intent = createIntent(from);
        intent.putExtra(MOBILE_NUMBER, mobileNumber);
        intent.putExtra(USER_PIN, userPin);
        intent.putExtra(CNIC, mobileNumber);
        intent.putExtra(USER_SESSION_TIMEOUT, userSessionTimeout);
        intent.putExtra(PushNotificationConstants.KEY_ROUTE_TO, userPushRoute);
        intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, WelcomeBackPermissionsActivity.class);
    }
}
