package com.tez.androidapp.rewamp.bima.claim.common.document.adapter

import android.graphics.PorterDuff
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import com.tez.androidapp.R
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.Evidence
import com.tez.androidapp.commons.utils.app.Constants
import com.tez.androidapp.commons.utils.app.Utility
import com.tez.androidapp.rewamp.bima.claim.common.document.adapter.EvidenceAdapter.EvidenceViewHolder
import com.tez.androidapp.rewamp.bima.claim.common.document.adapter.EvidenceAdapter.OnEvidenceUpdateListener
import kotlinx.android.synthetic.main.list_item_evidence.view.*
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener
import net.tez.tezrecycler.base.viewholder.BaseViewHolder

class EvidenceAdapter(items: List<Evidence>, listener: OnEvidenceUpdateListener?)
    : GenericRecyclerViewAdapter<Evidence, OnEvidenceUpdateListener?, EvidenceViewHolder?>(items, listener) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EvidenceViewHolder {
        val view = inflate(parent.context, R.layout.list_item_evidence, parent)
        return EvidenceViewHolder(view)
    }

    interface OnEvidenceUpdateListener : BaseRecyclerViewListener {
        fun onEditEvidence(docId: Int)
        fun onCaptureImage(docId: Int)
        fun onUploadDocument(docId: Int)
    }

    class EvidenceViewHolder(itemView: View) : BaseViewHolder<Evidence, OnEvidenceUpdateListener?>(itemView) {

        override fun onBind(item: Evidence, listener: OnEvidenceUpdateListener?) {
            super.onBind(item, listener)
            itemView.apply {
                ivName.text = Utility.getStringFromResource(item.name)
                tvFilesUploaded.text = context.getString(R.string.num_files_uploaded, item.evidenceFileList.size)
                clEvidence.setBackgroundResource(if (item.evidenceFileList.isNotEmpty()) R.drawable.evidence_bg_outline else 0)
                ivIcon.setBackgroundResource(if (item.evidenceFileList.isNotEmpty()) R.drawable.evidence_bg_outline else R.drawable.ic_grey_round_bg)
                ivIcon.setColorFilter(ContextCompat.getColor(context, if (item.evidenceFileList.isNotEmpty()) R.color.textViewTextColorGreen else R.color.stepDivider), PorterDuff.Mode.SRC_IN)
                ivOptions.setDoubleTapSafeOnClickListener {
                    val popupMenu = PopupMenu(it.context, it, Gravity.START)
                    popupMenu.inflate(R.menu.doc_file_upload)
                    popupMenu.menu.getItem(0).isVisible = item.evidenceFileList.isNotEmpty()
                    popupMenu.menu.getItem(1).isVisible = item.evidenceFileList.size < Constants.MAX_FILES_PER_CLAIM_DOCUMENT
                    popupMenu.menu.getItem(2).isVisible = item.evidenceFileList.size < Constants.MAX_FILES_PER_CLAIM_DOCUMENT
                    if (listener != null)
                        popupMenu.setOnMenuItemClickListener { menuItem -> onClickMenuItem(menuItem.itemId, item.id, listener) }
                    popupMenu.show()
                }
                val rejected = item.rejected && item.evidenceFileList.isEmpty()
                if (rejected) {
                    val redColor = Utility.getColorFromResource(R.color.textViewHeadingColorRed)
                    clEvidence.setBackgroundResource(R.drawable.evidence_bg_red_outline)
                    ivIcon.setBackgroundResource(R.drawable.evidence_bg_red_outline)
                    ivIcon.setColorFilter(redColor)
                    ivOptions.setColorFilter(redColor)
                    tvFilesUploaded.setTextColor(redColor)
                } else {
                    ivOptions.clearColorFilter()
                    tvFilesUploaded.setTextColor(Utility.getColorFromResource(R.color.textViewTextColorGreen))
                }
                setOnClickListener { ivOptions.performClick() }
            }
        }

        private fun onClickMenuItem(itemId: Int, docId: Int, listener: OnEvidenceUpdateListener): Boolean {
            when (itemId) {
                R.id.edit -> listener.onEditEvidence(docId)
                R.id.captureImage -> listener.onCaptureImage(docId)
                R.id.uploadDocument -> listener.onUploadDocument(docId)
            }
            return true
        }
    }
}