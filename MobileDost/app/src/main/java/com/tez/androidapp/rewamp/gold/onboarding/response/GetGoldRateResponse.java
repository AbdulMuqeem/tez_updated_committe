package com.tez.androidapp.rewamp.gold.onboarding.response;

import com.tez.androidapp.app.base.response.BaseResponse;

public class GetGoldRateResponse extends BaseResponse {

    private final String goldRate;
    private final String fetchTime;
    private final String unit;

    public GetGoldRateResponse(String goldRate, String fetchTime, String unit) {
        this.goldRate = goldRate;
        this.fetchTime = fetchTime;
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }

    public String getGoldRate() {
        return goldRate;
    }

    public String getFetchTime() {
        return fetchTime;
    }
}
