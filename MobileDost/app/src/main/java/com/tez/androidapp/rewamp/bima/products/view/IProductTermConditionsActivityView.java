package com.tez.androidapp.rewamp.bima.products.view;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.rewamp.signup.TermsAndCondition;

import java.util.List;

public interface IProductTermConditionsActivityView extends IBaseView {

    void setAdapter(@NonNull List<TermsAndCondition> termsAndConditionList);

    void setNsvContentVisibility(int visibility);

    void setShimmerVisibility(int visibility);

    void setTermCondition(String termCondition);
}
