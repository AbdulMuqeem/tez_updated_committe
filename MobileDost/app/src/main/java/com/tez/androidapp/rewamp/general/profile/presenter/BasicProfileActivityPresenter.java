package com.tez.androidapp.rewamp.general.profile.presenter;

import android.view.View;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Answer;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.AnswerSelected;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.BasicInformation;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Option;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateUserBasicProfileRequest;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.rewamp.general.profile.interactor.BasicProfileActivityInteractor;
import com.tez.androidapp.rewamp.general.profile.interactor.IBasicProfileActivityInteractor;
import com.tez.androidapp.rewamp.general.profile.view.IBasicProfileActivityView;

import net.tez.logger.library.utils.TextUtil;

import java.util.ArrayList;
import java.util.List;

public class BasicProfileActivityPresenter
        implements IBasicProfileActivityPresenter, IBasicProfileactivityInteractorOutput {

    private final IBasicProfileActivityInteractor iBasicProfileActivityInteractor;
    private final IBasicProfileActivityView iBasicProfileActivityView;
    private AnswerSelected selectedJob;
    private AnswerSelected selectedSubJob;

    public BasicProfileActivityPresenter(IBasicProfileActivityView iBasicProfileActivityView) {
        this.iBasicProfileActivityView = iBasicProfileActivityView;
        this.iBasicProfileActivityInteractor = new BasicProfileActivityInteractor(this);
    }

    @Override
    public void getOccupationOptions(String lang) {
        this.iBasicProfileActivityInteractor.getListOfOccupation(lang);
    }

    @Override
    public void updateUserBasicProfile() {
        this.iBasicProfileActivityView.showTezLoader();
        UpdateUserBasicProfileRequest updateUserProfileRequest = new UpdateUserBasicProfileRequest();
        updateUserProfileRequest.setFullName(this.iBasicProfileActivityView.getTextFromEtName());
        updateUserProfileRequest.setDateOfBirth(this.iBasicProfileActivityView.getTextFromEtDob());
        updateUserProfileRequest.setGender(this.iBasicProfileActivityView.getTextFromSGender());
        List<AnswerSelected> answerSelectedList = new ArrayList<>();
        answerSelectedList.add(selectedJob);
        Optional.ifPresent(selectedSubJob, subJob -> {
            answerSelectedList.add(selectedSubJob);
        });
        updateUserProfileRequest.setPersonalInformationList(answerSelectedList);
        this.iBasicProfileActivityInteractor.updateUserBasicProfile(updateUserProfileRequest);
    }


    @Override
    public void onUpdateUserProfileSuccess() {
        this.iBasicProfileActivityView.dismissTezLoader();
        this.iBasicProfileActivityView.unlockLimit();
    }

    @Override
    public void onUpdateUserProfileFailure(int errorCode, String message) {
        this.iBasicProfileActivityView.setBtContinueEnabled(true);
        this.iBasicProfileActivityView.dismissTezLoader();
        this.iBasicProfileActivityView.showError(errorCode);
    }

    @Override
    public void onQuestionOptionsSuccess(List<Option> jobs, BasicInformation basicInformation) {
        this.iBasicProfileActivityView.setVisibillityForMainCard(View.VISIBLE);
        this.iBasicProfileActivityView.setVisibillityForShimmer(View.GONE);
        this.iBasicProfileActivityView.setListenerToSGender(jobs);
        Optional.ifPresent(basicInformation, data -> {
            iBasicProfileActivityView.setTextToEtName(data.getFullName());
            iBasicProfileActivityView.setTextToEtDOB(data.getDateOfBirth());
            Optional.ifPresent(data.getGender(), gender -> {
                iBasicProfileActivityView.setTextToSGender(gender);
                int position = TextUtil.equals(gender, "M") ? 0 :
                        TextUtil.equals(gender, "F") ? 1 :
                                TextUtil.equals(gender, "T") ? 2 : 0;
                Option selectedJob = jobs.get(position);
                this.iBasicProfileActivityView.setAdapterToJobSpinner(jobs, selectedJob);
                Optional.ifPresent(data.getOccupation(), job -> {
                    Answer selectedJobAnswer = findSelectedAnswer(selectedJob, job);
                    if(selectedJobAnswer!=null){
                        this.selectedJob = new AnswerSelected(selectedJob.getRefName(), selectedJobAnswer);
                        iBasicProfileActivityView.setTextToJob(selectedJobAnswer.getText());
                        Option nextSubJob = findNextSubJob(jobs, selectedJobAnswer.getNextQuestionRefName());
                        updateSubJob(nextSubJob);
                        Optional.ifPresent(data.getOccupationDetail(), subjob->{
                            if(nextSubJob!=null){
                                this.iBasicProfileActivityView.setHintToSubJob(nextSubJob.getText());
                                Optional.ifPresent(findSelectedAnswer(nextSubJob, subjob), nextJob->{
                                    this.selectedSubJob = new AnswerSelected(nextSubJob.getRefName(), nextJob);
                                    iBasicProfileActivityView.setTextToSubJob(nextJob.getText());
                                });
                            }
                        });
                    }
                });
            });
        });
    }

    private Answer findSelectedAnswer(Option selectedOption, String selectedAnswer){
        for(Answer answer: selectedOption.getAnswers()){
            if(TextUtil.equals(selectedAnswer, answer.getRefName())){
                return answer;
            }
        }
        return null;
    }

    @Override
    public void onJobItemSelected(List<Option> jobs, Option selectedJob, Answer givenAnswerForSelectedJob) {
        this.selectedJob = new AnswerSelected(selectedJob.getRefName(), givenAnswerForSelectedJob);
        Optional.ifPresent(givenAnswerForSelectedJob.getNextQuestionRefName(), nextQuestion -> {
            updateSubJob(findNextSubJob(jobs, givenAnswerForSelectedJob.getNextQuestionRefName()));
        }, this::subCategoryDoesNotExist);
    }

    private Option findNextSubJob(List<Option> jobs, String selectedJobRefName) {
        Option nextSubJob = null;
        for (Option job : jobs) {
            if (TextUtil.equals(selectedJobRefName, job.getRefName(), false)) {
                nextSubJob = job;
            }
        }
        return nextSubJob;
    }

    private void updateSubJob(Option nextSubJob) {
        Optional.ifPresent(nextSubJob, (nextJob) -> {
            iBasicProfileActivityView.setVisibillityForSubJobSpinner(View.VISIBLE);
            this.iBasicProfileActivityView.setHintToSubJob(nextSubJob.getText());
            iBasicProfileActivityView.setAdapterToSubJobSpinner(nextJob.getAnswers(),
                    (parent, view, position, id) -> this.selectedSubJob =
                            new AnswerSelected(nextSubJob.getRefName(), nextJob.getAnswers().get(position)));
        }, this::subCategoryDoesNotExist);
    }

    @Override
    public void subCategoryDoesNotExist() {
        this.selectedSubJob = null;
        iBasicProfileActivityView.setVisibillityForSubJobSpinner(View.GONE);
    }

    @Override
    public void categoryDoesNotExist(){
        this.selectedJob = null;
        iBasicProfileActivityView.setTextToJob("");
    }

    @Override
    public void onQuestionOptionsFailure(int errorCode, String message) {
        this.iBasicProfileActivityView.showError(errorCode,
                (d, v) -> this.iBasicProfileActivityView.finishActivity());
    }
}
