package com.tez.androidapp.app.general.feature.wait.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.cnic.models.network.CNICDetails;
import com.tez.androidapp.commons.models.network.LoanDetails;

import java.io.Serializable;
import java.util.List;

/**
 * Created  on 4/7/2017.
 */

public class LoanStatusResponse extends BaseResponse implements Serializable {

    private String status;
    private LoanDetails loanDetails;
    private List<CNICDetails> cnicDetails;
    private Integer loanId;
    private Boolean limitCancelLock;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LoanDetails getLoanDetails() {
        return loanDetails;
    }

    public List<CNICDetails> getCNICDetails() {
        return cnicDetails;
    }

    public Integer getLoanId() {
        return loanId;
    }

    public Boolean getLimitCancelLock() {
        return limitCancelLock;
    }

    @Override
    public String toString() {
        return "LoanStatusResponse{" +
                "status='" + status + '\'' +
                ", loanDetails=" + loanDetails +
                ", cnicDetails=" + cnicDetails +
                ", loanId=" + loanId +
                "} " + super.toString();
    }
}
