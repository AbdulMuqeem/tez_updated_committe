package com.tez.androidapp.rewamp.general.beneficiary.entity;

import java.io.Serializable;

public class BeneficiaryRelation implements Serializable {

    private Integer relationshipId;
    private boolean isSelectable;
    private String errorDescription;
    private String name;
    private boolean isNameEditable;

    public Integer getRelationshipId() {
        return relationshipId;
    }

    public boolean isSelectable() {
        return isSelectable;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public String getName() {
        return name;
    }

    public boolean isNameEditable() {
        return isNameEditable;
    }
}
