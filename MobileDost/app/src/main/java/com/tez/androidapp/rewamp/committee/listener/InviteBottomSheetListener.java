package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.view.InviteBottomSheetDialog;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface InviteBottomSheetListener {
    void onDialogOptionClick(InviteBottomSheetDialog.InviteOption contact);
}
