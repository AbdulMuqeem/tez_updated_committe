package com.tez.androidapp.rewamp.general.feedback.view;

import android.os.Bundle;
import android.view.View;
import android.widget.RatingBar;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.constraintlayout.widget.Group;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.general.feedback.presenter.ILoanFeedbackActivityPresenter;
import com.tez.androidapp.rewamp.general.feedback.presenter.LoanLoanFeedbackActivityPresenter;
import com.tez.androidapp.rewamp.general.feedback.router.LoanFeedbackActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.BindViews;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public class LoanFeedbackActivity extends BaseActivity implements ILoanFeedbackActivityView, View.OnClickListener, RatingBar.OnRatingBarChangeListener {

    private static int FULL_RATING_THRESHOLD = 4;

    @BindViews({
            R.id.tvWalletIssue,
            R.id.tvLengthyProcess,
            R.id.tvLessTimeToRepay,
            R.id.tvFacedErrors,
            R.id.tvOther
    })
    private List<TezTextView> tvListReasons;

    @BindView(R.id.etFeedback)
    private TezEditTextView etFeedback;

    @BindView(R.id.ratingBar)
    private AppCompatRatingBar ratingBar;

    @BindView(R.id.groupFeedback)
    private Group groupFeedback;

    @BindView(R.id.groupReasons)
    private Group groupReasons;

    @BindView(R.id.ivEmoticon)
    private TezImageView ivEmoticon;

    @BindView(R.id.btSubmit)
    private TezButton btSubmit;

    @BindView(R.id.tvMessage)
    private TezTextView tvMessage;

    @Nullable
    private Integer cause;

    private final ILoanFeedbackActivityPresenter iLoanFeedbackActivityPresenter;

    public LoanFeedbackActivity() {
        this.iLoanFeedbackActivityPresenter = new LoanLoanFeedbackActivityPresenter(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_feedback);
        ViewBinder.bind(this);
        init();
    }

    private void init() {
        initListeners();
    }

    private void initListeners() {
        for (TezTextView view : tvListReasons)
            view.setOnClickListener(this);

        this.ratingBar.setOnRatingBarChangeListener(this);

        this.btSubmit.setDoubleTapSafeOnClickListener(view ->
                iLoanFeedbackActivityPresenter.submitFeedback(getLoanId(),
                        (int) ratingBar.getRating(),
                        cause,
                        etFeedback.getValueText(),
                        ratingBar.getRating() >= FULL_RATING_THRESHOLD));
    }

    private int getLoanId() {
        return getIntent().getIntExtra(LoanFeedbackActivityRouter.LOAN_ID, -1);
    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        int minRating = 1;
        if (rating >= minRating) {
            boolean fullRating = rating >= FULL_RATING_THRESHOLD;
            boolean feedbackVisible = groupFeedback.getVisibility() == View.VISIBLE;
            tvMessage.setText(fullRating ? R.string.hello_how_did_you_like_our_loan_service : R.string.we_are_really_sorry_you_feel_this_way);
            groupReasons.setVisibility(fullRating ? View.GONE : View.VISIBLE);
            groupFeedback.setVisibility(!fullRating && feedbackVisible ? View.VISIBLE : View.GONE);
            btSubmit.setButtonBackgroundStyle(fullRating ? TezButton.ButtonStyle.NORMAL : TezButton.ButtonStyle.INACTIVE);
            btSubmit.setEnabled(fullRating);
            btSubmit.setVisibility(View.VISIBLE);
            setIvEmoticonImage((int) rating);
            ivEmoticon.setVisibility(!fullRating && feedbackVisible ? View.GONE : View.VISIBLE);
            if (fullRating)
                changeBackground(0);
        } else
            ratingBar.setRating(minRating);
    }

    private void setIvEmoticonImage(int rating) {

        @DrawableRes int imgRes;

        if (rating >= FULL_RATING_THRESHOLD)
            imgRes = R.drawable.ic_happy_face;

        else if (rating == 3)
            imgRes = R.drawable.ic_normal_face;

        else
            imgRes = R.drawable.ic_sad_face;

        ivEmoticon.setImageResource(imgRes);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.tvWalletIssue:
                this.cause = 275;
                break;

            case R.id.tvLengthyProcess:
                this.cause = 276;
                break;

            case R.id.tvLessTimeToRepay:
                this.cause = 277;
                break;

            case R.id.tvFacedErrors:
                this.cause = 278;
                break;

            case R.id.tvOther:
                this.cause = 279;
                break;
        }

        ivEmoticon.setVisibility(View.GONE);
        groupFeedback.setVisibility(View.VISIBLE);
        changeBackground(v.getId());
        btSubmit.setButtonNormal();
        btSubmit.setEnabled(true);
        etFeedback.requestFocus();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    private void changeBackground(int viewId) {
        int filled = R.drawable.ic_cancel_limit_reason_filled;
        int empty = R.drawable.ic_cancel_limit_reasong_bg_empty;
        for (TezTextView tezTextView : tvListReasons) {
            if (tezTextView.getId() == viewId)
                tezTextView.setBackgroundResource(filled);
            else
                tezTextView.setBackgroundResource(empty);
        }
    }

    @Override
    protected String getScreenName() {
        return "FeedbackActivity";
    }
}
