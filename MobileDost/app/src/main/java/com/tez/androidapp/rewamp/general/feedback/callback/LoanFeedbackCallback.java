package com.tez.androidapp.rewamp.general.feedback.callback;

import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsResponse;

public interface LoanFeedbackCallback {

    void onSubmitFeedbackSuccess(DashboardCardsResponse dashboardCardsResponse);

    void onSubmitFeedbackFailure(int errorCode, String message);
}
