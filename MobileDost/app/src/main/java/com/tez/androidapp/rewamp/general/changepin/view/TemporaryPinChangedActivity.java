package com.tez.androidapp.rewamp.general.changepin.view;

import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.signup.PinChangedActivity;

public class TemporaryPinChangedActivity extends PinChangedActivity {

    @Override
    protected void onClickBtOkay() {
        DashboardActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }

    @Override
    protected String getScreenName() {
        return "TemporaryPinChangedActivity";
    }
}
