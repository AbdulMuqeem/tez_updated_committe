package com.tez.androidapp.commons.utils.cnic.detection.contracts;


import com.tez.androidapp.commons.utils.cnic.detection.model.RetumDataModel;

public interface CNICDetectionCallback {

    void onCNICDetectionSuccess(byte[] imageBytes, int width, int height, int previewFormat, RetumDataModel retumDataModel);

    void onCNICDetectionFailure();
}
