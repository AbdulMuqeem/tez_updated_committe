package com.tez.androidapp.rewamp.signup.presenter;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

public interface ISignupNumberVerificationInteractorOutput {
    void onSubmitSignUpRequestSuccess();

    void onSubmitSignUpRequestFailure(int errorCode, String message);

    void mobileNumberAlreadyExistForSignUp();
}
