package com.tez.androidapp.rewamp.bima.products.adapter

import android.view.View
import android.view.ViewGroup
import com.tez.androidapp.R
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.Claim
import com.tez.androidapp.commons.utils.app.Constants
import com.tez.androidapp.commons.utils.app.Utility
import kotlinx.android.synthetic.main.list_item_claim.view.*
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener
import net.tez.tezrecycler.base.viewholder.BaseViewHolder

class ClaimsAdapter(items: List<Claim>, listener: ClaimListener?) :
        GenericRecyclerViewAdapter<Claim, ClaimsAdapter.ClaimListener, ClaimsAdapter.ClaimViewHolder>(items, listener) {

    interface ClaimListener : BaseRecyclerViewListener {

        fun onClickClaim(claim: Claim)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClaimViewHolder {
        val view = inflate(parent.context, R.layout.list_item_claim, parent)
        return ClaimViewHolder(view)
    }

    class ClaimViewHolder(itemView: View) : BaseViewHolder<Claim, ClaimListener>(itemView) {

        override fun onBind(item: Claim, listener: ClaimListener?) {
            super.onBind(item, listener)
            itemView.run {
                ivIcon.setImageResource(Utility.getInsuranceProductIcon(item.productId))
                tvTitle.text = item.insurancePolicyName
                tvDescription.text = item.mobileUserInsurancePolicyNumber
                tvClaimStatus.text = item.claimStatus
                tvClaimAmount.text = context.getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(item.claimAmount))
                tvClaimDate.text = Utility.getFormattedDate(item.claimDate, Constants.INPUT_DATE_FORMAT, Constants.UI_DATE_FORMAT)
                tvClaimNumber.text = item.claimReferenceNumber
                tvClaimStatus.setTextColor(Utility.getColorFromResource(item.getClaimStatus().textColor))
                tvClaimStatus.setBackgroundResource(item.getClaimStatus().background)
            }

            setItemViewOnClickListener { listener?.onClickClaim(item) }
        }
    }
}