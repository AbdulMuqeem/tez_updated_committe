package com.tez.androidapp.rewamp.committee.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.CommitteeAuthCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.JoinCommitteeInvitationListener;
import com.tez.androidapp.rewamp.committee.presenter.IJoinCommitteeInvitationActivityInteractorOutput;
import com.tez.androidapp.rewamp.committee.response.GetInvitedPackageResponse;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class JoinCommitteeInvitationActivityInteractor implements IJoinCommitteeInvitationActivityInteractor {

    private final IJoinCommitteeInvitationActivityInteractorOutput mIJoinCommitteeInvitationActivityInteractorOutput;

    public JoinCommitteeInvitationActivityInteractor(IJoinCommitteeInvitationActivityInteractorOutput mIJoinCommitteeInvitationActivityInteractorOutput) {
        this.mIJoinCommitteeInvitationActivityInteractorOutput = mIJoinCommitteeInvitationActivityInteractorOutput;
    }

    @Override
    public void getInvitedPackage(String mobileNumber) {
        CommitteeAuthCloudDataStore.getInstance().getInvitedPackage(mobileNumber, new JoinCommitteeInvitationListener() {
            @Override
            public void onGetInvitedPackageSuccess(GetInvitedPackageResponse getInvitedPackageResponse) {
                mIJoinCommitteeInvitationActivityInteractorOutput.onGetInvitedPackageSuccess(getInvitedPackageResponse);
            }

            @Override
            public void onGetInvitedPackageFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getInvitedPackage(mobileNumber);
                else
                    mIJoinCommitteeInvitationActivityInteractorOutput.onGetInvitedPackageFailure(errorCode, message);
            }
        });
    }
}
