package com.tez.androidapp.rewamp.gold.onboarding.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.gold.onboarding.view.GoldOnboardingActivity;

public class GoldOnboardingActivityRouter extends BaseActivityRouter {

    public static GoldOnboardingActivityRouter createInstance() {
        return new GoldOnboardingActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, new Intent(from, GoldOnboardingActivity.class));
    }
}
