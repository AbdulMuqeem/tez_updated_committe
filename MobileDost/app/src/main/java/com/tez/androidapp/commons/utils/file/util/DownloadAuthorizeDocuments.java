package com.tez.androidapp.commons.utils.file.util;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.commons.utils.app.Utility;

import java.io.File;

/**
 * Created by FARHAN DHANANI on 7/16/2018.
 */
public interface DownloadAuthorizeDocuments {

    static void downloadDocuments(@NonNull final Context context,
                                  @NonNull final Uri fileUri,
                                  @NonNull final String documentName,
                                  final boolean enableAuthorization,
                                  @Nullable final String signature,
                                  @Nullable final String destinationDirectory,
                                  @Nullable final DownloadDocumentsCallBack documentsCallBack) {
        String slash = "/";

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()
                + slash
                + (signature == null ? documentName : signature));

        if (!file.exists()) {
            IDocumentDownloader iDocumentDownloader = new DownloadDocuments(context) {
                @NonNull
                @Override
                public Uri getFileUrl() {
                    return fileUri;
                }

                @NonNull
                @Override
                public String getFileTitle() {
                    return documentName;
                }

                @Nullable
                @Override
                public String getSignature() {
                    return signature;
                }

                @Override
                public boolean getAuthorization() {
                    return enableAuthorization;
                }

                @Override
                public String getDestinationFileDirectory() {
                    return destinationDirectory == null ?
                            super.getDestinationFileDirectory() :
                            destinationDirectory;
                }
            };
            DownloadDocumentsCallBack downloadDocumentsCallBack = new DownloadDocumentsCallBack() {
                @Override
                public void onSuccess(final Uri uri, String mimeType) {
                    if (documentsCallBack != null)
                        documentsCallBack.onSuccess(uri, mimeType);
                }

                @Override
                public void onFailure(int errorCode, @Nullable String message) {
                    if (Utility.isUnauthorized(errorCode)) {
                        iDocumentDownloader.downloadDocuments(this);
                    } else if (documentsCallBack != null) {
                        documentsCallBack.onFailure(errorCode, message);
                    }
                }
            };
            iDocumentDownloader.downloadDocuments(downloadDocumentsCallBack);
        } else {
            context.sendBroadcast(IDocumentDownloader.
                    createIntentForDownloadCompletedBroadCastReceiver(Uri.fromFile(file).toString()));
        }
    }
}
