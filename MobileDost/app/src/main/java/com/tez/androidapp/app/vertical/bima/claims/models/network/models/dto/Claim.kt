package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto

import com.tez.androidapp.rewamp.bima.claim.ClaimStatus
import java.io.Serializable

/**
 * * Created by Vinod Kumar on 10/14/2020.
 */
data class Claim(
        val id: Int,
        val mobileUserInsurancePolicyNumber: String,
        val claimAmount: Double,
        val claimDate: String,
        val insurancePolicyName: String,
        val claimStatus: String,
        val claimReferenceNumber: String,
        val productId: Int,
) : Serializable {

    fun getClaimStatus() = ClaimStatus.valueOf(claimStatus)
}