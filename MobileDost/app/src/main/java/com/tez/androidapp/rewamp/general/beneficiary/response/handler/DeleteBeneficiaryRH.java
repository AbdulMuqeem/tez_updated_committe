package com.tez.androidapp.rewamp.general.beneficiary.response.handler;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.general.beneficiary.listener.DeleteBeneficiaryListener;

public class DeleteBeneficiaryRH extends BaseRH<BaseResponse> {

    private final DeleteBeneficiaryListener listener;

    public DeleteBeneficiaryRH(BaseCloudDataStore baseCloudDataStore, DeleteBeneficiaryListener listener) {
        super(baseCloudDataStore);
        this.listener = listener;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse response = value.response().body();

        if (response != null) {

            if (isErrorFree(response))
                listener.onDeleteBeneficiarySuccess(response);
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());
        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        listener.onDeleteBeneficiaryFailure(errorCode, message);
    }
}
