package com.tez.androidapp.app.general.feature.authentication.signup.callbacks;

/**
 * Created  on 3/22/2017.
 */

public interface ResendOTPVerifyMobileNumberCallback {

    void onResendOTPVerifyMobileNumberSuccess();

    void onResendOTPVerifyMobileNumberFailure(String message);
}
