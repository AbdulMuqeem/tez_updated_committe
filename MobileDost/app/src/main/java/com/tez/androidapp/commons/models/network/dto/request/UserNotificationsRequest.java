package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 8/22/2017.
 */

public class UserNotificationsRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/notifications";

    public static final class Params {
        public static final String START_INDEX = "startIndex";
        public static final String MAX_RECORDS = "maxRecords";

        private Params() {
        }
    }
}
