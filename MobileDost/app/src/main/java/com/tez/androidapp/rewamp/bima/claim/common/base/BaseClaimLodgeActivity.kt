package com.tez.androidapp.rewamp.bima.claim.common.base

import android.os.Bundle
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import com.tez.androidapp.R
import com.tez.androidapp.app.base.ui.activities.BaseActivity
import com.tez.androidapp.commons.widgets.StepperView
import com.tez.androidapp.rewamp.bima.claim.common.success.router.ClaimSubmittedActivityRouter
import com.tez.androidapp.rewamp.bima.claim.health.listener.ClaimStepListener
import com.tez.androidapp.rewamp.bima.claim.health.router.BaseClaimLodgeActivityRouter
import com.tez.androidapp.rewamp.bima.claim.model.InsuranceClaimConfirmationDto
import kotlinx.android.synthetic.main.activity_base_claim_lodge.*

abstract class BaseClaimLodgeActivity : BaseActivity(), ClaimStepListener {
    private lateinit var claimProcessNavDirectionsList: List<NavDirections>
    protected lateinit var dependencies: BaseClaimLodgeActivityRouter.Dependencies

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base_claim_lodge)
        dependencies = BaseClaimLodgeActivityRouter.getDependencies(intent)
        claimProcessNavDirectionsList = initNavDirections()
        stepperView.setSteps(initStepperList())
    }

    override fun setStep(stepNumber: Int) = stepperView.setStepNumber(stepNumber)

    override fun navigate(currentStep: Int) {
        val navDirections = claimProcessNavDirectionsList[currentStep - 1]
        findNavController(R.id.tezNavHostFragment).navigate(navDirections)
    }

    override fun onClaimSubmitted(insuranceClaimConfirmationDto: InsuranceClaimConfirmationDto) {
        ClaimSubmittedActivityRouter().setDependenciesAndRoute(this,
                dependencies.productId,
                false,
                insuranceClaimConfirmationDto)
        finish()
    }

    protected abstract fun initNavDirections(): List<NavDirections>
    protected abstract fun initStepperList(): List<StepperView.Step>
}