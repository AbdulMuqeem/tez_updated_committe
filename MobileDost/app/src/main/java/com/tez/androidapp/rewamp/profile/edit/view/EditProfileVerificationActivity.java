package com.tez.androidapp.rewamp.profile.edit.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.profile.edit.presenter.EditProfileActivityPresenter;
import com.tez.androidapp.rewamp.profile.edit.presenter.EditProfileVerificationActivityPresenter;
import com.tez.androidapp.rewamp.profile.edit.presenter.IEditProfileVerificationActivityPresenter;
import com.tez.androidapp.rewamp.profile.edit.router.EditProfileVerificationActivityRouter;
import com.tez.androidapp.rewamp.signup.router.NumberVerificationActivityRouter;
import com.tez.androidapp.rewamp.signup.router.SignupNumberVerificationActivityRouter;
import com.tez.androidapp.rewamp.signup.verification.NumberVerificationActivity;

import net.tez.logger.library.utils.TextUtil;

public class EditProfileVerificationActivity
        extends NumberVerificationActivity implements IEditProfileVerificationActivityView{

    private final IEditProfileVerificationActivityPresenter iEditProfileVerificationActivityPresenter;

    public EditProfileVerificationActivity(){
        this.iEditProfileVerificationActivityPresenter = new EditProfileVerificationActivityPresenter(this);
    }

    @Override
    protected String getFlashCallLabel() {
        return "EDIT_PROFILE";
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    public void onUserSessionTimeOut() { }

    @Override
    public boolean canStartNumberVerification() {
        return !isMobileNumberChanged();
    }

    private boolean isMobileNumberChanged() {
        return !TextUtil.equals(getMobileNumberFromIntent(), getTextFromEtMobileNumber());
    }

    @Override
    public void preliminaryNumberVerificationWork() {
        super.preliminaryNumberVerificationWork();
        this.iEditProfileVerificationActivityPresenter.validateUser(getTextFromEtMobileNumber());
    }

    @Override
    public void createSinchVerification(String mobileNumber) {
        getIntent().putExtra(NumberVerificationActivityRouter.MOBILE_NUMBER, mobileNumber);
        startNumberVerification();
    }

    public void onVerified(){
        super.onVerified();
        this.iEditProfileVerificationActivityPresenter.updateUserProfile();
    }

    @Override
    public String getMobileNumber(){
        return getTextFromEtMobileNumber();
    }

    @Override
    public String getEmail(){
        return getIntent().getStringExtra(EditProfileVerificationActivityRouter.EMAIL);
    }

    @Override
    public String getCurrentAddress(){
        return getIntent().getStringExtra(EditProfileVerificationActivityRouter.CURRENT_ADDRESS);
    }

    @Override
    public int getSelectedCityId(){
        return getIntent().getIntExtra(EditProfileVerificationActivityRouter.SELECTED_CITY, -1);
    }

    @Override
    public void onCountDownFinish() {
        super.onCountDownFinish();
        setEnableEtMobileNumber(true);
    }

    @Override
    protected String getScreenName() {
        return "EditProfileVerificationActivity";
    }
}
