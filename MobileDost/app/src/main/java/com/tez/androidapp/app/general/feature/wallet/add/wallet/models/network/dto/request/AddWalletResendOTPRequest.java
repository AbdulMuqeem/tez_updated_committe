package com.tez.androidapp.app.general.feature.wallet.add.wallet.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 2/14/2017.
 */

public class AddWalletResendOTPRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/addWallet/resendOtp/{" + Params.MOBILE_ACCOUNT_NUMBER + "}/{" + Params.SERVICE_PROVIDER_ID + "}";


    public static final class Params {

        public static final String SERVICE_PROVIDER_ID = "serviceProviderId";
        public static final String MOBILE_ACCOUNT_NUMBER = "mobileAccountNumber";

        private Params() {
        }
    }
}
