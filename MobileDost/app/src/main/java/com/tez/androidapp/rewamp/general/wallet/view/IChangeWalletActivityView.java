package com.tez.androidapp.rewamp.general.wallet.view;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;

import java.util.List;

public interface IChangeWalletActivityView extends IBaseView {

    void initAdapter(@NonNull List<Wallet> walletList);

    void setClContentVisibility(int visibility);

    void showShimmer();

    void hideShimmer();
}
