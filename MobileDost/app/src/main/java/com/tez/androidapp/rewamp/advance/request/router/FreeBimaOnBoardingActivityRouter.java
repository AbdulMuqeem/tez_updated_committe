package com.tez.androidapp.rewamp.advance.request.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.request.view.FreeBimaOnBoardingActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

public class FreeBimaOnBoardingActivityRouter extends BaseActivityRouter {

    public static FreeBimaOnBoardingActivityRouter createInstance() {
        return new FreeBimaOnBoardingActivityRouter();
    }


    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, FreeBimaOnBoardingActivity.class);
    }
}
