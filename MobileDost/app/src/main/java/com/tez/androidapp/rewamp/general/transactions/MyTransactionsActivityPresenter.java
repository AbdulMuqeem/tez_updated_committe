package com.tez.androidapp.rewamp.general.transactions;

import android.view.View;

import com.tez.androidapp.app.general.feature.transactions.models.network.Transaction;
import com.tez.androidapp.app.general.feature.transactions.models.network.dto.response.GetUserTransactionsResponse;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import java.util.List;

/**
 * Created by VINOD KUMAR on 8/28/2019.
 */
public class MyTransactionsActivityPresenter implements IMyTransactionsActivityPresenter, IMyTransactionsActivityInteractorOutput {

    private final IMyTransactionsActivityView iMyTransactionsActivityView;
    private final IMyTransactionsActivityInteractor iMyTransactionsActivityInteractor;

    MyTransactionsActivityPresenter(IMyTransactionsActivityView iMyTransactionsActivityView) {
        this.iMyTransactionsActivityView = iMyTransactionsActivityView;
        iMyTransactionsActivityInteractor = new MyTransactionsActivityInteractor(this);
    }

    @Override
    public void getTransactions() {
        iMyTransactionsActivityView.showTezLoader();
        iMyTransactionsActivityInteractor.getTransactions();
    }

    @Override
    public void onGetUserTransactionsSuccess(GetUserTransactionsResponse getUserTransactionsResponse) {
        List<Transaction> transactionList = getUserTransactionsResponse.getTransactionList();
        if (transactionList != null && !transactionList.isEmpty())
            iMyTransactionsActivityView.setMyTransactionsAdapter(transactionList);
        else {
            iMyTransactionsActivityView.setRvMyActivitiesVisibility(View.GONE);
            iMyTransactionsActivityView.setLayoutEmptyTransactionVisibility(View.VISIBLE);
        }
        iMyTransactionsActivityView.dismissTezLoader();
    }

    @Override
    public void onGetUserTransactionsFailure(int statusCode, String message) {
        iMyTransactionsActivityView.dismissTezLoader();
        iMyTransactionsActivityView.showError(statusCode,
                (dialog, which) -> iMyTransactionsActivityView.finishActivity());
    }
}
