package com.tez.androidapp.rewamp.profile.trust.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.profile.router.CompleteProfileRouter;
import com.tez.androidapp.rewamp.profile.trust.view.ProfileTrustActivity;

public class ProfileTrustActivityRouter extends CompleteProfileRouter {

    public static ProfileTrustActivityRouter createInstance() {
        return new ProfileTrustActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        addAfterCompletionRoute(intent);
        route(from, intent);
    }

    public void setDependenciesAndRouteWithClearAndNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        addAfterCompletionRoute(intent);
        route(from, intent);
    }


    public void setDependenciesAndRoute(@NonNull BaseActivity from, int routeTo) {
        Intent intent = createIntent(from);
        addAfterCompletionRoute(intent, routeTo);
        route(from, intent);
    }

    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, ProfileTrustActivity.class);
    }
}
