package com.tez.androidapp.rewamp.general.suspend.account.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.suspend.account.view.SuspendAccountOTPActivity;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public class SuspendAccountOTPActivityRouter extends BaseActivityRouter {

    public static SuspendAccountOTPActivityRouter createInstance() {
        return new SuspendAccountOTPActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, SuspendAccountOTPActivity.class);
    }
}
