package com.tez.androidapp.rewamp.gold.onboarding.view;

import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.ViewModelProvider;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.general.network.model.NetworkCall;
import com.tez.androidapp.rewamp.gold.onboarding.viewmodel.Gold0nboardingViewModel;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.OnClick;
import net.tez.viewbinder.library.core.ViewBinder;

public class GoldOnboardingActivity extends BaseActivity {

    @BindView(R.id.tvGoldUnit)
    private TezTextView tvGoldUnit;

    @BindView(R.id.tvMessage)
    private TezTextView tvMessage;

    @BindView(R.id.tvGoldRate)
    private TezTextView tvGoldRate;

    @BindView(R.id.clContent)
    private TezConstraintLayout clContent;

    @BindView(R.id.shimmer)
    private TezLinearLayout shimmer;

    private Gold0nboardingViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gold_onboarding);
        ViewBinder.bind(this);
        this.viewModel = new ViewModelProvider(this).get(Gold0nboardingViewModel.class);
        init();
    }

    private void init() {
        initGoldRateLiveData();
        initNetworkLiveData();
    }

    private void initGoldRateLiveData() {
        viewModel.getGoldRateLiveData().observe(this, response -> {
            tvGoldUnit.setText(response.getUnit());
            String finalAmount = response.getGoldRate();
            double amount = Utility.getPatternAmount(response.getGoldRate());
            if (amount != -1) {
                finalAmount = Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(amount);
            }
            tvGoldRate.setText(getString(R.string.rs_value_string, finalAmount));

            String fetchTime = Utility.getFormattedDate(response.getFetchTime(),
                    Constants.INPUT_DATE_WITHOUT_ZONE_FORMAT,
                    Constants.OUTPUT_DATE_WITH_TIME_FORMAT);
            tvMessage.setText(getString(R.string.gold_rates_as_of, fetchTime));
        });
    }

    private void initNetworkLiveData() {
        viewModel.getNetworkCallMutableLiveData().observe(this, networkCall -> {
            NetworkCall.State networkState = networkCall.getState();
            switch (networkState) {
                case LOADING:
                    clContent.setVisibility(View.GONE);
                    shimmer.setVisibility(View.VISIBLE);
                    break;
                case FAILURE:
                    shimmer.setVisibility(View.GONE);
                    showError(networkCall.getErrorCode(), (dialog, which) -> finishActivity());
                    break;
                case SUCCESS:
                    shimmer.setVisibility(View.GONE);
                    clContent.setVisibility(View.VISIBLE);
                    break;
            }
        });
    }

    @OnClick(R.id.btOkay)
    private void routeToDashboard() {
        DashboardActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this);
    }

    @Override
    protected String getScreenName() {
        return "GoldOnboardingActivity";
    }
}