package com.tez.androidapp.rewamp.committee.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeLeaveRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/committee/leave/{" + Params.ID + "}";

//    http://3.21.127.35/api/

    public static final class Params {
        public static final String ID = "id";

        private Params() {
        }
    }

}
