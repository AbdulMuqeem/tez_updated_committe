package com.tez.androidapp.rewamp.bima.claim.response

import com.tez.androidapp.app.base.response.BaseResponse
import com.tez.androidapp.rewamp.bima.claim.model.InsuranceClaimConfirmationDto

class ConfirmClaimResponse(val insuranceClaimConfirmationDto: InsuranceClaimConfirmationDto)
    : BaseResponse()