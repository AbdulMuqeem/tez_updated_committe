package com.tez.androidapp.rewamp.general.wallet.interactor;

import com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks.GetAllWalletCallback;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.general.wallet.presenter.IChangeWalletActivityInteractorOutput;

import java.util.List;

public class ChangeWalletActivityInteractor implements IChangeWalletActivityInteractor {

    private final IChangeWalletActivityInteractorOutput iChangeWalletActivityInteractorOutput;

    public ChangeWalletActivityInteractor(IChangeWalletActivityInteractorOutput iChangeWalletActivityInteractorOutput) {
        this.iChangeWalletActivityInteractorOutput = iChangeWalletActivityInteractorOutput;
    }

    @Override
    public void getAllWallets() {
        UserAuthCloudDataStore.getInstance().getAllWallet(new GetAllWalletCallback() {
            @Override
            public void onGetAllWalletSuccess(List<Wallet> wallets) {
                iChangeWalletActivityInteractorOutput.onGetAllWalletSuccess(wallets);
            }

            @Override
            public void onGetAllWalletFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getAllWallets();
                else
                    iChangeWalletActivityInteractorOutput.onGetAllWalletFailure(errorCode, message);
            }
        });
    }
}
