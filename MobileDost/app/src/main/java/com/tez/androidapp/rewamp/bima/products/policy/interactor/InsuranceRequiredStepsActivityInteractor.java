package com.tez.androidapp.rewamp.bima.products.policy.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BimaCloudDataStore;
import com.tez.androidapp.repository.network.store.LoanCloudDataStore;
import com.tez.androidapp.rewamp.advance.request.callback.LoanRemainingStepsCallback;
import com.tez.androidapp.rewamp.advance.request.interactor.LoanRequiredStepsActivityInteractor;
import com.tez.androidapp.rewamp.advance.request.presenter.ILoanRequiredStepsActivityInteractorOutput;

import java.util.List;

public class InsuranceRequiredStepsActivityInteractor extends LoanRequiredStepsActivityInteractor {

    public InsuranceRequiredStepsActivityInteractor(ILoanRequiredStepsActivityInteractorOutput iLoanRequiredStepsActivityInteractorOutput) {
        super(iLoanRequiredStepsActivityInteractorOutput);
    }

    @Override
    public void getLoanRemainingSteps() {
        BimaCloudDataStore.getInstance().getInsuranceRemainingSteps(new LoanRemainingStepsCallback(){

            @Override
            public void onGetLoanRemainingStepsSuccess(List<String> stepsLeft) {
                iLoanRequiredStepsActivityInteractorOutput.onGetLoanRemainingStepsSuccess(stepsLeft);
            }

            @Override
            public void onGetLoanRemainingStepsFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getLoanRemainingSteps();
                else
                    iLoanRequiredStepsActivityInteractorOutput.onGetLoanRemainingStepsFailure(errorCode, message);
            }
        });
    }
}
