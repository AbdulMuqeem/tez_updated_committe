package com.tez.androidapp.rewamp.bima.products.policy.presenter;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.rewamp.bima.products.policy.callbacks.InitiatePurchasePolicyCallback;
import com.tez.androidapp.rewamp.bima.products.policy.callbacks.PurchasePolicyCallback;
import com.tez.androidapp.rewamp.bima.products.policy.response.PolicyDetails;

public interface IEasyPaisaInsurancePaymentActivityInteractorOutput
        extends InitiatePurchasePolicyCallback,
        PurchasePolicyCallback {

    void initiatePurchasePolicySuccess(PolicyDetails policyDetails, @NonNull String pin,
                                       @Nullable Location location);
}
