package com.tez.androidapp.app.general.feature.account.feature.profile.cnic.models.network;

import com.tez.androidapp.commons.utils.app.Constants;

import java.io.Serializable;

/**
 * Created  on 4/14/2017.
 */

public class CNICDetails implements Serializable {

    private String fileName;
    private String status;
    private String url;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
