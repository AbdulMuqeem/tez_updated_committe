package com.tez.androidapp.app.vertical.advance.loan.shared.limit.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.response.LoanLimitResponse;
import com.tez.androidapp.app.vertical.advance.loan.shared.limit.callbacks.LoanLimitCallback;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsRH;

/**
 * Created  on 3/1/2017.
 */

public class LoanLimitRH extends DashboardCardsRH<LoanLimitResponse> {

    private LoanLimitCallback loanLimitCallback;

    public LoanLimitRH(BaseCloudDataStore baseCloudDataStore, LoanLimitCallback loanLimitCallback) {
        super(baseCloudDataStore);
        this.loanLimitCallback = loanLimitCallback;
    }

    @Override
    protected void onSuccess(Result<LoanLimitResponse> value) {
        super.onSuccess(value);
        LoanLimitResponse loanLimitResponse = value.response().body();
        if (loanLimitResponse != null) {
            if (isErrorFree(loanLimitResponse))
                loanLimitCallback.onLoanLimitSuccess(loanLimitResponse);
            else
                onFailure(loanLimitResponse.getStatusCode(), loanLimitResponse.getErrorDescription());
        } else sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        loanLimitCallback.onLoanLimitFailure(errorCode, message);
    }
}
