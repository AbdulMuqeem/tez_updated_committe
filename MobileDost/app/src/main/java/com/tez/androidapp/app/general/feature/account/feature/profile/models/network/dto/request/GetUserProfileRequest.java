package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Rehman Murad Ali on 9/8/2017.
 */

public class GetUserProfileRequest extends BaseRequest{
    public static final String METHOD_NAME = "v1/user/account";
}
