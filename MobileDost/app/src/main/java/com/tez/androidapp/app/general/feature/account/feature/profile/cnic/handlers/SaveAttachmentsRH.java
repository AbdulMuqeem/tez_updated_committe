package com.tez.androidapp.app.general.feature.account.feature.profile.cnic.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.app.general.feature.account.feature.profile.cnic.callbacks.SaveAttachmentsCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.cnic.models.network.dto.response.SaveAttachmentsResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created  on 2/15/2017.
 */

public class SaveAttachmentsRH extends BaseRH<SaveAttachmentsResponse> {

    private SaveAttachmentsCallback saveAttachmentsCallback;

    public SaveAttachmentsRH(BaseCloudDataStore baseCloudDataStore,
                             @Nullable SaveAttachmentsCallback saveAttachmentsCallback) {
        super(baseCloudDataStore);
        this.saveAttachmentsCallback = saveAttachmentsCallback;
    }

    @Override
    protected void onSuccess(Result<SaveAttachmentsResponse> value) {
        SaveAttachmentsResponse saveAttachmentsResponse = value.response().body();
        if (saveAttachmentsResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            if (saveAttachmentsResponse.areAllFilesPersisted())

            if (saveAttachmentsCallback != null)
                saveAttachmentsCallback.onSaveAttachmentsSuccess(saveAttachmentsResponse);
        } else {
            onFailure(saveAttachmentsResponse.getStatusCode(), saveAttachmentsResponse.getErrorDescription());
        }
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (saveAttachmentsCallback != null) saveAttachmentsCallback.onSaveAttachmentsFailure(errorCode, message);
    }
}
