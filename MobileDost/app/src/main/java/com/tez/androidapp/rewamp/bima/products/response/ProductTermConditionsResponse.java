package com.tez.androidapp.rewamp.bima.products.response;

import com.tez.androidapp.app.base.response.BaseResponse;

import java.util.List;

public class ProductTermConditionsResponse extends BaseResponse {

    private List<String> termsConditions;

    public List<String> getTermsConditions() {
        return termsConditions;
    }
}
