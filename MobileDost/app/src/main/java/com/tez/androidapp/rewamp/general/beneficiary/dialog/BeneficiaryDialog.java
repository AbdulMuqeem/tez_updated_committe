package com.tez.androidapp.rewamp.general.beneficiary.dialog;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezDialog;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.rewamp.general.beneficiary.router.BeneficiaryListActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class BeneficiaryDialog extends TezDialog {

    @BindView(R.id.cvContent)
    private TezConstraintLayout cvContent;

    @BindView(R.id.ivClose)
    private TezImageView ivClose;

    @BindView(R.id.btManageBeneficiary)
    private TezButton btManageBeneficiary;

    private BeneficiaryDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.beneficiary_dialog);
        ViewBinder.bind(this);
        this.cancelTezDialogOnTouchOutsideOfView(cvContent);
    }

    public static void show(@NonNull BaseActivity activity) {
        BeneficiaryDialog beneficiaryDialog = new BeneficiaryDialog(activity);
        beneficiaryDialog.show();
        beneficiaryDialog.ivClose.setDoubleTapSafeOnClickListener(v -> beneficiaryDialog.dismiss());
        beneficiaryDialog.btManageBeneficiary.setDoubleTapSafeOnClickListener(v -> {
            //BeneficiaryListActivityRouter.createInstance().setDependenciesAndRoute(activity);
            beneficiaryDialog.dismiss();
        });
    }
}
