package com.tez.androidapp.rewamp.bima.claim.common.summary.adapter

import android.view.View
import android.view.ViewGroup
import com.tez.androidapp.R
import com.tez.androidapp.rewamp.bima.claim.common.summary.model.Summary
import kotlinx.android.synthetic.main.list_item_summary.view.*
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener
import net.tez.tezrecycler.base.viewholder.BaseViewHolder

class SummaryAdapter
    : GenericRecyclerViewAdapter<Summary, BaseRecyclerViewListener, SummaryAdapter.SummaryViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SummaryViewHolder {
        val view = inflate(parent.context, R.layout.list_item_summary, parent)
        return SummaryViewHolder(view)
    }

    class SummaryViewHolder(itemView: View) : BaseViewHolder<Summary, BaseRecyclerViewListener>(itemView) {

        override fun onBind(item: Summary, listener: BaseRecyclerViewListener?) {
            super.onBind(item, listener)
            itemView.run {
                tilQuestion.hint = item.question
                etQuestion.setText(item.answer)
                etQuestion.changeDrawable(etQuestion, item.icon, 0, 0, 0)
            }
        }
    }
}