package com.tez.androidapp.rewamp.general.invite.friend.presenter;

import com.tez.androidapp.app.general.feature.inivite.user.callback.GetReferralCodeCallback;

public interface IInviteFriendActivityInteractorOutput extends GetReferralCodeCallback {
}
