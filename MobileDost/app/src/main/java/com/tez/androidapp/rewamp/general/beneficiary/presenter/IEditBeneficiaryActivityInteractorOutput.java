package com.tez.androidapp.rewamp.general.beneficiary.presenter;

import com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks.SetBeneficiaryCallback;

public interface IEditBeneficiaryActivityInteractorOutput extends SetBeneficiaryCallback {
}
