package com.tez.androidapp.app.vertical.bima.claims.callback;

import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.UploadClaimDocumentResponse;

/**
 * Created by FARHAN DHANANI on 6/21/2018.
 */
public interface UploadClaimDocumentCallBack {

    void onUploadClaimDocumentSuccess(UploadClaimDocumentResponse uploadClaimDocumentResponse);

    void onUploadClaimDocumentFailure(int errorCode, String message);
}
