package com.tez.androidapp.app.general.feature.authentication.signup.callbacks;

/**
 * Created  on 3/22/2017.
 */

public interface VerifyMobileNumberOTPCallback {

    void onVerifyMobileNumberOTPSuccess();

    void onVerifyMobileNumberOTPFailure(String message);
}
