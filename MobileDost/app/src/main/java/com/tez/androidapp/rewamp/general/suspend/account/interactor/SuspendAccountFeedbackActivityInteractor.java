package com.tez.androidapp.rewamp.general.suspend.account.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.general.suspend.account.FeedbackCallback;
import com.tez.androidapp.rewamp.general.suspend.account.FeedbackRequest;
import com.tez.androidapp.rewamp.general.suspend.account.presenter.ISuspendAccountFeedbackActivityInteractorOutput;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public class SuspendAccountFeedbackActivityInteractor implements ISuspendAccountFeedbackActivityInteractor {

    private final ISuspendAccountFeedbackActivityInteractorOutput iSuspendAccountFeedbackActivityInteractorOutput;

    public SuspendAccountFeedbackActivityInteractor(ISuspendAccountFeedbackActivityInteractorOutput iSuspendAccountFeedbackActivityInteractorOutput) {
        this.iSuspendAccountFeedbackActivityInteractorOutput = iSuspendAccountFeedbackActivityInteractorOutput;
    }

    @Override
    public void sendFeedback(String feedback) {
        UserAuthCloudDataStore.getInstance().feedback(new FeedbackRequest(feedback), new FeedbackCallback() {
            @Override
            public void onFeedbackSuccess() {
                iSuspendAccountFeedbackActivityInteractorOutput.onFeedbackSuccess();
            }

            @Override
            public void onFeedbackFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    sendFeedback(feedback);
                else
                    iSuspendAccountFeedbackActivityInteractorOutput.onFeedbackFailure(errorCode, message);
            }
        });
    }
}
