package com.tez.androidapp.rewamp.bima.products.model

import java.io.Serializable

data class InsuranceProduct(
        val id: Int,
        val title: String,
        val policyTagLine: String,
        val disabled: Boolean,
        val productPurchased: Boolean,
        val policyId: Int? = null,
) : Serializable