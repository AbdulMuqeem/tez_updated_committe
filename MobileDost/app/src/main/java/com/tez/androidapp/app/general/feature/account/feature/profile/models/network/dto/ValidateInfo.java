package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto;

import java.io.Serializable;

/**
 * Created by FARHAN DHANANI on 12/4/2018.
 */
public class ValidateInfo implements Serializable {
    private Boolean cnicExists;
    private Boolean mobileNumberExists;

    public Boolean getCnicExists() {
        return cnicExists;
    }

    public void setCnicExists(Boolean cnicExists) {
        this.cnicExists = cnicExists;
    }

    public Boolean getMobileNumberExists() {
        return mobileNumberExists;
    }

    public void setMobileNumberExists(Boolean mobileNumberExists) {
        this.mobileNumberExists = mobileNumberExists;
    }
}
