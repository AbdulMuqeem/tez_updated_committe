package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.request.CommitteeInstallmentPayRequestCommittee;
import com.tez.androidapp.rewamp.committee.request.CommitteePaymentInitiateRequest;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface ICommitteeWalletVerificationActivityPresenter {

    void initiatePayment(CommitteePaymentInitiateRequest committeePaymentInitiateRequest);
    void payInstallment (CommitteeInstallmentPayRequestCommittee committeeInstallmentPayRequestCommittee);
}
