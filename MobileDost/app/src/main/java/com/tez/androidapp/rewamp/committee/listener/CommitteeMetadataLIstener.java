package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommitteeMetaDataResponse;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface CommitteeMetadataLIstener {

    void onCommitteeMetadataSuccess(CommitteeMetaDataResponse committeeMetaDataResponse);

    void onCommitteeMetadataFailure(int errorCode, String message);

}
