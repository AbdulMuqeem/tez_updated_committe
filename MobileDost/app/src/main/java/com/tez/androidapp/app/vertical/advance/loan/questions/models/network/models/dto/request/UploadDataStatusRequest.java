package com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by FARHAN DHANANI on 10/15/2018.
 */
public class UploadDataStatusRequest extends BaseRequest {
    public static final String METHOD_NAME = "v1/loan/data/upload/status";
}
