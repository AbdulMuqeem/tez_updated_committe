package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.TezDrawableHelper;

import net.tez.fragment.util.base.IBaseWidget;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

/**
 * Created by FARHAN DHANANI on 1/9/2019.
 */
public class TezImageView extends AppCompatImageView implements IBaseWidget {

    @NonNull
    private String imageLabel = "";

    public TezImageView(Context context) {
        super(context);
    }

    public TezImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    @Override
    @NonNull
    public String getLabel() {
        return imageLabel;
    }

    public TezImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TezImageView);

        try {

            int clickEffect = typedArray.getInt(R.styleable.TezImageView_click_effect, NO_EFFECT);
            attachClickEffect(clickEffect, this);
            String label = typedArray.getString(R.styleable.TezImageView_imageLabel);
            Drawable bgDrawable = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezImageView_backgroundCompat);
            if (bgDrawable != null)
                setBackground(bgDrawable);

            if (label != null)
                this.imageLabel = label;

        } finally {
            typedArray.recycle();
        }
    }

    @Override
    public void setDoubleTapSafeOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        super.setOnClickListener(doubleTapSafeOnClickListener);
    }
}
