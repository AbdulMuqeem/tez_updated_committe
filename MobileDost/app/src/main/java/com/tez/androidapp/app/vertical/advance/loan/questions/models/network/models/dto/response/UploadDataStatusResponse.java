package com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.dto.response;

import com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.dto.DataUploadStatus;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsResponse;

/**
 * Created by FARHAN DHANANI on 10/15/2018.
 */
public class UploadDataStatusResponse extends DashboardCardsResponse {

    private DataUploadStatus dataUploadStatus;

    public DataUploadStatus getDataUploadStatus() {
        return dataUploadStatus;
    }
}
