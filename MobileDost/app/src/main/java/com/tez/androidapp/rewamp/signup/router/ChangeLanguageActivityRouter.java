package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.ChangeLanguageActivity;

/**
 * Created by VINOD KUMAR on 8/9/2019.
 */
public class ChangeLanguageActivityRouter extends BaseActivityRouter {

    public static ChangeLanguageActivityRouter createInstance() {
        return new ChangeLanguageActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, ChangeLanguageActivity.class);
    }
}
