package com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.response;

import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsResponse;

/**
 * Created  on 3/1/2017.
 */

public class LoanLimitResponse extends DashboardCardsResponse {

    private Double loanLimit;
    private String expiryDate;
    private Integer loanId;
    private Boolean limitCancelLock;
    private Wallet wallet;

    public Wallet getWallet() {
        return wallet;
    }

    public Boolean getLimitCancelLock() {
        return limitCancelLock;
    }

    public Double getLoanLimit() {
        return loanLimit;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public Integer getLoanId() {
        return loanId;
    }

    @Override
    public String toString() {
        return "LoanLimitResponse{" +
                "loanLimit=" + loanLimit +
                ", expiryDate='" + expiryDate + '\'' +
                ", loanId=" + loanId +
                "} " + super.toString();
    }

    public void setLoanLimit(Double loanLimit) {
        this.loanLimit = loanLimit;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public void setLimitCancelLock(Boolean limitCancelLock) {
        this.limitCancelLock = limitCancelLock;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }
}
