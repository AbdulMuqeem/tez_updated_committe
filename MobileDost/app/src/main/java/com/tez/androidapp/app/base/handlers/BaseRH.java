package com.tez.androidapp.app.base.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.models.network.dto.request.UserTokenRefreshRequest;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Log;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * All Response Handlers(RH) should extend this class. Use the suffix 'RH' for all Response Handlers.
 * <p>
 * Created  on 2/20/2017.
 */

public abstract class BaseRH<T> implements Observer<Result<T>> {

    private BaseCloudDataStore baseCloudDataStore;

    public BaseRH(BaseCloudDataStore baseCloudDataStore) {
        this.baseCloudDataStore = baseCloudDataStore;
    }

    @Override
    public void onNext(Result<T> value) {
        if (value.isError()) {
            onError(value.error());
        } else {
            int responseCode = value.response().code();
            if (value.response().isSuccessful()) {
                T response = value.response().body();
                if (response instanceof BaseResponse && ((BaseResponse) response).getStatusCode() == ResponseStatusCode.APP_VERSION_INCOMPATIBLE.getCode())
                    Utility.showAppOutdatedDialogFromNonActivityClass();
                else
                    onSuccess(value);
            } else {
                String message;
                if (responseCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    UserCloudDataStore userCloudDataStore = UserCloudDataStore.getInstance();
                    String requestURL = value.response().raw().request().url().toString();
                    if (requestURL.contains(UserTokenRefreshRequest.METHOD_NAME)) {
                        Utility.showSessionExpireDialogFromNonActivityClass();
                    } else {
                        userCloudDataStore.tokenRefresh(this);
                    }
                    return;
                } else if (responseCode == HttpURLConnection.HTTP_INTERNAL_ERROR) {
                    message = Constants.ERROR_MESSAGE;
                } else if (responseCode == HttpURLConnection.HTTP_UNAVAILABLE) {
                    try {
                        message = value.response().errorBody().string();
                        JSONObject jsonObject = new JSONObject(message);
                        message = jsonObject.optString("error_description", "Scheduled maintenance in progress, please try again later.");
                    } catch (Exception e) {
                        message = "Scheduled maintenance in progress, please try again later.";
                    }
                } else if (responseCode == HttpURLConnection.HTTP_GATEWAY_TIMEOUT) {
                    message = "Service unavailable, please call customer support";
                } else {
                    message = Constants.ERROR_MESSAGE;
                }

                if (message == null) {
                    try {
                        message = value.response().errorBody().string();
                    } catch (IOException e) {
                        message = value.response().message();
                        Log.e(e);
                    }
                    if (Utility.isEmpty(message))
                        message = Constants.ERROR_MESSAGE;
                }
                onFailure(value.response().code(), message);
            }
        }
    }

    @Override
    public void onError(Throwable e) {
        if (e instanceof SocketTimeoutException) {
            onFailure(ResponseStatusCode.CONNECTION_TIMEOUT.getCode(), "Connection timeout.");
        } else if (e instanceof SocketException || e instanceof UnknownHostException) {
            onFailure(ResponseStatusCode.USER_NOT_CONNECTED_TO_INTERNET.getCode(), "It seems you are not connected to the internet. Please check.");
        }  else if (e instanceof SSLPeerUnverifiedException || e instanceof SSLHandshakeException) {
            Utility.showAppOutdatedDialogFromNonActivityClass();
        } else {
            onFailure(ResponseStatusCode.DEFAULT.getCode(), Constants.ERROR_MESSAGE);
        }
    }

    protected void sendDefaultFailure() {
        onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onSubscribe(Disposable d) {
        baseCloudDataStore.addDisposable(d);
    }

    @Override
    public void onComplete() {
    }

    protected boolean isErrorFree(BaseResponse baseResponse) {
        return baseResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR;
    }

    protected abstract void onSuccess(Result<T> value);

    public abstract void onFailure(int errorCode, @Nullable String message);
}
