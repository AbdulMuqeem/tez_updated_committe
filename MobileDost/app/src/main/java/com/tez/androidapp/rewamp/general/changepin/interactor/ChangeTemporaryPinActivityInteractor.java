package com.tez.androidapp.rewamp.general.changepin.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.ChangeTemporaryPinCallback;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.general.changepin.presenter.IChangeTemporaryPinActivityInteractorOutput;

public class ChangeTemporaryPinActivityInteractor implements IChangeTemporaryPinActivityInteractor {

    private final IChangeTemporaryPinActivityInteractorOutput iChangeTemporaryPinActivityInteractorOutput;

    public ChangeTemporaryPinActivityInteractor(IChangeTemporaryPinActivityInteractorOutput iChangeTemporaryPinActivityInteractorOutput) {
        this.iChangeTemporaryPinActivityInteractorOutput = iChangeTemporaryPinActivityInteractorOutput;
    }

    @Override
    public void changeTemporaryPin(@NonNull String pin) {
        UserAuthCloudDataStore.getInstance().changeTemporaryPin(pin, new ChangeTemporaryPinCallback() {
            @Override
            public void onChangeTemporaryPinSuccess() {
                Utility.logoutUser();
                iChangeTemporaryPinActivityInteractorOutput.onChangeTemporaryPinSuccess();
            }

            @Override
            public void onChangeTemporaryPinFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    changeTemporaryPin(pin);
                else
                    iChangeTemporaryPinActivityInteractorOutput.onChangeTemporaryPinFailure(errorCode, message);
            }
        });
    }
}
