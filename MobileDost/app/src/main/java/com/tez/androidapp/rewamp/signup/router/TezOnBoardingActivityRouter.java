package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;
import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.TezOnboardingActivity;

/**
 * Created by VINOD KUMAR on 7/31/2019.
 */
public class TezOnBoardingActivityRouter extends BaseActivityRouter {


    public static TezOnBoardingActivityRouter createInstance() {
        return new TezOnBoardingActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return  new Intent(from, TezOnboardingActivity.class);
    }
}
