package com.tez.androidapp.app.vertical.bima.shared.vertical.push.reciever;

import android.content.Intent;

import com.tez.androidapp.app.base.push.reciever.BasePushNotificationReceiver;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;
import com.tez.androidapp.commons.utils.app.Utility;

import java.util.Map;

/**
 * Created by FARHAN DHANANI on 7/5/2018.
 */
public class InsuranceFireBasePushReceiver extends BasePushNotificationReceiver {

    public InsuranceFireBasePushReceiver(Map<String, String> notificationMap) {
        super(notificationMap);
    }

    @Override
    public void process() {
        String type = this.getNotificationMap().get(PushNotificationConstants.KEY_TYPE);

        if (type != null) {

            switch (type) {

                case PushNotificationConstants.INSURANCE_PURCHASE:
                    notificationTypeInsurancePurchase();
                    break;

                case PushNotificationConstants.CLAIM_LODGE:
                case PushNotificationConstants.CLAIM_REVISE_PORTAL:
                    notificationTypeClaim();
                    break;

                default:
                    sendNotification();
                    break;
            }
        }
    }

    private void notificationTypeInsurancePurchase() {
        String title = getNotificationMap().get(PushNotificationConstants.KEY_TITLE);
        String message = getNotificationMap().get(PushNotificationConstants.KEY_MESSAGE_TEXT);
        Intent intent = new Intent();
        intent.putExtra(PushNotificationConstants.KEY_TITLE, title);
        intent.putExtra(PushNotificationConstants.KEY_ROUTE_TO, PushNotificationConstants.BIMA_ACTIVITY_MY_POLICY_FRAGMENT);
        Utility.sendNotification(intent, message);
    }

    private void notificationTypeClaim() {
        String title = getNotificationMap().get(PushNotificationConstants.KEY_TITLE);
        String message = getNotificationMap().get(PushNotificationConstants.KEY_MESSAGE_TEXT);
        Intent intent = new Intent();
        intent.putExtra(PushNotificationConstants.KEY_TITLE, title);
        intent.putExtra(PushNotificationConstants.KEY_ROUTE_TO, PushNotificationConstants.BIMA_ACTIVITY_MY_CLAIMS_FRAGMENT);
        Utility.sendNotification(intent, message);
    }
}
