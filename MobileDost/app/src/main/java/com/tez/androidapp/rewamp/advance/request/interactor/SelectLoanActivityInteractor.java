package com.tez.androidapp.rewamp.advance.request.interactor;

import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.response.LoanLimitResponse;
import com.tez.androidapp.app.vertical.advance.loan.shared.limit.callbacks.LoanLimitCallback;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.LoanCloudDataStore;
import com.tez.androidapp.rewamp.advance.request.callback.LoanPricingDetailsCallback;
import com.tez.androidapp.rewamp.advance.request.callback.ProcessingFeesCallback;
import com.tez.androidapp.rewamp.advance.request.callback.TenureDetailsCallback;
import com.tez.androidapp.rewamp.advance.request.entity.LoanDetail;
import com.tez.androidapp.rewamp.advance.request.entity.PricingDetail;
import com.tez.androidapp.rewamp.advance.request.entity.ProcessingFee;
import com.tez.androidapp.rewamp.advance.request.entity.TenureDetail;
import com.tez.androidapp.rewamp.advance.request.presenter.ISelectLoanActivityInteractorOutput;

import java.util.List;

public class SelectLoanActivityInteractor implements ISelectLoanActivityInteractor {

    private final ISelectLoanActivityInteractorOutput iSelectLoanActivityInteractorOutput;

    public SelectLoanActivityInteractor(ISelectLoanActivityInteractorOutput iSelectLoanActivityInteractorOutput) {
        this.iSelectLoanActivityInteractorOutput = iSelectLoanActivityInteractorOutput;
    }

    @Override
    public void getLoanLimit() {
        LoanCloudDataStore.getInstance().getLoanLimit(new LoanLimitCallback() {
            @Override
            public void onLoanLimitSuccess(LoanLimitResponse loanLimitResponse) {
                iSelectLoanActivityInteractorOutput.onLoanLimitSuccess(loanLimitResponse);
            }

            @Override
            public void onLoanLimitFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getLoanLimit();
                else
                    iSelectLoanActivityInteractorOutput.onLoanLimitFailure(errorCode, message);
            }
        });
    }

    @Override
    public void getLoanDetails() {
        LoanCloudDataStore.getInstance().getProcessingFeesDetails(new ProcessingFeesCallback() {

            @Override
            public void onProcessingFeesSuccess(List<ProcessingFee> processingFees) {

                LoanCloudDataStore.getInstance().getLoanPricingDetails(new LoanPricingDetailsCallback() {

                    @Override
                    public void onSuccessLoanPricingDetails(List<PricingDetail> loanPricingDetails, double latePaymentCharges) {

                        LoanCloudDataStore.getInstance().getTenuresDetails(new TenureDetailsCallback() {

                            @Override
                            public void onGetTenuresDetailsSuccess(List<TenureDetail> tenureDetailList) {
                                LoanDetail loanDetail = new LoanDetail(tenureDetailList, loanPricingDetails, processingFees, latePaymentCharges);
                                iSelectLoanActivityInteractorOutput.onGetLoanDetailsSuccess(loanDetail);
                            }

                            @Override
                            public void onGetTenuresDetailsFailure(int statusCode, String description) {
                                onGetLoanDetailsFailure(statusCode, description);
                            }
                        });
                    }

                    @Override
                    public void onFailureLoanPricingDetails(int statusCode, String description) {
                        onGetLoanDetailsFailure(statusCode, description);
                    }
                });
            }

            @Override
            public void onProcessingFeesFailure(int errorCode, String message) {
                onGetLoanDetailsFailure(errorCode, message);
            }
        });
    }

    private void onGetLoanDetailsFailure(int errorCode, String message) {
        if (Utility.isUnauthorized(errorCode))
            getLoanDetails();
        else
            iSelectLoanActivityInteractorOutput.onGetLoanDetailsFailure(errorCode, message);
    }
}

