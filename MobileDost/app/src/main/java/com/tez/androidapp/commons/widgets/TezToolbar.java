package com.tez.androidapp.commons.widgets;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewGroup;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.TezDrawableHelper;
import com.tez.androidapp.rewamp.signup.router.ContactUsActivityRouter;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

/**
 * Created by VINOD KUMAR on 5/24/2019.
 */
public class TezToolbar extends TezConstraintLayout {

    public TezToolbar(@NonNull Context context) {
        super(context);
    }

    public TezToolbar(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public TezToolbar(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void setToolbarBackground(@DrawableRes int id) {
        TezImageView bg = findViewById(R.id.ivToolbarBackground);
        bg.setImageDrawable(TezDrawableHelper.getDrawable(getContext(), id));
    }

    public void setToolbarLogo(@DrawableRes int id) {
        TezImageView logo = findViewById(R.id.ivToolbarLogo);
        TezTextView title = findViewById(R.id.tvToolbarHeading);
        logo.setImageDrawable(TezDrawableHelper.getDrawable(getContext(), id));
        logo.setVisibility(VISIBLE);
        title.setVisibility(GONE);
    }

    public void setToolbarTitle(@StringRes int id) {
        setToolbarTitle(Utility.getStringFromResource(id));
    }

    public void setToolbarTitle(@NonNull String text) {
        TezTextView title = findViewById(R.id.tvToolbarHeading);
        TezImageView logo = findViewById(R.id.ivToolbarLogo);
        title.setText(text);
        title.setVisibility(VISIBLE);
        logo.setVisibility(GONE);
    }

    public void showCustomerCare(boolean isShowCustomerCare) {
        RippleEffectImageView customerCare = findViewById(R.id.layoutToolbarCustomerCare);
        customerCare.setVisibility(isShowCustomerCare ? VISIBLE : GONE);
    }

    public void showBack(boolean isShowBack) {
        RippleEffectImageView back = findViewById(R.id.layoutToolbarBack);
        back.setVisibility(isShowBack ? VISIBLE : GONE);
    }

    public void setOnBackPressListener(@Nullable DoubleTapSafeOnClickListener listener) {
        RippleEffectImageView back = findViewById(R.id.layoutToolbarBack);
        back.setOnClickListener(listener);
    }

    public void setOnCustomerCareListener(@Nullable DoubleTapSafeOnClickListener listener) {
        RippleEffectImageView customerCare = findViewById(R.id.layoutToolbarCustomerCare);
        customerCare.setDoubleTapSafeOnClickListener(listener);
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs) {

        if (attrs != null) {
            this.setBackgroundColor(Color.TRANSPARENT);
            inflate(context, R.layout.app_toolbar, this);
            TezImageView bg = findViewById(R.id.ivToolbarBackground);
            TezImageView logo = findViewById(R.id.ivToolbarLogo);
            TezTextView title = findViewById(R.id.tvToolbarHeading);
            RippleEffectImageView back = findViewById(R.id.layoutToolbarBack);
            RippleEffectImageView customerCare = findViewById(R.id.layoutToolbarCustomerCare);

            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TezToolbar);

            try {

                boolean isShowCustomerCare = typedArray.getBoolean(R.styleable.TezToolbar_show_customer_care, true);
                boolean isShowBack = typedArray.getBoolean(R.styleable.TezToolbar_show_back, true);

                customerCare.setVisibility(isShowCustomerCare ? VISIBLE : GONE);
                back.setVisibility(isShowBack ? VISIBLE : GONE);

                customerCare.setDoubleTapSafeOnClickListener(view -> startContactUsActivity(context));
                back.setOnClickListener(view -> setOnBackPress(context));

                Drawable drawableBg = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezToolbar_image);
                Drawable drawableLogo = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.TezToolbar_tez_logo);
                String stringTitle = typedArray.getString(R.styleable.TezToolbar_heading);
                int logoVisibility = typedArray.getInt(R.styleable.TezToolbar_logo_visibility, 1);
                int logoGravity = typedArray.getInt(R.styleable.TezToolbar_logo_gravity, 1);
                int logoHeight = typedArray.getDimensionPixelSize(R.styleable.TezToolbar_logo_height, 0);
                int logoWidth = typedArray.getDimensionPixelSize(R.styleable.TezToolbar_logo_width, 0);

                if (logoHeight != 0 && logoWidth != 0) {
                    ViewGroup.LayoutParams params = logo.getLayoutParams();
                    params.height = logoHeight;
                    params.width = logoWidth;
                    logo.setLayoutParams(params);
                }

                if (logoGravity == 0)
                    setGravityToTop(logo);

                if (logoVisibility == 0)
                    logo.setVisibility(GONE);

                if (drawableBg != null)
                    bg.setImageDrawable(drawableBg);
                else
                    bg.setImageDrawable(TezDrawableHelper.getDrawable(getContext(), R.drawable.bg_pattern_base));

                if (drawableLogo != null) {
                    logo.setImageDrawable(drawableLogo);
                    title.setVisibility(GONE);

                } else if (stringTitle != null) {
                    title.setText(stringTitle);
                    logo.setVisibility(GONE);
                    if (!isShowBack)
                        back.setVisibility(INVISIBLE);
                    if (!isShowCustomerCare)
                        customerCare.setVisibility(INVISIBLE);
                } else {
                    logo.setImageDrawable(TezDrawableHelper.getDrawable(getContext(), R.drawable.ic_logo_small));
                    title.setVisibility(GONE);
                }

            } finally {
                typedArray.recycle();
            }
        }
    }

    private void setOnBackPress(Context context) {
        if (context instanceof Activity)
            ((Activity) context).onBackPressed();
    }

    private void setGravityToTop(TezImageView ivLogo) {
        ConstraintSet set = new ConstraintSet();
        set.clone(this);
        set.clear(ivLogo.getId(), ConstraintSet.BOTTOM);
        set.applyTo(this);

        ConstraintLayout.LayoutParams newLayoutParams = (ConstraintLayout.LayoutParams) ivLogo.getLayoutParams();
        newLayoutParams.topMargin = dpToPx(getContext(), 30);
        newLayoutParams.bottomMargin = 0;
        ivLogo.setLayoutParams(newLayoutParams);
    }

    private void startContactUsActivity(@NonNull Context context) {
        if (context instanceof BaseActivity) {
            ContactUsActivityRouter.createInstance().setDependenciesAndRoute((BaseActivity) context);
        }
    }
}
