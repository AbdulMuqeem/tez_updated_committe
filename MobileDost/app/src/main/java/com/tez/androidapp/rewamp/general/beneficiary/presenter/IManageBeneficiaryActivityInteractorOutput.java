package com.tez.androidapp.rewamp.general.beneficiary.presenter;

import com.tez.androidapp.app.vertical.bima.policies.callbacks.GetInsurancePoliciesCallback;

/**
 * Created by VINOD KUMAR on 8/30/2019.
 */
public interface IManageBeneficiaryActivityInteractorOutput extends GetInsurancePoliciesCallback {
}
