package com.tez.androidapp.rewamp.general.faquestion.presenter;

import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.faq.models.network.FAQ;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.rewamp.general.faquestion.FAQInteractor.FAQInteractor;
import com.tez.androidapp.rewamp.general.faquestion.FAQInteractor.IFAQInteractor;
import com.tez.androidapp.rewamp.general.faquestion.IFAQActivityView;

import java.util.List;

public class FAQPresenter implements IFAQPresenter, IFAQInteractorOutput {

    private final IFAQActivityView ifaqActivityView;
    private final IFAQInteractor ifaqInteractor;


    public FAQPresenter(IFAQActivityView ifaqActivityView) {
        this.ifaqActivityView = ifaqActivityView;
        this.ifaqInteractor = new FAQInteractor(this);
    }

    @Override
    public void callForFAQS(String selectedLanguage) {
        this.ifaqInteractor.callForFAQs(selectedLanguage);
    }

    @Override
    public void onGetFAQsSuccess(final List<FAQ> faqs) {
        Optional.doWhen(faqs == null || faqs.isEmpty(),
                () -> this.ifaqActivityView.showInformativeMessage(R.string.tez_faq_error,
                        (d, v) -> ifaqActivityView.finishActivity()),
                () -> this.ifaqActivityView.renderRecyclerView(faqs));
    }

    @Override
    public void onGetFAQsError(int errorCode, String message) {
        this.ifaqActivityView.showError(errorCode, (d, v) -> ifaqActivityView.finishActivity());
    }
}
