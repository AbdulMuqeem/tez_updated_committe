package com.tez.androidapp.commons.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.models.network.Dashboard;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.commons.models.network.MobileUserInfo;

/**
 * Created  on 9/9/2017.
 */

public class UserDashboardResponse extends BaseResponse {

    private Dashboard dashboard;

    public Dashboard getDashboardDetails() {
        return dashboard;
    }

    public LoanDetails getLoanDetails() {
        return dashboard.getLoanDetails();
    }

    public MobileUserInfo getMobileUserInfo() {
        return dashboard.getMobileUserInfo();
    }

    public boolean containsLoanDetails() {
        return dashboard.getLoanDetails() != null;
    }

    public boolean containsMobileUserInfo() {
        return dashboard.getMobileUserInfo() != null;
    }

    public boolean containNotificationCount() { return  dashboard.getUnreadNotificationsCount() != null; }

    public Integer getNotificationCount() { return dashboard.getUnreadNotificationsCount(); }
}
