package com.tez.androidapp.rewamp.advance.request.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.request.CancelLoanLimitRequest;

public interface ICancelLimitActivityInteractor extends IBaseInteractor {

    void cancelLoanLimit(@NonNull CancelLoanLimitRequest request);
}
