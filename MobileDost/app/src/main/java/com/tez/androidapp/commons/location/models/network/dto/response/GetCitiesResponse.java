package com.tez.androidapp.commons.location.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.location.models.network.City;

import java.util.List;

/**
 * Created  on 2/9/2017.
 */

public class GetCitiesResponse extends BaseResponse {

    private List<City> cities;

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    @Override
    public String toString() {
        return "GetCitiesResponse{" +
                "cities=" + cities +
                "} " + super.toString();
    }
}
