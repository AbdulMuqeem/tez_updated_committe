package com.tez.androidapp.app.base.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.app.base.observer.SessionTimeOutObserver;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;
import com.tez.androidapp.rewamp.signup.router.NewIntroActivityRouter;
import com.tez.androidapp.rewamp.signup.router.WelcomeBackActivityRouter;

import net.tez.fragment.util.optional.Optional;

public class PushNotificationHandlerActivity extends BaseActivity {

    @Override
    public void onAppStart(boolean inactiveTimeExceeded) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.navigateNotification();
    }

    private void navigateNotification() {
        Optional.ifPresent(MDPreferenceManager.getUser(), user -> {
            Optional.doWhen(SessionTimeOutObserver.getInstance().isAppActive(),
                    () -> MobileDostApplication.getInstance().sendOrderedBroadcast(new Intent(Constants.BROAD_CAST_RECIEVER_ON_VALIDATE_PIN_ACTIVITY), null,
                            new BroadcastReceiver() {
                                @Override
                                public void onReceive(Context context, Intent intent) {
                                    Optional.doWhen(getResultCode() == 1,
                                            PushNotificationHandlerActivity.this::startWelcomeBackActivity,
                                            () -> {
                                                String routeTo = getIntent().getStringExtra(PushNotificationConstants.KEY_ROUTE_TO);
                                                onPushRoute(routeTo == null ? "" : routeTo);
                                                finish();
                                            });
                                }
                            }, null, RESULT_OK, null, null),

                    this::startWelcomeBackActivity);
        }, this::startNewIntroActivity);
    }

    private void startNewIntroActivity() {
        NewIntroActivityRouter.createInstance().setDependenciesAndRoute(this);
        finish();
    }

    protected void startWelcomeBackActivity() {
        WelcomeBackActivityRouter.createInstance().setDependenciesAndRouteForPush(this,
                getIntent().getStringExtra(PushNotificationConstants.KEY_ROUTE_TO));
        finish();
    }

    @Override
    protected String getScreenName() {
        return "PushNotificationHandlerActivity";
    }
}
