package com.tez.androidapp.app.vertical.bima.claims.handlers

import com.jakewharton.retrofit2.adapter.rxjava2.Result
import com.tez.androidapp.app.base.handlers.BaseRH
import com.tez.androidapp.app.vertical.bima.claims.callback.ClaimsListCallBack
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.ClaimListResponse
import com.tez.androidapp.repository.network.store.BaseCloudDataStore

/**
 * Created by Vinod Kumar on 10/14/2020.
 */
class ClaimListRH(baseCloudDataStore: BaseCloudDataStore, private val callback: ClaimsListCallBack)
    : BaseRH<ClaimListResponse>(baseCloudDataStore) {

    override fun onSuccess(value: Result<ClaimListResponse>) {
        val response = value.response().body()
        response?.let {
            if (isErrorFree(it))
                callback.onMyClaimsListSuccess(it)
            else
                onFailure(it.statusCode, it.errorDescription)

        } ?: sendDefaultFailure()

    }

    override fun onFailure(errorCode: Int, message: String?) {
        callback.onMyClaimsListFailure(errorCode, message)
    }
}