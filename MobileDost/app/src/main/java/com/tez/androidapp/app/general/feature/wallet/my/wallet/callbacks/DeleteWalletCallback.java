package com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created by Rehman Murad Ali on 12/28/2017.
 */

public interface DeleteWalletCallback {

    void onDeleteWalletSuccess(BaseResponse response);

    void onDeleteWalletFailure(int errorCode, String message);
}
