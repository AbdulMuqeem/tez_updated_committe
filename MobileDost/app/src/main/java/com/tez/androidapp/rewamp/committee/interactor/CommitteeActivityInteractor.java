package com.tez.androidapp.rewamp.committee.interactor;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.CommitteeAuthCloudDataStore;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;
import com.tez.androidapp.rewamp.committee.listener.CommitteeCheckInvitesLIstener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeLoginLIstener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeMetadataLIstener;
import com.tez.androidapp.rewamp.committee.presenter.ICommitteeActivityInteractorOutput;
import com.tez.androidapp.rewamp.committee.request.CommitteeLoginRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCheckInvitesResponse;
import com.tez.androidapp.rewamp.committee.response.CommitteeMetaDataResponse;
import com.tez.androidapp.rewamp.committee.response.CommittteeLoginResponse;

import java.util.List;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class CommitteeActivityInteractor implements ICommitteeActivityInteractor {

    private final ICommitteeActivityInteractorOutput iCommitteeActivityInteractorOutput;

    public CommitteeActivityInteractor(ICommitteeActivityInteractorOutput committeeActivityInteractorOutput) {
        this.iCommitteeActivityInteractorOutput = committeeActivityInteractorOutput;
    }

    @Override
    public void getCommitteeMetada() {
        CommitteeAuthCloudDataStore.getInstance().getCommitteeMetadata(new CommitteeMetadataLIstener() {
            @Override
            public void onCommitteeMetadataSuccess(CommitteeMetaDataResponse committeeMetaDataResponse) {
                iCommitteeActivityInteractorOutput.onCommitteeMetadataSuccess(committeeMetaDataResponse);
            }

            @Override
            public void onCommitteeMetadataFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getCommitteeMetada();
                else
                    iCommitteeActivityInteractorOutput.onCommitteeMetadataFailure(errorCode, message);
            }
        });
    }

    @Override
    public void checkInvites(String mobileNumber) {
        CommitteeAuthCloudDataStore.getInstance().checkInvites(mobileNumber, new CommitteeCheckInvitesLIstener() {
            @Override
            public void onCommitteeCheckInvitesSuccess(List<CommitteeCheckInvitesResponse> committeeCheckInvitesResponse) {
                iCommitteeActivityInteractorOutput.onCommitteeCheckInvitesSuccess(committeeCheckInvitesResponse);

            }

            @Override
            public void onCommitteeCheckInvitesFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    checkInvites(mobileNumber);
                else
                    iCommitteeActivityInteractorOutput.onCommitteeCheckInvitesFailure(errorCode, message);
            }
        });
    }

    public void loginCall(CommitteeLoginRequest committeeLoginRequest) {
        UserCloudDataStore.getInstance().loginCall(committeeLoginRequest, new CommitteeLoginLIstener() {
            @Override
            public void onCommitteeLoginSuccess(CommittteeLoginResponse committteeLoginResponse) {
                iCommitteeActivityInteractorOutput.onCommitteeLoginSuccess(committteeLoginResponse);
            }

            @Override
            public void onCommitteeLoginFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    loginCall(committeeLoginRequest);
                else
                    iCommitteeActivityInteractorOutput.onCommitteeLoginFailure(errorCode, message);
            }
        });
    }
}
