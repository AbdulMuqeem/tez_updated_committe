package com.tez.androidapp.rewamp.profile.edit.view;

import com.tez.androidapp.app.base.ui.IBaseView;

public interface IEditProfileVerificationActivityView extends IBaseView {
    void createSinchVerification(String mobileNumber);

    String getMobileNumber();

    String getEmail();

    String getCurrentAddress();

    int getSelectedCityId();
}
