package com.tez.androidapp.rewamp.bima.products.policy.interactor;

public interface IPurchasePolicyActivityInteractor {

    void getInsuranceCoverage(int productId);
}
