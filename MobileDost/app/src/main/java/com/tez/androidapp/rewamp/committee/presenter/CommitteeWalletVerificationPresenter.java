package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.interactor.CommitteeWalletVerificationActivityInteractor;
import com.tez.androidapp.rewamp.committee.request.CommitteeInstallmentPayRequestCommittee;
import com.tez.androidapp.rewamp.committee.request.CommitteePaymentInitiateRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeInstallmentPayResponse;
import com.tez.androidapp.rewamp.committee.response.PaymentInitiateResponse;
import com.tez.androidapp.rewamp.committee.view.ICommitteeWalletVerificationActivityView;

/**
 * Created by Ahmad Izaz on 28-Nov-20
 **/
public class CommitteeWalletVerificationPresenter implements ICommitteeWalletVerificationActivityPresenter, ICommitteeWalletVerificationActivityInteractorOutput {


    private final ICommitteeWalletVerificationActivityView mIJoinCommitteeVerificationActivityView;
    private final CommitteeWalletVerificationActivityInteractor committeeWalletVerificationActivityInteractor;


    public CommitteeWalletVerificationPresenter(ICommitteeWalletVerificationActivityView iCommitteeWalletVerificationActivityView) {
        this.mIJoinCommitteeVerificationActivityView = iCommitteeWalletVerificationActivityView;
        committeeWalletVerificationActivityInteractor = new CommitteeWalletVerificationActivityInteractor(this);
    }


    @Override
    public void onPaymentInitiateSuccess(PaymentInitiateResponse paymentInitiateResponse) {
        mIJoinCommitteeVerificationActivityView.hideLoader();
        mIJoinCommitteeVerificationActivityView.onInitiatePayment(paymentInitiateResponse);
    }

    @Override
    public void onPaymentInitiateFailure(int errorCode, String message) {
        mIJoinCommitteeVerificationActivityView.showError(errorCode,
                (dialog, which) -> mIJoinCommitteeVerificationActivityView.finishActivity());
    }

    @Override
    public void initiatePayment(CommitteePaymentInitiateRequest committeePaymentInitiateRequest) {
        mIJoinCommitteeVerificationActivityView.showLoader();
        committeeWalletVerificationActivityInteractor.initiatePayment(committeePaymentInitiateRequest);
    }

    @Override
    public void payInstallment(CommitteeInstallmentPayRequestCommittee committeeInstallmentPayRequestCommittee) {
        mIJoinCommitteeVerificationActivityView.showLoader();
        committeeWalletVerificationActivityInteractor.payInstallment(committeeInstallmentPayRequestCommittee);
    }

    @Override
    public void onInstallmentPaySuccess(CommitteeInstallmentPayResponse committeeInstallmentPayResponse) {
        mIJoinCommitteeVerificationActivityView.hideLoader();
        mIJoinCommitteeVerificationActivityView.onInstallmentPay(committeeInstallmentPayResponse);
    }

    @Override
    public void onInstallmentPayFailure(int errorCode, String message) {
        mIJoinCommitteeVerificationActivityView.showError(errorCode,
                (dialog, which) -> mIJoinCommitteeVerificationActivityView.finishActivity());
    }
}
