package com.tez.androidapp.rewamp.advance.request.view;

import androidx.annotation.StringRes;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.rewamp.advance.request.request.LoanApplyRequest;

import java.util.List;

public interface IAdvanceTermsAndConditionActivityView extends IBaseView {

    void routeToWaitForDisbursementActivity();

    void routeToCompleteYourProfileActivity();

    void routeToDashboard();

    void routeToLoanRequiredStepsActivity(List<String> steps);

    void routeToAddBeneficiaryActivity();

    LoanApplyRequest getLoanApplyRequest();

    void setBtMainButtonEnabled(boolean enabled);

    String getFormattedErrorMessage(@StringRes int message);
}
