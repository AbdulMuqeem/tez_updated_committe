package com.tez.androidapp.rewamp.general.suspend.account.view;

import com.tez.androidapp.app.base.ui.IBaseView;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public interface ISuspendAccountOTPActivityView extends IBaseView {

    void takeFeedbackFromUser();

    void setPinViewOtpClearText();

    void startTimer();

    void setTvResendCodeEnabled(boolean enabled);

    void setPinViewOtpEnabled(boolean enabled);
}
