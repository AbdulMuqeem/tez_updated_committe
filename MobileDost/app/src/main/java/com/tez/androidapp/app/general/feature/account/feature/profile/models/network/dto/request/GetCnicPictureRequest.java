package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;
import com.tez.androidapp.commons.managers.MDPreferenceManager;


public class GetCnicPictureRequest extends BaseRequest {
    private static final String METHOD_NAME = "v1/user/attachment/";
    private static final String TEMINATOR = "";

    public static final String METHOD_NAME_FRONT_NIC = MDPreferenceManager.getBaseURL()
            + METHOD_NAME
            + Params.FRONT
            + TEMINATOR;

    public static final String METHOD_NAME_BACK_NIC = MDPreferenceManager.getBaseURL()
            + METHOD_NAME
            + Params.BACK
            + TEMINATOR;

    public static final String METHOD_NAME_SELFIE_NIC = MDPreferenceManager.getBaseURL()
            + METHOD_NAME
            + Params.SELFIE
            + TEMINATOR;

    public static final class Params {

        private static final String FRONT = "FRONT_CNIC";
        private static final String BACK = "BACK_CNIC";
        private static final String SELFIE = "SELFIE";

        private Params() {
        }
    }
}
