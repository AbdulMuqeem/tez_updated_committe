package com.tez.androidapp.rewamp.advance.limit.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;

public interface ILimitAssignedActivityInteractor extends IBaseInteractor {

    void getLoanLimit();
}
