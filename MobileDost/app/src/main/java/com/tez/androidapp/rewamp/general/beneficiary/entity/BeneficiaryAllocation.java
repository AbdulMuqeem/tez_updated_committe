package com.tez.androidapp.rewamp.general.beneficiary.entity;

import java.io.Serializable;

public class BeneficiaryAllocation implements Serializable {

    private int id;
    private Double amountAllocation;

    public BeneficiaryAllocation(int id, Double amountAllocation) {
        this.id = id;
        this.amountAllocation = amountAllocation;
    }
}
