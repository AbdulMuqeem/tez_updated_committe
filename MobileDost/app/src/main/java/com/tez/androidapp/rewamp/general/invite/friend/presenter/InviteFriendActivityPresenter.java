package com.tez.androidapp.rewamp.general.invite.friend.presenter;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.general.invite.friend.interactor.IInviteFriendActivityInteractor;
import com.tez.androidapp.rewamp.general.invite.friend.interactor.InviteFriendActivityInteractor;
import com.tez.androidapp.rewamp.general.invite.friend.view.IInviteFriendActivityView;
import com.tez.androidapp.services.TezCrashlytics;
import com.tez.androidapp.services.TezDynamicLinks;

public class InviteFriendActivityPresenter implements
        IInviteFriendActivityPresenter, IInviteFriendActivityInteractorOutput {

    private final IInviteFriendActivityView iInviteFriendActivityView;
    private final IInviteFriendActivityInteractor iInviteFriendActivityInteractor;

    public InviteFriendActivityPresenter(IInviteFriendActivityView iInviteFriendActivityView) {
        iInviteFriendActivityInteractor = new InviteFriendActivityInteractor(this);
        this.iInviteFriendActivityView = iInviteFriendActivityView;
    }

    @Override
    public void getRefferalCode() {
        this.iInviteFriendActivityInteractor.getReferralCode();
    }

    @Override
    public void onGetReferralCodeSuccess(String referralCode) {
        this.iInviteFriendActivityView.setOnClickListnerToBtOnClickListner(
                view -> onClickAppCompatButtonInviteFriend(referralCode));
        this.iInviteFriendActivityView.setVisibillityForTezLinearLayoutShimmer(View.GONE);
        this.iInviteFriendActivityView.setVisibillityForBtContinue(View.VISIBLE);
        this.iInviteFriendActivityView.setVisibillityForIvLogo(View.VISIBLE);
        this.iInviteFriendActivityView.setVisibillityForTvDescription(View.VISIBLE);
        this.iInviteFriendActivityView.setVisibillityForTvHeading(View.VISIBLE);
    }

    @Override
    public void onGetReferralCodeFailure(int errorCode, String message) {
        this.iInviteFriendActivityView.showError(errorCode,
                (d, v) -> this.iInviteFriendActivityView.finishActivity());
    }

    @Override
    public void onClickAppCompatButtonInviteFriend(String refCode) {
        if (!Utility.isEmpty(refCode)) {
            share(refCode);
        } else {
            this.iInviteFriendActivityView.showInformativeMessage(R.string.invalid_ref,
                    (d, v) -> iInviteFriendActivityView.finishActivity());
        }
    }

    private void share(String refCode) {
        this.iInviteFriendActivityView.showTezLoader();
        createDynamicUri(createShareUri(refCode));
    }

    private Uri createShareUri(String refCode) {

        Uri.Builder builder = new Uri.Builder();
        builder.scheme(Utility.getStringFromResource(R.string.config_scheme))
                .authority(Utility.getStringFromResource(R.string.chrome_url))
                .appendQueryParameter(Constants.REF_CODE, refCode);
        return builder.build();
    }

    private void createDynamicUri(Uri uri) {
        TezDynamicLinks.getInstance()
                .createDynamicLink()
                .setDomainUriPrefix(Utility.getStringFromResource(R.string.config_host))
                .setLink(uri)
                .buildShortDynamicLink()
                .addOnSuccessListener(shortDynamicLink -> {
                    Uri shortLink = shortDynamicLink.getShortLink();
                    String msg = Utility.getStringFromResource(R.string.string_hello)
                            + " "
                            + Utility.getStringFromResource(R.string.string_app_invite_desc)
                            + " Invitation link: "
                            + " "
                            + shortLink;
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
                    sendIntent.setType("text/plain");
                    this.iInviteFriendActivityView.startActivityFromIntent(sendIntent);
                    this.iInviteFriendActivityView.setBtContinueEnabled(true);
                    this.iInviteFriendActivityView.dismissTezLoader();
                }).addOnFailureListener(exception -> {
            TezCrashlytics.getInstance().recordException(exception);
            iInviteFriendActivityView.dismissTezLoader();
            this.iInviteFriendActivityView.showInformativeMessage(R.string.retry,
                    (d, v) -> this.iInviteFriendActivityView.finishActivity());
        });
    }
}
