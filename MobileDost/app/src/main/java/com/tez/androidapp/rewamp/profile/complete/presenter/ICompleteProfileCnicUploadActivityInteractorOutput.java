package com.tez.androidapp.rewamp.profile.complete.presenter;

import androidx.annotation.Nullable;

import com.tez.androidapp.commons.utils.cnic.detection.model.RetumDataModel;

public interface ICompleteProfileCnicUploadActivityInteractorOutput {

    void onUpdateCnicPictureSuccess(int requestCode, @Nullable String filePath, @Nullable RetumDataModel retumDataModel);


    void onUpdateCnicPictureFailure(int requestCode, int errorCode, String message);
}
