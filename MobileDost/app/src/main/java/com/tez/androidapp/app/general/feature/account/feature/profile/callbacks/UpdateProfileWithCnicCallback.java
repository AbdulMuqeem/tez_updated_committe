package com.tez.androidapp.app.general.feature.account.feature.profile.callbacks;

public interface UpdateProfileWithCnicCallback {
    void onSuccessUpdateProfileWithCnicCallback();
    void onFailureUpdateProfileWithCnicCallback(int errorCode, String message);
}
