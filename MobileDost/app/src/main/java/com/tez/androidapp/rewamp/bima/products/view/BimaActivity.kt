package com.tez.androidapp.rewamp.bima.products.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.tez.androidapp.R
import com.tez.androidapp.app.base.ui.activities.BaseActivity
import com.tez.androidapp.app.base.ui.fragments.BaseFragment
import com.tez.androidapp.commons.utils.app.Utility
import com.tez.androidapp.commons.widgets.TezTextView
import com.tez.androidapp.rewamp.bima.products.router.BimaActivityRouter
import kotlinx.android.synthetic.main.activity_bima.*

class BimaActivity : BaseActivity(), BimaCallback {

    private val fragmentsList: List<BaseFragment> = listOf(
            ProductsFragment.newInstance(),
            PoliciesFragment.newInstance(),
            ClaimsFragment.newInstance(),
    )

    private lateinit var textViewsList: List<TezTextView>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bima)
        init()
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        viewModelStore.clear()
        onClickPolicies()
    }

    private fun init() {
        initTextViews()
        initListeners()
        initInitialFragment(BimaActivityRouter.getDependencies(intent).initialFragment)
    }

    private fun initInitialFragment(initialFragment: Int) {
        when (initialFragment) {
            BimaActivityRouter.POLICY -> onClickPolicies()
            BimaActivityRouter.CLAIM -> onClickClaims()
            else -> onClickProducts()
        }
    }


    private fun initTextViews() {
        textViewsList = listOf(
                tvPackages,
                tvMyPolicy,
                tvMyClaims,
        )
    }

    private fun initListeners() {
        tvPackages.setDoubleTapSafeOnClickListener { onClickProducts() }
        tvMyPolicy.setDoubleTapSafeOnClickListener { onClickPolicies() }
        tvMyClaims.setDoubleTapSafeOnClickListener { onClickClaims() }
    }

    private fun onClickProducts() {
        replaceFragment(textViewsList[0], fragmentsList[0])
    }

    private fun onClickPolicies() {
        replaceFragment(textViewsList[1], fragmentsList[1])
    }

    private fun onClickClaims() {
        replaceFragment(textViewsList[2], fragmentsList[2])
    }

    private fun replaceFragment(textView: TezTextView, fragment: BaseFragment) {
        textViewsList.forEach {
            it.setTextColor(Utility.getColorFromResource(R.color.disabledColor))
            it.setTextViewDrawableColor(R.color.disabledColor)
        }
        textView.setTextColor(Utility.getColorFromResource(R.color.textViewTextColorBlack))
        textView.setTextViewDrawableColor(R.color.textViewTextColorGreen)
        replaceFragment(fragment)
    }

    private fun replaceFragment(baseFragment: BaseFragment) =
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragmentContainer, baseFragment)
                    .commit()

    override fun showProductsFragment() {
        onClickProducts()
    }

    override fun showPoliciesFragment() {
        onClickPolicies()
    }

    override fun setIllustrationVisibility(visibility: Int) {
        ivIllustration.visibility = visibility
        toolbar.setToolbarTitle(if (visibility == View.GONE) resources.getString(R.string.tez_bima) else "")
    }

    override fun setIllustrationAndVisibility(illustration: Int, visibility: Int) {
        ivIllustration.setImageResource(illustration)
        setIllustrationVisibility(visibility)
    }

    override fun getScreenName() = "BimaActivity"
}