package com.tez.androidapp.rewamp.reactivate.account.presenter;

public interface IAccountReactivateActivityPresenter {
    void verifyOtp(String otp);

    void resendOtp();
}
