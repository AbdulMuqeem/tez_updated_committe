package com.tez.androidapp.repository.network.store;

import com.tez.androidapp.rewamp.committee.listener.CommitteeCheckInvitesLIstener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeContributeListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeCreationLIstener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeDeclineListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeFilterListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeGroupChatListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeGroupChatSendMessageListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeInstallmentPayLIstener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeInviteListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeJoinListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeLeaveListener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeMetadataLIstener;
import com.tez.androidapp.rewamp.committee.listener.CommitteeWalletVerificationLIstener;
import com.tez.androidapp.rewamp.committee.listener.JoinCommitteeInvitationListener;
import com.tez.androidapp.rewamp.committee.listener.JoinCommitteeVerificationLIstener;
import com.tez.androidapp.rewamp.committee.listener.MyCommitteeListener;
import com.tez.androidapp.rewamp.committee.request.CommitteeCreateRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeInstallmentPayRequestCommittee;
import com.tez.androidapp.rewamp.committee.request.CommitteeInviteRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteePaymentInitiateRequest;
import com.tez.androidapp.rewamp.committee.request.CommitteeSendMessageRequest;
import com.tez.androidapp.rewamp.committee.request.JoinCommitteeRequest;
import com.tez.androidapp.rewamp.committee.request.JoinCommitteeVerificationRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCheckInvitesRH;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreationRH;
import com.tez.androidapp.rewamp.committee.response.CommitteeDeclineRH;
import com.tez.androidapp.rewamp.committee.response.CommitteeFilterRH;
import com.tez.androidapp.rewamp.committee.response.CommitteeGroupChatRH;
import com.tez.androidapp.rewamp.committee.response.CommitteeInstallmentPayRH;
import com.tez.androidapp.rewamp.committee.response.CommitteeInviteRH;
import com.tez.androidapp.rewamp.committee.response.CommitteeJoinRH;
import com.tez.androidapp.rewamp.committee.response.CommitteeLeaveRH;
import com.tez.androidapp.rewamp.committee.response.CommitteeMetadataRH;
import com.tez.androidapp.rewamp.committee.response.CommitteeSendMessageRH;
import com.tez.androidapp.rewamp.committee.response.CommitteeWalletsRH;
import com.tez.androidapp.rewamp.committee.response.GetInvitedCommitteePackageRH;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeVerificationRH;
import com.tez.androidapp.rewamp.committee.response.MyCommitteeRH;
import com.tez.androidapp.rewamp.committee.response.PaymentInitiateRH;
import com.tez.androidapp.rewamp.committee.shared.api.client.CommitteeAPI;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Ahmad Izaz on 05-Nov-20
 **/
public class CommitteeAuthCloudDataStore extends BaseAuthCloudDataStore {


    private static CommitteeAuthCloudDataStore committeeAuthCloudDataStore;
    private CommitteeAPI committeeAPI;

    private CommitteeAuthCloudDataStore() {
        super();
        committeeAPI = tezAPIBuilder.build().create(CommitteeAPI.class);
    }

    public static void clear() {
        committeeAuthCloudDataStore = null;
    }

    public static CommitteeAuthCloudDataStore getInstance() {
        if (committeeAuthCloudDataStore == null)
            committeeAuthCloudDataStore = new CommitteeAuthCloudDataStore();
        return committeeAuthCloudDataStore;
    }


    public void getCommitteeMetadata(CommitteeMetadataLIstener committeeMetadataLIstener) {
        committeeAPI.getCommitteeMetadata().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new CommitteeMetadataRH(this, committeeMetadataLIstener));
    }

    public void getInvitedPackage(String mobileNumber, JoinCommitteeInvitationListener joinCommitteeInvitationListener) {
        committeeAPI.getInvitedPackage(mobileNumber).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new GetInvitedCommitteePackageRH(this, joinCommitteeInvitationListener));
    }

    public void checkInvites(String mobileNumber, CommitteeCheckInvitesLIstener committeeCheckInvitesLIstener) {
        committeeAPI.checkInvites(mobileNumber).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new CommitteeCheckInvitesRH(this, committeeCheckInvitesLIstener));
    }

    public void getInvite(List<CommitteeInviteRequest> committeeInviteRequest, CommitteeInviteListener committeeInviteListener) {
        committeeAPI.getInvite(committeeInviteRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new CommitteeInviteRH(this, committeeInviteListener));
    }

    public void getWallets(CommitteeContributeListener committeeContributeListener) {
        committeeAPI.getWallets().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new CommitteeWalletsRH(this, committeeContributeListener));
    }

    public void getFilter(String committeeId, CommitteeFilterListener committeeFilterListener) {
        committeeAPI.getFilter(committeeId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new CommitteeFilterRH(this, committeeFilterListener));
    }

    public void getMessages(String committeeId, CommitteeGroupChatListener committeeGroupChatListener) {
        committeeAPI.getMessage(committeeId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new CommitteeGroupChatRH(this, committeeGroupChatListener));
    }

    public void sendMessage(CommitteeSendMessageRequest committeeSendMessageRequest, CommitteeGroupChatSendMessageListener committeeGroupChatSendMessageListener) {
        committeeAPI.sendMessage(committeeSendMessageRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new CommitteeSendMessageRH(this, committeeGroupChatSendMessageListener));
    }

    public void getInvite(CommitteeInviteRequest committeeInviteRequest, CommitteeInviteListener committeeInviteListener) {
        committeeAPI.getInvite(committeeInviteRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new CommitteeInviteRH(this, committeeInviteListener));
    }

    public void leaveCommittee(String committeeId, CommitteeLeaveListener committeeLeaveListener) {
        committeeAPI.leaveCommittee(committeeId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new CommitteeLeaveRH(this, committeeLeaveListener));
    }

    public void declineCommittee(String committeeId, CommitteeDeclineListener committeeDeclineListener) {
        committeeAPI.declineCommittee(committeeId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new CommitteeDeclineRH(this, committeeDeclineListener));
    }

    public void joinCommittee(JoinCommitteeRequest joinCommitteeRequest, CommitteeJoinListener committeeJoinListener) {
        committeeAPI.joinCommittee(joinCommitteeRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new CommitteeJoinRH(this, committeeJoinListener));
    }

    public void createCommittee(CommitteeCreateRequest committeeCreateRequest, CommitteeCreationLIstener committeeCreationLIstener) {
        committeeAPI.createCommittee(committeeCreateRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new CommitteeCreationRH(this, committeeCreationLIstener));
    }


    public void verifyOtp(JoinCommitteeVerificationRequest joinCommitteeVerificationRequest, JoinCommitteeVerificationLIstener joinCommitteeVerificationLIstener) {
        committeeAPI.verifyOtp(joinCommitteeVerificationRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new JoinCommitteeVerificationRH(this, joinCommitteeVerificationLIstener));
    }

    public void initiatePayment(CommitteePaymentInitiateRequest committeePaymentInitiateRequest, CommitteeWalletVerificationLIstener committeeWalletVerificationLIstener) {
        committeeAPI.initiatePayment(committeePaymentInitiateRequest).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new PaymentInitiateRH(this, committeeWalletVerificationLIstener));
    }

    public void payInstallment(CommitteeInstallmentPayRequestCommittee committeeInstallmentPayRequestCommittee, CommitteeInstallmentPayLIstener committeeInstallmentPayLIstener) {
        committeeAPI.payInstallment(committeeInstallmentPayRequestCommittee).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new CommitteeInstallmentPayRH(this, committeeInstallmentPayLIstener));
    }

    public void getMyCommittee(MyCommitteeListener committeeListener) {
        committeeAPI.getMyCommittee().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new MyCommitteeRH(this, committeeListener));
    }
}
