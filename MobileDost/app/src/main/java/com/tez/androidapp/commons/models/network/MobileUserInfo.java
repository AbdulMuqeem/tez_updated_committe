package com.tez.androidapp.commons.models.network;

import com.tez.androidapp.commons.models.network.dto.response.UserLoginResponse;

import java.io.Serializable;
import java.util.List;

/**
 * Created  on 9/12/2017.
 */

public class MobileUserInfo implements Serializable {

    private String userStatus;
    private List<String> requiredSteps;
    private List<String> optionalSteps;
    private Boolean reactivated;
    private Boolean updateProfileRequired;

    public Boolean getUpdateProfileRequired() {
        return updateProfileRequired;
    }

    public void setUpdateProfileRequired(Boolean updateProfileRequired) {
        this.updateProfileRequired = updateProfileRequired;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public List<String> getRequiredSteps() {
        return requiredSteps;
    }

    public void setRequiredSteps(List<String> requiredSteps) {
        this.requiredSteps = requiredSteps;
    }

    public Boolean getReactivated() {
        return reactivated;
    }

    public void setReactivated(Boolean reactivated) {
        this.reactivated = reactivated;
    }

    public boolean isCNICUploaded() {
        return requiredSteps == null || !requiredSteps.contains(UserLoginResponse.CNIC);
    }

    public boolean isMobileAccountAdded() {
        return optionalSteps == null || !optionalSteps.contains(UserLoginResponse.MOBILE_ACCOUNT);
    }

    public boolean noStepsLeft() {
        return requiredSteps == null || requiredSteps.isEmpty();
    }
}
