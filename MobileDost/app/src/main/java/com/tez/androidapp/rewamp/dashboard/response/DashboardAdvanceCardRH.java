package com.tez.androidapp.rewamp.dashboard.response;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.callbacks.DashboardAdvanceCardCallback;

public class DashboardAdvanceCardRH extends BaseRH<DashboardAdvanceCardResponse> {

    private final DashboardAdvanceCardCallback cardCallback;

    public DashboardAdvanceCardRH(BaseCloudDataStore baseCloudDataStore, DashboardAdvanceCardCallback callback) {
        super(baseCloudDataStore);
        this.cardCallback = callback;
    }

    @Override
    protected void onSuccess(Result<DashboardAdvanceCardResponse> value) {
        DashboardAdvanceCardResponse response = value.response().body();
        if (response != null && response.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            this.cardCallback.onDashboardAdvanceCardSuccess(response);
        else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.cardCallback.onDashboardAdvanceCardFailure(errorCode, message);
    }
}
