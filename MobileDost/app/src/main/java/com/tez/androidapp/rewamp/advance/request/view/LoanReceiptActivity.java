package com.tez.androidapp.rewamp.advance.request.view;

import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.advance.request.presenter.ILoanReceiptActivityPresenter;
import com.tez.androidapp.rewamp.advance.request.presenter.LoanReceiptActivityPresenter;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.OnClick;
import net.tez.viewbinder.library.core.ViewBinder;

public class LoanReceiptActivity extends BaseActivity implements ILoanReceiptActivityView {

    private final ILoanReceiptActivityPresenter iLoanReceiptActivityPresenter;

    @BindView(R.id.tvWalletName)
    private TezTextView tvWalletName;

    @BindView(R.id.tvWalletNo)
    private TezTextView tvWalletNo;

    @BindView(R.id.tvAmountValue)
    private TezTextView tvAmountValue;

    @BindView(R.id.tvTransactionIdValue)
    private TezTextView tvTransactionIdValue;

    @BindView(R.id.tvLifeOfLoanValue)
    private TezTextView tvLifeOfLoanValue;

    @BindView(R.id.tvAmountToRepayValue)
    private TezTextView tvAmountToRepayValue;

    @BindView(R.id.tvDateValue)
    private TezTextView tvDateValue;

    @BindView(R.id.clContent)
    private TezConstraintLayout clContent;

    @BindView(R.id.tvRepayNote)
    private TezTextView tvRepayNote;

    public LoanReceiptActivity() {
        iLoanReceiptActivityPresenter = new LoanReceiptActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_receipt);
        ViewBinder.bind(this);
        iLoanReceiptActivityPresenter.generateDisbursementReceipt();
    }

    @Override
    public void initValues(@NonNull LoanDetails loanDetails) {

        tvWalletName.setText(Utility.getWalletName(loanDetails.getWallet().getServiceProviderId()));
        tvWalletNo.setText(loanDetails.getWallet().getMobileAccountNumberFormatted());
        tvAmountValue.setText(getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabel(loanDetails.getAdvanceAmount())));
        tvTransactionIdValue.setText(loanDetails.getTransactionId());
        tvLifeOfLoanValue.setText(loanDetails.getTenureTitle());
        tvAmountToRepayValue.setText(getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabel(loanDetails.getAmountToReturn())));
        tvDateValue.setText(loanDetails.getDueDateDDMMMM());
        String formattedDueDate = loanDetails.getDueDateFormatted();
        String repayNoteDate = getString(R.string.repay_advance_note, formattedDueDate);
        int startIndex = repayNoteDate.indexOf(formattedDueDate);
        int endIndex = startIndex + formattedDueDate.length();
        SpannableString spannableString = new SpannableString(repayNoteDate);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.textViewTextColorGreen)), startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvRepayNote.setText(spannableString);
    }

    @Override
    public void setClContentVisibility(int visibility) {
        this.clContent.setVisibility(visibility);
    }

    @Override
    public void showTezLoader() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissTezLoader() {
        TezLinearLayout shimmer = findViewById(R.id.shimmer);
        shimmer.setVisibility(View.GONE);
        ViewGroup viewGroup = findViewById(R.id.rootViewGroup);
        viewGroup.removeView(shimmer);
    }

    @OnClick(R.id.btBack)
    @Override
    public void routeToDashboard() {
        DashboardActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this);
        finish();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected String getScreenName() {
        return "LoanReceiptActivity";
    }
}