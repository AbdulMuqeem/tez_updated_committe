package com.tez.androidapp.rewamp.general.profile.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Answer;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Option;
import com.tez.androidapp.commons.calendar.TezCalendar;
import com.tez.androidapp.commons.locale.LocaleHelper;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.validators.annotations.TextInputLayoutIfVisibleThenNotEmpty;
import com.tez.androidapp.commons.validators.annotations.TextInputLayoutNotEmpty;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezEditTextView;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezSpinner;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;
import com.tez.androidapp.rewamp.advance.limit.router.CheckYourLimitActivityRouter;
import com.tez.androidapp.rewamp.general.profile.presenter.BasicProfileActivityPresenter;
import com.tez.androidapp.rewamp.general.profile.presenter.IBasicProfileActivityPresenter;
import com.tez.androidapp.rewamp.profile.trust.router.ProfileTrustActivityRouter;
import com.tez.androidapp.rewamp.util.DialogUtil;

import net.tez.logger.library.utils.TextUtil;
import net.tez.validator.library.annotations.Order;
import com.tez.androidapp.commons.validators.annotations.MaxDateDeviationFromCurrentDate;
import net.tez.validator.library.core.FieldValidator;
import net.tez.validator.library.core.FilterChain;
import net.tez.validator.library.core.ValidationError;
import net.tez.validator.library.core.ValidationListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class BasicProfileActivity extends BaseActivity
        implements ValidationListener, IBasicProfileActivityView {

    @TextInputLayoutNotEmpty(value = {1, R.string.string_input_valid_current_name})
    @Order(1)
    @BindView(R.id.tilName)
    private TezTextInputLayout tilName;

    @TextInputLayoutNotEmpty(value = {1, R.string.string_input_your_gender})
    @Order(2)
    @BindView(R.id.tilGender)
    private TezTextInputLayout tilGender;

    @TextInputLayoutNotEmpty(value = {1, R.string.string_input_your_dob})
    @MaxDateDeviationFromCurrentDate(dateFormat = Constants.UI_DATE_FORMAT,
            dayDeviation = 0,
            monthDeviation = 0,
            yearDeviation = -18,
            value = {2, R.string.string_error_user_must_be_18_years_old})
    @Order(3)
    @BindView(R.id.tilDOB)
    private TezTextInputLayout tilDOB;

    @TextInputLayoutNotEmpty(value = {1, R.string.string_input_your_job})
    @Order(4)
    @BindView(R.id.tilJob)
    private TezTextInputLayout tilJob;

    @TextInputLayoutIfVisibleThenNotEmpty(value = {1, R.string.string_input_your_job})
    @Order(5)
    @BindView(R.id.tilSubJob)
    private TezTextInputLayout tilSubJob;

    @BindView(R.id.etName)
    private TezEditTextView etName;

    @BindView(R.id.sGender)
    private TezSpinner sGender;

    @BindView(R.id.etDOB)
    private TezEditTextView etDOB;

    @BindView(R.id.sJob)
    private TezSpinner sJob;

    @BindView(R.id.sSubJob)
    private TezSpinner sSubJob;

    @BindView(R.id.btContinue)
    private TezButton btContinue;

    @BindView(R.id.shimmer)
    private TezLinearLayout shimmer;

    @BindView(R.id.mainCard)
    private TezConstraintLayout mainCard;

    private CompositeDisposable allDisposables;

    private final IBasicProfileActivityPresenter iBasicProfileActivityPresenter;

    public BasicProfileActivity() {
        this.iBasicProfileActivityPresenter = new BasicProfileActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basic_activity_profile);
        ViewBinder.bind(this);
        init();
        initAdapters();
    }

    @Override
    public void setTextToEtName(String text) {
        this.etName.setText(text);
    }

    @Override
    public void setTextToSGender(String text) {
        String[] genders = getResources().getStringArray(R.array.genders);
        String gender = TextUtil.equals(text, "M") ? genders[0] :
                TextUtil.equals(text, "F") ? genders[1] :
                        TextUtil.equals(text, "T") ? genders[2] : "";
        this.sGender.setText(gender);
    }

    @Override
    public void setTextToEtDOB(String text) {
        this.etDOB.setText(Utility.getFormattedDate(text, Constants.BACKEND_DATE_FORMAT, Constants.UI_DATE_FORMAT));
    }

    @Override
    public void setTextToJob(String text) {
        this.sJob.setText(text);
    }

    @Override
    public void setTextToSubJob(String text) {
        this.sSubJob.setText(text);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.allDisposables = new CompositeDisposable();
        listenChangesOnViews();
    }

    private void init() {
        this.mainCard.setVisibility(View.GONE);
        this.shimmer.setVisibility(View.VISIBLE);
        ArrayAdapter<Answer> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, new ArrayList<>());
        this.sJob.setAdapter(arrayAdapter);
        this.sSubJob.setAdapter(arrayAdapter);
        this.iBasicProfileActivityPresenter.getOccupationOptions(LocaleHelper.getSelectedLanguageForBackend(this));
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.allDisposables.dispose();
    }

    private void listenChangesOnViews() {
        this.allDisposables.add(RxTextView.textChanges(this.etName).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));

        this.allDisposables.add(RxTextView.textChanges(this.etDOB).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));

        this.allDisposables.add(RxTextView.textChanges(this.sGender).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));

        this.allDisposables.add(RxTextView.textChanges(this.sJob).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));

        this.allDisposables.add(RxTextView.textChanges(this.sSubJob).toFlowable(BackpressureStrategy.LATEST)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(charSequence -> FieldValidator.validate(this, this)));
    }

    @Override
    public void setBtContinueEnabled(boolean enabled) {
        this.btContinue.setEnabled(enabled);
    }

    private void initAdapters() {
        this.sGender.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.genders)));
        this.etDOB.setOnClickListener(view -> TezCalendar.showCalendarForDob(this, etDOB));
        this.btContinue.setDoubleTapSafeOnClickListener(v -> FieldValidator.validate(BasicProfileActivity.this, new ValidationListener() {
            @Override
            public void validateSuccess() {
                setBtContinueEnabled(false);
                iBasicProfileActivityPresenter.updateUserBasicProfile();
            }

            @Override
            public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
                Utility.setErrorOnTezViews(validationError.getView(), validationError.getMessage());
                btContinue.setButtonInactive();
                filterChain.doFilter();
            }
        }));
    }

    @Override
    public String getTextFromEtName() {
        return this.etName.getValueText();
    }

    @Override
    public void setVisibillityForShimmer(int visibillity) {
        this.shimmer.setVisibility(visibillity);
    }

    @Override
    public void setAdapterToJobSpinner(List<Option> jobs, Option selectedOption) {
        ArrayAdapter<Answer> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, selectedOption.getAnswers());
        this.sJob.setAdapter(arrayAdapter);
        this.setHintToJob(selectedOption.getText());
        this.sJob.setOnItemClickListener((parent, view, position, id) -> {
            Answer answer = arrayAdapter.getItem(position);
            this.iBasicProfileActivityPresenter.onJobItemSelected(jobs, selectedOption, answer);
        });
    }

    @Override
    public void setHintToJob(String text){
        this.tilJob.setHint(text);
    }

    @Override
    public void setHintToSubJob(String text){
        this.tilSubJob.setHint(text);
    }

    @Override
    public void setAdapterToSubJobSpinner(List<Answer> data, AdapterView.OnItemClickListener listener) {
        this.sSubJob.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, data));
        this.sSubJob.setOnItemClickListener(listener);
    }

    @Override
    public void setVisibillityForSubJobSpinner(int visibillity) {
        this.tilSubJob.setVisibility(visibillity);
    }

    @Override
    public void setVisibillityForMainCard(int visibillity) {
        this.mainCard.setVisibility(visibillity);
    }

    @Override
    public String getTextFromEtDob() {
        return Utility.getFormattedDate(this.etDOB.getValueText(),
                Constants.UI_DATE_FORMAT,
                Constants.BACKEND_DATE_FORMAT);
    }

    @Override
    public void unlockLimit() {
        DialogUtil.showActionableDialog(this,
                R.drawable.ic_img_product_unlock,
                R.string.congratulations_tez_products_have_been_unlocked,
                R.string.recommend_complete_profile,
                R.string.check_your_loan_limit,
                R.string.complete_profile,
                (dialog, which) -> {

                    if (which == DialogInterface.BUTTON_POSITIVE)
                        CheckYourLimitActivityRouter.createInstance().setDependenciesAndRoute(this);

                    else if (which == DialogInterface.BUTTON_NEGATIVE)
                        ProfileTrustActivityRouter.createInstance().setDependenciesAndRoute(this);

                    finish();
                });
    }

    @Override
    public String getTextFromSGender() {
        return this.sGender.getText().charAt(0) + "";
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void validateSuccess() {
        this.btContinue.setButtonNormal();
    }

    @Override
    public void validatePassed(@NonNull View v) {
        Utility.setErrorOnTezViews(v, null);
    }

    @Override
    public void validateFailed(@NonNull ValidationError validationError, @NonNull FilterChain filterChain) {
        this.btContinue.setButtonInactive();
        filterChain.doFilter();
    }

    @Override
    public void setListenerToSGender(List<Option> jobs){
        this.sGender.setOnItemClickListener((parent, view, position, id) -> {
            setAdapterToJobSpinner(jobs, jobs.get(position));
            iBasicProfileActivityPresenter.categoryDoesNotExist();
            iBasicProfileActivityPresenter.subCategoryDoesNotExist();
        });
    }

    @Override
    protected String getScreenName() {
        return "BasicProfileActivity";
    }
}
