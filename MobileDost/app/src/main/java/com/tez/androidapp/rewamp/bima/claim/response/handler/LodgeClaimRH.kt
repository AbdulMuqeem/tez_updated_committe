package com.tez.androidapp.rewamp.bima.claim.response.handler

import com.jakewharton.retrofit2.adapter.rxjava2.Result
import com.tez.androidapp.app.base.handlers.BaseRH
import com.tez.androidapp.repository.network.store.BaseCloudDataStore
import com.tez.androidapp.rewamp.bima.claim.callback.LodgeClaimCallback
import com.tez.androidapp.rewamp.bima.claim.response.LodgeClaimResponse

class LodgeClaimRH(baseCloudDataStore: BaseCloudDataStore?,
                   private val lodgeClaimCallback: LodgeClaimCallback)
    : BaseRH<LodgeClaimResponse>(baseCloudDataStore) {
    override fun onSuccess(value: Result<LodgeClaimResponse>) {
        val response = value.response().body()
        response?.let {
            if (isErrorFree(it))
                lodgeClaimCallback.onSuccessLodgeClaim(it)
            else
                onFailure(it.statusCode, it.errorDescription)

        } ?: sendDefaultFailure()
    }

    override fun onFailure(errorCode: Int, message: String?) {
        lodgeClaimCallback.onFailureLodgeClaim(errorCode, message)
    }
}