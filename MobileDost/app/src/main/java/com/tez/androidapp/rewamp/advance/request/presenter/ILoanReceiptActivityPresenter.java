package com.tez.androidapp.rewamp.advance.request.presenter;

public interface ILoanReceiptActivityPresenter {

    void generateDisbursementReceipt();
}
