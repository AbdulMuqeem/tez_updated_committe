package com.tez.androidapp.rewamp.bima.claim.health.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.annotation.LayoutRes
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import com.tez.androidapp.R
import com.tez.androidapp.commons.calendar.TezCalendar
import com.tez.androidapp.commons.utils.textwatchers.TezTextWatcher
import com.tez.androidapp.commons.widgets.TezEditTextView
import com.tez.androidapp.commons.widgets.TezTextInputLayout
import com.tez.androidapp.rewamp.bima.claim.common.location.router.LocationPickerActivityRouter
import com.tez.androidapp.rewamp.bima.claim.health.base.BaseClaimStepFragment
import com.tez.androidapp.rewamp.bima.claim.health.viewmodel.DetailsViewModel
import kotlinx.android.synthetic.main.fragment_details.*
import kotlinx.android.synthetic.main.fragment_details.view.*

/**
 * Created by Vinod Kumar on 4/20/2020
 */
class DetailsFragment : BaseClaimStepFragment() {

    private val args: DetailsFragmentArgs by navArgs()
    private val viewModel: DetailsViewModel by activityViewModels()


    override fun initView(baseView: View, savedInstanceState: Bundle?) {

        claimSharedViewModel.claimQuestionListLiveData.value = viewModel.questionListLiveData.value

        viewModel.questionListLiveData.observe(viewLifecycleOwner) {
            val questions = viewModel.questionListLiveData.value
            etDateOfAdmission.setText(questions?.dateOfAdmission?.date)
            etDateOfDischarge.setText(questions?.dateOfDischarge?.date)
            etNameOfHospital.setText(questions?.locationOfHospital?.hospitalName)
            etLocationOfHospital.setText(questions?.locationOfHospital?.hospitalNameMap)
            etNearestLandmark.setText(questions?.locationOfHospital?.nearestLandMark)
            tilNearestLandmark.visibility = if (questions?.locationOfHospital?.hospitalLocation == null) View.GONE else View.VISIBLE
        }

        baseView.etDateOfAdmission.setDoubleTapSafeOnClickListener {
            TezCalendar.showCalendar(requireContext(), etDateOfAdmission) {
                viewModel.questionListLiveData.value?.dateOfAdmission?.date = it
            }
        }

        baseView.etDateOfDischarge.setDoubleTapSafeOnClickListener {
            TezCalendar.showCalendar(requireContext(), etDateOfDischarge) {
                viewModel.questionListLiveData.value?.dateOfDischarge?.date = it
            }
        }

        baseView.etLocationOfHospital.setDoubleTapSafeOnClickListener {
            LocationPickerActivityRouter.createInstance().setDependenciesAndRoute(this)
        }

        baseView.etNearestLandmark.run {
            addTextChangedListener(object : TezTextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    super.afterTextChanged(s)
                    viewModel.questionListLiveData.value?.locationOfHospital?.nearestLandMark = s.toString()
                }
            })
            imeOptions = EditorInfo.IME_ACTION_DONE
            setRawInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES)
        }

        baseView.etNameOfHospital.run {
            addTextChangedListener(object : TezTextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    super.afterTextChanged(s)
                    viewModel.questionListLiveData.value?.locationOfHospital?.hospitalName = s.toString()
                }
            })
            imeOptions = EditorInfo.IME_ACTION_DONE
            setRawInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES)
        }

        setNavigationOnBtContinue(true)
        btContinue.setButtonInactive()
        addTextChangeListener(baseView.tilDateOfAdmission, baseView.etDateOfAdmission)
        addTextChangeListener(baseView.tilDateOfDischarge, baseView.etDateOfDischarge)
        addTextChangeListener(baseView.tilNameOfHospital, baseView.etNameOfHospital)
    }

    private fun addTextChangeListener(til: TezTextInputLayout, et: TezEditTextView) {
        et.addTextChangedListener(object : TezTextWatcher {

            override fun afterTextChanged(s: Editable) {
                til.error = null

                when {
                    validate() != null -> btContinue.setButtonInactive()
                    else -> btContinue.setButtonNormal()
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK
                && requestCode == LocationPickerActivityRouter.REQUEST_CODE_PICK_LOCATION
                && data != null) {
            val result = LocationPickerActivityRouter.getResult(data)
            val question = viewModel.questionListLiveData.value?.locationOfHospital
            question?.hospitalNameMap = result.address
            question?.hospitalLocation = "${result.latLng.latitude},${result.latLng.longitude}"
            etLocationOfHospital.setText(result.address)
            tilNearestLandmark.visibility = View.VISIBLE
            tilNearestLandmark.requestFocus()
        }
    }

    override fun onClickContinue() =
            validate()?.let {
                when (it.first) {
                    1 -> tilDateOfAdmission.setError(it.second)
                    2 -> tilDateOfDischarge.setError(it.second)
                    3 -> tilNameOfHospital.setError(it.second)
                }
            } ?: super.onClickContinue()

    fun validate(): Pair<Int, Int>? = viewModel.validate(args.activationDate,
            args.expiryDate,
            etDateOfAdmission.valueText,
            etDateOfDischarge.valueText,
            etNameOfHospital.valueText)

    @get:LayoutRes
    override val layoutResId: Int
        get() = R.layout.fragment_details

    override val stepNumber: Int
        get() = args.stepNumber
}