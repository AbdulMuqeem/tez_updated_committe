package com.tez.androidapp.rewamp.bima.claim.request

import com.tez.androidapp.app.base.request.BaseRequest

object InitiateReviseClaimRequest : BaseRequest() {
    const val CLAIM_ID = "claimId"
    const val METHOD_NAME = "v1/claim/revise/{$CLAIM_ID}"
}