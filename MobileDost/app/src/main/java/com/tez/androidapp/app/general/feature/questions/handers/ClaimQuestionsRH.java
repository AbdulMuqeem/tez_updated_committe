package com.tez.androidapp.app.general.feature.questions.handers;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.questions.callbacks.ClaimQuestionsCallback;
import com.tez.androidapp.app.general.feature.questions.models.network.models.dto.response.ClaimQuestionsResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created  on 2/17/2017.
 */

public class ClaimQuestionsRH extends BaseRH<ClaimQuestionsResponse> {

    private ClaimQuestionsCallback claimQuestionsCallback;

    public ClaimQuestionsRH(BaseCloudDataStore baseCloudDataStore, @NonNull ClaimQuestionsCallback
            claimQuestionsCallback) {
        super(baseCloudDataStore);
        this.claimQuestionsCallback = claimQuestionsCallback;
    }

    @Override
    protected void onSuccess(Result<ClaimQuestionsResponse> value) {
        ClaimQuestionsResponse response = value.response().body();
        if (response != null) {

            if (isErrorFree(response))
                claimQuestionsCallback.onQuestionsSuccess(response.getQuestions());
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());

        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        claimQuestionsCallback.onQuestionsFailure(errorCode, message);
    }
}
