package com.tez.androidapp.app.base.ui.activities;

import android.view.View;
import android.widget.ExpandableListView;

import androidx.core.view.GravityCompat;
import androidx.core.view.ViewCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.tez.androidapp.R;
import com.tez.androidapp.commons.locale.LocaleHelper;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.User;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.dashboard.NavigationExpandableListAdapter;

import net.tez.viewbinder.library.core.ViewBinder;

/**
 * Created by VINOD KUMAR on 4/9/2019.
 */
public abstract class NavigationActivity extends BaseActivity {

    private static final int CONTENT_INDEX = 0;
    private DrawerLayout drawer;

    private TezImageView ivUserImage;
    private TezTextView tvUserName;


    @Override
    public void setContentView(int layoutResID) {
        View baseView = inflateLayout(R.layout.activity_navigation_layout);
        View childView = inflateLayout(layoutResID);

        drawer = baseView.findViewById(R.id.drawer_layout);
        ViewCompat.setLayoutDirection(drawer, ViewCompat.LAYOUT_DIRECTION_LTR);
        drawer.addView(childView, CONTENT_INDEX);
        setContentView(baseView);
        ViewBinder.bind(this);
        int layoutDirection = LocaleHelper.getLayoutDirection(this);
        ViewCompat.setLayoutDirection(childView, layoutDirection);
        ViewCompat.setLayoutDirection(findViewById(R.id.navigationList), layoutDirection);

        this.init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.initName();
    }

    @Override
    protected void onStop() {
        super.onStop();
        closeDrawer();
    }

    public void closeDrawer() {
        drawer.closeDrawer(GravityCompat.START);
    }

    protected void openDrawer() {
        drawer.openDrawer(GravityCompat.START);
    }

    protected boolean isDrawerClosed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return false;
        }
        return true;
    }

    private void init() {
        this.initDataMembers();
        this.initializeNavigationMenu();
        this.setDrawerListener();
    }

    private void initDataMembers() {
        NavigationView navView = findViewById(R.id.nav_view);
        View headerLayout = navView.getHeaderView(0);
        ivUserImage = headerLayout.findViewById(R.id.ivUserImage);
        tvUserName = headerLayout.findViewById(R.id.tvUserName);
    }

    private void initName() {
        User user = MDPreferenceManager.getUser();

        if (user != null) {

            Utility.loadProfilePicture(ivUserImage);

            if (user.getFullName() != null)
                tvUserName.setText(user.getFullName());
            else
                tvUserName.setText(getString(R.string.welcome));
        }
    }

    private void initializeNavigationMenu() {
        NavigationExpandableListAdapter adapter = new NavigationExpandableListAdapter(this);
        ExpandableListView expandableList = findViewById(R.id.navigationList);
        expandableList.setAdapter(adapter);
    }

    private void setDrawerListener() {
        View content = drawer.getChildAt(CONTENT_INDEX);
        this.drawer.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float translation = drawerView.getWidth() * slideOffset;
                content.setTranslationX(translation);
            }
        });
        drawer.setScrimColor(getResources().getColor(R.color.scrimColor));
    }

    @Override
    public void onBackPressed() {
        if (isDrawerClosed())
            super.onBackPressed();
    }
}
