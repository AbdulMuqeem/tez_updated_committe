package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response;

import android.graphics.Path;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Answer;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Option;

import net.tez.logger.library.utils.TextUtil;

import java.util.List;

public class QuestionOptionsResponse extends BaseResponse {
    private List<Option> data;

    public List<Option> getData() {
        return data;
    }

    public void setData(List<Option> data) {
        this.data = data;
    }
}
