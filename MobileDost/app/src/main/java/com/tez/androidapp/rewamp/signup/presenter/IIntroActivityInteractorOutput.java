package com.tez.androidapp.rewamp.signup.presenter;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

public interface IIntroActivityInteractorOutput {

    void onUserInfoSuccess(String mobileNumber, String socialId, String principalName,
                           int socialType, boolean isRegistered);

    void onUserInfoFailure(int errorCode, String message);
}
