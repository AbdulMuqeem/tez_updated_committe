package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by FARHAN DHANANI on 6/29/2018.
 */
public class ClaimedDocumentDetail implements Serializable {

    private Integer claimDocumentId;
    private String status;
    private InsuranceDocumentDto insuranceDocumentDto;
    private InsuranceClaimDocumentCommentDto insuranceClaimDocumentCommentDto;
    private List<InsuranceClaimDocumentFileDto> insuranceClaimDocumentFileDtoList;

    public List<InsuranceClaimDocumentFileDto> getInsuranceClaimDocumentFileDtoList() {
        return insuranceClaimDocumentFileDtoList;
    }

    public void setInsuranceClaimDocumentFileDtoList(List<InsuranceClaimDocumentFileDto> insuranceClaimDocumentFileDtoList) {
        this.insuranceClaimDocumentFileDtoList = insuranceClaimDocumentFileDtoList;
    }

    public List<String> getDocumentFilesPaths() {
        List<String> filePaths = new ArrayList<>(getInsuranceClaimDocumentFileDtoList().size());
        for (InsuranceClaimDocumentFileDto fileDto : getInsuranceClaimDocumentFileDtoList())
            filePaths.add(fileDto.getFilePath());
        return filePaths;
    }

    public InsuranceClaimDocumentCommentDto getCommentDto() {
        return insuranceClaimDocumentCommentDto;
    }

    public Integer getClaimDocumentId() {
        return claimDocumentId;
    }

    public void setClaimDocumentId(Integer claimDocumentId) {
        this.claimDocumentId = claimDocumentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public InsuranceDocumentDto getInsuranceDocumentDto() {
        return insuranceDocumentDto;
    }

    public void setInsuranceDocumentDto(InsuranceDocumentDto insuranceDocumentDto) {
        this.insuranceDocumentDto = insuranceDocumentDto;
    }

}
