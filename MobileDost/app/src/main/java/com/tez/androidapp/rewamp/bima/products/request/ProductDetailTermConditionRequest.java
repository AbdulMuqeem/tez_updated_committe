package com.tez.androidapp.rewamp.bima.products.request;

import com.tez.androidapp.app.base.request.BaseRequest;

public class ProductDetailTermConditionRequest extends BaseRequest {

    public static final String METHOD_NAME = "/api/v1/insurance/{" + ProductTermConditionsRequest.Params.PRODUCT_ID + "}/termcondition";

    public static final class Params {

        public static final String PRODUCT_ID = "productId";
    }
}
