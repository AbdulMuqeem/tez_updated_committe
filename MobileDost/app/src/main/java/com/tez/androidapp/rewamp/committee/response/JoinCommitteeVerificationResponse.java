package com.tez.androidapp.rewamp.committee.response;

import com.tez.androidapp.app.base.response.BaseResponse;

import java.io.Serializable;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public class JoinCommitteeVerificationResponse extends BaseResponse implements Serializable {

    private String message;
    private boolean response;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }
}



