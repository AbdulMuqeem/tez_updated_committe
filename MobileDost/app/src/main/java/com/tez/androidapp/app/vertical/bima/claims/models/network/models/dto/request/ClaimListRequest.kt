package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request

/**
 * Created by Vinod Kumar on 10/14/2020.
 */
object ClaimListRequest {
    const val METHOD_NAME = "v1/claim/all"
}