package com.tez.androidapp.rewamp.advance.request.presenter;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.request.CancelLoanLimitRequest;
import com.tez.androidapp.rewamp.advance.request.interactor.CancelLimitActivityInteractor;
import com.tez.androidapp.rewamp.advance.request.interactor.ICancelLimitActivityInteractor;
import com.tez.androidapp.rewamp.advance.request.view.ICancelLimitActivityView;

public class CancelLimitActivityPresenter implements ICancelLimitActivityPresenter, ICancelLimitActivityInteractorOutput {

    private final ICancelLimitActivityView iCancelLimitActivityView;
    private final ICancelLimitActivityInteractor iCancelLimitActivityInteractor;

    public CancelLimitActivityPresenter(ICancelLimitActivityView iCancelLimitActivityView) {
        this.iCancelLimitActivityView = iCancelLimitActivityView;
        iCancelLimitActivityInteractor = new CancelLimitActivityInteractor(this);
    }

    @Override
    public void cancelLoanLimit(int loanId, int cancellationReasonId) {
        iCancelLimitActivityView.showTezLoader();
        CancelLoanLimitRequest request = new CancelLoanLimitRequest(loanId, cancellationReasonId);
        iCancelLimitActivityInteractor.cancelLoanLimit(request);
    }

    @Override
    public void onCancelLoanLimitSuccess(BaseResponse response) {
        iCancelLimitActivityView.routeToDashboardActivity();
    }

    @Override
    public void onCancelLoanLimitFailure(int errorCode, String message) {
        iCancelLimitActivityView.dismissTezLoader();
        iCancelLimitActivityView.showError(errorCode);
    }
}
