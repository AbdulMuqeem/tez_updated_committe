package com.tez.androidapp.repository.network.models.response;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created by Rehman Murad Ali on 4/19/2018.
 */
public class VerifyActiveDeviceResponse extends BaseResponse {
    private Boolean verifyDevice;
    private String mobileNumber;
    private Boolean isDeviceKeyPresent;

    public VerifyActiveDeviceResponse(Boolean v, String m, Boolean i){
        verifyDevice =v;
        mobileNumber =m;
        isDeviceKeyPresent =i;
    }

    public Boolean getVerifyDevice() {
        return verifyDevice;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public Boolean getIsDeviceKeyPresent() {
        return isDeviceKeyPresent != null && isDeviceKeyPresent;
    }
}
