package com.tez.androidapp.rewamp.general.beneficiary.interactor;

import com.tez.androidapp.app.vertical.bima.policies.callbacks.GetInsurancePoliciesCallback;
import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.response.GetInsurancePoliciesResponse;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.general.beneficiary.presenter.IManageBeneficiaryActivityInteractorOutput;

/**
 * Created by VINOD KUMAR on 8/30/2019.
 */
public class ManageBeneficiaryActivityInteractor implements IManageBeneficiaryActivityInteractor {

    private final IManageBeneficiaryActivityInteractorOutput iManageBeneficiaryActivityInteractorOutput;

    public ManageBeneficiaryActivityInteractor(IManageBeneficiaryActivityInteractorOutput iManageBeneficiaryActivityInteractorOutput) {
        this.iManageBeneficiaryActivityInteractorOutput = iManageBeneficiaryActivityInteractorOutput;
    }

    @Override
    public void getManageBeneficiaryPolicies() {
        UserAuthCloudDataStore.getInstance().getManageBeneficiaryPolicies(new GetInsurancePoliciesCallback() {
            @Override
            public void onGetInsurancePoliciesSuccess(GetInsurancePoliciesResponse getInsurancePoliciesResponse) {
                iManageBeneficiaryActivityInteractorOutput.onGetInsurancePoliciesSuccess(getInsurancePoliciesResponse);
            }

            @Override
            public void onGetInsurancePoliciesFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    getManageBeneficiaryPolicies();
                else
                    iManageBeneficiaryActivityInteractorOutput.onGetInsurancePoliciesFailure(errorCode, message);

            }
        });
    }
}
