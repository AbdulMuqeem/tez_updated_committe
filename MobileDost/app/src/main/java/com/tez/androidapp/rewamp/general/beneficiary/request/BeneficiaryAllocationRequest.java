package com.tez.androidapp.rewamp.general.beneficiary.request;

import androidx.annotation.Nullable;

import com.tez.androidapp.app.base.request.BaseRequest;
import com.tez.androidapp.rewamp.general.beneficiary.entity.BeneficiaryAllocation;

import java.util.List;

public class BeneficiaryAllocationRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/beneficiary/allocate";

    private List<BeneficiaryAllocation> beneficiaryList;

    @Nullable
    private Integer lastBeneficiaryAllocated;

    public BeneficiaryAllocationRequest(List<BeneficiaryAllocation> beneficiaryList, @Nullable Integer lastBeneficiaryAllocated) {
        this.beneficiaryList = beneficiaryList;
        this.lastBeneficiaryAllocated = lastBeneficiaryAllocated;
    }
}
