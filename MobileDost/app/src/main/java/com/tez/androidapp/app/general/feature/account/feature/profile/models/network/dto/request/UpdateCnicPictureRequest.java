package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;
import com.tez.androidapp.commons.managers.MDPreferenceManager;

public class UpdateCnicPictureRequest extends BaseRequest {
    public static final String METHOD_NAME = "v1/user/attachment/cnic/upload";

    public static String FRONT = "frontCnicCopy";
    public static String BACK  = "backCnicCopy";
    public static String SELFIE = "selfie";
    public static String AUTODETECTED = "isAutoDetectFailed";
}
