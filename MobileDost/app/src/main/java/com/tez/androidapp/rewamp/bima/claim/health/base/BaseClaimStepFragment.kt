package com.tez.androidapp.rewamp.bima.claim.health.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.activityViewModels
import com.tez.androidapp.R
import com.tez.androidapp.app.base.ui.fragments.BaseFragment
import com.tez.androidapp.commons.widgets.TezButton
import com.tez.androidapp.rewamp.bima.claim.common.viewmodel.ClaimSharedViewModel
import com.tez.androidapp.rewamp.bima.claim.health.listener.ClaimStepListener
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener
import net.tez.viewbinder.library.core.BindView
import net.tez.viewbinder.library.core.ViewBinder

/**
 * Created by Vinod Kumar on 4/17/2020
 */
abstract class BaseClaimStepFragment : BaseFragment() {

    protected lateinit var listener: ClaimStepListener

    @BindView(R.id.btContinue)
    protected lateinit var btContinue: TezButton

    private var baseView: View? = null

    protected val claimSharedViewModel: ClaimSharedViewModel by activityViewModels()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as ClaimStepListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (baseView == null) {
            baseView = inflater.inflate(layoutResId, container, false)
            ViewBinder.bind(this, baseView!!)
            initView(baseView!!, savedInstanceState)
        }
        return baseView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listener.setStep(stepNumber)
    }

    protected fun setNavigationOnBtContinue(isActive: Boolean) {
        btContinue.setButtonBackgroundStyle(
                if (isActive) TezButton.ButtonStyle.NORMAL
                else TezButton.ButtonStyle.INACTIVE)
        btContinue.setDoubleTapSafeOnClickListener(
                if (isActive) DoubleTapSafeOnClickListener { onClickContinue() }
                else null)
    }

    protected open fun onClickContinue() = listener.navigate(stepNumber)

    protected abstract fun initView(baseView: View, savedInstanceState: Bundle?)

    protected abstract val stepNumber: Int

    @get:LayoutRes
    protected abstract val layoutResId: Int
}