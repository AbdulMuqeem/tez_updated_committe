package com.tez.androidapp.rewamp.signup.verification;

import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.BuildConfig;
import com.tez.androidapp.R;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.models.network.dto.request.UserInfoRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserSignUpRequest;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.signup.presenter.ISignupPermissionActivityPresenter;
import com.tez.androidapp.rewamp.signup.presenter.SignupNumberVerificationPresenter;
import com.tez.androidapp.rewamp.signup.router.AccountAlreadyExistsActivityRouter;
import com.tez.androidapp.rewamp.signup.router.NewTermsAndConditionActivityRouter;
import com.tez.androidapp.rewamp.signup.router.NumberVerificationActivityRouter;
import com.tez.androidapp.rewamp.signup.router.SignupNumberVerificationActivityRouter;
import com.tez.androidapp.rewamp.signup.view.ISignupPermissionActivityView;

import net.tez.logger.library.utils.TextUtil;

public class SignupNumberVerificationActivity extends NumberVerificationActivity implements
        ISignupPermissionActivityView, LocationAvailableCallback {


    private final ISignupPermissionActivityPresenter iSignupNumberVerificationPresenter;

    public SignupNumberVerificationActivity() {
        iSignupNumberVerificationPresenter =
                new SignupNumberVerificationPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected String getFlashCallLabel() {
        return "SIGN-UP";
    }

    @Override
    public void onVerified() {
        super.onVerified();
        NewTermsAndConditionActivityRouter.createInstance().setDependenciesAndRoute(this,
                getPin(),
                getTextFromEtMobileNumber());
        finish();
    }

    private String getPin() {
        return getIntent().getStringExtra(SignupNumberVerificationActivityRouter.USER_PIN);
    }

    private boolean isMobileNumberChanged() {
        return !TextUtil.equals(getMobileNumberFromIntent(),
                getTextFromEtMobileNumber());
    }

    @Override
    public void onCountDownFinish() {
        super.onCountDownFinish();
        setEnableEtMobileNumber(true);
    }

    @Override
    public boolean canStartNumberVerification() {
        return !isMobileNumberChanged();
    }

    @Override
    public void navigateToMobileNumberAlreadyExistActivity() {
        AccountAlreadyExistsActivityRouter.createInstance().setDependenciesAndRoute(this,
                getTextFromEtMobileNumber(),
                getSocialId(),
                getSocialType(),
                getReferralCode());
        finish();
    }

    @Override
    public void preliminaryNumberVerificationWork() {
        super.preliminaryNumberVerificationWork();
        getCurrentLocation(this);
    }

    @Override
    public void createSinchVerification() {
        getIntent().putExtra(SignupNumberVerificationActivityRouter.MOBILE_NUMBER, getTextFromEtMobileNumber());
        getIntent().putExtra(NumberVerificationActivityRouter.PRINCIPLE, getTextFromEtMobileNumber());
        startNumberVerification();
    }

    @Override
    public void onLocationAvailable(@NonNull Location location) {
        this.iSignupNumberVerificationPresenter.submitSignUpRequest(generateUserSignUpRequest(location));
    }

    @Override
    public void onLocationFailed(String message) {
        this.iSignupNumberVerificationPresenter.submitSignUpRequest(generateUserSignUpRequest(null));
    }

    private UserSignUpRequest generateUserSignUpRequest(@Nullable Location location) {

        return Utility.createSignupRequest(this,
                location,
                getTextFromEtMobileNumber(),
                getReferralCode(),
                getSocialId(),
                getSocialType());
    }

    protected String getMobileNumberFromIntent() {
        return getIntent().getStringExtra(SignupNumberVerificationActivityRouter.MOBILE_NUMBER).replaceAll("-", "");
    }

    private String getSocialId() {
        return getIntent().getStringExtra(SignupNumberVerificationActivityRouter.SOCIAL_ID);
    }

    private int getSocialType() {
        return getIntent().getIntExtra(SignupNumberVerificationActivityRouter.SOCIAL_TYPE,
                Utility.PrincipalType.MOBILE_NUMBER.getValue());
    }

    protected String getReferralCode() {
        return getIntent().getStringExtra(SignupNumberVerificationActivityRouter.REF_CODE);
    }

    @Override
    public void onUserSessionTimeOut() {
        showInformativeMessage(R.string.code_1075,
                (dialog, which) -> Utility.directUserToIntroScreenAtGlobalLevel());
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    protected String getScreenName() {
        return "SignupNumberVerificationActivity";
    }
}
