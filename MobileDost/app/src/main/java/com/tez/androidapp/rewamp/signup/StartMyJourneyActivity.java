package com.tez.androidapp.rewamp.signup;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.PlaybackActivity;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.signup.router.SignupActivityRouter;
import com.tez.androidapp.rewamp.signup.router.StartMyJourneyActivityRouter;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.fragment.util.optional.Optional;
import net.tez.viewbinder.library.core.ViewBinder;

public class StartMyJourneyActivity extends PlaybackActivity implements DoubleTapSafeOnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_my_journey);
        ViewBinder.bind(this);
        this.updateToolbar(findViewById(R.id.toolbarJourney));
        init();
    }

    private void init() {
        initListeners();
        initTexts();
    }

    private void initTexts() {
        String number = getIntent().getStringExtra(StartMyJourneyActivityRouter.MOBILE_NUMBER);
        TezTextView tvEditNumber = findViewById(R.id.tvEditNumber);
        TezTextView tvNumber = findViewById(R.id.tvNumber);
        TezTextView tvConfirmNumber = findViewById(R.id.tvConfirmNumber);
        Optional.ifPresent(number, n -> {
            tvNumber.setText(number);
            tvNumber.setVisibility(View.VISIBLE);
            tvEditNumber.setVisibility(View.VISIBLE);
            tvConfirmNumber.setVisibility(View.VISIBLE);
        }, () -> {
            tvNumber.setVisibility(View.GONE);
            tvEditNumber.setVisibility(View.GONE);
            tvConfirmNumber.setVisibility(View.GONE);
        });
    }

    private void initListeners() {
        TezTextView tvEditNumber = findViewById(R.id.tvEditNumber);
        TezTextView tvCancel = findViewById(R.id.tvCancel);
        TezButton btStartJourney = findViewById(R.id.btStartJourney);
        tvEditNumber.setDoubleTapSafeOnClickListener(this);
        tvCancel.setDoubleTapSafeOnClickListener(this);
        btStartJourney.setDoubleTapSafeOnClickListener(this);
    }

    @Override
    public void doubleTapSafeOnClick(View view) {

        switch (view.getId()) {

            case R.id.tvEditNumber:
            case R.id.tvCancel:
                finish();
                break;

            case R.id.btStartJourney:
                startSignUp();
        }
    }

    private void startSignUp() {
        Intent intent = getIntent();
        String number = intent.getStringExtra(StartMyJourneyActivityRouter.MOBILE_NUMBER);
        String socialId = intent.getStringExtra(StartMyJourneyActivityRouter.SOCIAL_ID);
        String refCode = intent.getStringExtra(StartMyJourneyActivityRouter.REF_CODE);
        int socialType = intent.getIntExtra(StartMyJourneyActivityRouter.SOCIAL_TYPE, Utility.PrincipalType.MOBILE_NUMBER.getValue());
        SignupActivityRouter.createInstance().setDependenciesAndRoute(this, number, socialId, socialType, refCode);
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Override
    protected String getScreenName() {
        return "StartMyJourneyActivity";
    }
}
