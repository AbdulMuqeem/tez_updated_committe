package com.tez.androidapp.rewamp.general.beneficiary.view;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.general.feature.account.feature.beneficiary.models.network.BeneficiaryDetails;

public interface IPolicyDetailActivityView extends IBaseView {

    void initBeneficiaryDetails(@NonNull BeneficiaryDetails beneficiaryDetails);

    void setBtTermsAndConditionsClickListener(@NonNull String terms);

    void setBtEditBeneficiaryVisibility(int visibility);

    void setClContentVisibility(int visibility);
}
