package com.tez.androidapp.rewamp.general.beneficiary.listener;

import com.tez.androidapp.app.base.response.BaseResponse;

public interface DeleteBeneficiaryListener {

    void onDeleteBeneficiarySuccess(BaseResponse response);

    void onDeleteBeneficiaryFailure(int errorCode, String message);
}
