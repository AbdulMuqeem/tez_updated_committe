package com.tez.androidapp.rewamp.general.suspend.account.presenter;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.rewamp.general.suspend.account.interactor.ISuspendAccountOTPActivityInteractor;
import com.tez.androidapp.rewamp.general.suspend.account.interactor.SuspendAccountOTPActivityInteractor;
import com.tez.androidapp.rewamp.general.suspend.account.view.ISuspendAccountOTPActivityView;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public class SuspendAccountOTPActivityPresenter implements ISuspendAccountOTPActivityPresenter, ISuspendAccountOTPActivityInteractorOutput {

    private final ISuspendAccountOTPActivityView iSuspendAccountOTPActivityView;
    private final ISuspendAccountOTPActivityInteractor iSuspendAccountOTPActivityInteractor;

    public SuspendAccountOTPActivityPresenter(ISuspendAccountOTPActivityView iSuspendAccountOTPActivityView) {
        this.iSuspendAccountOTPActivityView = iSuspendAccountOTPActivityView;
        iSuspendAccountOTPActivityInteractor = new SuspendAccountOTPActivityInteractor(this);
    }

    @Override
    public void suspendAccount() {
        iSuspendAccountOTPActivityView.setPinViewOtpEnabled(false);
        iSuspendAccountOTPActivityView.showTezLoader();
        iSuspendAccountOTPActivityInteractor.suspendAccount();
    }

    @Override
    public void onSuspendAccountSuccess(BaseResponse baseResponse) {
        iSuspendAccountOTPActivityView.setPinViewOtpEnabled(true);
        iSuspendAccountOTPActivityView.startTimer();
        iSuspendAccountOTPActivityView.dismissTezLoader();
    }

    @Override
    public void onSuspendAccountFailure(int errorCode, String message) {
        iSuspendAccountOTPActivityView.dismissTezLoader();
        iSuspendAccountOTPActivityView.showError(errorCode,
                (dialog, which) -> iSuspendAccountOTPActivityView.finishActivity());
    }

    @Override
    public void resendCode() {
        iSuspendAccountOTPActivityView.setTvResendCodeEnabled(false);
        iSuspendAccountOTPActivityView.showTezLoader();
        iSuspendAccountOTPActivityInteractor.resendCode();
    }

    @Override
    public void onResendCodeSuccess() {
        iSuspendAccountOTPActivityView.setPinViewOtpClearText();
        iSuspendAccountOTPActivityView.dismissTezLoader();
        iSuspendAccountOTPActivityView.startTimer();
    }

    @Override
    public void onResendCodeFailure(int errorCode) {
        iSuspendAccountOTPActivityView.dismissTezLoader();
        iSuspendAccountOTPActivityView.setPinViewOtpClearText();
        iSuspendAccountOTPActivityView.showError(errorCode,
                (dialog, which) -> iSuspendAccountOTPActivityView.finishActivity());
    }

    @Override
    public void suspendAccountVerifyOtp(@NonNull String otp) {
        iSuspendAccountOTPActivityView.showTezLoader();
        iSuspendAccountOTPActivityInteractor.suspendAccountVerifyOtp(otp);
    }

    @Override
    public void onSuspendAccountVerifyOTPSuccess(BaseResponse baseResponse) {
        iSuspendAccountOTPActivityView.setTezLoaderToBeDissmissedOnTransition();
        iSuspendAccountOTPActivityView.takeFeedbackFromUser();
    }

    @Override
    public void onSuspendAccountVerifyOTPFailure(int errorCode, String message) {
        iSuspendAccountOTPActivityView.setPinViewOtpEnabled(true);
        iSuspendAccountOTPActivityView.setPinViewOtpClearText();
        iSuspendAccountOTPActivityView.dismissTezLoader();
        iSuspendAccountOTPActivityView.showError(errorCode);
    }
}
