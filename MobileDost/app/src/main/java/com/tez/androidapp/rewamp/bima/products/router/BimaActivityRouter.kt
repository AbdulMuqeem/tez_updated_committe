package com.tez.androidapp.rewamp.bima.products.router

import android.content.Intent
import com.tez.androidapp.app.base.ui.activities.BaseActivity
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter
import com.tez.androidapp.rewamp.bima.products.view.BimaActivity

class BimaActivityRouter : BaseActivityRouter() {

    fun setDependenciesAndRoute(from: BaseActivity) {
        route(from, createIntent(from))
    }

    fun setDependenciesAndRoute(from: BaseActivity, initialFragment: Int) {
        val intent = createIntent(from)
        intent.putExtra(INITIAL_FRAGMENT, initialFragment)
        route(from, intent)
    }

    private fun createIntent(from: BaseActivity) = Intent(from, BimaActivity::class.java)

    class Dependencies internal constructor(intent: Intent) {
        val initialFragment = intent.getIntExtra(INITIAL_FRAGMENT, -100)
    }

    companion object {
        @JvmStatic
        fun createInstance(): BimaActivityRouter = BimaActivityRouter()

        fun getDependencies(intent: Intent): Dependencies = Dependencies(intent)

        private const val INITIAL_FRAGMENT: String = "INITIAL_FRAGMENT"

        const val POLICY = 2
        const val CLAIM = 3
    }
}