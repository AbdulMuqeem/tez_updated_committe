package com.tez.androidapp.rewamp.bima.products.view

import androidx.annotation.DrawableRes

interface BimaCallback {

    fun showProductsFragment()

    fun showPoliciesFragment()

    fun setIllustrationVisibility(visibility: Int)

    fun setIllustrationAndVisibility(@DrawableRes illustration: Int, visibility: Int)
}