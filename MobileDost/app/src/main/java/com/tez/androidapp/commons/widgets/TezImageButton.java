package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;

import com.tez.androidapp.R;

import net.tez.fragment.util.base.IBaseWidget;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

/**
 * Created by VINOD KUMAR on 2/15/2019.
 */
public class TezImageButton extends AppCompatImageButton implements IBaseWidget {


    @NonNull
    private String imageLabel = "";

    public TezImageButton(Context context) {
        super(context);
    }

    public TezImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public TezImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    @NonNull
    @Override
    public String getLabel() {
        return imageLabel;
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TezImageButton);

        try {

            int clickEffect = typedArray.getInt(R.styleable.TezImageButton_click_effect, RIPPLE_EFFECT);
            attachClickEffect(clickEffect, this);

            String label = typedArray.getString(R.styleable.TezImageButton_click_effect);
            if (label != null)
                this.imageLabel = label;

        } finally {
            typedArray.recycle();
        }
    }

    @Override
    public void setDoubleTapSafeOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        super.setOnClickListener(doubleTapSafeOnClickListener);
    }
}
