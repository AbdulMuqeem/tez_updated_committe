package com.tez.androidapp.app.general.feature.account.feature.suspension.account.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.UserReActivateAccountOTPCallback;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;


/**
 * Created  on 9/17/2017.
 */

public class UserReActivateAccountRH extends BaseRH<BaseResponse> {

    UserReActivateAccountOTPCallback userReActivateAccountOTPCallback;

    public UserReActivateAccountRH(BaseCloudDataStore baseCloudDataStore, UserReActivateAccountOTPCallback userReActivateAccountOTPCallback) {
        super(baseCloudDataStore);
        this.userReActivateAccountOTPCallback = userReActivateAccountOTPCallback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse baseResponse = value.response().body();
        if (isErrorFree(baseResponse)) userReActivateAccountOTPCallback.onUserAccountReActivatedSuccess(baseResponse);
        else onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());

    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        userReActivateAccountOTPCallback.onUserAccountReActivatedFailure(new BaseResponse(errorCode, message));
    }
}
