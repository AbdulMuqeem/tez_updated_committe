package com.tez.androidapp.rewamp.reactivate.account.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.reactivate.account.view.AccountReactivateActivity;

public class AccountReactivateActivityRouter extends BaseActivityRouter {

    public static AccountReactivateActivityRouter createInstance() {
        return new AccountReactivateActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, AccountReactivateActivity.class);
    }
}
