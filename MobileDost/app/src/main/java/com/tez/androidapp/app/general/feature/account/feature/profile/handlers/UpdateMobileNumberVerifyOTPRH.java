package com.tez.androidapp.app.general.feature.account.feature.profile.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateMobileNumberVerifyOTPCallback;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created by Rehman Murad Ali on 9/8/2017.
 */

public class UpdateMobileNumberVerifyOTPRH extends BaseRH<BaseResponse> {
    UpdateMobileNumberVerifyOTPCallback updateMobileNumberVerifyOTPCallback;

    public UpdateMobileNumberVerifyOTPRH(BaseCloudDataStore baseCloudDataStore, UpdateMobileNumberVerifyOTPCallback updateMobileNumberVerifyOTPCallback) {
        super(baseCloudDataStore);
        this.updateMobileNumberVerifyOTPCallback = updateMobileNumberVerifyOTPCallback;
    }


    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse baseResponse = value.response().body();
        if(baseResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
        {
            updateMobileNumberVerifyOTPCallback.onUpdateMobileNumberVerifyOTPSuccess(baseResponse);
        }
        else
            updateMobileNumberVerifyOTPCallback.onUpdateMobileNumberVerifyOTPFailure(baseResponse.getStatusCode(),baseResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        updateMobileNumberVerifyOTPCallback.onUpdateMobileNumberVerifyOTPFailure(errorCode,message);
    }
}
