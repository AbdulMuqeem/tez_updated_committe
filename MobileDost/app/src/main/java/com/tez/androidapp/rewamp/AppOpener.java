package com.tez.androidapp.rewamp;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.util.DialogUtil;

@SuppressWarnings("WeakerAccess")
public final class AppOpener {

    private AppOpener() {

    }

    public static void openEmail(@NonNull Activity activity, String emailAddress) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            String uri = "mailto:" + emailAddress + "?subject=" + "" + "&body=" + "";
            intent.setData(Uri.parse(uri));
            activity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            if (cannotOpenAppInPlayStore(activity, "com.google.android.gm"))
                showCannotOpenDialog(activity, R.string.email);
        } catch (Exception e) {
            showCannotOpenDialog(activity, R.string.email);
        }
    }

    public static void openDialer(@NonNull Activity activity, String dialTo) {
        try {
            String number = ("tel:" + dialTo);
            Intent mIntent = new Intent(Intent.ACTION_DIAL);
            mIntent.setData(Uri.parse(number));
            activity.startActivity(mIntent);
        } catch (ActivityNotFoundException e) {
            if (cannotOpenAppInPlayStore(activity, "com.google.android.dialer"))
                showCannotOpenDialog(activity, R.string.dialer);
        } catch (Exception e) {
            showCannotOpenDialog(activity, R.string.dialer);
        }
    }

    public static void openWhatsapp(@NonNull Activity activity, @NonNull String number) {
        try {
            Intent sendIntent = new Intent("android.intent.action.MAIN");
            sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, "");
            sendIntent.putExtra("jid", number + "@s.whatsapp.net");
            sendIntent.setPackage("com.whatsapp");
            activity.startActivity(sendIntent);
        } catch (ActivityNotFoundException e) {
            if (cannotOpenAppInPlayStore(activity, "com.whatsapp"))
                showCannotOpenDialog(activity, R.string.whatsapp);
        } catch (Exception e) {
            showCannotOpenDialog(activity, R.string.whatsapp);
        }
    }

    public static void openMessenger(@NonNull Activity activity, long facebookUserId) {
        try {
            Uri uri = Uri.parse("fb-messenger://user/");
            uri = ContentUris.withAppendedId(uri, facebookUserId);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            activity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            if (cannotOpenAppInPlayStore(activity, "com.facebook.orca"))
                showCannotOpenDialog(activity, R.string.messenger);
        } catch (Exception e) {
            showCannotOpenDialog(activity, R.string.messenger);
        }
    }

    public static void openUrl(@NonNull Activity activity, @NonNull String url) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            activity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            if (cannotOpenAppInPlayStore(activity, "com.android.chrome"))
                showCannotOpenDialog(activity, R.string.browser);
        } catch (Exception e) {
            showCannotOpenDialog(activity, R.string.browser);
        }
    }

    private static boolean cannotOpenAppInPlayStore(@NonNull Activity activity, @NonNull String appPackageName) {
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            return false;
        } catch (android.content.ActivityNotFoundException e) {
            try {
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google" + ".com/store/apps/details?id=" + appPackageName)));
                return false;
            } catch (Exception e1) {
                return true;
            }
        }
    }

    private static void showCannotOpenDialog(@NonNull Activity activity, @StringRes int stringRes) {
        DialogUtil.showInformativeDialog(activity, activity.getString(R.string.error), activity.getString(R.string.app_open_error, activity.getString(stringRes)));
    }
}
