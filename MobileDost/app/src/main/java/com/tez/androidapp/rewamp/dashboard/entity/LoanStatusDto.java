package com.tez.androidapp.rewamp.dashboard.entity;

import com.tez.androidapp.commons.models.network.LoanDetails;

import java.io.Serializable;

public class LoanStatusDto implements Serializable {

    private String status;
    private LoanDetails loanDetails;
    private Integer loanId;

    public String getStatus() {
        return status;
    }

    public LoanDetails getLoanDetails() {
        return loanDetails;
    }

    public Integer getLoanId() {
        return loanId;
    }
}
