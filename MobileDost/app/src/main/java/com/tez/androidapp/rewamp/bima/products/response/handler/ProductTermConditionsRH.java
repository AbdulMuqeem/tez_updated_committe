package com.tez.androidapp.rewamp.bima.products.response.handler;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.bima.products.callback.ProductTermConditionsCallback;
import com.tez.androidapp.rewamp.bima.products.response.ProductTermConditionsResponse;

public class ProductTermConditionsRH extends BaseRH<ProductTermConditionsResponse> {

    private final ProductTermConditionsCallback callback;

    public ProductTermConditionsRH(BaseCloudDataStore baseCloudDataStore, ProductTermConditionsCallback callback) {
        super(baseCloudDataStore);
        this.callback = callback;
    }

    @Override
    protected void onSuccess(Result<ProductTermConditionsResponse> value) {
        ProductTermConditionsResponse response = value.response().body();

        if (response != null) {
            if (isErrorFree(response))
                callback.onGetTermsConditionsSuccess(response);
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());
        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        callback.onGetTermsConditionsFailure(errorCode, message);
    }
}
