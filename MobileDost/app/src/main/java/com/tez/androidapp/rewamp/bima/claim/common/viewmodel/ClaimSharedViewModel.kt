package com.tez.androidapp.rewamp.bima.claim.common.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tez.androidapp.app.general.feature.questions.models.network.models.ClaimQuestions
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.Evidence
import com.tez.androidapp.rewamp.general.purpose.Purpose

class ClaimSharedViewModel : ViewModel() {
    val amountLiveData = MutableLiveData<Double>()
    val reasonLiveData = MutableLiveData<Purpose>()
    val claimQuestionListLiveData = MutableLiveData<ClaimQuestions>()
    val claimDocumentListLiveData = MutableLiveData<List<Evidence>>()
}