package com.tez.androidapp.rewamp.bima.products.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.tez.androidapp.R
import com.tez.androidapp.app.base.ui.fragments.BaseFragment
import com.tez.androidapp.commons.utils.app.Constants
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Failure
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Loading
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Success
import com.tez.androidapp.rewamp.bima.products.adapter.ProductsAdapter
import com.tez.androidapp.rewamp.bima.products.adapter.ProductsAdapter.ProductListener
import com.tez.androidapp.rewamp.bima.products.model.InsuranceProduct
import com.tez.androidapp.rewamp.bima.products.policy.router.BaseProductActivityRouter
import com.tez.androidapp.rewamp.bima.products.policy.router.CoronaDefenseActivityRouter
import com.tez.androidapp.rewamp.bima.products.policy.router.DigitalOpdActivityRouter
import com.tez.androidapp.rewamp.bima.products.policy.router.HospitalCoverageActivityRouter
import com.tez.androidapp.rewamp.bima.products.router.ProductPolicyActivityRouter
import com.tez.androidapp.rewamp.bima.products.viewmodel.ProductsViewModel
import kotlinx.android.synthetic.main.products_fragment.*

class ProductsFragment : BaseFragment(), ProductListener {

    private var baseView: View? = null
    private lateinit var callback: BimaCallback
    private val model: ProductsViewModel by activityViewModels()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = context as BimaCallback
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        baseView = baseView ?: inflater.inflate(R.layout.products_fragment, container, false)
        return baseView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callback.setIllustrationVisibility(View.GONE)
        model.getInsuranceProductListLiveData().observe(viewLifecycleOwner, {
            if (rvProducts.adapter == null) {
                val adapter = ProductsAdapter(it, this)
                rvProducts.adapter = adapter
            }
        })
        model.networkCallMutableLiveData.observe(viewLifecycleOwner, {

            when (it) {
                is Loading -> {
                    shimmer.visibility = View.VISIBLE
                    clContent.visibility = View.GONE
                }
                is Failure -> {
                    shimmer.visibility = View.GONE
                    clContent.visibility = View.GONE
                    showError(it.errorCode)
                }
                is Success -> {
                    shimmer.visibility = View.GONE
                    clContent.visibility = View.VISIBLE
                }
            }
        })
    }

    override fun onPurchaseProduct(insuranceProduct: InsuranceProduct) = getBaseActivity {
        var router: BaseProductActivityRouter? = null
        when (insuranceProduct.id) {
            Constants.HOSPITAL_COVERAGE_PLAN_ID -> router = HospitalCoverageActivityRouter.createInstance()
            Constants.DIGITAL_OPD_PLAN_ID -> router = DigitalOpdActivityRouter.createInstance()
            Constants.CORONA_DEFENSE_PLAN_ID -> router = CoronaDefenseActivityRouter.createInstance()
        }
        router?.setDependenciesAndRoute(it, insuranceProduct.title)
    }

    override fun onGetProductDetails(insuranceProduct: InsuranceProduct) = getBaseActivity {
        ProductPolicyActivityRouter.createInstance()
                .setDependenciesAndRoute(it, insuranceProduct.policyId!!, insuranceProduct.id, insuranceProduct.title)
    }

    companion object {
        fun newInstance(): ProductsFragment = ProductsFragment()
    }
}