package com.tez.androidapp.rewamp.profile.trust.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.profile.complete.router.CnicInformationActivityRouter;
import com.tez.androidapp.rewamp.profile.complete.router.CompleteProfileCnicUploadActivityRouter;
import com.tez.androidapp.rewamp.profile.router.CompleteProfileRouter;
import com.tez.androidapp.rewamp.profile.trust.presenter.IProfileTrustActivityPresenter;
import com.tez.androidapp.rewamp.profile.trust.presenter.ProfileTrustActivityPresnter;
import com.tez.androidapp.rewamp.signup.router.ContactUsActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class ProfileTrustActivity extends BaseActivity implements IProfileTrustActivityView {

    @BindView(R.id.btCreateProfile)
    private TezButton btCreateProfile;

    @BindView(R.id.tvQuestion)
    private TezTextView tvQuestion;

    private final IProfileTrustActivityPresenter iProfileTrustActivityPresenter;

    public ProfileTrustActivity() {
        this.iProfileTrustActivityPresenter =
                new ProfileTrustActivityPresnter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_trust);
        ViewBinder.bind(this);
        init();
    }

    @Override
    public void routeToProfileCompletionCnicUploadActivity(boolean isCnicFrontPicToBeUploaded,
                                                           boolean isCnicBackPicToBeUploaded,
                                                           boolean isCnicSelfieToBeUploaded) {
        CompleteProfileCnicUploadActivityRouter.createInstance().setDependenciesAndRoute(this,
                isCnicFrontPicToBeUploaded,
                isCnicBackPicToBeUploaded,
                isCnicSelfieToBeUploaded,
                getKeyRoute());
    }

    private int getKeyRoute() {
        return getIntent().getIntExtra(CompleteProfileRouter.ROUTE_KEY, CompleteProfileRouter.ROUTE_TO_DEFAULT);
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void routeToCnicInformationActivity() {
        CnicInformationActivityRouter.createInstance().setDependenciesAndRoute(this, getKeyRoute());
    }

    private void init() {
        setListeners();
    }

    private void setListeners() {
        this.btCreateProfile.setDoubleTapSafeOnClickListener(this::onClickBtCreateProfile);
        this.tvQuestion.setDoubleTapSafeOnClickListener(view ->
                ContactUsActivityRouter.createInstance().setDependenciesAndRoute(this));
    }

    private void onClickBtCreateProfile(View v) {
        showTezLoader();
        this.iProfileTrustActivityPresenter.getCnicUploads();
    }

    @Override
    protected String getScreenName() {
        return "ProfileTrustActivity";
    }
}
