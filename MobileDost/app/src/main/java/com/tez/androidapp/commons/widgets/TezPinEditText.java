package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.textwatchers.TezTextWatcher;

import net.tez.logger.library.utils.TextUtil;

/**
 * Created by VINOD KUMAR on 5/27/2019.
 */
public class TezPinEditText extends TezFrameLayout implements View.OnClickListener {

    private TezEditTextView etPin;
    private TezImageView ivEye;
    private TezTextInputLayout tilPin;

    public TezPinEditText(Context context) {
        super(context);
    }

    public TezPinEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public TezPinEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public String getPin() {
        return this.etPin.getValueText();
    }

    public void setErrorGlobally(String errorGlobally) {
        this.tilPin.setError(errorGlobally);
        this.etPin.changeDrawableColorOnError(errorGlobally);
    }

    public void setError(String error) {
        this.tilPin.setError(error);
        this.etPin.changeDrawableColorOnError(error);
    }

    public void clearText() {
        this.etPin.clearText();
    }

    public TezEditTextView getPinEditText() {
        return this.etPin;
    }

    public void setPin(@NonNull String pin) {
        this.etPin.setText(pin);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.ivEye) {

            TransformationMethod method = this.etPin.getTransformationMethod();
            if (method == null) {
                this.etPin.setTransformationMethod(PasswordTransformationMethod.getInstance());
                this.ivEye.setImageResource(R.drawable.ic_eye);
            } else {
                this.etPin.setTransformationMethod(null);
                this.ivEye.setImageResource(R.drawable.ic_eye_active);
            }

            Editable text = this.etPin.getText();
            if (text != null)
                this.etPin.setSelection(text.length());

            this.etPin.requestFocus();
        }
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs) {

        View view = inflate(context, R.layout.tez_pin_edit_text, null);
        this.etPin = view.findViewById(R.id.etPin);
        this.ivEye = view.findViewById(R.id.ivEye);
        this.etPin.acceptOnlyDigits();
        this.etPin.disableCopyPaste();
        this.ivEye.setOnClickListener(this);
        this.ivEye.setVisibility(GONE);
        this.etPin.setTransformationMethod(PasswordTransformationMethod.getInstance());
        this.etPin.addTextChangedListener(new TezTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                ivEye.setVisibility(s == null || TextUtil.isEmpty(s.toString()) ? GONE : VISIBLE);
            }
        });

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TezPinEditText);

        try {

            int maxDigits = typedArray.getInt(R.styleable.TezPinEditText_pin_digits, 4);
            this.etPin.setMaxDigits(maxDigits);

            String hint = typedArray.getString(R.styleable.TezPinEditText_hint);
            View layout = view.findViewById(R.id.tilPin);
            if (layout instanceof TezTextInputLayout) {
                this.tilPin = (TezTextInputLayout) layout;
                if (hint != null)
                    this.tilPin.setHint(hint);
            }

            int imeOption = typedArray.getInt(R.styleable.TezPinEditText_pin_ime_option, 0);
            switch (imeOption) {

                default:
                case 0:
                    this.etPin.setImeOptions(EditorInfo.IME_ACTION_DONE);
                    break;

                case 1:
                    this.etPin.setImeOptions(EditorInfo.IME_ACTION_NEXT);
            }

        } finally {
            typedArray.recycle();
        }

        this.addView(view);
    }
}
