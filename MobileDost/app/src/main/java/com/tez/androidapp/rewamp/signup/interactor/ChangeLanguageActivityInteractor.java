package com.tez.androidapp.rewamp.signup.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.signup.languagecall.LanguageCallback;
import com.tez.androidapp.rewamp.signup.presenter.IChangeLanguageActivityInteractorOutput;

/**
 * Created by VINOD KUMAR on 8/9/2019.
 */
public class ChangeLanguageActivityInteractor implements IChangeLanguageActivityInteractor {

    private final IChangeLanguageActivityInteractorOutput iChangeLanguageActivityInteractorOutput;

    public ChangeLanguageActivityInteractor(IChangeLanguageActivityInteractorOutput iChangeLanguageActivityInteractorOutput) {
        this.iChangeLanguageActivityInteractorOutput = iChangeLanguageActivityInteractorOutput;
    }


    @Override
    public void sendUpdatedLanguageCodeToServer(@NonNull final String languageCode,
                                                final @NonNull String lang) {

        UserAuthCloudDataStore.getInstance().language(languageCode, new LanguageCallback() {
            @Override
            public void onLanguageSuccess() {
                iChangeLanguageActivityInteractorOutput.onLanguageSuccess(lang);
            }

            @Override
            public void onLanguageFailure(int errorCode, String errorDescription) {
                if (Utility.isUnauthorized(errorCode))
                    sendUpdatedLanguageCodeToServer(languageCode, lang);
                else
                    iChangeLanguageActivityInteractorOutput.onLanguageFailure(errorCode, errorDescription);
            }
        });
    }
}
