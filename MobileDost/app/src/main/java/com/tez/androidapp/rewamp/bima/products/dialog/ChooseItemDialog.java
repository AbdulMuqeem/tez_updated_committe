package com.tez.androidapp.rewamp.bima.products.dialog;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.recyclerview.widget.RecyclerView;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezDialog;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.bima.products.adapter.ChooseItemAdapter;
import com.tez.androidapp.rewamp.bima.products.model.Item;

import java.util.List;

public class ChooseItemDialog extends TezDialog {

    private List<Item> itemList;
    @StringRes
    private int heading;
    private ChooseItemAdapter.ChooseItemListener listener;

    public ChooseItemDialog(@NonNull Context context) {
        super(context);
    }

    public void setHeading(@StringRes int heading) {
        this.heading = heading;
    }

    public void setItemList(@NonNull List<Item> itemList) {
        this.itemList = itemList;
    }

    public void setListener(ChooseItemAdapter.ChooseItemListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_number_dialog);
        TezTextView tvHeading = findViewById(R.id.tvHeading);
        tvHeading.setText(heading);
        TezTextView tvCancel = findViewById(R.id.tvCancel);
        tvCancel.setDoubleTapSafeOnClickListener(v -> dismiss());
        RecyclerView rvNumbers = findViewById(R.id.rvNumbers);
        ChooseItemAdapter adapter = new ChooseItemAdapter(itemList, listener);
        rvNumbers.setAdapter(adapter);
    }
}
