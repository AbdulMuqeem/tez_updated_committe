package com.tez.androidapp.rewamp.gold.onboarding.callback;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.rewamp.gold.onboarding.response.GetGoldRateResponse;

public interface GetGoldRateCallback {

    void onGetGoldRateSuccess(@NonNull GetGoldRateResponse getGoldRateResponse);

    void onGetGoldRateFailure(int errorCode, @Nullable String message);
}
