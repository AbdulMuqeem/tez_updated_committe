package com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks;

import com.tez.androidapp.commons.models.network.dto.response.UserLoginResponse;

/**
 * Created  on 12/15/2016.
 */

public interface UserLoginCallback {

    void onUserLoginSuccess(UserLoginResponse loginResponse);

    void onUserLoginFailure(int errorCode, String message);
}
