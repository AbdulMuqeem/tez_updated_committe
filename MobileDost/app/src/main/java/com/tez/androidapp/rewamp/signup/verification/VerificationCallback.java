package com.tez.androidapp.rewamp.signup.verification;

/**
 * Created by FARHAN DHANANI on 6/5/2019.
 */
public interface VerificationCallback {
    void onVerificationTriesFinish();
    void onSinchFlashCallPermissionFailure();
    void onCountDownFinish();
    void onVerified();
    void onUserSessionTimeOut();
}
