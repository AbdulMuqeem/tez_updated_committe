package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;
import com.tez.androidapp.rewamp.committee.view.CommitteeInviteActivity;

import androidx.annotation.NonNull;

import static com.tez.androidapp.rewamp.committee.router.CommitteeCompletionActivityRouter.COMMITTEE_DATA;

public class CommitteeInviteActivityRouter extends BaseActivityRouter {


    public static final String SELECTED_AMOUNT = "SELECTED_AMOUNT";
    public static final String SELECTED_MEMBER = "SELECTED_MEMBER";

    public static CommitteeInviteActivityRouter createInstance() {
        return new CommitteeInviteActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, CommitteePackageModel committeePackageModel) {
        Intent intent = createIntent(from);
        intent.putExtra(COMMITTEE_DATA, committeePackageModel);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CommitteeInviteActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, Integer amount, Integer members) {
        Intent intent = createIntent(from);
        intent.putExtra(SELECTED_AMOUNT, amount);
        intent.putExtra(SELECTED_MEMBER, members);
        route(from, intent);
    }
}
