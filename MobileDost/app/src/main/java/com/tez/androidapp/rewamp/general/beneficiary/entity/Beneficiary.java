package com.tez.androidapp.rewamp.general.beneficiary.entity;

import java.io.Serializable;

public class Beneficiary implements Serializable {

    private Integer id;
    private String name;
    private String mobileNumber;
    private String relationship;
    private Integer relationshipId;
    private boolean isDefault;
    private boolean isEditable;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getRelationship() {
        return relationship;
    }

    public Integer getRelationshipId() {
        return relationshipId;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public boolean isEditable() {
        return isEditable;
    }
}
