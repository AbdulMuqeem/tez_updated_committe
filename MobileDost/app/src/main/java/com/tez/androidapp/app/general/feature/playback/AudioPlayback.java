package com.tez.androidapp.app.general.feature.playback;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RawRes;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;

import com.tez.androidapp.app.MobileDostApplication;
import net.tez.fragment.util.optional.Optional;

/**
 * Created by VINOD KUMAR on 5/10/2019.
 */
public class AudioPlayback implements AudioControlInterface,
        DefaultLifecycleObserver,
        MediaPlayer.OnCompletionListener,
        AudioManager.OnAudioFocusChangeListener {

    private AudioPlaybackCountDownTimer audioPlaybackCountDownTimer;

    @Nullable
    private AudioPlaybackListener audioPlaybackListener;

    @RawRes
    private int audioId;

    @Nullable
    private MediaPlayer mediaPlayer;

    private AudioPlayback(@RawRes int audioId,
                          @Nullable AudioPlaybackListener audioPlaybackListener) {
        this.audioId = audioId;
        this.audioPlaybackListener = audioPlaybackListener;
        this.audioPlaybackCountDownTimer = new AudioPlaybackCountDownTimer();
    }

    @NonNull
    public static AudioControlInterface create(@RawRes int audioId,
                                               @Nullable AudioPlaybackListener audioPlaybackListener) {
        return new AudioPlayback(audioId, audioPlaybackListener);
    }

    @NonNull
    @Override
    public DefaultLifecycleObserver getObserver() {
        return this;
    }

    @Override
    public void setAudioId(int audioId) {
        this.audioId = audioId;
        this.stopSound();
        this.releaseMediaPlayer();
        this.initMediaPlayer();
        this.resetAudioCountDownTimer();
    }

    @Override
    public void startPlayer() {
        this.requestAudioFocus(mp -> this.sendOnCompleteToCallback());
    }

    @Override
    public void stopPlayer() {
        this.stopSound();
    }

    @Override
    public void startTimer() {
        this.startAudioCountDownTimer();
    }

    @Override
    public void stopTimer() {
        this.stopAudioCountDownTimer();
    }

    @Override
    public void release() {
        this.stopAudioCountDownTimer();
        this.stopSound();
        this.releaseMediaPlayer();
        this.releaseMembers();
    }

    @Override
    public void onStart(@NonNull LifecycleOwner owner) {
        this.initMediaPlayer();
    }

    @Override
    public void onResume(@NonNull LifecycleOwner owner) {
        this.startAudioCountDownTimer();
    }

    @Override
    public void onPause(@NonNull LifecycleOwner owner) {
        this.stopAudioCountDownTimer();
    }

    @Override
    public void onStop(@NonNull LifecycleOwner owner) {
        this.stopSound();
        this.releaseMediaPlayer();
    }

    @Override
    public void onDestroy(@NonNull LifecycleOwner owner) {
        this.releaseMembers();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        this.sendOnCompleteToCallback();
        this.resetAudioCountDownTimer();
        this.audioPlaybackCountDownTimer.increaseTimesAudioHasPlayed();
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        if (mediaPlayer != null && focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK)
            this.mediaPlayer.setVolume(0.2f, 0.2f);
    }


    private void initMediaPlayer() {
        this.mediaPlayer = MediaPlayer.create(MobileDostApplication.getAppContext(), audioId);
        if (mediaPlayer != null) {
            mediaPlayer.setVolume(100f, 100f);
            mediaPlayer.setOnCompletionListener(this);
            this.audioPlaybackCountDownTimer.resetTimesAudioHasPlayed();
        } else
            this.stopAudioCountDownTimer();
    }

    private void setOnCompletionListener(MediaPlayer.OnCompletionListener listener) {
        if (mediaPlayer != null)
            this.mediaPlayer.setOnCompletionListener(listener);
    }

    private void requestAudioFocus(MediaPlayer.OnCompletionListener listener) {
        this.setOnCompletionListener(listener);
        Context appContext = MobileDostApplication.getAppContext();
        AudioManager audioManager = (AudioManager) appContext.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
            if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
                this.playSound();
            else
                this.sendOnCompleteToCallback();
        }
    }

    private void playSound() {
        try {
            if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
                Optional.ifPresent(audioPlaybackListener, AudioPlaybackListener::onPlaybackStart);
                this.mediaPlayer.start();
            } else
                this.resetAudioCountDownTimer();
        } catch (Exception ignore) {
            //ignore
        }
    }

    private void stopSound() {
        try {
            if (mediaPlayer != null) {
                this.mediaPlayer.stop();
                this.sendOnCompleteToCallback();
            }
        } catch (Exception ignore) {
            //ignore
        }
    }

    private void sendOnCompleteToCallback() {
        Optional.ifPresent(audioPlaybackListener, AudioPlaybackListener::onPlaybackComplete);
    }

    private void releaseMediaPlayer() {
        Optional.ifPresent(mediaPlayer, MediaPlayer::release);
    }

    private void releaseMembers() {
        this.mediaPlayer = null;
        this.audioPlaybackListener = null;
        this.audioPlaybackCountDownTimer = null;
    }

    private void startAudioCountDownTimer() {
        this.audioPlaybackCountDownTimer.start();
    }

    private void stopAudioCountDownTimer() {
        this.audioPlaybackCountDownTimer.cancel();
    }

    private void resetAudioCountDownTimer() {
        this.stopAudioCountDownTimer();
        this.startAudioCountDownTimer();
    }

    private class AudioPlaybackCountDownTimer extends TezCountDownTimer {

        private static final int MAX_TIMES_PLAYBACK_ALLOWED = 3;
        private static final int MILLIS_IN_FUTURE = 15000;
        private static final int COUNT_DOWN_INTERVAL = 1000;
        private int mTimesAudioHasPlayed = 0;

        private AudioPlaybackCountDownTimer() {
            super(MILLIS_IN_FUTURE, COUNT_DOWN_INTERVAL);
        }

        @Override
        public void onFinish() {

            if (mediaPlayer != null && mTimesAudioHasPlayed < MAX_TIMES_PLAYBACK_ALLOWED) {
                AudioPlayback.this.requestAudioFocus(AudioPlayback.this);
            } else
                this.cancel();
        }

        private void resetTimesAudioHasPlayed() {
            this.mTimesAudioHasPlayed = 0;
        }

        private void increaseTimesAudioHasPlayed() {
            this.mTimesAudioHasPlayed++;
        }
    }
}
