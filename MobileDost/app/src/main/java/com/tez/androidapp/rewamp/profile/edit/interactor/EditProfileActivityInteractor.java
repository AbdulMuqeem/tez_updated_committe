package com.tez.androidapp.rewamp.profile.edit.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.GetUserProfileCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.SetUserPictureCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateUserProfileCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.ValidateInfoCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.ValidateInfo;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateUserProfileRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.ValidateInfoRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.GetUserProfileResponse;
import com.tez.androidapp.commons.location.callbacks.GetCitiesCallback;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.app.City;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;
import com.tez.androidapp.rewamp.profile.edit.presenter.IEditProfileActivityInteractorOutput;

import java.io.File;
import java.util.List;

public class EditProfileActivityInteractor implements IEditProfileActivityInteractor {

    private final IEditProfileActivityInteractorOutput iEditProfileActivityInteractorOutput;

    public EditProfileActivityInteractor(IEditProfileActivityInteractorOutput iEditProfileActivityInteractorOutput) {
        this.iEditProfileActivityInteractorOutput = iEditProfileActivityInteractorOutput;
    }

    @Override
    public void getCities() {
        UserCloudDataStore.getInstance().getCities(new GetCitiesCallback() {
            @Override
            public void onGetCitiesSuccess(@NonNull List<City> cities) {
                iEditProfileActivityInteractorOutput.onGetCitiesSuccess(cities);
                getUserProfile();
            }

            @Override
            public void onGetCitiesError(int errorcode, String message) {
                iEditProfileActivityInteractorOutput.onGetCitiesError(errorcode, message);
            }
        });
    }

    @Override
    public void setUserPicture(@NonNull File file) {
        UserAuthCloudDataStore.getInstance().setUserPicture(file, new SetUserPictureCallback() {
            @Override
            public void setPictureOnSuccess(BaseResponse baseResponse) {
                MDPreferenceManager.setProfilePictureLastModifiedTime(System.currentTimeMillis());
                iEditProfileActivityInteractorOutput.setPictureOnSuccess(file);
            }

            @Override
            public void setPictureOnFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    setUserPicture(file);
                else iEditProfileActivityInteractorOutput.setPictureOnFailure(errorCode, message);
            }
        });
    }

    private void getUserProfile() {
        UserAuthCloudDataStore.getInstance().getUserProfile(new GetUserProfileCallback() {
            @Override
            public void onGetUserProfileSuccess(GetUserProfileResponse getUserProfileResponse) {
                iEditProfileActivityInteractorOutput.
                        onGetUserProfileSuccess(getUserProfileResponse.getUserProfile());
            }

            @Override
            public void onGetUserProfileFailure(int errorCode, String message) {
                Optional.doWhen(Utility.isUnauthorized(errorCode),
                        () -> getUserProfile(),
                        () -> iEditProfileActivityInteractorOutput
                                .onGetUserProfileFailure(errorCode, message));
            }
        });
    }

    @Override
    public void updateUserProfile(@NonNull UpdateUserProfileRequest updateUserProfileRequest) {
        UserAuthCloudDataStore.getInstance().updateUserProfile(updateUserProfileRequest, new UpdateUserProfileCallback() {
            @Override
            public void onUpdateUserProfileSuccess() {
                iEditProfileActivityInteractorOutput.onUpdateUserProfileSuccess();
            }

            @Override
            public void onUpdateUserProfileFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    updateUserProfile(updateUserProfileRequest);
                else
                    iEditProfileActivityInteractorOutput.onUpdateUserProfileFailure(errorCode, message);
            }
        });
    }

    @Override
    public void validateInfo(final @NonNull String mobileNumber,
                             final String userAddress,
                             final String userEmail,
                             final int userId) {
        UserAuthCloudDataStore.getInstance().validateInfo(new ValidateInfoCallback() {
            @Override
            public void onValidateInfoSuccess(ValidateInfo validateInfo) {
                iEditProfileActivityInteractorOutput.onValidateInfoSuccess(
                        mobileNumber, userAddress, userEmail,userId);
            }

            @Override
            public void onValidateInfoFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode)) {
                    validateInfo(mobileNumber, userAddress, userEmail, userId);
                } else {
                    iEditProfileActivityInteractorOutput.onValidateInfoFailure(errorCode, message);
                }
            }
        }, new ValidateInfoRequest(null, mobileNumber));
    }
}
