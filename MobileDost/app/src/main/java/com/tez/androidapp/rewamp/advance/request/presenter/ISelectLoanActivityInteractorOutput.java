package com.tez.androidapp.rewamp.advance.request.presenter;

import com.tez.androidapp.app.vertical.advance.loan.shared.limit.callbacks.LoanLimitCallback;
import com.tez.androidapp.rewamp.advance.request.entity.LoanDetail;

public interface ISelectLoanActivityInteractorOutput extends LoanLimitCallback {

    void onGetLoanDetailsSuccess(LoanDetail loanDetail);

    void onGetLoanDetailsFailure(int errorCode, String message);
}
