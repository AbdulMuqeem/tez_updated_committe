package com.tez.androidapp.commons.utils.textwatchers;

import android.content.Context;
import android.graphics.PorterDuff;
import android.widget.EditText;

import com.tez.androidapp.R;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.commons.widgets.TezImageView;



public class ImageChangerTextWatcher implements TezTextWatcher {
    private TezImageView appCompatImageView;
    private EditText tezEditTextView;


    public ImageChangerTextWatcher(TezImageView appCompatImageView, EditText tezEditTextView) {
        this.appCompatImageView = appCompatImageView;
        this.tezEditTextView = tezEditTextView;
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        Context context = MobileDostApplication.getAppContext();
        int color = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M ? context.getColor(R.color
                .colorGreen) : context.getResources().getColor(R.color.colorGreen);
        if (charSequence.length() > 0)
            appCompatImageView.getDrawable().mutate().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        else if (!tezEditTextView.isFocused())
            appCompatImageView.getDrawable().mutate().setColorFilter(null);
    }
}
