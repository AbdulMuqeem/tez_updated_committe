package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.request;

import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.ClaimedDocumentDetail;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.InsuranceAnswerDto;

import java.util.List;

/**
 * Created by FARHAN DHANANI on 6/29/2018.
 */
public class UploadClaimAnswerRequest {
    public static final String METHOD_NAME = "v1/insurance/claim";
    private Integer mobileUserInsurancePolicyId;
    private List<ClaimedDocumentDetail> documentList;
    private List<InsuranceAnswerDto> insuranceAnswerDtoList;
    private Integer insuranceClaimId;

    private Double lat;
    private Double lng;
    private Double amount;

    public static String getMethodName() {
        return METHOD_NAME;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Integer getMobileUserInsurancePolicyId() {
        return mobileUserInsurancePolicyId;
    }

    public void setMobileUserInsurancePolicyId(Integer mobileUserInsurancePolicyId) {
        this.mobileUserInsurancePolicyId = mobileUserInsurancePolicyId;
    }

    public List<ClaimedDocumentDetail> getDocumentList() {
        return documentList;
    }

    public void setDocumentList(List<ClaimedDocumentDetail> documentList) {
        this.documentList = documentList;
    }

    public List<InsuranceAnswerDto> getInsuranceAnswerDtoList() {
        return insuranceAnswerDtoList;
    }

    public void setInsuranceAnswerDtoList(List<InsuranceAnswerDto> insuranceAnswerDtoList) {
        this.insuranceAnswerDtoList = insuranceAnswerDtoList;
    }

    public Integer getInsuranceClaimId() {
        return insuranceClaimId;
    }

    public void setInsuranceClaimId(Integer insuranceClaimId) {
        this.insuranceClaimId = insuranceClaimId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

}
