package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.request.CommitteeCreateRequest;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface ICommitteeTermsCondActivityPresenter {

    void createCommittee(CommitteeCreateRequest committeeCreateRequest);
}
