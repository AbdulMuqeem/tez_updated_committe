package com.tez.androidapp.app.vertical.bima.add.beneficiary.callbacks;

public interface SetDefaultBeneficiaryCallback {

    void onSetDefaultBeneficiarySuccess();

    void onSetDefaultBeneficiaryFailure(int errorCode, String message);
}
