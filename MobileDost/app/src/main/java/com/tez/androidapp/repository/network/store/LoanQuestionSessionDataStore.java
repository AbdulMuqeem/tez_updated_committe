package com.tez.androidapp.repository.network.store;

import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.QuestionOptionsCallBack;
import com.tez.androidapp.app.general.feature.account.feature.profile.handlers.QuestionOptionsRH;
import com.tez.androidapp.app.vertical.advance.loan.shared.api.client.MobileDostLoanAPI;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by FARHAN DHANANI on 9/19/2018.
 */
public class LoanQuestionSessionDataStore extends BaseCloudDataStore {

    final MobileDostLoanAPI mobileDostLoanAPI;

    LoanQuestionSessionDataStore() {
        super();
        mobileDostLoanAPI = tezAPIBuilder.build().create(MobileDostLoanAPI.class);
    }

    public static LoanQuestionSessionDataStore getInstance() {
        return LazyHolder.INSTANCE;
    }

    @Override
    protected String getBaseUrl() {
        return super.getBaseUrl().replace("api/", "");
    }

    public void getQuestionOptions(String lang, String refName, QuestionOptionsCallBack questionOptionsCallBack) {
        mobileDostLoanAPI.getOptions(lang, refName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new QuestionOptionsRH(this, questionOptionsCallBack));
    }


    private static class LazyHolder {
        private static final LoanQuestionSessionDataStore INSTANCE = new LoanQuestionSessionDataStore();
    }
}
