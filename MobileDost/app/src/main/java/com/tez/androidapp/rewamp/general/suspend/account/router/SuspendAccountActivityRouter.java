package com.tez.androidapp.rewamp.general.suspend.account.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.suspend.account.view.SuspendAccountActivity;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public class SuspendAccountActivityRouter extends BaseActivityRouter {


    public static SuspendAccountActivityRouter createInstance() {
        return new SuspendAccountActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, SuspendAccountActivity.class);
    }
}
