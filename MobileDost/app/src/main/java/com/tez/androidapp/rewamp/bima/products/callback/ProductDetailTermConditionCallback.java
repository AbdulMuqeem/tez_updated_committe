package com.tez.androidapp.rewamp.bima.products.callback;

public interface ProductDetailTermConditionCallback {

    void onGetProductDetailTermConditionSuccess(String htmlTermCondition);

    void onGetProductDetailTermConditionFailure(int errorCode, String message);
}
