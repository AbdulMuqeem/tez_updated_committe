package com.tez.androidapp.rewamp.bima.products.viewmodel

import androidx.lifecycle.MutableLiveData
import com.tez.androidapp.app.vertical.bima.claims.callback.ClaimsListCallBack
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.Claim
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.ClaimListResponse
import com.tez.androidapp.commons.utils.app.Utility
import com.tez.androidapp.repository.network.store.BimaCloudDataStore
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Failure
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Loading
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Success
import com.tez.androidapp.rewamp.bima.claim.common.network.viewmodel.NetworkBoundViewModel


class ClaimsViewModel : NetworkBoundViewModel() {

    private var claimListLiveDate: MutableLiveData<List<Claim>>? = null

    fun getClaimListLiveData(): MutableLiveData<List<Claim>> {
        if (claimListLiveDate == null) {
            claimListLiveDate = MutableLiveData()
            getClaimList()
        }
        return claimListLiveDate!!
    }

    private fun getClaimList() {
        networkCallMutableLiveData.value = Loading

        BimaCloudDataStore.getInstance().getClaimList(object : ClaimsListCallBack {

            override fun onMyClaimsListSuccess(claimListResponse: ClaimListResponse) {
                claimListLiveDate!!.value = claimListResponse.claims
                networkCallMutableLiveData.value = Success
            }


            override fun onMyClaimsListFailure(errorCode: Int, message: String?) {
                if (Utility.isUnauthorized(errorCode))
                    getClaimList()
                else
                    networkCallMutableLiveData.value = Failure(errorCode)
            }

        })
    }
}