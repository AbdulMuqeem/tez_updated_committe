package com.tez.androidapp.rewamp.bima.claim.response.handler

import com.jakewharton.retrofit2.adapter.rxjava2.Result
import com.tez.androidapp.app.base.handlers.BaseRH
import com.tez.androidapp.repository.network.store.BaseCloudDataStore
import com.tez.androidapp.rewamp.bima.claim.callback.InitiateReviseClaimCallback
import com.tez.androidapp.rewamp.bima.claim.response.InitiateReviseClaimResponse

class InitiateReviseClaimRH(baseCloudDataStore: BaseCloudDataStore,
                            private val initiateReviseClaimCallback: InitiateReviseClaimCallback)
    : BaseRH<InitiateReviseClaimResponse>(baseCloudDataStore) {

    override fun onSuccess(value: Result<InitiateReviseClaimResponse>) {
        val response = value.response().body()
        response?.let {
            if (isErrorFree(it))
                initiateReviseClaimCallback.onInitiateReviseClaimSuccess(it)
            else
                onFailure(it.statusCode, it.errorDescription)
        } ?: sendDefaultFailure()
    }

    override fun onFailure(errorCode: Int, message: String?) {
        initiateReviseClaimCallback.onInitiateReviseClaimFailure(errorCode, message)
    }
}