package com.tez.androidapp.app.general.feature.account.feature.profile.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateProfileWithCnicCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.response.UpdateProfileWithCnicResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsRH;

public class UpdateProfileWithCnicRH extends DashboardCardsRH<UpdateProfileWithCnicResponse> {

    private final UpdateProfileWithCnicCallback updateProfileWithCnicCallback;

    public UpdateProfileWithCnicRH(BaseCloudDataStore baseCloudDataStore, UpdateProfileWithCnicCallback updateProfileWithCnicCallback) {
        super(baseCloudDataStore);
        this.updateProfileWithCnicCallback = updateProfileWithCnicCallback;
    }

    @Override
    protected void onSuccess(Result<UpdateProfileWithCnicResponse> value) {
        super.onSuccess(value);
        UpdateProfileWithCnicResponse updateProfileWithCnicResponse = value.response().body();
        if (updateProfileWithCnicResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
            updateProfileWithCnicCallback.onSuccessUpdateProfileWithCnicCallback();
        else
            onFailure(updateProfileWithCnicResponse.getStatusCode(), updateProfileWithCnicResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        updateProfileWithCnicCallback.onFailureUpdateProfileWithCnicCallback(errorCode, message);
    }
}
