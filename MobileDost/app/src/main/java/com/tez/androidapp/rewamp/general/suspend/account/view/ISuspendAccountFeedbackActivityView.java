package com.tez.androidapp.rewamp.general.suspend.account.view;

import com.tez.androidapp.app.base.ui.IBaseView;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public interface ISuspendAccountFeedbackActivityView extends IBaseView {

    void setBtSubmitEnabled(boolean enabled);

    void takeUserToIntroScreen();
}
