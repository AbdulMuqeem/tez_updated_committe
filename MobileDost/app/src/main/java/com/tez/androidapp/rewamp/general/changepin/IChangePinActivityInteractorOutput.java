package com.tez.androidapp.rewamp.general.changepin;

import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.ChangePinCallback;

/**
 * Created by VINOD KUMAR on 8/26/2019.
 */
public interface IChangePinActivityInteractorOutput extends ChangePinCallback {
}
