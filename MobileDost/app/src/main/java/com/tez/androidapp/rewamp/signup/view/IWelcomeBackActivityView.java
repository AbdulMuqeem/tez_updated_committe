package com.tez.androidapp.rewamp.signup.view;

import androidx.annotation.StringRes;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.models.network.DeviceInfo;

public interface IWelcomeBackActivityView extends IBaseView {

    void routeToDashboard();

    void askPermission(String pin);

    String userPushRoute();

    void makeCall();

    void startValidation(LocationAvailableCallback callback);

    void showUnexpectedErrorDialog();

    void showAppOutDatedDialog(@StringRes int message);

    DeviceInfo getDeviceInfo();

    void createSinchVerification(String mobileNumber, boolean callDeviceKey, String pin);

    void startChangeTemporaryPinActivity();

    void navigateToReactivateAccountActivity();
}
