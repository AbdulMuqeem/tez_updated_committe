package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto;

import retrofit2.http.Body;

public class MobileUserRejectedFeildsDto {
    Boolean isCnicRejected = false;
    Boolean isCnicExpiryDateRejected = false;
    Boolean isCnicDateOfIssueRejected = false;
    Boolean isFullNameRejected = false;
    Boolean isMotherMaidenNameRejected = false;
    Boolean isDateOfBirthRejected = false;
    Boolean isPlaceOfBirthRejected = false;
    Boolean isGenderRejected = false;
    Boolean isFatherNameRejected = false;
    Boolean isHusbandNameRejected = false;
    Boolean isMaritalStatusRejected = false;

    public Boolean getCnicRejected() {
        return isCnicRejected;
    }

    public Boolean getCnicExpiryDateRejected() {
        return isCnicExpiryDateRejected;
    }

    public Boolean getCnicDateOfIssueRejected() {
        return isCnicDateOfIssueRejected;
    }

    public Boolean getFullNameRejected() {
        return isFullNameRejected;
    }

    public Boolean getMotherMaidenNameRejected() {
        return isMotherMaidenNameRejected;
    }

    public Boolean getDateOfBirthRejected() {
        return isDateOfBirthRejected;
    }

    public Boolean getPlaceOfBirthRejected() {
        return isPlaceOfBirthRejected;
    }

    public Boolean getGenderRejected() {
        return isGenderRejected;
    }

    public Boolean getFatherNameRejected() {
        return isFatherNameRejected;
    }

    public Boolean getHusbandNameRejected() {
        return isHusbandNameRejected;
    }

    public Boolean getMaritalStatusRejected() {
        return isMaritalStatusRejected;
    }

    public void setMaritalStatusRejected(Boolean maritalStatusRejected) {
        isMaritalStatusRejected = maritalStatusRejected;
    }
}
