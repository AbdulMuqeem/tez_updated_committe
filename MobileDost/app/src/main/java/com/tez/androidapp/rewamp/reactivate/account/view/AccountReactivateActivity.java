package com.tez.androidapp.rewamp.reactivate.account.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.os.Bundle;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.ToolbarActivity;
import com.tez.androidapp.app.general.feature.playback.TezCountDownTimer;
import com.tez.androidapp.commons.receivers.SMSReceiver;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezPinView;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.reactivate.account.presenter.AccountReactivateActivityPresenter;
import com.tez.androidapp.rewamp.reactivate.account.presenter.IAccountReactivateActivityPresenter;

import net.tez.viewbinder.library.core.BindView;

public class AccountReactivateActivity extends ToolbarActivity
        implements IAccountReactivateActivityView, TezPinView.OnPinEnteredListener {

    @BindView(R.id.tvResendCode)
    private TezTextView tvResendCode;

    @BindView(R.id.pinViewOTP)
    private TezPinView pinViewOtp;

    private final IAccountReactivateActivityPresenter iAccountReactivateActivityPresenter;

    private final SMSReceiver smsReceiver = new SMSReceiver(this::onOtpReceived);

    @Override
    public void onAppStart(boolean inactiveTimeExceeded) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_reactivate);
        //this.tezToolbar.setToolbarTitle();
        init();
    }

    public AccountReactivateActivity() {
        iAccountReactivateActivityPresenter = new AccountReactivateActivityPresenter(this);
    }

    private void init() {
        startTimer();
        initOnClickListener();
    }

    private void initOnClickListener() {
        tvResendCode.setDoubleTapSafeOnClickListener(view ->
                this.iAccountReactivateActivityPresenter.resendOtp());
        pinViewOtp.setOnPinEnteredListener(this);
        registerSmsReceiver();
    }


    @Override
    public void setPinViewOtpEnabled(boolean enabled) {
        pinViewOtp.setEnabled(enabled);
    }

    @Override
    public void setTvResendCodeEnabled(boolean enabled) {
        tvResendCode.setEnabled(enabled);
    }

    @Override
    public void setPinViewOtpClearText() {
        pinViewOtp.clearText();
    }

    @Override
    public void startTimer() {
        int totalTime = 45 * 1000;
        int oneSecond = 1000;
        setTvResendCodeEnabled(false);
        new TezCountDownTimer(totalTime, oneSecond) {

            @Override
            public void onTick(long millisUntilFinished) {
                String second = "00:" + (int) millisUntilFinished / 1000;
                tvResendCode.setText(second);
            }

            @Override
            public void onFinish() {
                tvResendCode.setText(R.string.resend_code);
                setTvResendCodeEnabled(true);
            }
        }.start();
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void onPinEntered(@NonNull String pin) {
        unRegisterSmsReceiver();
        iAccountReactivateActivityPresenter.verifyOtp(pin);
    }

    private void onOtpReceived(String otp) {
        this.pinViewOtp.setText(otp);
    }

    @Override
    public void navigateToDashboard(){
        DashboardActivityRouter.createInstance().setDependenciesAndRoute(this);
        finish();
    }

    @Override
    public void registerSmsReceiver(){
        smsReceiver.register(this);
    }

    private void unRegisterSmsReceiver(){
        smsReceiver.unregister(this);
    }

    @Override
    protected String getScreenName() {
        return "AccountReactivateActivity";
    }
}
