package com.tez.androidapp.app.general.feature.account.feature.suspension.account.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.SuspendAccountVerifyOTPCallback;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;

/**
 * Created by Rehman Murad Ali on 8/22/2017.
 */

public class SuspendAccountVerifyOTPRH extends BaseRH<BaseResponse> {
    SuspendAccountVerifyOTPCallback suspendAccountVerifyOTPCallback;

    public SuspendAccountVerifyOTPRH(BaseCloudDataStore baseCloudDataStore, @Nullable SuspendAccountVerifyOTPCallback suspendAccountVerifyOTPCallback) {
        super(baseCloudDataStore);
        this.suspendAccountVerifyOTPCallback = suspendAccountVerifyOTPCallback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse baseResponse = value.response().body();
        if (isErrorFree(baseResponse)) suspendAccountVerifyOTPCallback.onSuspendAccountVerifyOTPSuccess(baseResponse);
        else onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        suspendAccountVerifyOTPCallback.onSuspendAccountVerifyOTPFailure(errorCode, message);
    }
}
