package com.tez.androidapp.rewamp.bima.claim.common.details.router

import android.content.Intent
import com.tez.androidapp.app.base.ui.activities.BaseActivity
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter
import com.tez.androidapp.rewamp.bima.claim.common.details.view.ClaimDetailsActivity

class ClaimDetailsActivityRouter : BaseActivityRouter() {

    fun setDependenciesAndRoute(from: BaseActivity,
                                claimId: Int) {
        val intent = Intent(from, ClaimDetailsActivity::class.java)
        intent.putExtra(CLAIM_ID, claimId)
        route(from, intent)
    }

    class Dependencies(intent: Intent) {
        val claimId = intent.getIntExtra(CLAIM_ID, -100)
    }

    companion object {
        private const val CLAIM_ID = "CLAIM_ID"

        fun getDependencies(intent: Intent) = Dependencies(intent)

        @JvmStatic
        fun createInstance(): ClaimDetailsActivityRouter = ClaimDetailsActivityRouter()
    }
}