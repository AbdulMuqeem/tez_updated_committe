package com.tez.androidapp.app.vertical.bima.claims.models;

import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;

/**
 * Created by VINOD KUMAR on 3/13/2019.
 */
public class DateChoiceInsuranceAnswerResponseDto extends InsuranceAnswerResponseDto {

    public DateChoiceInsuranceAnswerResponseDto(String response) {
        this.response = response;
    }

    @Override
    public void setAnswer(CommonAnswers insuranceAnswerDto) {
        String formattedDate = Utility.getFormattedDate(response, Constants.BACKEND_DATE_FORMAT, Constants.UI_DATE_FORMAT);
        insuranceAnswerDto.setResponse(formattedDate);
    }
}
