package com.tez.androidapp.rewamp.signup.verification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;

import androidx.annotation.StringRes;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.models.network.DeviceInfo;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.CustomerSupport;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;
import com.tez.androidapp.rewamp.general.changepin.router.ChangeTemporaryPinActivityRouter;
import com.tez.androidapp.rewamp.reactivate.account.router.AccountReactivateActivityRouter;
import com.tez.androidapp.rewamp.signup.presenter.IWelcomeBackActivityPresenter;
import com.tez.androidapp.rewamp.signup.presenter.WelcomeBackActivityPresenter;
import com.tez.androidapp.rewamp.signup.router.NewIntroActivityRouter;
import com.tez.androidapp.rewamp.signup.router.NumberVerificationActivityRouter;
import com.tez.androidapp.rewamp.signup.router.WelcomeBackActivityNumberVerificationRouter;
import com.tez.androidapp.rewamp.signup.router.WelcomeBackActivityRouter;
import com.tez.androidapp.rewamp.signup.router.WelcomeBackPermissionsActivityRouter;
import com.tez.androidapp.rewamp.signup.view.IWelcomeBackActivityView;
import com.tez.androidapp.rewamp.util.DialogUtil;

public class WelcomeBackActivityNumberVerification extends NumberVerificationActivity
        implements IWelcomeBackActivityView {

    private final IWelcomeBackActivityPresenter iWelcomeBackActivityPresenter;

    private final BroadcastReceiver isOnValidatePinActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setResultCode(1);
        }
    };

    public WelcomeBackActivityNumberVerification() {
        this.iWelcomeBackActivityPresenter
                = new WelcomeBackActivityPresenter(this);
    }

    private String getPin() {
        return getIntent().getStringExtra(WelcomeBackActivityNumberVerificationRouter.USER_PIN);
    }

    private boolean getCallForDeviceKey() {
        return getIntent().getBooleanExtra(WelcomeBackActivityNumberVerificationRouter.CALL_DEVICE_KEY,
                true);
    }

    @Override
    public void onVerified() {
        super.onVerified();
        Optional.ifPresent(getPin(), pin -> {
            iWelcomeBackActivityPresenter
                    .checkForVerifyOrLogin(getCallForDeviceKey(), getPin());
        }, this::showUnexpectedErrorDialog);
    }


    @Override
    public void onCountDownFinish() {
        super.onCountDownFinish();
        setEnableEtMobileNumber(false);
    }

    @Override
    public void routeToDashboard() {
        Optional.doWhen(!isUserSessionExpired(),
                ()->Optional.ifPresent(userPushRoute(),
                        this::onPushRoute,
                        ()-> DashboardActivityRouter.createInstance().setDependenciesAndRoute(this)));
        setResult(RESULT_OK);
        this.finish();
    }

    @Override
    protected void initializingView2() {
        this.tvEditNumber.setVisibility(View.GONE);
        super.initializingView2();
    }

    private boolean isUserSessionExpired() {
        return getIntent().getBooleanExtra(WelcomeBackActivityRouter.USER_SESSION_TIMEOUT, false);
    }

    @Override
    public String userPushRoute(){
        return getIntent().getStringExtra(PushNotificationConstants.KEY_ROUTE_TO);
    }

    @Override
    protected String getFlashCallLabel() {
        return "VERIFY-USER";
    }

    @Override
    public void onUserSessionTimeOut() {

    }

    protected String getMobileNumberFromIntent() {
        return getIntent().getStringExtra(NumberVerificationActivityRouter.MOBILE_NUMBER);
    }

    @Override
    public void askPermission(String pin) {
        WelcomeBackPermissionsActivityRouter.createInstance().setDependenciesAndRouteToForwardResult(this,
                getMobileNumberFromIntent(), getPin(), isUserSessionExpired(), userPushRoute());
        finish();
    }

    @Override
    public void makeCall() {
        CustomerSupport.openDialer(this);
        finish();
    }

    @Override
    public void startValidation(LocationAvailableCallback callback) {
        getCurrentLocation(callback);
    }


    @Override
    public void showUnexpectedErrorDialog() {
        this.showInformativeMessage(getString(R.string.string_something_unexpected_happened), (dialog, which) -> {
            NewIntroActivityRouter.createInstance().setDependenciesAndRoute(this, Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        });
    }

    @Override
    public void showAppOutDatedDialog(@StringRes int message) {
        DialogUtil.showActionableDialog(this,
                R.string.update_app,
                message,
                R.string.update,
                (dialog, which) -> {
                    if (which == DialogInterface.BUTTON_POSITIVE)
                        Utility.openPlayStoreForApp(this, this.getPackageName());
                });
    }


    @Override
    public DeviceInfo getDeviceInfo() {
        return Utility.getUserDeviceInfo(this);
    }

    @Override
    public void createSinchVerification(String mobileNumber, boolean callDeviceKey, String pin) {
        startNumberVerification();
    }

    @Override
    public void startChangeTemporaryPinActivity() {
        finish();
        ChangeTemporaryPinActivityRouter.createInstance().setDependenciesAndRoute(getHostActivity());
    }


    @Override
    public void navigateToReactivateAccountActivity() {
        finish();
        AccountReactivateActivityRouter.createInstance().setDependenciesAndRoute(this);
    }

    @Override
    public int getAudioId() {
        return 0;
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(isOnValidatePinActivity);
        } catch (IllegalArgumentException e){
            //ignore
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(isOnValidatePinActivity, new IntentFilter(Constants.BROAD_CAST_RECIEVER_ON_VALIDATE_PIN_ACTIVITY));
    }

    @Override
    protected String getScreenName() {
        return "WelcomeBackActivityNumberVerification";
    }
}
