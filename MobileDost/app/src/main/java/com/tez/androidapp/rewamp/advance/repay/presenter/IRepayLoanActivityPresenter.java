package com.tez.androidapp.rewamp.advance.repay.presenter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;
import com.tez.androidapp.commons.models.network.LoanDetails;

public interface IRepayLoanActivityPresenter {

    void getRepayReceipt(int loanId);

    void onClickRepayAdvance(@NonNull LoanDetails loanDetails, @Nullable Wallet wallet, double amount);
}
