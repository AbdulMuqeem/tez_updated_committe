package com.tez.androidapp.rewamp.profile.complete.interactor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.PersonalQuestionCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdatePersonalInfoCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.AnswerSelected;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.PersonalInfoOption;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Question;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdatePersonalInfoRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.repository.network.store.LoanAuthQuestionSessionDataStore;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.profile.complete.presenter.IPersonalInformationFragmentInteractorOutput;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;

public class PersonalInformationFragmentInteractor implements IPersonalInformationFragmentInteractor {

    private final IPersonalInformationFragmentInteractorOutput iPersonalInformationFragmentInteractorOutput;

    public PersonalInformationFragmentInteractor(IPersonalInformationFragmentInteractorOutput iPersonalInformationFragmentInteractorOutput) {
        this.iPersonalInformationFragmentInteractorOutput = iPersonalInformationFragmentInteractorOutput;
    }

    @Override
    public void getPersonalQuestions(String lang){
        LoanAuthQuestionSessionDataStore.getInstance().getPersonalQuestions(lang, new PersonalQuestionCallback() {
            @Override
            public void onPersonalQuestionSuccess(List<Question> data) {
                iPersonalInformationFragmentInteractorOutput.onPersonalQuestionSuccess(data);
            }

            @Override
            public void onPersonalQuestionFailure(int errorCode, String message) {
                Optional.doWhen(Utility.isUnauthorized(errorCode),
                        () -> getPersonalQuestions(lang),
                        ()-> iPersonalInformationFragmentInteractorOutput.onPersonalQuestionFailure(errorCode, message));
            }
        });
    }

    @Override
    public void updatePersonalInfo(@Nullable String file,
                                   @Nullable String questionRef, List<AnswerSelected> answerSelecteds){
        MultipartBody.Part imagePart = null;
        MultipartBody.Part selectedAnswers = null;
        if(file!=null && questionRef!=null) {
            File image = new File(file);
            imagePart = MultipartBody.Part.createFormData(questionRef, image.getName(),
                    MultipartBody.create(MediaType.parse(Utility.getMimeTypeFromLocalFileUri(file)), image));
        }
        selectedAnswers = MultipartBody.Part.createFormData(UpdatePersonalInfoRequest.PERSONAL_INFO,
                new Gson().toJson(new PersonalInfoOption(answerSelecteds)));
        UserAuthCloudDataStore.getInstance().updateUserPersonalInfo(imagePart, selectedAnswers, new UpdatePersonalInfoCallback() {
            @Override
            public void onUpdatePersonalInfoSuccess(boolean isProfileCompleted) {
                iPersonalInformationFragmentInteractorOutput.onUpdatePersonalInfoSuccess(isProfileCompleted);
            }

            @Override
            public void onUpdatePersonalInfoFailure(int errorCode, String message) {
                Optional.doWhen(Utility.isUnauthorized(errorCode),
                        ()->updatePersonalInfo(file, questionRef, answerSelecteds),
                        ()->iPersonalInformationFragmentInteractorOutput.onPersonalQuestionFailure(errorCode, message));
            }
        });
    }
}
