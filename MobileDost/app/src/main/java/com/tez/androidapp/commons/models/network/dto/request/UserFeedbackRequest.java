package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Rehman Murad Ali on 8/30/2017.
 */

public class UserFeedbackRequest extends BaseRequest {
    public static final String METHOD_NAME = "v1/user/feedback";
    Float rating;
    Integer loanId;
    String comment;

    public UserFeedbackRequest() {
    }

    public UserFeedbackRequest(Float rating, Integer loanId, String comment) {
        this.rating = rating;
        this.loanId = loanId;
        this.comment = comment;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "UserFeedbackRequest{" +
                "rating=" + rating +
                ", loanId=" + loanId +
                ", comment='" + comment + '\'' +
                '}';
    }
}
