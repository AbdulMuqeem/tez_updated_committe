package com.tez.androidapp.rewamp.bima.products.policy.router;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.request.router.LoanRequiredStepsActivityRouter;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.bima.products.policy.view.InsuranceRequiredStepsActivity;
import com.tez.androidapp.rewamp.bima.products.policy.view.PurchasePolicyActivity;

public class InsuranceRequiredStepsActivityRouter extends LoanRequiredStepsActivityRouter {

    public static InsuranceRequiredStepsActivityRouter createInstance() {
        return new InsuranceRequiredStepsActivityRouter();
    }

    @Override
    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, InsuranceRequiredStepsActivity.class);
    }
}
