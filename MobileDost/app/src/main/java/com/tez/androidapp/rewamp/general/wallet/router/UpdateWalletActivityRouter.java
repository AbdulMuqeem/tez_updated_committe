package com.tez.androidapp.rewamp.general.wallet.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.general.wallet.view.UpdateWalletActivity;

public class UpdateWalletActivityRouter extends AddWalletTermsAndConditionActivityRouter {

    public static final int REQUEST_CODE_UPDATE_WALLET = 12787;
    public static final int RESULT_DELETE = 1;

    public static UpdateWalletActivityRouter createInstance() {
        return new UpdateWalletActivityRouter();
    }

    @Override
    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, UpdateWalletActivity.class);
    }

    @Override
    protected int getRequestCode() {
        return REQUEST_CODE_UPDATE_WALLET;
    }
}
