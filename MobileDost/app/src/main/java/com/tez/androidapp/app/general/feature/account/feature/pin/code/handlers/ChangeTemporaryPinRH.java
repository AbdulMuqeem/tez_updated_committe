package com.tez.androidapp.app.general.feature.account.feature.pin.code.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.ChangeTemporaryPinCallback;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created  on 9/15/2017.
 */

public class ChangeTemporaryPinRH extends BaseRH<BaseResponse> {

    ChangeTemporaryPinCallback changeTemporaryPinCallback;

    public ChangeTemporaryPinRH(BaseCloudDataStore baseCloudDataStore, ChangeTemporaryPinCallback changeTemporaryPinCallback) {
        super(baseCloudDataStore);
        this.changeTemporaryPinCallback = changeTemporaryPinCallback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse baseResponse = value.response().body();
        if (baseResponse != null) {
            if (isErrorFree(baseResponse))
                changeTemporaryPinCallback.onChangeTemporaryPinSuccess();
            else
                onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
        } else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        changeTemporaryPinCallback.onChangeTemporaryPinFailure(errorCode, message);
    }
}
