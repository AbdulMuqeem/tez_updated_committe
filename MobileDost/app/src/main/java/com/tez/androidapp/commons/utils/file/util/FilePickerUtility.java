package com.tez.androidapp.commons.utils.file.util;

import android.Manifest;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.tez.androidapp.commons.utils.app.PermissionCheckUtil;

import java.util.List;


/**
 * Created by FARHAN DHANANI on 7/10/2018.
 */
public final class FilePickerUtility {


    private FilePickerUtility() {
    }

    public static void showChoiceDialog(@NonNull final AppCompatActivity activity,
                                        @NonNull final IFilePickerUtilityPermissionCallBack iFilePickerUtility) {
        getChoiceDialog(activity, iFilePickerUtility).setOnCancelListener(iFilePickerUtility).show();
    }

    private static AlertDialog.Builder getChoiceDialog(@NonNull final AppCompatActivity activity,
                                                       @NonNull final IFilePickerUtilityPermissionCallBack iFilePickerUtility) {
        final String[] items = {"Open Camera", "Select Document Image", "Select Document File"};
        AlertDialog.Builder build = new AlertDialog.Builder(activity);
        build.setItems(items, (dialog, which) -> handleOnClickListnerForDialog(activity, iFilePickerUtility, which));
        return build;
    }

    private static void handleOnClickListnerForDialog(@NonNull final AppCompatActivity activity,
                                                      @NonNull final IFilePickerUtilityPermissionCallBack iFilePickerUtility,
                                                      final int which) {
        switch (which) {
            case 0:
                onCameraClicked(activity, iFilePickerUtility);
                break;
            case 1:
                onPickPhotoClicked(activity, iFilePickerUtility);
                break;
            case 2:
                onPickDocClicked(activity, iFilePickerUtility);
                break;

            default:
                //Default will never be reached
                break;
        }
    }

    private static void onCameraClicked(@NonNull AppCompatActivity activity,
                                        @NonNull IFilePickerUtilityPermissionCallBack iFilePickerUtility) {
        final String[] cameraPermissions = {Manifest.permission.CAMERA};
        List<String> missingPermissions = PermissionCheckUtil.getInstance(activity).getMissingPermissionsForSignUp(cameraPermissions);
        if (missingPermissions.isEmpty()) {
            iFilePickerUtility.onCameraClicked();
        } else {
            Dexter.withActivity(activity).withPermissions(missingPermissions).withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport report) {
                    if (report.areAllPermissionsGranted())
                        iFilePickerUtility.onCameraClicked();
                    else
                        iFilePickerUtility.onPermissionsChecked(report);
                }

                @Override
                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                    iFilePickerUtility.onPermissionRationaleShouldBeShown(permissions, token);
                }
            }).check();
        }
    }

    private static void onPickPhotoClicked(@NonNull AppCompatActivity activity,
                                           @NonNull IFilePickerUtilityPermissionCallBack iFilePickerUtility) {
        final String[] filePermissions = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE};
        List<String> missingPermissions = PermissionCheckUtil.getInstance(activity).getMissingPermissionsForSignUp(filePermissions);
        if (missingPermissions.isEmpty()) {
            iFilePickerUtility.onPickPhotoClicked();
        } else {
            Dexter.withActivity(activity).withPermissions(missingPermissions).withListener(iFilePickerUtility).check();
        }
    }

    private static void onPickDocClicked(@NonNull AppCompatActivity activity,
                                         @NonNull IFilePickerUtilityPermissionCallBack iFilePickerUtility) {
        final String[] filePermissions = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE};
        List<String> missingPermissions = PermissionCheckUtil.getInstance(activity).getMissingPermissionsForSignUp(filePermissions);
        if (missingPermissions.isEmpty()) {
            iFilePickerUtility.onPickDocClicked();
        } else {
            Dexter.withActivity(activity).withPermissions(missingPermissions).withListener(iFilePickerUtility).check();
        }
    }
}
