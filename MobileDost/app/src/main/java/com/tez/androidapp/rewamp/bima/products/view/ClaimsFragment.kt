package com.tez.androidapp.rewamp.bima.products.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.tez.androidapp.R
import com.tez.androidapp.app.base.ui.fragments.BaseFragment
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.Claim
import com.tez.androidapp.rewamp.bima.claim.common.details.router.ClaimDetailsActivityRouter
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Failure
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Loading
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Success
import com.tez.androidapp.rewamp.bima.products.adapter.ClaimsAdapter
import com.tez.androidapp.rewamp.bima.products.viewmodel.ClaimsViewModel
import kotlinx.android.synthetic.main.claims_fragment.*

class ClaimsFragment : BaseFragment(), ClaimsAdapter.ClaimListener {

    private var baseView: View? = null
    private lateinit var callback: BimaCallback
    private val viewModel: ClaimsViewModel by activityViewModels()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = context as BimaCallback
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        baseView = baseView ?: inflater.inflate(R.layout.claims_fragment, container, false)
        return baseView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callback.setIllustrationVisibility(View.GONE)
        viewModel.networkCallMutableLiveData.observe(viewLifecycleOwner, {
            when (it) {
                is Loading -> {
                    shimmer.visibility = View.VISIBLE
                    clContent.visibility = View.GONE
                }
                is Failure -> {
                    shimmer.visibility = View.GONE
                    clContent.visibility = View.GONE
                    showError(it.errorCode)
                }
                is Success -> {
                    shimmer.visibility = View.GONE
                    clContent.visibility = View.VISIBLE
                }
            }
        })

        viewModel.getClaimListLiveData().observe(viewLifecycleOwner) {
            if (rvClaims.adapter == null) {
                rvClaims.adapter = ClaimsAdapter(it, this)
                layoutEmptyClaim.visibility = if (it.isEmpty()) View.VISIBLE else View.GONE
                clContent.visibility = if (it.isEmpty()) View.GONE else View.VISIBLE
            }

            callback.setIllustrationAndVisibility(R.drawable.ic_illustration_empty_claim,
                    if (it.isEmpty()) View.VISIBLE else View.GONE)
        }

        btLodgeClaim.setDoubleTapSafeOnClickListener { callback.showPoliciesFragment() }
    }

    companion object {
        fun newInstance(): ClaimsFragment = ClaimsFragment()
    }

    override fun onClickClaim(claim: Claim) {
        getBaseActivity {
            ClaimDetailsActivityRouter().setDependenciesAndRoute(it, claim.id)
        }
    }
}