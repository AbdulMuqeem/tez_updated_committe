package com.tez.androidapp.rewamp.util;

import com.tez.androidapp.rewamp.committee.model.CommitteePackageModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ahmad Izaz on 22-Nov-20
 **/
public class PackageListing {
    final static int MIN_INSTALLMENT_AMOUNT = 500;
    final static int MIN_TOTAL_AMOUNT = 1500;
    final static int MAX_TOTAL_AMOUNT = 25000;
    final static int MIN_MEMBERS = 3;
    final static int MAX_MEMBERS = 6;

    public static List<CommitteePackageModel> createInstallmentPackages(int totalAmount, int totalMembers) {
        List<CommitteePackageModel> packages = new ArrayList<>();
        if (isValid(totalAmount, totalMembers)) {
            //create Monthly
            int monthylInstallment = totalAmount / totalMembers;
            int duration = totalMembers;
            CommitteePackageModel monthlyPackage = new CommitteePackageModel(1, totalMembers, monthylInstallment);
            monthlyPackage.setSelectedAmount(totalAmount);
            monthlyPackage.setSelectedMembers(totalMembers);
            int biweeklyInstallment = monthylInstallment / 2;

            //create bi weekly
            CommitteePackageModel biweeklyPackage = new CommitteePackageModel(2, duration, biweeklyInstallment);
            biweeklyPackage.setSelectedAmount(totalAmount);
            biweeklyPackage.setSelectedMembers(totalMembers);
            if (monthlyPackage.getInstallmentAmount() >= MIN_INSTALLMENT_AMOUNT) {
                packages.add(monthlyPackage);
            }
            if (biweeklyPackage.getInstallmentAmount() >= MIN_INSTALLMENT_AMOUNT) {
//                packages.add(biweeklyPackage);
            }
        }

        return packages;
    }

    private static boolean isValid(int totalAmount, int totalMembers) {
        return totalAmount <= MAX_TOTAL_AMOUNT && totalAmount >= MIN_TOTAL_AMOUNT && totalMembers >= MIN_MEMBERS && totalMembers <= MAX_MEMBERS;
    }

}