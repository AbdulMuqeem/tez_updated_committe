package com.tez.androidapp.rewamp.general.suspend.account;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public interface FeedbackCallback {

    void onFeedbackSuccess();

    void onFeedbackFailure(int errorCode, String message);
}
