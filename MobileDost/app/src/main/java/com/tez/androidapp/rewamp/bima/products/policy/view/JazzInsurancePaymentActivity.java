package com.tez.androidapp.rewamp.bima.products.policy.view;

public class JazzInsurancePaymentActivity
        extends EasyPaisaInsurancePaymentActivity
        implements IEasyPaisaInsurancePaymentActivityView {


    private static final int JAZZ_PIN_LENGTH = 4;

    @Override
    protected int getPinLength() {
        return JAZZ_PIN_LENGTH;
    }

}
