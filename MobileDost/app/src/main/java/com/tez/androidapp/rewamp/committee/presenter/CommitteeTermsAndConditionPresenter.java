package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.interactor.CommitteeTermsCondActivityInteractor;
import com.tez.androidapp.rewamp.committee.request.CommitteeCreateRequest;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;
import com.tez.androidapp.rewamp.committee.view.ICommitteeTermsCondActivityView;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public class CommitteeTermsAndConditionPresenter implements ICommitteeTermsCondActivityPresenter, ICommitteeTermsCondActivityInteractorOutput {


    private final ICommitteeTermsCondActivityView iCommitteeTermsCondActivityView;
    private final CommitteeTermsCondActivityInteractor mCommitteeTermsCondActivityInteractor;

    public CommitteeTermsAndConditionPresenter(ICommitteeTermsCondActivityView iCommitteeTermsCondActivityView) {
        this.iCommitteeTermsCondActivityView = iCommitteeTermsCondActivityView;
        mCommitteeTermsCondActivityInteractor = new CommitteeTermsCondActivityInteractor(this);
    }

    @Override
    public void onCommitteeCreateSuccess(CommitteeCreateResponse committeeCreateResponse) {
        iCommitteeTermsCondActivityView.hideLoader();
        iCommitteeTermsCondActivityView.onCommitteeCreate();
    }

    @Override
    public void onCommitteeCreateFailed(int errorCode, String message) {
        iCommitteeTermsCondActivityView.hideLoader();
        iCommitteeTermsCondActivityView.showError(errorCode,
                (dialog, which) -> iCommitteeTermsCondActivityView.finishActivity());

    }

    @Override
    public void createCommittee(CommitteeCreateRequest committeeCreateRequest) {
        iCommitteeTermsCondActivityView.showLoader();
        mCommitteeTermsCondActivityInteractor.createCommittee(committeeCreateRequest);
    }
}
