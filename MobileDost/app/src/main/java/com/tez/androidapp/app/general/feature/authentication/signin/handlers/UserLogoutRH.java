package com.tez.androidapp.app.general.feature.authentication.signin.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.authentication.signin.callbacks.UserLogoutCallback;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created  on 4/7/2017.
 */

public class UserLogoutRH extends BaseRH<BaseResponse> {

    UserLogoutCallback userLogoutCallback;

    public UserLogoutRH(BaseCloudDataStore baseCloudDataStore, UserLogoutCallback userLogoutCallback) {
        super(baseCloudDataStore);
        this.userLogoutCallback = userLogoutCallback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse baseResponse = value.response().body();
        if (baseResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            userLogoutCallback.onUserLogoutSuccess();
        } else {
            onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
        }

    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        userLogoutCallback.onUserLogoutFailure(errorCode, message);

    }
}
