package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface CommitteeCompletionListener {

    void onCommitteeCreationSuccess(CommitteeCreateResponse committeeCreateResponse);

    void onCommitteeCreationFailure(int errorCode, String message);
}
