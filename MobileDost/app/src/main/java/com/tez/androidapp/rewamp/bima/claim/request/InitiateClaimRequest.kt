package com.tez.androidapp.rewamp.bima.claim.request

import com.tez.androidapp.app.base.request.BaseRequest

data class InitiateClaimRequest(val mobileUserInsurancePolicyId: Int) : BaseRequest() {

    companion object {
        const val METHOD_NAME = "v1/claim/initiate/{mobileUserInsurancePolicyId}"
        const val MOBILE_USER_INSURANCE_POLICY_ID = "mobileUserInsurancePolicyId"
    }
}