package com.tez.androidapp.rewamp.general.wallet.presenter;

import androidx.annotation.Nullable;

public interface IVerifyWalletOtpActivityPresenter {

    void addWallet(int serviceProviderId, String mobileAccountNumber, boolean isDefault, @Nullable Integer previousMobileUserAccountId);

    void verifyAddWalletOtp(int serviceProviderId, String mobileAccountNumber, String otp);

    void resendAddWalletOtp(int serviceProviderId, String mobileAccountNumber);
}
