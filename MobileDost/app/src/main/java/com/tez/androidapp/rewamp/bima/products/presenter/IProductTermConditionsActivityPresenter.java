package com.tez.androidapp.rewamp.bima.products.presenter;

public interface IProductTermConditionsActivityPresenter {

    void getProductTermConditions(int productId);

    void getProductDetailedTermCondition(int productId);
}
