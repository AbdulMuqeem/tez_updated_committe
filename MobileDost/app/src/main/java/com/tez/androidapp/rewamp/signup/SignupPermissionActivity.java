package com.tez.androidapp.rewamp.signup;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.commons.location.callbacks.LocationAvailableCallback;
import com.tez.androidapp.commons.models.network.dto.request.UserInfoRequest;
import com.tez.androidapp.commons.models.network.dto.request.UserSignUpRequest;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.rewamp.signup.presenter.ISignupPermissionActivityPresenter;
import com.tez.androidapp.rewamp.signup.presenter.SignupPermissionActivityPresenter;
import com.tez.androidapp.rewamp.signup.router.AccountAlreadyExistsActivityRouter;
import com.tez.androidapp.rewamp.signup.router.SignupActivityRouter;
import com.tez.androidapp.rewamp.signup.router.SignupNumberVerificationActivityRouter;
import com.tez.androidapp.rewamp.signup.router.SignupPermissionActivityRouter;
import com.tez.androidapp.rewamp.signup.view.ISignupPermissionActivityView;

import java.util.List;

public class SignupPermissionActivity extends PermissionActivity implements ISignupPermissionActivityView, LocationAvailableCallback {

    private final ISignupPermissionActivityPresenter iSignupPermissionActivityPresenter;

    public SignupPermissionActivity() {
        iSignupPermissionActivityPresenter = new SignupPermissionActivityPresenter(this);
    }

    @Override
    protected List<String> getRequiredPermissions() {
        return Utility.getSignupPermissions(this);
    }

    @Override
    protected void onAllPermissionsGranted() {
        super.onAllPermissionsGranted();
        getCurrentLocation(this);
    }

    @Override
    public void createSinchVerification() {
        SignupNumberVerificationActivityRouter
                .createInstance()
                .setDependenciesAndRoute(this, getMobileNumber(), getPin(), getSocialId(), getSocialType(), getReferralCode());
        finish();
    }

    @Override
    public void navigateToMobileNumberAlreadyExistActivity() {
        AccountAlreadyExistsActivityRouter.createInstance().setDependenciesAndRoute(this,
                getMobileNumber(),
                getSocialId(),
                getSocialType(),
                getReferralCode());
        finish();
    }

    private String getMobileNumber() {
        return getIntent().getStringExtra(SignupPermissionActivityRouter.MOBILE_NUMBER).replaceAll("-", "");
    }

    private String getPin() {
        return getIntent().getStringExtra(SignupPermissionActivityRouter.USER_PIN);
    }

    private String getReferralCode() {
        return getIntent().getStringExtra(SignupPermissionActivityRouter.REF_CODE);
    }

    private String getSocialId() {
        return getIntent().getStringExtra(SignupPermissionActivityRouter.SOCIAL_ID);
    }

    private int getSocialType() {
        return getIntent().getIntExtra(SignupPermissionActivityRouter.SOCIAL_TYPE,
                Utility.PrincipalType.MOBILE_NUMBER.getValue());
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }

    @Override
    public void onLocationRequestInitialized() {
        showTezLoader();
    }

    @Override
    public void onLocationAvailable(@NonNull Location location) {
        this.iSignupPermissionActivityPresenter.submitSignUpRequest(getSignupRequest(location));
    }

    @Override
    public void onLocationFailed(String message) {
        showTezLoader();
        this.iSignupPermissionActivityPresenter.submitSignUpRequest(getSignupRequest(null));
    }

    private UserSignUpRequest getSignupRequest(@Nullable Location location) {
        return Utility.createSignupRequest(this,
                location,
                getMobileNumber(),
                getReferralCode(),
                getSocialId(),
                getSocialType());
    }

    @Override
    protected String getScreenName() {
        return "SignupPermissionActivity";
    }
}
