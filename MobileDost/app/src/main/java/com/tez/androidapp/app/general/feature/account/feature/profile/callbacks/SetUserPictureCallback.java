package com.tez.androidapp.app.general.feature.account.feature.profile.callbacks;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created by Rehman Murad Ali on 8/25/2017.
 */

public interface SetUserPictureCallback {

    void setPictureOnSuccess(BaseResponse baseResponse);

    void setPictureOnFailure(int errCode, String message);

}
