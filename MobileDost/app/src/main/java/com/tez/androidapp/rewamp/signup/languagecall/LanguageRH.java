package com.tez.androidapp.rewamp.signup.languagecall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

/**
 * Created by VINOD KUMAR on 8/9/2019.
 */
public class LanguageRH extends BaseRH<BaseResponse> {

    @NonNull
    private LanguageCallback languageCallback;


    public LanguageRH(BaseCloudDataStore baseCloudDataStore, @NonNull LanguageCallback languageCallback) {
        super(baseCloudDataStore);
        this.languageCallback = languageCallback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse baseResponse = value.response().body();
        if (baseResponse != null) {
            if (baseResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR)
                this.languageCallback.onLanguageSuccess();
            else
                onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
        } else
            onFailure(ResponseStatusCode.DEFAULT.getCode(), "");
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.languageCallback.onLanguageFailure(errorCode, message);
    }
}
