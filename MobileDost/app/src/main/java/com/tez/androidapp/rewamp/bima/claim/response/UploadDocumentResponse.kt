package com.tez.androidapp.rewamp.bima.claim.response

import com.tez.androidapp.app.base.response.BaseResponse

class UploadDocumentResponse(val data: Int? = null) : BaseResponse()