package com.tez.androidapp.rewamp.general.purpose;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.widgets.TezImageView;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.tezrecycler.base.GenericRecyclerViewAdapter;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import java.util.List;

public class PurposeAdapter extends GenericRecyclerViewAdapter<Purpose,
        PurposeAdapter.PurposeListener,
        PurposeAdapter.PurposeViewHolder> {

    private int selectedPosition = -1;

    public PurposeAdapter(@NonNull List<Purpose> items, @Nullable PurposeListener listener) {
        super(items, listener);
    }

    public PurposeAdapter(@NonNull List<Purpose> items, int selectedPosition, @Nullable PurposeListener listener) {
        super(items, listener);
        this.selectedPosition = selectedPosition;
    }

    @NonNull
    @Override
    public PurposeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflate(parent.getContext(), R.layout.grid_item_purpose, parent);
        return new PurposeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PurposeViewHolder holder, int position) {
        holder.onBind(position, getItems(), getListener());
    }

    public int getSelectedPurposeId() {
        return selectedPosition != -1 ? getItems().get(selectedPosition).getId() : -1;
    }

    public interface PurposeListener extends BaseRecyclerViewListener {

        void onClickPurpose(int position, @NonNull Purpose purpose);
    }

    public class PurposeViewHolder extends BaseViewHolder<Purpose, PurposeListener> {

        @BindView(R.id.ivIcon)
        private TezImageView ivIcon;

        @BindView(R.id.tvTitle)
        private TezTextView tvTitle;

        @BindView(R.id.ivSelectedPurpose)
        private TezImageView ivSelectedPurpose;

        private PurposeViewHolder(View itemView) {
            super(itemView);
            ViewBinder.bind(this, itemView);
        }

        @Override
        public void onBind(int position, List<Purpose> items, @Nullable PurposeListener listener) {
            super.onBind(position, items, listener);
            Purpose item = items.get(position);
            ivIcon.setImageResource(item.getIconRes());
            tvTitle.setText(item.getTitleRes());
            ivSelectedPurpose.setVisibility(selectedPosition == position ? View.VISIBLE : View.GONE);
            itemView.setOnClickListener(v -> notify(position));

            if (listener != null && selectedPosition == position)
                listener.onClickPurpose(position, item);
        }

        private void notify(int position) {
            if (selectedPosition != position) {
                int oldPosition = selectedPosition;
                selectedPosition = position;

                notifyItemChanged(oldPosition);
                notifyItemChanged(position);
            }
        }
    }
}
