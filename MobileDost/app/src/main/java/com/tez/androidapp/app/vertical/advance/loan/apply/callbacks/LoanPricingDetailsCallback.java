package com.tez.androidapp.app.vertical.advance.loan.apply.callbacks;

import com.tez.androidapp.app.vertical.advance.loan.apply.models.network.PricingDetail;

import java.util.List;

public interface LoanPricingDetailsCallback {
    void onSuccessLoanPricingDetails(List<PricingDetail> loanPricingDetails, double latePaymentCharges);
    void onFailureLoanPricingDetials(int statusCode, String description);
}
