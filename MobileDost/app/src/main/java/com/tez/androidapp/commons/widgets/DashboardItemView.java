package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.TezDrawableHelper;

/**
 * Created by VINOD KUMAR on 6/17/2019.
 */
public class DashboardItemView extends TezCardView {

    private TezTextView tvTitle;

    public DashboardItemView(@NonNull Context context) {
        super(context);
    }

    public DashboardItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public DashboardItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    @NonNull
    @Override
    public String getLabel() {
        return tvTitle.getLabel();
    }

    private void init(@NonNull Context context, @NonNull AttributeSet attrs) {
        inflate(context, R.layout.view_dashboard_item, this);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.DashboardItemView);

        try {

            this.tvTitle = findViewById(R.id.tvTitle);

            TezImageView ivVerticalLine = findViewById(R.id.ivVerticalLine);
            TezImageView ivImage = findViewById(R.id.ivImage);
            TezTextView tvMessage = findViewById(R.id.tvMessage);

            Drawable image = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.DashboardItemView_item_image);
            String title = typedArray.getString(R.styleable.DashboardItemView_item_title);
            String message = typedArray.getString(R.styleable.DashboardItemView_item_message);
            boolean active = typedArray.getBoolean(R.styleable.DashboardItemView_item_active, true);

            if (image != null)
                ivImage.setImageDrawable(image);

            if (title != null)
                tvTitle.setText(title);

            if (message != null)
                tvMessage.setText(message);

            if (!active) {
                ivVerticalLine.setColorFilter(getColor(context, R.color.editTextBottomLineColorGrey));
                tvTitle.setTextColor(getColor(context, R.color.editTextBottomLineColorGrey));
                tvMessage.setTextColor(getColor(context, R.color.editTextBottomLineColorGrey));
                attachClickEffect(NO_EFFECT, this);
            }

        } finally {
            typedArray.recycle();
        }
    }
}
