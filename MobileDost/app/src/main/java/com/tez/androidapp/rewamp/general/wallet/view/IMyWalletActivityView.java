package com.tez.androidapp.rewamp.general.wallet.view;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;

public interface IMyWalletActivityView extends IBaseView {

    void setEmptyWalletsVisibility(boolean visible);

    void setNsvContentVisibility(int visibility);

    void showShimmer();

    void hideShimmer();

    void setWalletCardState(@NonNull Wallet wallet);

    void setLayoutAccountDetailVisibility(int visibility);

    void resetCards();
}
