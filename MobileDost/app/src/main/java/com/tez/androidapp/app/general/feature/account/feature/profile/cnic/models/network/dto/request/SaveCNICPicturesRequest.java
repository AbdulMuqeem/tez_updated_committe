package com.tez.androidapp.app.general.feature.account.feature.profile.cnic.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by VINOD KUMAR on 8/28/2018.
 */
public class SaveCNICPicturesRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/saveCnicPictures";

    public static final class Params {

        public static final String FRONT_CNIC_COPY = "frontCnicCopy";
        public static final String BACK_CNIC_COPY = "backCnicCopy";
        public static final String SELFIE = "selfie";

        private Params() {
        }
    }
}
