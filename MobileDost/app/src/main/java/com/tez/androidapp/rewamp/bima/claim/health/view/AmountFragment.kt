package com.tez.androidapp.rewamp.bima.claim.health.view

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.navigation.fragment.navArgs
import com.tez.androidapp.R
import com.tez.androidapp.commons.utils.app.Utility
import com.tez.androidapp.rewamp.bima.claim.health.base.BaseClaimStepFragment
import com.tez.androidapp.rewamp.util.AmountFormatterTextWatcher
import kotlinx.android.synthetic.main.fragment_amount.view.*

/**
 * Created by Vinod Kumar on 4/17/2020
 */
class AmountFragment : BaseClaimStepFragment() {

    private val args: AmountFragmentArgs by navArgs()

    override fun initView(baseView: View, savedInstanceState: Bundle?) {
        baseView.tllAmount.setOnClickListener { Utility.showKeyboard(baseView.etAmount) }
        baseView.etAmount.run {
            setCursorToLastPositionAlways()
            disableCopyPaste()
            addTextChangedListener(object : AmountFormatterTextWatcher(baseView.tvRs, this) {
                override fun onAmountEntered(amount: Double) {
                    super.onAmountEntered(amount)
                    setNavigationOnBtContinue(amount > 0)
                    claimSharedViewModel.amountLiveData.value = amount
                }
            })
        }
    }

    @get:LayoutRes
    override val layoutResId: Int
        get() = R.layout.fragment_amount

    override val stepNumber: Int
        get() = args.stepNumber
}