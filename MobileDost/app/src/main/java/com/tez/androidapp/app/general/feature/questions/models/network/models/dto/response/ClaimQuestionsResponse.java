package com.tez.androidapp.app.general.feature.questions.models.network.models.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.general.feature.questions.models.network.models.ClaimQuestion;

import java.util.List;

/**
 * Created  on 2/21/2017.
 */

public class ClaimQuestionsResponse extends BaseResponse {

    private List<ClaimQuestion> questions;

    public List<ClaimQuestion> getQuestions() {
        return questions;
    }
}
