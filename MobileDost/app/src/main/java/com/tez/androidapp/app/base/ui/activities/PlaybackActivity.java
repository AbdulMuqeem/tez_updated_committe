package com.tez.androidapp.app.base.ui.activities;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;

import com.tez.androidapp.R;
import com.tez.androidapp.app.general.feature.playback.AudioControlInterface;
import com.tez.androidapp.app.general.feature.playback.AudioPlayback;
import com.tez.androidapp.app.general.feature.playback.IAudioPlayback;
import com.tez.androidapp.commons.widgets.TezAudioTextView;

import net.tez.viewbinder.library.core.BindView;

/**
 * Created by Vinod Kumar on 15/5/2019.
 */

public abstract class PlaybackActivity extends ToolbarActivity implements IAudioPlayback {


    @BindView(R.id.textViewHeading)
    protected TezAudioTextView textViewHeading;

    @Nullable
    private AudioControlInterface audioPlayback;


    public PlaybackActivity() {
        if (isValidAudio())
            this.audioPlayback = AudioPlayback.create(getAudioId(), this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (audioPlayback != null)
            getLifecycle().addObserver(this.audioPlayback.getObserver());
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.setListenerOnAudioView();
    }

    @Override
    protected void onDestroy() {
        if (audioPlayback != null)
            getLifecycle().removeObserver(this.audioPlayback.getObserver());
        super.onDestroy();
    }

    @Nullable
    @Override
    public AudioControlInterface getAudioPlayback() {
        return audioPlayback;
    }

    @Nullable
    @Override
    public View getViewForAudio() {
        return textViewHeading;
    }

    @Override
    public void showTezLoader() {
        super.showTezLoader();
        if (audioPlayback != null) {
            audioPlayback.stopPlayer();
            audioPlayback.stopTimer();
        }
    }

    @Override
    public void dismissTezLoader() {
        super.dismissTezLoader();
        if (audioPlayback != null && getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED))
            audioPlayback.startTimer();

    }

    private boolean isValidAudio() {
        int audioId = getAudioId();
        return audioId != -1 && audioId != 0;
    }
}
