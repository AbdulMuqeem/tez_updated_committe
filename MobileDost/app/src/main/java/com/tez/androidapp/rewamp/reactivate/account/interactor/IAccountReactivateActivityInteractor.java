package com.tez.androidapp.rewamp.reactivate.account.interactor;

public interface IAccountReactivateActivityInteractor {
    void resendAccountReactivationOtp();

    void verifyAccountOtp(String otp);
}
