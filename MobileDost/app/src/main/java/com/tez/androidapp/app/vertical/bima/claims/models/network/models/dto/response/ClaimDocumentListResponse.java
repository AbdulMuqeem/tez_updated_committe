package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.Evidence;

import java.util.List;

/**
 * Created by FARHAN DHANANI on 6/14/2018.
 */
public class ClaimDocumentListResponse extends BaseResponse {

    private List<Evidence> documentList;

    public List<Evidence> getDocumentList() {
        return documentList;
    }
}
