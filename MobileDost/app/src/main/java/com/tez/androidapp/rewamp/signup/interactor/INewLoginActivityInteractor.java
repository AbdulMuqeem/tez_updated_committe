package com.tez.androidapp.rewamp.signup.interactor;

import com.tez.androidapp.commons.models.network.dto.request.UserLoginRequest;

public interface INewLoginActivityInteractor  {
    void callLogin(UserLoginRequest loginRequest);

    void resendAccountReactivationOtp();
}
