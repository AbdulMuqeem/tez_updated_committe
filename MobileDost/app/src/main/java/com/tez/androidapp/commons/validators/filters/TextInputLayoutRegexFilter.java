package com.tez.androidapp.commons.validators.filters;

import androidx.annotation.NonNull;
import android.widget.EditText;

import com.tez.androidapp.commons.validators.annotations.TextInputLayoutRegex;
import com.tez.androidapp.commons.widgets.TezTextInputLayout;

import net.tez.validator.library.filters.Filter;
import net.tez.validator.library.utils.TextUtil;

/**
 * Created by FARHAN DHANANI on 6/6/2019.
 */
public class TextInputLayoutRegexFilter implements Filter<TezTextInputLayout, TextInputLayoutRegex> {
    @Override
    public boolean isValidated(@NonNull TezTextInputLayout view, @NonNull TextInputLayoutRegex annotation) {
        EditText editText = view.getEditText();
        String text = editText == null ? "" : editText.getText() == null ? "" : editText.getText().toString();
        return TextUtil.isValidWithRegex(text, annotation.regex());
    }
}
