package com.tez.androidapp.rewamp.bima.products.model

import androidx.annotation.DrawableRes

data class Item(val id: Int? = null,
                @DrawableRes val icon: Int,
                val text: String)