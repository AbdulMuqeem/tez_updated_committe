package com.tez.androidapp.repository.network.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.models.network.AccessToken;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.repository.network.store.UserCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

import okhttp3.Response;

/**
 * Created  on 6/14/2017.
 */

public class UserTokenRefreshRH extends BaseRH<AccessToken> {
    Response response;
    BaseRH baseRH;

    public UserTokenRefreshRH(BaseCloudDataStore baseCloudDataStore) {
        super(baseCloudDataStore);
    }


    public UserTokenRefreshRH(UserCloudDataStore userCloudDataStore, BaseRH baseRH) {
        this(userCloudDataStore);
        this.baseRH = baseRH;
    }

    @Override
    protected void onSuccess(Result<AccessToken> value) {
        AccessToken accessToken = value.response().body();
        MDPreferenceManager.setAccessToken(accessToken);
        if (baseRH != null) {
            baseRH.onFailure(Constants.UNAUTHORIZED_ERROR_CODE, "Unauthorized");
        }
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if(baseRH != null) {
            baseRH.onFailure(errorCode,"Moaziz saarif, hum aap ko filhaal services farhaam nahi kar sakte. Baraye meherbani thori dair baad koshish karien.");
        }
    }
}
