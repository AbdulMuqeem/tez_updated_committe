package com.tez.androidapp.rewamp.advance.request.presenter;

import com.tez.androidapp.rewamp.advance.request.callback.LoanApplyCallback;
import com.tez.androidapp.rewamp.advance.request.callback.LoanRemainingStepsCallback;

public interface IAdvanceTermsAndConditionActivityInteractorOutput extends LoanRemainingStepsCallback, LoanApplyCallback {

}
