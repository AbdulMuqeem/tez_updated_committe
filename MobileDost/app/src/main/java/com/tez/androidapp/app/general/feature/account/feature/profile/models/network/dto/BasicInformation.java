package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto;

public class BasicInformation {
    private String fullName;
    private String gender;
    private String dateOfBirth;
    private String occupation;
    private String occupationDetail;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOccupationDetail() {
        return occupationDetail;
    }

    public void setOccupationDetail(String occupationDetail) {
        this.occupationDetail = occupationDetail;
    }
}
