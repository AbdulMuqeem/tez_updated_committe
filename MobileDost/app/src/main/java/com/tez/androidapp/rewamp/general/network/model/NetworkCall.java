package com.tez.androidapp.rewamp.general.network.model;

public class NetworkCall {

    private State state;

    private Integer errorCode;

    public NetworkCall(State state) {
        this.state = state;
    }

    public NetworkCall(State state, Integer errorCode) {
        this.state = state;
        this.errorCode = errorCode;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public State getState() {
        return state;
    }

    public enum State {
        LOADING,
        SUCCESS,
        FAILURE
    }
}
