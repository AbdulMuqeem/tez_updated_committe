package com.tez.androidapp.rewamp.committee.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezLoader;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.committee.response.CommitteeInstallmentPayResponse;
import com.tez.androidapp.rewamp.committee.router.MyCommitteeActivityRouter;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

import androidx.annotation.Nullable;

import static com.tez.androidapp.rewamp.committee.router.CommitteeContributeSummaryActivityRouter.INSTALLMENT_PAY_RESPONSE;

public class CommitteeContributeSummaryActivity extends BaseActivity implements IBaseView, DoubleTapSafeOnClickListener {


    @BindView(R.id.tvInstallmentValue)
    TezTextView tvInstallmentValue;

    @BindView(R.id.tvTotalAmountValue)
    TezTextView tvTotalAmountValue;

    @BindView(R.id.tvWalletValue)
    TezTextView tvWalletValue;

    @BindView(R.id.tvWalletNumberValue)
    TezTextView tvWalletNumberValue;

    @BindView(R.id.tvTransactionIdValue)
    TezTextView tvTransactionIdValue;

    @BindView(R.id.tvDateValue)
    TezTextView tvDateValue;

    @BindView(R.id.tvInstallmentAmountValue)
    TezTextView tvInstallmentAmountValue;

    @BindView(R.id.btDone)
    TezButton doneButton;

    private CommitteeInstallmentPayResponse committeeInstallmentPayResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_committee_contribute_summary);
            ViewBinder.bind(this);
            doneButton.setDoubleTapSafeOnClickListener(this);
            fetchExtras();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setValues() {
        try {

            tvInstallmentAmountValue.setText(committeeInstallmentPayResponse.getPaymentInfo().getInstallmentAmount() + "");
            tvTotalAmountValue.setText(committeeInstallmentPayResponse.getPaymentInfo().getCommitteeAmount() + "");
            tvWalletValue.setText(committeeInstallmentPayResponse.getPaymentInfo().getWalletProvider());
            tvWalletNumberValue.setText(committeeInstallmentPayResponse.getPaymentInfo().getWalletNumber());
            tvTransactionIdValue.setText(committeeInstallmentPayResponse.getPaymentInfo().getTransactionId());
            tvDateValue.setText(committeeInstallmentPayResponse.getPaymentInfo().getDate());
            tvInstallmentValue.setText(committeeInstallmentPayResponse.getPaymentInfo().getRound() + "");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fetchExtras() {
        try {
            Intent intent = getIntent();
            if (intent != null
                    && intent.getExtras() != null) {
                committeeInstallmentPayResponse = (CommitteeInstallmentPayResponse) intent.getExtras().getSerializable(INSTALLMENT_PAY_RESPONSE);
                setValues();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected String getScreenName() {
        return "CommitteeSummaryActivity";
    }

    @Override
    public void doubleTapSafeOnClick(View view) {
        try {
            switch (view.getId()) {
                case R.id.btDone:
                    MyCommitteeActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    protected TezLoader getTezLoader() {
        return createLoader();
    }


}
