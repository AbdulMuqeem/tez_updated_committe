package com.tez.androidapp.commons.models.network;

import java.io.Serializable;

/**
 * Created by VINOD KUMAR on 7/1/2019.
 */
public class MobileUserEditableFieldsDto implements Serializable {

    private Boolean cnicEditable;
    private Boolean dateOfBirthEditable;
    private Boolean cnicExpiryDateEditable;
    private Boolean cnicDateOfIssueEditable;
    private Boolean motherMaidenNameEditable;
    private Boolean fullNameEditable;
    private Boolean placeOfBirthEditable;

    public Boolean getCnicEditable() {
        return cnicEditable;
    }

    public Boolean getDateOfBirthEditable() {
        return dateOfBirthEditable;
    }

    public Boolean getCnicExpiryDateEditable() {
        return cnicExpiryDateEditable;
    }

    public Boolean getCnicDateOfIssueEditable() {
        return cnicDateOfIssueEditable;
    }

    public Boolean getMotherMaidenNameEditable() {
        return motherMaidenNameEditable;
    }

    public Boolean getFullNameEditable() {
        return fullNameEditable;
    }

    public Boolean getPlaceOfBirthEditable() {
        return placeOfBirthEditable;
    }

    public boolean isNoFieldEditable() {
        return !cnicEditable
                && !dateOfBirthEditable
                && !cnicExpiryDateEditable
                && !cnicDateOfIssueEditable
                && !motherMaidenNameEditable
                && !fullNameEditable
                && !placeOfBirthEditable;
    }
}
