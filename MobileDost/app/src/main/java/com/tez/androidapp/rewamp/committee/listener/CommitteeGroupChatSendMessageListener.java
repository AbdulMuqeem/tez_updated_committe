package com.tez.androidapp.rewamp.committee.listener;

import com.tez.androidapp.rewamp.committee.response.CommitteeSendMessageResponse;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface CommitteeGroupChatSendMessageListener {

    void onSendSuccess(CommitteeSendMessageResponse committeeSendMessageResponse);

    void onSendFailure(int errorCode, String message);

}
