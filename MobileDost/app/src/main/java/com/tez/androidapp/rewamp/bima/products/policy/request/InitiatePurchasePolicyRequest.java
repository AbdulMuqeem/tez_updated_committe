package com.tez.androidapp.rewamp.bima.products.policy.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.tez.androidapp.app.base.request.BaseRequest;

public class InitiatePurchasePolicyRequest extends BaseRequest implements Parcelable {

    public static final String METHOD_NAME = "v1/insurance/policy/purchase/initiate";

    private int productId;
    private int productPlanId;
    private double coverageAmount;
    private double finalPrice;
    private String expiryDate;
    private String activationDate;
    private int mobileAccountId;
    private String pin;

    public InitiatePurchasePolicyRequest(){

    }

    protected InitiatePurchasePolicyRequest(Parcel in) {
        productId = in.readInt();
        productPlanId = in.readInt();
        coverageAmount = in.readDouble();
        finalPrice = in.readDouble();
        expiryDate = in.readString();
        activationDate = in.readString();
        mobileAccountId = in.readInt();
        pin = in.readString();
    }

    public static final Creator<InitiatePurchasePolicyRequest> CREATOR = new Creator<InitiatePurchasePolicyRequest>() {
        @Override
        public InitiatePurchasePolicyRequest createFromParcel(Parcel in) {
            return new InitiatePurchasePolicyRequest(in);
        }

        @Override
        public InitiatePurchasePolicyRequest[] newArray(int size) {
            return new InitiatePurchasePolicyRequest[size];
        }
    };

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getProductPlanId() {
        return productPlanId;
    }

    public void setProductPlanId(int productPlanId) {
        this.productPlanId = productPlanId;
    }

    public double getCoverageAmount() {
        return coverageAmount;
    }

    public void setCoverageAmount(double coverageAmount) {
        this.coverageAmount = coverageAmount;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(String activationDate) {
        this.activationDate = activationDate;
    }

    public int getMobileAccountId() {
        return mobileAccountId;
    }

    public void setMobileAccountId(int mobileAccountId) {
        this.mobileAccountId = mobileAccountId;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(productId);
        dest.writeInt(productPlanId);
        dest.writeDouble(coverageAmount);
        dest.writeDouble(finalPrice);
        dest.writeString(expiryDate);
        dest.writeString(activationDate);
        dest.writeInt(mobileAccountId);
        dest.writeString(pin);
    }


}
