package com.tez.androidapp.rewamp.profile.complete.interactor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tez.androidapp.commons.utils.cnic.detection.model.RetumDataModel;

public interface ICompleteProfileCnicUploadActivityInteractor {

    void updateUserCnicPicture(boolean isAutoDetected,
                               int requestCode,
                               @NonNull String imageType,
                               @Nullable String imagePath,
                               @Nullable RetumDataModel retumDataModel);
}
