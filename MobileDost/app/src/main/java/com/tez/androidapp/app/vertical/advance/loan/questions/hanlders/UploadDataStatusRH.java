package com.tez.androidapp.app.vertical.advance.loan.questions.hanlders;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.vertical.advance.loan.questions.callbacks.UploadDataStatusCallback;
import com.tez.androidapp.app.vertical.advance.loan.questions.models.network.models.dto.response.UploadDataStatusResponse;
import com.tez.androidapp.commons.utils.app.Constants;
import net.tez.fragment.util.optional.Optional;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsRH;

/**
 * Created by FARHAN DHANANI on 10/15/2018.
 */
public class UploadDataStatusRH extends DashboardCardsRH<UploadDataStatusResponse> {

    private UploadDataStatusCallback loanQuestionStatusCallback;

    public UploadDataStatusRH(BaseCloudDataStore baseCloudDataStore,
                              @Nullable UploadDataStatusCallback loanQuestionsCallback) {
        super(baseCloudDataStore);
        this.loanQuestionStatusCallback = loanQuestionsCallback;
    }

    @Override
    protected void onSuccess(Result<UploadDataStatusResponse> value) {
        super.onSuccess(value);
        UploadDataStatusResponse loanQuestionsStatusResponse = value.response().body();
        Optional.ifPresent(loanQuestionsStatusResponse, loanQuestionStatusResponse -> {
            if (loanQuestionStatusResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
                Optional.ifPresent(loanQuestionStatusCallback, loanQuestionStatusCallback1 -> {
                    loanQuestionStatusCallback1.onUploadDataStatusSuccess(loanQuestionsStatusResponse);
                });
            } else
                onFailure(loanQuestionStatusResponse.getStatusCode(), loanQuestionsStatusResponse.getErrorDescription());
        });
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        Optional.ifPresent(loanQuestionStatusCallback, loanQuestionStatusCallback1 -> {
            loanQuestionStatusCallback1.onUploadDataStatusFailure(errorCode, message);
        });
    }
}
