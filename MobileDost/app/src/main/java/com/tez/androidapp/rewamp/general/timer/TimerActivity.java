package com.tez.androidapp.rewamp.general.timer;

import android.os.Bundle;
import android.view.View;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.general.feature.playback.TezCountDownTimer;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public abstract class TimerActivity extends BaseActivity implements ITimerActivityView {

    @BindView(R.id.tvMessage)
    protected TezTextView tvMessage;

    @BindView(R.id.tvPrecautionNote)
    protected TezTextView tvPrecautionNote;

    @BindView(R.id.tvTimer)
    private TezTextView tvTimer;

    private final TezCountDownTimer timer = new TezCountDownTimer(getTotalTime(), 1000 /* 1 second */) {

        @Override
        public void onTick(long millisUntilFinished) {

            long secs = (long) Math.ceil((double) millisUntilFinished / 1000);
            long secondsRemaining = secs % 60;
            long minutes = secs / 60;
            String timerText = Utility.getTwoDigitInteger((int) minutes)
                    + Utility.getStringFromResource(R.string.string_colon)
                    + Utility.getTwoDigitInteger((int) secondsRemaining);
            tvTimer.setText(timerText);
        }

        @Override
        public void onFinish() {
            TimerActivity.this.onTimerFinish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        ViewBinder.bind(this);
    }

    @Override
    public void startTimer() {
        tvTimer.setVisibility(View.VISIBLE);
        timer.start();
    }

    @Override
    public void stopTimer() {
        tvTimer.setVisibility(View.GONE);
        timer.cancel();
    }

    protected abstract void onTimerFinish();

    protected abstract long getTotalTime();
}
