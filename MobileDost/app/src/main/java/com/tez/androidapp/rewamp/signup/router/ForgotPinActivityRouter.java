package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;
import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.ForgotPinActivity;

/**
 * Created by VINOD KUMAR on 7/31/2019.
 */
public class ForgotPinActivityRouter extends BaseActivityRouter {

    public static ForgotPinActivityRouter createInstance() {
        return new ForgotPinActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, ForgotPinActivity.class);
    }
}
