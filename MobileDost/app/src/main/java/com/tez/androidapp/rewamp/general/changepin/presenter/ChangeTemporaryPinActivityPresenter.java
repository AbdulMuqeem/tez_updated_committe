package com.tez.androidapp.rewamp.general.changepin.presenter;

import androidx.annotation.NonNull;

import com.tez.androidapp.rewamp.general.changepin.interactor.ChangeTemporaryPinActivityInteractor;
import com.tez.androidapp.rewamp.general.changepin.interactor.IChangeTemporaryPinActivityInteractor;
import com.tez.androidapp.rewamp.general.changepin.view.IChangeTemporaryPinActivityView;

public class ChangeTemporaryPinActivityPresenter implements IChangeTemporaryPinActivityPresenter, IChangeTemporaryPinActivityInteractorOutput {

    private final IChangeTemporaryPinActivityView iChangeTemporaryPinActivityView;
    private final IChangeTemporaryPinActivityInteractor iChangeTemporaryPinActivityInteractor;

    public ChangeTemporaryPinActivityPresenter(IChangeTemporaryPinActivityView iChangeTemporaryPinActivityView) {
        this.iChangeTemporaryPinActivityView = iChangeTemporaryPinActivityView;
        iChangeTemporaryPinActivityInteractor = new ChangeTemporaryPinActivityInteractor(this);
    }

    @Override
    public void changeTemporaryPin(@NonNull String pin) {
        iChangeTemporaryPinActivityView.showTezLoader();
        iChangeTemporaryPinActivityInteractor.changeTemporaryPin(pin);
    }

    @Override
    public void onChangeTemporaryPinSuccess() {
        iChangeTemporaryPinActivityView.onTemporaryPinChangedSuccessfully();
    }

    @Override
    public void onChangeTemporaryPinFailure(int errorCode, String message) {
        iChangeTemporaryPinActivityView.dismissTezLoader();
        iChangeTemporaryPinActivityView.showError(errorCode);
    }
}
