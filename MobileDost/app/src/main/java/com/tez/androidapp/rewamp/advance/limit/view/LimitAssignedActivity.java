package com.tez.androidapp.rewamp.advance.limit.view;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.widgets.TezButton;
import com.tez.androidapp.commons.widgets.TezConstraintLayout;
import com.tez.androidapp.commons.widgets.TezLinearLayout;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.advance.limit.presenter.ILimitAssignedActivityPresenter;
import com.tez.androidapp.rewamp.advance.limit.presenter.LimitAssignedActivityPresenter;
import com.tez.androidapp.rewamp.advance.request.router.SelectLoanActivityRouter;
import com.tez.androidapp.rewamp.dashboard.router.DashboardActivityRouter;

import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class LimitAssignedActivity extends BaseActivity implements ILimitAssignedActivityView {

    private final ILimitAssignedActivityPresenter iLimitAssignedActivityPresenter;

    @BindView(R.id.tvClock)
    private TezTextView tvClock;

    @BindView(R.id.btContinue)
    private TezButton btContinue;

    @BindView(R.id.clContent)
    private TezConstraintLayout clContent;

    @BindView(R.id.shimmer)
    private TezLinearLayout shimmer;

    public LimitAssignedActivity() {
        iLimitAssignedActivityPresenter = new LimitAssignedActivityPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_limit_assigned);
        ViewBinder.bind(this);
        init();

    }

    private void init() {
        iLimitAssignedActivityPresenter.getLoanLimit();
    }

    @Override
    public void setTvClockText(String text) {
        this.tvClock.setText(text);
    }

    @Override
    public void setClickListenerToRouteToSelectLoanActivity() {
        this.btContinue.setDoubleTapSafeOnClickListener(view -> {
            SelectLoanActivityRouter.createInstance().setDependenciesAndRoute(this);
            finish();
        });
    }

    @Override
    public void routeToDashboard() {
        DashboardActivityRouter.createInstance().setDependenciesAndRouteWithoutNewTask(this);
    }

    @Override
    public void setClContentVisibility(int visibility) {
        this.clContent.setVisibility(visibility);
    }

    @Override
    public void showTezLoader() {
        shimmer.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissTezLoader() {
        shimmer.setVisibility(View.GONE);
    }

    @Override
    protected String getScreenName() {
        return "LimitAssignedActivity";
    }
}