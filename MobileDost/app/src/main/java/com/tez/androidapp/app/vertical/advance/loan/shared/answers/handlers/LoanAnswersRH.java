package com.tez.androidapp.app.vertical.advance.loan.shared.answers.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.vertical.advance.loan.shared.answers.callbacks.LoanAnswersCallback;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created  on 2/22/2017.
 */

public class LoanAnswersRH extends BaseRH<BaseResponse> {

    LoanAnswersCallback loanAnswersCallback;

    public LoanAnswersRH(BaseCloudDataStore baseCloudDataStore, LoanAnswersCallback loanAnswersCallback) {
        super(baseCloudDataStore);
        this.loanAnswersCallback = loanAnswersCallback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse response = value.response().body();
        if (loanAnswersCallback != null) {
            if (response.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) loanAnswersCallback.onLoanAnswersSuccess();
            else onFailure(response.getStatusCode(), response.getErrorDescription());
        }
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (loanAnswersCallback != null) loanAnswersCallback.onLoanAnswersFailure(errorCode, message);
    }
}
