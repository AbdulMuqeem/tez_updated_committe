package com.tez.androidapp.rewamp.general.feedback.interactor;

import com.tez.androidapp.rewamp.general.feedback.callback.LoanFeedbackCallback;

public interface ILoanFeedbackActivityInteractorOutput extends LoanFeedbackCallback {
}
