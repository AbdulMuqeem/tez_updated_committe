package com.tez.androidapp.rewamp.signup.presenter;

import com.tez.androidapp.app.general.feature.account.feature.pin.code.callbacks.UserLoginCallback;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.UserReActivateAccountResendOTPCallback;

public interface INewLoginActivityInteractorOutput
        extends UserLoginCallback, UserReActivateAccountResendOTPCallback {
}
