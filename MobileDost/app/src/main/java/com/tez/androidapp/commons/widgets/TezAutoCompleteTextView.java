package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;

import net.tez.fragment.util.base.IBaseWidget;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

/**
 * Created by VINOD KUMAR on 2/15/2019.
 */
public class TezAutoCompleteTextView extends AppCompatAutoCompleteTextView implements IBaseWidget {

    public TezAutoCompleteTextView(Context context) {
        super(context);
    }

    public TezAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TezAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setDoubleTapSafeOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        super.setOnClickListener(doubleTapSafeOnClickListener);
    }


    public String getValueText() {
        Editable editable = getText();
        if (editable != null)
            return editable.toString().trim();
        return null;
    }
}
