package com.tez.androidapp.commons.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.google.android.material.tabs.TabLayout;

/**
 * Created by VINOD KUMAR on 12/7/2018.
 */
public class TezTabLayout extends TabLayout {
    private Typeface mTypeface;

    public TezTabLayout(Context context) {
        super(context);
        init();
    }

    public TezTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TezTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        //  mTypeface = Typeface.createFromAsset(getContext().getAssets(), Constants.PATH_TO_EXO_REGULAR_FONT);
        this.setTabRippleColor(null);
    }

    /*@Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        final ViewGroup tabStrip = (ViewGroup) getChildAt(0);
        final int tabCount = tabStrip.getChildCount();
        ViewGroup tabView;
        int tabChildCount;
        View tabViewChild;

        for (int i = 0; i < tabCount; i++) {
            tabView = (ViewGroup) tabStrip.getChildAt(i);
            tabChildCount = tabView.getChildCount();
            for (int j = 0; j < tabChildCount; j++) {
                tabViewChild = tabView.getChildAt(j);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(mTypeface, Typeface.BOLD);
                }
            }
        }
    }*/
}