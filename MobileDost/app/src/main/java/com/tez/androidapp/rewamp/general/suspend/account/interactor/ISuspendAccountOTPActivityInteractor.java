package com.tez.androidapp.rewamp.general.suspend.account.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public interface ISuspendAccountOTPActivityInteractor extends IBaseInteractor {

    void resendCode();

    void suspendAccount();

    void suspendAccountVerifyOtp(@NonNull String otp);
}
