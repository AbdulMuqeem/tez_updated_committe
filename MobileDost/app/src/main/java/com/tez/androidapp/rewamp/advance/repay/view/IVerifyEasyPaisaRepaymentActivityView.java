package com.tez.androidapp.rewamp.advance.repay.view;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.commons.models.network.LoanDetails;

public interface IVerifyEasyPaisaRepaymentActivityView extends IBaseView {

    void setPinViewOtpClearText();

    void setPinViewOtpEnabled(boolean enabled);

    void navigateToDashboardWithNewStack();

    String getFormattedErrorMessage(@StringRes int message);

    void routeToRepaymentSuccessFullActivity(@NonNull LoanDetails loanDetails);
}
