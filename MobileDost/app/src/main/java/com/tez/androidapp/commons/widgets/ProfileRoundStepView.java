package com.tez.androidapp.commons.widgets;

import android.content.Context;

import androidx.annotation.FontRes;
import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;

import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.tez.androidapp.R;

/**
 * Created by VINOD KUMAR on 7/24/2019.
 */
public class ProfileRoundStepView extends TezConstraintLayout {

    public ProfileRoundStepView(Context context) {
        super(context);
    }

    public ProfileRoundStepView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ProfileRoundStepView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(@NonNull Context context, AttributeSet attrs) {
        this.setBackgroundColor(getColor(context, R.color.transparent));
        inflate(context, R.layout.round_step, this);
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ProfileRoundStepView);
            try {
                int step = typedArray.getInt(R.styleable.ProfileRoundStepView_step, 0);
                setStep(step == 0 ? Steps.USER_IS_FILLING_CNIC_DETAILS : Steps.USER_IS_FILLING_PERSONAL_DETAILS);
            } finally {
                typedArray.recycle();
            }
        }

    }

    public void setStep(@NonNull Steps step) {

        switch (step) {

            case USER_IS_FILLING_CNIC_DETAILS:
                userIsFillingCnic();
                break;

            case USER_IS_FILLING_PERSONAL_DETAILS:
                userIsFillingPersonalDetails();
                break;
        }
    }

    private void userIsFillingCnic() {
        TezTextView tvNumber = findViewById(R.id.tvStepNumberCnic);
        TezTextView tvName = findViewById(R.id.tvStepCnicName);
        TezImageView ivIcon = findViewById(R.id.ivStepCnicIcon);
        View viewStepSeparator = findViewById(R.id.viewStepSeparator1);
        viewStepSeparator.setBackgroundColor(getColor(getContext(), R.color.roundStepSeparator));
        int colorWhite = getColor(getContext(), R.color.buttonTextColorWhite);
        int colorGreen = getColor(getContext(), R.color.textViewTextColorGreen);
        tvNumber.setTextColor(colorWhite);
        tvNumber.setBackgroundResource(R.drawable.circle_green_filled);
        tvName.setTextColor(colorGreen);
        ivIcon.setColorFilter(colorGreen);
        changeTvNameFont(tvName, R.font.barlow_semi_bold);
        unFillPersonalDetails();
    }

    private void unFillPersonalDetails() {
        TezTextView tvNumber = findViewById(R.id.tvStepNumberPersonal);
        TezTextView tvName = findViewById(R.id.tvStepPersonalName);
        TezImageView ivIcon = findViewById(R.id.ivStepPersonalIcon);
        int colorBlack = getColor(getContext(), R.color.textViewTextColorBlack);
        int colorWhite = getColor(getContext(), R.color.buttonTextColorWhite);
        tvNumber.setTextColor(colorWhite);
        tvNumber.setBackgroundResource(R.drawable.round_bg_grey);
        tvName.setTextColor(colorBlack);
        changeTvNameFont(tvName, R.font.barlow_medium);
        ivIcon.setColorFilter(getColor(getContext(), R.color.editTextBottomLineColorGrey));
    }

    private void changeTvNameFont(TezTextView tvName, @FontRes int font) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
            tvName.setTypeface(ResourcesCompat.getFont(getContext(), font));
        else
            tvName.setTypeface(getResources().getFont(font));
    }

    private void userIsFillingPersonalDetails() {
        fillCnic();
        TezTextView tvNumber = findViewById(R.id.tvStepNumberPersonal);
        TezTextView tvName = findViewById(R.id.tvStepPersonalName);
        TezImageView ivIcon = findViewById(R.id.ivStepPersonalIcon);
        View viewStepSeparator = findViewById(R.id.viewStepSeparator1);
        int colorGreen = getColor(getContext(), R.color.textViewTextColorGreen);
        int colorWhite = getColor(getContext(), R.color.buttonTextColorWhite);
        viewStepSeparator.setBackgroundColor(colorGreen);
        tvNumber.setTextColor(colorWhite);
        tvNumber.setBackgroundResource(R.drawable.circle_green_filled);
        tvName.setTextColor(colorGreen);
        changeTvNameFont(tvName, R.font.barlow_semi_bold);
        ivIcon.setColorFilter(colorGreen);
    }

    private void fillCnic() {
        TezTextView tvNumber = findViewById(R.id.tvStepNumberCnic);
        TezTextView tvName = findViewById(R.id.tvStepCnicName);
        TezImageView ivIcon = findViewById(R.id.ivStepCnicIcon);
        tvNumber.setTextColor(getColor(getContext(), R.color.screen_background));
        tvNumber.setBackgroundResource(R.drawable.round_bg_grey);
        tvName.setTextColor(getColor(getContext(), R.color.textViewTextColorBlack));
        changeTvNameFont(tvName, R.font.barlow_medium);
        ivIcon.setColorFilter(getColor(getContext(), R.color.editTextBottomLineColorGrey));
    }


    public enum Steps {

        USER_IS_FILLING_CNIC_DETAILS,
        USER_IS_FILLING_PERSONAL_DETAILS
    }

}
