package com.tez.androidapp.app.general.feature.wait.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.wait.callbacks.LoanStatusCallback;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.app.general.feature.wait.models.network.dto.response.LoanStatusResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;

/**
 * Created  on 4/6/2017.
 */

public class LoanStatusRH extends BaseRH<LoanStatusResponse> {

    LoanStatusCallback loanStatusCallback;

    public LoanStatusRH(BaseCloudDataStore baseCloudDataStore, LoanStatusCallback loanStatusCallback) {
        super(baseCloudDataStore);
        this.loanStatusCallback = loanStatusCallback;
    }

    @Override
    protected void onSuccess(Result<LoanStatusResponse> value) {
        LoanStatusResponse loanStatusResponse = value.response().body();
        if (loanStatusResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            loanStatusCallback.onLoanStatusSuccess(loanStatusResponse);
        } else {
            onFailure(loanStatusResponse.getStatusCode(), loanStatusResponse.getErrorDescription());
        }

    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        loanStatusCallback.onLoanStatusFailure(errorCode, message);
    }
}
