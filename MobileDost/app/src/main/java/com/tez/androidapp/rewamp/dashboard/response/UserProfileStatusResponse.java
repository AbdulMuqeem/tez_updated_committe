package com.tez.androidapp.rewamp.dashboard.response;

import com.tez.androidapp.app.base.response.BaseResponse;

public class UserProfileStatusResponse extends BaseResponse {

    private String profileStatus;

    public String getProfileStatus() {
        return profileStatus;
    }
}
