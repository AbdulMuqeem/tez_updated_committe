package com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.models.network.LoanDetails;

/**
 * Created  on 8/29/2017.
 */

public class RepayReceiptResponse extends BaseResponse {
    LoanDetails loanDetails;

    public LoanDetails getLoanDetails() {
        return loanDetails;
    }

    public void setLoanDetails(LoanDetails loanDetails) {
        this.loanDetails = loanDetails;
    }

    @Override
    public String toString() {
        return "RepayReceiptResponse{" +
                "loanDetails=" + loanDetails +
                '}';
    }
}
