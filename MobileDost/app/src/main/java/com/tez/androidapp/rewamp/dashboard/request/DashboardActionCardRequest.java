package com.tez.androidapp.rewamp.dashboard.request;

import com.tez.androidapp.app.base.request.BaseRequest;

public class DashboardActionCardRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/dashboard/card/action";
}
