package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;
import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.Create4DigitPin;

/**
 * Created by VINOD KUMAR on 7/31/2019.
 */
public class Create4DigitPinActivityRouter extends BaseActivityRouter {

    public static Create4DigitPinActivityRouter createInstance() {
        return new Create4DigitPinActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, Create4DigitPin.class);
    }
}
