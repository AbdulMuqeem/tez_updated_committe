package com.tez.androidapp.rewamp.bima.claim.callback

import com.tez.androidapp.app.base.response.BaseResponse

interface InitiateClaimCallback {

    fun onSuccessInitiateClaim(response: BaseResponse)

    fun onFailureInitiateClaim(errorCode: Int, message: String?)
}