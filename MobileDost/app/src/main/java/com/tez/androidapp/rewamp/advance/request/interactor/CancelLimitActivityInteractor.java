package com.tez.androidapp.rewamp.advance.request.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.callbacks.CancelLoanLimitCallback;
import com.tez.androidapp.app.vertical.advance.loan.cancel.limit.models.network.dto.request.CancelLoanLimitRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.LoanCloudDataStore;
import com.tez.androidapp.rewamp.advance.request.presenter.ICancelLimitActivityInteractorOutput;

public class CancelLimitActivityInteractor implements ICancelLimitActivityInteractor {

    private final ICancelLimitActivityInteractorOutput iCancelLimitActivityInteractorOutput;

    public CancelLimitActivityInteractor(ICancelLimitActivityInteractorOutput iCancelLimitActivityInteractorOutput) {
        this.iCancelLimitActivityInteractorOutput = iCancelLimitActivityInteractorOutput;
    }

    @Override
    public void cancelLoanLimit(@NonNull CancelLoanLimitRequest request) {
        LoanCloudDataStore.getInstance().cancelLoanLimit(request, new CancelLoanLimitCallback() {
            @Override
            public void onCancelLoanLimitSuccess(BaseResponse response) {
                iCancelLimitActivityInteractorOutput.onCancelLoanLimitSuccess(response);
            }

            @Override
            public void onCancelLoanLimitFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    cancelLoanLimit(request);
                else
                    iCancelLimitActivityInteractorOutput.onCancelLoanLimitFailure(errorCode, message);
            }
        });
    }
}
