package com.tez.androidapp.rewamp.reactivate.account.presenter;

import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.UserReActivateAccountOTPCallback;
import com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks.UserReActivateAccountResendOTPCallback;

public interface IAccountReactivateActivityInteractorOutput extends
        UserReActivateAccountResendOTPCallback, UserReActivateAccountOTPCallback {
}
