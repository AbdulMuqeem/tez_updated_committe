package com.tez.androidapp.rewamp.general.invite.friend.interactor;

import com.tez.androidapp.app.general.feature.inivite.user.callback.GetReferralCodeCallback;
import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.general.invite.friend.presenter.IInviteFriendActivityInteractorOutput;

public class InviteFriendActivityInteractor
        implements IInviteFriendActivityInteractor, GetReferralCodeCallback {

    private final IInviteFriendActivityInteractorOutput iInviteFriendActivityInteractorOutput;


    public InviteFriendActivityInteractor(IInviteFriendActivityInteractorOutput iInviteFriendActivityInteractorOutput) {
        this.iInviteFriendActivityInteractorOutput = iInviteFriendActivityInteractorOutput;
    }

    @Override
    public void getReferralCode() {
        String refCode = getReferralCodeFromPref();
        if (!Utility.isEmpty(refCode)) {
            this.iInviteFriendActivityInteractorOutput.onGetReferralCodeSuccess(refCode);
        } else {
            UserAuthCloudDataStore.getInstance().getReferralCode(this);
        }
    }

    private String getReferralCodeFromPref() {
        return MDPreferenceManager.getReferralCode();
    }

    private void setRefferalCodeInPref(String refferalCode) {
        MDPreferenceManager.setReferralCode(refferalCode);
    }

    @Override
    public void onGetReferralCodeSuccess(String referralCode) {
        this.setRefferalCodeInPref(referralCode);
        this.iInviteFriendActivityInteractorOutput.onGetReferralCodeSuccess(referralCode);
    }

    @Override
    public void onGetReferralCodeFailure(int errorCode, String message) {
        if (Utility.isUnauthorized(errorCode))
            this.getReferralCode();
        else
            this.iInviteFriendActivityInteractorOutput.onGetReferralCodeFailure(errorCode, message);
    }

}
