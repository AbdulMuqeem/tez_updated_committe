package com.tez.androidapp.commons.utils.authorized.alert.image.popup;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.Transformation;
import com.tez.androidapp.app.MobileDostApplication;

/**
 * Created by FARHAN DHANANI on 7/8/2018.
 */
public interface UIForAlertImage {

    void setErrorImage(int errorImageDrawable);

    @NonNull
    Transformation<Bitmap>[] getTransformation();

    void setPlaceholder(int placeholderId);

    int getPlaceholder();

    void onInitiate();

    void onSuccess();

    void onError();

    int getErrorImageDrawable();

    ImageView getTarget();

    default int getHeight() {
        Context context = MobileDostApplication.getInstance().getApplicationContext();
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    default int getWidth() {
        Context context = MobileDostApplication.getInstance().getApplicationContext();
        return context.getResources().getDisplayMetrics().widthPixels;
    }
}
