package com.tez.androidapp.rewamp.cloud.client;

import com.tez.androidapp.rewamp.cloud.ApiClient;

import okhttp3.OkHttpClient;

public class DataLiftClient extends BaseClient {

    @Override
    public ApiClient build(){
        return null;//buildClient(DataLiftAPI.class, buildOkHttpClient());
    }

    @Override
    protected ApiClient build(OkHttpClient.Builder okBuilder) {
        return null;//buildClient(DataLiftAPI.class, okBuilder);
    }

}
