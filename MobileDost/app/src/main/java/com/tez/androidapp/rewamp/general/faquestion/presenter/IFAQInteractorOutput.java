package com.tez.androidapp.rewamp.general.faquestion.presenter;

import com.tez.androidapp.app.general.feature.faq.callbacks.GetFAQsCallback;

public interface IFAQInteractorOutput extends GetFAQsCallback {
}
