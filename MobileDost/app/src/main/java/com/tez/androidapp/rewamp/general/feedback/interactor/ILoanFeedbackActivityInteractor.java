package com.tez.androidapp.rewamp.general.feedback.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;
import com.tez.androidapp.rewamp.general.feedback.request.LoanFeedbackRequest;

public interface ILoanFeedbackActivityInteractor extends IBaseInteractor {

    void submitFeedback(@NonNull LoanFeedbackRequest loanFeedbackRequest);
}
