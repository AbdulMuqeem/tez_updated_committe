package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

public class AccountLinkRequest extends BaseRequest {
    public static final String METHOD_NAME = "v1/user/account/link";

    private String socialId;
    private Integer socialType;

    public AccountLinkRequest(String socialId, Integer socialType){
        this.socialId = socialId;
        this.socialType = socialType;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public Integer getSocialType() {
        return socialType;
    }

    public void setSocialType(Integer socialType) {
        this.socialType = socialType;
    }
}
