package com.tez.androidapp.rewamp.advance.request.presenter;

import androidx.annotation.Nullable;

import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;

public interface ISelectLoanActivityPresenter {

    void getLoanLimit();

    void setMobileAccountId(@Nullable Wallet wallet);

    void calculateAllAmounts(@Nullable Wallet wallet, double loanAmount, int dayTenure);

    void onClickBtContinue(double loanAmount, @Nullable Wallet wallet);
}
