package com.tez.androidapp.rewamp.general.wallet.router;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.app.base.ui.fragments.BaseFragment;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.view.ChangeWalletActivity;

public class ChangeWalletActivityRouter extends BaseActivityRouter {

    public static final int REQUEST_CODE_CHANGE_WALLET = 10873;
    public static final String RESULT_DATA_WALLET = "RESULT_DATA_WALLET";

    public static ChangeWalletActivityRouter createInstance() {
        return new ChangeWalletActivityRouter();
    }

    public void setDependenciesAndRouteForResult(@NonNull BaseActivity from) {
        routeForResult(from, createIntent(from), REQUEST_CODE_CHANGE_WALLET);
    }

    public void setDependenciesAndRouteForResult(@NonNull BaseFragment from) {
        if (from.isResumed())
            routeForResult(from, createIntent(from.requireContext()), REQUEST_CODE_CHANGE_WALLET);
    }

    private Intent createIntent(@NonNull Context from) {
        return new Intent(from, ChangeWalletActivity.class);
    }
}
