package com.tez.androidapp.rewamp.advance.limit.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.advance.limit.view.LimitDeniedActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;

public class LimitDeniedActivityRouter extends BaseActivityRouter {

    public static LimitDeniedActivityRouter createInstance() {
        return new LimitDeniedActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, LimitDeniedActivity.class);
    }
}
