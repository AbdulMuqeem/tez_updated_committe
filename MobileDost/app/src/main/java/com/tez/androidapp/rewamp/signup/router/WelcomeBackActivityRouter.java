package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.utils.app.PushNotificationConstants;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.WelcomeBackActivity;

/**
 * Created by VINOD KUMAR on 8/26/2019.
 */
public class WelcomeBackActivityRouter extends BaseActivityRouter {

    public static final String USER_SESSION_TIMEOUT = "user_session_timeout";

    public static WelcomeBackActivityRouter createInstance() {
        return new WelcomeBackActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    public void setDependenciesAndRouteWithClearAndNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        route(from, intent);
    }

    public void setDependenciesAndRouteForUserSessionTimeout(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        intent.putExtra(USER_SESSION_TIMEOUT, true);
        route(from, intent);
    }

    public void setDependenciesAndRouteForPush(@NonNull BaseActivity from, String routeTo) {
        Intent intent = createIntent(from);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(PushNotificationConstants.KEY_ROUTE_TO, routeTo==null?"":routeTo);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, WelcomeBackActivity.class);
    }
}
