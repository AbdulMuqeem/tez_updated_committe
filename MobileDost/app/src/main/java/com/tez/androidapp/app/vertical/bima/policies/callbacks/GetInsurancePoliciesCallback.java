package com.tez.androidapp.app.vertical.bima.policies.callbacks;

import com.tez.androidapp.app.vertical.bima.policies.models.network.dto.response.GetInsurancePoliciesResponse;

/**
 * Created by Rehman Murad Ali on 8/25/2017.
 */

public interface GetInsurancePoliciesCallback {

    void onGetInsurancePoliciesSuccess(GetInsurancePoliciesResponse getInsurancePoliciesResponse);

    void onGetInsurancePoliciesFailure(int errorCode, String message);
}
