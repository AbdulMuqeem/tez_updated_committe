package com.tez.androidapp.app.general.feature.account.feature.profile.callbacks;

import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.UserProfile;
import com.tez.androidapp.commons.models.network.User;

public interface GetCompleteUserProfileCallback {
    void onCompleteUserProfileSuccess(User user);
    void onCompleteUserProfileFailure(int errorCode, String message);
}
