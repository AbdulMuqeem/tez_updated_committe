package com.tez.androidapp.rewamp.general.beneficiary.response.handler;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.general.beneficiary.listener.GetBeneficiariesListener;
import com.tez.androidapp.rewamp.general.beneficiary.response.BeneficiariesResponse;

public class BeneficiariesRH extends BaseRH<BeneficiariesResponse> {

    private GetBeneficiariesListener listener;

    public BeneficiariesRH(BaseCloudDataStore baseCloudDataStore, GetBeneficiariesListener listener) {
        super(baseCloudDataStore);
        this.listener = listener;
    }

    @Override
    protected void onSuccess(Result<BeneficiariesResponse> value) {
        BeneficiariesResponse beneficiariesResponse = value.response().body();
        if (beneficiariesResponse != null) {
            if (isErrorFree(beneficiariesResponse))
                this.listener.onGetBeneficiariesSuccess(beneficiariesResponse);
            else
                onFailure(beneficiariesResponse.getStatusCode(), beneficiariesResponse.getErrorDescription());
        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        this.listener.onGetBeneficiariesFailure(errorCode, message);
    }
}
