package com.tez.androidapp.commons.utils.authorized.alert.image.popup;

import android.graphics.Bitmap;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;


/**
 * Created by FARHAN DHANANI on 7/8/2018.
 */
@SuppressWarnings("unused")
public class DefaultUIForAlertImage implements UIForAlertImage {
    private final ImageView imageView;
    private final Integer height;
    private final Integer width;
    private int errorImageDrawable;
    private int placeholderId;
    private Transformation<Bitmap>[] transformation;

//
//    public DefaultUIForAlertImage(@NonNull PhotoView photoView) {
//        this.imageView = photoView;
//        this.height = null;
//        this.width = null;
//    }

    public DefaultUIForAlertImage(@NonNull ImageView tezImageView) {
        this.imageView = tezImageView;
        this.height = null;
        this.width = null;
    }

    public DefaultUIForAlertImage(@NonNull ImageView tezImageView,
                                  int height,
                                  int width) {
        this.imageView = tezImageView;
        this.height = height;
        this.width = width;
    }

    @Override
    public int getErrorImageDrawable() {
        return this.errorImageDrawable;
    }

    @Override
    public void setErrorImage(int errorImageDrawable) {
        this.errorImageDrawable = errorImageDrawable;
    }

    @NonNull
    @Override
    public Transformation<Bitmap>[] getTransformation() {
        if (transformation != null)
            return transformation;
        CenterCrop[] centerCrops = new CenterCrop[1];
        centerCrops[0] = new CenterCrop();
        return centerCrops;
    }

    @Override
    public void setPlaceholder(int placeholderId) {
        this.placeholderId = placeholderId;
    }

    @SafeVarargs
    public final void setTransformation(@NonNull Transformation<Bitmap>... transformation) {
        this.transformation = transformation;
    }

    @Override
    public int getPlaceholder() {
        return this.placeholderId;
    }

    @Override
    public void onInitiate() {
        //Left
    }

    @Override
    public void onSuccess() {
        //Left
    }

    @Override
    public void onError() {
        //Left
    }

    @Override
    public ImageView getTarget() {
        return this.imageView;
    }

    @Override
    public int getHeight() {
        return height == null ? UIForAlertImage.super.getHeight() : height;
    }

    @Override
    public int getWidth() {
        return width == null ? UIForAlertImage.super.getWidth() : width;
    }
}
