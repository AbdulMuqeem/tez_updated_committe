package com.tez.androidapp.rewamp.signup.presenter;

import android.location.Location;

public interface IWelcomeBackActivityPresenter {
    void validatePin(String pin, String mobileNumber);

    void saveLocationInPreference(Location location);

    void verifyUserActiveDevice(String pin, String mobileNumber);

    void checkForVerifyOrLogin(boolean callDeviceKey, String pin);
}
