package com.tez.androidapp.rewamp.bima.products.model;

import androidx.annotation.Nullable;

import java.io.Serializable;

public class InsuranceServiceProviderContact implements Serializable {

    @Nullable
    private String contactEmail;

    @Nullable
    private String contactWhatsApp;

    private String contactPhone;

    private String contactTimings;

    @Nullable
    public String getContactEmail() {
        return contactEmail;
    }

    @Nullable
    public String getContactWhatsApp() {
        return contactWhatsApp;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public String getContactTimings() {
        return contactTimings;
    }
}
