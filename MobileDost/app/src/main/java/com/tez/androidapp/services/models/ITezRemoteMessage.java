package com.tez.androidapp.services.models;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Map;

public interface ITezRemoteMessage {

    @NonNull
    Map<String, String> getData();

    @Nullable
    TezRemoteMessage.TezNotification getNotification();

    interface ITezNotification {

        @Nullable
        String getTitle();

        @Nullable
        String getBody();
    }
}
