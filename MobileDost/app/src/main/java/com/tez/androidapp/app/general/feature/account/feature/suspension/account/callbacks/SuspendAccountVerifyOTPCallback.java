package com.tez.androidapp.app.general.feature.account.feature.suspension.account.callbacks;

import com.tez.androidapp.app.base.response.BaseResponse;

/**
 * Created by Rehman Murad Ali on 8/22/2017.
 */

public interface SuspendAccountVerifyOTPCallback {


    void onSuspendAccountVerifyOTPSuccess(BaseResponse baseResponse);

    void onSuspendAccountVerifyOTPFailure(int errorCode, String message);
}
