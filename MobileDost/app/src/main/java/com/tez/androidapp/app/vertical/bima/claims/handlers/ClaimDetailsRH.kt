package com.tez.androidapp.app.vertical.bima.claims.handlers

import com.jakewharton.retrofit2.adapter.rxjava2.Result
import com.tez.androidapp.app.base.handlers.BaseRH
import com.tez.androidapp.app.vertical.bima.claims.callback.ClaimDetailsCallBack
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.ClaimDetailsResponse
import com.tez.androidapp.repository.network.store.BaseCloudDataStore

/**
 * Created by Vinod Kumar on 10/19/2020.
 */
class ClaimDetailsRH(baseCloudDataStore: BaseCloudDataStore, private val callBack: ClaimDetailsCallBack)
    : BaseRH<ClaimDetailsResponse>(baseCloudDataStore) {

    override fun onSuccess(value: Result<ClaimDetailsResponse>) {
        val response = value.response().body()
        response?.let {
            if (isErrorFree(it))
                callBack.onMyClaimSuccess(it.claimDetailDto)
            else
                onFailure(it.statusCode, it.errorDescription)
        } ?: sendDefaultFailure()
    }

    override fun onFailure(errorCode: Int, message: String?) {
        callBack.onMyClaimFailure(errorCode, message)
    }
}