package com.tez.androidapp.commons.models.extract;

import com.tez.androidapp.commons.utils.app.Utility;

/**
 * Created  on 12/8/2016.
 */

public class ExtractedSms {
    private String address;
    private String receivedDate;
    private String sentDate;
    private String body;
    private String type;

    public ExtractedSms(me.everything.providers.android.telephony.Sms sms) {
        address = sms.address;
        type = sms.type == null ? null : sms.type.name();
        receivedDate = Utility.getFormattedDate(sms.receivedDate);
        sentDate = Utility.getFormattedDate(sms.sentDate);
        body = sms.body;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(String receivedDate) {
        this.receivedDate = receivedDate;
    }

    public String getSentDate() {
        return sentDate;
    }

    public void setSentDate(String sentDate) {
        this.sentDate = sentDate;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
