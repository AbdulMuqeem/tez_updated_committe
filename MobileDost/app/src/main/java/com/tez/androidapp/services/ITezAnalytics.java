package com.tez.androidapp.services;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface ITezAnalytics {

    void setAnalyticsCollectionEnabled(boolean enabled);

    void logEvent(@NonNull String event, @Nullable Bundle params);

    void setUserId(@NonNull String userId);

    void trackEvent(String category, String action, String label);
}
