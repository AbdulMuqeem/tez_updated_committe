package com.tez.androidapp.rewamp.bima.products.response.handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.rewamp.bima.products.callback.ProductPolicyCallback;
import com.tez.androidapp.rewamp.bima.products.response.ProductPolicyResponse;

public class ProductPolicyRH extends BaseRH<ProductPolicyResponse> {

    private final ProductPolicyCallback callback;

    public ProductPolicyRH(BaseCloudDataStore baseCloudDataStore, @NonNull ProductPolicyCallback callback) {
        super(baseCloudDataStore);
        this.callback = callback;
    }

    @Override
    protected void onSuccess(Result<ProductPolicyResponse> value) {
        ProductPolicyResponse response = value.response().body();

        if (response != null) {

            if (isErrorFree(response))
                callback.onGetProductPolicySuccess(response);
            else
                onFailure(response.getStatusCode(), response.getErrorDescription());
        } else
            sendDefaultFailure();
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        callback.onGetProductPolicyFailure(errorCode, message);
    }
}
