package com.tez.androidapp.commons.utils.authorized.alert.image.popup;

import android.graphics.Bitmap;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.EmptySignature;
import com.bumptech.glide.signature.ObjectKey;
import com.tez.androidapp.app.MobileDostApplication;
import com.tez.androidapp.commons.utils.app.Utility;

/**
 * Created by FARHAN DHANANI on 7/8/2018.
 */
public class DefaultLocalCacheImageLoader
        implements LocalCacheImageLoader {

    private String signature;
    private Uri fileUri;
    private UIForAlertImage customUIForAlertImage;

    @Override
    public void setUrlToLoad(@NonNull Uri urlToLoad) {
        this.fileUri = urlToLoad;
    }

    @Override
    public void setSignatureToFile(@Nullable String signature) {
        this.signature = signature;
    }

    @Override
    public void setFileToLoad(@NonNull Uri fileToLoad) {
        this.fileUri = fileToLoad;
    }

    @Override
    public void setUI(@NonNull UIForAlertImage uiForAlertImage) {
        this.customUIForAlertImage = uiForAlertImage;
    }

    @Override
    public void loadImage() {
        if (this.fileUri != null
                && !Utility.isEmpty(this.fileUri.toString())
                && this.customUIForAlertImage != null) {
            customUIForAlertImage.onInitiate();
            RequestOptions requestOptions = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .signature(getSignature() != null ? new ObjectKey(getSignature()) : EmptySignature.obtain())
                    .placeholder(customUIForAlertImage.getPlaceholder())
                    .error(customUIForAlertImage.getErrorImageDrawable())
                    .transform(customUIForAlertImage.getTransformation())
                    .encodeQuality(70);
            Glide.with(MobileDostApplication.getInstance().getApplicationContext())
                    .asBitmap()
                    .load(this.fileUri.getPath())
                    .apply(requestOptions)
                    .listener(new RequestListener<Bitmap>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                            customUIForAlertImage.onError();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                            customUIForAlertImage.onSuccess();
                            return false;
                        }
                    })
                    .into(this.customUIForAlertImage.getTarget());
        }
    }

    @Override
    public String getSignature() {
        return this.signature;
    }
}
