package com.tez.androidapp.rewamp.bima.claim.common.details.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.lifecycle.ViewModelProvider
import com.tez.androidapp.R
import com.tez.androidapp.app.base.ui.activities.BaseActivity
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.ClaimDetailDto
import com.tez.androidapp.commons.utils.app.Constants
import com.tez.androidapp.commons.utils.app.Utility
import com.tez.androidapp.commons.widgets.TezLoader
import com.tez.androidapp.commons.widgets.TezTextView
import com.tez.androidapp.rewamp.bima.claim.ClaimStatus.*
import com.tez.androidapp.rewamp.bima.claim.common.details.router.ClaimDetailsActivityRouter
import com.tez.androidapp.rewamp.bima.claim.common.details.viewmodel.ClaimDetailsViewModel
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Failure
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Loading
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Success
import com.tez.androidapp.rewamp.bima.claim.revise.router.ReviseClaimActivityRouter
import kotlinx.android.synthetic.main.activity_claim_details.*
import kotlinx.android.synthetic.main.claims_fragment.clContent
import kotlinx.android.synthetic.main.claims_fragment.shimmer

class ClaimDetailsActivity : BaseActivity() {

    private lateinit var viewModel: ClaimDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_claim_details)
        viewModel = ViewModelProvider(this).get(ClaimDetailsViewModel::class.java)
        init()
    }

    private fun init() {
        observeData(ClaimDetailsActivityRouter.getDependencies(intent).claimId)
        observeNetwork()
    }

    private fun observeNetwork() {
        viewModel.networkCallMutableLiveData.observe(this, {
            when (it) {
                is Loading -> {
                    shimmer.visibility = View.VISIBLE
                    clContent.visibility = View.GONE
                }
                is Failure -> {
                    shimmer.visibility = View.GONE
                    clContent.visibility = View.GONE
                    showError(it.errorCode)
                }
                is Success -> {
                    shimmer.visibility = View.GONE
                    clContent.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun observeData(claimId: Int) {
        viewModel.getClaimDetailsLiveData(claimId).observe(this) {
            val claimStatus = valueOf(it.claimStatus)
            tvClaimStatus.text = it.claimStatus
            tvClaimStatus.setTextColor(Utility.getColorFromResource(claimStatus.textColor))
            tvClaimStatus.setBackgroundResource(claimStatus.background)
            btOkay.setDoubleTapSafeOnClickListener { finish() }
            when (claimStatus) {
                Pending -> onPending(it)
                Approved -> onApproved(it)
                Disbursed -> onDisbursed(it)
                Rejected -> onRejected(it)
                Revise -> onRevise(it)
            }
        }
    }

    private fun observeInitiateReviseClaimNetwork() {
        viewModel.initiateReviseClaimNetworkLiveData.observe(this, {
            when (it) {
                is Loading -> {
                    showTezLoader()
                }
                is Failure -> {
                    dismissTezLoader()
                    showError(it.errorCode)
                }
                is Success -> {
                    setTezLoaderToBeDissmissedOnTransition()
                }
            }
        })
    }

    private fun observeInitiateReviseClaim(claimDetailDto: ClaimDetailDto) {
        viewModel.initiateReviseClaimLiveData.observe(this) {
            ReviseClaimActivityRouter().setDependenciesAndRoute(
                    this,
                    claimDetailDto.productId,
                    claimDetailDto.id,
                    it.data,
                    claimDetailDto.comments?.toTypedArray(),
            )
        }
    }

    private fun onPending(claimDetailDto: ClaimDetailDto) {
        toolbar.setToolbarTitle(R.string.claim_pending)
        ivLogo.visibility = View.GONE
        tvHeading.setText(R.string.your_claim_is_in_review)
        tvDescription.setText(R.string.we_will_contact_you_soon)
        ivIcon.setImageResource(Utility.getInsuranceProductIcon(claimDetailDto.productId))
        tvTitle.text = claimDetailDto.insurancePolicyName
        tvMessage.text = getString(R.string.policy_no_value, claimDetailDto.mobileUserInsurancePolicyNumber)
        tvClaimAmount.text = getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(claimDetailDto.claimAmount!!))
        tvClaimDate.text = Utility.getFormattedDate(claimDetailDto.claimDate, Constants.INPUT_DATE_FORMAT, Constants.UI_DATE_FORMAT)
        tvClaimNumber.text = claimDetailDto.claimReferenceNumber
        cvClaimSummary.visibility = View.GONE
    }

    private fun onRejected(claimDetailDto: ClaimDetailDto) {
        tvHeading.setText(R.string.claim_rejected)
        tvDescription.setText(R.string.unfortunately_claim_rejected)
        tvReasons.setText(R.string.reasons_of_rejection)
        tvHeading.setTextColor(Utility.getColorFromResource(R.color.textViewHeadingColorRed))
        tvDescription.setTypeface(R.font.barlow_medium)
        cvClaimDetails.visibility = View.GONE
        cvClaimSummary.visibility = View.GONE
        claimDetailDto.comments?.let { showComments(it) }
    }

    private fun onApproved(claimDetailDto: ClaimDetailDto) {
        toolbar.setToolbarTitle(R.string.claim_approved)
        ivLogo.visibility = View.GONE
        tvHeading.visibility = View.GONE
        tvDescription.visibility = View.GONE
        tllClaimAmount.visibility = View.GONE
        tllTransactionId.visibility = View.GONE
        tllDate.visibility = View.GONE
        ivIcon.setImageResource(Utility.getInsuranceProductIcon(claimDetailDto.productId))
        tvTitle.text = claimDetailDto.insurancePolicyName
        tvMessage.text = getString(R.string.policy_no_value, claimDetailDto.mobileUserInsurancePolicyNumber)
        tvClaimDate.text = Utility.getFormattedDate(claimDetailDto.claimDate, Constants.INPUT_DATE_FORMAT, Constants.UI_DATE_FORMAT)
        tvClaimNumber.text = claimDetailDto.claimReferenceNumber
        tvAmount.text = getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(claimDetailDto.claimAmount!!))
        tvWallet.text = claimDetailDto.mobileUserAccountName
        tvWalletNumber.text = claimDetailDto.mobileUserAccountNumber
    }

    private fun onDisbursed(claimDetailDto: ClaimDetailDto) {
        toolbar.setToolbarTitle(R.string.claim_disbursed)
        ivLogo.visibility = View.GONE
        tvHeading.visibility = View.GONE
        tvDescription.visibility = View.GONE
        tllClaimAmount.visibility = View.GONE
        ivIcon.setImageResource(Utility.getInsuranceProductIcon(claimDetailDto.productId))
        tvTitle.text = claimDetailDto.insurancePolicyName
        tvMessage.text = getString(R.string.policy_no_value, claimDetailDto.mobileUserInsurancePolicyNumber)
        tvClaimDate.text = Utility.getFormattedDate(claimDetailDto.claimDate, Constants.INPUT_DATE_FORMAT, Constants.UI_DATE_FORMAT)
        tvClaimNumber.text = claimDetailDto.claimReferenceNumber
        tvAmount.text = getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(claimDetailDto.claimAmount!!))
        tvWallet.text = claimDetailDto.mobileUserAccountName
        tvWalletNumber.text = claimDetailDto.mobileUserAccountNumber
        tvTransactionId.text = claimDetailDto.transactionId
        tvDate.text = Utility.getFormattedDate(claimDetailDto.transactionDate, Constants.INPUT_DATE_FORMAT, Constants.OUTPUT_DATE_FORMAT)
    }

    private fun onRevise(claimDetailDto: ClaimDetailDto) {
        ivLogo.setImageResource(R.drawable.ic_illustration_revise_claim)
        tvHeading.setText(R.string.revise_your_claim)
        tvDescription.setText(R.string.revise_claim_and_resubmit)
        ivIcon.setImageResource(Utility.getInsuranceProductIcon(claimDetailDto.productId))
        tvTitle.text = claimDetailDto.insurancePolicyName
        tvMessage.text = getString(R.string.policy_no_value, claimDetailDto.mobileUserInsurancePolicyNumber)
        tvClaimAmount.text = getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(claimDetailDto.claimAmount!!))
        tvClaimDate.text = Utility.getFormattedDate(claimDetailDto.claimDate, Constants.INPUT_DATE_FORMAT, Constants.UI_DATE_FORMAT)
        tvClaimNumber.text = claimDetailDto.claimReferenceNumber
        cvClaimSummary.visibility = View.GONE
        tvClaimStatus.visibility = View.GONE
        btOkay.setText(R.string.revise_claim)
        claimDetailDto.comments?.let { showComments(it) }
        btOkay.setDoubleTapSafeOnClickListener {
            viewModel.initiateReviseClaim(claimDetailDto.id)
        }
        observeInitiateReviseClaimNetwork()
        observeInitiateReviseClaim(claimDetailDto)
    }

    private fun showComments(comments: List<String>) {
        tllComments.visibility = if (comments.isEmpty()) View.GONE else View.VISIBLE
        comments.forEach {
            val textView = TezTextView(this)
            textView.text = it
            textView.textSize = 12f
            textView.setTypeface(R.font.barlow_regular)
            textView.setTextColor(Utility.getColorFromResource(R.color.textViewTextColorBlack))
            textView.compoundDrawablePadding = Utility.dpToPx(12f)
            textView.changeDrawable(textView, R.drawable.ic_bullet_dot_black, 0, 0, 0)
            val params = LinearLayoutCompat.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                    LinearLayoutCompat.LayoutParams.WRAP_CONTENT)
            params.topMargin = Utility.dpToPx(4f)
            textView.layoutParams = params
            tllComments.addView(textView)
        }
    }

    override fun getTezLoader(): TezLoader? = createLoader()

    override fun getScreenName(): String = "ClaimDetailsActivity"
}