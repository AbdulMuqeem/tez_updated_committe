package com.tez.androidapp.rewamp.bima.products.policy.callbacks;

import com.tez.androidapp.rewamp.bima.products.policy.response.InsuranceProductDetailResponse;

public interface InsuranceProductDetailCallback {

    public void onSuccessInsuranceProductDetail(InsuranceProductDetailResponse insuranceProductDetailResponse);
    public void onFailureInsuranceProductDetal(int errorCode, String message);
}
