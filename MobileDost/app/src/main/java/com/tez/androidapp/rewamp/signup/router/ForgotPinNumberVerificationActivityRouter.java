package com.tez.androidapp.rewamp.signup.router;

import android.content.Intent;
import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.signup.verification.ForgotPinNumberVerificationActivity;
import com.tez.androidapp.rewamp.signup.verification.SignupNumberVerificationActivity;

/**
 * Created by VINOD KUMAR on 7/31/2019.
 */
public class ForgotPinNumberVerificationActivityRouter extends NumberVerificationActivityRouter {

    public static ForgotPinNumberVerificationActivityRouter createInstance() {
        return new ForgotPinNumberVerificationActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from) {
        route(from, createIntent(from));
    }

    @Override
    @NonNull
    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, ForgotPinNumberVerificationActivity.class);
    }
}
