
package com.tez.androidapp.rewamp.committee.response;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CommitteeFilterResponseMemberList implements Serializable, Parcelable {

    @SerializedName("joiningDate")
    private String mJoiningDate;
    @SerializedName("userId")
    private Long mUserId;
    @SerializedName("username")
    private String mUsername;

    private boolean isHeader;

    public CommitteeFilterResponseMemberList() {
    }

    protected CommitteeFilterResponseMemberList(Parcel in) {
        mJoiningDate = in.readString();
        if (in.readByte() == 0) {
            mUserId = null;
        } else {
            mUserId = in.readLong();
        }
        mUsername = in.readString();
        isHeader = in.readByte() != 0;
    }

    public static final Creator<CommitteeFilterResponseMemberList> CREATOR = new Creator<CommitteeFilterResponseMemberList>() {
        @Override
        public CommitteeFilterResponseMemberList createFromParcel(Parcel in) {
            return new CommitteeFilterResponseMemberList(in);
        }

        @Override
        public CommitteeFilterResponseMemberList[] newArray(int size) {
            return new CommitteeFilterResponseMemberList[size];
        }
    };

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }

    public String getJoiningDate() {
        return mJoiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        mJoiningDate = joiningDate;
    }

    public Long getUserId() {
        return mUserId;
    }

    public void setUserId(Long userId) {
        mUserId = userId;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mJoiningDate);
        if (mUserId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(mUserId);
        }
        dest.writeString(mUsername);
        dest.writeByte((byte) (isHeader ? 1 : 0));
    }
}
