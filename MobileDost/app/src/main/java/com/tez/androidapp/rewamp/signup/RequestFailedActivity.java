package com.tez.androidapp.rewamp.signup;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.commons.widgets.TezTextView;
import com.tez.androidapp.rewamp.CustomerSupport;
import com.tez.androidapp.rewamp.signup.router.RequestFailedActivityRouter;

import net.tez.viewbinder.library.core.OnClick;
import net.tez.viewbinder.library.core.ViewBinder;

public class RequestFailedActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_failed);
        ViewBinder.bind(this);
        this.setTitle();
        this.setMessage();
    }

    private void setTitle() {
        TezTextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getStringResourceFromIntent(RequestFailedActivityRouter.TITLE));
    }

    private void setMessage() {
        TezTextView tvMessage = findViewById(R.id.tvMessage);
        tvMessage.setText(getStringResourceFromIntent(RequestFailedActivityRouter.MESSAGE));
    }

    @StringRes
    private int getStringResourceFromIntent(@NonNull String key) {
        return getIntent().getIntExtra(key, 0);
    }

    @OnClick(R.id.btEmail)
    private void openEmail() {
        CustomerSupport.openEmail(this);
    }

    @OnClick(R.id.btCall)
    private void openDialer() {
        CustomerSupport.openDialer(this);
    }

    @OnClick(R.id.btWhatsapp)
    private void openWhatsapp() {
        CustomerSupport.openWhatsapp(this);
    }

    @OnClick(R.id.btMessenger)
    private void openMessenger() {
        CustomerSupport.openMessenger(this);
    }

    @Override
    protected String getScreenName() {
        return "RequestFailedActivity";
    }
}
