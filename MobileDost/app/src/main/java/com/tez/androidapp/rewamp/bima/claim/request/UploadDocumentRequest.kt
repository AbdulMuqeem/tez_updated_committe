package com.tez.androidapp.rewamp.bima.claim.request

import com.tez.androidapp.app.base.request.BaseRequest

class UploadDocumentRequest : BaseRequest() {

    companion object {
        const val METHOD_NAME = "v1/claim/document/upload"
        const val ATTACHMENT = "attachment"
        const val CLAIM_ID = "claimId"
        const val CATEGORY_ID = "categoryId"
    }
}