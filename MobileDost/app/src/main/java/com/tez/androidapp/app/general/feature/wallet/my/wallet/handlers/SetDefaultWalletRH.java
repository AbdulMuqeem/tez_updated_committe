package com.tez.androidapp.app.general.feature.wallet.my.wallet.handlers;

import androidx.annotation.Nullable;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.tez.androidapp.app.base.handlers.BaseRH;
import com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks.SetDefaultWalletCallback;
import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.repository.network.store.BaseCloudDataStore;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.app.Utility;

/**
 * Created by Rehman Murad Ali on 12/26/2017.
 */

public class SetDefaultWalletRH extends BaseRH<BaseResponse> {

    SetDefaultWalletCallback setDefaultWalletCallback;

    public SetDefaultWalletRH(BaseCloudDataStore baseCloudDataStore, SetDefaultWalletCallback setDefaultWalletCallback) {
        super(baseCloudDataStore);
        this.setDefaultWalletCallback = setDefaultWalletCallback;
    }

    @Override
    protected void onSuccess(Result<BaseResponse> value) {
        BaseResponse baseResponse = value.response().body();
        if (baseResponse.getStatusCode() == Constants.ERROR_CODE_NO_ERROR) {
            Utility.deletePicture();
            if (setDefaultWalletCallback != null) setDefaultWalletCallback.setDefaultWalletSuccess(baseResponse);
        } else {
            onFailure(baseResponse.getStatusCode(), baseResponse.getErrorDescription());
        }
    }

    @Override
    public void onFailure(int errorCode, @Nullable String message) {
        if (setDefaultWalletCallback != null) setDefaultWalletCallback.setDefaultWalletFailure(errorCode, message);
    }
}
