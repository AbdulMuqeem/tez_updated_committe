package com.tez.androidapp.rewamp.general.profile.presenter;

import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.QuestionOptionsCallBack;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateUserProfileCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.BasicInformation;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.Option;

import java.util.List;

public interface IBasicProfileactivityInteractorOutput
        extends UpdateUserProfileCallback {
    void onQuestionOptionsSuccess(List<Option> data, BasicInformation basicInformation);
    void onQuestionOptionsFailure(int errorCode, String message);
}
