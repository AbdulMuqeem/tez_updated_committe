package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created  on 3/7/2017.
 */

public class UserReActivateAccountOTPRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/account/reactivate";

    private String otp;

    public UserReActivateAccountOTPRequest(String otp){
        this.otp = otp;
    }

}
