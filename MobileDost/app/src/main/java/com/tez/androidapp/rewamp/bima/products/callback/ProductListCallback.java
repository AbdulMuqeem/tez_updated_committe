package com.tez.androidapp.rewamp.bima.products.callback;

import com.tez.androidapp.rewamp.bima.products.response.ProductListResponse;

public interface ProductListCallback {

    void onGetProductListSuccess(ProductListResponse productListResponse);

    void onGetProductListFailure(int errorCode, String message);
}
