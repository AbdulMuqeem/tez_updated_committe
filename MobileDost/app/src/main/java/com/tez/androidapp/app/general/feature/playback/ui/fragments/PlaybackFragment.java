package com.tez.androidapp.app.general.feature.playback.ui.fragments;


import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;

import com.tez.androidapp.R;
import com.tez.androidapp.app.base.ui.fragments.BaseFragment;
import com.tez.androidapp.app.general.feature.playback.AudioControlInterface;
import com.tez.androidapp.app.general.feature.playback.AudioPlayback;
import com.tez.androidapp.app.general.feature.playback.AudioPlaybackListener;
import com.tez.androidapp.app.general.feature.playback.IAudioPlayback;
import com.tez.androidapp.commons.widgets.TezTextView;

import net.tez.viewbinder.library.core.BindView;

/**
 * Created by Rehman Murad Ali on 12/12/2017.
 */

public abstract class PlaybackFragment extends BaseFragment implements IAudioPlayback, AudioPlaybackListener {

    @NonNull
    private final AudioControlInterface audioPlayback;

    @BindView(R.id.textViewHeading)
    protected TezTextView textViewHeading;

    public PlaybackFragment() {
        this.audioPlayback = AudioPlayback.create(getAudioId(), this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLifecycle().addObserver(this.audioPlayback.getObserver());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setListenerOnAudioView();
    }

    @Override
    public void onDestroy() {
        getLifecycle().removeObserver(this.audioPlayback.getObserver());
        super.onDestroy();
    }

    @Override
    public void showTezLoader() {
        super.showTezLoader();
        audioPlayback.stopPlayer();
        audioPlayback.stopTimer();
    }

    @Override
    public void dismissTezLoader() {
        super.dismissTezLoader();
        if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED))
            audioPlayback.startTimer();
    }

    @NonNull
    @Override
    public AudioControlInterface getAudioPlayback() {
        return audioPlayback;
    }

    @Nullable
    @Override
    public View getViewForAudio() {
        return textViewHeading;
    }
}
