package com.tez.androidapp.rewamp.profile.edit.interactor;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.UpdateUserProfileCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.callbacks.ValidateInfoCallback;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.ValidateInfo;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.UpdateUserProfileRequest;
import com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request.ValidateInfoRequest;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;
import com.tez.androidapp.rewamp.profile.edit.presenter.IEditProfileVerificationActivityInteractorOutput;

public class EditProfileVerificactionActivitInteractor implements IEditProfileVerificationActivityInteractor {
    private final IEditProfileVerificationActivityInteractorOutput iEditProfileVerificationActivityInteractorOutput;

    public EditProfileVerificactionActivitInteractor(IEditProfileVerificationActivityInteractorOutput iEditProfileVerificationActivityInteractorOutput) {
        this.iEditProfileVerificationActivityInteractorOutput = iEditProfileVerificationActivityInteractorOutput;
    }

    @Override
    public void updateUserProfile(@NonNull UpdateUserProfileRequest updateUserProfileRequest) {
        UserAuthCloudDataStore.getInstance().updateUserProfile(updateUserProfileRequest, new UpdateUserProfileCallback() {
            @Override
            public void onUpdateUserProfileSuccess() {
                iEditProfileVerificationActivityInteractorOutput.onUpdateUserProfileSuccess();
            }

            @Override
            public void onUpdateUserProfileFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode))
                    updateUserProfile(updateUserProfileRequest);
                else
                    iEditProfileVerificationActivityInteractorOutput.onUpdateUserProfileFailure(errorCode, message);
            }
        });
    }

    @Override
    public void validateInfo(final @NonNull String mobileNumber) {
        UserAuthCloudDataStore.getInstance().validateInfo(new ValidateInfoCallback() {
            @Override
            public void onValidateInfoSuccess(ValidateInfo validateInfo) {
                iEditProfileVerificationActivityInteractorOutput.onValidateInfoSuccess(mobileNumber);
            }

            @Override
            public void onValidateInfoFailure(int errorCode, String message) {
                if (Utility.isUnauthorized(errorCode)) {
                    validateInfo(mobileNumber);
                } else {
                    iEditProfileVerificationActivityInteractorOutput.onValidateInfoFailure(errorCode, message);
                }
            }
        }, new ValidateInfoRequest(null, mobileNumber));
    }
}
