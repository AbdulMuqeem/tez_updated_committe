package com.tez.androidapp.rewamp.committee.presenter;

import com.tez.androidapp.rewamp.committee.request.CommitteeSendMessageRequest;

/**
 * Created by Ahmad Izaz on 08-Nov-20
 **/
public interface ICommitteeGroupChatActivityPresenter {
    void getMessages(String committeeId);

    void sendMessage(CommitteeSendMessageRequest committeeSendMessageRequest);
}
