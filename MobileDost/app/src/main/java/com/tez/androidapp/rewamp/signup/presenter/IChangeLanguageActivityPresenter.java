package com.tez.androidapp.rewamp.signup.presenter;

import androidx.annotation.NonNull;

/**
 * Created by VINOD KUMAR on 8/9/2019.
 */
public interface IChangeLanguageActivityPresenter {

    void changeLanguage(@NonNull final String lang);
}
