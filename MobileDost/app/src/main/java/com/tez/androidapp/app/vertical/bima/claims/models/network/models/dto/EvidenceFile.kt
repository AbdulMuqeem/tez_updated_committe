package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Vinod Kumar on 5/6/2020.
 */
data class EvidenceFile(val categoryId: Int, val path: String) : Parcelable {

    constructor(parcel: Parcel) : this(parcel.readInt(),
            parcel.readString()!!)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(categoryId)
        parcel.writeString(path)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<EvidenceFile> {
        override fun createFromParcel(parcel: Parcel): EvidenceFile {
            return EvidenceFile(parcel)
        }

        override fun newArray(size: Int): Array<EvidenceFile?> {
            return arrayOfNulls(size)
        }
    }
}