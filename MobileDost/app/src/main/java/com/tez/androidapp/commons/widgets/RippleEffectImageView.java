package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.tez.androidapp.R;
import com.tez.androidapp.rewamp.TezDrawableHelper;

/**
 * Created by VINOD KUMAR on 7/22/2019.
 */
public class RippleEffectImageView extends RippleFrameLayout {

    public RippleEffectImageView(@NonNull Context context) {
        super(context);
    }

    public RippleEffectImageView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            init(context, attrs);
    }

    public RippleEffectImageView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            init(context, attrs);
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs) {
        inflate(context, R.layout.imageview_ripple_effect, this);
        TezImageView ivImage = findViewById(R.id.ivImage);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RippleEffectImageView);

        try {

            Drawable imageDrawable = TezDrawableHelper.getDrawable(context, typedArray, R.styleable.RippleEffectImageView_image_source);
            if (imageDrawable != null)
                ivImage.setImageDrawable(imageDrawable);

        } finally {
            typedArray.recycle();
        }
    }
}
