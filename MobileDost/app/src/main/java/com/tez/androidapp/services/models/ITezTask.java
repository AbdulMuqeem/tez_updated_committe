package com.tez.androidapp.services.models;

import androidx.annotation.NonNull;

import com.tez.androidapp.services.exception.TezApiException;
import com.tez.androidapp.services.listeners.TezOnFailureListener;
import com.tez.androidapp.services.listeners.TezOnSuccessListener;

public interface ITezTask<I, O> {

    @NonNull
    TezTask<I, O> addOnSuccessListener(@NonNull TezOnSuccessListener<O> onSuccessListener);

    @NonNull
    TezTask<I, O> addOnFailureListener(TezOnFailureListener onFailureListener);

    O getResultWithException() throws TezApiException;
}
