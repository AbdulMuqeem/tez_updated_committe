package com.tez.androidapp.rewamp.gold.onboarding.viewmodel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.repository.network.store.GoldCloudDataStore;
import com.tez.androidapp.rewamp.general.network.model.NetworkCall;
import com.tez.androidapp.rewamp.general.network.viewmodel.NetworkBoundViewModel;
import com.tez.androidapp.rewamp.gold.onboarding.callback.GetGoldRateCallback;
import com.tez.androidapp.rewamp.gold.onboarding.response.GetGoldRateResponse;

public class Gold0nboardingViewModel extends NetworkBoundViewModel {

    private MutableLiveData<GetGoldRateResponse> goldRateLiveData;

    @NonNull
    public MutableLiveData<GetGoldRateResponse> getGoldRateLiveData() {
        if (goldRateLiveData == null) {
            goldRateLiveData = new MutableLiveData<>();
            getGoldRate();
        }
        return goldRateLiveData;
    }

    private void getGoldRate() {
        networkCallMutableLiveData.postValue(new NetworkCall(NetworkCall.State.LOADING));
        GoldCloudDataStore.getInstance().getGoldRate(new GetGoldRateCallback() {
            @Override
            public void onGetGoldRateSuccess(@NonNull GetGoldRateResponse getGoldRateResponse) {
                networkCallMutableLiveData.postValue(new NetworkCall(NetworkCall.State.SUCCESS));
                goldRateLiveData.postValue(getGoldRateResponse);
            }

            @Override
            public void onGetGoldRateFailure(int errorCode, @Nullable String message) {
                if (Utility.isUnauthorized(errorCode))
                    getGoldRate();
                else
                    networkCallMutableLiveData.postValue(new NetworkCall(NetworkCall.State.FAILURE, errorCode));
            }
        });
    }
}
