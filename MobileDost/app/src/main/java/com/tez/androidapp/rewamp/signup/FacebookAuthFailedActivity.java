package com.tez.androidapp.rewamp.signup;

import android.os.Bundle;
import android.view.View;

import com.tez.androidapp.R;

public class FacebookAuthFailedActivity extends BaseErrorActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        setTvHeadingText(R.string.facebook_auth_failed);
        setTvMessageText(R.string.facebook_auth_failed_desc);
        setBtMainButtonText(R.string.try_again);
        setIvLogoImageResource(R.drawable.img_fb_login_failed);
        setBtMainButtonOnClickListener(view -> finishActivity());
        setTvNumberVisibility(View.GONE);
    }

    @Override
    protected String getScreenName() {
        return "FacebookAuthFailedActivity";
    }
}
