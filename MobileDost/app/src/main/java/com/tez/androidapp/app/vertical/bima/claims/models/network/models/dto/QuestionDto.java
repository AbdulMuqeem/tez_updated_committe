package com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by VINOD KUMAR on 3/14/2019.
 */
public class QuestionDto implements Serializable {

    private Integer id;
    private String question;
    private String questionType;
    private String questionSubType;
    private List<String> options;

    QuestionDto(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public String getQuestionType() {
        return questionType;
    }

    public String getQuestionSubType() {
        return questionSubType;
    }

    public List<String> getOptions() {
        return options;
    }
}
