package com.tez.androidapp.rewamp.bima.products.policy.response;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

public class PurchasedInsurancePolicy implements Parcelable {
    private int productId;
    private int policyId;
    private String policyName;
    private String policyNumber;
    private String issueDate;
    @Nullable
    private String activationDate;
    private String expiryDate;
    private double premium;
    private double discount;
    private double finalPrice;
    private String policyProviderCode;
    private Boolean resumeAsyncResponse;

    protected PurchasedInsurancePolicy(Parcel in) {
        productId = in.readInt();
        policyId = in.readInt();
        policyName = in.readString();
        policyNumber = in.readString();
        issueDate = in.readString();
        activationDate = in.readString();
        expiryDate = in.readString();
        premium = in.readDouble();
        discount = in.readDouble();
        finalPrice = in.readDouble();
        policyProviderCode = in.readString();
        byte tmpResumeAsyncResponse = in.readByte();
        resumeAsyncResponse = tmpResumeAsyncResponse == 0 ? null : tmpResumeAsyncResponse == 1;
    }

    public static final Creator<PurchasedInsurancePolicy> CREATOR = new Creator<PurchasedInsurancePolicy>() {
        @Override
        public PurchasedInsurancePolicy createFromParcel(Parcel in) {
            return new PurchasedInsurancePolicy(in);
        }

        @Override
        public PurchasedInsurancePolicy[] newArray(int size) {
            return new PurchasedInsurancePolicy[size];
        }
    };

    public int getProductId() {
        return productId;
    }

    public int getPolicyId() {
        return policyId;
    }

    public void setPolicyId(int policyId) {
        this.policyId = policyId;
    }

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    @Nullable
    public String getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(String activationDate) {
        this.activationDate = activationDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public double getPremium() {
        return premium;
    }

    public void setPremium(double premium) {
        this.premium = premium;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getPolicyProviderCode() {
        return policyProviderCode;
    }

    public void setPolicyProviderCode(String policyProviderCode) {
        this.policyProviderCode = policyProviderCode;
    }

    public Boolean getResumeAsyncResponse() {
        return resumeAsyncResponse;
    }

    public void setResumeAsyncResponse(Boolean resumeAsyncResponse) {
        this.resumeAsyncResponse = resumeAsyncResponse;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(productId);
        dest.writeInt(policyId);
        dest.writeString(policyName);
        dest.writeString(policyNumber);
        dest.writeString(issueDate);
        dest.writeString(activationDate);
        dest.writeString(expiryDate);
        dest.writeDouble(premium);
        dest.writeDouble(discount);
        dest.writeDouble(finalPrice);
        dest.writeString(policyProviderCode);
        dest.writeByte((byte) (resumeAsyncResponse == null ? 0 : resumeAsyncResponse ? 1 : 2));
    }
}
