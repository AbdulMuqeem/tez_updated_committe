package com.tez.androidapp.rewamp.bima.products.policy.callbacks;

import com.tez.androidapp.rewamp.bima.products.policy.response.PurchasedInsurancePolicy;

public interface PurchasePolicyCallback {
    void purchasePolicySuccess(PurchasedInsurancePolicy purchasedInsurancePolicy);
    void purchasePolicyFailure(int errorCode, String message);
}
