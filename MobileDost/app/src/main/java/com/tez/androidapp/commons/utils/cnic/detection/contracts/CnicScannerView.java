package com.tez.androidapp.commons.utils.cnic.detection.contracts;

import android.content.Intent;
import android.hardware.Camera;

import com.tez.androidapp.commons.utils.cnic.detection.model.RetumDataModel;

public interface CnicScannerView {

    void setScreenOrientation(int orientation);

    void setVisibilityOfCnicSquare(int visibility);

    void setCameraAndStartPreview(Camera camera, String detectObject);

    void setVisibilityOfTextViewHoldCnic(int visibility);

    void onPictureTaken(String filePath);

    void onPictureTakenFailure();

    void setVisibilityOfImageViewSelfiePreview(int visibility);

    void setVisibilityOfImageViewCapture(int visibility);

    void setVisibilityOfImageViewRetake(int visibility);

    void finishActivity();

    void startDetection();

    Intent setIntentForActivity(String filePath, RetumDataModel retumDataModel);

    void setResultOK(Intent intent);
}
