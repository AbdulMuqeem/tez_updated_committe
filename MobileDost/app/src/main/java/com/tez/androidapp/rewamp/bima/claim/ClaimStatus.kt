package com.tez.androidapp.rewamp.bima.claim


import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import com.tez.androidapp.R

enum class ClaimStatus(@ColorRes val textColor: Int,
                       @DrawableRes val background: Int) {

    Pending(R.color.textViewDescriptionColor, R.drawable.status_pending_bg),
    Approved(R.color.textViewTextColorGreen, R.drawable.status_active_bg),
    Disbursed(R.color.textViewTextColorGreen, R.drawable.status_active_bg),
    Rejected(R.color.stateRejectedColorRed, R.drawable.status_rejected_bg),
    Revise(R.color.revisedStatusTextColor, R.drawable.status_revised_bg)
}