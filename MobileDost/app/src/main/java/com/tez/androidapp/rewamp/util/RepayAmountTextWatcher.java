package com.tez.androidapp.rewamp.util;

import android.content.Context;
import android.text.Editable;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.textwatchers.TezTextWatcher;
import com.tez.androidapp.commons.widgets.TezEditTextView;

public class RepayAmountTextWatcher implements TezTextWatcher {

    private static final int MAX_DECIMAL_PLACES = 2;
    private static final int WITHOUT_DECIMAL_ALLOWED_DIGITS = 5;
    private TezEditTextView editText;
    private Context context;


    public RepayAmountTextWatcher(Context context, TezEditTextView editTextView) {
        this.context = context;
        this.editText = editTextView;
        this.editText.setCursorToLastPositionAlways();
        this.editText.disableCopyPaste();
        String value = editText.getValueText();
        if (value != null)
            editText.setSelection(value.length());
    }

    @Override
    public void afterTextChanged(Editable s) {
        String decimalPoint = context.getString(R.string.decimal_point);
        String zeroWithDecimalPoint = context.getString(R.string.zero_with_decimal_point);


        // If contains only decimal point in amount then it must be "0."
        String text = s.subSequence(0, s.length()).toString();
        if (text.equals(decimalPoint)) {
            editText.setText(zeroWithDecimalPoint);
            editText.setSelection(zeroWithDecimalPoint.length());
        } else {

            //If contains multiple decimal points then it must be converted to one decimal point
            int beginDecimalIndex = text.indexOf(decimalPoint);
            int lastDecimalIndex = text.lastIndexOf(decimalPoint);
            if (beginDecimalIndex != lastDecimalIndex) {
                String oneDecimalString = s.subSequence(0, beginDecimalIndex + 1).toString();
                editText.setText(oneDecimalString);
                editText.setSelection(oneDecimalString.length());
            } else {

                //if contains more than max allowed decimal places then it must be converted to stay within limit"MAX_DECIMAL_PLACES"
                int decimalLocation = text.indexOf(decimalPoint);
                if (decimalLocation != -1) {
                    int afterDecimalDigitCount = s.subSequence(decimalLocation + 1, s.length()).length();
                    if (afterDecimalDigitCount > MAX_DECIMAL_PLACES) {
                        String finalValue = s.subSequence(0, decimalLocation + MAX_DECIMAL_PLACES + 1).toString();
                        editText.setText(finalValue);
                        editText.setSelection(finalValue.length());
                    }
                    //If exceed the limit of allowed digits without decimal point then it must be converted to stay within limit "WITHOUT_DECIMAL_ALLOWED_DIGITS"
                } else if (text.length() > WITHOUT_DECIMAL_ALLOWED_DIGITS) {
                    int difference = text.length() - WITHOUT_DECIMAL_ALLOWED_DIGITS;
                    String allowedDigitsAmount = s.subSequence(0, s.length() - difference).toString();
                    editText.setText(allowedDigitsAmount);
                    editText.setSelection(allowedDigitsAmount.length());
                }
            }
        }
    }
}
