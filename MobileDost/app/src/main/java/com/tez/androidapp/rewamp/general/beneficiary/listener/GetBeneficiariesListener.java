package com.tez.androidapp.rewamp.general.beneficiary.listener;

import com.tez.androidapp.rewamp.general.beneficiary.response.BeneficiariesResponse;

public interface GetBeneficiariesListener {

    void onGetBeneficiariesSuccess(BeneficiariesResponse beneficiariesResponse);

    void onGetBeneficiariesFailure(int errorCode, String message);
}
