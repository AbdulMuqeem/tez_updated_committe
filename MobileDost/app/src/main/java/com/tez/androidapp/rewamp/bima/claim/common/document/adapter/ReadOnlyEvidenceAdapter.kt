package com.tez.androidapp.rewamp.bima.claim.common.document.adapter

import android.view.View
import android.view.ViewGroup
import com.tez.androidapp.R
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.Evidence
import com.tez.androidapp.commons.utils.app.Utility
import kotlinx.android.synthetic.main.list_item_read_only_evidence.view.*
import net.tez.tezrecycler.base.GenericRecyclerViewAdapter
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener
import net.tez.tezrecycler.base.viewholder.BaseViewHolder

class ReadOnlyEvidenceAdapter(items: List<Evidence>, listener: ReadOnlyEvidenceListener?)
    : GenericRecyclerViewAdapter<Evidence,
        ReadOnlyEvidenceAdapter.ReadOnlyEvidenceListener,
        ReadOnlyEvidenceAdapter.ReadOnlyEvidenceViewHolder>(items, listener) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReadOnlyEvidenceViewHolder {
        val view = inflate(parent.context, R.layout.list_item_read_only_evidence, parent)
        return ReadOnlyEvidenceViewHolder(view)
    }

    override fun onBindViewHolder(holder: ReadOnlyEvidenceViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.onBind(position, items, listener)
    }

    interface ReadOnlyEvidenceListener : BaseRecyclerViewListener {
        fun onClickEvidence(evidence: Evidence)
    }

    class ReadOnlyEvidenceViewHolder(itemView: View) : BaseViewHolder<Evidence, ReadOnlyEvidenceListener>(itemView) {

        override fun onBind(position: Int, items: MutableList<Evidence>, listener: ReadOnlyEvidenceListener?) {
            super.onBind(position, items, listener)
            val item = items[position]
            itemView.run {
                tvName.text = Utility.getStringFromResource(item.name)
                tvFilesAttached.text = context.getString(R.string.num_files_uploaded, item.evidenceFileList.size)
                viewSeparator.visibility = if (position == items.size - 1) View.GONE else View.VISIBLE
                ivViewDoc.setDoubleTapSafeOnClickListener { listener?.onClickEvidence(item) }
            }
        }
    }
}