package com.tez.androidapp.commons.receivers;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.tez.androidapp.commons.managers.MDPreferenceManager;
import com.tez.androidapp.commons.utils.app.Constants;
import com.tez.androidapp.commons.utils.extract.ExtractionUtil;

/**
 * BluetoothReceiver to fetch all the Bluetooths the device has connected to for DataLift.
 *
 * Created  on 1/24/2017.
 */

public class BluetoothReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if (action != null && action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
            if (state == BluetoothAdapter.STATE_ON
                    && System.currentTimeMillis() - Constants.WEEK_MILLIS >= MDPreferenceManager.getLastSentBluetoothTime()) {
                ExtractionUtil extractionUtil = new ExtractionUtil(context);
                String blueTooth = extractionUtil.extractBluetoothUnsafe();
                MDPreferenceManager.setLastSentBluetoothTime(System.currentTimeMillis());
                if (!blueTooth.isEmpty())
                    MDPreferenceManager.setBlueToothData(blueTooth);
            }
        }
    }

    public void register(Context context) {
        context.registerReceiver(this, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
    }
}
