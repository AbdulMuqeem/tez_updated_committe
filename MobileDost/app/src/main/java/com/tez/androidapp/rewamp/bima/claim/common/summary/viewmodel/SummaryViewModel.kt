package com.tez.androidapp.rewamp.bima.claim.common.summary.viewmodel

import androidx.lifecycle.MutableLiveData
import com.tez.androidapp.app.general.feature.wallet.my.wallet.callbacks.GetAllWalletCallback
import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.Evidence
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.EvidenceFile
import com.tez.androidapp.commons.utils.app.Utility
import com.tez.androidapp.commons.utils.file.util.FileUtil
import com.tez.androidapp.repository.network.store.BimaCloudDataStore
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore
import com.tez.androidapp.rewamp.bima.claim.callback.ConfirmClaimCallback
import com.tez.androidapp.rewamp.bima.claim.callback.LodgeClaimCallback
import com.tez.androidapp.rewamp.bima.claim.callback.UploadDocumentCallback
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Failure
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Loading
import com.tez.androidapp.rewamp.bima.claim.common.network.model.NetworkState
import com.tez.androidapp.rewamp.bima.claim.common.network.model.Success
import com.tez.androidapp.rewamp.bima.claim.common.network.viewmodel.NetworkBoundViewModel
import com.tez.androidapp.rewamp.bima.claim.request.ConfirmClaimRequest
import com.tez.androidapp.rewamp.bima.claim.request.LodgeClaimRequest
import com.tez.androidapp.rewamp.bima.claim.request.UploadDocumentRequest
import com.tez.androidapp.rewamp.bima.claim.response.ConfirmClaimResponse
import com.tez.androidapp.rewamp.bima.claim.response.LodgeClaimResponse
import com.tez.androidapp.rewamp.bima.claim.response.UploadDocumentResponse
import okhttp3.MediaType
import okhttp3.MultipartBody
import java.io.File
import java.util.*

class SummaryViewModel : NetworkBoundViewModel() {

    val walletLiveData: MutableLiveData<Wallet> = MutableLiveData()
    val evidenceFilesQueue: Queue<EvidenceFile> = LinkedList()
    val confirmClaimLiveData: MutableLiveData<ConfirmClaimResponse> = MutableLiveData()
    var claimId: Int? = null
    val lodgeClaimNetworkLiveData: MutableLiveData<NetworkState> = MutableLiveData()

    fun getWallet() {
        if (walletLiveData.value != null)
            return
        networkCallMutableLiveData.value = Loading
        UserAuthCloudDataStore.getInstance().getAllWallet(object : GetAllWalletCallback {
            override fun onGetAllWalletSuccess(wallets: List<Wallet>) {
                walletLiveData.value = wallets.find { it.isDefault }
                networkCallMutableLiveData.value = Success
            }

            override fun onGetAllWalletFailure(errorCode: Int, message: String) =
                    if (Utility.isUnauthorized(errorCode)) getWallet() else {
                        networkCallMutableLiveData.value = Failure(errorCode)
                    }
        })
    }

    fun initiateQueue(evidenceList: List<Evidence>) {
        evidenceFilesQueue.clear()
        evidenceList.forEach {
            evidenceFilesQueue.addAll(it.evidenceFileList)
        }
    }

    fun lodgeClaim(lodgeClaimRequest: LodgeClaimRequest) {
        lodgeClaimNetworkLiveData.value = Loading
        BimaCloudDataStore.getInstance().lodgeClaim(lodgeClaimRequest, object : LodgeClaimCallback {

            override fun onSuccessLodgeClaim(lodgeClaimResponse: LodgeClaimResponse) {
                claimId = lodgeClaimResponse.claimId
                uploadDocument(claimId!!)
            }

            override fun onFailureLodgeClaim(errorCode: Int, message: String?) {
                if (Utility.isUnauthorized(errorCode))
                    lodgeClaim(lodgeClaimRequest)
                else
                    lodgeClaimNetworkLiveData.value = Failure(errorCode)
            }
        })
    }

    fun uploadDocument(claimId: Int) {
        lodgeClaimNetworkLiveData.value = Loading
        if (evidenceFilesQueue.isEmpty()) {
            confirmClaim(ConfirmClaimRequest(claimId))
            return
        }
        val evidenceFile = evidenceFilesQueue.peek()!!
        val file = File(evidenceFile.path)
        if (file.extension != "pdf")
            Utility.compressFile(file, 70)
        val name = "${System.currentTimeMillis()}.${file.extension}"
        val data = MultipartBody.Part.createFormData(UploadDocumentRequest.ATTACHMENT,
                name,
                MultipartBody.create(MediaType.parse(Utility.getMimeTypeFromLocalFileUri(file.absolutePath)), file))

        val claimIdPart = MultipartBody.Part.createFormData(UploadDocumentRequest.CLAIM_ID, claimId.toString())
        val categoryIdPart = MultipartBody.Part.createFormData(UploadDocumentRequest.CATEGORY_ID, evidenceFile.categoryId.toString())

        BimaCloudDataStore.getInstance().uploadDocument(data, claimIdPart, categoryIdPart,
                object : UploadDocumentCallback {
                    override fun onSuccessUploadDocument(uploadDocumentResponse: UploadDocumentResponse) {
                        evidenceFilesQueue.poll()
                        uploadDocument(claimId)
                    }

                    override fun onFailureUploadDocument(errorCode: Int, message: String?) {
                        if (Utility.isUnauthorized(errorCode))
                            uploadDocument(claimId)
                        else
                            lodgeClaimNetworkLiveData.value = Failure(errorCode)
                    }
                })
    }

    fun confirmClaim(confirmClaimRequest: ConfirmClaimRequest) {
        BimaCloudDataStore.getInstance().confirmClaim(confirmClaimRequest, object : ConfirmClaimCallback {
            override fun onSuccessConfirmClaim(confirmClaimResponse: ConfirmClaimResponse) {
                FileUtil.deleteClaimFiles()
                lodgeClaimNetworkLiveData.value = Success
                confirmClaimLiveData.value = confirmClaimResponse
            }

            override fun onFailureConfirmClaim(errorCode: Int, message: String?) {
                if (Utility.isUnauthorized(errorCode))
                    confirmClaim(confirmClaimRequest)
                else
                    lodgeClaimNetworkLiveData.value = Failure(errorCode)
            }
        })
    }
}