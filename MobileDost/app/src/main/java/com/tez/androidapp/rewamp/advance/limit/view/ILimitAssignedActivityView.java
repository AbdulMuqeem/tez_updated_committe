package com.tez.androidapp.rewamp.advance.limit.view;

import com.tez.androidapp.app.base.ui.IBaseView;

public interface ILimitAssignedActivityView extends IBaseView {

    void setTvClockText(String text);

    void setClickListenerToRouteToSelectLoanActivity();

    void routeToDashboard();

    void setClContentVisibility(int visibility);
}
