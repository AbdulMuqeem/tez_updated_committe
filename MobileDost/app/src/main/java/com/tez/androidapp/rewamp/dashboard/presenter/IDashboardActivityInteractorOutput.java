package com.tez.androidapp.rewamp.dashboard.presenter;

import com.tez.androidapp.rewamp.dashboard.callbacks.DashboardActionCardCallback;
import com.tez.androidapp.rewamp.dashboard.callbacks.DashboardAdvanceCardCallback;
import com.tez.androidapp.rewamp.dashboard.callbacks.UserProfileStatusCallback;

public interface IDashboardActivityInteractorOutput extends DashboardAdvanceCardCallback,
        DashboardActionCardCallback,
        UserProfileStatusCallback {

}
