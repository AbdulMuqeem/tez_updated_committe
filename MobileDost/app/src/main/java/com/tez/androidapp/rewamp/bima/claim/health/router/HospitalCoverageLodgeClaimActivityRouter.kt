package com.tez.androidapp.rewamp.bima.claim.health.router

import android.content.Intent
import com.tez.androidapp.app.base.ui.activities.BaseActivity
import com.tez.androidapp.rewamp.bima.claim.health.view.HospitalCoverageLodgeClaimActivity

class HospitalCoverageLodgeClaimActivityRouter : BaseClaimLodgeActivityRouter() {
    override fun createIntent(from: BaseActivity): Intent =
            Intent(from, HospitalCoverageLodgeClaimActivity::class.java)

    companion object {
        fun createInstance(): HospitalCoverageLodgeClaimActivityRouter = HospitalCoverageLodgeClaimActivityRouter()
    }
}