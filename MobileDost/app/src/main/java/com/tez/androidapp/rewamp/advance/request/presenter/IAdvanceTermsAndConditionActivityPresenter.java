package com.tez.androidapp.rewamp.advance.request.presenter;

public interface IAdvanceTermsAndConditionActivityPresenter {

    void checkRemainingSteps();

    void applyForLoan();
}
