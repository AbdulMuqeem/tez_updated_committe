package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import net.tez.fragment.util.base.IBaseWidget;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

/**
 * Created by VINOD KUMAR on 6/17/2019.
 */
public class TezConstraintLayout extends ConstraintLayout implements IBaseWidget {

    public TezConstraintLayout(Context context) {
        super(context);
    }

    public TezConstraintLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TezConstraintLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setDoubleTapSafeOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        super.setOnClickListener(doubleTapSafeOnClickListener);
    }
}
