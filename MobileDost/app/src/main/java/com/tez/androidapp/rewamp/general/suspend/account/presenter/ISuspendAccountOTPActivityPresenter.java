package com.tez.androidapp.rewamp.general.suspend.account.presenter;

import androidx.annotation.NonNull;

/**
 * Created by VINOD KUMAR on 8/27/2019.
 */
public interface ISuspendAccountOTPActivityPresenter {

    void resendCode();

    void suspendAccount();

    void suspendAccountVerifyOtp(@NonNull String otp);
}
