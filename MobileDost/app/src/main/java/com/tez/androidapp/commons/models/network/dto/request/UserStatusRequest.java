package com.tez.androidapp.commons.models.network.dto.request;

import com.tez.androidapp.app.base.request.BaseRequest;

/**
 * Created by Rehman Murad Ali on 3/1/2018.
 */

public class UserStatusRequest extends BaseRequest {

    public static final String METHOD_NAME = "v1/user/status";

}
