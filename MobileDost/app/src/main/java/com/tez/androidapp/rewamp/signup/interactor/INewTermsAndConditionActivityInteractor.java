package com.tez.androidapp.rewamp.signup.interactor;

import com.tez.androidapp.commons.models.network.dto.request.UserLoginRequest;

public interface INewTermsAndConditionActivityInteractor {
    void callSetPin(String pin);

    void callLogin(UserLoginRequest loginRequest);
}
