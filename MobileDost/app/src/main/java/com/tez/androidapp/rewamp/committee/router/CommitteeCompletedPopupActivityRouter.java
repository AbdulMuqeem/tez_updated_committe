package com.tez.androidapp.rewamp.committee.router;

import android.content.Intent;
import android.os.Bundle;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.committee.response.CommitteeCreateResponse;
import com.tez.androidapp.rewamp.committee.response.JoinCommitteeResponse;
import com.tez.androidapp.rewamp.committee.view.CommitteeCompletedPopupActivity;

import androidx.annotation.NonNull;

public class CommitteeCompletedPopupActivityRouter extends BaseActivityRouter {


    public static CommitteeCompletedPopupActivityRouter createInstance() {
        return new CommitteeCompletedPopupActivityRouter();
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, CommitteeCreateResponse committeePackage) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putSerializable(CommitteeSummaryActivityRouter.COMMITTE_CREATEION, committeePackage);
        intent.putExtras(bundle);
        route(from, intent);
    }

    public void setDependenciesAndRoute(@NonNull BaseActivity from, JoinCommitteeResponse joinCommitteeResponse) {
        Intent intent = createIntent(from);
        Bundle bundle = new Bundle();
        bundle.putSerializable(CommitteeSummaryActivityRouter.JOIN_COMMITTEE_RESPONSE, joinCommitteeResponse);
        intent.putExtras(bundle);
        route(from, intent);
    }

    private Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, CommitteeCompletedPopupActivity.class);
    }

    public void setDependenciesAndRouteWithoutNewTask(@NonNull BaseActivity from) {
        Intent intent = createIntent(from);
        route(from, intent);
    }

}
