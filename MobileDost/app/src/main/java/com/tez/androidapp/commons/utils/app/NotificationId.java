package com.tez.androidapp.commons.utils.app;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by VINOD KUMAR on 7/11/2019.
 */
public class NotificationId {
    private final static AtomicInteger c = new AtomicInteger(0);
    public static int getID() {
        return c.incrementAndGet();
    }
}
