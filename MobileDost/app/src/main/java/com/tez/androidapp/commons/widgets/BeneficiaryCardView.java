package com.tez.androidapp.commons.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.tez.androidapp.R;
import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.TezDrawableHelper;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.viewbinder.library.core.BindView;
import net.tez.viewbinder.library.core.ViewBinder;

public class BeneficiaryCardView extends TezCardView {

    @BindView(R.id.tvMobileNo)
    private TezTextView tvMobileNo;

    @BindView(R.id.tvAddNumber)
    private TezTextView tvAddNumber;

    @BindView(R.id.tvName)
    private TezTextView tvName;

    @BindView(R.id.tvRelation)
    private TezTextView tvRelation;

    @BindView(R.id.ivIcon)
    private TezImageView ivIcon;

    @BindView(R.id.ivEdit)
    private TezImageView ivEdit;

    @BindView(R.id.tvAssign)
    private TezTextView tvAssign;

    @BindView(R.id.tvCoverage)
    private TezTextView tvCoverage;

    @BindView(R.id.tvCoverageAmount)
    private TezTextView tvCoverageAmount;

    @BindView(R.id.viewSeparator)
    private View viewSeparator;

    @BindView(R.id.clContent)
    private TezConstraintLayout clContent;

    public BeneficiaryCardView(@NonNull Context context) {
        this(context, null);
    }

    public BeneficiaryCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BeneficiaryCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.card_view_beneficiary, this);
        ViewBinder.bind(this, this);
    }

    @NonNull
    @Override
    public String getLabel() {
        return tvRelation.getLabel();
    }

    public void setName(String name) {
        tvName.setText(name);
    }

    public void setNumber(String number) {
        if (number == null) {
            tvAddNumber.setVisibility(VISIBLE);
            tvMobileNo.setVisibility(GONE);
        } else {
            tvMobileNo.setText(number);
            tvAddNumber.setVisibility(GONE);
            tvMobileNo.setVisibility(VISIBLE);
        }
    }

    public void setRelation(Integer relationshipId) {
        tvRelation.setText(Utility.getBeneficiaryRelationName(relationshipId));
    }

    public void setIvEditVisibility(int visibility) {
        ivEdit.setVisibility(visibility);
    }

    public void setDefault(boolean isDefault) {
        clContent.setBackground(isDefault ? TezDrawableHelper.getDrawable(getContext(), R.drawable.wallet_selected_bg) : null);
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) viewSeparator.getLayoutParams();
        int margin = dpToPx(getContext(), 1.5f);
        params.topMargin = isDefault ? margin : 0;
        params.bottomMargin = isDefault ? margin : 0;
        viewSeparator.setLayoutParams(params);
    }

    public void setRelationIcon(Integer relationshipId) {
        ivIcon.setImageResource(Utility.getBeneficiaryRelationImage(relationshipId));
    }

    public void setOnEditClickListener(DoubleTapSafeOnClickListener listener) {
        ivEdit.setDoubleTapSafeOnClickListener(listener);
        tvAddNumber.setDoubleTapSafeOnClickListener(listener);
    }

    public void hideAllocationViews() {
        viewSeparator.setVisibility(GONE);
        tvAssign.setVisibility(GONE);
        tvCoverageAmount.setVisibility(GONE);
        tvCoverage.setVisibility(GONE);
    }

    public void setCoverageAmount(@Nullable Double amount) {
        boolean visibleAmountView = amount == null || amount.intValue() > 0;
        viewSeparator.setVisibility(VISIBLE);
        tvCoverage.setVisibility(visibleAmountView ? VISIBLE : GONE);
        tvCoverageAmount.setVisibility(visibleAmountView ? VISIBLE : GONE);
        tvAssign.setVisibility(visibleAmountView ? GONE : VISIBLE);
        if (visibleAmountView)
            tvCoverageAmount.setText(amount == null ? "-" : getContext().getString(R.string.rs_value_string, Utility.getFormattedAmountWithoutCurrencyLabelAndDecimals(amount)));
    }

    public void setOnAssignCoverageClickListener(@Nullable DoubleTapSafeOnClickListener listener) {
        tvAssign.setDoubleTapSafeOnClickListener(listener);
        tvCoverage.setDoubleTapSafeOnClickListener(listener);
        tvCoverageAmount.setDoubleTapSafeOnClickListener(listener);
    }
}
