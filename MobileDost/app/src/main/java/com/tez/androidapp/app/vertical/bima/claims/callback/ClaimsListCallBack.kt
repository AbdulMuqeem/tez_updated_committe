package com.tez.androidapp.app.vertical.bima.claims.callback

import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.response.ClaimListResponse

/**
 * Created by Vinod Kumar on 10/14/2020.
 */
interface ClaimsListCallBack {
    fun onMyClaimsListSuccess(claimListResponse: ClaimListResponse)
    fun onMyClaimsListFailure(errorCode: Int, message: String?)
}