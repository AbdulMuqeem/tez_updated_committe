package com.tez.androidapp.rewamp.committee.interactor;

import com.tez.androidapp.app.base.interactor.IBaseInteractor;
import com.tez.androidapp.rewamp.committee.request.JoinCommitteeVerificationRequest;

/**
 * Created by Ahmad Izaz on 07-Nov-20
 **/
public interface IJoinCommitteeVerificationActivityInteractor extends IBaseInteractor {

    void verifyOtp(JoinCommitteeVerificationRequest joinCommitteeVerificationRequest);
}
