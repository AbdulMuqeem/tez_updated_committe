package com.tez.androidapp.commons.models.extract;

import android.bluetooth.BluetoothDevice;
import android.os.Build;

/**
 * Created  on 12/8/2016.
 */

public class ExtractedBluetooth {

    private String name;
    private String type;
    private String bondState;
    private String deviceType;
    private String address;

    public ExtractedBluetooth(String name, String type, String bondState, String deviceType, String address) {
        this.name = name;
        this.type = type;
        this.bondState = bondState;
        this.deviceType = deviceType;
        this.address = address;
    }

    public ExtractedBluetooth(BluetoothDevice bluetoothDevice) {
        this.name = bluetoothDevice.getName();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
            this.type = String.valueOf(bluetoothDevice.getType());
        else this.type = "";
        this.bondState = String.valueOf(bluetoothDevice.getBondState());
        this.deviceType = String.valueOf(bluetoothDevice.getBluetoothClass().getDeviceClass());
        this.address = bluetoothDevice.getAddress();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBondState() {
        return bondState;
    }

    public void setBondState(String bondState) {
        this.bondState = bondState;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
