package com.tez.androidapp.rewamp.general.wallet.router;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.activities.BaseActivity;
import com.tez.androidapp.rewamp.base.router.BaseActivityRouter;
import com.tez.androidapp.rewamp.general.wallet.view.AddWalletTermsAndConditionActivity;

public class AddWalletTermsAndConditionActivityRouter extends BaseActivityRouter {

    public static final int REQUEST_CODE_ADD_WALLET = 19929;

    public static final String SERVICE_PROVIDER_ID = "SERVICE_PROVIDER_ID";
    public static final String MOBILE_ACCOUNT_NUMBER = "MOBILE_ACCOUNT_NUMBER";
    public static final String MOBILE_ACCOUNT_ID = "MOBILE_ACCOUNT_ID";
    public static final String IS_DEFAULT = "IS_DEFAULT";

    public static AddWalletTermsAndConditionActivityRouter createInstance() {
        return new AddWalletTermsAndConditionActivityRouter();
    }

    public void setDependenciesAndRouteForResult(@NonNull BaseActivity from,
                                                 int serviceProviderId,
                                                 String mobileAccountNumber,
                                                 Integer mobileUserAccountId,
                                                 boolean isDefault) {
        Intent intent = createIntent(from);
        intent.putExtra(SERVICE_PROVIDER_ID, serviceProviderId);
        intent.putExtra(MOBILE_ACCOUNT_NUMBER, mobileAccountNumber);
        intent.putExtra(IS_DEFAULT, isDefault);
        intent.putExtra(MOBILE_ACCOUNT_ID, mobileUserAccountId);
        routeForResult(from, intent, getRequestCode());
    }

    protected Intent createIntent(@NonNull BaseActivity from) {
        return new Intent(from, AddWalletTermsAndConditionActivity.class);
    }

    protected int getRequestCode() {
        return REQUEST_CODE_ADD_WALLET;
    }

}
