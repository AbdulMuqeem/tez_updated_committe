package com.tez.androidapp.rewamp.general.termsandcond;

import androidx.annotation.NonNull;

import com.tez.androidapp.rewamp.signup.TermsAndCondition;

/**
 * Created by VINOD KUMAR on 8/26/2019.
 */
class NavigationTerm extends TermsAndCondition {

    @NonNull
    private String fileName;

    NavigationTerm(@NonNull String fileName, @NonNull String termsAndCondtion) {
        super(termsAndCondtion);
        this.fileName = fileName;
    }

    @NonNull
    String getFileName() {
        return fileName;
    }
}
