package com.tez.androidapp.rewamp.cloud.client;

import com.tez.androidapp.rewamp.cloud.ApiClient;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public abstract class DecoratorClient extends BaseClient {

    protected final BaseClient baseClient;

    public DecoratorClient(BaseClient baseClient){
        this.baseClient= baseClient;
    }


    @Override
    public ApiClient build() {
        return this.baseClient.build(buildOkHttpClient());
    }

    @Override
    protected ApiClient build(OkHttpClient.Builder okBuilder) {
        return this.baseClient.build(okBuilder);
    }

    @Override
    protected Retrofit.Builder buildRetrofitClient(OkHttpClient.Builder okBuilder) {
        return this.baseClient.buildRetrofitClient(okBuilder);
    }

    @Override
    protected <T> T buildClient(Class<T> apiInterface, OkHttpClient.Builder oBuilder) {
        return this.baseClient.buildClient(apiInterface, oBuilder);
    }
}
