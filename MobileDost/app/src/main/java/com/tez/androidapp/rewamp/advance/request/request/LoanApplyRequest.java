package com.tez.androidapp.rewamp.advance.request.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.tez.androidapp.app.base.request.BaseRequest;
import com.tez.androidapp.commons.models.network.DeviceInfo;

/**
 * Created  on 3/22/2017.
 */

public class LoanApplyRequest extends BaseRequest implements Parcelable {

    public static final String METHOD_NAME = "v1/loan/apply";
    public static final Creator<LoanApplyRequest> CREATOR = new Creator<LoanApplyRequest>() {
        @Override
        public LoanApplyRequest createFromParcel(Parcel in) {
            return new LoanApplyRequest(in);
        }

        @Override
        public LoanApplyRequest[] newArray(int size) {
            return new LoanApplyRequest[size];
        }
    };
    private Integer walletServiceProviderId;
    private Integer duration;
    private Integer loanId;
    private Integer mobileAccountId;
    private Double interestRate;
    private Double latePaymentCharges;
    private Double processingFee;
    private Double amount;
    private Double lat;
    private Double lng;
    private DeviceInfo deviceInfo;
    private Integer purposeId;
    private String otherReason;
    private String loanOutstandingAmountRange;
    private String loanTakenFromType;
    private String loanTakenFromName;


    public LoanApplyRequest() {
    }

    protected LoanApplyRequest(Parcel in) {
        if (in.readByte() == 0) {
            walletServiceProviderId = null;
        } else {
            walletServiceProviderId = in.readInt();
        }
        if (in.readByte() == 0) {
            duration = null;
        } else {
            duration = in.readInt();
        }
        if (in.readByte() == 0) {
            loanId = null;
        } else {
            loanId = in.readInt();
        }
        if (in.readByte() == 0) {
            mobileAccountId = null;
        } else {
            mobileAccountId = in.readInt();
        }
        if (in.readByte() == 0) {
            interestRate = null;
        } else {
            interestRate = in.readDouble();
        }
        if (in.readByte() == 0) {
            latePaymentCharges = null;
        } else {
            latePaymentCharges = in.readDouble();
        }
        if (in.readByte() == 0) {
            processingFee = null;
        } else {
            processingFee = in.readDouble();
        }
        if (in.readByte() == 0) {
            amount = null;
        } else {
            amount = in.readDouble();
        }
        if (in.readByte() == 0) {
            lat = null;
        } else {
            lat = in.readDouble();
        }
        if (in.readByte() == 0) {
            lng = null;
        } else {
            lng = in.readDouble();
        }
        if (in.readByte() == 0) {
            purposeId = null;
        } else {
            purposeId = in.readInt();
        }
        otherReason = in.readString();
        loanOutstandingAmountRange = in.readString();
        loanTakenFromType = in.readString();
        loanTakenFromName = in.readString();
        deviceInfo = in.readParcelable(DeviceInfo.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (walletServiceProviderId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(walletServiceProviderId);
        }
        if (duration == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(duration);
        }
        if (loanId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(loanId);
        }
        if (mobileAccountId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(mobileAccountId);
        }
        if (interestRate == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(interestRate);
        }
        if (latePaymentCharges == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(latePaymentCharges);
        }
        if (processingFee == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(processingFee);
        }
        if (amount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(amount);
        }
        if (lat == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(lat);
        }
        if (lng == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(lng);
        }
        if (purposeId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(purposeId);
        }
        dest.writeString(otherReason);
        dest.writeString(loanOutstandingAmountRange);
        dest.writeString(loanTakenFromType);
        dest.writeString(loanTakenFromName);
        dest.writeParcelable(deviceInfo, 1);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void setPurposeId(Integer purposeId) {
        this.purposeId = purposeId;
    }

    public void setOtherReason(String otherReason) {
        this.otherReason = otherReason;
    }

    public void setLoanOutstandingAmountRange(String loanOutstandingAmountRange) {
        this.loanOutstandingAmountRange = loanOutstandingAmountRange;
    }

    public void setLoanTakenFromType(String loanTakenFromType) {
        this.loanTakenFromType = loanTakenFromType;
    }

    public void setLoanTakenFromName(String loanTakenFromName) {
        this.loanTakenFromName = loanTakenFromName;
    }

    public Integer getMobileAccountId() {
        return mobileAccountId;
    }

    public void setMobileAccountId(Integer mobileAccountId) {
        this.mobileAccountId = mobileAccountId;
    }

    public void setWalletServiceProviderId(Integer walletServiceProviderId) {
        this.walletServiceProviderId = walletServiceProviderId;
    }

    public Integer getWalletServiceProviderId() {
        return walletServiceProviderId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public Double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(Double interestRate) {
        this.interestRate = interestRate;
    }

    public Double getLatePaymentCharges() {
        return latePaymentCharges;
    }

    public void setLatePaymentCharges(Double latePaymentCharges) {
        this.latePaymentCharges = latePaymentCharges;
    }

    public Double getProcessingFee() {
        return processingFee;
    }

    public void setProcessingFee(Double processingFee) {
        this.processingFee = processingFee;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}


