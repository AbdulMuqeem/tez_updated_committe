package com.tez.androidapp.app.vertical.advance.loan.repayment.shared.models.network.dto.response;

import com.tez.androidapp.commons.models.network.LoanDetails;
import com.tez.androidapp.rewamp.dashboard.response.DashboardCardsResponse;

/**
 * Created  on 8/29/2017.
 */

public class RepayLoanResponse extends DashboardCardsResponse {

    private LoanDetails loanDetails;

    public LoanDetails getLoanDetails() {
        return loanDetails;
    }

}
