package com.tez.androidapp.app.general.feature.account.feature.profile.models.network.dto.request;

public class QuestionOptionsRequest {
    public static final String METHOD_NAME = "query/question";


    public static final class Params {
        public static final String LANG = "lang";
        public static final String REF_NAME = "refName";

        public static final String OCCUPATION = "Occupation";
        public static final String MARITAL_STATUS = "marital_status";

        private Params() {
        }
    }
}
