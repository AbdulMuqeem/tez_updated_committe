package com.tez.androidapp.app.general.feature.questions.models.network.models

/**
 * Created by Vinod Kumar on 5/7/2020.
 */
sealed class ClaimQuestion(val question: String)

class LocationQuestion(
        question: String,
        var nearestLandMark: String? = null,
        var hospitalLocation: String? = null,
        var hospitalName: String? = null,
        var hospitalNameMap: String? = null
) : ClaimQuestion(question)

class DateQuestion(
        question: String,
        var date: String? = null
) : ClaimQuestion(question)