package com.tez.androidapp.rewamp.cloud.client;

import androidx.annotation.NonNull;

import com.tez.androidapp.commons.utils.app.Utility;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AuthClientInterceptor extends DecoratorClient implements Interceptor {

    public AuthClientInterceptor(BaseClient baseClient){
        super(baseClient);
    }


    @Override
    public OkHttpClient.Builder buildOkHttpClient() {
        return baseClient.buildOkHttpClient().addInterceptor(this);
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request originalRequest = chain.request();
        String auth = Utility.getAuthorizationHeaderValue();
        if (auth != null) {
            originalRequest = originalRequest.newBuilder().header("Authorization", auth).build();
        } else {
            Utility.directUserToIntroScreenAtGlobalLevel();
        }
        return chain.proceed(originalRequest);
    }
}
