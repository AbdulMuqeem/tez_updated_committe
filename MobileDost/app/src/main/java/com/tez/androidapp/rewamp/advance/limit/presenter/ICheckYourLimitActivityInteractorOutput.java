package com.tez.androidapp.rewamp.advance.limit.presenter;

import com.tez.androidapp.app.vertical.advance.loan.questions.callbacks.UploadDataStatusCallback;
import com.tez.androidapp.app.vertical.advance.loan.shared.limit.callbacks.LoanLimitCallback;

public interface ICheckYourLimitActivityInteractorOutput extends LoanLimitCallback, UploadDataStatusCallback {
}
