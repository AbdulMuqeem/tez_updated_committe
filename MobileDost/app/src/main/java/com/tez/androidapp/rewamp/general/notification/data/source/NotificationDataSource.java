package com.tez.androidapp.rewamp.general.notification.data.source;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.PageKeyedDataSource;

import com.tez.androidapp.app.general.feature.notification.callbacks.UserNotificationsCallback;
import com.tez.androidapp.app.general.feature.notification.models.netwrok.Notification;
import com.tez.androidapp.repository.network.store.UserAuthCloudDataStore;

import java.util.List;

public class NotificationDataSource extends PageKeyedDataSource<Integer, Notification> {

    public static final int PAGE_SIZE = 10;
    private static final int FIRST_PAGE = 0;

    private final MutableLiveData<NetworkState> networkState = new MutableLiveData<>();


    public MutableLiveData<NetworkState> getNetworkState() {
        return networkState;
    }



    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, Notification> callback) {
        networkState.postValue(NetworkState.LOADING);

        UserAuthCloudDataStore.getInstance()
                .getUserNotifications(FIRST_PAGE, new UserNotificationsCallback() {
                    @Override
                    public void onUserNotificationsSuccess(int recordCount, List<Notification> notifications) {
                        callback.onResult(notifications, null, FIRST_PAGE+PAGE_SIZE);
                        networkState.postValue(NetworkState.LOADED);
                    }

                    @Override
                    public void onUserNotificationsError(int errorCode, String message) {
                        networkState.postValue(new NetworkState(NetworkState.Status.FAILED, errorCode));

                    }
                });
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Notification> callback) {
        networkState.postValue(NetworkState.LOADING);
        UserAuthCloudDataStore.getInstance().getUserNotifications(params.key, new UserNotificationsCallback() {
            @Override
            public void onUserNotificationsSuccess(int recordCount, List<Notification> notificationList) {
                callback.onResult(notificationList,  params.key -PAGE_SIZE);
                networkState.postValue(NetworkState.LOADED);
            }

            @Override
            public void onUserNotificationsError(int errorCode, String message) {
                networkState.postValue(new NetworkState(NetworkState.Status.FAILED, errorCode));
            }
        });
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Notification> callback) {
        networkState.postValue(NetworkState.LOADING);
        UserAuthCloudDataStore.getInstance().getUserNotifications(params.key, new UserNotificationsCallback() {
            @Override
            public void onUserNotificationsSuccess(int recordCount, List<Notification> notificationList) {
                Integer key = notificationList.size()>0 ? params.key + PAGE_SIZE : null;
                callback.onResult(notificationList, key);
                networkState.postValue(NetworkState.LOADED);
            }

            @Override
            public void onUserNotificationsError(int errorCode, String message) {
                networkState.postValue(new NetworkState(NetworkState.Status.FAILED, errorCode));
            }
        });
    }
}
