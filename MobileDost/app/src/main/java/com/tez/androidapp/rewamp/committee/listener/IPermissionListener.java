package com.tez.androidapp.rewamp.committee.listener;

/**
 * Created by Ahmad Izaz on 21-Nov-20
 **/
public interface IPermissionListener {
    public void onPermissionGranted();
}
