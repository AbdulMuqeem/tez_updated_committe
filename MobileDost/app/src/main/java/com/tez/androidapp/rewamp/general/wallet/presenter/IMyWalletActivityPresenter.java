package com.tez.androidapp.rewamp.general.wallet.presenter;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.general.feature.wallet.my.wallet.models.Wallet;

public interface IMyWalletActivityPresenter {

    void getAllWallets();

    void setDefaultWallet(@NonNull Wallet wallet);

    void deleteWallet(int mobileAccountId);
}
