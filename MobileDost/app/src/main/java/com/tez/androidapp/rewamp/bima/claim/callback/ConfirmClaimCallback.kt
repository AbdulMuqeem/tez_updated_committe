package com.tez.androidapp.rewamp.bima.claim.callback

import com.tez.androidapp.rewamp.bima.claim.response.ConfirmClaimResponse

interface ConfirmClaimCallback {

    fun onSuccessConfirmClaim(confirmClaimResponse: ConfirmClaimResponse)

    fun onFailureConfirmClaim(errorCode: Int, message: String?)
}