package com.tez.androidapp.rewamp.bima.claim.revise.listener

import com.tez.androidapp.rewamp.bima.claim.model.InsuranceClaimConfirmationDto

interface ReviseClaimListener {

    fun onClaimSubmitted(insuranceClaimConfirmationDto: InsuranceClaimConfirmationDto)
}