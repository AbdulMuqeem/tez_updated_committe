package com.tez.androidapp.rewamp.general.transactions;

import androidx.annotation.NonNull;

import com.tez.androidapp.app.base.ui.IBaseView;
import com.tez.androidapp.app.general.feature.transactions.models.network.Transaction;

import java.util.List;

/**
 * Created by VINOD KUMAR on 8/28/2019.
 */
public interface IMyTransactionsActivityView extends IBaseView {

    void setMyTransactionsAdapter(@NonNull List<Transaction> myTransactionList);

    void setLayoutEmptyTransactionVisibility(int visibility);

    void setRvMyActivitiesVisibility(int visibility);
}
