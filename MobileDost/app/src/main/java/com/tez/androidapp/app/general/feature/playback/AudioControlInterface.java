package com.tez.androidapp.app.general.feature.playback;

import androidx.annotation.NonNull;
import androidx.annotation.RawRes;
import androidx.lifecycle.DefaultLifecycleObserver;

/**
 * Created by VINOD KUMAR on 5/13/2019.
 */
public interface AudioControlInterface {

    @NonNull
    DefaultLifecycleObserver getObserver();

    void setAudioId(@RawRes int audioResId);

    void startPlayer();

    void stopPlayer();

    void startTimer();

    void stopTimer();

    void release();

}
