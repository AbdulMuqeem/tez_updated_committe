package com.tez.androidapp.app.general.feature.wallet.my.wallet.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.tez.androidapp.commons.utils.app.Utility;

import java.io.Serializable;

/**
 * Created by Rehman Murad Ali on 12/6/2017.
 */

public class Wallet implements Serializable, Parcelable {
    public static final Creator<Wallet> CREATOR = new Creator<Wallet>() {
        @Override
        public Wallet createFromParcel(Parcel in) {
            return new Wallet(in);
        }

        @Override
        public Wallet[] newArray(int size) {
            return new Wallet[size];
        }
    };
    private int mobileAccountId;
    private String mobileAccountNumber;
    private int serviceProviderId;
    private int externalServiceProviderId;
    private String cnic;
    private String ownerName;
    private boolean isDefault;
    private String walletLinkingTimestamp;

    protected Wallet(Parcel in) {
        mobileAccountId = in.readInt();
        mobileAccountNumber = in.readString();
        serviceProviderId = in.readInt();
        externalServiceProviderId = in.readInt();
        cnic = in.readString();
        ownerName = in.readString();
        isDefault = in.readByte() != 0;
        walletLinkingTimestamp = in.readString();
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public int getMobileAccountId() {
        return mobileAccountId;
    }

    public void setMobileAccountId(int mobileAccountId) {
        this.mobileAccountId = mobileAccountId;
    }

    public String getMobileAccountNumber() {
        return mobileAccountNumber;
    }

    public void setMobileAccountNumber(String mobileAccountNumber) {
        this.mobileAccountNumber = mobileAccountNumber;
    }

    public String getMobileAccountNumberFormatted() {
        return "0" + mobileAccountNumber.substring(2);
    }

    public int getServiceProviderId() {
        return serviceProviderId;
    }

    public void setServiceProviderId(int serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }

    public int getExternalServiceProviderId() {
        return externalServiceProviderId;
    }

    public void setExternalServiceProviderId(int externalServiceProviderId) {
        this.externalServiceProviderId = externalServiceProviderId;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getWalletLinkingTimestamp() {
        return Utility.getFormattedDateWithUpdatedTimeZone(walletLinkingTimestamp, "yyyy-MM-dd'T'HH:mm:ss.SSSZ", "dd, MMM yy");
    }

    public void setWalletLinkingTimestamp(String walletLinkingTimestamp) {
        this.walletLinkingTimestamp = walletLinkingTimestamp;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mobileAccountId);
        dest.writeString(mobileAccountNumber);
        dest.writeInt(serviceProviderId);
        dest.writeInt(externalServiceProviderId);
        dest.writeString(cnic);
        dest.writeString(ownerName);
        dest.writeByte((byte) (isDefault ? 1 : 0));
        dest.writeString(walletLinkingTimestamp);
    }
}
