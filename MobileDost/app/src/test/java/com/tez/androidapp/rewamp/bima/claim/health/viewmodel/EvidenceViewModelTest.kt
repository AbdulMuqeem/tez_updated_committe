package com.tez.androidapp.rewamp.bima.claim.health.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.EvidenceFile
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class EvidenceViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var evidenceViewModel: EvidenceViewModel

    @Before
    fun setUp() {
        evidenceViewModel = EvidenceViewModel()
    }

    @Test
    fun evidenceListLiveData_SizeShouldBeFour() {
        assertThat(evidenceViewModel.evidenceListLiveData.value).hasSize(4)
    }

    @Test
    fun getEvidenceFileList_DocId_EmptyList() {
        assertThat(evidenceViewModel.getEvidenceFileList(296)).isEmpty()
    }

    @Test
    fun updateEvidenceFileList_ValidDocId_UpdateOriginalList() {
        val docId = 296
        val list = mutableListOf(
                EvidenceFile(docId, "path"),
                EvidenceFile(docId, "path")
        )
        evidenceViewModel.updateEvidenceFileList(docId, list)
        assertThat(list).isEqualTo(evidenceViewModel.evidenceListLiveData.value?.find { it.id == docId }?.evidenceFileList)
    }

    @Test
    fun updateEvidenceFileList_InvalidDocId_DoNotUpdateOriginalList() {
        val docId = 38947
        val list = mutableListOf(
                EvidenceFile(docId, "path"),
                EvidenceFile(docId, "path")
        )
        evidenceViewModel.updateEvidenceFileList(docId, list)
        evidenceViewModel.evidenceListLiveData.value?.forEach {
            assertThat(it.evidenceFileList).isNotEqualTo(list)
        }
    }
}