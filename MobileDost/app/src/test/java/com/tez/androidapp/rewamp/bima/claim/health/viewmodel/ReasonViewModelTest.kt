package com.tez.androidapp.rewamp.bima.claim.health.viewmodel


import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test

class ReasonViewModelTest {

    private lateinit var reasonViewModel: ReasonViewModel

    @Before
    fun setup() {
        reasonViewModel = ReasonViewModel()
    }

    @Test
    fun reasonsListLiveData_MustContainTenReasons() {
        assertThat(reasonViewModel.purposeListLiveData.value).hasSize(10)
    }

    @Test
    fun getRandomOrderReasons_TwoRandomOrderedList_ShouldNotBeEqual() {
        val list1 = reasonViewModel.getRandomOrderReasons()
        val list2 = reasonViewModel.getRandomOrderReasons()
        assertThat(list1).isNotEqualTo(list2)

    }
}