package com.tez.androidapp.rewamp.bima.claim.common.summary.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.Evidence
import com.tez.androidapp.app.vertical.bima.claims.models.network.models.dto.EvidenceFile
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SummaryViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var summaryViewModel: SummaryViewModel

    @Before
    fun setup() {
        summaryViewModel = SummaryViewModel()
    }

    @Test
    fun initiateQueue() {
        val evidenceFileList = mutableListOf(
                EvidenceFile(296, "Path")
        )
        val evidenceList = listOf(
                Evidence(1, 0, evidenceFileList, false)
        )
        summaryViewModel.initiateQueue(evidenceList)
        assertThat(evidenceFileList).isEqualTo(summaryViewModel.evidenceFilesQueue)
    }

    @Test
    fun getWallet() {

    }

    @Test
    fun uploadDocument() {

    }

    @Test
    fun confirmClaim() {

    }
}