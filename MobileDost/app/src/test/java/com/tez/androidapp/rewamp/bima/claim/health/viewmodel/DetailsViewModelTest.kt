package com.tez.androidapp.rewamp.bima.claim.health.viewmodel

import com.google.common.truth.Truth.assertThat
import com.tez.androidapp.R
import org.junit.Before
import org.junit.Test

class DetailsViewModelTest {

    private lateinit var detailsViewModel: DetailsViewModel

    @Before
    fun setUp() {
        detailsViewModel = DetailsViewModel()
    }

    @Test
    fun validate_AdmissionDateBeforeActivationDate_ReturnPair() {
        val pair = detailsViewModel.validate(
                activationDate = "03-02-2020",
                expiryDate = "01-01-2020",
                admissionDate = "01-02-2020",
                dischargeDate = "01-01-2020",
                nameOfHospital = ""
        )
        assertThat(pair?.first).isEqualTo(1)
        assertThat(pair?.second).isEqualTo(R.string.code_3007)
    }

    @Test
    fun validate_AdmissionDateAfterClaimDate_ReturnPair() {
        val pair = detailsViewModel.validate(
                activationDate = "03-02-2020",
                admissionDate = "03-02-2021",
                expiryDate = "05-02-2020",
                dischargeDate = "03-02-2020",
                nameOfHospital = ""
        )
        assertThat(pair?.first).isEqualTo(1)
        assertThat(pair?.second).isEqualTo(R.string.code_3008)
    }

    @Test
    fun validate_AdmissionDateAfterExpiryDate_ReturnPair() {
        val pair = detailsViewModel.validate(
                activationDate = "01-02-2020",
                expiryDate = "02-02-2020",
                admissionDate = "03-02-2020",
                dischargeDate = "01-01-2020",
                nameOfHospital = ""
        )
        assertThat(pair?.first).isEqualTo(1)
        assertThat(pair?.second).isEqualTo(R.string.code_3009)
    }

    @Test
    fun validate_AdmissionDateAfterDischargeDate_ReturnPair() {
        val pair = detailsViewModel.validate(
                activationDate = "01-02-2020",
                expiryDate = "03-02-2020",
                admissionDate = "02-02-2020",
                dischargeDate = "01-02-2020",
                nameOfHospital = ""
        )
        assertThat(pair?.first).isEqualTo(2)
        assertThat(pair?.second).isEqualTo(R.string.code_3010)
    }

    @Test
    fun validate_DischargeDateAfterClaimDate_ReturnPair() {
        val pair = detailsViewModel.validate(
                activationDate = "01-02-2020",
                expiryDate = "03-02-2020",
                admissionDate = "02-02-2020",
                dischargeDate = "01-02-2021",
                nameOfHospital = ""
        )
        assertThat(pair?.first).isEqualTo(2)
        assertThat(pair?.second).isEqualTo(R.string.code_3011)
    }

    @Test
    fun validate_NameOfHospital_ReturnNonNull() {
        val pair = detailsViewModel.validate(
                activationDate = "01-02-2020",
                expiryDate = "03-02-2020",
                admissionDate = "02-02-2020",
                dischargeDate = "03-02-2020",
                nameOfHospital = ""
        )
        assertThat(pair?.first).isEqualTo(3)
        assertThat(pair?.second).isEqualTo(R.string.name_of_hospital_empty_error)
    }

    @Test
    fun validate_DischargeDateAfterClaimDate_ReturnNull() {
        val pair = detailsViewModel.validate(
                activationDate = "01-02-2020",
                expiryDate = "03-02-2020",
                admissionDate = "02-02-2020",
                dischargeDate = "03-02-2020",
                nameOfHospital = "Name"
        )
        assertThat(pair).isNull()
    }
}