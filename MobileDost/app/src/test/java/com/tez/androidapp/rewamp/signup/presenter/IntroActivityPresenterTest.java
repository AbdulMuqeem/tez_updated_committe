package com.tez.androidapp.rewamp.signup.presenter;

import com.tez.androidapp.commons.utils.app.Utility;
import com.tez.androidapp.rewamp.signup.interactor.IIntroActivityInteractor;
import com.tez.androidapp.rewamp.signup.view.IIntroActivityView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class IntroActivityPresenterTest {

    private static final String MOBILE_NUMBER = "03313232336";
    private static final String SOCIAL_ID = "989829374923";
    private static final String PRINCIPAL_NAME = "45101987464";
    private static final int SOCIAL_TYPE = Utility.PrincipalType.MOBILE_NUMBER.getValue();

    @Mock
    private IIntroActivityView view;

    @Mock
    private IIntroActivityInteractor interactor;

    private IntroActivityPresenter presenter;


    @Before
    public void setUp() {
        this.presenter = new IntroActivityPresenter(view, interactor);
    }


    @Test
    public void callUserInfo() {
        presenter.callUserInfo(MOBILE_NUMBER, null, Utility.PrincipalType.MOBILE_NUMBER.getValue());
        verify(view).showTezLoader();
        verify(interactor).callUserInfo(MOBILE_NUMBER, null, Utility.PrincipalType.MOBILE_NUMBER.getValue());
    }

    @Test
    public void onUserInfoSuccess_with_registered_user() {
        presenter.onUserInfoSuccess(MOBILE_NUMBER, SOCIAL_ID, PRINCIPAL_NAME, SOCIAL_TYPE, true);
        verify(view).navigateToLoginActivity(PRINCIPAL_NAME, SOCIAL_ID, SOCIAL_TYPE);
    }

    @Test
    public void onUserInfoSuccess_with_unregistered_user() {
        presenter.onUserInfoSuccess(MOBILE_NUMBER, SOCIAL_ID, PRINCIPAL_NAME, SOCIAL_TYPE, false);
        verify(view).navigateToSignUpActivity(MOBILE_NUMBER, SOCIAL_ID, SOCIAL_TYPE);
    }

    @Test
    public void onUserInfoFailure() {
        presenter.onUserInfoFailure(1, "");
        verify(view).dismissTezLoader();
        verify(view).showError(1);
    }
}