package com.tez.androidapp.rewamp.signup;

import org.junit.Test;

import static org.junit.Assert.*;

public class TezOnboardingActivityTest {

    @Test
    public void check_correct_list_size_is_returned() {
        TezOnboardingActivity activity = new TezOnboardingActivity();
        assertEquals(activity.getOnboardingContentList().size(), 3);
    }
}