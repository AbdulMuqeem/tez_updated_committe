package com.tez.androidapp.rewamp.signup.presenter;

import com.tez.androidapp.commons.models.network.dto.response.UserLoginResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserSignUpSetPinResponse;
import com.tez.androidapp.rewamp.signup.interactor.INewTermsAndConditionActivityInteractor;
import com.tez.androidapp.rewamp.signup.view.INewTermsAndConditionActivityView;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class NewTermsAndConditionActivityPresenterTest {

    @Mock
    private INewTermsAndConditionActivityView view;

    @Mock
    private INewTermsAndConditionActivityInteractor interactor;

    private NewTermsAndConditionActivityPresenter presenter;

    @Before
    public void setUp() {
        this.presenter = new NewTermsAndConditionActivityPresenter(interactor, view);
    }

    @Test
    public void callSetPin() {
        presenter.callSetPin("0000");
        verify(interactor).callSetPin("0000");
    }

    @Test
    public void onUserSignUpSetPinSuccess() {
        presenter.onUserSignUpSetPinSuccess(mock(UserSignUpSetPinResponse.class), "0000");
        verify(interactor).callLogin(any());
    }

    @Test
    public void callLogin() {
        presenter.callLogin("0000");
        verify(interactor).callLogin(any());
    }

    @Test
    public void onUserSignUpSetPinError() {
        presenter.onUserSignUpSetPinError(199, "");
        verify(view).dismissTezLoader();
        verify(view).showError(199);
    }

    @Test
    public void onUserLoginSuccess() {
        presenter.onUserLoginSuccess(mock(UserLoginResponse.class));
        verify(view).setTezLoaderToBeDissmissedOnTransition();
        verify(view).navigateToDashBoardActivity();
    }

    @Test
    public void onUserLoginFailure_with_signup_cash_timeout() {
        presenter.onUserLoginFailure(ResponseStatusCode.SIGNUP_CACHE_TIMEOUT.getCode(), "");
        verify(view).setTezLoaderToBeDissmissedOnTransition();
    }

    @Test
    public void onUserLoginFailure() {
        presenter.onUserLoginFailure(861, "");
        verify(view).dismissTezLoader();
    }
}