package com.tez.androidapp.rewamp.signup.presenter;

import com.tez.androidapp.app.base.response.BaseResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserLoginResponse;
import com.tez.androidapp.rewamp.signup.interactor.INewLoginActivityInteractor;
import com.tez.androidapp.rewamp.signup.view.INewLoginActivityView;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NewLoginActivityPresenterTest {

    @Mock
    private INewLoginActivityView view;

    @Mock
    private INewLoginActivityInteractor interactor;

    private NewLoginActivityPresenter presenter;


    @Before
    public void setUp() {
        this.presenter = new NewLoginActivityPresenter(view, interactor);
    }

    @Test
    public void callLogin() {
        presenter.callLogin(any());
        verify(interactor).callLogin(any());
    }

    @Test
    public void onUserLoginSuccess_with_temporary_pin_set() {
        UserLoginResponse userLoginResponse = mock(UserLoginResponse.class);
        when(userLoginResponse.getTemporaryPinSet()).thenReturn(true);
        presenter.onUserLoginSuccess(userLoginResponse);
        verify(view).onTemporaryPinSet();
        verify(view, never()).onUserReactivateAccount();
        verify(view, never()).userPinIsVerified();
    }

    @Test
    public void onUserLoginSuccess_with_with_user_activation_pending() {
        UserLoginResponse userLoginResponse = mock(UserLoginResponse.class);
        when(userLoginResponse.isUserActivationPending()).thenReturn(true);
        presenter.onUserLoginSuccess(userLoginResponse);
        verify(view).onUserReactivateAccount();
        verify(view, never()).onTemporaryPinSet();
        verify(view, never()).userPinIsVerified();
    }

    @Test
    public void onUserLoginSuccess_with_user_pin_is_verified() {
        UserLoginResponse userLoginResponse = mock(UserLoginResponse.class);
        presenter.onUserLoginSuccess(userLoginResponse);
        verify(view, never()).onUserReactivateAccount();
        verify(view, never()).onTemporaryPinSet();
        verify(view).userPinIsVerified();
    }

    @Test
    public void onUserLoginFailure() {
        presenter.onUserLoginFailure(500, "");
        verify(view).dismissTezLoader();
        verify(view).showError(500, view.getDialogOnClickListener());
    }

    @Test
    public void onUserLoginFailure_with_user_is_locked() {
        presenter.onUserLoginFailure(ResponseStatusCode.USER_LOCKED.getCode(), "");
        verify(view).startRequestFailedActivity();
    }


    @Test
    public void resendOtp() {
        presenter.resendOtp();
        verify(interactor).resendAccountReactivationOtp();
    }

    @Test
    public void onUserAccountReActivateResendOTPSuccess() {
        presenter.onUserAccountReActivateResendOTPSuccess(mock(BaseResponse.class));
        verify(view).startUserReactiviateAccount();
    }

    @Test
    public void onUserAccountReActivateResendOTPFailure() {
        BaseResponse response = mock(BaseResponse.class);
        when(response.getStatusCode()).thenReturn(129);
        presenter.onUserAccountReActivateResendOTPFailure(response);
        verify(view).showError(129);
    }
}