package com.tez.androidapp.rewamp.signup.presenter;

import android.location.Location;

import com.tez.androidapp.commons.models.network.DeviceInfo;
import com.tez.androidapp.commons.models.network.dto.response.UserLoginResponse;
import com.tez.androidapp.commons.models.network.dto.response.UserVerifyResponse;
import com.tez.androidapp.repository.network.models.response.VerifyActiveDeviceResponse;
import com.tez.androidapp.rewamp.signup.interactor.IWelcomeBackActivityInteractor;
import com.tez.androidapp.rewamp.signup.view.IWelcomeBackActivityView;
import com.tez.androidapp.rewamp.util.ResponseStatusCode;

import net.tez.fragment.util.listener.DoubleTapSafeDialogClickListener;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WelcomeBackActivityPresenterTest {

    @Mock
    private IWelcomeBackActivityView view;

    @Mock
    private IWelcomeBackActivityInteractor interactor;

    private WelcomeBackActivityPresenter presenter;


    @Before
    public void setUp() {
        this.presenter = new WelcomeBackActivityPresenter(interactor, view);
    }

    @Test
    public void validatePin() {

    }

    @Test
    public void saveLocationInPreference() {
        Location location = new Location("");
        presenter.saveLocationInPreference(location);
        verify(interactor).saveLocationInPreference(location);
    }

    @Test
    public void verifyUserActiveDevice() {
        when(view.getDeviceInfo()).thenReturn(mock(DeviceInfo.class));
        presenter.verifyUserActiveDevice("0000", "03333333333");
        verify(interactor).verifyUserActiveDevice(any(), eq("0000"));
    }

    @Test
    public void onVerifyActiveDeviceSuccess_create_sinch_verification() {
        VerifyActiveDeviceResponse response = mock(VerifyActiveDeviceResponse.class);
        when(response.getMobileNumber()).thenReturn("03333333333");
        when(response.getVerifyDevice()).thenReturn(true);
        when(response.getIsDeviceKeyPresent()).thenReturn(false);
        presenter.onVerifyActiveDeviceSuccess(response, "0000");
        verify(view).createSinchVerification("03333333333", true, "0000");
    }

    @Test
    public void checkForVerifyOrLogin() {

    }

    @Test
    public void onUserLoginSuccess_with_temporary_pin_set() {
        UserLoginResponse response = mock(UserLoginResponse.class);
        when(response.getTemporaryPinSet()).thenReturn(true);
        presenter.onUserLoginSuccess(response);
        verify(view).startChangeTemporaryPinActivity();
    }

    @Test
    public void onUserLoginSuccess_with_user_activation_pending() {
        UserLoginResponse response = mock(UserLoginResponse.class);
        when(response.isUserActivationPending()).thenReturn(true);
        presenter.onUserLoginSuccess(response);
        verify(view).navigateToReactivateAccountActivity();
    }

    @Test
    public void onUserLoginSuccess() {
        UserLoginResponse response = mock(UserLoginResponse.class);
        when(response.getTemporaryPinSet()).thenReturn(true);
        presenter.onUserLoginSuccess(response);
        verify(view).startChangeTemporaryPinActivity();
    }

    @Test
    public void onUserLoginFailure_with_outdated_app() {
        presenter.onUserLoginFailure(ResponseStatusCode.APP_VERSION_INCOMPATIBLE.getCode(), "");
        verify(view).showAppOutDatedDialog(ResponseStatusCode.getDescriptionFromErrorCode(ResponseStatusCode.APP_VERSION_INCOMPATIBLE.getCode()));
    }

    @Test
    public void onUserLoginFailure() {
        presenter.onUserLoginFailure(123, "");
        verify(view).showError(eq(123), any(DoubleTapSafeDialogClickListener.class));
    }

    @Test
    public void onUserVerifySuccess_with_temporary_pin() {
        UserVerifyResponse response = mock(UserVerifyResponse.class);
        when(response.getTemporaryPinSet()).thenReturn(true);
        presenter.onUserVerifySuccess(response);
        verify(view).startChangeTemporaryPinActivity();
    }

    @Test
    public void onUserVerifySuccess() {
        UserVerifyResponse response = mock(UserVerifyResponse.class);
        presenter.onUserVerifySuccess(response);
        verify(view).routeToDashboard();
    }

    @Test
    public void onUserVerifyFailure() {
        onUserLoginFailure();
    }
}