package com.tez.androidapp.rewamp.signup.presenter;

import com.tez.androidapp.commons.models.network.dto.request.UserSignUpRequest;
import com.tez.androidapp.rewamp.signup.interactor.ISignupPermissionActivityInteractor;
import com.tez.androidapp.rewamp.signup.view.ISignupPermissionActivityView;

import net.tez.fragment.util.listener.DoubleTapSafeDialogClickListener;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class SignupNumberVerificationPresenterTest {

    @Mock
    private ISignupPermissionActivityView view;

    @Mock
    private ISignupPermissionActivityInteractor interactor;

    private SignupPermissionActivityPresenter presenter;

    @Before
    public void setUp() {
        presenter = new SignupNumberVerificationPresenter(view, interactor);
    }

    @Test
    public void submitSignUpRequest() {
        UserSignUpRequest request = mock(UserSignUpRequest.class);
        presenter.submitSignUpRequest(request);
        verify(interactor).submitUserSignUpRequest(request);
    }

    @Test
    public void onSubmitSignUpRequestSuccess() {
        presenter.onSubmitSignUpRequestSuccess();
        verify(view).createSinchVerification();
    }

    @Test
    public void onSubmitSignUpRequestFailure() {
        presenter.onSubmitSignUpRequestFailure(987, "");
        verify(view).showError(eq(987), any(DoubleTapSafeDialogClickListener.class));
    }
}