package com.tez.androidapp.rewamp.bima.claim.common.location.presenter

import com.google.common.truth.Truth.assertThat
import com.tez.androidapp.rewamp.bima.claim.common.location.view.ILocationPickerActivityView
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LocationPickerActivityPresenterTest {

    private lateinit var presenter: LocationPickerActivityPresenter

    @Mock
    private lateinit var view: ILocationPickerActivityView

    @Before
    fun setUp() {
        presenter = LocationPickerActivityPresenter(view)
    }

    @Test
    fun setFinalAddress_ShouldSetResultInView() {
        presenter.setFinalAddress("Name", "Address")
        verify(view).setResults(anyString())
    }

    @Test
    fun getFinalAddress_NameAddress_MergeRepetition() {
        val name = "Karachi"
        val address = "Karachi, Sindh, Pakistan"
        val expected = "Karachi, Sindh, Pakistan"
        val actual = presenter.getFinalAddress(address, name)
        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun getFinalAddress_NameAddress_DoNotMergeUniqueElement() {
        val name = "Defense"
        val address = "Karachi,Pakistan"
        val expected = "Defense, Karachi,Pakistan"
        val actual = presenter.getFinalAddress(address, name)
        assertThat(actual).isEqualTo(expected)
    }
}