package net.tez.validator.library.filters.text;

import androidx.annotation.NonNull;
import android.widget.EditText;
import net.tez.validator.library.annotations.text.DateRange;
import net.tez.validator.library.filters.Filter;
import net.tez.validator.library.utils.TextUtil;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by FARHAN DHANANI on 11/25/2018.
 */
@SuppressWarnings("unused")
public class DateRangeFilter implements Filter<EditText, DateRange> {

    @Override
    public boolean isValidated(@NonNull EditText view, @NonNull DateRange annotation) {
        String text = view.getText().toString();
        try {
            Date enteredDate = TextUtil.getFormattedDate(text, annotation.getDateFormat());
            return enteredDate.before(new Date(annotation.getMaxDateMillis()))
                    && enteredDate.after(new Date(annotation.getMinDateMillis()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }
}
