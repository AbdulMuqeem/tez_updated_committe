package net.tez.validator.library.defaults;

import net.tez.validator.library.core.AbstractChain;
import net.tez.validator.library.models.ProcessModel;

/**
 * Created by FARHAN DHANANI on 10/20/2018.
 */
public class MintFilterChain extends AbstractChain {

    @Override
    public void doFilter() {
        ProcessModel processModel = getProcessModel();
        if (processModel != null) {
            getFieldProcessor().process(processModel, this);
        }
    }
}
