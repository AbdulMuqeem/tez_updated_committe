package net.tez.validator.library.filters.text;

import androidx.annotation.NonNull;
import android.widget.EditText;

import net.tez.validator.library.annotations.text.MinDateDeviationFromCurrentDate;
import net.tez.validator.library.filters.Filter;
import net.tez.validator.library.utils.TextUtil;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by FARHAN DHANANI on 11/25/2018.
 */
@SuppressWarnings("unused")
public class MinDateDeviationFromCurrentDateFilter implements Filter<EditText, MinDateDeviationFromCurrentDate> {

    @Override
    public boolean isValidated(@NonNull EditText view, @NonNull MinDateDeviationFromCurrentDate annotation) {
        String text = view.getText().toString();
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.YEAR, annotation.yearDeviation());
            calendar.add(Calendar.MONTH, annotation.monthDeviation());
            calendar.add(Calendar.DATE, annotation.dayDeviation());
            return TextUtil.getFormattedDate(text, annotation.dateFormat())
                    .after(calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }
}
