package net.tez.validator.library.annotations.checkbox;

import net.tez.validator.library.annotations.Filterable;
import net.tez.validator.library.filters.checkbox.CheckBoxFilter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by FARHAN DHANANI on 5/16/2018.
 */

@SuppressWarnings("unused")
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Filterable(CheckBoxFilter.class)
public @interface CheckedCheckBox {
    int[] value();
}
