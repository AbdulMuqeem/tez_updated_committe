package net.tez.validator.library.core;

import androidx.annotation.NonNull;
import android.view.View;

import net.tez.validator.library.utils.ObjectUtil;

import java.lang.annotation.Annotation;

/**
 * Created by FARHAN DHANANI on 5/13/2018.
 */

@SuppressWarnings({"WeakerAccess", "unchecked", "unused"})
public class ValidationError {

    @NonNull private final String message;
    @NonNull private final View view;
    @NonNull private final Annotation annotation;

    public ValidationError(@NonNull String message, @NonNull View view, @NonNull Annotation annotation) {
        this.message = ObjectUtil.requireNonNull(message);
        this.view = ObjectUtil.requireNonNull(view);
        this.annotation = ObjectUtil.requireNonNull(annotation);
    }

    @NonNull
    public String getMessage() {
        return message;
    }

    @NonNull
    public <T extends View> T getView() {
        return (T) view;
    }

    @NonNull
    public Annotation getAnnotation() {
        return annotation;
    }
}
