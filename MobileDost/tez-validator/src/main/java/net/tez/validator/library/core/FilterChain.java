package net.tez.validator.library.core;

/**
 * Created by FARHAN DHANANI on 5/14/2018.
 */

@SuppressWarnings("WeakerAccess")
public interface FilterChain {

    void doFilter();
    boolean hasNext();
}