package net.tez.validator.library.annotations.text;

import androidx.annotation.NonNull;

import net.tez.validator.library.annotations.Filterable;
import net.tez.validator.library.filters.text.RegexFilter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Created by FARHAN DHANANI on 5/13/2018.
 */

@SuppressWarnings("unused")
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Filterable(RegexFilter.class)
public @interface Regex {
    @NonNull String regex();
    int[] value();
}
