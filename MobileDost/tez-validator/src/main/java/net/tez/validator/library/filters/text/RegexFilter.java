package net.tez.validator.library.filters.text;

import androidx.annotation.NonNull;
import android.widget.EditText;

import net.tez.validator.library.annotations.text.Regex;
import net.tez.validator.library.filters.Filter;
import net.tez.validator.library.utils.TextUtil;

/**
 * Created by FARHAN DHANANI on 5/13/2018.
 */

@SuppressWarnings("unused")
public class RegexFilter implements Filter<EditText, Regex> {

    @Override
    public boolean isValidated(@NonNull EditText view, @NonNull Regex annotation) {
        return TextUtil.isValidWithRegex(view.getText().toString(), annotation.regex());
    }
}
