package net.tez.validator.library.annotations.spinner;

import net.tez.validator.library.annotations.Filterable;
import net.tez.validator.library.filters.spinner.SpinnerValidationFilter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by FARHAN DHANANI on 8/12/2018.
 */

@SuppressWarnings("unused")
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Filterable(SpinnerValidationFilter.class)
public @interface SpinnerValidation {
    int index() default 0;
    int[] value();
}