package net.tez.validator.library.filters;

import androidx.annotation.NonNull;
import android.view.View;

import java.lang.annotation.Annotation;

/**
 * Created by FARHAN DHANANI on 5/15/2018.
 */

public interface Filter<V extends View, T extends Annotation> {
    boolean isValidated(@NonNull V view, @NonNull T annotation);
}
