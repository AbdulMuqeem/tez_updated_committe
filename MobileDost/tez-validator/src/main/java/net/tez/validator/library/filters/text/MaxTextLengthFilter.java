package net.tez.validator.library.filters.text;

import androidx.annotation.NonNull;
import android.widget.EditText;

import net.tez.validator.library.annotations.text.MaxLength;
import net.tez.validator.library.filters.Filter;

/**
 * Created by FARHAN DHANANI on 5/13/2018.
 */

@SuppressWarnings("unused")
public class MaxTextLengthFilter implements Filter<EditText, MaxLength> {

    @Override
    public boolean isValidated(@NonNull EditText view, @NonNull MaxLength annotation) {
        String text = view.getText().toString();
        return text.length() <= annotation.length();
    }
}
