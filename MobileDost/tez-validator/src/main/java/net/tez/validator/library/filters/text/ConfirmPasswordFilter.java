package net.tez.validator.library.filters.text;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import android.view.View;
import android.widget.EditText;

import net.tez.validator.library.annotations.text.ConfirmPassword;
import net.tez.validator.library.filters.Filter;

/**
 * Created by FARHAN DHANANI on 8/12/2018.
 */

@SuppressWarnings("unused")
public class ConfirmPasswordFilter implements Filter<EditText, ConfirmPassword> {

    @Override
    public boolean isValidated(@NonNull EditText view, @NonNull ConfirmPassword annotation) {

        Context context = view.getContext();
        if (context instanceof Activity) {
            View password = ((Activity) context).findViewById(annotation.id());
            if (password instanceof EditText) {
                return ((EditText) password).getText().toString().equals(view.getText().toString());
            }
        }
        return false;
    }
}
