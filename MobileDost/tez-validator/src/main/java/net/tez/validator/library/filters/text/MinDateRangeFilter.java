package net.tez.validator.library.filters.text;

import androidx.annotation.NonNull;
import android.widget.EditText;
import net.tez.validator.library.annotations.text.MinDateRange;
import net.tez.validator.library.filters.Filter;
import net.tez.validator.library.utils.TextUtil;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by FARHAN DHANANI on 11/25/2018.
 */
@SuppressWarnings("unused")
public class MinDateRangeFilter implements Filter<EditText, MinDateRange> {

    @Override
    public boolean isValidated(@NonNull EditText view, @NonNull MinDateRange annotation) {
        String text = view.getText().toString();
        try {
            return TextUtil.getFormattedDate(text, annotation.getDateFormat())
                    .after(new Date(annotation.getMinDateMillis()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }
}
