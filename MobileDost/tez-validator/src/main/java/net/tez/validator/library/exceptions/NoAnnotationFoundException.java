package net.tez.validator.library.exceptions;

/**
 * Created by FARHAN DHANANI on 10/19/2018.
 */
public class NoAnnotationFoundException extends RuntimeException {

    public NoAnnotationFoundException(String message) {
        super(message);
    }
}
