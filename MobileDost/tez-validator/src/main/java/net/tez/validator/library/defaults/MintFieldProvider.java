package net.tez.validator.library.defaults;

import android.app.Dialog;
import android.app.Fragment;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import net.tez.validator.library.core.FieldProvider;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by FARHAN DHANANI on 10/19/2018.
 */
public class MintFieldProvider implements FieldProvider {
    private static ClassFilter classFilter = () -> Object.class;

    @Override
    public Field[] getFields(@NonNull Object inspectedObj) {
        Set<Field> fieldSet = new HashSet<>();
        Class<?> view = inspectedObj.getClass();
        while ((classFilter.getActivityClass().isAssignableFrom(view) && !view.equals(classFilter.getActivityClass())) ||
                (classFilter.getDialogClass().isAssignableFrom(view) && !view.equals(classFilter.getDialogClass())) ||
                (classFilter.getSupportFragmentClass().isAssignableFrom(view) && !view.equals(classFilter.getSupportFragmentClass())) ||
                (classFilter.getFragmentClass().isAssignableFrom(view) && !view.equals(classFilter.getFragmentClass())) ||
                (classFilter.getViewHolderClass().isAssignableFrom(view) && !view.equals(classFilter.getViewHolderClass())) ||
                (classFilter.getViewClass().isAssignableFrom(view) && !view.equals(classFilter.getViewClass()))
        ) {
            fieldSet.addAll(Arrays.asList(view.getDeclaredFields()));
            view = view.getSuperclass();
        }
        return fieldSet.toArray(new Field[0]);
    }

    //return inspectedObj.getClass().getDeclaredFields();

    public interface ClassFilter {

        default Class<?> getActivityClass() {
            return AppCompatActivity.class;
        }

        default Class<?> getDialogClass() {
            return Dialog.class;
        }

        default Class<?> getViewClass() {
            return View.class;
        }

        default Class<?> getSupportFragmentClass() {
            return Fragment.class;
        }

        @SuppressWarnings("deprecation")
        default Class<?> getFragmentClass() {
            return android.app.Fragment.class;
        }

        Class<?> getViewHolderClass();
    }
}
