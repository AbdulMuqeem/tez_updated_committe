package net.tez.tezrecycler.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;
import net.tez.tezrecycler.base.viewholder.BaseViewHolder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by FARHAN DHANANI on 6/20/2018.
 */
public abstract class GenericRecyclerViewAdapter<
        E,
        L extends BaseRecyclerViewListener,
        VH extends BaseViewHolder<E, L>>
        extends RecyclerView.Adapter<VH> implements List<E> {

    private List<E> items = new ArrayList<>();
    private L listener;

    protected GenericRecyclerViewAdapter() {
    }

    protected GenericRecyclerViewAdapter(List<E> items) {
        this.items = items;
    }

    protected GenericRecyclerViewAdapter(@NonNull L listener) {
        this.listener = listener;
    }

    protected GenericRecyclerViewAdapter(@NonNull List<E> items, @Nullable L listener) {
        this.items = items;
        this.listener = listener;
    }

    @Override
    @NonNull
    public abstract VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType);

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        E item = get(position);
        holder.onBind(item, listener);
    }

    @Override
    public int getItemCount() {
        return size();
    }

    @NonNull
    public List<E> getItems() {
        return items;
    }

    public void setItems(@NonNull List<E> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Nullable
    public L getListener() {
        return listener;
    }

    public void setListener(@Nullable L listener) {
        this.listener = listener;
    }

    @Override
    public boolean add(@Nullable E item) {
        boolean isAdded = items.add(item);
        if (isAdded)
            notifyItemInserted(items.size() - 1);
        return isAdded;
    }

    @Override
    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    @Override
    @Nullable
    public E get(int index) {
        if (isIndexInRange(index))
            return items.get(index);
        return null;
    }

    @Override
    @Nullable
    public E set(int index, @Nullable E element) {
        if (isIndexInRange(index)) {
            E item = items.set(index, element);
            notifyItemChanged(index);
            return item;
        }
        return null;
    }

    @Override
    public void add(int index, @Nullable E element) {
        if (isIndexInRange(index)) {
            items.add(index, element);
            notifyItemInserted(index);
        }
    }

    @Override
    @Nullable
    public E remove(int index) {
        if (isIndexInRange(index)) {
            E item = items.remove(index);
            notifyItemRemoved(index);
            return item;
        }
        return null;
    }

    @Override
    public int indexOf(@Nullable Object o) throws ClassCastException {
        return items.indexOf(o);
    }

    @Override
    public int lastIndexOf(@Nullable Object o) throws ClassCastException {
        return items.lastIndexOf(o);
    }

    @NonNull
    @Override
    public ListIterator<E> listIterator() {
        return items.listIterator();
    }

    @NonNull
    @Override
    public ListIterator<E> listIterator(int index) throws IndexOutOfBoundsException {
        return items.listIterator(index);
    }

    @NonNull
    @Override
    public List<E> subList(int fromIndex, int toIndex) throws IndexOutOfBoundsException {
        return items.subList(fromIndex, toIndex);
    }

    @Override
    public boolean remove(@Nullable Object item) throws ClassCastException {
        int position = indexOf(item);
        if (position > -1) {
            boolean isRemoved = items.remove(item);
            if (isRemoved)
                notifyItemRemoved(position);
            return isRemoved;
        }
        return false;
    }

    @Nullable
    public E removeLastItem() throws ClassCastException {
        int index = size() - 1;
        if (index > -1)
            return this.remove(size() - 1);
        return null;
    }

    @Override
    public boolean containsAll(@NonNull Collection<?> c) {
        return this.items.containsAll(c);
    }

    @Override
    public boolean addAll(@NonNull Collection<? extends E> c) throws ClassCastException, NullPointerException {
        boolean isAdded = items.addAll(c);
        if (isAdded)
            notifyItemRangeInserted(size(), c.size());
        return isAdded;
    }

    @Override
    public boolean addAll(int index, @NonNull Collection<? extends E> c) throws IndexOutOfBoundsException, NullPointerException {
        boolean isAdded = items.addAll(index, c);
        if (isAdded)
            notifyItemRangeInserted(index, c.size());
        return isAdded;
    }

    @Override
    public boolean removeAll(@NonNull Collection<?> c) throws ClassCastException {
        boolean isRemoved = items.removeAll(c);
        if (isRemoved)
            notifyDataSetChanged();
        return isRemoved;
    }

    @Override
    public boolean retainAll(@NonNull Collection<?> c) throws ClassCastException {
        boolean isRetained = items.retainAll(c);
        if (isRetained)
            notifyDataSetChanged();
        return isRetained;
    }

    @Override
    public int size() {
        return items != null ? items.size() : 0;
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    @Override
    public boolean contains(Object o) throws ClassCastException {
        return items.contains(o);
    }

    @NonNull
    @Override
    public Iterator<E> iterator() {
        return items.iterator();
    }

    @NonNull
    @Override
    public Object[] toArray() {
        return items.toArray();
    }

    @NonNull
    @Override
    public <T> T[] toArray(@NonNull T[] a) throws ArrayStoreException {
        return items.toArray(a);
    }

    private boolean isIndexInRange(int index) {
        return index > -1 && index < size();
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }

    @NonNull
    protected View inflate(@NonNull Context context, @LayoutRes final int layout, @Nullable final ViewGroup parent, final boolean attachToRoot) {
        return LayoutInflater.from(context).inflate(layout, parent, attachToRoot);
    }

    @NonNull
    protected View inflate(@NonNull Context context, @LayoutRes final int layout, final @Nullable ViewGroup parent) {
        return inflate(context, layout, parent, false);
    }
}
