package net.tez.tezrecycler.listener;

import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;

/**
 * Created by VINOD KUMAR on 11/26/2018.
 */
public interface OnItemClick extends BaseRecyclerViewListener {

    void onItemClick(int position);
}
