package net.tez.tezrecycler.base.viewholder;

import android.content.Context;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;
import net.tez.tezrecycler.base.listener.BaseRecyclerViewListener;

import java.util.List;

/**
 * Created by FARHAN DHANANI on 6/20/2018.
 */
public abstract class BaseViewHolder<T, L extends BaseRecyclerViewListener> extends RecyclerView.ViewHolder {
    public BaseViewHolder(View itemView) {
        super(itemView);
    }

    public void onBind(T item, @Nullable L listener) {

    }

    public void onBind(int position, List<T> items, @Nullable L listener) {
        //This function is not called from Generic recycler view, to use this method you need to
        //Override onBindViewHolder in your adapter and call this method
    }

    protected void setItemViewOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener) {
        this.itemView.setOnClickListener(doubleTapSafeOnClickListener);
    }

    protected Context getContext() {
        return itemView.getContext();
    }
}
