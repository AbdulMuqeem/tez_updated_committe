package net.tez.permission.runtime.core;

import androidx.annotation.NonNull;

import net.tez.permission.runtime.models.Perm;

/**
 * Created by Vinod Kumar on 10/13/2018.
 */
public class PermImpl<T> implements Perm<T> {

    @NonNull
    private final String type;
    private final int requestCode;
    @NonNull
    private final T caller;

    PermImpl(@NonNull String type, int requestCode, @NonNull T caller) {
        this.type = type;
        this.requestCode = requestCode;
        this.caller = caller;
    }

    @NonNull
    @Override
    public String getType() {
        return type;
    }

    @Override
    public int getRequestCode() {
        return requestCode;
    }

    @NonNull
    @Override
    public T getCaller() {
        return caller;
    }
}
