package net.tez.permission.runtime.core;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import net.tez.permission.runtime.callbacks.PermissionCallback;
import net.tez.permission.runtime.callbacks.PermissionSettingCallback;
import net.tez.permission.runtime.models.Perm;
import net.tez.permission.runtime.utils.PersistentUtil;

/**
 * Created by Vinod Kumar on 5/7/2018.
 */

@SuppressWarnings({"unchecked", "WeakerAccess", "unused"})
public class RuntimePermission {

    public static boolean isPermissionRequired() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static boolean hasPermission(@NonNull Context context, String perm) {
        return !isPermissionRequired() || ContextCompat.checkSelfPermission(context, perm) == PackageManager.PERMISSION_GRANTED;
    }

    public static <T extends Fragment> void requestPermission(@NonNull T caller, String permType, @NonNull PermissionCallback<T> callback) {
        requestPermission(caller, permType, 1947, callback);
    }

    public static <T extends Fragment> void requestPermission(@NonNull T caller, String permType, int requestCode, @NonNull PermissionCallback<T> callback) {
        // noinspection ConstantConditions
        requestPermission(caller.getActivity(), caller, permType, 1947, callback);
    }

    public static <T extends FragmentActivity> void requestPermission(@NonNull T caller, String permType, @NonNull PermissionCallback<T> callback) {
        requestPermission(caller, permType, 1947, callback);
    }

    public static <T extends FragmentActivity> void requestPermission(@NonNull T caller, @NonNull String permType, int requestCode, @NonNull PermissionCallback<T> callback) {
        requestPermission(caller, caller, permType, requestCode, callback);
    }

    private static <T> void requestPermission(FragmentActivity activity, @NonNull T caller, @NonNull String permType, int requestCode, @NonNull PermissionCallback<T> callback) {

        if (activity == null)
            return;

        final Perm<T> perm = new PermImpl<>(permType, requestCode, caller);

        if (isPermissionRequired() && !hasPermission(activity, permType)) {

            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, perm.getType()) && !PersistentUtil.isCameFirst(activity, perm.getType())) {
                callback.onPermanentDenied(perm);
            } else {
                FragmentManager fragmentManager = activity.getSupportFragmentManager();
                PermsFragment fragment = PermsFragment.getInstance(perm, callback);
                fragmentManager.beginTransaction().add(fragment, "PERMS_FRAGMENT").commitAllowingStateLoss();
            }
        } else {
            callback.onGranted(perm);
        }
    }

    public static <T extends Fragment> void openAppSettingIntent(@NonNull T caller, @NonNull String permType, @NonNull PermissionSettingCallback<T> callback) {
        openAppSettingIntent(caller, permType, 148, callback);
    }

    public static <T extends Fragment> void openAppSettingIntent(@NonNull T caller, @NonNull String permType, int requestCode, @NonNull PermissionSettingCallback<T> callback) {
        // noinspection ConstantConditions
        openAppSettingIntent(caller.getActivity(), caller, permType, requestCode, callback);
    }

    public static <T extends FragmentActivity> void openAppSettingIntent(@NonNull T caller, @NonNull String permType, @NonNull PermissionSettingCallback<T> callback) {
        openAppSettingIntent(caller, permType, 148, callback);
    }

    public static <T extends FragmentActivity> void openAppSettingIntent(@NonNull T caller, @NonNull String permType, int requestCode, @NonNull PermissionSettingCallback<T> callback) {
        openAppSettingIntent(caller, caller, permType, requestCode, callback);
    }

    private static <T> void openAppSettingIntent(FragmentActivity activity, @NonNull T caller, @NonNull String permType, int requestCode, @NonNull PermissionSettingCallback<T> callback) {

        if (activity == null)
            return;

        final Perm<T> perm = new PermImpl<>(permType, requestCode, caller);

        if (isPermissionRequired()) {

            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            AppSettingFragment fragment = AppSettingFragment.getInstance(perm, activity.getPackageName(), callback);
            fragmentManager.beginTransaction().add(fragment, "APP_SETTING_FRAGMENT").commitAllowingStateLoss();

        } else {
            callback.onGranted(perm);
        }
    }
}
