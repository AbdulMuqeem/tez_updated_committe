package net.tez.permission.runtime.core;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import net.tez.permission.runtime.callbacks.PermissionCallback;
import net.tez.permission.runtime.models.Perm;

/**
 * Created by Vinod Kumar on 10/13/2018.
 */
public class PermsFragment<T> extends Fragment {

    private Perm<T> perm;
    private PermissionCallback<T> callback;

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        requestPermissions(new String[]{perm.getType()}, perm.getRequestCode());
        return null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == perm.getRequestCode()) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callback.onGranted(perm);
            } else {
                callback.onDenied(perm);
            }

            // remove fragment from manager
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager != null) {
                fragmentManager.beginTransaction().remove(this).commitAllowingStateLoss();
            }
        }
    }

    public static <T> PermsFragment getInstance(Perm<T> perm, PermissionCallback<T> callback) {
        PermsFragment<T> fragment = new PermsFragment<>();
        fragment.perm = perm;
        fragment.callback = callback;
        return fragment;
    }
}