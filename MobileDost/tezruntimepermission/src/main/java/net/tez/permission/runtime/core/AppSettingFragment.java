package net.tez.permission.runtime.core;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import net.tez.permission.runtime.callbacks.PermissionSettingCallback;
import net.tez.permission.runtime.models.Perm;

/**
 * Created by Vinod Kumar on 10/13/2018.
 */
public class AppSettingFragment<T> extends Fragment {

    private Perm<T> perm;
    private PermissionSettingCallback<T> callback;
    private String packageName;

    public static <T> AppSettingFragment getInstance(@NonNull Perm<T> perm,
                                                     @NonNull String packageName,
                                                     @NonNull PermissionSettingCallback<T> callback) {
        AppSettingFragment<T> fragment = new AppSettingFragment<>();
        fragment.perm = perm;
        fragment.packageName = packageName;
        fragment.callback = callback;
        return fragment;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", packageName, null));
        startActivityForResult(intent, perm.getRequestCode());
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Context context = getContext();
        if (context != null && RuntimePermission.hasPermission(context, perm.getType())) {
            callback.onGranted(perm);
        } else {
            callback.onDenied(perm);
        }

        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            fragmentManager.beginTransaction().remove(this).commitAllowingStateLoss();
        }
    }
}