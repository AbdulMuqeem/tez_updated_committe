package net.tez.logger.library.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.tez.logger.library.core.FormatStrategy;
import net.tez.logger.library.core.LogAdapter;
import net.tez.logger.library.formats.StyleLogFormatStrategy;
import net.tez.logger.library.utils.ObjectUtil;


/**
 * Created by FARHAN DHANANI on 5/10/2018.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class AndroidLogAdapter implements LogAdapter {

    @NonNull
    private final FormatStrategy formatStrategy;

    public AndroidLogAdapter() {
        this(StyleLogFormatStrategy.newBuilder().build());
    }

    public AndroidLogAdapter(@NonNull FormatStrategy formatStrategy) {
        this.formatStrategy = ObjectUtil.requireNonNull(formatStrategy);
    }

    @Override
    public boolean isLoggable(int priority, @Nullable String tag) {
        return true;
    }

    @Override
    public void log(int priority, @Nullable String tag, @NonNull String message) {
        formatStrategy.log(priority, tag, message);
    }
}
