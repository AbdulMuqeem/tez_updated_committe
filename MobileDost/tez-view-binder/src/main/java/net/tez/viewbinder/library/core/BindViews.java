package net.tez.viewbinder.library.core;

import androidx.annotation.IdRes;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by FARHAN DHANANI on 5/17/2018.
 */

/**
 * Denotes that an integer parameter, field or method return value is expected
 * to be an id resource reference (e.g. android.R.id.copy).
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface BindViews {
    @IdRes int[] value();
}
