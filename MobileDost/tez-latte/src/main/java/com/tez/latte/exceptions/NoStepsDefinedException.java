package com.tez.latte.exceptions;

public class NoStepsDefinedException extends RuntimeException
{
    public NoStepsDefinedException()
    {
        super("No steps defined for the feature");
    }
}