package com.tez.latte;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.tez.latte.exceptions.DuplicatedStepDefinitionException;
import com.tez.latte.exceptions.NoStepsDefinedException;
import com.tez.latte.exceptions.StepDefinitionNotFoundException;

import org.junit.Rule;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import gherkin.ast.Node;
import gherkin.ast.Step;

import static androidx.test.InstrumentationRegistry.getTargetContext;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

public class TezLatteTest
{
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().port(8080));

    private final ScenarioConfig scenarioConfig;
    private final TestLog testLog;

    public TezLatteTest(ScenarioConfig scenario)
    {
        this.scenarioConfig = scenario;
        this.testLog = new TestLog();

        beforeScenarioStarts(scenario.scenario(), scenario.locale());

        Localization localization = new Localization(getTargetContext());
        localization.locale(scenarioConfig.locale());
    }

    protected void beforeScenarioStarts(Scenario scenario, Locale locale)
    {
    }

    protected void afterScenarioEnds(Scenario scenario, Locale locale)
    {
    }

    protected void start(TezLatteSteps... targets)
    {
        if (targets.length == 0)
        {
            throw new NoStepsDefinedException();
        }

        Scenario scenario = scenarioConfig.scenario();

        testLog.logScenario(scenario);

        List<StepDefinition> stepDefinitions = new ArrayList<>();

        for (TezLatteSteps tezLatteSteps : targets)
        {
            stepDefinitions.addAll(tezLatteSteps.stepDefinitions());
        }

        //validateStepDefinitions(stepDefinitions);

        try
        {
            for (Step step : scenario.steps())
            {
                processStep(step, stepDefinitions);
            }
        }
        catch (Exception e)
        {
            if (scenarioConfig.hasScreenshotProvider())
            {
                ScreenshotProvider screenshotProvider = scenarioConfig.screenshotProvider();
                screenshotProvider.takeScreenshot(scenario);
            }

            throw e;
        }
        finally
        {
            afterScenarioEnds(scenarioConfig.scenario(), scenarioConfig.locale());
        }
    }

    private void validateStepDefinitions(List<StepDefinition> stepDefinitions)
    {
        Set<String> patterns = new HashSet<>();

        for (StepDefinition stepDefinition : stepDefinitions)
        {
            String pattern = stepDefinition.pattern();

            if (!patterns.contains(pattern))
            {
                patterns.add(pattern);
            }
            else
            {
                throw new DuplicatedStepDefinitionException(stepDefinition.method(), pattern);
            }
        }
    }

    private void processStep(Step step, List<StepDefinition> stepDefinitions)
    {
        String keyword = step.getKeyword().trim();
        String text = step.getText().trim();
        Node argument = step.getArgument();

        testLog.logStep(keyword, text);

        for (StepDefinition stepDefinition : stepDefinitions)
        {
            if (stepDefinition.matches(text))
            {
                stepDefinition.invoke(text, argument);
                return;
            }
        }

        throw new StepDefinitionNotFoundException(keyword, text);
    }
}