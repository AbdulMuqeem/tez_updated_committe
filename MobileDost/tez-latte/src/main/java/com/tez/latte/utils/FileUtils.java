package com.tez.latte.utils;

import android.content.Context;

import com.github.tomakehurst.wiremock.core.WireMockApp;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileUtils {
    private final Context contextForAssets;
    private final String tempDirectoryName = "files";
    private final String replacementDirectoryName = WireMockApp.FILES_ROOT;
    private final String dataDirectory = "/data/data";
    private final String packageName;

    private FileUtils(Context contextForAssets1){
        this.contextForAssets = contextForAssets1;
        this.packageName = contextForAssets.getPackageName();
    }

    private void copyFileOrDir(String path){
        final String[] assets;
        try{
            assets = this.contextForAssets.getAssets().list(path);
            if(assets.length==0){
                copyFile(path);
            } else{
                //String fullPath = this.dataDirectory
            }
        } catch (IOException ex){

        }
    }

    private void copyFile(String fileName){
        final InputStream inputStream;
        final OutputStream out;

        try{
            inputStream = this.contextForAssets.getAssets().open(fileName);
            String newFileName = this.dataDirectory +
                    "/" + this.packageName + "/" + fileName;
            newFileName = newFileName.replace(tempDirectoryName, replacementDirectoryName);
            out = new FileOutputStream(newFileName);
            final byte[] buffer = new byte[1024];
            int read = inputStream.read(buffer);
            while(read!=-1){
                out.write(buffer, 0, read);
                read = inputStream.read(buffer);
            }
            inputStream.close();
            out.flush();
            out.close();
        } catch (Exception ex){

        }
    }
}
