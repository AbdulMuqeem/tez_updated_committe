package net.tez.fragment.util.base;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.tez.fragment.util.effect.RippleEffectTouchListener;
import net.tez.fragment.util.effect.ScaleEffectOnTouchListener;
import net.tez.fragment.util.listener.DoubleTapSafeOnClickListener;

/**
 * Created by VINOD KUMAR on 2/18/2019.
 */
public interface IBaseWidget {

    int NO_EFFECT = 0;
    int SCALE_EFFECT = 1;
    int RIPPLE_EFFECT = 2;

    void setDoubleTapSafeOnClickListener(@Nullable DoubleTapSafeOnClickListener doubleTapSafeOnClickListener);

    default void changeDrawable(@NonNull TextView textView,
                                @DrawableRes int start,
                                @DrawableRes int top,
                                @DrawableRes int end,
                                @DrawableRes int bottom) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(start, top, end, bottom);
        } else
            textView.setCompoundDrawablesWithIntrinsicBounds(start, top, end, bottom);
    }

    default void changeDrawable(@NonNull TextView textView,
                                Drawable start,
                                Drawable top,
                                Drawable end,
                                Drawable bottom) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(start, top, end, bottom);
        } else
            textView.setCompoundDrawablesWithIntrinsicBounds(start, top, end, bottom);
    }

    default int getColor(@NonNull Context context, @ColorRes int colorRes) {
        return context.getResources().getColor(colorRes);
    }

    default int dpToPx(Context context, float dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    default int spToPx(Context context, float sp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }

    default int getLeftStartPadding(@NonNull View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Math.max(view.getPaddingStart(), view.getPaddingLeft());
        }
        return view.getPaddingLeft();
    }

    default int getRightEndPadding(@NonNull View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Math.max(view.getPaddingEnd(), view.getPaddingRight());
        }
        return view.getPaddingRight();
    }

    default void attachClickEffect(int clickEffect, View view) {

        if (clickEffect == NO_EFFECT)
            setNoEffect(view);

        if (clickEffect == SCALE_EFFECT)
            setScaleEffect(view);

        else if (clickEffect == RIPPLE_EFFECT)
            setRippleEffect(view);
    }

    default void setNoEffect(@NonNull View view) {
        view.setOnTouchListener(null);
    }

    default void setScaleEffect(@NonNull View view) {
        view.setOnTouchListener(new ScaleEffectOnTouchListener());
    }

    @NonNull
    default String getLabel() {
        return "";
    }

    default void setRippleEffect(@NonNull View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.setOnTouchListener(new RippleEffectTouchListener(view));
        }
    }
}
