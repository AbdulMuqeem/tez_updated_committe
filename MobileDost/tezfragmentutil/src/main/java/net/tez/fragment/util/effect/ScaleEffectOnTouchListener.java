package net.tez.fragment.util.effect;

import android.view.View;

/**
 * Created by VINOD KUMAR on 7/9/2019.
 */
public class ScaleEffectOnTouchListener extends TouchEffectListener {

    @Override
    public void onTouchEffect(View view) {

        // Scale down

        view.animate().scaleXBy(-0.05f).setDuration(70).start();
        view.animate().scaleYBy(-0.05f).setDuration(70).start();
    }

    @Override
    public void onNormalEffect(View view) {

        // Scale Up

        view.animate().cancel();
        view.animate().scaleX(1f).setDuration(40).start();
        view.animate().scaleY(1f).setDuration(40).start();
    }

}
