package net.tez.fragment.util.effect;

import android.content.res.ColorStateList;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.view.View;

import androidx.annotation.RequiresApi;

import com.example.vendvinodkumar.fragmentutil.R;

/**
 * Created by VINOD KUMAR on 7/19/2019.
 */
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class RippleEffectTouchListener extends TouchEffectListener {

    public RippleEffectTouchListener(View view) {
        setRippleEffect(view);
    }


    private void setRippleEffect(View view) {
        view.setBackground(getRippleDrawable(view));
    }


    private RippleDrawable getRippleDrawable(View view) {
        return new RippleDrawable(getRippleColorState(), view.getBackground(), null);
    }

    private ColorStateList getRippleColorState() {
        return new ColorStateList(
                new int[][]
                        {
                                new int[]{android.R.attr.state_pressed},
                                new int[]{android.R.attr.state_focused},
                                new int[]{android.R.attr.state_activated},
                                new int[]{}
                        },
                new int[]
                        {
                                R.color.colorGrey,
                                R.color.colorGrey,
                                R.color.colorGrey,
                                R.color.colorGrey
                        }
        );
    }

    @Override
    public void onTouchEffect(View view) {
        view.setPressed(true);
    }

    @Override
    public void onNormalEffect(View view) {
        view.setPressed(false);
    }
}
