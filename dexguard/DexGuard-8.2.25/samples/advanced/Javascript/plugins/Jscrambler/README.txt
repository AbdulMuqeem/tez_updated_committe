===============================================================================
This sample illustrates how to use an existing Javascript obfuscator as a
plugin for DexGuard. This sample incorporates the Jscrambler obfuscator:
https://www.jscrambler.com
===============================================================================

Plugin description
------------------
The obfuscator we're delegating to in this sample is available as the NPM node
'jscrambler'.

This sample plugin writes out the Javascript files to a temporary directory
and calls the node using the Runtime.exec() method of Java. Afterwards, the
obfuscated temporary files are fed back into DexGuard.


Set up
------
The plugin prefers the NPM node to be installed under plugins/Jscrambler/lib.
To install the plugin, first install Node.js on your system. Make sure that
you have a sufficiently recent version, for example v8.11.1. This will also
install NPM. Next, navigate to the plugins/Jscrambler/lib directory:

  $ cd plugins/Jscrambler/lib

And finally install the jscrambler node:

  $ npm install jscrambler

Alternatively, you can specify where the node is installed (the directory
where you called `npm install`) via the command line option:

  -Ddexguard.jscrambler.path=path/to/install/dir/

With the node correctly installed, build the plugin by navigating back up
to the plugins/Jscrambler directory:

  $ cd ..

and running the command:

  $ gradle jar


Running the samples
-------------------

The Jscrambler obfuscator will only work if a Jscrambler JSON
configuration file is provided. A small sample configuration is already
included in the root folder of the samples (jscrambler.json), but it still
requires a Jscrambler access key, secret key and application id. You can
create these by setting up a user account at

https://www.jscrambler.com

and create a new application via the website's dashboard.

With the plugin and Jscrambler configuration ready for use, you need to tell
DexGuard where to find the JAR and configuration file:

-javascriptplugin ../../plugins/Jscrambler/lib/sample-obfuscation-plugin.jar jscrambler.json

and also which Javascript files to obfuscate:

-obfuscatejavascript assets/**.js

Now you're ready to build the app with obfuscated javascript files.


Configuration
-------------

You can customize the obfuscation via the app page on the dashboard of the
JScrambler website, afterwards generate a new jscrambler.json file, and replace
 the existing jscrambler.json in the app root directory with the new one.

You can find more information on the obfuscation options on the Jscrambler
website:

https://docs.jscrambler.com/code-integrity/documentation/transformations


Troubleshooting
---------------
- 'An unexpected error has occured in jscrambler':
    This means the obfuscator has failed. It currently gives no description
    on the terminal about what caused the error when it crashes. Check the
    following conditions:
    1) the defined location of your configuration file is valid
    2) the syntax of the configuration file is correct


Removing the node
-----------------
To remove the node installation, navigate to plugin/lib/ and run the command

  $ npm uninstall jscrambler

You can then safely delete the empty node_modules directory and the
package-lock.json file. If you plan to run the sample again, make sure you
don't delete the package.json file.
