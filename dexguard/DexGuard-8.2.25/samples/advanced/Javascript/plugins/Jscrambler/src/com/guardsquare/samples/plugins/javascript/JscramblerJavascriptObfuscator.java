/*
 * ProGuard -- shrinking, optimization, obfuscation, and preverification
 *             of Java bytecode.
 *
 * Copyright (c) 2002-2018 Guardsquare NV
 */
/*
 * ProGuard -- shrinking, optimization, obfuscation, and preverification
 *             of Java bytecode.
 *
 * Copyright (c) 2002-2018 Guardsquare NV
 */
package com.guardsquare.samples.plugins.javascript;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.*;

/**
 * Jscrambler Javascript obfuscation plugin.
 */
public class JscramblerJavascriptObfuscator extends JavascriptDirectoryObfuscator
{
    private static final String DEXGUARD_PREFIX   = "DexGuard_";

    private final String pathToJar;
    private final String pathToCommand;


    public JscramblerJavascriptObfuscator() throws IOException
    {
        String pathToJar = System.getProperty("dexguard.jscrambler.path");

        if (pathToJar == null)
        {
            try
            {
                pathToJar =
                    getClass().getProtectionDomain().getCodeSource().getLocation().toURI().resolve(".").getPath();
            }
            catch (URISyntaxException ue)
            {
                // Should never happen.
                throw new IllegalStateException("Failed to retrieve jscrambler code source location");
            }
        }

        this.pathToJar = pathToJar;

        pathToCommand =
            pathToJar   +
            "node_modules"  + File.separator +
            "jscrambler"    + File.separator +
            "dist"          + File.separator +
            "bin"           + File.separator +
            "jscrambler.js";
    }


    @Override
    public File obfuscate(File configurationFile, File inputDirectory) throws IOException
    {
        if (configurationFile == null)
        {
            throw new IllegalArgumentException("Jscrambler requires a configuration file.");
        }

        File outputFile = Files.createTempDirectory(DEXGUARD_PREFIX).toFile();

        try
        {
            // Obfuscate the input file using an external command.
            String commandWithOptions =
                pathToCommand +
                " -c " + configurationFile.getCanonicalPath() +
                " -C " + inputDirectory.getCanonicalPath() +
                " -o " + outputFile.getCanonicalPath() +
                " " + inputDirectory.getCanonicalPath() + File.separator + "**";

            System.out.println("commandWithOptions = " + commandWithOptions);
            Process pr = Runtime.getRuntime().exec(commandWithOptions);

            if (pr.waitFor() != 0)
            {
                throw new IllegalStateException("An unexpected error has occured in jscrambler. " +
                                                "See README.txt for troubleshooting tips");
            }

        }
        catch (IOException ioe)
        {
            throw new IOException("\n\n" +
                "Please make sure you have the jscrambler node correctly installed in the directory: \n" +
                "  " + pathToJar + "\n" +
                "via the command `npm install jscrambler`. See README.txt for more details",
                ioe);
        }
        catch (InterruptedException ie)
        {
            throw new IllegalStateException("Javascript obfuscator was unexpectedly interrupted", ie);
        }

        return outputFile;
    }
}
