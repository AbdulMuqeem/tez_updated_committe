/*
 * ProGuard -- shrinking, optimization, obfuscation, and preverification
 *             of Java bytecode.
 *
 * Copyright (c) 2002-2018 Guardsquare NV
 */
package com.guardsquare.samples.plugins.javascript;

import com.guardsquare.dexguard.obfuscation.asset.JavascriptObfuscator;

import java.io.*;
import java.nio.file.Files;

/**
 * {@link JavascriptObfuscator} that writes all Javascript files to be obfuscated to a temporary directory, where they
 * can be obfuscated all at once, before being written.
 */
public abstract class JavascriptDirectoryObfuscator extends JavascriptObfuscator
{
    private static final String DEXGUARD_PREFIX = "DexGuard_";
    private static final int    BUFFER_SIZE     = 8 * 1024;

    private final File inputDirectory;

    private File outputDirectory;


    public JavascriptDirectoryObfuscator() throws IOException
    {
        inputDirectory = Files.createTempDirectory(DEXGUARD_PREFIX).toFile();
    }


    // Implementations for JavascriptObfuscator.

    @Override
    public void collectInformation(File        configurationFile,
                                   String      inputFileName,
                                   InputStream inputStream) throws IOException
    {
        // Copy the input file to a temporary file in the inputDirectory.
        File         outputFile   = new File(inputDirectory, inputFileName);

        // Ensure the parent directory exists.
        outputFile.getParentFile().mkdirs();

        // Copy the file contents.
        OutputStream outputStream = new FileOutputStream(outputFile);

        copyData(inputStream, outputStream);

        outputStream.close();
    }


    @Override
    public OutputStream obfuscate(File         configurationFile,
                                  String       inputFileName,
                                  OutputStream outputStream) throws IOException
    {
        // Obfuscate the Javascript files, if it hasn't been done yet.
        if (outputDirectory == null)
        {
            outputDirectory = obfuscate(configurationFile, inputDirectory);
        }

        // Write the contents of the temporary output file to the given output stream.
        File        outputFile  = new File(outputDirectory, inputFileName);
        InputStream inputStream = new FileInputStream(outputFile);

        copyData(inputStream, outputStream);

        inputStream.close();
        outputStream.close();

        // The obfuscated file has already been written - return a dummy output stream.
        return new DummyOutputStream();
    }


    /**
     * Obfuscates all files in the given input directory and its subdirectories.
     *
     * @param configurationFile optional file that was passed as a second argument to
     *                          `-javascriptplugin jarPath [configurationFile].`
     * @param inputDirectory    the directory containing the files to be obfuscated.
     * @return                  the directory where the obfuscated copies are located. It can be identical to the input
     *                          directory, if the files are overwritten.
     */
    public abstract File obfuscate(File configurationFile,
                                   File inputDirectory) throws IOException;


    // Small utility methods.

    /**
     * Copy all data from the given input stream to the given output stream.
     */
    private void copyData(InputStream inputStream, OutputStream outputStream) throws IOException
    {
        byte[] buffer = new byte[BUFFER_SIZE];
        int    bytesRead;
        while ((bytesRead = inputStream.read(buffer)) != -1)
        {
            outputStream.write(buffer, 0, bytesRead);
        }
    }


    /**
     * Dummy output stream.
     */
    private static class DummyOutputStream extends OutputStream
    {
        @Override
        public void write(int b) {}
    }
}
