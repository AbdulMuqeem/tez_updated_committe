===============================================================================
This sample illustrates how to use an existing Javascript obfuscator as a
plugin for DexGuard. This sample incorporates the open source obfuscator by
Github user Sanex3339:
https://github.com/javascript-obfuscator/javascript-obfuscator
===============================================================================

Plugin description
------------------
The obfuscator we're delegating to in this sample is available as the NPM node
'javascript-obfuscator'. Since it is written in TypeScript and not in Java, we
can't directly run the code from Java.

This lightweight sample plugin writes out the Javascript to a temporary file
and calls the node using the Runtime.exec() method of Java. Afterwards, the
obfuscated temporary file is fed back into DexGuard.

Set up
------
The plugin prefers the NPM node to be installed under plugins/Sanex/lib/.
To install the plugin, first install Node.js on your system. Make sure that
you have a sufficiently recent version, for example v8.11.1. This will also
install NPM. Next, navigate to the plugins/Sanex/lib/ directory:

  $ cd plugins/Sanex/lib

and install the javascript-obfuscator node:

  $ npm install javascript-obfuscator

Alternatively, you can specify where the node is installed (the directory
where you called `npm install`) via the command line option:

  -Ddexguard.sanex.path=path/to/install/dir/

The obfuscator will only work if the install directory contains a dummy
package.json file, like the one we provide with the sample.

With the node correctly installed, build the plugin by navigating back up
to the plugins/Sanex directory:

  $ cd ..

and running the command:

  $ gradle jar

You now have a plugin jar and the Javascript obfuscator on which it depends.

Running the samples
-------------------

With the plugin ready for use, you can build the sample applications
CordovaApp and ReactNativeApp.

If you have your own sample applications, you have to tell DexGuard where to
find the plugin jar and which Javascript files to obfuscate. Navigate to your
dexguard-project.txt file and specify the following rules:

-javascriptplugin ../../plugins/Sanex/lib/sample-obfuscation-plugin.jar
-obfuscatejavascript assets/**.js

Now you're ready to build the app with obfuscated javascript files.

Configuration
-------------
This javascript-obfuscator can read in configuration files of the form:

{
  optionName1: <optionValue>
  optionName2: <optionValue>
}

Where the options are defined on the project's Github page. Note that this
differs from the configuration examples that are listed on the GitHub page.
The examples that are shown there are passed to the obfuscator through code,
and thus use `commas` to separate options. When passing a file, these commas
are not parsed and should be removed (see the example configuration files
under plugins/Sanex/lib/).

You can define such a file for yourself and pass it to the plugin by changing
the option in your DexGuard configuration to point to the config file location
(relative to your DexGuard configuration file):

  -javascriptplugin ../../plugins/Sanex/lib/sample-obfuscation-plugin.jar ../../plugins/Sanex/lib/medium.txt

The three example configurations with varying strength and performance impact
from the obfuscator's github page are included in this sample and are named
low.txt, medium.txt and high.txt. You can try these out or experiment with
the options to match your needs.

Troubleshooting
---------------
- 'An unexpected error has occured in javascript-obfuscator':
    This means the obfuscator has failed. It currently gives no description
    on the terminal about what caused the error when it crashes. Check the
    following conditions:
    1) the defined location of your configuration file is valid
    2) the syntax of the configuration options is correct (see below)
    3) There is a valid package.json file present.

- Configuration from the Github page doesn't work:
    You need to remove the commas from the example configs in order for them
    to work.

Removing the node
-----------------
To remove the node installation, navigate to plugins/Sanex/lib/ and run the command

  $ npm uninstall javascript-obfuscator

You can then safely delete the empty node_modules directory and the
package-lock.json file. If you plan to run the sample again, make sure you
don't delete the package.json file.
