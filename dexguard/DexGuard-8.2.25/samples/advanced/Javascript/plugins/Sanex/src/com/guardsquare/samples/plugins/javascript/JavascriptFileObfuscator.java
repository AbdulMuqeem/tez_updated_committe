/*
 * ProGuard -- shrinking, optimization, obfuscation, and preverification
 *             of Java bytecode.
 *
 * Copyright (c) 2002-2018 Guardsquare NV
 */
package com.guardsquare.samples.plugins.javascript;

import com.guardsquare.dexguard.obfuscation.asset.JavascriptObfuscator;

import java.io.*;


/**
 * {@link JavascriptObfuscator} working with files instead of output streams.
 */
public abstract class JavascriptFileObfuscator extends JavascriptObfuscator
{
    private static final String DEXGUARD_PREFIX   = "DexGuard_";
    private static final String JAVASCRIPT_SUFFIX = "_IN.js";
    private static final int    BUFFER_SIZE       = 8 * 1024;


    // Implementations for JavascriptObfuscator.

    @Override
    public OutputStream obfuscate(File         configurationFile,
                                  String       inputFileName,
                                  OutputStream outputStream) throws IOException
    {
        // Create a temporary file where the input can be written.
        File inputFile = File.createTempFile(DEXGUARD_PREFIX, JAVASCRIPT_SUFFIX);

        return new JavascriptObfuscatingOutputStream(configurationFile,
                                                     inputFile,
                                                     outputStream);
    }


    /**
     * Obfuscates the given file.
     *
     * @param configurationFile optional file that was passed as a second argument to
     *                          `-javascriptplugin jarPath [extraArgument].`
     * @param inputFile         the Javascript file to be obfuscated.
     * @return                  the obfuscated result.
     * @throws IOException
     */
    public abstract File obfuscate(File configurationFile,
                                   File inputFile) throws IOException;



    /**
     * FileOutputStream extension that obfuscates the written file when the stream is closed,
     * and writes the obfuscated result to a given output stream.
     */
    private class JavascriptObfuscatingOutputStream extends FileOutputStream
    {
        private final File         configurationFile;
        private final File         inputFile;
        private final OutputStream outputStream;


        JavascriptObfuscatingOutputStream(File         configurationFile,
                                          File         inputFile,
                                          OutputStream outputStream) throws FileNotFoundException
        {
            super(inputFile);
            this.configurationFile = configurationFile;
            this.inputFile         = inputFile;
            this.outputStream      = outputStream;
        }


        // Implementations for OutputStream.

        public void close() throws IOException
        {
            super.close();

            InputStream inputStream = null;
            File        outputFile  = null;
            try
            {
                // Obfuscated the input.
                outputFile  = obfuscate(configurationFile, inputFile);

                // Write the obfuscated result to the wrapped output stream.
                inputStream = new FileInputStream(outputFile);

                byte[] buffer = new byte[BUFFER_SIZE];
                int    bytesRead;
                while ((bytesRead = inputStream.read(buffer)) != -1)
                {
                    outputStream.write(buffer, 0, bytesRead);
                }
            }
            finally
            {
                if (inputStream != null)
                {
                    inputStream.close();
                }
                if (outputStream != null)
                {
                    outputStream.close();
                }

                // Clean up the temporary files.
                if (inputFile != null && !inputFile.delete())
                {
                    throw new IOException("Could not delete temporary file: " + inputFile.getAbsolutePath());
                }
                if (outputFile != null && !outputFile.delete())
                {
                    throw new IOException("Could not delete temporary file: " + outputFile.getAbsolutePath());
                }
            }
        }
    }
}
