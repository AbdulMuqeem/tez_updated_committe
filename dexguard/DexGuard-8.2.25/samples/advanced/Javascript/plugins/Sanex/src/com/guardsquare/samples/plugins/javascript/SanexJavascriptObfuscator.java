/*
 * ProGuard -- shrinking, optimization, obfuscation, and preverification
 *             of Java bytecode.
 *
 * Copyright (c) 2002-2018 Guardsquare NV
 */
package com.guardsquare.samples.plugins.javascript;


import java.io.*;
import java.net.URISyntaxException;


/**
 * This lightweight plugin class obfuscates Javascript files using the open source javascript-obfuscator npm node.
 */
public class SanexJavascriptObfuscator
    extends JavascriptFileObfuscator
{
    private static final String DEXGUARD_PREFIX   = "DexGuard_";
    private static final String JAVASCRIPT_SUFFIX = "_OUT.js";

    private final String pathToJar;
    private final String pathToCommand;


    public SanexJavascriptObfuscator()
    {
        String pathToJar = System.getProperty("dexguard.sanex.path");

        if (pathToJar == null)
        {
            try
            {
                pathToJar =
                    getClass().getProtectionDomain().getCodeSource().getLocation().toURI().resolve(".").getPath();
            }
            catch (URISyntaxException ue)
            {
                // Should never happen.
                throw new IllegalStateException("Failed to retrieve javascript obfuscator code source location");
            }
        }

        this.pathToJar = pathToJar;

        pathToCommand =
            pathToJar +
            "node_modules" + File.separator +
            "javascript-obfuscator" + File.separator +
            "bin" + File.separator +
            "javascript-obfuscator";
    }


    /**
     * Obfuscates the given Javascript input file by calling the Node.js javascript-obfuscator node.
     * The configuration file is parsed and the options defined in it are applied during obfuscation.
     */
    @Override
    public File obfuscate(File configurationFile, File inputFile) throws IOException
    {
        File outputFile = File.createTempFile(DEXGUARD_PREFIX, JAVASCRIPT_SUFFIX);

        try
        {
            // Obfuscate the input file using an external command.
            Process pr = Runtime.getRuntime().exec(
                            pathToCommand +
                            " "    + inputFile.getAbsolutePath()  +
                            " -o " + outputFile.getAbsolutePath() +
                            (configurationFile == null ? "" : " --config " + configurationFile.getCanonicalPath()));

            if (pr.waitFor() != 0)
            {
                throw new IllegalStateException("An unexpected error has occured in javascript-obfuscator. " +
                                                "See README.txt for troubleshooting tips");
            }

        }
        catch (IOException ioe)
        {
            throw new IOException("\n\n" +
                "Please make sure you have the javascript-obfuscator node correctly installed in the directory: \n" +
                "  " + pathToJar + "\n" +
                "via the command `npm install javascript-obfuscator`. See README.txt for more details",
                ioe);
        }
        catch (InterruptedException ie)
        {
            throw new IllegalStateException("Javascript obfuscator was unexpectedly interrupted", ie);
        }

        return outputFile;
    }
}
