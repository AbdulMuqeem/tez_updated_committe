# DexGuard's default settings are fine for this sample application.

# We'll display some more statistics about the processed code.
-verbose

# Specify the path to our plugin jar that we built with `gradle jar`
-javascriptplugin ../../../../plugins/Sanex/lib/sample-obfuscation-plugin.jar ../../../../plugins/Sanex/lib/low.txt
-dontwarn com.guardsquare.samples.plugins.javascript.SanexJavascriptObfuscator

# Or we can also use Jscrambler plugin instead:
#-javascriptplugin ../../../../plugins/Jscrambler/lib/sample-obfuscation-plugin.jar jscrambler.json
#-dontwarn com.guardsquare.samples.plugins.javascript.JscramblerJavascriptObfuscator

# Specify that we want to obfuscate the javascript files
-obfuscatejavascript assets/**.bundle
