LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := nativecrash
LOCAL_SRC_FILES := com_example_jni_NativeBridge.c

# Strip all non-exported symbol names.
LOCAL_CPPFLAGS += -fvisibility=hidden
LOCAL_CFLAGS   += -fvisibility=hidden

include $(BUILD_SHARED_LIBRARY)
