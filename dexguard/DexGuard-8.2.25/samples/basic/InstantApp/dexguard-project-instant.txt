# Create two Instant App features (beside the base feature).
#
# You can put activities in the base feature, with empty feature name ''.
# You can put multiple activities in the same feature, by specifying the same
# feature name (but different URLs).

-instantappfeature hello com.example.HelloActivity   https://example.com/hello
-instantappfeature bye   com.example.GoodbyeActivity https://example.com/bye
