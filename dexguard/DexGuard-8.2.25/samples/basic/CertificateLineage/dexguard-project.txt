# Display some more statistics about the processed code.
-verbose

# The certificate lineage file that specifies our subsequent certificates.

-certificatelineage debug.lineage

# It's optional in this case, since we only have two certificates, and we're
# specifying both of them. DexGuard can then create the certificate lineage
# for us.

# The oldest signing key is already specified in build.gradle.

#-keystore         debug1.keystore
#-keystorepassword android
#-keyalias         AndroidDebugKey
#-keypassword      android

# We specify the replacement signing key here.

-keystore         debug2.keystore
-keystorepassword android
-keyalias         AndroidDebugKey
-keypassword      android
