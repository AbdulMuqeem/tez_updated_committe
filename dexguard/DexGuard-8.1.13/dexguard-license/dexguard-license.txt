# DexGuard license file
licensee              = Tez Financial Services
contact               = Adeel Raees
contactEmail          = ehsan.yusuf@tezfinancialservices.pk
product               = DexGuard
version               = 7.*,8.*
package               = com.tez.androidapp
expiryDate            = 2019-04-07
maintenanceExpiryDate = 2019-04-07
key                   = 02a4f4818e03e882627528e7219a12b16e9e57a5889901d315cb7fff338a6d85717a18214888a5afaf57f394dad8f19082a6e65b605584fe9a4b4e001c870ee4
