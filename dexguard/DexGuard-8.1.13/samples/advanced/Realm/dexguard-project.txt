# DexGuard's default settings are fine for this sample application.

# Display some more statistics about the processed code.
-verbose

# Hide warnings related to optional okhttp library
-dontwarn com.squareup.okhttp.**
-dontwarn okio.**
