/*
 * Sample application to illustrate processing with DexGuard.
 *
 * Copyright (c) 2012-2017 GuardSquare NV
 */
package com.example

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem

import kotlinx.android.synthetic.main.activity_main.*

/**
 * Sample activity that displays "Hello world!".
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
