This sample shows the use of the startup time optimization option 
-performanceprofile.

The option is currently enabled in the dexguard-project.txt file with a 
sample .hprof file for this application. To use this option for other
applications, you should follow the instructions in the documentation to 
generate your own .hprof files (docs/usage.html#performanceprofile).

To verify whether you enabled the option successfully, you can inspect the
dex file within the resulting apk with a tool such as dexdump. You will see 
that the classes are ordered differently compared to the apk without the 
performanceprofile option enabled. When you inspect the Java source files, 
you will see that this new ordering for this very simple sample corresponds 
to the order in which these classes are accessed during startup.