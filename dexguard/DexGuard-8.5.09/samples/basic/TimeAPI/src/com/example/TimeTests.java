/*
 * Sample application to illustrate Java 8 time API conversion with DexGuard.
 *
 * Copyright (c) 2018 Guardsquare NV
 */
package com.example;

import java.time.*;
import java.time.chrono.*;
import java.time.format.*;
import java.time.temporal.*;

import static java.time.DayOfWeek.*;
import static java.time.temporal.TemporalAdjusters.*;

import android.util.*;

/**
 * Various tests that use the Java 8 time API.
 */
public class TimeTests
{
    private static final String TAG = "TimeAPI";

    public static void run()
    {
        testLocalDate();
        testLocalDate2();
        testLocalDate3();
        testInstant();
        testZone();
        testPeriod();
        testFormatter();
        testChronology();
    }

    private static void testLocalDate()
    {
        LocalDate date  = LocalDate.of(2014, Month.JUNE, 10);
        int       year  = date.getYear();       // 2014
        Month     month = date.getMonth();      // JUNE
        int       dom   = date.getDayOfMonth(); // 10
        DayOfWeek dow   = date.getDayOfWeek();  // Tuesday
        int       len   = date.lengthOfMonth(); // 30 (days in June)
        boolean   leap  = date.isLeapYear();    // false (not a leap year)

        Log.d(TAG, "" + year + " " + month + " " + dom + " " + dow + " " + len + " " + leap);
    }

    private static void testLocalDate2()
    {
        LocalDate date = LocalDate.of(2014, Month.JUNE, 10);
        date = date.withYear(2015); // 2015-06-10
        Log.d(TAG, date.toString());
        date = date.plusMonths(2);  // 2015-08-10
        Log.d(TAG, date.toString());
        date = date.minusDays(1);   // 2015-08-09
        Log.d(TAG, date.toString());
    }

    private static void testLocalDate3()
    {
        LocalDate date = LocalDate.of(2014, Month.JUNE, 10);
        date = date.with(lastDayOfMonth());
        date = date.with(nextOrSame(WEDNESDAY));

        LocalTime time   = LocalTime.of(20, 30);
        int       hour   = time.getHour();   // 20
        int       minute = time.getMinute(); // 30
        time = time.withSecond(6);           // 20:30:06
        Log.d(TAG, time.toString());
        time = time.plusMinutes(3);          // 20:33:06
        Log.d(TAG, time.toString());

        LocalDateTime dt1 = LocalDateTime.of(2014, Month.JUNE, 10, 20, 30);
        Log.d(TAG, dt1.toString());
        LocalDateTime dt2 = LocalDateTime.of(date, time);
        Log.d(TAG, dt2.toString());
        LocalDateTime dt3 = date.atTime(20, 30);
        Log.d(TAG, dt3.toString());
        LocalDateTime dt4 = date.atTime(time);
        Log.d(TAG, dt4.toString());
    }

    private static void testInstant()
    {
        Instant start = Instant.now();
        // perform some calculation
        Instant end = Instant.now().plusMillis(100);
        Log.d(TAG, "end.isAfter(start) = " + end.isAfter(start));
    }

    private static void testZone()
    {
        ZoneId zone = ZoneId.of("Europe/Paris");

        LocalDate     date = LocalDate.of(2014, Month.JUNE, 10);
        ZonedDateTime zdt1 = date.atStartOfDay(zone);
        Log.d(TAG, zdt1.toString());

        Instant       instant = Instant.now();
        ZonedDateTime zdt2    = instant.atZone(zone);
        Log.d(TAG, zdt2.toString());
    }

    private static void testPeriod()
    {
        Period    sixMonths = Period.ofMonths(6);
        LocalDate date      = LocalDate.now();
        LocalDate future    = date.plus(sixMonths);
        Log.d(TAG, future.toString());
    }

    private static void testFormatter()
    {
        DateTimeFormatter f = DateTimeFormatter.ofPattern("dd/MM/uuuu");
        LocalDate date      = LocalDate.parse("24/06/2014", f);
        String str          = date.format(f);
        Log.d(TAG, str);
    }

    private static void testChronology()
    {
        Chronology chronology = IsoChronology.INSTANCE;
        ChronoLocalDate date  = chronology.dateNow();
        Log.d(TAG, date.toString());
    }
}
