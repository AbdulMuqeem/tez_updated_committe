/*
 * Sample application to illustrate Java 8 time API conversion with DexGuard.
 *
 * Copyright (c) 2018 Guardsquare NV
 */
package com.example;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.*;

/**
 * Sample activity that runs some Java 8 time API tests.
 */
public class HelloWorldActivity extends Activity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        TimeTests.run();

        Toast.makeText(this, "DexGuard has backported this Java 8 time API sample to Android", Toast.LENGTH_LONG).show();
    }
}
