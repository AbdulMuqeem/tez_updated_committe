/*
 * Sample application to illustrate Java 8 time API conversion with DexGuard.
 *
 * Copyright (c) 2018 Guardsquare NV
 */
package com.example;

import android.app.Application;
import com.jakewharton.threetenabp.AndroidThreeTen;


/**
 * Sample application that initializes the timezone database.
 */
public class HelloWorldApplication extends Application
{
    @Override
    public void onCreate()
    {
        super.onCreate();
        AndroidThreeTen.init(this);
    }
}
